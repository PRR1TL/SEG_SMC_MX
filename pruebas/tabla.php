
<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post">
<button type="submit" id="export_data" name='export_data' value="Export to excel" class="btn btn-info">Export to excel</button>
</form>

<table id="" class="table table-striped table-bordered">
<tr>
<th>Name</th>
<th>Gender</th>
<th>Age</th>
<th>Designation</th>
<th>Address</th>
</tr>
<tbody>
<?php for ($i = 0; $i < 20; $i++) { ?>
<tr>
<td><?php echo 'name'.$i; ?></td>
<td><?php echo 'gender'.$i;  ?></td>
<td><?php echo 'age'.$i; ?></td>
<td><?php echo 'designation'.$i;  ?></td>
<td><?php echo 'address'.$i;  ?></td>
</tr>
<?php } ?>
</tbody>
</table>

<?php 
    if(isset($_POST["export_data"])) {
$filename = "tecnicas".date('Ymd') . ".xls";
header("Content-Type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=\"$filename\"");
$show_coloumn = false;
if(!empty($developer_records)) {
foreach($developer_records as $record) {
if(!$show_coloumn) {
// display field/column names in first row
echo implode("\t", array_keys($record)) . "\n";
$show_coloumn = true;
}
echo implode("\t", array_values($record)) . "\n";
}
}
exit;
}

?>

