<!DOCTYPE html>
<html lang="en">

<head>
<title>A gridstack.js dashboard</title>

<link rel="stylesheet" href="https://bootswatch.com/3/paper/bootstrap.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/gridstack.js/0.4.0/gridstack.css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.0/lodash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gridstack.js/0.4.0/gridstack.all.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>

<body>
<div class="container">
<div class="row">
<div class="col-sm-12">
<h2>A gridstack.js dashboard</h2>
</div>
<div class="col-sm-12 well">

<!– gridstack.js elements go here –>
<div class="grid-stack " id="sortable">
<div class="grid-stack-item col-md-3 col-sm-4" data-gs-x="0" data-gs-y="0" data-gs-width="2"
data-gs-height="2">
<div class="grid-stack-item-content panel panel-primary">
<div class="panel-heading">
<h3 class="panel-title">Widget 1</h3>
</div>
<div class="panel-body">
content here
</div>
</div>
</div>
<div class="grid-stack-item col-md-3 col-sm-4" data-gs-x="2" data-gs-y="0" data-gs-width="2"
data-gs-height="2">
<div class="grid-stack-item-content panel panel-success">
<div class="panel-heading">
<h3 class="panel-title">Widget 2</h3>
</div>
<div class="panel-body">
content here
</div>
</div>
</div>
<div class="grid-stack-item col-md-3 col-sm-4" data-gs-x="4" data-gs-y="0" data-gs-width="2"
data-gs-height="2">
<div class="grid-stack-item-content panel panel-warning">
<div class="panel-heading">
<h3 class="panel-title">Widget 3</h3>
</div>
<div class="panel-body">
content here
</div>
</div>
</div>
<div class="grid-stack-item col-md-3 col-sm-4" data-gs-x="6" data-gs-y="0" data-gs-width="2"
data-gs-height="2">
<div class="grid-stack-item-content panel panel-danger">
<div class="panel-heading">
<h3 class="panel-title">Widget 4</h3>
</div>
<div class="panel-body">
content here
</div>
</div>
</div>
<div class="grid-stack-item col-md-3 col-sm-4" data-gs-x="8" data-gs-y="0" data-gs-width="2"
data-gs-height="2">
<div class="grid-stack-item-content panel panel-success">
<div class="panel-heading">
<h3 class="panel-title">Widget 5</h3>
</div>
<div class="panel-body">
content here

</div>
</div>
</div>

</div>
</div>
</div>
</div>
<script type="text/javascript">

$(function () {
$('.grid-stack').gridstack();
});

$(function () {
$("#sortable").sortable();
$("#sortable").disableSelection();
// $(“.panel").draggable();
// $(“.panel").resizable();
});
</script>
</body>

</html>