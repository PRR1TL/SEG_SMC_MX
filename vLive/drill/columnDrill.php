<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<style>
    #chartdiv{
        width: 100%;
        height: 50%;
    }
    
</style>

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>

<br><br><br><br><br>

<div id="chartdiv">
    <script>
        var chart;

        var chartData = [{
            country: "USA",
            visits: 4025,
            subdata: [{ 
                    country: "New York", visits: 1000 
                },{ 
                    country: "California", visits: 785 
                },{ 
                    country: "Florida", visits: 501 
                },{ 
                    country: "Illinois", visits: 321 
                },{ 
                    country: "Washington", visits: 101 
                }
            ]},{
                country: "China",
                visits: 1882
            },{
                country: "Japan",
                visits: 1809
            },{
                country: "Germany",
                visits: 1322
            }];

        AmCharts.ready(function() {
            // SERIAL CHART
            chart = new AmCharts.AmSerialChart();
            chart.dataProvider = chartData;
            chart.categoryField = "country";
            chart.startDuration = 1;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.labelRotation = 90;
            categoryAxis.gridPosition = "start";

            // value
            // in case you don't want to change default settings of value axis,
            // you don't need to create it, as one value axis is created automatically.
            // GRAPH
            var graph = new AmCharts.AmGraph();
            graph.valueField = "visits";
            graph.balloonText = "[[category]]: [[value]]";
            graph.type = "column";
            graph.lineAlpha = 0;
            graph.fillAlphas = 0.8;
            chart.addGraph(graph);

            chart.addListener("clickSlice", function (event) {
                // get chart object
                var chart = event.chart;

                // check if drill-down data is avaliable
                if (event.dataItem.dataContext.data !== undefined) {
                    // save for back button
                    chart.drillLevels.push(event.dataItem.dataContext);
                    // replace data
                    chart.dataProvider = event.dataItem.dataContext.data;
                    // replace title
                    chart.titles[0].text = event.dataItem.dataContext.title;

                    // add back link
                    // let's add a label to go back to yearly data
                    event.chart.addLabel(
                        20, 35, 
                        "< Go back",
                        undefined, 
                        undefined, 
                        undefined, 
                        undefined, 
                        undefined, 
                        undefined, 
                    'javascript:drillUp();');

                    // take in data and animate
                    chart.validateData();
                    chart.animateAgain();
                } 
            });

            function drillUp() {
                // get level
                chart.drillLevels.pop();
                var level = chart.drillLevels[chart.drillLevels.length - 1];

                // replace data
                chart.dataProvider = level.data;

                // replace title
                chart.titles[0].text = level.title;

                // remove labels
                if (chart.drillLevels.length === 1)
                  chart.clearLabels();

                // take in data and animate
                chart.validateData();
                chart.animateAgain();
             }

            chart.addListener("clickGraphItem", function (event) {
                // let's look if the clicked graph item had any subdata to drill-down into
                if (event.item.dataContext.subdata != undefined) {
                    // wow it has!
                    // let's set that as chart's dataProvider
                    event.chart.dataProvider = event.item.dataContext.subdata;                    
                    event.chart.addLabel(
                        0, 0, 
                        "< Go back",
                        undefined, 
                        undefined, 
                        undefined, 
                        undefined, 
                        undefined, 
                        undefined, 
                      'javascript:drillUp();');
                    
                    event.chart.validateData();
                } 
            });
            chart.write("chartdiv");
        });
        
        
        function drillUp() {
            console.log("si entro");
            // get level
            chart.drillLevels.pop();
            var level = chart.drillLevels[chart.drillLevels.length - 1];
            // replace data
            chart.dataProvider = level.data;
            // replace title
            chart.titles[0].text = level.title;
            // remove labels
            if (chart.drillLevels.length === 1)
                chart.clearLabels();
            // take in data and animate
            chart.validateData();
            chart.animateAgain();
        }
        
    </script>
</div>