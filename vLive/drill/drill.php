<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<style>
    #chartdiv{
        width: 100%;
        height: 50%
    }
    
</style>
    


<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<div id="chartdiv">
    <script>
        var chartData = [{
            "title": "Marketing",
            "value": 23,
            "url":"#",
            "description":"click to drill-down",
            "data": [
              { "title": "Jan", "value": 1, "data": [
                { "title": "AAA", "value": 2 },
                { "title": "BBB", "value": 5 },
                { "title": "CCC", "value": 1 },
              ] },
              { "title": "Feb", "value": 2 },
              { "title": "Mar", "value": 1 },
              { "title": "Apr", "value": 3 },
              { "title": "May", "value": 2 },
              { "title": "Jun", "value": 1 },
              { "title": "Jul", "value": 2 },
              { "title": "Aug", "value": 3 },
              { "title": "Sep", "value": 3 },
              { "title": "Oct", "value": 1 },
              { "title": "Nov", "value": 1 },
              { "title": "Dec", "value": 3 }
            ]
          }, {
            "title": "Sales",
            "value": 26,
            "url":"#",
            "description":"click to drill-down",
            "data": [
              { "title": "Jan", "value": 4 },
              { "title": "Feb", "value": 3 },
              { "title": "Mar", "value": 1 },
              { "title": "Apr", "value": 4 },
              { "title": "May", "value": 2 },
              { "title": "Jun", "value": 1 },
              { "title": "Jul", "value": 2 },
              { "title": "Aug", "value": 2 },
              { "title": "Sep", "value": 3 },
              { "title": "Oct", "value": 1 },
              { "title": "Nov", "value": 1 },
              { "title": "Dec", "value": 3 }
            ]
          }, {
            "title": "Logistics",
            "value": 30,
            "url":"#",
            "description":"click to drill-down",
            "data": [
              { "title": "Jan", "value": 2 },
              { "title": "Feb", "value": 3 },
              { "title": "Mar", "value": 1 },
              { "title": "Apr", "value": 5 },
              { "title": "May", "value": 2 },
              { "title": "Jun", "value": 1 },
              { "title": "Jul", "value": 2 },
              { "title": "Aug", "value": 2 },
              { "title": "Sep", "value": 3 },
              { "title": "Oct", "value": 1 },
              { "title": "Nov", "value": 1 },
              { "title": "Dec", "value": 3 }
            ]
          }];

          // create pie chart
          var chart = AmCharts.makeChart("chartdiv", {
            "type": "pie",
            "dataProvider": chartData,
            "valueField": "value",
            "titleField": "title",
            "labelText": "[[title]]: [[value]]",
            "pullOutOnlyOne": true,
            "titles": [{
              "text": "Departments"
            }],
            "allLabels": []
          });

          // initialize step array
          chart.drillLevels = [{
            "title": "Departments",
            "data": chartData
          }];

          // add slice click handler
          chart.addListener("clickSlice", function (event) {
            // get chart object
            var chart = event.chart;
            // check if drill-down data is avaliable
            if (event.dataItem.dataContext.data !== undefined) {
              // save for back button
              chart.drillLevels.push(event.dataItem.dataContext);
              // replace data
              chart.dataProvider = event.dataItem.dataContext.data;
              // replace title
              chart.titles[0].text = event.dataItem.dataContext.title;
              // add back link
              // let's add a label to go back to yearly data
              event.chart.addLabel(
                0, 25, 
                "< Go back",
                undefined, 
                undefined, 
                undefined, 
                undefined, 
                undefined, 
                undefined, 
                'javascript:drillUp();');
              // take in data and animate
              chart.validateData();
              chart.animateAgain();
            }
          });

          function drillUp() {

            // get level
            chart.drillLevels.pop();
            var level = chart.drillLevels[chart.drillLevels.length - 1];

            // replace data
            chart.dataProvider = level.data;

            // replace title
            chart.titles[0].text = level.title;

            // remove labels
            if (chart.drillLevels.length === 1)
              chart.clearLabels();

            // take in data and animate
            chart.validateData();
            chart.animateAgain();
          }
    </script>
</div>





