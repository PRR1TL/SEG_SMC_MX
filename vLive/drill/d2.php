<!DOCTYPE>

<html>
<head>
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
<script src="https://www.amcharts.com/lib/3/amstock.js"></script>

</head>


<body>
<div id="chartdiv" style="width: 100%; height: 355px;"></div>

<script>
var chart;

var chartData = [{
    country: "Somaliland",
    visits: 4025,
     "allLabels": [{
        "text": "",
        "x": 10,
        "y": 15,
        "url": "#"
    }],

    subdata: [
        { country: "Gabiley", visits: 1000,   subdata: [
        { country: "Arabsiyo", visits: 1000 },
        { country: "Gabilay", visits: 785 },
         { country: "Wajale", visits: 1000 },

    ] },
        { country: "Sanaag", visits: 785, subdata: [
        { country: "Ceeri Gaabo", visits: 1000 },


    ] },
        { country: "M.Jeex", visits: 501,  subdata: [
        { country: "Hargeysa", visits: 1000 },


    ] }

    ]},
{
    country: "Puntland",
    visits: 3882,
    subdata: [
        { country: "Bari", visits: 1000,   subdata: [
        { country: "BOSASO", visits: 500 },
        { country: "Balibusle", visits: 785 },


    ] },
         { title : "title1",
        country: "Mudug", visits: 785, subdata: [
        { country: "Sheerbi", visits: 1000 },
          { country: "Ufayn", visits: 1000 },



    ] }


    ]

    },

];


AmCharts.ready(function() {
    // SERIAL CHART
    chart = new AmCharts.AmSerialChart();
    chart.dataProvider = chartData;
    chart.categoryField = "country";
    chart.startDuration = 0;

    // AXES
    // category
    var categoryAxis = chart.categoryAxis;
    categoryAxis.labelRotation = 0;
    categoryAxis.gridPosition = "start";

    // value
    // in case you don't want to change default settings of value axis,
    // you don't need to create it, as one value axis is created automatically.
    // GRAPH
    var graph = new AmCharts.AmGraph();
    graph.valueField = "visits";
    graph.balloonText = "[[category]]: [[value]]";
     graph.titleField = "title";
    graph.type = "column";
    graph.lineAlpha = 0;
    graph.fillAlphas = 0.8;
    chart.addGraph(graph);
    chart.angle = 30;
chart.depth3D = 15;


chart.drillLevels = [{
  "title": "Average Revenue Per User",
  "data": chartData
}];

    chart.addListener("clickGraphItem", function (event) {
        // let's look if the clicked graph item had any subdata to drill-down into
        if (event.item.dataContext.subdata != undefined) {
            // wow it has!
            // let's set that as chart's dataProvider


    // replace data


    // replace title



    // add back link
    // let's add a label to go back to yearly data      



    // add back link
    // let's add a label to go back to yearly data
              event.chart.addLabel(
      0, 25, 
      "<<< Go back",
      undefined, 
      undefined, 
      undefined, 
      undefined, 
      undefined, 
      undefined, 
      'javascript:drillUp();');


            event.chart.dataProvider = event.item.dataContext.subdata;
            event.chart.validateData();

        }

           event.chart.allLabels[0].text = "Go Back " + event.chart.title[0].text;
    });


    chart.write("chartdiv");

});

function drillUp() {
    // get level
    chart.drillLevels.pop();
    var level = chart.drillLevels[chart.drillLevels.length - 1];

    // replace data
    chart.dataProvider = level.data;

    // replace title
    chart.titles[0].text = level.title;

    // remove labels
    if (chart.drillLevels.length === 1)
      chart.clearLabels();

    // take in data and animate
    chart.validateData();
    chart.animateAgain();
}


</script>

</body>



</html>