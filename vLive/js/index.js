/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {  
   
    //INSERT DESDE EL INDEX 
    $('#fILAsistencia').submit(function( event ) { 
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "./db/admin/iLAsistencia.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertILAsistencia").html(datos);                 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    }); 
    
    // INSERT DE LISTA DE JUNTAS 
    $('#fIJunta').submit(function( event ) { 
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "./db/admin/iLJuntas.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertIJunta").html(datos);                 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    }); 
    
    // INSERT EVALUACON DE 5Ss
    $('#fLDaily').submit(function( event ) { 
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "./db/admin/i5Ss.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertIA5S").html(datos);                 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    }); 
});   
   