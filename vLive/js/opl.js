/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function() { 
    //VISUALIZACION DE MODAL
    $('.btn-login').on('click', function() { 
        //console.log("entra");
        $('#mLogin').modal({ 
            show: true, 
            backdrop: false, 
            keyboard: false 
        }); 
    }); 
    
    $('.btn-logOut').on('click', function(e){ 
        $('#mLogOut').modal({ 
            show: true, 
            backdrop: false 
        }); 
    }); 
    
    //PASO DE INFORMACION DE MODAL A BASE DE DATOS PARA COMPROBAR DATOS 
    $( "#fLogin" ).submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/login.php",
            data: parametros,
            success: function(datos){
                $("#alertLogin").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
    
    $( "#fLogOut" ).submit(function( event ) {  //FUNCION CUANDO LE DAN EN LOGIN
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/exit.php",
            data: parametros,
            success: function(datos){
                $("#datos_ajax").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            if (jqXHR.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (jqXHR.status == 404) {
                alert('Requested page not found [404]');
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
          });
        event.preventDefault();
    });      
    
    $("#fCompromisoL").datepicker({
        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
        minDate: new Date(),
        //OYENTES DE LOS PICKERS 
        onSelect: function(date) {
            //Cuando se seleccione una fecha se cierra el panel
            $("#ui-datepicker-div").hide(); 
        }
    });    
    
    $("#fStandard").datepicker({ 
        //OYENTES DE LOS PICKERS 
        onSelect: function(date) { 
            //Cuando se seleccione una fecha se cierra el panel
            $("#ui-datepicker-div").hide(); 
        } 
    }); 
    
    var dI = new Date();
    var dIA= new Date();
    var dF = new Date();
    dI.setDate(dI.getDate() - 25);
    dIA.setDate(dIA.getDate() - 5);
    dF.setDate(dF.getDate());
    
    $("#fRegistro").datepicker({ 
        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER  
        minDate: dI, 
        maxDate: dF, 
        //OYENTES DE LOS PICKERS 
        onSelect: function(date) { 
            //Cuando se seleccione una fecha se cierra el panel 
            $("#ui-datepicker-div").hide(); 
        } 
    }); 
    
    $("#fRegistroC").datepicker({ 
        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
        minDate: dIA, 
        maxDate: dF, 
        //OYENTES DE LOS PICKERS 
        onSelect: function(date) { 
            //Cuando se seleccione una fecha se cierra el panel 
            changeDatos(); 
            $("#ui-datepicker-div").hide(); 
        } 
    });

    $("#fCompromisoP").datepicker({
        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
        minDate: new Date(), 
        //OYENTES DE LOS PICKERS 
        onSelect: function ( date ) {
            //cuando se seleccione una fecha se cierra el panel
            $("#ui-datepicker-div").hide();                  
        } 
    }); 
    
    $("#fIniOpl").datepicker({ 
        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
        maxDate: new Date(), 
        //OYENTES DE LOS PICKERS 
        onSelect: function ( date ) {
            //cuando se seleccione una fecha se cierra el panel
            $("#ui-datepicker-div").hide();                  
        } 
    }); 
    
    $("#fFinOpl").datepicker({ 
        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
        maxDate: new Date(), 
        //OYENTES DE LOS PICKERS 
        onSelect: function ( date ) {
            //cuando se seleccione una fecha se cierra el panel
            $("#ui-datepicker-div").hide();                  
        } 
    }); 
    
    $("#fIni").datepicker({ 
        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
        maxDate: new Date(), 
        dateFormat: "M, yy",
        //OYENTES DE LOS PICKERS 
        onSelect: function ( date ) {
            //cuando se seleccione una fecha se cierra el panel
            $("#ui-datepicker-div").hide();                  
        } 
    }); 
    
    $("#fIniM").datepicker({ 
        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
        maxDate: new Date(), 
        dateFormat: "M, yy",
        //OYENTES DE LOS PICKERS 
        onSelect: function ( date ) {
            //cuando se seleccione una fecha se cierra el panel
            $("#ui-datepicker-div").hide();                  
        } 
    }); 
    
    $("#fFin").datepicker({
        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
        maxDate: new Date(), 
        //OYENTES DE LOS PICKERS 
        onSelect: function ( date ) {
            //cuando se seleccione una fecha se cierra el panel
            $("#ui-datepicker-div").hide();                  
        } 
    }); 

    //ENVIO DE INFORMACION
    //SEGURIDAD
    $('#fISeguridad').submit(function( event ) { 
        var parametros = $(this).serialize(); 
        $.ajax({
            type: "POST", 
            url: "../../db/admin/iSeguridad.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertISeguridad").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
    
    //OPL LINEAS
    $('#fIOPL').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/iOPL.php",
            data: parametros,
            success: function(datos) { 
                $("#alertOpl").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
   
    $('#fUOplL').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/uOPL.php",
            data: parametros,
            success: function(datos) { 
                $("#alertUOplLU").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
    
    $('#fDOplL').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/dOPL.php",
            data: parametros,
            success: function(datos) { 
                $("#alerDtOplL").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
    
    //OPL PROYECTOS
    $('#fIOPLP').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/iOPL.php",
            data: parametros,
            success: function(datos) { 
                $("#alertOplP").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
   
    $('#fUOplP').submit(function( event ) {  
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/uOPL.php",
            data: parametros,
            success: function(datos) { 
                $("#alertOplPU").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
    
    $('#fDOplP').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/dOPL.php",
            data: parametros,
            success: function(datos) { 
                $("#alertOplPD").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
    
    //CALIDAD INTERNA
    $('#fIQI').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({ 
            type: "POST",
            url: "../../db/admin/iQI.php",
            data: parametros,
            success: function(datos) { 
                $("#alertIQI").html(datos);
                location.href="../otras/calidadInterna.php";
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            }
        }); 
        event.preventDefault(); 
    }); 
    
    // 0 KM
    $('#fIKM').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/iKM.php",
            data: parametros,
            success: function(datos) { 
                $("#alertOplPD").html(datos);
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
    
    //INVENTARIO
    $('#fIInventory').submit(function( event ) { 
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/iInventory.php",
            data: parametros,
            success: function(datos) { 
                $("#alertIInventory").html(datos);
                location.href="../otras/inventario.php";
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    }); 
    
    //INVENTARIO
    $('#fITarget').submit(function( event ) { 
        //alert("entra submit FIOPL");
        var parametros = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "../../db/admin/iTarget.php",
            data: parametros,
            success: function(datos) { 
                $("#alertITarget").html(datos); 
            }
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) {
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else {
                alert('Uncaught Error: ' + jqXHR.responseText);
            }
        });
        event.preventDefault();
    });     
    
    //LISTA DE ASISTENCIA PARA JUNTA
    $('#fIJunta').submit(function( event ) { 
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/iLJuntas.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertIJunta").html(datos);                 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    }); 
    
    //LISTA DE ASISTENCIA 
    $('#fILAsistencia').submit(function( event ) { 
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/iLAsistencia.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertILAsistencia").html(datos);                 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    }); 
    
    $('#fULAsistencia').submit(function( event ) { 
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/uLAsistencia.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertULAsistencia").html(datos); 
                location.reload();
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    }); 
    
    $('#fDLAsistencia').submit(function( event ) { 
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/dLAsistencia.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertLAD").html(datos); 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    }); 
    
    $('#fITProduccion').submit(function( event ) { 
        console.log("entra");
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/iTProduccion.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertITProduccion").html(datos); 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    });
    
    $('#fUTLinea').submit(function( event ) { 
        console.log("entra");
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/uTLinea.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertITProduccion").html(datos); 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    });
    
    $('#fUTProd').submit(function( event ) { 
        var parametros = $(this).serialize(); 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/uTProd.php", 
            data: parametros, 
            success: function(datos) { 
                $("#alertITProduccion").html(datos); 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        event.preventDefault(); 
    });
    
    //TARGETS
    $('#mUTLinea').on('show.bs.modal', function (event) { 
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var prod = button.data('prod'); // Extraer la información de atributos de datos     
        var scrap = button.data('scrap'); // Extraer la información de atributos de datos
        var cambio = button.data('cambio'); // Extraer la información de atributos de datos
        var cali= button.data('cali'); // Extraer la información de atributos de datos
        var org = button.data('org'); // Extraer la información de atributos de datos
        var tec = button.data('tec'); // Extraer la información de atributos de datos
        var oee = button.data('oee'); // Extraer la información de atributos de datos
        var linea = button.data('linea'); // Extraer la información de atributos de datos
        var dia = button.data('dia'); // Extraer la información de atributos de datos

        var modal = $(this);
        modal.find('.modal-body #tProdU').val(prod);
        modal.find('.modal-body #tScrapU').val(scrap);
        modal.find('.modal-body #tCamU').val(cambio);
        modal.find('.modal-body #tCaliU').val(cali);
        modal.find('.modal-body #tOrgU').val(org);
        modal.find('.modal-body #tTecU').val(tec);
        modal.find('.modal-body #tOEEU').val(oee);
        modal.find('.modal-body #lineaU').val(linea);
        modal.find('.modal-body #fStandard').val(dia);
    });
    
    $('#mUTProd').on('show.bs.modal', function (event) { 
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var pzas = button.data('pzas'); // Extraer la información de atributos de datos 
        var linea = button.data('linea'); // Extraer la información de atributos de datos
        var dia = button.data('dia'); // Extraer la información de atributos de datos

        var modal = $(this);
        modal.find('.modal-body #mtaPzasU0').val(pzas); 
        modal.find('.modal-body #mtaPzasU').val(pzas);
        modal.find('.modal-body #lineaTU').val(linea);
        modal.find('.modal-body #fProdU').val(dia);
    });
    
    // OBTENCION DE INFORMACION Y MOSTRAR EN MODAL 
    // OPL LINEA
    $('#mUOplL').on('show.bs.modal', function (event) { 
        //console.log("algo esta pasando");
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var idOpl = button.data('id'); // Extraer la información de atributos de datos     
        var fInicio = button.data('finicio'); // Extraer la información de atributos de datos
        var fCompromiso = button.data('fcompromiso'); // Extraer la información de atributos de datos
        var hallazgo = button.data('hallazgo'); // Extraer la información de atributos de datos
        var op = button.data('op'); // Extraer la información de atributos de datos
        var linea = button.data('linea'); // Extraer la información de atributos de datos
        var causa = button.data('causa'); // Extraer la información de atributos de datos
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var responsable = button.data('resp'); // Extraer la información de atributos de datos
        var soporte = button.data('sop'); // Extraer la información de atributos de datos
        var estado = button.data('estado'); // Extraer la información de atributos de datos

        var modal = $(this);
        modal.find('.modal-body #idOplLU').val(idOpl);
        modal.find('.modal-body #fRegistroLU').val(fInicio);
        modal.find('.modal-body #fCompromisoLU').val(fCompromiso);
        modal.find('.modal-body #hallazgoLU').val(hallazgo);
        modal.find('.modal-body #operacionLU').val(op);
        modal.find('.modal-body #causaLU').val(causa);
        modal.find('.modal-body #lineaLU').val(linea);
        modal.find('.modal-body #accionLU').val(accion);
        modal.find('.modal-body #responsableLU').val(responsable);
        modal.find('.modal-body #detectaLU').val(soporte);
        modal.find('.modal-body #estadoLU').val(estado);
    });
    
    $('#mDOplL').on('show.bs.modal', function (event) { 
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var idOpl = button.data('id'); // Extraer la información de atributos de datos     
        var fInicio = button.data('finicio'); // Extraer la información de atributos de datos
        var fCompromiso = button.data('fcompromiso'); // Extraer la información de atributos de datos
        var hallazgo = button.data('hallazgo'); // Extraer la información de atributos de datos
        var op = button.data('op'); // Extraer la información de atributos de datos
        var linea = button.data('linea'); // Extraer la información de atributos de datos
        var causa = button.data('causa'); // Extraer la información de atributos de datos
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var responsable = button.data('resp'); // Extraer la información de atributos de datos
        var soporte = button.data('sop'); // Extraer la información de atributos de datos
        var estado = button.data('estado'); // Extraer la información de atributos de datos

        var modal = $(this);
        modal.find('.modal-body #idOplLD').val(idOpl);
        modal.find('.modal-body #fRegistroLD').val(fInicio);
        modal.find('.modal-body #fCompromisoLD').val(fCompromiso);
        modal.find('.modal-body #hallazgoLD').val(hallazgo);
        modal.find('.modal-body #operacionLD').val(op);
        modal.find('.modal-body #causaLD').val(causa);
        modal.find('.modal-body #lineaLD').val(linea);
        modal.find('.modal-body #accionLD').val(accion);
        modal.find('.modal-body #responsableLD').val(responsable);
        modal.find('.modal-body #detectaLD').val(soporte);
        modal.find('.modal-body #estadoLD').val(estado);
    });
    
    //OPL PROYECTO
    $('#mUOplP').on('show.bs.modal', function (event) { 
        //console.log("algo esta pasando");
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var idpr = button.data('idpr'); // Extraer la información de atributos de datos     
        var id = button.data('id'); // Extraer la información de atributos de datos  
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var area = button.data('area'); // Extraer la información de atributos de datos
        var fCompromiso = button.data('fcompromiso'); // Extraer la información de atributos de datos
        var fInicio = button.data('fini'); // Extraer la información de atributos de datos
        var estado = button.data('estatus'); // Extraer la información de atributos de datos
        var evaluacion = button.data('evaluacion'); // Extraer la información de atributos de datos 
        var responsable = button.data('resp'); // Extraer la información de atributos de datos 
        var soporte = button.data('soporte'); // Extraer la información de atributos de datos 

        var modal = $(this);
        modal.find('.modal-body #idOplPU').val(id); 
        modal.find('.modal-body #idOplPrPU').val(idpr); 
        modal.find('.modal-body #fRegistroPU').val(fInicio); 
        modal.find('.modal-body #fCompromisoPU').val(fCompromiso); 
        modal.find('.modal-body #lineaPU').val(area); 
        modal.find('.modal-body #accionPU').val(accion); 
        modal.find('.modal-body #areaPU').val(area); 
        modal.find('.modal-body #responsablePU').val(responsable); 
        modal.find('.modal-body #detectaPU').val(soporte); 
        modal.find('.modal-body #estadoPU').val(estado); 
        modal.find('.modal-body #evaluacionPU').val(evaluacion); 
    });
    
    $('#mDOplP').on('show.bs.modal', function (event) { 
        var button = $(event.relatedTarget); // Botón que activó el modal    
        var idpr = button.data('idpr'); // Extraer la información de atributos de datos     
        var id = button.data('id'); // Extraer la información de atributos de datos  
        var accion = button.data('accion'); // Extraer la información de atributos de datos
        var area = button.data('area'); // Extraer la información de atributos de datos
        var fCompromiso = button.data('fcompromiso'); // Extraer la información de atributos de datos
        var fInicio = button.data('fini'); // Extraer la información de atributos de datos
        var estado = button.data('estatus'); // Extraer la información de atributos de datos
        var evaluacion = button.data('evaluacion'); // Extraer la información de atributos de datos 
        var responsable = button.data('resp'); // Extraer la información de atributos de datos 
        var soporte = button.data('soporte'); // Extraer la información de atributos de datos 

        var modal = $(this);
        modal.find('.modal-body #idOplPD').val(id); 
        modal.find('.modal-body #idOplPrPD').val(idpr); 
        modal.find('.modal-body #fRegistroPD').val(fInicio); 
        modal.find('.modal-body #fCompromisoPD').val(fCompromiso); 
        modal.find('.modal-body #lineaPD').val(area); 
        modal.find('.modal-body #accionPD').val(accion); 
        modal.find('.modal-body #areaPD').val(area); 
        modal.find('.modal-body #responsablePD').val(responsable); 
        modal.find('.modal-body #detectaPD').val(soporte); 
        modal.find('.modal-body #estadoPD').val(estado); 
        modal.find('.modal-body #evaluacionPD').val(evaluacion); 
    });
    
    //LISTAS DE ASISTENCIA 
    $('#mULAsistencia').on('show.bs.modal', function (event) { 
        var button = $(event.relatedTarget); // Botón que activó el modal 
        var id = button.data('numempleado'); // Extraer la información de atributos de datos 
        var linea = button.data('linea'); // Extraer la información de atributos de datos  
        var fecha = button.data('fecha'); // Extraer la información de atributos de datos
        var nombre = button.data('nombre'); // Extraer la información de atributos de datos
        var vasist = button.data('tasist'); // Extraer la información de atributos de datos
        var horas = button.data('horas'); // Extraer la información de atributos de datos
        var operacion = button.data('operacion'); // Extraer la información de atributos de datos
        
        console.log("*id: "+id+" l: "+fecha+" f: "+fecha+" n: "+nombre+" t: "+vasist+" h: "+horas+" ps: "+operacion); 
        var modal = $(this); 
        modal.find('.modal-body #uLAId').val(id); 
        modal.find('.modal-body #uLALinea').val(linea); 
        modal.find('.modal-body #uLANombre').val(nombre); 
        modal.find('.modal-body #tAsistencia').val(vasist); 
        modal.find('.modal-body #uTAsistencia').val(vasist); 
        modal.find('.modal-body #uLOperacion').val(operacion); 
        modal.find('.modal-body #uLAHoras').val(horas); 
        modal.find('.modal-body #uLAFecha').val(fecha); 
    }); 
    
    //LISTAS DE ASISTENCIA 
    $('#mULJunta').on('show.bs.modal', function (event) { 
        var button = $(event.relatedTarget); // Botón que activó el modal 
        
        var linea = button.data('linea'); // Extraer la información de atributos de datos  
        var fecha = button.data('fecha'); // Extraer la información de atributos de datos
        var nombre = button.data('nombre'); // Extraer la información de atributos de datos
        var vasist = button.data('tasist'); // Extraer la información de atributos de datos
    
        console.log("* l: "+fecha+" f: "+fecha+" n: "+nombre+" t: "+vasist); 
        var modal = $(this); 
        modal.find('.modal-body #uLALinea').val(linea); 
        modal.find('.modal-body #uLANombre').val(nombre); 
        modal.find('.modal-body #tAsistencia').val(vasist); 
        modal.find('.modal-body #uTAsistencia').val(vasist); 
        modal.find('.modal-body #uLAFecha').val(fecha); 
    }); 
    
    $('#mDLAsistencia').on('show.bs.modal', function (event) { 
        var tA = "";
        var button = $(event.relatedTarget); // Botón que activó el modal 
        var id = button.data('numempleado'); // Extraer la información de atributos de datos 
        var linea = button.data('linea'); // Extraer la información de atributos de datos  
        var fecha = button.data('fecha'); // Extraer la información de atributos de datos
        var nombre = button.data('nombre'); // Extraer la información de atributos de datos
        var vasist = button.data('tasist'); // Extraer la información de atributos de datos
        var horas = button.data('horas'); // Extraer la información de atributos de datos
        var puesto = button.data('puesto'); // Extraer la información de atributos de datos
        
        switch(vasist){
            case 'A': 
                tA = "Asistencia";
                break;
            case 'B': 
                tA = "Baja";
                break;
            case 'C': 
                tA = "Capacitacion";
                break;            
            case 'F': 
                tA = "Falta";
                break;
            case 'I': 
                tA = "Incapacidad";
                break;
            case 'P': 
                tA = "Permiso";
                break; 
            case 'S': 
                tA = "Suspencion";
                break;
            case 'V': 
                tA = "Vacaciones";
                break;
            case 'R': 
                tA = "Retardo";
                break;
        } 
         
        var modal = $(this); 
        modal.find('.modal-body #idLAD').val(id); 
        modal.find('.modal-body #lineaLAD').val(linea); 
        modal.find('.modal-body #nameLAD').val(nombre); 
        modal.find('.modal-body #tAsisLAD').val(tA);  
        modal.find('.modal-body #puestoLAD').val(puesto); 
        modal.find('.modal-body #fechaLAD').val(fecha); 
    }); 
    
});






