<script src="//www.amcharts.com/lib/3/amcharts.js"></script>
<script src="//www.amcharts.com/lib/3/serial.js"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js"></script>
    
<?php

/* 
 *  To change this license header, choose License Headers in Project Properties.
 *  To change this template file, choose Tools | Templates
 *  and open the template in the editor.
 */

    session_start();
    include '../../../db/ServerFunctions.php';   
    
    $anio = $_SESSION['anio'];
    $mes = $_SESSION['mes']; 
    
    #INCIALIZACION DE VARIABLES PARA LAS FECHAS
    for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $fecha[$vDate] = date("M, d", strtotime($i)); 
        $prodDay[$vDate] = 0; 
        $caliDay[$vDate] = 0; 
        $orgDay[$vDate] = 0; 
        $tecDay[$vDate] = 0; 
        $cambioDay[$vDate] = 0; 
        $desDay[$vDate] = 0; 
        $targetDay[$vDate] = 0; 
        $pTotalDay[$vDate] = 0;            
    } 
    
    #CONSULTA PARA OEE DIARIO
    $oeeProductionDay = oeeProductionDay($_SESSION['linea'], $_SESSION['fIni'], $_SESSION['fFin']); 
    for($i = 0; $i < count($oeeProductionDay); $i++) { 
        $date = explode("-", $oeeProductionDay[$i][0],3); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $prodDay[$vDate] = @round($oeeProductionDay[$i][1],2); 
        $caliDay[$vDate] = @round($oeeProductionDay[$i][4],2); 
        $orgDay[$vDate] = @round($oeeProductionDay[$i][3],2); 
        $tecDay[$vDate] = @round($oeeProductionDay[$i][2],2); 
        $cambioDay[$vDate] = @round($oeeProductionDay[$i][5],2); 
        $desDay[$vDate] = @round($oeeProductionDay[$i][6],2); 

        //DATOS PARA LA TABLA 
        $pTopic[1][$vDate] = @round($oeeProductionDay[$i][1],2); 
        $pTopic[2][$vDate] = @round($oeeProductionDay[$i][4],2); 
        $pTopic[3][$vDate] = @round($oeeProductionDay[$i][3],2); 
        $pTopic[4][$vDate] = @round($oeeProductionDay[$i][2],2); 
        $pTopic[5][$vDate] = @round($oeeProductionDay[$i][5],2); 
        $pTopic[6][$vDate] = @round($oeeProductionDay[$i][6],2); 
        $pTotalDay[$vDate] = @round($pTopic[1][$vDate]+$pTopic[2][$vDate]+$pTopic[3][$vDate]+$pTopic[4][$vDate]+$pTopic[5][$vDate]+$pTopic[6][$vDate],1);
        
    } 
    
    #CONSULTA DE TARGET 
    $ctargetDay = cTargetOEEDaily($_SESSION['linea'], $_SESSION['fIni'], $_SESSION['fFin']); 
    for ($i = 0; $i < count($ctargetDay); $i++){ 
        $date = explode("-", $ctargetDay[$i][0],3); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $targetDay[$vDate] = $ctargetDay[$i][1]; 
    } 
    
    #CONSULTA DE OEE MENSUAL 
    $cOEEAcumm = cPercentTopicMonth($_SESSION['linea'], date("Y", strtotime($_SESSION['fIni'])), date("m", strtotime($_SESSION['fIni']))); 
    for ($i = 0; $i < count($cOEEAcumm); $i++){
        $aOEE = @round($cOEEAcumm[$i][1],2); 
        $aTec = @round($cOEEAcumm[$i][2],2);  
        $aOrg = @round($cOEEAcumm[$i][3],2);  
        $aCali = @round($cOEEAcumm[$i][4],2); 
        $aCambio = @round($cOEEAcumm[$i][5],2);  
        $aDes = @round($cOEEAcumm[$i][6],2);  
    } 
    
    
?>
<style>
    #oee {
        width: 100%; 
        min-height: 200px; 
        max-height: 600px;
        margin-top: -12px;
    }
</style>

<div > 
    <div id="oee" >         
        <script> 
            var chart = AmCharts.makeChart("oee", { 
                "type": "serial",
                "theme": "none",                
                "dataProvider": [<?php for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                        $date = explode("-", $i); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?> 
                { 
                    "date": "<?php echo $fecha[$vDate]; ?>", 
                    "prod": <?php echo $prodDay[$vDate]; ?>, 
                    "calidad": <?php echo $caliDay[$vDate]; ?>, 
                    "org": <?php echo $orgDay[$vDate]; ?>, 
                    "tec": <?php echo $tecDay[$vDate]; ?>, 
                    "cambio": <?php echo $cambioDay[$vDate]; ?>, 
                    "desempenio": <?php echo $desDay[$vDate]; ?>, 
                    "Meta": <?php echo $targetDay[$vDate]; ?> 
                }, 
                <?php } ?> {
                    "date": "Acumm",
                    "prod": <?php echo $aOEE; ?>, 
                    "calidad": <?php echo $aCali; ?>, 
                    "org": <?php echo $aOrg; ?>, 
                    "tec": <?php echo $aTec; ?>, 
                    "cambio": <?php echo $aCambio; ?>, 
                    "desempenio": <?php echo $aDes; ?>, 
                    "Meta": <?php echo $targetDay[$vDate]; ?> 
                }],
                "valueAxes": [{ 
                    "maximum": 100,
                    "minimum": 0,
                    "stackType": "regular", 
                    "axisAlpha": 0.3, 
                    "gridAlpha": 0.2 
                }], 
                "graphs": [ {
                        "fillAlphas": 1,
                        "fillColors": "#03A64A",
                        "lineColor": "#03A64A",
    //                                "fillColors": "#62cf73",
    //                                "lineColor": "#62cf73",
                        "labelText": "[[value]]", 
                        "lineAlpha": 1, 
                        "id":"mProd", 
                        "title": "OEE", 
                        "type": "column", 
                        "color": "#FFFF", 
                        "valueField": "prod",  
                        "balloon": { 
                            "enabled": false 
                        }, 
                    }, { 
                        "fillAlphas": 1, 
                        "fillColors": "#0477BF", 
                        "lineColor": "#0477BF", 
    //                                "fillColors": "#311B92",
    //                                "lineColor": "#311B92",
                        "labelText": "[[value]]",
                        "lineAlpha": 1,
                        "title": "Técnicas",
                        "type": "column",
                        "color": "#FFFF",
                        "valueField": "tec", 
                        "balloon": {
                            "enabled": false
                        },
                    }, {
                        "fillAlphas": 1,
                        "fillColors": "#F06292",
                        "lineColor": "#F06292C",
                        "labelText": "[[value]]",
                        "lineAlpha": 1,
                        "title": "Organizacionales",
                        "type": "column",
                        "color": "#FFFF",
                        "valueField": "org", 
                        "balloon": {
                            "enabled": false
                        },
                    }, { 
                        "fillAlphas": 1,
                        "fillColors": "#F20505",
                        "lineColor": "#F20505",
                        "labelText": "[[value]]",
                        "lineAlpha": 1,
                        "title": "Calidad",
                        "type": "column",
                        "color": "#FFFF",
                        "valueField": "calidad", 
                        "balloon": {
                            "enabled": false
                        },
                    }, { 
                        "fillAlphas": 1,
                        "fillColors": "#000000",
                        "lineColor": "#000000",
    //                                "fillColors": "#3498db",
    //                                "lineColor": "#3498db",
                        "labelText": "[[value]]",
                        "lineAlpha": 1,
                        "title": "Cambios",
                        "type": "column",
                        "color": "#FFFFFF",
                        "valueField": "cambio", 
                        "balloon": {
                            "enabled": false
                        },
                    }, { 
                        "fillAlphas": 1,
                        "fillColors": "#9E9E9E", 
                        "lineColor": "#9E9E9E", 
                        "labelText": "[[value]]", 
                        "lineAlpha": 1,
                        "title": "Desempeño", 
                        "type": "column", 
                        "color": "#FFFFFF",
                        "valueField": "desempenio",
                        "balloon": {
                            "enabled": false
                        },
                    }, { 
                        "id": "graph2",                                 
                        "lineThickness": 2,                 
                        "lineColor": "#F25C5C",
                        "fillAlphas": 0, 
                        "lineAlpha": 2, 
                        "title": "Meta", 
                        "valueField": "Meta", 
                        "dashLengthField": "dashLengthLine",
                        "balloonText": "<b>[[category]]</b><br>Meta: </b><span style='font-size:100%'><b>[[value]]%</b></span><br>OEE: </b><span style='font-size:100%'><b>[[prod]]%</b></span><br>Técnicas: </b><span style='font-size:100%'><b>[[tec]]%</b></span><br>Organizacional: </b><span style='font-size:100%'><b>[[org]]%</b></span><br>Calidad: </b><span style='font-size:100%'><b>[[calidad]]%</b></span><br>Cambio: </b><span style='font-size:100%'><b>[[cambio]]%</b></span><br>Desempenio: </b><span style='font-size:100%'><b>[[desempenio]]%</b></span>"
                    }], 
                "chartCursor": { 
                    "pan": true, 
                    "valueLineEnabled": true, 
                    "cursorAlpha": 0, 
                    "valueLineAlpha": 0.2 
                }, 
                "categoryField": "date" ,
                "categoryAxis": { 
                    "dashLength": 1, 
                    "minorGridEnabled": true 
                }
            });
        </script>
    </div>
</div>
