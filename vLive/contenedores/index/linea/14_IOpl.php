<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../../db/ServerFunctions.php';
    session_start();
    date_default_timezone_set("America/Mexico_City");     
    
    //RECIBE LOS VALORES DEL AJAX 
    $linea = $_SESSION['linea'];
    $anio = $_SESSION['anio'];
    $mes = $_SESSION['mes'];
    if ($mes < 10  && strlen($mes) < 2 ) {
        $mes = (string) '0'.$mes; 
    } 
   
    $fIni = $anio.'-'.$mes.'-01'; 
    $fFin = date("Y-m-d");
    
    if (isset($_SESSION["filtro"])) {
        $filtroPuntos = $_SESSION["filtro"];
    } else {
        $filtroPuntos = 0;
    } 
    
    if (isset($_SESSION['privilegio'])){ 
        $privilegio = $_SESSION['privilegio']; 
    } else { 
        $privilegio = 0;
    } 
    
    $fActual = date("Y-m-d"); 
    
    //FECHA
    $now = date("Y-m-d"); 
    $actual = date("m/d/Y"); 
    $month = date("m"); 
    $year = date("y"); 
    $fecha = date("Y-m-d",strtotime($now."- 15 days")); 

    //CONSULTA DE LINEAS
    $cLineas = listarLineas(); 

    $dia = date("Y-m-d"); 
    switch ($filtroPuntos) { 
        case 0: //TODOS
            $cOplLinea = cOplLinea(date("Y-m-d", strtotime($fIni)), date("Y-m-d", strtotime($fFin)), $linea); 
            break;
        case 1: // ABIERTOS
            $cOplLinea = cOPLPFuturosL($linea, date("Y-m-d", strtotime($fIni)), date("Y-m-d", strtotime($fFin)), $dia); 
            break;
        case 2: // CERRADOS EN TIEMPO
            $cOplLinea = cOPLCerradosATiempoL($linea, date("Y-m-d", strtotime($fIni)), date("Y-m-d", strtotime($fFin))); 
            break;
        case 3: // CERRADOS FUERA DE TIEMPO
            $cOplLinea = cOPLCerradosFTiempoL($linea, date("Y-m-d", strtotime($fIni)), date("Y-m-d", strtotime($fFin))); 
            break;
        case 4: // SIN CERRAR
            $cOplLinea = cOPLSinCerrarL($linea, date("Y-m-d", strtotime($fIni)), date("Y-m-d", strtotime($fFin)), $dia); 
            break;  
        default :
            $cOplLinea = cOplLinea(date("Y-m-d", strtotime($fIni)), date("Y-m-d", strtotime($fFin)), $linea); 
            break;
    } 
     
?>    

<div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" style="font-size: 12px;" > 
    <table style="width: 100%; font-size: 10px;" > 
        <thead style="background-color: #eaeded;" > 
            <tr> 
                <th class="contenidoCentrado" >NO.</th> 
                <th >FECHA</th> 
                <th >DETECTO</th> 
                <th >ESTACION</th> 
                <th >DESVIACIO </th> 
                <th >ACCION </th> 
                <th >RESPONSABLE</th> 
                <th >F.COMPROMISO</th> 
                <th >ESTADO</th> 
            </tr> 
        </thead> 
        <tbody > 
        <?php for ($i = 0; $i < count($cOplLinea); $i++) { ?>
            <tr style="background: 
                <?php 
                    if ($cOplLinea[$i][9] == 4) { 
                        if ($cOplLinea[$i][11]->format('Y-m-d') <= $cOplLinea[$i][8]->format('Y-m-d') ) { //CERRADO EN TIEMPO 
                            echo '#b4fac0'; 
                        } else { //if ($cOplLinea[$i][11]->format('Y-m-d') > $cOplLinea[$i][8]->format('Y-m-d') ){ //CERRADOS FUERA DE TIEMPO
                            echo '#f7b076'; 
                        } 
                    } else { 
                        if ($cOplLinea[$i][8]->format('Y-m-d') >= $dia ) { //EN ESPERA 
                            echo '#faf3b4'; 
                        } else { //if ($cOplLinea[$i][8]->format('Y-m-d') < $dia ) { //SIN CERRAR 
                            echo '#d44949'; 
                        } 
                    } 
                ?>" > 
                <td > <?php echo $i+1 ?> </td> 
                <td > <?php echo $cOplLinea[$i][1]->format('d-M'); ?> </td> 
                <td > <?php echo $cOplLinea[$i][2]; ?> </td> 
                <td > <?php echo $cOplLinea[$i][3]; ?> </td> 
                <td > <?php echo $cOplLinea[$i][5]; ?> </td> 
                <td > <?php echo $cOplLinea[$i][6]; ?> </td> 
                <td > <?php echo $cOplLinea[$i][7]; ?> </td> 
                <td > <?php echo $cOplLinea[$i][8]->format('d-M'); ?> </td> 
                <td > 
                    <?php                                     
                        switch ($cOplLinea[$i][9]){
                            case '0';
                            ?>
                                <img src="./imagenes/opl/opl0.png" style="width: 3vh">
                            <?php
                                break;
                            case '1';
                            ?>
                                <img src="./imagenes/opl/opl1.png" style="width: 3vh" >
                            <?php 
                                break; 
                            case '2'; 
                            ?> 
                                <img src="./imagenes/opl/opl2.png" style="width: 3vh" >
                            <?php 
                                break; 
                            case '3'; 
                            ?>
                                <img src="./imagenes/opl/opl3.png" style="width: 3vh" > 
                            <?php 
                                break; 
                            case '4'; 
                            ?>
                                <img src="./imagenes/opl/opl4.png" style="width: 3vh" > 
                            <?php
                                break; 
                            default;
                            ?>
                                <img src="./imagenes/opl/opl0.png" style="width: 3vh" >
                            <?php
                                break;
                        } 
                    ?>
                </td> 
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>