<HTML>
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">-->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <?php 
        include '../../../db/ServerFunctions.php';
        session_start(); 
        //RECIBE LOS VALORES DEL AJAX     
        $line = $_SESSION['linea'];
        $anio = $_SESSION['anio'];
        $mes = $_SESSION['mes'];        
                
        //INCIALIZAMOS LAS VARIABLES 
        //TABLA 
        $countProblem = 0;
        $problema[0] = "";
        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0;
            $valuePlanDayT[$vDate] = 0;
            $cumRealDay[$vDate] = 0;
            $cumMetaDay[$vDate] = 0;
            $targetDayT[$vDate] = 80; 
        }         
        
        $jTDay = jidokaEntregasDia($line, $anio);                
        
        //DIA
        $aux = 1;
        $auxCumReal = 0;
        $auxCumMeta = 0;
        
        #REALES REPORTADAS
        for ($i = 0; $i < count($jTDay); $i++ ) { 
            $date = explode("-", $jTDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $valueDayT[$vDate] = $jTDay[$i][1]; 
            //$valuePlanDayT[$vDate] = (int) $jTDay[$i][1]+500; 
        }          
        
        #TARGET 
        $cTargetDay = cTargetPzasDaily($line, $_SESSION['fIni'], $_SESSION['fFin']);
        for ($i = 0; $i < count($cTargetDay); $i++){
            $date = explode("-", $cTargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $valuePlanDayT[$vDate] = (int) $cTargetDay[$i][1]; 
        } 
        
        #HACER EL ACUMULADO TANTO DE REALES COMO DE PLANEADAS 
        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            
            #APARTADO PARA ACUMULADO REAL
            if ($date[2] != $aux ){ 
                #SE HACE EL ACUMULADO DE PIEZAS 
                $cumRealDay[$vDate] = $auxCumReal + $valueDayT[$vDate]; 
                $cumMetaDay[$vDate] = $auxCumMeta + $valuePlanDayT[$vDate]; 
                
                $auxCumReal += $valueDayT[$vDate]; 
                $auxCumMeta += $valuePlanDayT[$vDate];                
            } else {                 
                #SE REINICIA EL CONTEO, YA QUE ES POR MES
                $cumRealDay[$vDate] = $valueDayT[$vDate]; 
                $cumMetaDay[$vDate] = $valuePlanDayT[$vDate]; 
                
                $auxCumReal = $valueDayT[$vDate];
                $auxCumMeta = $valuePlanDayT[$vDate]; 
            } 
        }  
        
    ?>
    
    <body>
        <div > 
            <style> 
                #grafEntregas { 
                    width: 100%; 
                    min-height: 200px; 
                    max-height: 600px;
                    margin-top: -12px; 
                } 
            </style> 
            
            <div id="grafEntregas" class="jidokaDay" > 
                <script> 
                    var chart = AmCharts.makeChart("grafEntregas", { 
                        "type": "serial",
                        "theme": "light",
                        "dataDateFormat": "YYYY-MM-DD",
                        "precision": 0, 
                        "valueAxes": [{ 
                            "id": "v1", 
                            "title": "Reales", 
                            "position": "left", 
                            "autoGridCount": false 
                        }, {
                            "id": "v2", 
                            "title": "Meta", 
                            "gridAlpha": 0, 
                            "position": "right", 
                            "autoGridCount": false,
                            "minimum": 0
                        }],
                        "graphs": [{ 
                            "valueAxis": "v1",
                            "lineColor": "#e1ede9",
                            "fillColors": "#e1ede9",
                            "fillAlphas": 1,
                            "type": "column",
                            "title": "Planeadas",
                            "valueField": "dPlan",
                            "clustered": false,
                            "columnWidth": 0.5,
                            "balloonText": "<b style='font-size: 100%'>[[category]]</b><br>Planeadas: <b style='font-size: 100%'>[[dPlan]]</b> <br>Reales: <b style='font-size: 100%'>[[dReales]]</b> <br>Diferencia: <b style='font-size: 100%'>[[difD]]</b><br><br>Cumm Planeado: <b style='font-size: 100%'>[[cumPlan]]</b><br>Cumm Real: <b style='font-size: 100%'>[[cumReal]]</b> <br>Diferencia Cumm: <b style='font-size: 100%'>[[difCumm]]</b>  "
                        }, { 
                            "valueAxis": "v1", 
                            "lineColor": "#02538b", 
                            "fillColors": "#02538b", 
                            "fillAlphas": 1, 
                            "type": "column", 
                            "title": "Reales", 
                            "valueField": "dReales", 
                            "clustered": false, 
                            "columnWidth": 0.3, 
                            "balloon": {
                                "enabled": false
                            } 
                        }, { 
                            "valueAxis": "v2", 
                            "lineColor": "#02538b", 
                            "fillColors": "#02538b", 
                            "fillAlphas": 1, 
                            "type": "none", 
                            "valueField": "difD", 
                            "clustered": false, 
                            "columnWidth": 0.3, 
                            "enabled": false,
                            "balloon": {
                                "enabled": false
                            } 
                        }, { 
                            "valueAxis": "v2",
                            "lineColor": "#02538b", 
                            "fillColors": "#02538b", 
                            "fillAlphas": 1, 
                            "type": "none", 
                            "valueField": "difCumm", 
                            "clustered": false, 
                            "columnWidth": 0.3, 
                            "enabled": false, 
                            "balloon": {
                                "enabled": false
                            } 
                        }, { 
                            "valueAxis": "v2", 
                            "lineThickness": 2, 
                            "lineColor": "#bc2727", 
                            "type": "smoothedLine", 
                            "dashLength": 5,
                            "title": "Cumm Plan", 
                            "useLineColorForBulletBorder": true, 
                            "valueField": "cumPlan", 
                            "balloon": { 
                                "enabled": false 
                            } 
                        },{ 
                            "valueAxis": "v2", 
                            "lineThickness": 2, 
                            "lineColor": "#000000", 
                            "type": "smoothedLine",
                            "dashLength": 5,
                            "title": "Cumm Real",
                            "useLineColorForBulletBorder": true,
                            "valueField": "cumReal",
                            "balloon": {
                                "enabled": false
                            } 
                        }],
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha": 0,
                            "valueLineAlpha": 0.2
                        },
                        "categoryField": "date",
                        "categoryAxis": {
                            "parseDates": true,
                            "dashLength": 1,
                            "minorGridEnabled": true,
                            "stackType": "regular", 
                            "axisAlpha": 0.3, 
                            "gridAlpha": 0.2 
                        }, 
                        "dataProvider": [
                        <?php  
                            for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                $date = explode("-", $i);                                 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                        ?> {
                            "date": "<?php echo "$fecha[$vDate]" ?>",
                            "cumReal": <?php echo "$cumRealDay[$vDate]" ?>,
                            "cumPlan": <?php echo "$cumMetaDay[$vDate]" ?>,
                            "dReales": <?php echo $valueDayT[$vDate]; ?>,
                            "dPlan": <?php echo $valuePlanDayT[$vDate]; ?>, 
                            "difCumm": <?php echo $cumMetaDay[$vDate] - $cumRealDay[$vDate]; ?>, 
                            "difD": <?php echo $valuePlanDayT[$vDate]-$valueDayT[$vDate]; ?>
                        }, 
                        <?php } ?>        
                        ]
                    });
                </script>
            </div>
 