<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    date_default_timezone_set("America/Mexico_City");     
    
    $mes = $_SESSION["mes"];
    $anio = $_SESSION["anio"]; 
    $vYM = $anio.'-'.$mes;
    
    $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));  
    
    $vYM = $anio.'-'.$mes;
    
    if (date("Y-m")!= $vYM ){
        $vMDay = $ultimoDiaMes;
    } else {
        $vMDay = date("d");
    }
    
    if (isset($_SESSION['privilegio'])){ 
        $privilegio = $_SESSION['privilegio']; 
    } else { 
        $privilegio = 0; 
    } 
    
    //FUNCIONES   
    include '../../../db/ServerFunctions.php'; 

    //CONSULTA DE OPL LINEA PENDIENTES        
    for ($i = 1; $i <= $ultimoDiaMes+1; $i++){
        $vCampD[$i] = 0; 
        $vKmD[$i] = 0;
    }         

    //CONSULTA DE OPL LINEA PENDIENTES(MES)    
    for ($i = 1; $i <= 13; $i++) { 
        $vCampM[$i] = 0; 
        $vKmM[$i] = 0; 
    } 

    #CONSULTA DE CALIDAD INTERNA
    $aCD = 1; //CONTADOR PARA VCAMP
    $aFCD = 1; //CONTADOR PARA DIAS DE FALLA

    $cIMonth = cCalidadInternaMes($_SESSION["linea"], $_SESSION['fIni'], $_SESSION['fFin']); 
    $cKmMonth = cCalidadKmMes($_SESSION["linea"], $_SESSION['fIni'], $_SESSION['fFin']);
    $cIYear = cCalidadInternaAnio($_SESSION["linea"], $anio); 
    $cKmYear = cCalidadKmAnio($_SESSION["linea"], $anio);
    
    $cInternosY1 = 0;
    $cInternosY2 = 0; 
    $cKmY1 = 0; 
    $cKmY2 = 0;       
    $cInternosM1 = 1;
    $cInternosM2 = 1; 
    $cKmM1 = 1; 
    $cKmM2 = 1;       

    /************ AÑO / YEAR ***************/
    #CONSULTA DE CALIDAD INTERNA 
    if (count($cIYear) > 0){             
        $cInternosY2 = count($cIYear);
        for ($i = 0; $i < count($cIYear); $i++){  
            $v = $cIYear[$i][0]; 
            $v2 = $cIYear[$i][0]+1; 

            if ($cIYear[$i][0] < 10 ){
                $v = '0'.$v; 
            } 

            if ($cIYear[$i][0] < 9){
                $v2 = '0'.$v2; 
            }
            $vDI_IY2[$i] = date("Y-m-d-", strtotime($anio.'-'.$v.'-01')); 
            $vDF_IY2[$i] = $anio.'-'.$v2.'-01'; 

            if ($i == 0) {
                if (count($cKmYear) > 1 ){
                    for ($j = 1; $j < 13; $j ++){ 
                        if ( $j < $cIYear[$i][0] &&  $cIYear[$i][0] != $j ){ 
                            $a = $j;
                            $a2 = $j+1;
                            if ($j < 10 ){ 
                                $a = '0'.$a; 
                            } 
                            if ($j < 9 ){ 
                                $a2 = '0'.$a2; 
                            } 
                            $vDI_IY1[$cInternosY1] = $anio.'-'.$a.'-01'; 
                            $vDF_IY1[$cInternosY1] = $anio.'-'.$a2.'-01'; 
                            //echo "<br>",$vDI_IY1[$cInternosY1]+", "+$vDF_IY1[$cInternosY1];
                            $cInternosY1++;
                        }
                    }
                } else {
                    for ($j = 1; $j < 13; $j ++){ 
                        if ( $j <(int) $mes &&  $cIYear[$i][0] != $j ){ 
                            $a = $j;
                            $a2 = $j+1;
                            if ($j < 10 ){ 
                                $a = '0'.$a; 
                            } 
                            if ($j < 9 ){ 
                                $a2 = '0'.$a2; 
                            } 
                            $vDI_IY1[$cInternosY1] = $anio.'-'.$a.'-01'; 
                            $vDF_IY1[$cInternosY1] = $anio.'-'.$a2.'-01'; 
                            //echo "<br>",$vDI_IY1[$cInternosY1]+", "+$vDF_IY1[$cInternosY1];
                            $cInternosY1++;
                        }
                    }
                }                     
            } else { 
                for ($j = 1; $j < 13; $j ++){ 
                    if ($cIYear[$i][0] != $j && $j > $cIYear[$i-1][0] && $j <= (int) $mes ){ 
                        $a = $j;
                        $a2 = $j+1;
                        if ($j < 10 ){
                            $a = '0'.$j;
                        } 
                        if ($j < 9 ){ 
                            $a2 = '0'.($a2); 
                        } 
                        $vDI_IY1[$cInternosY1] = $anio.'-'.$a.'-01'; 
                        $vDF_IY1[$cInternosY1] = $anio.'-'.$a2.'-01'; 
                        //echo "<br>",$vDI_IY1[$cInternosY1]+", "+$vDF_IY1[$cInternosY1];
                        $cInternosY1++;
                    }
                }
            } 
        } 
    } else {             
        $cInternosY2 = 0; 
        $vDI_IY1[$cInternosY1] = date($anio."-01-01"); 
        if (date("Y-m") != $vYM){
            $vDF_IY1[$cInternosY1] = date($anio."-12-31"); 
        } else { 
            $vDF_IY1[$cInternosY1] = date("Y-m-d"); 
        } 
        $cInternosY1 = 1;
    }

    #CONSULTA DE CALIDAD INTERNA 
    if (count($cKmYear) > 0) {             
        $cKmY2 = count($cKmYear);
        for ($i = 0; $i < count($cKmYear); $i++){                 
            $v = $cKmYear[$i][0]; 
            $v2 = $cKmYear[$i][0]+1; 

            if ($cKmYear[$i][0] < 10 ){
                $v = '0'.$v; 
            } 

            if ($cKmYear[$i][0] < 9){
                $v2 = '0'.$v2; 
            }
            $vDI_KY2[$i] = date("Y-m-d-", strtotime($anio.'-'.$v.'-01')); 
            $vDF_KY2[$i] = $anio.'-'.$v2.'-01'; 

            if ($i == 0) { 
                if (count($cKmYear) > 1 ){
                    for ($j = 1; $j < 13; $j++){ 
                        if ( $j <= $cKmYear[$i][0] && $cKmYear[$i][0] != $j ){ 
                            $a = $j;
                            $a2 = $j+1;
                            if ($j < 10 ){
                                $a = '0'.$a;
                            } 
                            if ($j < 9 ){ 
                                $a2 = '0'.$a2;
                            } 
                            $vDI_KY1[$cKmY1] = $anio.'-'.$a.'-01'; 
                            $vDF_KY1[$cKmY1] = $anio.'-'.$a2.'-01'; 
                            $cKmY1++; 
                        } 
                    } 
                } else { 
                    for ($j = 1; $j < 13; $j++){ 
                        if ( $j <= (int) $mes && $cKmYear[$i][0] != $j ){ 
                            $a = $j; 
                            $a2 = $j+1; 
                            if ($j < 10 ){ 
                                $a = '0'.$a; 
                            } 
                            if ($j < 9 ){ 
                                $a2 = '0'.$a2;
                            } 
                            $vDI_KY1[$cKmY1] = $anio.'-'.$a.'-01'; 
                            $vDF_KY1[$cKmY1] = $anio.'-'.$a2.'-01'; 
                            $cKmY1++;
                        }
                    } 
                }
            } else { 
                for ($j = 1; $j < 13; $j++){ 
                    if ($cKmYear[$i][0] != $j && $j > $cKmYear[$i-1][0] && $j <= (int) $mes ){ 
                        $a = $j;
                        $a2 = $j+1;
                        if ($j < 10 ){ 
                            $a = '0'.$j; 
                        } 
                        if ($j < 9 ){ 
                            $a2 = '0'.$a2; 
                        } 
                        $vDI_KY1[$cKmY1] = $anio.'-'.$a.'-01'; 
                        $vDF_KY1[$cKmY1] = $anio.'-'.$a2.'-01';
                        $cKmY1++;
                    }
                }
            } 
        }             

    } else {             
        $cInternosY2 = 0; 
        $vDI_KY1[$cKmY1] = date($anio."-01-01"); 
        if (date("Y-m") != $vYM){ 
             $vDF_KY1[$cKmY1] = date($anio."-12-31"); 
        } else {
             $vDF_KY1[$cKmY1] = date("Y-m-d");
        } 
        $cKmY1 = 1; 
    } 

    /************ MES / MONTH ***************/
    #CONSULTA DE CALIDAD INTERNA 
    if (count($cIMonth) > 0){  
        $cInternosM2 = count($cIMonth)+1;
        for ($i = 0; $i < count($cIMonth); $i++){              
            $v = $cIMonth[$i][1]; 
            
            if ($cIMonth[$i][1] < 10 ){ 
                $v = '0'.$v; 
            } 
            
            $vDI_IM2[$i+1] = date("Y-m-d", strtotime($anio.'-'.$mes.'-'.$v)); 
            $vDF_IM2[$i+1] = $anio.'-'.$mes.'-'.$v;  
            if ($i == 0) { 
                if (count($cKmMonth) > 1 ){ 
                    for ($j = 1; $j < $vMDay; $j++ ){ 
                        if ( $j < $cIMonth[$i][1] && $cIMonth[$i][1] != $j ){ 
                            $a = $j; 
                            if ($j < 10 ){ 
                                $a = '0'.$a; 
                            } 
                            $vDI_IM1[$cInternosM1] = $anio.'-'.$mes.'-'.$a; 
                            $vDF_IM1[$cInternosM1] = $anio.'-'.$mes.'-'.$a; 
                            $cInternosM1++; 
                        } 
                    } 
                } else { 
                    for ($j = 1; $j <= $vMDay; $j++ ){ 
                        if ( $cIMonth[$i][1] != $j ){ 
                            $a = $j; 
                            if ($j < 10 ){ 
                                $a = '0'.$a; 
                            }  
                            $vDI_IM1[$cInternosM1] = $anio.'-'.$mes.'-'.$a; 
                            $vDF_IM1[$cInternosM1] = $anio.'-'.$mes.'-'.$a; 
                            $cInternosM1++; 
                        } 
                    } 
                } 
            } else { 
                for ($j = 1; $j < $vMDay; $j++ ){ 
                    if ($cIMonth[$i][1] != $j && $j > $cIMonth[$i][1]  ){ 
                        $a = $j; 
                        if ($j < 10 ){
                            $a = '0'.$j;
                        }  
                        $vDI_IM1[$cInternosM1] = $anio.'-'.$mes.'-'.$a; 
                        $vDF_IM1[$cInternosM1] = $anio.'-'.$mes.'-'.$a; 
                        $cInternosM1++;
                    }
                }
            } 
        } 
    } else {             
        $cInternosM2 = 0;         
        $vDI_IM1[1] = date($anio.'-'.$mes.'-01');
        $vDF_IM1[1] = date($anio.'-'.$mes.'-'.$vMDay);
        $cInternosM1 = 2;
    } 
    
    #CONSULTA DE CALIDAD INTERNA 
    if (count($cKmMonth) > 0) {             
        $cKmM2 = count($cKmMonth)+1;
        for ($i = 0; $i < count($cKmMonth); $i++){                 
            $v = $cKmMonth[$i][1];  

            if ($cKmMonth[$i][1] < 10 ){
                $v = '0'.$v; 
            } 
 
            $vDI_KM2[$i+1] = date("Y-m-d-", strtotime($anio.'-'.$mes.'-'.$v)); 
            $vDF_KM2[$i+1] = $anio.'-'.$mes.'-'.$v; 

            if ($i == 0) { 
                if (count($cKmMonth) > 1 ){
                    for ($j = 1; $j < $vMDay; $j++){ 
                        if ( $j <= $cKmMonth[$i][1] && $cKmMonth[$i][1] != $j ){ 
                            $a = $j; 
                            if ($j < 10 ){ 
                                $a = '0'.$a; 
                            }  
                            $vDI_KM1[$cKmM1] = $anio.'-'.$mes.'-'.$a; 
                            $vDF_KM1[$cKmM1] = $anio.'-'.$mes.'-'.$a; 
                            $cKmM1++; 
                        }
                    } 
                } else {
                    for ($j = 1; $j <= $vMDay; $j++){ 
                        if ( $cKmMonth[$i][1] != $j ){ 
                            $a = $j; 
                            if ($j < 10 ){
                                $a = '0'.$a;
                            } 
                            $vDI_KM1[$cKmM1] = $anio.'-'.$mes.'-'.$a; 
                            $vDF_KM1[$cKmM1] = $anio.'-'.$mes.'-'.$a; 
                            $cKmM1++;
                        }
                    } 
                }
            } else { 
                for ($j = 1; $j < $vMDay; $j++){ 
                    if ($cKmMonth[$i][1] != $j && $j > $cKmMonth[$i][1] ){ 
                        $a = $j;
                        $a2 = $j+1;
                        if ($j < 10 ){ 
                            $a = '0'.$j; 
                        } 
                        $vDI_KM1[$cKmM1] = $anio.'-'.$mes.'-'.$a; 
                        $vDF_KM1[$cKmM1] = $anio.'-'.$mes.'-'.$a;
                        $cKmM1++;
                    }
                }
            } 
        }             

    } else {             
        $cKmM2 = 0; 
        $vDI_KM1[1] = date($anio.'-'.$mes.'-01'); 
        $vDF_KM1[1] = date($anio.'-'.$mes.'-'.$vMDay); 
        $cKmM1 = 2; 
    }  
    
?>

<head>   
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
    
    <style> 
        #gMonth {
            width: 150px;
            height: 150px;
        }
        #gYear {
            width: 150px;
            height: 150px;
        }
    </style>
</head> 

<body> 
    <div style="display:inline; " >
        <div id="gMonth" style="float:left;" > 
            <script> 
                am4core.useTheme(am4themes_animated); 
                var chart = am4core.create("gMonth", am4charts.RadarChart); 

                chart.data = [ 
                    <?php for ($j = 1; $j < $cInternosM1; $j++){ ?>
                    { 
                        category: "Campo", 
                        startDate1: "<?php echo $vDI_IM1[$j]; ?>", 
                        endDate1: "<?php echo $vDF_IM1[$j]; ?>" 
                    }, 
                    <?php }  for ($j = 1; $j < $cInternosM2; $j++){ ?> 
                    { 
                        category: "Campo", 
                        startDate2: "<?php echo $vDI_IM2[$j]; ?>", 
                        endDate2: "<?php echo $vDF_IM2[$j]; ?>" 
                    }, 
                    <?php } for ($j = 1; $j < $cKmM1; $j++){ ?> 
                    { 
                        category: "0 km", 
                        startDate1: "<?php echo $vDI_KM1[$j]; ?>", 
                        endDate1: "<?php echo $vDF_KM1[$j]; ?>" 
                    }, 
                    <?php }  for ($j = 1; $j < $cKmM2; $j++){ ?> 
                    { 
                        category: "0 km", 
                        startDate2: "<?php echo $vDI_KM2[$j]; ?>", 
                        endDate2: "<?php echo $vDF_KM2[$j]; ?>" 
                    }, 
                    <?php } ?>  
                ]; 

                chart.padding(20, 20, 20, 20);
                chart.colors.step = 2;
                chart.dateFormatter.inputDateFormat = "YYYY-MM-dd";
                chart.innerRadius = am4core.percent(35);

                var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.labels.template.location = 0.5;
                categoryAxis.renderer.labels.template.horizontalCenter = "right";
                categoryAxis.renderer.grid.template.location = 0;
                categoryAxis.renderer.tooltipLocation = 0.5;
                categoryAxis.renderer.grid.template.strokeOpacity = 0.2;
                categoryAxis.renderer.minGridDistance = 10;
                categoryAxis.mouseEnabled = false; 
                categoryAxis.tooltip.disabled = true;

                var dateAxis = chart.xAxes.push(new am4charts.DateAxis()); 
                dateAxis.renderer.labels.template.horizontalCenter = "left"; 
                dateAxis.strictMinMax = true; 
                dateAxis.renderer.maxLabelPosition = 0.99; 
                dateAxis.renderer.grid.template.strokeOpacity = 0.3; 
                dateAxis.min = new Date(<?php echo $anio; ?>, <?php echo $mes-1; ?>, 1, 0, 0, 0).getTime(); 
                dateAxis.max = new Date(<?php echo $anio; ?>, <?php echo $mes; ?>, 1, 0, 0, 0).getTime(); 
                dateAxis.mouseEnabled = false; 
                dateAxis.tooltip.disabled = true; 
                dateAxis.baseInterval = {count:1, timeUnit:"day"}; 

                var series1 = chart.series.push(new am4charts.RadarColumnSeries());
                series1.name = "Series 1";
                series1.dataFields.openDateX = "startDate1";
                series1.dataFields.dateX = "endDate1";
                series1.dataFields.categoryY = "category";
                series1.fill = am4core.color("#23B14D");
                series1.clustered = false;
                series1.columns.template.radarColumn.cornerRadius = 100;
                series1.columns.template.tooltipText = "{category}: \n\ {openDateX} a {dateX}";

                var series2 = chart.series.push(new am4charts.RadarColumnSeries());
                series2.name = "Series 2";
                series2.dataFields.openDateX = "startDate2";
                series2.dataFields.dateX = "endDate2";
                series2.dataFields.categoryY = "category"; 
                series2.fill = am4core.color("#ff0f00");
                series2.clustered = false;
                series2.columns.template.radarColumn.cornerRadius = 100;
                series2.columns.template.tooltipText = "{category}: \n\ {openDateX} a {dateX}";

                chart.seriesContainer.zIndex = -1;

                chart.cursor = new am4charts.RadarCursor();
                chart.cursor.innerRadius = am4core.percent(5);
                chart.cursor.lineY.disabled = true;

                var yearLabel = chart.radarContainer.createChild(am4core.Label);
                yearLabel.text = "DIAS SIN FALLAS"; 
                yearLabel.fontSize = 10; 
                yearLabel.horizontalCenter = "middle"; 
                yearLabel.verticalCenter = "middle"; 
            </script>
        </div> 
        
        <div id="gYear" style="float:left;" > 
            <script> 
                am4core.useTheme(am4themes_animated); 
                var chart = am4core.create("gYear", am4charts.RadarChart); 

                chart.data = [ 
                    <?php for ($j = 0; $j < $cInternosY1; $j++){ ?>
                    { 
                        category: "Campo", 
                        startDate1: "<?php echo $vDI_IY1[$j]; ?>", 
                        endDate1: "<?php echo $vDF_IY1[$j]; ?>" 
                    }, 
                    <?php }  for ($j = 0; $j < $cInternosY2; $j++){ ?> 
                    { 
                        category: "Campo", 
                        startDate2: "<?php echo $vDI_IY2[$j]; ?>", 
                        endDate2: "<?php echo $vDF_IY2[$j]; ?>" 
                    }, 
                    <?php } for ($j = 0; $j < $cKmY1; $j++){ ?> 
                    { 
                        category: "0 km", 
                        startDate1: "<?php echo $vDI_KY1[$j]; ?>", 
                        endDate1: "<?php echo $vDF_KY1[$j]; ?>" 
                    }, 
                    <?php }  for ($j = 0; $j < $cKmY2; $j++){ ?> 
                    { 
                        category: "0 km", 
                        startDate2: "<?php echo $vDI_KY2[$j]; ?>", 
                        endDate2: "<?php echo $vDF_KY2[$j]; ?>" 
                    }, 
                    <?php } ?>  
                ]; 

                chart.padding(20, 20, 20, 20);
                chart.colors.step = 2;
                chart.dateFormatter.inputDateFormat = "YYYY-MM-dd";
                chart.innerRadius = am4core.percent(35);

                var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
                categoryAxis.dataFields.category = "category";
                categoryAxis.renderer.labels.template.location = 0.5;
                categoryAxis.renderer.labels.template.horizontalCenter = "right";
                categoryAxis.renderer.grid.template.location = 0;
                categoryAxis.renderer.tooltipLocation = 0.5;
                categoryAxis.renderer.grid.template.strokeOpacity = 0.2;
                categoryAxis.renderer.minGridDistance = 10;
                categoryAxis.mouseEnabled = false; 
                categoryAxis.tooltip.disabled = true;

                var dateAxis = chart.xAxes.push(new am4charts.DateAxis()); 
                dateAxis.renderer.labels.template.horizontalCenter = "left"; 
                dateAxis.strictMinMax = true; 
                dateAxis.renderer.maxLabelPosition = 0.99; 
                dateAxis.renderer.grid.template.strokeOpacity = 0.3; 
                dateAxis.min = new Date(<?php echo $anio; ?>, 0, 1, 0, 0, 0).getTime(); 
                dateAxis.max = new Date(<?php echo $anio+1; ?>, 0, 1, 0, 0, 0).getTime(); 
                dateAxis.mouseEnabled = false; 
                dateAxis.tooltip.disabled = true; 
                dateAxis.baseInterval = {count:1, timeUnit:"day"}; 

                var series1 = chart.series.push(new am4charts.RadarColumnSeries());
                series1.name = "Series 1";
                series1.dataFields.openDateX = "startDate1";
                series1.dataFields.dateX = "endDate1";
                series1.dataFields.categoryY = "category";
                series1.fill = am4core.color("#23B14D");
                series1.clustered = false;
                series1.columns.template.radarColumn.cornerRadius = 10;
                series1.columns.template.tooltipText = "{category}: \n\ {openDateX}";

                var series2 = chart.series.push(new am4charts.RadarColumnSeries());
                series2.name = "Series 2";
                series2.dataFields.openDateX = "startDate2";
                series2.dataFields.dateX = "endDate2";
                series2.dataFields.categoryY = "category"; 
                series2.fill = am4core.color("#ff0f00");
                series2.clustered = false;
                series2.columns.template.radarColumn.cornerRadius = 10;
                series2.columns.template.tooltipText = "{category}: \n\ {openDateX}";

                chart.seriesContainer.zIndex = -1;

                chart.cursor = new am4charts.RadarCursor();
                chart.cursor.innerRadius = am4core.percent(5);
                chart.cursor.lineY.disabled = true;

                var yearLabel = chart.radarContainer.createChild(am4core.Label);
                yearLabel.text = "<?php echo $anio; ?>"; 
                yearLabel.fontSize = 10; 
                yearLabel.horizontalCenter = "middle"; 
                yearLabel.verticalCenter = "middle"; 
            </script>
        </div> 
    </div> 
</body>
