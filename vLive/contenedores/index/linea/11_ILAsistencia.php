<?PHP 
    include '../../../db/ServerFunctions.php'; 
    session_start(); 
    
    $linea = $_SESSION['linea']; 
    $dia = date("Y-m-d"); 
    
    //CONSULTA A BASE DE DATOS 
    $cLista = cAsistenciaIndexLinea($linea, $dia); 
    
?>

<div style="width: 100%; font-size: 10px; " >
    <table style="width: 100%;"  > 
        <thead style="background-color: #eaeded;" > 
            <tr> 
                <th >No.</th> 
                <th >Puesto</th> 
                <th >Nombre</th> 
                <th >Asistencia </th> 
            </tr> 
        </thead> 
        <tbody > 
            <?php for($i = 0; $i < count($cLista); $i++ ){ ?>
            <tr >
                <td > <?php echo $i+1; ?> </td> 
                <td > <?php echo $cLista[$i][0]?> </td>  
                <td > <?php echo $cLista[$i][1]; ?> </td> 
                <td > <?php echo $cLista[$i][2]; ?> </td> 
            </tr> 
            <?php } ?> 
        </tbody>
    </table>
</div>