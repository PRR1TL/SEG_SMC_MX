<head>
    <style> 
        .no { 
            display:none; 
        } 
        .si { 
            display:block; 
        } 
    </style>
</head> 

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../../db/ServerFunctions.php';
    session_start();
    date_default_timezone_set("America/Mexico_City"); 
    
    $linea = $_SESSION['linea'];    
    $anio = $_SESSION['anio'];
    $mes = $_SESSION['mes']; 
    $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio)); 
    
    if (date("Y-m") == date("Y-m", strtotime($_SESSION['fIni'])) ){ 
        $dMax = date("d"); 
    } else { 
        $dMax = $ultimoDiaMes+1;  
    } 
    
    //CONTADORES 
    $sAInseguroD = 0;
    $sCInseguraD = 0;
    $sIncidenteD = 0;
    $sPAuxilioD = 0;
    $sIncapacidadD = 0;
    $sFatalD = 0;
    $sAcumuladoD = 0;
    $sRecordD = 0;
    $sAInseguroR = 0;
    $sCInseguraR = 0;
    $sIncidenteR = 0;
    $sPAuxilioR = 0;
    $sIncapacidadR = 0;
    $sFatalR = 0;
    $sAcumuladoR = 0;
    $sRecordR = 0;
    
    $aContingenciaR = 0;
    $aContingenciaD = 0;
    $aEmergenciaR = 0;
    $aEmergenciaD = 0;
    $aNoConformidadR = 0;
    $aNoConformidadD = 0;
    $aAcumuladoR = 0;
    $aAcumuladoD = 0;
    $aRecordR = 0;
    $aRecordD = 0;
    
    //CONSULTA DE INSIDENTES DE SEGURIDAD
    $cSeguridad = cSeguridad($linea, $_SESSION['fIni'], $_SESSION['fFin']); 
    if (count($cSeguridad) > 0){ 
        for ($i = 1; $i <= $dMax; $i++ ){ 
        ?>
        <script>
            document.getElementById("gS<?php echo $i; ?>").className = "si";
        </script>
        <?php
        } 
        for ($i = 0; $i < count($cSeguridad); $i++){ 
            $vD = $cSeguridad[$i][0];
            if ($cSeguridad[$i][2] == 2){ //LINEAS
                switch ($cSeguridad[$i][1]){ 
                    //AZUL
                    case "ACTO INSEGURO":
                        $sAInseguroD++;
                        ?>
                        <script>
                            document.getElementById("gS<?php echo $vD; ?>").className = "no";
                            document.getElementById("bS<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    case "CONDICION INSEGURA":
                        $sCInseguraD++;                        
                        ?>
                        <script>
                            document.getElementById("gS<?php echo $vD; ?>").className = "no";
                            document.getElementById("bS<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    //AMARILLO
                    case "PRIMER AUXILIO":
                        $sPAuxilioD++;
                        ?>
                        <script>
                            document.getElementById("gS<?php echo $vD; ?>").className = "no";
                            document.getElementById("yS<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    case "INCIDENTE":
                       $sIncidenteD++;
                        ?>
                        <script>
                            document.getElementById("gS<?php echo $vD; ?>").className = "no";
                            document.getElementById("yS<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    //ROJO
                    case "FATAL":
                        $sFatalD++;
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("rA<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                    case "INCAPACITANTE":
                        $sIncapacidadD++;
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("rA<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;                
                } 
            } else { //HSE 
                switch ($cSeguridad[$i][1]){
                    //AZUL
                    case "ACTO INSEGURO":
                        $sAInseguroR++;
                        ?>
                        <script>
                            document.getElementById("gS<?php echo $vD; ?>").className = "no";
                            document.getElementById("bS<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    case "CONDICION INSEGURA":
                        $sCInseguraR++;                        
                        ?>
                        <script>
                            document.getElementById("gS<?php echo $vD; ?>").className = "no";
                            document.getElementById("bS<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    //AMARILLO
                    case "PRIMER AUXILIO":
                        $sPAuxilioR++;
                        ?>
                        <script>
                            document.getElementById("gS<?php echo $vD; ?>").className = "no";
                            document.getElementById("yS<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    case "INCIDENTE":
                       $sIncidenteR++;
                        ?>
                        <script>
                            document.getElementById("gS<?php echo $vD; ?>").className = "no";
                            document.getElementById("yS<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    //ROJO
                    case "FATAL":
                        $sFatalR++;
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("rA<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                    case "INCAPACITANTE":
                        $sIncapacidadR++;
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("rA<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;                
                } 
            }
        } 
    } else { 
        for ($i = 1; $i < $dMax; $i++ ){ 
   ?>
        <script>
            document.getElementById("gS<?php echo $i; ?>").className = "si";
        </script>
    <?php
        } 
    } 
    
    //CONSULTA DE INSIDENTES DE MEDIO AMBIENTE     
    $cAmbiente = cMAmbiente($linea, $_SESSION['fIni'], $_SESSION['fFin']); 
    if (count($cAmbiente) > 0){ 
        for ($i = 1; $i <= $dMax; $i++ ){ 
        ?>
        <script>
            document.getElementById("gA<?php echo $i; ?>").className = "si";
        </script>
        <?php
        } 
        for ($i = 0; $i < count($cAmbiente); $i++){ 
            $vD = $cAmbiente[$i][0];
            if ($cAmbiente[$i][2] == 2){ //LINEAS
                switch ($cAmbiente[$i][1]){
                    case "NO CONFORMIDAD AMBIENTAL":
                        //AZUL
                        $aNoConformidadD++;
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("bA<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    case "EMERGENCIA AMBIENTAL":
                        //AMARILLO
                        $aEmergenciaD++; 
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("yA<?php echo $vD; ?>").className = "si";
                        </script> 
                        <?php
                        break; 
                    case "CONTINGENCIA AMBIENTAL":
                        //ROJO
                        $aContingenciaD++;
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("rA<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;                
                } 
            } else {
                switch ($cAmbiente[$i][1]){
                    case "NO CONFORMIDAD AMBIENTAL":
                        //AZUL
                        $aNoConformidadR++;
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("bA<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    case "EMERGENCIA AMBIENTAL":
                        //AMARILLO
                        $aEmergenciaR++;
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("yA<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;
                    case "CONTINGENCIA AMBIENTAL":
                        //ROJO
                        $aContingenciaR++;
                        ?>
                        <script>
                            document.getElementById("gA<?php echo $vD; ?>").className = "no";
                            document.getElementById("rA<?php echo $vD; ?>").className = "si";
                        </script>
                        <?php
                        break;                
                } 
            }
        } 
    } else { 
        for ($i = 1; $i < $dMax; $i++ ){ 
        ?>
        <script>
            document.getElementById("gA<?php echo $i; ?>").className = "si";
        </script>
        <?php
        } 
    }
    
    if (isset($_SESSION['privilegio'])){ 
        $privilegio = $_SESSION['privilegio']; 
    } else { 
        $privilegio = 0; 
    } 
       
?>

<body>    
    <div style="width: 100%;" > 
        <div style="width: 50%; float: left;" > 
            <img src="./imagenes/seguridad0.png" style="width: 150px; height: 90px; " > 
            <div style="margin-left: 80px; margin-top: -82px; " >
                <div class="row" style="margin-left: 7px; " > 
                    <img class="no" id="gS1" name="gS1" src="./imagenes/seguridad/verde.png" style="width: 15px; height: 14px; padding: 2px; " > 
                    <img class="no" id="yS1" name="yS1" src="./imagenes/seguridad/amarillo.png" style="width: 15px; height: 14px; padding: 2px; " > 
                    <img class="no" id="bS1" name="bS1" src="./imagenes/seguridad/azul.png" style="width: 15px; height: 14px; padding: 2px; " > 
                    <img class="no" id="rS1" name="rS1" src="./imagenes/seguridad/rojo.png" style="width: 15px; height: 14px; padding: 2px; " > 
                    <img class="no" id="gS2" name="gS2" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS2" name="yS2" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS2" name="bS2" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS2" name="rS2" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS3" name="gS3" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS3" name="yS3" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS3" name="bS3" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS3" name="rS3" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                </div> 
                <div class="row" style="margin-left: 7px; margin-top: -3px; " > 
                    <img class="no" id="gS4" name="gS4" src="./imagenes/seguridad/verde.png" style="width: 15px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS4" name="yS4" src="./imagenes/seguridad/amarillo.png" style="width: 15px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS4" name="bS4" src="./imagenes/seguridad/azul.png" style="width: 15px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS4" name="rS4" src="./imagenes/seguridad/rojo.png" style="width: 15px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS5" name="gS5" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS5" name="yS5" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS5" name="bS5" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS5" name="rS5" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS6" name="gS6" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS6" name="yS6" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" >
                    <img class="no" id="bS6" name="bS6" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS6" name="rS6" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                </div> 
                <div class="row" style="margin-top: -2.8px; " > 
                    <img class="no" id="gS7" name="gS7" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS7" name="yS7" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS7" name="bS7" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS7" name="rS7" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS8" name="gS8" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS8" name="yS8" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS8" name="bS8" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS8" name="rS8" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" >
                    <img class="no" id="gS9" name="gS9" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS9" name="yS9" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS9" name="bS9" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS9" name="rS9" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS10" name="gS10" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS10" name="yS10" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS10" name="bS10" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS10" name="rS10" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS11" name="gS11" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS11" name="yS11" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS11" name="bS11" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS11" name="rS11" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS12" name="gS12" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS12" name="yS12" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS12" name="bS12" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS12" name="rS12" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS13" name="gS13" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS13" name="yS13" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS13" name="bS13" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS13" name="rS13" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                </div> 
                <div class="row" style="margin-top: -2.9px; " > 
                    <img class="no" id="gS14" name="gS14" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS14" name="yS14" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS14" name="bS14" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS14" name="rS14" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS15" name="gS15" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS15" name="yS15" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS15" name="bS15" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS15" name="rS15" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS16" name="gS16" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS16" name="yS16" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS16" name="bS16" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS16" name="rS16" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS17" name="gS17" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS17" name="yS17" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS17" name="bS17" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS17" name="rS17" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS18" name="gS18" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS18" name="yS18" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS18" name="bS18" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS18" name="rS18" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS19" name="gS19" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS19" name="yS19" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS19" name="bS19" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS19" name="rS19" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS20" name="gS20" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" >
                    <img class="no" id="yS20" name="yS20" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS20" name="bS20" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS20" name="rS20" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                </div> 
                <div class="row" style="margin-top: -2.5px; " > 
                    <img class="no" id="gS21" name="gS21" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS21" name="yS21" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS21" name="bS21" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS21" name="rS21" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS22" name="gS22" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS22" name="yS22" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS22" name="bS22" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS22" name="rS22" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS23" name="gS23" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS23" name="yS23" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS23" name="bS23" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS23" name="rS23" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS24" name="gS24" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS24" name="yS24" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS24" name="bS24" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS24" name="rS24" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS25" name="gS25" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS25" name="yS25" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS25" name="bS25" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS25" name="rS25" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS26" name="gS26" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS26" name="yS26" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS26" name="bS26" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS26" name="rS26" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS27" name="gS27" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS27" name="yS27" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS27" name="bS27" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS27" name="rS27" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                </div> 
                <div class="row" style="margin-left: 7px; margin-top: -3.3px; " > 
                    <img class="no" id="gS28" name="gS28" src="./imagenes/seguridad/verde.png" style="width: 15px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS28" name="yS28" src="./imagenes/seguridad/amarillo.png" style="width: 15px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS28" name="bS28" src="./imagenes/seguridad/azul.png" style="width: 15px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS28" name="rS28" src="./imagenes/seguridad/rojo.png" style="width: 15px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS29" name="gS29" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS29" name="yS29" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS29" name="bS29" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS29" name="rS29" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="gS30" name="gS30" src="./imagenes/seguridad/verde.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS30" name="yS30" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS30" name="bS30" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS30" name="rS30" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -3px; height: 14px; padding: 2px;" > 
                </div> 
                <div class="row" style="margin-left: 20px; margin-top: -2.9px; " > 
                    <img class="no" id="gS31" name="gS31" src="./imagenes/seguridad/verde.png" style="width: 15px; height: 14px; padding: 2px;" > 
                    <img class="no" id="yS31" name="yS31" src="./imagenes/seguridad/amarillo.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="bS31" name="bS31" src="./imagenes/seguridad/azul.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                    <img class="no" id="rS31" name="rS31" src="./imagenes/seguridad/rojo.png" style="width: 15px; margin-left: -2px; height: 14px; padding: 2px;" > 
                </div> 
            </div> 
        </div> 

        <div style="width: 50%; float: left;" > 
            <img src="./imagenes/pSeguridad.png" style="width: 110px; height: 90px; " > 
            <div style="font-size: 8px; margin-top: -90px; margin-left: 53px;" > 
                <div class="row" >
                    <p ><?php echo $sFatalD; ?></p>
                    <p style="margin-left: 15px;" ><?php echo $sFatalR; ?></p>
                </div>
                <div class="row" style="margin-top: -17px; " >
                    <p style="margin-left: -5px; " ><?php echo $sIncapacidadD; ?></p>
                    <p style="margin-left: 23px; " ><?php echo $sIncapacidadR; ?></p>
                </div>
                <div class="row" style="margin-top: -19px; " >
                    <p style="margin-left: -10px; " ><?php echo $sPAuxilioD; ?></p>
                    <p style="margin-left: 33px;" ><?php echo $sPAuxilioR; ?></p>
                </div>
                <div class="row" style="margin-top: -21px; " >
                    <p style="margin-left: -15px;" ><?php echo $sIncidenteD; ?></p>
                    <p style="margin-left: 43px;" ><?php echo $sIncapacidadR; ?></p>
                </div>
                <div class="row" style="margin-top: -19px; " >
                    <p style="margin-left: -18px;" ><?php echo $sCInseguraD; ?></p>
                    <p style="margin-left: 48px;" ><?php echo $sCInseguraR; ?></p>
                </div>
                <div class="row" style="margin-top: -19px; " >
                    <p style="margin-left: -23px;" ><?php echo $sAInseguroD; ?></p>
                    <p style="margin-left: 58px;" ><?php echo $sAInseguroR; ?></p>
                </div>
                <div class="row" style="margin-top: -16px; " >
                    <p style="margin-left: -28px;" ><?php echo $aAcumuladoD; ?></p>
                    <p style="margin-left: 11px;" ><?php echo $sAcumuladoR; ?></p>
                    <p style="margin-left: 42px;" ><?php echo $sRecordD; ?></p>
                    <p style="margin-left: 9px;" ><?php echo $sRecordR; ?></p> 
                </div>
            </div> 
        </div> 
    </div>     
    <div style="width: 100%; margin-top: 5px; float: left; " > 
        <div style="width: 50%; float: left; " >             
            <img src="./imagenes/arbol0.png" style="width: 150px; height: 90px; " > 
            <div style="margin-left: 24px; margin-top: -87px; margin-left: 99px; "  >
                <div class="row" > 
                    <img class="no" id="gA1" name="gA1" src="./imagenes/seguridad/verde.png" style="width: 14px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA2" name="gA2" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA3" name="gA3" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA4" name="gA4" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA5" name="gA5" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" >                            
                    <!--AMARILLO-->  
                    <img class="no" id="yA1" name="yA1" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA2" name="yA2" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA3" name="yA3" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA4" name="yA4" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA5" name="yA5" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--AZUL-->
                    <img class="no" id="bA1" name="bA1" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA2" name="bA2" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA3" name="bA3" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA4" name="bA4" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA5" name="bA5" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--ROJO-->
                    <img class="no" id="rA1" name="rA1" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA2" name="rA2" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA3" name="rA3" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA4" name="rA4" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA5" name="rA5" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                 </div> 
                <div class="row" style="margin-top: -3px; margin-left: -21px;" >                       
                    <img class="no" id="gA6" name="gA6" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA7" name="gA7" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA8" name="gA8" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA9" name="gA9" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="gA10" name="gA10" src="./imagenes/seguridad/verde.png" style="width:14px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA11" name="gA11" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA12" name="gA12" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--AMARILLO--> 
                    <img class="no" id="yA6" name="yA6" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA7" name="yA7" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA8" name="yA8" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA9" name="yA9" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="yA10" name="yA10" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA11" name="yA11" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA12" name="yA12" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--AZUL-->
                    <img class="no" id="bA6" name="bA6" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA7" name="bA7" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA8" name="bA8" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA9" name="bA9" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="bA10" name="bA10" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA11" name="bA11" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA12" name="bA12" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--ROJO-->
                    <img class="no" id="rA6" name="rA6" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA7" name="rA7" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA8" name="rA8" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA9" name="rA9" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="rA10" name="rA10" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA11" name="rA11" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA12" name="rA12" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                </div> 
                <div class="row" style="margin-top: -3px; margin-left: -21px; " > 
                    <img class="no" id="gA13" name="gA13" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA14" name="gA14" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA15" name="gA15" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA16" name="gA16" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="gA17" name="gA17" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA18" name="gA18" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA19" name="gA19" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--AMARILLO-->  
                    <img class="no" id="yA13" name="yA13" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA14" name="yA14" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA15" name="yA15" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA16" name="yA16" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="yA17" name="yA17" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA18" name="yA18" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA19" name="yA19" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--AZUL-->
                    <img class="no" id="bA13" name="bA13" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA14" name="bA14" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA15" name="bA15" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA16" name="bA16" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="bA17" name="bA17" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA18" name="bA18" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA19" name="bA19" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--ROJO-->
                    <img class="no" id="rA13" name="rA13" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA14" name="rA14" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA15" name="rA15" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA16" name="rA16" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="rA17" name="rA17" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA18" name="rA18" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA19" name="rA19" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                 </div> 
                <div class="row" style="margin-top: -3px; margin-left: -21px;" > 
                    <img class="no" id="gA20" name="gA20" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA21" name="gA21" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA22" name="gA22" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA23" name="gA23" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="gA24" name="gA24" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA25" name="gA25" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA26" name="gA26" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--AMARILLO--> 
                    <img class="no" id="yA20" name="yA20" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA21" name="yA21" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA22" name="yA22" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA23" name="yA23" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="yA24" name="yA24" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA25" name="yA25" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA26" name="yA26" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--AZUL-->
                    <img class="no" id="bA20" name="bA20" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA21" name="bA21" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA22" name="bA22" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA23" name="bA23" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="bA24" name="bA24" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA25" name="bA25" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA26" name="bA26" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--ROJO-->
                    <img class="no" id="rA20" name="rA20" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA21" name="rA21" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA22" name="rA22" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA23" name="rA23" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; padding: 2px;" > 
                    <img class="no" id="rA24" name="rA24" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA25" name="rA25" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA26" name="rA26" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                 </div> 
                <div class="row" style="margin-left: 1px; margin-top: -3px; " > 
                    <img class="no" id="gA27" name="gA21" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA28" name="gA22" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="gA29" name="gA23" src="./imagenes/seguridad/verde.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--AMARILLO-->  
                    <img class="no" id="yA27" name="yA21" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA28" name="yA22" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="yA29" name="yA23" src="./imagenes/seguridad/amarillo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--AZUL--> 
                    <img class="no" id="bA27" name="bA21" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA28" name="bA22" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="bA29" name="bA23" src="./imagenes/seguridad/azul.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <!--ROJO--> 
                    <img class="no" id="rA27" name="rA21" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA28" name="rA22" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
                    <img class="no" id="rA29" name="rA23" src="./imagenes/seguridad/rojo.png" style="width: 13.5px; margin-left: -3px; height: 15px; padding: 2px;" > 
               </div> 
                <div class="row" style="margin-left: 7px; margin-top: -3px; " > 
                    <img class="no" id="gA30" name="gA30" src="./imagenes/seguridad/verde.png" style="width: 14px; height: 14px; padding: 2px;" > 
                    <!--AMARILLO-->  
                    <img class="no" id="yA30" name="yA30" src="./imagenes/seguridad/amarillo.png" style="width: 14px; height: 14px; padding: 2px;" > 
                    <!--AZUL-->
                    <img class="no" id="bA30" name="bA30" src="./imagenes/seguridad/azul.png" style="width: 14px; height: 14px; padding: 2px;" > 
                    <!--ROJO-->
                    <img class="no" id="rA30" name="rA30" src="./imagenes/seguridad/rojo.png" style="width: 14px; height: 14px; padding: 2px;" > 
                 </div> 
                <div class="row" style="margin-left: 7px; margin-top: -1px; " > 
                    <img class="no" id="gA31" name="gA31" src="./imagenes/seguridad/verde.png" style="width: 14px; height: 14px; padding: 2px;" > 
                    <!--AMARILLO--> 
                    <img class="no" id="yA31" name="yA31" src="./imagenes/seguridad/amarillo.png" style="width: 14px; height: 14px; padding: 2px;" > 
                    <!--AZUL--> 
                    <img class="no" id="bA31" name="bA31" src="./imagenes/seguridad/azul.png" style="width: 14px; height: 14px; padding: 2px;" > 
                    <!--ROJO--> 
                    <img class="no" id="rA31" name="rA31" src="./imagenes/seguridad/rojo.png" style="width: 14px; height: 14px; padding: 2px;" > 
                </div> 
            </div> 
        </div> 
        <div style="width: 50%; float: left; " > 
            <img src="./imagenes/pAmbiente.png" style="width: 115px; height: 90px; " > 
            <div style="font-size: 8px; margin-top: -87px; margin-left: 52px; " > 
                <div class="row" > 
                    <p><?php echo $aContingenciaD; ?></p> 
                    <p style="margin-left: 16px;" ><?php echo $aContingenciaR; ?></p> 
                </div> 
                <div class="row" style="margin-top: -10px; " > 
                    <p style="margin-left: -10px;" > <?php echo $aEmergenciaD; ?> </p> 
                    <p style="margin-left: 38px;" > <?php echo $aEmergenciaR; ?> </p> 
                </div> 
                <div class="row" style="margin-top: -10px; " > 
                    <p style="margin-left: -18px;" > <?php echo $aNoConformidadD; ?> </p> 
                    <p style="margin-left: 55px;" > <?php echo $aNoConformidadR; ?></p> 
                </div> 
                <div class="row" style="margin-top: -10px; " > 
                    <p style="margin-left: -28px;" > <?php echo $aAcumuladoD; ?> </p>
                    <p style="margin-left: 13px;" > <?php echo $aAcumuladoR; ?> </p>
                    <p style="margin-left: 42px;" > <?php echo $aRecordD; ?> </p>
                    <p style="margin-left: 13px;" ><?php echo $aRecordR; ?></p> 
                </div> 
            </div> 
        </div>
    </div>     
</body>
