<HTML>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
        
    <?php  
        //FUNCIONES   
        include '../../../db/ServerFunctions.php';         
        session_start();
        date_default_timezone_set("America/Mexico_City");   
        
        $linea = $_SESSION["linea"]; 
        $mes = $_SESSION["mes"];
        $anio = $_SESSION["anio"];

        if (strlen($mes) < 2){
            $mes = '0'.$mes;
        } 
        
        if (isset($_SESSION['privilegio'])){ 
            $privilegio = $_SESSION['privilegio']; 
        } else { 
            $privilegio = 0; 
        } 
        
        
        $cCalidadIMonth = cCalidadInternaMes($linea, $_SESSION['fIni'], $_SESSION['fFin']); 
        $cCalidadKmMonth = cCalidadKmMes($linea, $_SESSION['fIni'], $_SESSION['fFin']); 
        
        /**************************** DIARIA *********************************/
        # PIEZAS Y MINUTOS REPORTADOS 
        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $fecha[$vDate] = $i; 
            $dateT[$vDate] = date("M, d",strtotime($i)); 
            $cantFIniternas[$vDate] = 0; 
            $cantFKM[$vDate] = 0; 
            $targetDay[$vDate] = 0;
        }
        
        #CALIDAD INTERNA
        for ($i = 0; $i < count($cCalidadIMonth); $i++){ 
            $vDate = $anio.$mes.(int)$cCalidadIMonth[$i][1]; 
            $cantFIniternas[$vDate] = $cCalidadIMonth[$i][0]; 
        } 
        
        #CALIDAD 0-KM 
        for ($i = 0; $i < count($cCalidadKmMonth); $i++){ 
            $vDate = $anio.$mes.(int)$cCalidadKmMonth[$i][1]; 
            $cantFKM[$vDate] = $cCalidadKmMonth[$i][0]; 
        } 
        
        # CONSULTA FALLAS INTERNAS REGISTRADAS         
//        for ($i = 0; $i < count($cTargetMonth); $i++){ 
//            $date = explode("-", $cTargetMonth[$i][0]); 
//            $vDate = $date[0].$date[1].(int)$date[2];             
//            $targetDay[$vDate] = $cTargetMonth[$i][1]; 
//        } 
        
        $dias = (strtotime($_SESSION['fIni'])- strtotime($_SESSION['fFin']))/86400; 
        
    ?>     
<body>
    <div class=" row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" >   
        <div id="fInternas" style="width: 105%; min-height: 200px; max-height: 600px; margin-top: -12px;" >
            <script>
                var chart = AmCharts.makeChart("fInternas", {
                    "type": "serial",
                    "theme": "light",
                    "dataDateFormat": "YYYY-MM-DD",
                    "precision": 2, 
                    "dataProvider": [
                        <?php for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                $date = explode("-", $i); 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                            ?> 
                        { 
                            "date": "<?php echo $fecha[$vDate]; ?>", 
                            "FInternas": <?php echo $cantFIniternas[$vDate]; ?>, 
                            "0km": <?php echo $cantFKM[$vDate]; ?>, 
                            "meta": <?php echo $targetDay[$vDate]; ?> 
                        }, 
                        <?php } ?> 
                    ],
                    "valueAxes": [{ 
                        "minimum": 0,
                        "fontSize": 8, 
                        "stackType": "regular", 
                        "axisAlpha": 0.3, 
                        "gridAlpha": 0.2 
                    }], 
                    "graphs": [{ 
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:11px'>[[category]]: <b>[[value]]</b></span>",
                        "fillAlphas": 1, 
                        "fillColors": "#311B92", 
                        "lineColor": "#311B92",
                        "labelText": "[[value]]", 
                        "lineAlpha": 1, 
                        "title": "Fallas Internas", 
                        "type": "column", 
                        "color": "#FFFF",
                        "valueField": "FInternas" 
                    }, { 
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:11px'>[[category]]: <b>[[value]]</b></span>",
                        "fillAlphas": 1, 
                        "fillColors": "#FF5C4D", 
                        "lineColor": "#FF5C4D", 
                        "labelText": "[[value]]", 
                        "lineAlpha": 1, 
                        "title": "0-KM", 
                        "type": "column", 
                        "color": "#FFFF", 
                        "valueField": "0km" 
                    }, { 
                        "id": "graph2", 
                        "balloonText": "<b>[[title]]</b><br><span style='font-size:11px'>[[category]]: <b>[[value]]</b></span>",
                        "lineThickness": 2,                         
                        "fillAlphas": 0, 
                        "lineAlpha": 2, 
                        "title": "Meta", 
                        "valueField": "meta", 
                        "dashLengthField": "dashLengthLine" 
                    }],
                    "chartCursor": { 
                        "pan": true, 
                        "valueLineEnabled": true, 
                        "valueLineBalloonEnabled": true, 
                        "cursorAlpha": 0, 
                        "valueLineAlpha": 0.2 
                    }, 
                    "categoryField": "date",
                    "categoryAxis": {
                        "parseDates": true, 
                        "dashLength": 1, 
                        "minorGridEnabled": true 
                    } 
                });
            </script> 
        </div> 
    </div> 
</body>    


        