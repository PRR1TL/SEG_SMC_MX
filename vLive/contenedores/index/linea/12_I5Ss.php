<HTML>
    
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>     
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">-->
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <!-- LIBRERIAS Y CONFIGURACION PARA PICKERS -->
    <!-- LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR -->     
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <?php 
        include '../../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
                
        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0; 
            $targetDayT[$vDate] = 0; 
        } 
        
        $jTDay = c5SDay($_SESSION['linea'], $_SESSION['fIni'], $_SESSION['fFin']); 
         
        /************************ DIA **********************/
        for ($i = 0; $i < count($jTDay); $i++ ){
            $date = explode("-", $jTDay[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2];         
            $valueDayT[$vDate] = $jTDay[$i][1]; 
        }  
        
        //DATOS PARA TAMAÑO DE TABLA  
        //$dias = (strtotime($_SESSION['fIni'])- strtotime($_SESSION['fFin']))/86400;
        $dias = (strtotime($_SESSION['fIni'])- strtotime($_SESSION['fFin']))/86400;
        $dias = abs($dias); 
        $dias = floor($dias)+1; 
        //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
        //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS 
        if ($dias > 19 ) { 
            $rowspan = 20;
        } else { 
            //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
            //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS 
            $rowspan = $dias;
        } 

    ?> 
    
    <style>
        #audit {
            width: 100%; 
            min-height: 200px; 
            max-height: 600px;
            margin-top: -45px;
        }
    </style>
    
    <body> 
        <div>  
            <div id="audit" >                
                <script>
                    var chart = AmCharts.makeChart("audit", { 
                        "type": "serial", 
                        "theme": "light", 
                        "dataDateFormat": "YYYY-MM-DD", 
                        "precision": 0, 
                        "dataProvider": [ 
                        <?php  
                            for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                $date = explode("-", $i);                                 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                        ?>    
                        { 
                            "date": "<?php echo "$fecha[$vDate]" ?>", 
                            "production": <?php echo $valueDayT[$vDate]; ?>, 
                            "meta": "<?php echo $targetDayT[$vDate]; ?>" 
                        }, 
                        <?php } ?>
                        ], 
                        "valueAxes": [{
                            "id": "v1",
                            "title": "Indicador",
                            "position": "left",
                            "autoGridCount": false
                        }],
                        "graphs": [{
                            "fillAlphas": 0.9, 
                            "lineAlpha": 0.2, 
                            "lineColor": "#02538b", 
                            "type": "column", 
                            "title": "Indicador",
                            "valueField": "production",
                            "balloonText": "[[title]]: <b style='font-size: 130%'>[[value]]</b>"
                        }, {
                            "valueAxis": "v2",                            
                            "lineThickness": 2,
                            "lineColor": "#62cf73",
                            "type": "line",
                            "title": "Meta",
                            "valueField": "meta",
                            "balloonText": "[[title]]: <b style='font-size: 130%'>[[value]]</b>"
                        }], 
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha": 0,
                            "valueLineAlpha": 0.2
                        },
                        "categoryField": "date",
                        "categoryAxis": {
                            "parseDates": true,
                            "dashLength": 1,
                            "minorGridEnabled": true
                        }
                    });
                </script> 
            </div>  
        </div> 
    </body>