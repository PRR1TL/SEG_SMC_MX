<HTML>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <?php 
    
        include '../../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
        //RECIBIMOS LOS VALORES DEL AJAX 
        $tema = "Tecnicas";
        
        $line = $_SESSION['linea'];
        $anio = $_SESSION['anio'];
        $mes = $_SESSION['mes'];
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio)); 
        $fecha1 = $anio.'-'.$mes.'-01';
        $fecha2 = $anio.'-'.$mes.'-'.$ultimoDiaMes; 
        
        //INCIALIZAMOS LAS VARIABLES
        //TABLA
        $countProblem = 0;
        $problema[0] = "";
        for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){        
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0;
            $targetDayT[$vDate] = 0;       
        }        
        
        //TIPO DE CALCULO QUE SE VA REALIZAR DE ACUERDO A LA OPCION DEL COMBO    
        $jTDay = jidokaDia($tema, $line, $anio);                   
        
        /********************* DIA ********************/
        for ($i = 0; $i < count($jTDay); $i++ ){
            $date = explode("-", $jTDay[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2];         
            $valueDayT[$vDate] = $jTDay[$i][1]; 
        } 
        
        #TARGET
        $cTargetDay = cTargetTecDaily($line, $fecha1, $fecha2); 
        for ($i = 0; $i < count($cTargetDay); $i++){ 
            $date = explode("-", $cTargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $targetDayT[$vDate] = $cTargetDay[$i][1]; 
        }         
        
    ?>

    <style>
        #jITec {
            width: 100%; 
            min-height: 200px; 
            max-height: 600px;
            margin-top: -12px;
        }
    </style>
    
    
    <div id="jITec" >                
        <script>
            var chartTec = AmCharts.makeChart("jITec", {
                "type": "serial",
                "theme": "light",
                "dataDateFormat": "YYYY-MM-DD",
                "precision": 0,
                "dataProvider": [
                <?php  
                    for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                        $date = explode("-", $i); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                ?> 
                { 
                    <?php if ($valueDayT[$vDate] > $targetDayT[$vDate]){ ?>
                        "lineColor": "#CD4C47", 
                    <?php } else { ?> 
                        "lineColor": "#02538b", 
                    <?php } ?> 
                    "date": "<?php echo $fecha[$vDate]; ?>", 
                    "production": <?php echo $valueDayT[$vDate]; ?>, 
                    "meta": "<?php echo $targetDayT[$vDate]; ?>"                        
                }, 
                <?php } ?> 
                ], 
                "valueAxes": [{ 
                    "id": "v1", 
                    "title": "Indicador", 
                    "position": "left", 
                    "autoGridCount": false, 
                    "labelFunction": function(value) { 
                        return Math.round(value); 
                    } 
                }],
                "graphs": [{
                    "valueAxis": "v1", 
                    "lineThickness": 2,
                    "lineColor": "#02538b",
                    "fillColorsField": "lineColor",
                    "lineColorField": "lineColor",
                    "type": "smoothedLine",
                    "title": "Indicador",
                    "valueField": "production",
                    "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]] min</b>"
                }, {
                    "valueAxis": "v2", 
                    "hideBulletsCount": 50,
                    "lineThickness": 2,
                    "lineColor": "#62cf73",
                    "type": "smoothedLine",
                    "title": "Meta",
                    "valueField": "meta",
                    "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]] min</b>"
                }], 
                "chartCursor": {
                    "pan": true,
                    "valueLineEnabled": true,
                    "valueLineBalloonEnabled": true,
                    "cursorAlpha": 0,
                    "valueLineAlpha": 0.2
                },
                "categoryField": "date",
                "categoryAxis": {
                    "parseDates": true,
                    "dashLength": 1,
                    "minorGridEnabled": true
                },
                "balloon": {
                    "borderThickness": 1,
                    "shadowAlpha": 0
                },
                "panelsSettings": {
                    "usePrefixes": true
                },"export": { 
                    "enabled": false
                }
            }); 
        </script>
    </div>  
</HTML>
    
