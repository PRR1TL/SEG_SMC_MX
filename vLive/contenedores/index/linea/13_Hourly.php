<HTML>
    <head>
        <LINK REL=StyleSheet HREF="../../../css/estiloHourly.css" TYPE="text/css" MEDIA=screen /> 
        <title>Hourly Count</title> 
        <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" /> 
        <meta charset="UTF-8" > 
        <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" > 
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" > 
        <script src="//www.amcharts.com/lib/4/core.js" ></script> 
        <script src="//www.amcharts.com/lib/4/charts.js" ></script> 
        <script src="//www.amcharts.com/lib/4/themes/animated.js" ></script> 
        
        <!--LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR--> 
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" > 
        <script src="https://code.jquery.com/jquery-1.12.4.js" ></script> 
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script> 
        
        <?php 
            require_once '../../../db/ServerFunctions.php'; 
            session_start(); 
            
            $_SESSION['nivelReporte'] = 3; 
            $line = $_SESSION["linea"]; 
            $fecha = date('Y-m-d'); 
            $fechaP = date("m/d/Y", strtotime($fecha)); 
            $targetOEE = 80; 
            
            $lineasArrObj = listarLineas(); 
            for ($i = 0; $i < count($lineasArrObj); $i++) { 
                $lineaArr[$i] = $lineasArrObj[$i][0]; 
            } 
            
            //INCIAMOS EL CONTEO PARA LAS HORAS
            for( $i = 0; $i < 24; $i++){ 
                $minT[$i] = 0; 
                $pzasTargetRealH[$i] = 0; 
                $pzasAcumTargetRealH[$i] = 0; 
                $acumuladoPzasProdH[$i] = 0;  
                
                $pzasAcumTarget[$i] = 0; 
                $pzasProdH[$i] = 0; 
                $cumPzasProdH[$i] = 0; 
                $noParteTcH[$i] = ''; 
                $typeH[$i] = ''; 
                $tcH[$i] = ''; 
                $pzasScrapH[$i] = 0; 

                $reworkH[$i] = 0; 
                $durCambioH[$i] = 0; 
                $durTecnicasH[$i] = 0; 
                $durCalidadH[$i] = 0; 
                $durOrgH[$i] = 0; 
                $durDesempenioH[$i] = 0; 
                
                //ETIQUETA DE PRODUCCION 
                $typeH[$i] = ''; 

                //LOS CODIGOS X & DESCRIPCION X SON DE CAMBIOS & TECNICAS
                $codeHX[$i] = '';
                $descriptionHX[$i]= '';

                //LOS CODIGOS Y DESCRIPCION Y SON ORGANIZACIONALES & DESEMPENIO
                $codeHY[$i] = '';
                $descriptionHY[$i]= ''; 

                $periodOEEH[$i] = 0; 
                $OEEAcumH[$i] = 0;                 
                $pzasTargetH[$i] = 0; 
            } 
           
            $datHourlyPzas = hourlyDiaPzasProd($line, $fecha); 
            if (count($datHourlyPzas) > 0){ 
                for($i = 0; $i < count($datHourlyPzas); $i++ ){ 
                    $h = $datHourlyPzas[$i][0];
                    $hour[$i] = $h;

                    if ($i != 0 && $hour[$i-1] == $h ){ 
                        $pzasProdH[$h] = $pzasProdH[$h] + $datHourlyPzas[$i][1]; 
                    } else { 
                        $pzasProdH[$h] = $datHourlyPzas[$i][1]; 
                    } 
                    
                    #ACUMULADO DE PIEZAS PRODUCIDAS POR SHIFS
                    if ($i != 0){
                        if ($h == 6 || $h == 15 || $h == 0){
                            $cumPzasProdH[$h] = $pzasProdH[$h];
                        } else {
                            $cumPzasProdH[$h] = $cumPzasProdH[$h-1] + $pzasProdH[$h];
                        }
                    } else {                
                        $cumPzasProdH[$h] =  $pzasProdH[$h];
                    } 
                }                
                
                #REFERENCIA DE PIEZAS PRODUCIDAS EN EL DIA
                $datNoPartTc = hourlyNoParteTC($line, $fecha);
                for ($i = 0; $i < count($datNoPartTc); $i++){
                    $h = $datNoPartTc[$i][0];                        
                    if (isset($typeH[$h])){
                        $typeH[$h] = $typeH[$h].'<br>'.$datNoPartTc[$i][1].' / '.$datNoPartTc[$i][2];
                    } else {
                        $typeH[$h] = $datNoPartTc[$i][1].' / '.$datNoPartTc[$i][2];
                    } 
                } 
                        
                #CALCULO DE MINUTOS REPORTADOS SIN TOMAR EN CUENTA LO PAROS PLANEADOS
                for($i = 0; $i < 24; $i++){ 
                    $minT[$i] = 60; 
                    
                    if ($pzasProdH[$i] != 0 && $pzasProdH[$h] > 0){ 
                        #TIEMPO PONDERADO DE TRABAJO 
                        $datTCPonderadoGeneralH = hourlyTCPonderadoHGeneral($line, $fecha, $i); 
                        for ($j = 0; $j < count($datTCPonderadoGeneralH); $j++){ 
                            $h = $datTCPonderadoGeneralH[$j][0]; 
                            $tcH[$h] = $datTCPonderadoGeneralH[$j][1]; 
                            $typeH[$h] = $typeH[$h].'<br> ('.$tcH[$h].')'; 
                        }
                    } else {
                        //TIEMPO CICLO GENERAL PARA FALLAS                       
                        $datTCPonderadoFallaH= hourlyTCPonderadoFallaHGeneral($line, $fecha, $i);
                        for ($j = 0; $j < count($datTCPonderadoFallaH); $j++){
                            $h = $datTCPonderadoFallaH[$j][0];
                            $tcH[$h] = $datTCPonderadoFallaH[$j][1];
                            $typeH[$h] = $typeH[$h].'<br> ('.$tcH[$h].')';
                        }
                    }
                }
                //
                $datMinDescDia = minDescHoraHourly($line, $fecha);
                for ( $i = 0; $i < count($datMinDescDia); $i++){
                    $h = $datMinDescDia[$i][0];
                    $minT[$h] = 60 - $datMinDescDia[$i][1];
                }

                #PIEZAS PLANEADAS POR HORA
                $datHPzasEspPercent = piezasEsperadasyPercentHora($line, $fecha);
                for ($i = 0; $i < count($datHPzasEspPercent); $i++){
                    $h = $datHPzasEspPercent[$i][0];
                    $pzasTargetH[$h] = $datHPzasEspPercent[$i][1];
                    $periodOEEH[$h] = @round($datHPzasEspPercent[$i][2],2);     
                }
                    
                $pzasAcum = 0;
                for( $i = 0; $i < 24 ; $i++){
                    if($i == 0 || $i == 6 || $i == 15){
                        $pzasAcumTarget[$i] = $pzasTargetH[$i];  
                    }else {
                        if ($i == 0){
                            $pzasAcumTarget[$i] = $pzasTargetH[$i]; 
                        }else {
                            if ($typeH[$i] != ''){
                                $pzasAcumTarget[$i] = $pzasAcumTarget[$i-1] + $pzasTargetH[$i]; 
                            } else {
                                $pzasAcumTarget[$i] = $pzasAcumTarget[$i-1] + 0; 
                            }                            
                        }
                    }
                    $pzasAcum = $pzasAcumTarget[$i];
                }

                #CONSULTAS PARA ACUMULADO DE PIEZAS PRODUCIDAS HOURLY                
                for ($i = 0; $i < 24; $i++){
                    if ($i != 0 && $i != 6 && $i != 15  ){
                        if ($cumPzasProdH[$i] != 0){
                            $acumuladoPzasProdH[$i] = $acumuladoPzasProdH[$i-1] + $pzasProdH[$i];
                        }else {                        
                            $acumuladoPzasProdH[$i] =  $acumuladoPzasProdH[$i-1];
                        }        
                    }else {
                        $acumuladoPzasProdH[$i] =  $cumPzasProdH[$i];
                    }                    
                }

                //CONSULTAS POR TEMAS (PRIMARIOS)
                //SCRAP POR HORAS
                $datHourlyScrap = hourlyDiaPzasCalidad($line, $fecha);
                for($i = 0; $i < count($datHourlyScrap); $i++){
                    $h = $datHourlyScrap[$i][0];
                    $pzasScrapH[$h] = $datHourlyScrap[$i][1];
                }
                
                //CALIDAD
                $datCalidad = hourlyDiaDurCalidad($line, $fecha);
                for($i = 0; $i < count($datCalidad); $i++){
                    $h = $datCalidad[$i][0];
                    $problemaCalidad[$h] = $datCalidad[$i][1];
                    $opCalidad[$h] = $datCalidad[$i][2];
                    $durCalidadH[$h] = $datCalidad[$i][3];
                    //echo "<br>",$h,' -> ', $problemaCalidad[$h];
                }
                
                //REWORK
                //CAMBIOS
                $datCambios = hourlyDiaDurCambios($line, $fecha); 
                for($i = 0; $i < count($datCambios); $i++ ){ 
                    $h = $datCambios[$i][0]; 
                    $problemaCambioH[$h] = 'Cambio de Modelo '.$datCambios[$i][1].' '. $datCambios[$i][5] .': '.$datCambios[$i][3]. ' a '.$datCambios[$i][4];
                    $opCambioH[$h] = $datCambios[$i][2]; 
                    $durCambioH[$h] = $datCambios[$i][6]; 
                } 

                //TECNICAS
                $datTecnicas = hourlyDiaDurTecnicas($line, $fecha);
                for($i = 0; $i < count($datTecnicas); $i++ ){
                    $h = $datTecnicas[$i][0];                   
                    $problemaTecnicas[$h] = $datTecnicas[$i][1];
                    $opTecnicas[$h] = $datTecnicas[$i][2];
                    $durTecnicasH[$h] = $datTecnicas[$i][3];
                } 

                //ORGANIZACIONALES 
                $datOrganizacionales = hourlyDiaOrg($line, $fecha);
                for($i = 0; $i < count($datOrganizacionales); $i++){ 
                    $h = $datOrganizacionales[$i][0]; 
                    $problemOrgH[$h] = $datOrganizacionales[$i][1]; 
                    $opOrgH[$h] = $datOrganizacionales[$i][2]; 
                    $durOrgH[$h] = $datOrganizacionales[$i][3]; 
                }        
                
                ########### APARTADO PARA PROBLEMAS SECUNDARIOS *************
                //CALIDAD
                $datCalidadSec = hourlyDiaDurCalidadSec($line, $fecha);
                for($i = 0; $i < count($datCalidadSec); $i++){
                    $h = $datCalidadSec[$i][0];
                    $problemaCalidadSec[$h] = $datCalidadSec[$i][1].' ('.$datCalidadSec[$i][3].')';
                    $opCalidadSec[$h] = $datCalidadSec[$i][2];                    
                }
                
                //CAMBIOS
                $datCambiosSec = hourlyDiaDurCambiosSec($line, $fecha);
                for($i = 0; $i < count($datCambiosSec); $i++ ){
                    $h = $datCambiosSec[$i][0];
                    $problemaCambioSecH[$h] = 'Cambio de Modelo '.$datCambiosSec[$i][1].' '. $datCambiosSec[$i][5] .': '.$datCambiosSec[$i][3]. ' a '.$datCambiosSec[$i][4].' ('.$datCambiosSec[$i][6].')';
                    $opCambioSecH[$h] = $datCambiosSec[$i][2];
                }

                //TECNICAS
                $datTecnicasSec = hourlyDiaDurTecnicasSec($line, $fecha);
                for($i = 0; $i < count($datTecnicasSec); $i++ ){
                    $h = $datTecnicasSec[$i][0];                   
                    $problemaTecnicasSec[$h] = $datTecnicasSec[$i][1].' ('. $datTecnicasSec[$i][3].')';
                    $opTecnicasSec[$h] = $datTecnicasSec[$i][2];
                }

                //ORGANIZACIONALES 
                $datOrganizacionalesSec = hourlyDiaOrgSec($line, $fecha);
                for($i = 0; $i < count($datOrganizacionalesSec); $i++){
                    $h = $datOrganizacionalesSec[$i][0];
                    $problemOrgSecH[$h] = $datOrganizacionalesSec[$i][1].' ('.$datOrganizacionalesSec[$i][3].')';
                    $opOrgSecH[$h] = $datOrganizacionalesSec[$i][2]; 
                }    
                
                /**************************************************************/ 
                //DESEMPENIO
                $datDesempenio = hourlyDiaDesempenio($line, $fecha);
                for($i = 0; $i < count($datDesempenio); $i++){
                    $h = $datDesempenio[$i][0];
                    $opDesempenio[$h] = $line;
                    $problemaDesempenioH[$h] = 'Perdida Por Desempenio';
                    $durDesempenioH[$h] = $datDesempenio[$i][1];
                }
                
                // ARREGLO PARA JUNTAS LOS CODIGOS Y LOS PROBLEMAS POR X & Y
                for($i = 0; $i < 24; $i++ ){                     
                    //X: CALIDAD Y CAMBIO DE MODELO 
                    if (isset($opCambioH[$i]) && isset($opCalidad[$i]) ){
                        $codeHX[$i] = $opCalidad[$i].', '.$opCambioH[$i].'<br>';
                        $descriptionHX[$i]= $problemaCalidad[$i].', '.$problemaCambioH[$i].' <br> ';
                    } else if(isset($opCambioH[$i]) && !isset($opCalidad[$i])){
                        $codeHX[$i] = $opCambioH[$i].'<br>';
                        $descriptionHX[$i]= $problemaCambioH[$i].' <br> ';
                    } else if(!isset($opCambioH[$i]) && isset($opCalidad[$i])){
                        $codeHX[$i] = $opCalidad[$i].'<br>';
                        $descriptionHX[$i]= $problemaCalidad[$i].' <br> ';
                    }
                    
                    //CODIGOS SECUNDARIOS
                    if (isset($opCalidadSec[$i]) && isset($opCambioSecH[$i])){
                        $codeHX[$i] = $codeHX[$i].$opCalidadSec[$i].'<br>'.$opCambioSecH[$i]; 
                    } else if (!isset($opCalidadSec[$i]) && isset($opCambioSecH[$i])){
                        $codeHX[$i] = $codeHX[$i].'<br>'.$opCambioSecH[$i]; 
                    } else if (isset($opCalidadSec[$i]) && isset($opCambioSecH[$i])){
                        $codeHX[$i] = $codeHX[$i].'<br>'.$opCalidadSec[$i]; 
                    } 
                    
                    //PROBLEMAS SECUNDARIOS
                    if (isset($problemaCalidadSec[$i]) && isset($problemaCambioSecH)){
                        $descriptionHX[$i] = $descriptionHX[$i].$problemaCalidadSec[$i].'<br>'.$problemaCambioSecH[$i]; 
                    } else if (!isset($problemaCalidadSec[$i]) && isset($problemaCambioSecH[$i])){
                        $descriptionHX[$i] = $descriptionHX[$i].$problemaCambioSecH[$i]; 
                    } else if (isset($problemaCalidadSec[$i]) && !isset($problemaCambioSec[$i])){
                        $descriptionHX[$i] = $descriptionHX[$i].$problemaCalidadSec[$i]; 
                    }
                    
                    //SEPARACION DE PROBLEMAS
                    if (isset($opOrgH[$i]) && isset($opTecnicas[$i]) && isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opOrgH[$i].', '.$opTecnicas[$i].', '.$opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].', '.$problemaTecnicas[$i].', '.$problemaDesempenioH[$i].' <br> ';
                    } else if (isset($opOrgH[$i]) && isset($problemaTecnicas[$i]) && !isset($opDesempenio[$i])){
                        $codeHY[$i] = $opOrgH[$i].', '.$opTecnicas[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].', '.$problemaTecnicas[$i].' <br> ';
                    } else if (isset($opOrgH[$i]) && !isset($problemaTecnicas[$i]) && isset($opDesempenio[$i])){
                        $codeHY[$i] = $opOrgH[$i].','.$opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].','.$problemaDesempenioH[$i].' <br> ';
                    } else if (!isset($opOrgH[$i]) && isset($problemaTecnicas[$i]) && isset($opDesempenio[$i])){
                        $codeHY[$i] = $opTecnicas[$i].', '.$opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemaTecnicas[$i].', '.$problemaDesempenioH[$i].' <br> ';
                    } else if (isset($opOrgH[$i]) && !isset($opTecnicas[$i]) && !isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opOrgH[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].' <br> ';
                    } else if (!isset($opOrgH[$i]) && isset($opTecnicas[$i]) && !isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opTecnicas[$i].'<br>';
                        $descriptionHY[$i] = $problemaTecnicas[$i].' <br> ';
                    } else if (!isset($opOrgH[$i]) && !isset($opTecnicas[$i]) && isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemaDesempenioH[$i].' <br> ';
                    }  
                    
                    //CODIGOS SECUNDARIOS
                    if (isset($opTecnicasSec[$i]) && isset($opOrgSecH[$i])){ 
                        $codeHY[$i] = $codeHY[$i].$opTecnicasSec[$i].'<br>'.$opOrgSecH[$i]; 
                    } else if (!isset($opTecnicasSec[$i]) && isset($opOrgSecH[$i])){ 
                        $codeHY[$i] = $codeHY[$i].$opOrgSecH[$i];
                    } else if (isset($opTecnicasSec[$i]) && !isset($opOrgSecH[$i])){
                        $codeHY[$i] = $codeHY[$i].$opTecnicasSec[$i]; 
                    }
                    
                    //PROBLEMAS SECUNDARIOS
                    if (isset($problemOrgSecH[$i]) && isset($problemaTecnicasSec[$i])){                       
                        $descriptionHY[$i] = $descriptionHY[$i].$problemOrgSecH[$i].'<br>'.$problemaTecnicasSec[$i]; 
                    } else if (!isset($problemOrgSecH[$i]) && isset($problemaTecnicasSec[$i])){
                        $descriptionHY[$i] = $descriptionHY[$i].$problemaTecnicasSec[$i]; 
                    } else if (isset($problemOrgSecH[$i]) && !isset($problemaTecnicasSec[$i])){
                        $descriptionHY[$i] = $descriptionHY[$i].$problemOrgSecH[$i]; 
                    } 
                }        

                //PARA EL TARGET DE ACUERDO REAL (QUITA LOS MINUTOS EN AUTOMATICO LAS PIEZAS DE ACUERDO A LOS MINUTOS DE LOS PAROS)
                // Y TOMANDO EL TIEMPO PLANEADO QUITANDO COMEDOR, INICIO DE COMEDOR Y ASI            
                for($i = 0; $i < 24; $i++){ 
                    $tiempoReal[$i] = $minT[$i] - ($durCambioH[$i] + $durTecnicasH[$i] + $durOrgH[$i] + $durDesempenioH[$i]); 
                    
                    // COMPARAMOS SI EL TIEMPO PERDIDO ES MAYOR AL TIEMPLO PLANEADO (TIEMPO QUE SE LE QUITA COMEDOR Y JUNTAS)
                    if($tiempoReal[$i] >= 0 ){ 
                        if ($tcH[$i] != '') {  
                            $pzasTargetRealH[$i] = @round((60 * $tiempoReal[$i]) / $tcH[$i]) - $pzasScrapH[$i]; 
                        } else { 
                            $pzasTargetRealH[$i] = 0; 
                        } 
                    } else { 
                        $pzasTargetRealH[$i] = 0; 
                    } 
                } 

                $pzasAcum2 = 0; 
                for( $i = 0; $i < 24 ; $i++){ 
                    if($i == 0 || $i == 6 || $i == 15 ){ 
                        $pzasAcumTargetRealH[$i] = $pzasTargetRealH[$i]; 
                    }else { 
                        if ($i == 0){ 
                            $pzasAcumTargetRealH[$i] = $pzasTargetRealH[$i]; 
                        }else { 
                            $pzasAcumTargetRealH[$i] = $pzasAcumTargetRealH[$i-1] + $pzasTargetRealH[$i]; 
                        } 
                    } 
                    $pzasAcum2 = $pzasAcumTargetRealH[$i]; 
                } 
                $totalPzasTarget = $pzasAcumTarget[5] + $pzasAcumTarget[14] + $pzasAcumTarget[23];
                $totalPzasTargetReal = $pzasAcumTargetRealH[5] + $pzasAcumTargetRealH[14] + $pzasAcumTargetRealH[23] ;
                $totalPzasProducidas = $acumuladoPzasProdH[5] + $acumuladoPzasProdH[14] + $acumuladoPzasProdH[23];
               
                //TOTAL DE FALLAS 
                for ($i = 0; $i < 24; $i++){
                    if ($i == 0){                 
                        $pzasTotalScrap = $pzasScrapH[$i];
                        $durTotalCambios = $durCambioH[$i];
                        $durTotalTecnicas = $durTecnicasH[$i];
                        $durTotalOrg = $durOrgH[$i];
                        $durTotalDesempenio = $durDesempenioH[$i];
                    }else { 
                        $pzasTotalScrap = $pzasTotalScrap + $pzasScrapH[$i];
                        $durTotalCambios = $durTotalCambios + $durCambioH[$i];
                        $durTotalTecnicas = $durTotalTecnicas + $durTecnicasH[$i];
                        $durTotalOrg = $durTotalOrg + $durOrgH[$i];
                        $durTotalDesempenio = $durTotalDesempenio + $durDesempenioH[$i];
                    } 
                } 
                
                //PORCENTAJES OEE 
                $div = 0;
                $acum[0] = 0;
                for ($i = 0; $i < 24; $i++){
                    if ($i == 0){
                        if ($pzasProdH[$i] != 0){
                            $acum[$i] = ($acumuladoPzasProdH[$i] * 100 ) / $pzasAcumTarget[$i];
                        } else {
                            $acum[$i] = 0;
                        }
                            $div = 1;
                    }else {
                        if ($i == 0 || $i == 6 || $i == 15 ){
                            if ($pzasProdH[$i] > 0){
                                $acum[$i] = ($acumuladoPzasProdH[$i] * 100 ) / $pzasAcumTarget[$i];
                            } else {
                                $acum[$i] = 0;
                            }
                            $div = 1;
                        } else {
                            if ($pzasProdH[$i] != 0){
                                $acum[$i] = ($acumuladoPzasProdH[$i] * 100 ) / $pzasAcumTarget[$i];
                            } else { 
                                $acum[$i] = $acum[$i-1];
                            }
                            $div++;
                        }
                    }
                    $OEEAcumH[$i] = @round($acum[$i],2,PHP_ROUND_HALF_UP) ;
                }
            } else { 
                //INDICADORES DE PIEZAS (DAOLY TOTAL)
                $totalPzasTarget = 0;
                $totalPzasTargetReal = 0;
                $totalPzasProducidas = 0;
                $pzasTotalScrap = 0;
                $durTotalCambios = 0;
                $durTotalTecnicas = 0;
                $durTotalOrg = 0;
                $durTotalDesempenio = 0; 
            }        
            
            //PIE DE HOURLY 
            $datOEEDay = percentHourlyDay ($line, $fecha);             
            
            $pFnProd = 0;
            $pFnTec = 0;
            $pFnOrg = 0;
            $pFnCal = 0;
            $pFnCam = 0;
            $pFnTFal = 0;
            
            for ($i = 0; $i < count($datOEEDay); $i++){ 
                $pFnProd = @round($datOEEDay [$i][0],2);
                $pFnTec = @round($datOEEDay [$i][1],2);
                $pFnOrg = @round($datOEEDay [$i][2],2);
                $pFnCal = @round($datOEEDay [$i][3],2);
                $pFnCam = @round($datOEEDay [$i][4],2);
                $pFnTFal = @round($datOEEDay [$i][5],2);
            } 
            
            //APARTADO PARA LINETACK
            //CONSULTA PARA EL TOTAL DE PIEZAS PRODUCIDAS ESE DIA
            $pzasTotalDia = 0;
            $datPzasTotalDia = piezasTotalesDiaHourly($line, $fecha);
            for ($i = 0; $i < count($datPzasTotalDia); $i++) {
                $pzasTotalDia = $datPzasTotalDia[$i][0];
            }
            
            $lineTakt = 0;            
            //CONSULTA DE CANTIDAD DE PIEZAS POR TIEMPO CICLO 
            $datPzasTC = piezasTCHourly($line, $fecha);
            for ($i = 0; $i < count($datPzasTC); $i++){ 
                $pzasTc[$i] = $datPzasTC[$i][0]; 
                $tcPzas[$i] = $datPzasTC[$i][1]; 
                //HACEMOS EL PORCENTAJE POR TIEMPO CICLO 
                $percentTc[$i] = @round(($pzasTc[$i]*100)/$pzasTotalDia, 0, PHP_ROUND_HALF_UP )*0.010;                 
                //OBTENEMOS EL PRODUCTO DE LOS PORCENTAJES POR EL TIEMPO CICLO 
                $prodTc[$i] = $tcPzas[$i] * $percentTc[$i];                 
                //SE HACE LA SUMATORIA DE LOS PRODUCTOS (SUMPROD) 
                $lineTakt += $prodTc[$i]; 
            } 
        ?>
            
        <!--FUNCIONALIDAD DE LOS DATEPICKET-->
        <script>            
            $( function() { 
                $( "#dateEnd").datepicker({                     
                    dateFormat: 'mm/dd/yy',
                    maxDate: new Date(),
                    onSelect: function(date) { 
                        var l = document.getElementById("lineaCombo").value; 
                        var f = document.getElementById("dateEnd").value; 
                        var t = document.getElementById("Meta").value; 
                        // MANDAMOS AJAX PARA LA SESION Y ASI REFRESCAR 
                        $.ajax({ 
                            type: "POST", 
                            url: "../../db/sesionHourly.php", 
                            data: {linea: l, fecha: f, target: t}, 
                            success: function (result) { 
                                location.reload(true);
                            } 
                        }).fail( function( jqXHR, textStatus, errorThrown ) { 
                            if (jqXHR.status === 0) { 
                                alert('Not connect: Verify Network.'); 
                            } else if (jqXHR.status == 404) { 
                                alert('Requested page not found [404]');
                            } else if (jqXHR.status == 500) { 
                                alert('Internal Server Error [500].'); 
                            } else if (textStatus === 'parsererror') { 
                                alert('Requested JSON parse failed.'); 
                            } else if (textStatus === 'timeout') { 
                                alert('Time out error.'); 
                            } else if (textStatus === 'abort') { 
                                alert('Ajax request aborted.'); 
                            } else { 
                                alert('Uncaught Error: ' + jqXHR.responseText); 
                            } 
                        }); 
                    } 
                }); 
                                
                $( "#target" ).keypress(function( event ) { 
                    if ( event.which == 13 || event.keyCode == 10 || event.keyCode == 13 ) {                         
                        var l = document.getElementById("lineaCombo").value; 
                        var f = document.getElementById("dateEnd").value; 
                        var t = document.getElementById("Meta").value; 
                        
                        // MANDAMOS AJAX PARA LA SESION Y ASI REFRESCAR 
                        $.ajax({ 
                            type: "POST", 
                            url: "../../db/sesionHourly.php", 
                            data: {linea: l, fecha: f, target: t}, 
                            success: function (result) { 
                                location.reload(true);
                            } 
                        }).fail( function( jqXHR, textStatus, errorThrown ) { 
                            if (jqXHR.status === 0) { 
                                alert('Not connect: Verify Network.');
                            } else if (jqXHR.status == 404) {
                                alert('Requested page not found [404]');
                            } else if (jqXHR.status == 500) {
                                alert('Internal Server Error [500].');
                            } else if (textStatus === 'parsererror') {
                                alert('Requested JSON parse failed.');
                            } else if (textStatus === 'timeout') {
                                alert('Time out error.');
                            } else if (textStatus === 'abort') {
                                alert('Ajax request aborted.');
                            } else {
                                alert('Uncaught Error: ' + jqXHR.responseText);
                            }
                        });
                    } 
                }); 
            } ); 
            
            function consultaLinea() { 
                var l = document.getElementById("lineaCombo").value; 
                var f = document.getElementById("dateEnd").value; 
                var t = document.getElementById("Meta").value; 
                
                // MANDAMOS AJAX PARA LA SESION Y ASI REFRESCAR 
                $.ajax({ 
                    type: "POST", 
                    url: "../../db/sesionHourly.php", 
                    data: {linea: l, fecha: f, target: t}, 
                    success: function (result) { 
                        location.reload(true);
                    } 
                }).fail( function( jqXHR, textStatus, errorThrown ) { 
                    if (jqXHR.status === 0) { 
                        alert('Not connect: Verify Network.'); 
                    } else if (jqXHR.status == 404) { 
                        alert('Requested page not found [404]'); 
                    } else if (jqXHR.status == 500) { 
                        alert('Internal Server Error [500].'); 
                    } else if (textStatus === 'parsererror') { 
                        alert('Requested JSON parse failed.'); 
                    } else if (textStatus === 'timeout') { 
                        alert('Time out error.'); 
                    } else if (textStatus === 'abort') { 
                        alert('Ajax request aborted.'); 
                    } else { 
                        alert('Uncaught Error: ' + jqXHR.responseText); 
                    } 
                }); 
            } 
        </script>  
    </head> 
    
    <body> 
        <table id="table" style="display: flex; flex-flow: column; height: 100%; width: 96%; font-size:1.2vh; margin-left: 2%; " > 
            <thead style="font-weight:bold; width: 100%" > 
                <tr style="background: #F2F2F2;" > 
                    <th class="thH" ></th> 
                    <th class="thH" >Time</th> 
                    <th class="thH" COLSPAN=2 >Target</th> 
                    <th class="thH" COLSPAN=2>Real Prod</th> 
                    <th class="thH" >Pces / Hour</th> 
                    <th class="thH" >Type</th> 
                    <th class="thH" COLSPAN=2>OEE%</th> 
                </tr> 
                <tr style="background: #F2F2F2" height="25vh" > 
                    <th class="thH" style="width: 1.55%" >Period</th> 
                    <th class="thH" style="width: 1.7%" >Mins</th> 
                    <th class="thH" style="width: 1.9%" >Units</th>
                    <th class="thH" style="width: 2.0%" >Cum</th> 
                    <th class="thH" style="width: 1.9%" >Units</th>
                    <th class="thH" style="width: 2.3%" >Cum</th> 
                    <th class="thH" style="width: 10.7%" ></th>                       
                    <th class="thH" style="width: 2.7%" >Period OEE</th><th class="thH" style="width: 4.3%" > OEE Cumm </th> 
                </tr> 
            </thead>
            <tbody style=" flex: 1 1 auto; display: block; overflow-y: scroll; width: 100%;" > 
                <!-- PRIMER TURNO --> 
                <?php 
                    for ($i = 6; $i < 15; $i++){ ?> 
                    <tr style= "background: <?php  if($periodOEEH[$i] == 0 ){ echo '#F2F3F4';} ?> ;" > 
                        <td class="tdH" style="width: 6.8%"> <?php echo $i,'-',$i+1; ?> </td> 
                        <td class="tdH" align='center' style="width: 6.3%" > <?php echo $minT[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width: 6.4%;  background: #cacfd2 " > <?php echo $pzasTargetH[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width: 6.4%" > <?php echo $pzasAcumTarget[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width: 6.4%;  background: <?php if( ($pzasProdH[$i]*100)/$pzasTargetH[$i] > $targetOEE ){ echo '#58d68d'; 
                                                } else { 
                                                    if ($pzasProdH[$i] == 0){ echo '#B3B6B7'; } else { 
                                                        if ( $pzasTargetRealH[$i] > $pzasProdH[$i] ){ echo '#cd6155'; 
                                                        } else { echo '#f7dc6f';  } 
                                                    }  
                                                } ?>; 
                                                color: black;" > 
                                            <?php 
                                                echo $pzasProdH[$i]; 
                                            ?> </td> 
                        <td class="tdH" align='center' style="width: 6.8%" > <?php echo $acumuladoPzasProdH[$i];?> </td> 
                        <td class="tdH" style="width: 24.64%" > 
                            <div id="<?php echo "chartdiv".$i; ?>" style="height: 40px; margin-top: -15px;" > 
                                <script> 
                                    // Themes begin
                                    am4core.useTheme(am4themes_animated); 
                                    // Themes end
                                    var container = am4core.create("<?php echo "chartdiv".$i; ?>", am4core.Container); 
                                    container.width = am4core.percent(100); 
                                    container.height = am4core.percent(200); 
                                    container.layout = "vertical"; 
                                    container.hideCredits = true; 

                                    /* Create chart instance */
                                    var chart = container.createChild(am4charts.XYChart); 
                                    chart.paddingRight = 10; 

                                    /* Add data */
                                    chart.data = [{ 
                                        "category": "Evaluation", 
                                        "value": <?php echo $pzasProdH[$i]; ?>, 
                                        "Meta": <?php echo $pzasTargetH[$i]; ?> 
                                    }]; 

                                    /* Create axes */
                                    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis()); 
                                    categoryAxis.renderer.labels.template.disabled = true; 
                                    categoryAxis.dataFields.category = "category"; 
                                    categoryAxis.renderer.minGridDistance = 10; 
                                    categoryAxis.renderer.grid.template.disabled = true; 

                                    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
                                    valueAxis.renderer.minGridDistance = 10; 
                                    valueAxis.renderer.labels.template.disabled = true; 
                                    valueAxis.renderer.grid.template.disabled = true; 
                                    valueAxis.min = 0; 
                                    valueAxis.strictMinMax = true; 
                                    valueAxis.renderer.labels.template.adapter.add("text", function(text) { 
                                        return text ; 
                                    }); 

                                    /* Create series */
                                    var series = chart.series.push(new am4charts.ColumnSeries()); 
                                    series.dataFields.valueX = "value"; 
                                    series.dataFields.categoryY = "category"; 
                                    series.columns.template.fill = am4core.color("#23B14D"); 
                                    series.columns.template.stroke = am4core.color("#334EFF"); 
                                    series.columns.template.strokeWidth = .1; 
                                    series.columns.template.strokeOpacity = 0.5; 
                                    series.columns.template.height = am4core.percent(100); 
                                    series.columns.template.tooltipText = "[bold]Real: [/] {value}pzas [bold]Meta: [/]{target}pzas"; 

                                    var series2 = chart.series.push(new am4charts.LineSeries()); 
                                    series2.dataFields.valueX = "Meta"; 
                                    series2.dataFields.categoryY = "category"; 
                                    series2.strokeWidth = 4; 

                                    var bullet = series2.bullets.push(new am4charts.Bullet()); 
                                    var line = bullet.createChild(am4core.Line); 
                                    line.x1 = 0; 
                                    line.y1 = -200;
                                    line.x2 = 0;
                                    line.y2 = 200;
                                    line.stroke = am4core.color("#ff0f00");
                                    line.strokeWidth = 4; 
                                </script> 
                            </div> 
                        </td>                    
                        </td> 
                        <td class="tdH" align='center' style="width: 8.5%" ><?php echo $periodOEEH[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width: 8.7%" ><?php echo $OEEAcumH[$i];?> </td> 
                    </tr> 
                <?php } ?>                         
                <!--SEPARADOR / INIDICADOR DE PRIMER TURNO--> 
                <tr bgcolor=" <?php if( $OEEAcumH[14] < $targetOEE ){ echo '#cd6155'; /*color rojo*/} else {echo '#58d68d'; /*color verde*/} ?> " > 
                    <td class="tdH" colspan="2" ><b> 1 Shift </b></td> 
                    <td class="tdH" colspan="2" align="center" ><b> <?php echo $pzasAcumTarget[14]; ?> </b></td> 
                    <td class="tdH" colspan="2" align="center" ><b> <?php echo $acumuladoPzasProdH[14]; ?> </b></td> 
                    <td class="tdH" >  </td> 
                    <td class="tdH" ><b> <?php echo $periodOEEH[14].'%'; ?> </td> 
                    <td class="tdH" ><b> <?php echo $OEEAcumH[14].'%';  ?> </td> 
                </tr> 
                <!-- SEGUNDO TURNO -->
                <?php 
                    for ($i = 15; $i < 24; $i++){ ?> 
                    <tr style= "background: <?php  if($periodOEEH[$i] == 0 ){ echo '#F2F3F4'; } ?> ;" > 
                        <td class="tdH" style="width:2.45%"> <?php echo $i,'-',$i+1; ?> </td> 
                        <td class="tdH" align='center' style="width:2.2%" > <?php echo $minT[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width: 2.3%;  background: #cacfd2 " > <?php echo $pzasTargetH[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width:2.3%" > <?php echo $pzasAcumTarget[$i]; ?> </td>                             
                        <td class="tdH" align='center' style="width: 2.3%;  background: <?php if( ($pzasProdH[$i]*100)/$pzasTargetH[$i] > $targetOEE ){ echo '#58d68d'; 
                                                } else { 
                                                    if ($pzasProdH[$i] == 0){ echo '#B3B6B7'; } else { 
                                                        if ( $pzasTargetRealH[$i] > $pzasProdH[$i] ){ echo '#cd6155'; 
                                                        } else { echo '#f7dc6f';  } 
                                                    } 
                                                } ?>; 
                                                color: black;"> 
                                            <?php 
                                                echo $pzasProdH[$i]; 
                                            ?> </td> 
                        <td class="tdH" align='center' style="width:2.3%" > <?php echo $acumuladoPzasProdH[$i];?> </td> 
                        <td class="tdH" style="width: 8.57%" > 
                            <div id="<?php echo "chartdiv".$i; ?>" style="height: 40px; margin-top: -15px;" > 
                                <script>
                                    // Themes begin
                                    am4core.useTheme(am4themes_animated); 
                                    // Themes end
                                    var container = am4core.create("<?php echo "chartdiv".$i; ?>", am4core.Container); 
                                    container.width = am4core.percent(100); 
                                    container.height = am4core.percent(200); 
                                    container.layout = "vertical"; 
                                    container.hideCredits = true; 

                                    /* Create chart instance */
                                    var chart = container.createChild(am4charts.XYChart); 
                                    chart.paddingRight = 10; 
                                    
                                    /* Add data */ 
                                    chart.data = [{ 
                                        "category": "Evaluation", 
                                        "value": <?php echo $pzasProdH[$i]; ?>, 
                                        "Meta": <?php echo $pzasTargetH[$i]; ?> 
                                    }]; 

                                    /* Create axes */
                                    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis()); 
                                    categoryAxis.renderer.labels.template.disabled = true; 
                                    categoryAxis.dataFields.category = "category"; 
                                    categoryAxis.renderer.minGridDistance = 10; 
                                    categoryAxis.renderer.grid.template.disabled = true; 

                                    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
                                    valueAxis.renderer.minGridDistance = 10; 
                                    valueAxis.renderer.labels.template.disabled = true; 
                                    valueAxis.renderer.grid.template.disabled = true; 
                                    valueAxis.min = 0; 
                                    valueAxis.strictMinMax = true; 
                                    valueAxis.renderer.labels.template.adapter.add("text", function(text) { 
                                        return text ; 
                                    }); 

                                    /* Create series */
                                    var series = chart.series.push(new am4charts.ColumnSeries()); 
                                    series.dataFields.valueX = "value"; 
                                    series.dataFields.categoryY = "category"; 
                                    series.columns.template.fill = am4core.color("#23B14D"); 
                                    series.columns.template.stroke = am4core.color("#334EFF"); 
                                    series.columns.template.strokeWidth = .1; 
                                    series.columns.template.strokeOpacity = 0.5; 
                                    series.columns.template.height = am4core.percent(100); 
                                    series.columns.template.tooltipText = "[bold]Real: [/] {value}pzas [bold]Meta: [/]{target}pzas"; 

                                    var series2 = chart.series.push(new am4charts.LineSeries()); 
                                    series2.dataFields.valueX = "Meta"; 
                                    series2.dataFields.categoryY = "category"; 
                                    series2.strokeWidth = 4; 

                                    var bullet = series2.bullets.push(new am4charts.Bullet()); 
                                    var line = bullet.createChild(am4core.Line); 
                                    line.x1 = 0; 
                                    line.y1 = -200;
                                    line.x2 = 0;
                                    line.y2 = 200;
                                    line.stroke = am4core.color("#ff0f00");
                                    line.strokeWidth = 4; 
                                </script> 
                            </div> 
                        </td> 
                        </td> 
                        <td class="tdH" align='center' style="width:2.5%" ><?php echo $periodOEEH[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width:2.4%" ><?php echo $OEEAcumH[$i];?> </td> 
                    </tr> 
                <?php } ?>

                <!--SEPARADOR / INIDICADOR DE SEGUNDO TURNO-->
                <tr  bgcolor=" <?php if( $OEEAcumH[23] < $targetOEE ){ echo '#cd6155'; /*color rojo*/} else {echo '#58d68d'; /*color verde*/} ?> " > 
                    <td class="tdH" colspan="2" ><b> 2 Shift </b></td> 
                    <td class="tdH" colspan="2" align="center" ><b> <?php echo $pzasAcumTarget[23]; ?> </b></td> 
                    <td class="tdH" colspan="2" align="center" ><b> <?php echo $acumuladoPzasProdH[23]; ?> </b></td> 
                    <td class="tdH" >  </td> 
                    <td class="tdH" ><b> <?php echo $periodOEEH[23].'%'; ?> </td> 
                    <td class="tdH" ><b> <?php echo $OEEAcumH[23].'%';  ?> </td> 
                </tr> 
                <?php  ?>
                <!-- TERCER TURNO -->

                <?php
                if ($OEEAcumH[5] > 0){ 
                    for ($i = 0; $i < 6; $i++){ ?> 
                    <tr style= "background: <?php  if($periodOEEH[$i] == 0 ){ echo '#F2F3F4'; } ?> ;" > 
                        <td class="tdH" style="width:2.45%"> <?php echo $i,'-',$i+1; ?> </td> 
                        <td class="tdH" align='center' style="width:2.2%" > <?php echo $minT[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width: 2.3%;  background: #cacfd2 " > <?php echo $pzasTargetH[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width:2.3%" > <?php echo $pzasAcumTarget[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width: 2.3%; background: <?php if( ($pzasProdH[$i]*100)/$pzasTargetH[$i] > $targetOEE ){ echo '#58d68d'; 
                                                } else { 
                                                    if ($pzasProdH[$i] == 0){ echo '#B3B6B7'; } else { 
                                                        if ( $pzasTargetRealH[$i] > $pzasProdH[$i] ){ echo '#cd6155'; 
                                                        } else { echo '#f7dc6f';  } 
                                                    } 
                                                } ?>; 
                                                color: black;" > 
                                            <?php 
                                                echo $pzasProdH[$i];
                                            ?> </td> 
                        <td align='center' style="width:2.29%" > <?php echo $acumuladoPzasProdH[$i];?> </td> 
                        <td class="tdH" style="width: 8.51%" > 
                            <div id="<?php echo "chartdiv".$i; ?>" style="height: 40px; margin-top: -15px;" > 
                                <script> 
                                    // Themes begin
                                    am4core.useTheme(am4themes_animated); 
                                    // Themes end
                                    var container = am4core.create("<?php echo "chartdiv".$i; ?>", am4core.Container); 
                                    container.width = am4core.percent(100); 
                                    container.height = am4core.percent(200); 
                                    container.layout = "vertical"; 
                                    container.hideCredits = true; 

                                    /* Create chart instance */
                                    var chart = container.createChild(am4charts.XYChart); 
                                    chart.paddingRight = 10; 

                                    /* Add data */
                                    chart.data = [{ 
                                        "category": "Evaluation", 
                                        "value": <?php echo $pzasProdH[$i]; ?>, 
                                        "Meta": <?php echo $pzasTargetH[$i]; ?> 
                                    }]; 

                                    /* Create axes */
                                    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis()); 
                                    categoryAxis.renderer.labels.template.disabled = true; 
                                    categoryAxis.dataFields.category = "category"; 
                                    categoryAxis.renderer.minGridDistance = 10; 
                                    categoryAxis.renderer.grid.template.disabled = true; 

                                    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
                                    valueAxis.renderer.minGridDistance = 10; 
                                    valueAxis.renderer.labels.template.disabled = true; 
                                    valueAxis.renderer.grid.template.disabled = true; 
                                    valueAxis.min = 0; 
                                    valueAxis.strictMinMax = true; 
                                    valueAxis.renderer.labels.template.adapter.add("text", function(text) { 
                                        return text ; 
                                    }); 

                                    /* Create series */
                                    var series = chart.series.push(new am4charts.ColumnSeries()); 
                                    series.dataFields.valueX = "value"; 
                                    series.dataFields.categoryY = "category"; 
                                    series.columns.template.fill = am4core.color("#23B14D"); 
                                    series.columns.template.stroke = am4core.color("#334EFF"); 
                                    series.columns.template.strokeWidth = .1; 
                                    series.columns.template.strokeOpacity = 0.5; 
                                    series.columns.template.height = am4core.percent(100); 
                                    series.columns.template.tooltipText = "[bold]Real: [/] {value}pzas [bold]Meta: [/]{target}pzas"; 

                                    var series2 = chart.series.push(new am4charts.LineSeries()); 
                                    series2.dataFields.valueX = "Meta"; 
                                    series2.dataFields.categoryY = "category"; 
                                    series2.strokeWidth = 4; 

                                    var bullet = series2.bullets.push(new am4charts.Bullet()); 
                                    var line = bullet.createChild(am4core.Line); 
                                    line.x1 = 0; 
                                    line.y1 = -200;
                                    line.x2 = 0;
                                    line.y2 = 200;
                                    line.stroke = am4core.color("#ff0f00");
                                    line.strokeWidth = 4; 
                                </script> 
                            </div> 
                        </td>                             
                        </td> 
                        <td class="tdH" align='center' style="width:2.5%" ><?php echo $periodOEEH[$i]; ?> </td> 
                        <td class="tdH" align='center' style="width:2.4%" ><?php echo $OEEAcumH[$i];?> </td> 
                    </tr> 
                <?php } ?> 

                <!--SEPARADOR / INIDICADOR DE TERCER TURNO-->
                <tr  bgcolor=" <?php if( $OEEAcumH[5] < $targetOEE ){ echo '#cd6155'; /*color rojo*/} else {echo '#58d68d'; /*color verde*/} ?> " > 
                    <td class="tdH" colspan="2" ><b> 3 Shift </b></td> 
                    <td class="tdH" colspan="2" align="center" ><b> <?php echo $pzasAcumTarget[5]; ?> </b></td> 
                    <td class="tdH" colspan="2" align="center" ><b> <?php echo $pzasAcumTargetRealH[5]; ?> </b></td> 
                    <td class="tdH" colspan="2" align="center" ><b> <?php echo $acumuladoPzasProdH[5]; ?> </b></td> 
                    <td class="tdH" >  </td> 
                    <td class="tdH" >  </td> 
                    <td class="tdH" >  </td> 
                    <td class="tdH" >  </td> 
                    <td class="tdH" >  </td> 
                    <td class="tdH" >  </td> 
                    <td class="tdH" ><b> <?php echo $periodOEEH[5].'%'; ?> </td> 
                    <td class="tdH" ><b> <?php echo $OEEAcumH[5].'%';  ?> </td> 
                </tr> 
                <?php } ?> 
            </tbody>
            <tfoot width = '100%' > 
                <!--PROCENTAJES E INDICADORES TOTAlES DE HOURLY--> 
                <tr class="tdH" style= " background: <?php echo '#d5dbdb' ?>; height: 2.5vh; width: 96%" > 
                    <td class="tdH" style="width: 6.88%" colspan="2" >Daily loss percent</td> 
                    <td class="tdH" style="width: 3.6%" > <?php  echo $pFnProd,'%'; ?> </td> 
                    <td class="tdH" style="width: 6.7%" ></td> <!-- GRAFICAS-->
                    <td class="tdH" style="width: 2.2%" ></td> <!-- OEE -->
                    <td class="tdH" style="width: 2.9%" ></td> <!-- OEE CUM -->
                </tr> 

                <tr style="background: #d5dbdb; height: 2.5vh; width: 96%" > 
                    <td class="tdH" >Dayli Total</td> 
                    <td class="tdH" > <?php echo $totalPzasTarget ?>  </td> 
                    <td class="tdH" > <?php echo $totalPzasProducidas ?>  </td> 
                    <td class="tdH" > </td> 
                    <td class="tdH" ></td> 
                    <td class="tdH" ></td> 
                </tr> 
            </tfoot> 
       </table> 
    </body> 
</html> 
