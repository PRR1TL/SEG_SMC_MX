<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <?php
        include '../../../db/ServerFunctions.php'; 
        session_start(); 
        
        $line = $_SESSION['linea']; 
        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes']; 
        $fIni = date('Y-m-d', strtotime('-1 day')) ;
        $fFin = date('Y-m-d', strtotime('-1 day')) ;
                
        $dattop5 = t5TecnicasYOrganizacionales($line,$fIni,$fFin);
        for ($i = 0; $i < count($dattop5); $i++){ 
            $opTO[$i] = $dattop5[$i][0]; 
            $problemTO[$i] = $dattop5[$i][1]; 
            $durTO[$i] = $dattop5[$i][2]; 
            $frecTO[$i] = $dattop5[$i][3]; 
            $vImpTO[$i] = $dattop5[$i][2]; 
        } 

    ?>
    <br>    
        <div aling = "center" id="ptTO" style="width: 100%; height: 180px; margin-top: -40px; " > 
            <script> 
                var chart = AmCharts.makeChart("ptTO", {
                    "type": "serial",
                    "theme": "light",
                    "rotate": true,
                    "dataProvider": [ 
                    <?PHP for($i = 0 ;$i < count($dattop5);$i++){ ?> 
                        { 
                            "country": "<?php echo "$opTO[$i],$problemTO[$i]";?>", 
                            "visits": "<?php echo $vImpTO[$i]; ?>" 
                        },
                    <?php } ?>
                    ],
                    "graphs": [{
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "fontSize": 2,
                        "valueField": "visits",
                        "lineColor": "#02538b",
                        "balloonText": "<b style='font-size: 100%'>[[value]] min</b>"
                    }],                    
                    "categoryField": "country",
                    "title": "Tecnicas y organizacionales",
                    "categoryAxis": {
                        "reverse": true
                    }, "responsive": {
                        "enabled": true
                    }                     
                }); 
            </script> 
        </div>     
    <br>
    
