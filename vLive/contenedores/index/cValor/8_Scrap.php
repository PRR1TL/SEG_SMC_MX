<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../../db/principal.php'; 
    session_start();
    
    switch ($_SESSION['nivelReporte']){
        case 0: //PLANTA             
            break; 
        case 1: //PRODUCTO 
            $cIFKDay = ifk_Day_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
            $cMiscDay = miscellaneous_Day_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
            $cTargetDay = targetScrap_Day_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']) ;
            break; 
        case 2: //CADENA DE VALOR 
            $cIFKDay = ifk_Day_CValor($_SESSION['cValorL'], $_SESSION['fIni'], $_SESSION['fFin']); 
            $cMiscDay = miscellaneous_Day_CValor($_SESSION['cValorL'], $_SESSION['fIni'], $_SESSION['fFin']); 
            $cTargetDay  = targetScrap_Day_CValor($_SESSION['cValorL'], $_SESSION['fIni'], $_SESSION['fFin']); 
            break; 
    } 
    
    for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        $tDayIFK[$vDate] = 0; 
        $tDayMisc[$vDate] = 0; 
        $tCummDay[$vDate] = 0; 
        $targetD[$vDate] = 0; 
        $lblday[$vDate] = date("M d", strtotime($i)); 
        $sTotalDIFK = 0; 
        $sTotalDMisc = 0; 
    }     
    
    for($i = 0; $i < count($cIFKDay); $i++){ 
        $date = explode("-", $cIFKDay[$i][0]); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        $tDayIFK[$vDate] = $cIFKDay[$i][1]; 
        $sTotalDIFK += $tDayIFK[$vDate]; 
    } 
    
    for ($i = 0; $i < count($cMiscDay); $i++ ) { 
        $date = explode("-", $cMiscDay[$i][0]); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        $tDayMisc[$vDate] = $cMiscDay[$i][1]; 
        $sTotalDMisc += $tDayMisc[$vDate]; 
    } 
    
    # CONSULTA DE TARGETS
    for ($i = 0; $i < count($cTargetDay); $i++){
        $date = explode("-", $cTargetDay[$i][0]); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $targetD[$vDate] = $cTargetDay[$i][1];
    }
    
    for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        $tCummDay[$vDate] = $tDayMisc[$vDate] + $tDayIFK[$vDate]; 
        $diffDay[$vDate] = @round($targetD[$vDate] - $tCummDay[$vDate],2); 
    }     

?>

<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script> 

<div id="gScrap" name="gScrap" class="jidokaDay" style="width: 100%; min-height: 200px; max-height: 600px; margin-top: -12px; " > 
    <script> 
        var chart = AmCharts.makeChart("gScrap", {
            "type": "serial", 
            "theme": "none",             
            "dataProvider": [                     
                <?php for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                    $date = explode("-", $i); 
                    $vDate = $date[0].$date[1].(int)$date[2];  ?>
                {
                    "day": "<?php echo $lblday[$vDate];?> ",
                    "ifk": <?php echo $tDayIFK[$vDate]; ?>,
                    "misc": <?php echo $tDayMisc[$vDate]; ?>,
                    "meta": <?php echo $targetD[$vDate]; ?>, 
                    "cummD": <?php echo $tCummDay[$vDate]; ?>, 
                    "diffD": <?php echo $diffDay[$vDate]; ?>
                },
                <?php } ?>             
           ], 
            "valueAxes": [{ 
                "stackType": "regular", 
                "unit": "$", 
                "fontSize": 8, 
                "axisAlpha": 0.3, 
                "minimum": 0
            }], 
            "graphs": [{ 
                "fillAlphas": 1, 
                "labelText": "[[value]]", 
                "lineAlpha": 1, 
                "title": "IFK", 
                "type": "column", 
                "color": "#000000", 
                "valueField": "ifk", 
                "balloon": {
                    "enabled": false
                } 
            }, {  
                "fillAlphas": 1, 
                "labelText": "[[value]]", 
                "lineAlpha": 1, 
                "title": "Miscellaneous", 
                "type": "column", 
                "color": "#000000", 
                "valueField": "misc", 
                "balloon": {
                    "enabled": false
                } 
            }, {
                "lineThickness": 2, 
                "lineColor": false, 
                "type": "none", 
                "valueField": "cummD", 
                "enabled": false,
                "balloon": { 
                    "enabled": false 
                } 
            }, {
                "lineThickness": 2, 
                "lineColor": false, 
                "type": "none", 
                "valueField": "diffD", 
                "enabled": false,
                "balloon": { 
                    "enabled": false 
                } 
            }, {                 
                "lineThickness": 2, 
                "lineColor": "#62cf73", 
                "title": "Meta", 
                "type": "line", 
                "valueField": "meta", 
                "balloonText": "<b>[[category]]</b><br>Misc: </b><span style='font-size:100%'><b>[[misc]]</b></span><br>IFK: </b><span style='font-size:100%'><b>[[ifk]]</b><br><br></span>[[title]]: </b><span style='font-size:100%'><b>[[meta]]</b></span><br></span>Real: </b><span style='font-size:100%'><b>[[cummD]]</b></span><br></span>Diferencia: </b><span style='font-size:100%'><b>[[diffD]]</b></span>"
            }], 
            "chartCursor": {
                "pan": true, 
                "valueLineEnabled": true, 
                "valueLineBalloonEnabled": true, 
                "cursorAlpha": 0, 
                "valueLineAlpha": 0.2 
            },
            "categoryField": "day", 
            "categoryAxis": { 
                "gridAlpha": 0.2, 
                "gridPosition": "start", 
                "axisAlpha": 0, 
                "position": "left" 
            }, 
            "export": { 
                "enabled": true 
            } 
        }); 
    </script> 
</div> 









