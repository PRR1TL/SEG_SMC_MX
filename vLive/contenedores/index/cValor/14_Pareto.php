<script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
<script src="https://www.amcharts.com/lib/3/serial.js"></script> 

<?php 

    include '../../../db/principal.php'; 
    session_start(); 
    
    //PRIMER APARTADO EL ACTUAL 
    if ($_SESSION["fIni"] > $_SESSION["fFin"]){ 
        $fIni = date("Y-m-d", strtotime($_SESSION["fFin"])); 
        $fFin = date("Y-m-d", strtotime($_SESSION["fIni"])); 
    } else { 
        $fIni = date("Y-m-d", strtotime($_SESSION["fIni"])); 
        $fFin = date("Y-m-d", strtotime($_SESSION["fFin"])); 
    } 
    
    switch ($_SESSION['nivelReporte']){ 
        case 0: //PLANTA 
                  
            break; 
        case 1: //PRODUCTO 
            $dattop3 = pareto_TOP3_Producto($_SESSION['productoL'], $fIni, $fFin); 
            $dattop5 = pareto_TOP5_Producto($_SESSION['productoL'], $fIni, $fFin); 
            $dattop1 = pareto_TOP1_Producto($_SESSION['productoL'], $fIni, $fFin); 
            break; 
        case 2: //CADENA DE VALOR 
            $dattop3 = pareto_TOP3_CValor($_SESSION["cValorL"], $fIni, $fFin); 
            $dattop5 = pareto_TOP5_CValor($_SESSION["cValorL"], $fIni, $fFin); 
            $dattop1 = pareto_TOP1_CValor($_SESSION["cValorL"], $fIni, $fFin); 
            break; 
    } 
    
    for($i = 0; $i < count($dattop3); $i++){ 
        $opCali[$i] = $dattop3[$i][0].' ('.$dattop3[$i][1].')'; 
        $problemCali[$i] = $dattop3[$i][2]; 
        $dat1Cali[$i] = $dattop3[$i][3]; 
        $dat2Cali[$i] = $dattop3[$i][4]; 
    } 
        
    for ($i = 0; $i < count($dattop5); $i++) { 
        $pzasTecyOrg[$i] = 0; 
        $x = 0; 
        $opTecOrg[$i] = $dattop5[$i][0].' ('.$dattop5[$i][1].')'; 
        $problemTecOrg[$i] = $dattop5[$i][2]; 
        $dat1TecOrg[$i] = $dattop5[$i][3]; 
        $dat2TecOrg[$i] = $dattop5[$i][4]; 
        $dat3TecOrg[$i] = 0; 
    } 

    for ($i = 0; $i < count($dattop1); $i++) { 
        $pzasCam[$i] = 0; 
        $x = 0; 
        $cambio[$i] =  $dattop1[$i][0].' ('.$dattop1[$i][1].')'; 
        $dat1Cam[$i] = $dattop1[$i][2]; 
        $dat2Cam[$i] = $dattop1[$i][3]; 
        $dat3Cam[$i] = 0; 
        $seg[$x] = 0; 
    } 
        
    $tituloTop1[0] = "Top 1: Cambio de modelo (Minutos)"; 
    $tituloTop3[0] = "Top 3: NO Calidad (Piezas)"; 
    $tituloTop5[0] = "Top 5: Técnicas y organizacionales (Minutos)";                 

?> 
    
    <div class="container" style="margin-top: 1%; width: 110%; height: 150%; " > 
        <div class="row" > 
            <div class="col-lg-6 col-md-6 col-xs-6 col-sh-6" style="border-style: solid; border-width: 1px; " > 
                <h6 class="contenidoCentrado"> <strong> <?php echo $tituloTop5[0] ?> </strong></h6> 
                <div aling = "center" id="pttecorg" class="grafTop3" style="width: 90%; height: 250px" > 
                    <script> 
                        var chart = AmCharts.makeChart("pttecorg", { 
                            "type": "serial", 
                            "theme": "light", 
                            "rotate": true, 
                            "dataProvider": [ 
                            <?PHP for($i = 0 ;$i < count($dattop5);$i++){ ?> 
                                { 
                                    "country": "<?php echo "$opTecOrg[$i],$problemTecOrg[$i]";?>", 
                                    "visits": "<?php echo $dat1TecOrg[$i]; ?>" 
                                }, 
                            <?php } ?> 
                            ], 
                            "graphs": [{ 
                                "balloonText": "<span> <b>[[value]] min</b></span>", 
                                "fillAlphas": 0.9, 
                                "lineAlpha": 0.2, 
                                "type": "column", 
                                "valueField": "visits", 
                                "lineColor": "#02538b" 
                            }], 
                            "categoryField": "country", 
                            "categoryAxis": {
                                "reverse": true
                            }
                        });
                    </script>
                </div>

                
            </div> 
            <div class="col-lg-6 col-md-6 col-xs-6 col-sh-6" style="border-style: solid; border-width: 1px;"> 
                <h6 class="contenidoCentrado "><strong> <?php echo $tituloTop1[0] ?> </strong></h6> 
                <div aling = "center" id="ptCamb" class="grafTop3" style="width: 90%; height: 60px"> 
                    <script> 
                        var chart = AmCharts.makeChart("ptCamb", {
                            "type": "serial",
                            "theme": "light",
                            "rotate": true,
                            "dataProvider": [
                            <?PHP for($i = 0 ;$i < count($dattop1);$i++){?>
                                {
                                    "country": "<?php echo "$cambio[$i]";?>",
                                    "visits": "<?php echo $dat1Cam[$i]; ?>"
                                },
                            <?php } ?>
                            ], 
                            "graphs": [{ 
                                "balloonText": "<span> <b>[[value]] min</b></span>",
                                "fillAlphas": 0.9, 
                                "lineAlpha": 0.2, 
                                "type": "column", 
                                "valueField": "visits", 
                                "lineColor": "#02538b" 
                            }], 
                            "categoryField": "country", 
                            "categoryAxis": { 
                                "reverse": true
                            } 
                        }); 
                    </script> 
                </div> 
                
                <h6 class="contenidoCentrado"> <strong> <?php echo $tituloTop3[0] ?> </strong></h6> 
                <div aling = "center" id="ptCali" class="grafTop3" style="width: 90%; height: 180px; "> 
                    <script>
                        var chart = AmCharts.makeChart("ptCali", { 
                            "type": "serial",
                            "theme": "light",
                            "rotate": true,
                            "dataProvider": [
                            <?PHP for($i = 0 ; $i < count($dattop3); $i++){ ?>
                                {
                                    "country": "<?php echo "$opCali[$i], $problemCali[$i]";?>",
                                    "visits": "<?php echo $dat1Cali[$i]; ?>"
                                },
                            <?php } ?>
                            ],
                            "graphs": [{ 
                                "balloonText": "<span> <b>[[value]] min</b></span>",
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "type": "column",
                                "valueField": "visits",
                                "lineColor": "#02538b"
                            }],
                            "categoryField": "country",
                            "categoryAxis": {
                                "reverse": true
                            }
                        });
                    </script> 
                </div> 
            <br><br><br><br><br><br> 
            </div> 
        </div> 
    </div> 
    