
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

<script> 
    //CONFIGURACION DE LA TABLA 
    $(function () { 
        var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
        $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
            fixedColumnsLeft: 1, //CONTADOR Y FILA FIJOS 
            fixedColumnsRight: 0, //CABECERA FIJAS 
            columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
            scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
            scrollY: 0 //INICIO DE SCROLL LATERAL | 
        }); 
    }); 

    function getFixedColumnsData() {} 
</script>
        
<?php 
session_start();
include '../../../db/principal.php'; 

for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
    $date = explode("-", $i); 
    $vDate = $date[0].$date[1].(int)$date[2]; 
    $fecha[$vDate] = $i; 
    $cadDate[$vDate] = date("M d", strtotime($i)); 
    $vMax[$vDate] = 0; 
    $vMin[$vDate] = 0; 
    $vReal[$vDate] = 0; 
} 

$cInventario = inventarioD_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']);
for ($i = 0; $i < count($cInventario); $i++ ){
    $date = explode("-", $cInventario[$i][0]->format('Y-m-d'));
    $vDate = $date[0].$date[1].(int)$date[2]; 
    
    $vMax[$vDate] = $cInventario[$i][1];
    $vMin[$vDate] = $cInventario[$i][2]; 
    $vReal[$vDate] = $cInventario[$i][3];  
} 

//DATOS PARA TAMAÑO DE TABLA  
$dias = (strtotime($_SESSION['fIni'])- strtotime($_SESSION['fFin']))/86400;
$dias = abs($dias); 
$dias = floor($dias); 
//DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
//NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
if ($dias > 11 ) { 
    $rowspan = 12; 
} else { 
    $rowspan = $dias+1;
} 

?> 

<style> 
    #chartdiv { 
        width: 100%; 
        height: 25vh; 
        margin-top: -30px; 
    } 
    
</style>

<div id="chartdiv" > 
    <script>
        var chart = AmCharts.makeChart("chartdiv", { 
            "type": "serial", 
            "theme": "none", 
            "dataDateFormat": "YYYY-MM-DD",
            "precision": 0, 
            "valueAxes": [{ 
                "id": "v1", 
                "fontSize": 8, 
                "unit": "pzs",
                "position": "left",
                "autoGridCount": false
            }],
            "graphs": [{ 
                "valueAxis": "v1", 
                "lineColor": "#62cf73", 
                "fillColors": "#62cf73", 
                "fillAlphas": 1,
                "type": "column",
                "title": "Real",
                "valueField": "real",
                "clustered": false,
                "columnWidth": 0.3,
                "legendValueText": "[[value]]",
                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
            }, { 
                "valueAxis": "v2", 
                "lineThickness": 2, 
                "lineColor": "#731F35", 
                "type": "smoothedLine", 
                "dashLength": 1, 
                "title": "Max", 
                "useLineColorForBulletBorder": true, 
                "valueField": "max", 
                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>" 
            }, {           
                "lineThickness": 2,
                "lineColor": "#0D0D0D",
                "type": "smoothedLine",
                "dashLength": 1,
                "title": "Min",
                "valueField": "min",
                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
            }], 
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 0,
                "valueLineAlpha": 0.2
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
            },          
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
            },
            "export": {
                "enabled": true
            },
            "dataProvider": [
                <?php for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){        
                      $date = explode("-", $i); 
                      $vDate = $date[0].$date[1].(int)$date[2]; ?>  
               {
                    "date": "<?php echo $fecha[$vDate] ?>",
                    "max": <?php echo $vMax[$vDate] ?>,
                    "min": <?php echo $vMin[$vDate] ?>,
                    "real": <?php echo $vReal[$vDate] ?>
                }, <?php } ?>  
            ]
        });    
    </script>    
</div> 

