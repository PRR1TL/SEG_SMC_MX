<HTML>
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">-->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <?php 
        include '../../../db/principal.php'; 
        session_start();         
        
        //echo "<br>",$_SESSION['nivelReporte'], ', ', $_SESSION['productoL'] ;
        switch ($_SESSION['nivelReporte']) { 
            case 0 : 
                break; 
            case 1: //PRODUCTO 
                if ($_SESSION['productoL'] == 'GE' ){ 
                    $rSubEnsambles = 3; 
                    $titulo[0] = 'ROTOR'; 
                    $titulo[1] = 'ESTATOR'; 
                    $titulo[2] = 'RECTIFICADOR'; 
                    
                    //INCIALIZAMOS LAS VARIABLES
                    for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                        for ($j = 0; $j < $rSubEnsambles; $j++){
                            $date = explode("-", $i); 
                            $vDate = $date[0].$date[1].(int)$date[2]; 

                            $fecha[$j][$vDate] = $i;
                            $valueDayT[$j][$vDate] = 0;
                            $valuePlanDayT[$j][$vDate] = 0;
                            $cumRealDay[$j][$vDate] = 0;
                            $cumMetaDay[$j][$vDate] = 0;
                            $targetDayT[$j][$vDate] = 80; 
                        } 
                    }                     

                    #ROTOR 
                    $cRotor = pzas_ROTOR_INDUCIDO_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cRotor); $i++ ){ 
                        $date = explode("-", $cRotor[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valueDayT[0][$vDate] = $cRotor[$i][1]; 
                    }
                    
                    $cMtaRotor = pzasMta_ROTOR_INDUCIDO_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cMtaRotor); $i++ ){ 
                        $date = explode("-", $cMtaRotor[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valuePlanDayT[0][$vDate] = $cMtaRotor[$i][1]; 
                    } 
                    
                    #ESTATOR 
                    $cEstator = pzas_ESTATOR_SOLENOIDE_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cEstator); $i++ ){ 
                        $date = explode("-", $cEstator[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valueDayT[1][$vDate] = $cRotor[$i][1];
                    } 
                    
                    $cMtaEstator = pzasMta_ESTATOR_SOLENOIDE_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cMtaEstator); $i++ ){ 
                        $date = explode("-", $cMtaEstator[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valuePlanDayT[1][$vDate] = $cMtaEstator[$i][1]; 
                    }                     
                    
                    #RECTIFICADOR 
                    $cRectificador = pzas_RECTIFICADOR_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cRectificador); $i++ ){
                        $date = explode("-", $cRectificador[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valueDayT[2][$vDate] = $cRotor[$i][1];
                    } 
                    
                    $cMtaRectificador = pzasMta_RECTIFICADOR_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cMtaRectificador); $i++ ){ 
                        $date = explode("-", $cMtaRectificador[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valuePlanDayT[2][$vDate] = $cMtaRectificador[$i][1]; 
                    } 
                    
                    #TARGET 

                    #ACULULADO
                    for ($j = 0; $j < $rSubEnsambles; $j++ ){ 
                        $aux = 1; 
                        $auxCumReal[$j] = 0; 
                        $auxCumMeta[$j] = 0; 
                        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                            $date = explode("-", $i); 
                            $vDate = $date[0].$date[1].(int)$date[2]; 

                            #APARTADO PARA ACUMULADO REAL
                            if ($date[2] != $aux ){ 
                                #SE HACE EL ACUMULADO DE PIEZAS 
                                $cumRealDay[$j][$vDate] = $auxCumReal[$j] + $valueDayT[$j][$vDate]; 
                                $cumMetaDay[$j][$vDate] = $auxCumMeta[$j] + $valuePlanDayT[$j][$vDate]; 

                                $auxCumReal[$j] += $valueDayT[$j][$vDate]; 
                                $auxCumMeta[$j] += $valuePlanDayT[$j][$vDate];                
                            } else {                 
                                #SE REINICIA EL CONTEO, YA QUE ES POR MES
                                $cumRealDay[$j][$vDate] = $valueDayT[$j][$vDate]; 
                                $cumMetaDay[$j][$vDate] = $valuePlanDayT[$j][$vDate]; 

                                $auxCumReal[$j] = $valueDayT[$j][$vDate];
                                $auxCumMeta[$j] = $valuePlanDayT[$j][$vDate]; 
                            } 
                        }  
                    }
                    
                } else { 
                    $rSubEnsambles = 2;
                    $titulo[0] = 'INDUCIDO'; 
                    $titulo[1] = 'SOLENOIDE';
                    
                    //INCIALIZAMOS LAS VARIABLES
                    for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                        for ($j = 0; $j < $rSubEnsambles; $j++){
                            $date = explode("-", $i); 
                            $vDate = $date[0].$date[1].(int)$date[2]; 

                            $fecha[$j][$vDate] = $i;
                            $valueDayT[$j][$vDate] = 0;
                            $valuePlanDayT[$j][$vDate] = 0;
                            $cumRealDay[$j][$vDate] = 0;
                            $cumMetaDay[$j][$vDate] = 0;
                            $targetDayT[$j][$vDate] = 80; 
                        } 
                    }                     

                    #ROTOR 
                    $cRotor = pzas_ROTOR_INDUCIDO_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cRotor); $i++ ){ 
                        $date = explode("-", $cRotor[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valueDayT[0][$vDate] = $cRotor[$i][1]; 
                    }
                    
                    $cMtaRotor = pzasMta_ROTOR_INDUCIDO_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cMtaRotor); $i++ ){ 
                        $date = explode("-", $cMtaRotor[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valuePlanDayT[0][$vDate] = $cMtaRotor[$i][1]; 
                    } 
                    
                    #ESTATOR 
                    $cEstator = pzas_ESTATOR_SOLENOIDE_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cEstator); $i++ ){ 
                        $date = explode("-", $cEstator[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valueDayT[1][$vDate] = $cRotor[$i][1];
                    } 
                    
                    $cMtaEstator = pzasMta_ESTATOR_SOLENOIDE_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                    for ($i = 0; $i < count($cMtaEstator); $i++ ){ 
                        $date = explode("-", $cMtaEstator[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valuePlanDayT[1][$vDate] = $cMtaEstator[$i][1]; 
                    }                     
                    
                    #ACULULADO
                    for ($j = 0; $j < $rSubEnsambles; $j++ ){ 
                        $aux = 1; 
                        $auxCumReal[$j] = 0; 
                        $auxCumMeta[$j] = 0; 
                        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                            $date = explode("-", $i); 
                            $vDate = $date[0].$date[1].(int)$date[2]; 

                            #APARTADO PARA ACUMULADO REAL
                            if ($date[2] != $aux ){ 
                                #SE HACE EL ACUMULADO DE PIEZAS 
                                $cumRealDay[$j][$vDate] = $auxCumReal[$j] + $valueDayT[$j][$vDate]; 
                                $cumMetaDay[$j][$vDate] = $auxCumMeta[$j] + $valuePlanDayT[$j][$vDate]; 

                                $auxCumReal[$j] += $valueDayT[$j][$vDate]; 
                                $auxCumMeta[$j] += $valuePlanDayT[$j][$vDate];                
                            } else {                 
                                #SE REINICIA EL CONTEO, YA QUE ES POR MES
                                $cumRealDay[$j][$vDate] = $valueDayT[$j][$vDate]; 
                                $cumMetaDay[$j][$vDate] = $valuePlanDayT[$j][$vDate]; 

                                $auxCumReal[$j] = $valueDayT[$j][$vDate];
                                $auxCumMeta[$j] = $valuePlanDayT[$j][$vDate]; 
                            } 
                        }  
                    }
                } 
                break; 
            case 2: 
                $subEnsambles = listar_SubEnsable_CadVal($_SESSION['cValorL']); 
                $rSubEnsambles = count($subEnsambles);
                //TABLA
                $countProblem = 0;
                $problema[0] = ""; 

                //INCIALIZAMOS LAS VARIABLES
                for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                    for ($j = 0; $j < count($subEnsambles); $j++){
                        $date = explode("-", $i); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 

                        $fecha[$j][$vDate] = $i;
                        $valueDayT[$j][$vDate] = 0;
                        $valuePlanDayT[$j][$vDate] = 0;
                        $cumRealDay[$j][$vDate] = 0;
                        $cumMetaDay[$j][$vDate] = 0;
                        $targetDayT[$j][$vDate] = 80; 
                    } 
                } 

                for ($j = 0; $j < count($subEnsambles); $j++){
                    $jTDay = entregas_Sub_CadValor($subEnsambles[$j][0], $_SESSION['fIni'], $_SESSION['fFin']); 
                    $titulo[$j] = $subEnsambles[$j][1];
                    $aux = 1;
                    $auxCumReal[$j] = 0;
                    $auxCumMeta[$j] = 0; 
                    
                    for ($i = 0; $i < count($jTDay); $i++ ) { 
                        $date = explode("-", $jTDay[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valueDayT[$j][$vDate] = $jTDay[$i][1]; 
                    } 
                    
                    #TARGET 
                    $cTargetDay = metaPzas_Sub_CadValor($subEnsambles[$j][0], $_SESSION['fIni'], $_SESSION['fFin']);
                    for ($i = 0; $i < count($cTargetDay); $i++){
                        $date = explode("-", $cTargetDay[$i][0]); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                        $valuePlanDayT[$j][$vDate] = (int) $cTargetDay[$i][1]; 
                    } 

                    #HACER EL ACUMULADO TANTO DE REALES COMO DE PLANEADAS 
                    for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                        $date = explode("-", $i); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 

                        #APARTADO PARA ACUMULADO REAL
                        if ($date[2] != $aux ){ 
                            #SE HACE EL ACUMULADO DE PIEZAS 
                            $cumRealDay[$j][$vDate] = $auxCumReal[$j] + $valueDayT[$j][$vDate]; 
                            $cumMetaDay[$j][$vDate] = $auxCumMeta[$j] + $valuePlanDayT[$j][$vDate]; 

                            $auxCumReal[$j] += $valueDayT[$j][$vDate]; 
                            $auxCumMeta[$j] += $valuePlanDayT[$j][$vDate];                
                        } else {                 
                            #SE REINICIA EL CONTEO, YA QUE ES POR MES
                            $cumRealDay[$j][$vDate] = $valueDayT[$j][$vDate]; 
                            $cumMetaDay[$j][$vDate] = $valuePlanDayT[$j][$vDate]; 

                            $auxCumReal[$j] = $valueDayT[$j][$vDate];
                            $auxCumMeta[$j] = $valuePlanDayT[$j][$vDate]; 
                        } 
                    }              
                }
                
                break; 
        } 
        
    ?>
    
   
    
        <body>
            <div >             
                <?php for ($j = 0; $j < $rSubEnsambles; $j++){ ?>
                <div id="<?php echo "sE".$j; ?>" class="jidokaDay" style=" width: 100%; min-height: 100px; max-height: 300px; margin-top: -20px;" > 
                    <script> 
                        var chart = AmCharts.makeChart("<?php echo "sE".$j; ?>", { 
                            "type": "serial",
                            "theme": "light",
                            "titles":[{"text":"<?php echo $titulo[$j]; ?>", "marginTop": 150}],
                            "dataDateFormat": "YYYY-MM-DD",
                            "precision": 0,
                            "valueAxes": [{ 
                                "id": "v1", 
                                "fontSize": 8, 
                                "unit": "pzs",
                                "position": "left", 
                                "autoGridCount": false 
                            }, { 
                                "id": "v2", 
                                "fontSize": 8, 
                                "unit": "pzs",
                                "gridAlpha": 0, 
                                "position": "right", 
                                "autoGridCount": false 
                            }],
                            "graphs": [{ 
                                "id": "g3", 
                                "valueAxis": "v1", 
                                "lineColor": "#e1ede9", 
                                "fillColors": "#e1ede9", 
                                "fillAlphas": 1, 
                                "type": "column", 
                                "title": "Piezas Esperadas", 
                                "valueField": "sales2", 
                                "clustered": false, 
                                "columnWidth": 0.5, 
                                "legendValueText": "[[value]]", 
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>" 
                            }, { 
                                "id": "g4", 
                                "valueAxis": "v1", 
                                "lineColor": "#02538b", 
                                "fillColors": "#02538b", 
                                "fillAlphas": 1, 
                                "type": "column", 
                                "title": "Piezas Reales", 
                                "valueField": "sales1", 
                                "clustered": false,
                                "columnWidth": 0.3,
                                "legendValueText": "[[value]]",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }, {
                                "id": "g1",
                                "valueAxis": "v2",                                 
                                "lineThickness": 2,
                                "lineColor": "#bc2727",
                                "type": "smoothedLine",
                                "dashLength": 5,
                                "title": "Cumm Plan",
                                "useLineColorForBulletBorder": true,
                                "valueField": "cumPlan",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            },{
                                "id": "g2",
                                "valueAxis": "v2",
                                "lineThickness": 2,
                                "lineColor": "#000000",
                                "type": "smoothedLine",
                                "dashLength": 5,
                                "title": "Cumm Real",
                                "useLineColorForBulletBorder": true,
                                "valueField": "cumReal",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],
                            "chartCursor": {
                                "pan": true,
                                "valueLineEnabled": true,
                                "valueLineBalloonEnabled": true,
                                "cursorAlpha": 0,
                                "valueLineAlpha": 0.2
                            },
                            "categoryField": "date",
                            "categoryAxis": {
                                "parseDates": true,
                                "dashLength": 1,
                                "minorGridEnabled": true
                            },                        
                            "balloon": {
                                "borderThickness": 1,
                                "shadowAlpha": 0
                            },
                            "dataProvider": [
                            <?php  
                                for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                            ?> {
                                "date": "<?php echo $fecha[$j][$vDate] ?>",
                                "cumReal": <?php echo $cumRealDay[$j][$vDate] ?>,
                                "cumPlan": <?php echo $cumMetaDay[$j][$vDate] ?>,
                                "sales1": <?php echo $valueDayT[$j][$vDate]; ?>,
                                "sales2": <?php echo $valuePlanDayT[$j][$vDate]; ?>
                            }, 
                            <?php } ?>        
                            ]
                        });
                    </script> 
                </div>             
                <?php } ?>
            </div>
     
    
    