<script src="//www.amcharts.com/lib/3/amcharts.js"></script>
<script src="//www.amcharts.com/lib/3/serial.js"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js"></script>
    
<?php

/* 
 *  To change this license header, choose License Headers in Project Properties.
 *  To change this template file, choose Tools | Templates
 *  and open the template in the editor.
 */

    session_start();
    include '../../../db/principal.php'; 
   
    switch ($_SESSION['nivelReporte']){
        case 0: //PLANTA
            
            break; 
        case 1: //PRODUCTO 
            $oeeProductionDay = oeeDia_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
            $ctargetDay = targetOEEDia_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
            break; 
        case 2: //CADENA DE VALOR 
            $oeeProductionDay = oeeDia_CValor($_SESSION['cValorL'], $_SESSION['fIni'], $_SESSION['fFin']); 
            $ctargetDay = targetOEEDia_CValor($_SESSION['cValorL'], $_SESSION['fIni'], $_SESSION['fFin']); 
            break;
    } 
    
    #INCIALIZACION DE VARIABLES PARA LAS FECHAS 
    for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $fecha[$vDate] = $i; 
        $prodDay[$vDate] = 0; 
        $caliDay[$vDate] = 0; 
        $orgDay[$vDate] = 0; 
        $tecDay[$vDate] = 0; 
        $cambioDay[$vDate] = 0; 
        $desDay[$vDate] = 0; 
        $targetDay[$vDate] = 0; 
        $pTotalDay[$vDate] = 0; 
    } 
    # CONSULTA PARA DIARIO
    for($i = 0; $i < count($oeeProductionDay); $i++) { 
        $date = explode("-", $oeeProductionDay[$i][0],3); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $prodDay[$vDate] = @round($oeeProductionDay[$i][1],2); 
        $caliDay[$vDate] = @round($oeeProductionDay[$i][4],2); 
        $orgDay[$vDate] = @round($oeeProductionDay[$i][3],2); 
        $tecDay[$vDate] = @round($oeeProductionDay[$i][2],2); 
        $cambioDay[$vDate] = @round($oeeProductionDay[$i][5],2); 
        $desDay[$vDate] = @round($oeeProductionDay[$i][6],2); 
        
        //DATOS PARA LA TABLA 
        $pTopic[1][$vDate] = @round($oeeProductionDay[$i][1],2); 
        $pTopic[2][$vDate] = @round($oeeProductionDay[$i][4],2); 
        $pTopic[3][$vDate] = @round($oeeProductionDay[$i][3],2);
        $pTopic[4][$vDate] = @round($oeeProductionDay[$i][2],2); 
        $pTopic[5][$vDate] = @round($oeeProductionDay[$i][5],2); 
        $pTopic[6][$vDate] = @round($oeeProductionDay[$i][6],2); 
        $pTotalDay[$vDate] = @round($pTopic[1][$vDate]+$pTopic[2][$vDate]+$pTopic[3][$vDate]+$pTopic[4][$vDate]+$pTopic[5][$vDate]+$pTopic[6][$vDate],1);
    } 
    
    #CONSULTA DE TARGET 
    
    for ($i = 0; $i < count($ctargetDay); $i++) { 
        $date = explode("-", $ctargetDay[$i][0],3); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $targetDay[$vDate] = $ctargetDay[$i][1]; 
    } 
    
?> 
<style> 
    #oee { 
        width: 100%; 
        margin-left: -15px;
        min-height: 200px; 
        max-height: 600px;
        margin-top: -12px;
    } 
</style>

<div > 
    <div id="oee" > 
        <script> 
            var chart = AmCharts.makeChart("oee", { 
            "type": "serial",
            "theme": "light",
            "dataDateFormat": "YYYY-MM-DD",
            "precision": 2, 
            "dataProvider": [
                <?php for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                        $date = explode("-", $i); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?> 
                { 
                    "date": "<?php echo $fecha[$vDate]; ?>", 
                    "prod": <?php echo $prodDay[$vDate]; ?>, 
                    "calidad": <?php echo $caliDay[$vDate]; ?>, 
                    "org": <?php echo $orgDay[$vDate]; ?>, 
                    "tec": <?php echo $tecDay[$vDate]; ?>, 
                    "cambio": <?php echo $cambioDay[$vDate]; ?>, 
                    "desempenio": <?php echo $desDay[$vDate]; ?>, 
                    "Meta": <?php echo $targetDay[$vDate]; ?> 
                }, 
                <?php } ?> 
            ],
            "valueAxes": [{ 
                "maximum": 100,
                "minimum": 0,
                "unit": "%",
                "fontSize": 8, 
                "stackType": "regular", 
                "axisAlpha": 0.3, 
                "gridAlpha": 0.2 
            }], 
            "graphs": [{ 
                "balloonText": "<b>[[title]]: </b><span style='font-size:12px'><b>[[value]]</b></span>",
                "fillAlphas": 1, 
                "fillColors": "#62cf73", 
                "lineColor": "#62cf73", 
                "labelText": "[[value]]", 
                "lineAlpha": 1, 
                "title": "OEE", 
                "type": "column", 
                "color": "#FFFF", 
                "valueField": "prod" 
            }, { 
                "balloonText": "<b>[[title]]: </b><span style='font-size:12px'><b>[[value]]</b></span>",
                "fillAlphas": 1, 
                "fillColors": "#311B92", 
                "lineColor": "#311B92", 
                "labelText": "[[value]]", 
                "lineAlpha": 1, 
                "title": "Técnicas", 
                "type": "column", 
                "color": "#FFFF", 
                "valueField": "tec" 
            }, { 
                "balloonText": "<b>Org: </b><span style='font-size:12px'><b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#F06292", 
                "lineColor": "#F06292C", 
                "labelText": "[[value]]", 
                "lineAlpha": 1, 
                "title": "Organizacionales", 
                "type": "column", 
                "color": "#FFFF", 
                "valueField": "org" 
            }, {
                "balloonText": "<b>[[title]]: </b><span style='font-size:12px'><b>[[value]]</b></span>",
                "fillAlphas": 1, 
                "fillColors": "#B71C1C", 
                "lineColor": "#B71C1C", 
                "labelText": "[[value]]", 
                "lineAlpha": 1, 
                "title": "Calidad", 
                "type": "column", 
                "color": "#FFFF", 
                "valueField": "calidad" 
            }, {
                "balloonText": "<b>[[title]]: </b><span style='font-size:12px'><b>[[value]]</b></span>",
                "fillAlphas": 1, 
                "fillColors": "#3498db", 
                "lineColor": "#3498db", 
                "labelText": "[[value]]", 
                "lineAlpha": 1, 
                "title": "Cambios", 
                "type": "column", 
                "color": "#FFFFFF", 
                "valueField": "cambio", 
                "balloon": {
                    "enabled": false
                } 
            }, { 
                "fillAlphas": 1, 
                "fillColors": "#9E9E9E", 
                "lineColor": "#9E9E9E", 
                "lineAlpha": 1, 
                "type": "column", 
                "color": "#FFFFFF", 
                "valueField": "desempenio", 
                "balloon": {
                    "enabled": false
                } 
            }, { 
                "id": "graph2", 
                "balloonText": "<b>Meta: </b><span style='font-size:12px'><b>[[value]]</b></span>",
                "lineThickness": 2,                 
                "lineColor": "#000000",              
                "fillAlphas": 0, 
                "lineAlpha": 2, 
                "title": "Meta", 
                "valueField": "Meta", 
                "dashLengthField": "dashLengthLine" 
            }], 
            "balloon": {
                "adjustBorderColor": true,
                "color": "#000000",
                "cornerRadius": 5,
                "fillColor": "#FFFFFF"
            }, 
            "chartCursor": { 
                "pan": true, 
                "valueLineEnabled": true, 
                "valueLineBalloonEnabled": true, 
                "cursorAlpha": 0, 
                "valueLineAlpha": 0.2 
            }, 
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true, 
                "dashLength": 1, 
                "minorGridEnabled": true 
            } 
        });
        </script>
    </div>
</div>
