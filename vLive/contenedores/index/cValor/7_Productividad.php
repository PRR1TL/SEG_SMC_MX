<HTML>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>    
    
    <?php 
        include '../../../db/principal.php'; 
        session_start(); 
        $date = new DateTime; 
        
        switch ($_SESSION['nivelReporte']){
            case 0: // PLANTA

                break; 
            case 1: // PRODUCTO 
                $productivityPzasDay = prodD_PzasMin_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                $datHombresDia = prodD_Hombres_Turnos_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']);                
                $cTargetDay = prodD_Target_Producto($_SESSION['productoL'], $_SESSION['fIni'], $_SESSION['fFin']);
                break;             
            case 2: // 
                $productivityPzasDay = prodD_PzasMin_CValor($_SESSION['cValorL'], $_SESSION['fIni'], $_SESSION['fFin']); 
                $datHombresDia = prodD_Hombres_Turnos_CValor($_SESSION['cValorL'], $_SESSION['fIni'], $_SESSION['fFin']);                
                $cTargetDay = prodD_Target_CValor($_SESSION['cValorL'], $_SESSION['fIni'], $_SESSION['fFin']);
                break;
        } 
        
        /**************************** DIARIA *********************************/
        # PIEZAS Y MINUTOS REPORTADOS
        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $fecha[$vDate] = $i;
            $dateT[$vDate] = date("M, d",strtotime($i));
            $pzasD[$vDate] = 0; 
            $minD[$vDate] = 0; 
            $hrsHombreD[$vDate] = 0; 
            $hrsD[$vDate] = 0; 
            $mensD[$vDate] = 0; 
            $turnosD[$vDate] = 0; 
            $productivityDay[$vDate] = 0; 
            $targetDay[$vDate] = 0;
        }
        
        for($i = 0; $i < count($productivityPzasDay); $i++){
            $date = explode("-", $productivityPzasDay[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $pzasD[$vDate] = $productivityPzasDay[$i][1];
            $minD[$vDate] = $productivityPzasDay[$i][2];
            $hrsD[$vDate] = $minD[$vDate]/60; 
        }
        
        # HOMBRES DIA 
        for ($i = 0; $i < count($datHombresDia); $i++ ){
            $date = explode("-", $datHombresDia[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $mensD[$vDate] = $datHombresDia[$i][1];
            $turnosD[$vDate] = $datHombresDia[$i][2]; 
        }        
        
        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $hrsHombreD[$vDate] = @(($mensD[$vDate]*$hrsD[$vDate])/$turnosD[$vDate]); 
            $productivityDay[$vDate] = @round(($pzasD[$vDate]/ $hrsHombreD[$vDate]),2); 
        }
        
        # TARGET         
        for ($i = 0; $i < count($cTargetDay); $i++){
            $date = explode("-", $cTargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2];             
            $targetDay[$vDate] = $cTargetDay[$i][1];
        }
        
        
        $dias = (strtotime($_SESSION['fIni'])- strtotime($_SESSION['fFin']))/86400;
        
    ?>

    <div id="dProductividad" class="jidokaDay" style="width: 102%; min-height: 200px; max-height: 600px; margin-top: -12px;" >
        <script>
            var chart = AmCharts.makeChart("dProductividad", {
                "type": "serial",
                "theme": "light",
                "dataDateFormat": "YYYY-MM-DD",
                "precision": 1,
                "dataProvider": [
                <?php  
                    for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                        $date = explode("-", $i);                                 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                ?>    
                {
                    "date": "<?php echo "$fecha[$vDate]" ?>",                            
                    "production": <?php echo $productivityDay[$vDate]; ?>,
                    "meta": "<?php echo $targetDay[$vDate]; ?>"                            
                },
                <?php } ?>
                ],
                "valueAxes": [{
                    "id": "v1",
                    "position": "left",
                    "autoGridCount": false,
                    "labelFunction": function(value) {
                        return Math.round(value);
                    } 
                }],
                "graphs": [{
                    "valueAxis": "v1",
                    "fillAlphas": 1,                    
                    "lineThickness": 2,
                    "lineColor": "#02538b",
                    "fillColors": "#02538b",
                    "type": "column",
                    "title": "Productividad",
                    "useLineColorForBulletBorder": true,
                    "valueField": "production",
                    "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                }, {
                    "valueAxis": "v2",                     
                    "lineThickness": 2,
                    "lineColor": "#62cf73",
                    "type": "smoothedLine",
                    "title": "Meta",
                    "useLineColorForBulletBorder": true,
                    "valueField": "meta",
                    "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                }], 
                "chartCursor": {
                    "pan": true,
                    "valueLineEnabled": true,
                    "valueLineBalloonEnabled": true,
                    "cursorAlpha": 0,
                    "valueLineAlpha": 0.2
                },
                "categoryField": "date",
                "categoryAxis": {
                    "parseDates": true,
                    "dashLength": 1,
                    "minorGridEnabled": true
                },
                "balloon": {
                    "borderThickness": 1,
                    "shadowAlpha": 0
                },
                "panelsSettings": {
                  "usePrefixes": true
                },                        
                "export": {
                    "enabled": false
                } 
            });
        </script>
        <br><br><br>
    </div>
    
    

