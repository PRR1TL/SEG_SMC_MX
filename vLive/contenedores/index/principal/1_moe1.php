<HTML>
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">-->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <?php 
        include '../../../db/principal.php'; 
        session_start(); 
        
        $dN = date("Y-m"); 
        $ultimoDiaMes = date("t",mktime(0,0,0, date("m"),1, date("Y")));        
        //INCIALIZAMOS LAS VARIABLES 
        $countProblem = 0;
        $problema[0] = "";
        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) {  
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0;
            $valuePlanDayT[$vDate] = 0;
            $cumRealDay[$vDate] = 0;
            $cumMetaDay[$vDate] = 0;
            $targetDayT[$vDate] = 80; 
        } 
        
        //DIA
        $aux = 1;
        $auxCumReal = 0;
        $auxCumMeta = 0;
        
        #REALES REPORTADAS
        $jTDay = pzasEFProducto("GE", $_SESSION['fIni'], $_SESSION['fFin']); 
        
        for ($i = 0; $i < count($jTDay); $i++ ) { 
            $date = explode("-", $jTDay[$i][0],3); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $valueDayT[$vDate] = $jTDay[$i][1]; 
        }          
        
        #TARGET 
        $cTargetDay = mtaPzasEFProducto("GE", $_SESSION['fIni'], $_SESSION['fFin']);
        for ($i = 0; $i < count($cTargetDay); $i++){             
            $date = explode("-", $cTargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2];             
            $valuePlanDayT[$vDate] = (int) $cTargetDay[$i][1]; 
            //echo "<br>", $cTargetDay[$i][0]; 
        } 
        
        #HACER EL ACUMULADO TANTO DE REALES COMO DE PLANEADAS 
        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            
            #APARTADO PARA ACUMULADO REAL
            if ($date[2] != $aux ){ 
                #SE HACE EL ACUMULADO DE PIEZAS 
                $cumRealDay[$vDate] = $auxCumReal + $valueDayT[$vDate]; 
                $cumMetaDay[$vDate] = $auxCumMeta + $valuePlanDayT[$vDate]; 
                
                $auxCumReal += $valueDayT[$vDate]; 
                $auxCumMeta += $valuePlanDayT[$vDate];                
            } else {                 
                #SE REINICIA EL CONTEO, YA QUE ES POR MES
                $cumRealDay[$vDate] = $valueDayT[$vDate]; 
                $cumMetaDay[$vDate] = $valuePlanDayT[$vDate]; 
                
                $auxCumReal = $valueDayT[$vDate];
                $auxCumMeta = $valuePlanDayT[$vDate]; 
            } 
        }  
        
    ?>
    
    <body>
        <div > 
            <style> 
                #moe1 { 
                    margin-left: -5%;
                    width: 110%; 
                    height: 25vh; 
                } 
            </style> 
            
            <div id="moe1" class="jidokaDay" >
                <script>
                    var chart = AmCharts.makeChart("moe1", {
                        "type": "serial",
                        "theme": "light",
                        "dataDateFormat": "YYYY-MM-DD",
                        "precision": 0,
                        "valueAxes": [{
                            "id": "v1", 
                            "title": "Reales", 
                            "position": "left", 
                            "autoGridCount": false
                        }, {
                            "id": "v2", 
                            "title": "Meta", 
                            "gridAlpha": 0, 
                            "position": "right", 
                            "autoGridCount": false 
                        }],
                        "graphs": [{
                            "id": "g3",
                            "valueAxis": "v1",
                            "lineColor": "#e1ede9",
                            "fillColors": "#e1ede9",
                            "fillAlphas": 1,
                            "type": "column",
                            "title": "Piezas Esperadas",
                            "valueField": "sales2",
                            "clustered": false,
                            "columnWidth": 0.5,
                            "legendValueText": "[[value]]",
                            "balloonText": "[[title]]: <b style='font-size: 120%'>[[value]]</b>"
                        }, {
                            "id": "g4",
                            "valueAxis": "v1",
                            "lineColor": "#02538b",
                            "fillColors": "#02538b",
                            "fillAlphas": 1,
                            "type": "column",
                            "title": "Piezas Reales",
                            "valueField": "sales1",
                            "clustered": false,
                            "columnWidth": 0.3,
                            "legendValueText": "[[value]]",
                            "balloonText": "[[title]]: <b style='font-size: 120%'>[[value]]</b>"
                        }, {
                            "id": "g1",
                            "valueAxis": "v2",                             
                            "lineThickness": 2,
                            "lineColor": "#bc2727",
                            "type": "smoothedLine",
                            "dashLength": 5,
                            "title": "Cumm Plan",
                            "useLineColorForBulletBorder": true,
                            "valueField": "cumPlan",
                            "balloonText": "[[title]]: <b style='font-size: 120%'>[[value]]</b>"
                        },{
                            "id": "g2",
                            "valueAxis": "v2",                            
                            "lineThickness": 2,
                            "lineColor": "#000000",
                            "type": "smoothedLine",
                            "dashLength": 5,
                            "title": "Cumm Real",
                            "useLineColorForBulletBorder": true,
                            "valueField": "cumReal",
                            "balloonText": "[[title]]: <b style='font-size: 120%'>[[value]]</b>"
                        }],
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha": 0,
                            "valueLineAlpha": 0.2
                        },
                        "categoryField": "date",
                        "categoryAxis": {
                            "parseDates": true,
                            "dashLength": 1,
                            "minorGridEnabled": true
                        },                        
                        "balloon": {
                            "borderThickness": 1,
                            "shadowAlpha": 0
                        },
                        "dataProvider": [
                        <?php  
                            for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                $date = explode("-", $i);                                 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                        ?> {
                            "date": "<?php echo "$fecha[$vDate]" ?>",
                            "cumReal": <?php echo "$cumRealDay[$vDate]" ?>,
                            "cumPlan": <?php echo "$cumMetaDay[$vDate]" ?>,
                            "sales1": <?php echo $valueDayT[$vDate]; ?>,
                            "sales2": <?php echo $valuePlanDayT[$vDate]; ?>
                        }, 
                        <?php } ?>        
                        ]
                    });
                </script>
            </div>
 