<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

    <?php 
        #METODO BURBUJA  
        function burbuja($vo ,$n) { 
            for($i = 1; $i < $n; $i++) { 
                for($j = 0; $j < $n-$i; $j++) { 
                    if($vo[$j] < $vo[$j+1]) { 
                        $k = $vo[$j+1]; 
                        $vo[$j+1] = $vo[$j]; 
                        $vo[$j] = $k; 
                    } 
                } 
            }  
            return $vo;
        } 
    
        #CONEXION A BASE DE DATOS Y TODO EL ANALISIS    
        include '../../db/ServerFunctions.php';
        session_start();
        
        //PRIMER APARTADO EL ACTUAL
        $line = $_SESSION["linea"];        
        $tipo = $_POST["tipoDato"];
        $fI = $_POST["fIni"];
        $fF = $_POST["fFin"];    
        
        $fIni = date("Y-m-d", strtotime($fI));
        $fFin = date("Y-m-d", strtotime($fF));
        
        if ($fIni > $fFin) { 
            $fIni = date("Y-m-d", strtotime($fF)); 
            $fFin = date("Y-m-d", strtotime($fI)); 
        } 
        
        switch ($tipo){
            case 0; // por defecto
            case 1; // por duracion                
                $dattop3 = t3Organizacionales($line, $fIni, $fFin); 
                for($i = 0; $i < count($dattop3); $i++){ 
                    $x = 0; 
                    $pzasT[$i] = 0;
                    $operacionTec[$i] = $dattop3[$i][0]; 
                    $problemaTec[$i] = $dattop3[$i][1]; 
                    $opTec[$i] = (string) $operacionTec[$i]; //cambio de valor para imprimir operacionTecOrg 
                    $dat1[$i] = $dattop3[$i][2]; 
                    $dat2[$i] = $dattop3[$i][3]; 
                    
                    #APARTADO PARA PIEZAS 
                    $datNoParte = t3OrganizacionalesNoParte($line, $fIni, $fFin, $problemaTec[$i],$dattop3[$i][4]); 
                    for($j = 0;  $j < count($datNoParte); $j++){
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] = $durNoParte[$x] * 60; 
                        
                        $datTcNoParte = t3Tc($line, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++) { 
                            $tcTec[$k] = ceil($datTcNoParte[$k][0]); 
                            // TRANSFORMACION DE TIEMPO A PZAS 
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN); 
                            $pzasT[$i] = $pzasT[$i] + $pzasNP[$k]; 
                        } 
                        $x++;
                    } 
                    $dat3[$i] = $pzasT[$i];
                }
                $dattop3 = t3Organizacionales($line, $fIni, $fFin); 
                
                $titulo [0] = "Top 5: Orgaizacionales (Duración)";
                $band = 1;
                break;
                
            case 2;// por frecuencia
                $dattop3 = t3OrganizacionalesFrec($line,$fIni,$fFin);
                
                for($i = 0; $i < count($dattop3); $i++) { 
                    $x = 0; 
                    $pzasT[$i] = 0; 
                    $operacionTec[$i] = $dattop3[$i][0]; 
                    $problemaTec[$i] = $dattop3[$i][1]; 
                    $opTec[$i] = (string) $operacionTec[$i]; //cambio de valor para imprimir operacionTecOrg
                    $dat1[$i] = $dattop3[$i][2]; 
                    $dat2[$i] = $dattop3[$i][3]; 

                    #APARTADO PARA PIEZAS
                    $datNoParte = t3OrganizacionalesNoParte($line, $fIni, $fFin, $problemaTec[$i], $dattop3[$i][4]); 
                    for($j = 0;  $j < count($datNoParte); $j++){
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1];                             
                        $seg[$x] = $durNoParte[$x] * 60; 

                        $datTcNoParte = t3Tc($line, $noParte[$x]);
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = ceil($datTcNoParte[$k][0]); 
                            // TRANSFORMACION DE TIEMPO A PZAS 
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN); 
                            $pzasT[$i] = $pzasT[$i] + $pzasNP[$k]; 
                        } 
                        $x++;
                    } 
                    $dat3[$i] = $pzasT[$i]; 
                }                 
                
                $dattop3 = t3TecnicasFrec($line,$fIni,$fFin);                
                
                $titulo [0] = "Top 5: Orgaizacionales (Frecuencia)";
                $band = 2;
                break;
                
            case 3;// por piezas
                $arrayPzas = array();
                $dattop3 = t3Organizacionales($line, $fIni, $fFin); 
                for($i = 0; $i < count($dattop3); $i++){ 
                    $x = 0; 
                    $pzasT[$i] = 0; 
                    $operacionTec[$i] = $dattop3[$i][0]; 
                    $problemaTec[$i] = $dattop3[$i][1]; 
                    $opTec[$i] = (string) $operacionTec[$i]; //cambio de valor para imprimir operacionTecOrg 
                    
                    #APARTADO PARA PIEZAS
                    $datNoParte = t3OrganizacionalesNoParte($line, $fIni, $fFin, $problemaTec[$i],$dattop3[$i][4]); 
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] = $durNoParte[$x] * 60; 
                        
                        $datTcNoParte = t3Tc($line, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = ceil($datTcNoParte[$k][0]); 
                            // TRANSFORMACION DE TIEMPO A PZAS 
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN); 
                            $pzasT[$i] = $pzasT[$i] + $pzasNP[$k]; 
                        } 
                        $x++; 
                    } 
                    array_push($arrayPzas, $pzasT[$i]); 
                } 
                $dattop3 = t3Organizacionales($line, $fIni, $fFin); 
                
                $titulo [0] = "Top 5: Orgaizacionales (Piezas)"; 
                $band = 3; 
                
                #LLAMANOS AL METODO BURBUJA 
                $VectorB = burbuja($arrayPzas,sizeof($arrayPzas)); 
                
                #ASIGNACION DE VALORES 
                for($i = 0; $i < sizeof($VectorB); $i++){ 
                    $dat1[$i] = $VectorB[$i]; 
                    for ($j = 0; $j < sizeof($VectorB); $j++){ 
                        if($dat1[$i] == $pzasT[$j] ){ 
                            $dat2[$i] = $dattop3[$j][2]; 
                            $dat3[$i] = $dattop3[$j][3]; 
                        } 
                    } 
                } 
                break;
        }
        
    ?>
    
        <div aling = "center" class="col-lg-12 col-md-12 col-xs-12 col-sh-12 contenidoCentrado" style="margin-top: -4.8%" >
             <h5 aling = "center" class="contenidoCentrado" style="margin-top: -1%; align-items: center; align-content: center "><b><?php echo $titulo[0] ?></b></h5>
        </div>
        <div aling = "center" id="ptc" class="grafTop3" style="width: 90%; height: 50%; margin-top: -1.5% " > 
            <script>
                var chart = AmCharts.makeChart("ptc", {
                    "type": "serial",
                    "theme": "light",
                    
                    "rotate": true,
                    "dataProvider": [
                    <?PHP for($i = 0 ;$i < count($dattop3);$i++){?>
                        {
                            "country": "<?php echo "$opTec[$i], $problemaTec[$i]";?>",
                            "visits": "<?php echo $dat1[$i]; ?>"
                        },
                    <?php } ?>
                    ],
                    "graphs": [{
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "visits",
                        "lineColor": "#02538b"
                    }],
                    "categoryField": "country",
                    "categoryAxis": {
                        "reverse": true
                    }
                });
            </script>
        </div>
        <div aling = "center" style="align-content: center; text-align: center; height: 70%" class="col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
            <table style="width: 75%; margin-left: 12.5%" class="table table-bordered col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
                <thead style="background-color: #eaeded;" class="contenidoCentrado"> 
                    <tr> 
                        <th >Operacion </th> 
                        <th >Descripción </th> 
                    <?php switch ($tipo){ 
                        case 2: ?>
                        <th >Frecuencia </th> 
                        <th >Duracion </th>                        
                        <th >Piezas</th> 
                        <?php break; 
                        case 3:
                            ?>
                        <th >Piezas</th>                         
                        <th >Duracion </th>
                        <th >Frecuencia </th> 
                        <?php break;  
                        default :?>
                        <th >Duracion </th> 
                        <th >Frecuencia </th> 
                        <th >Piezas</th> 
                    <?php 
                        break;
                    } ?> 
                    </tr> 
                </thead> 
                <tbody > 
                <?php for ($i = 0; $i < count($dattop3); $i++) { ?> 
                    <tr> 
                        <td  > <?php echo $operacionTec[$i] ?> </td> 
                        <td width='55%' > <?php echo $problemaTec[$i]; ?> </td> 
                        <td > <?php echo $dat1[$i]; ?> </td> 
                        <td > <?php echo $dat2[$i]; ?> </td> 
                        <td > <?php echo $dat3[$i]; ?> </td> 
                    </tr> 
                <?php } ?> 
                </tbody> 
            </table> 
        </div> 
        
