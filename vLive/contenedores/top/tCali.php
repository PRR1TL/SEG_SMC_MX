<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

    <?php
        include '../../db/ServerFunctions.php'; 
        session_start();
        
        //PRIMER APARTADO EL ACTUAL
        $line = $_SESSION["linea"];        
        $tipo = $_POST["tipoDato"];
        $operacion = $_POST["operacion"];
        
        $fI = $_POST["fIni"];
        $fF = $_POST["fFin"];  
        
        //CONVERSION DE FECHA
        $fIni = date("Y-m-d", strtotime($fI));
        $fFin = date("Y-m-d", strtotime($fF));
                
        if ($fIni > $fFin) { 
            $fIni = date("Y-m-d", strtotime($fF)); 
            $fFin = date("Y-m-d", strtotime($fI)); 
        } 
        
        switch ($tipo){
            case 0; // por defecto
            case 1; // por defecto
            case 3; // por duracion
                if ($operacion == 0){ //SELECCION DE OPERACION
                    $dattop3 = t3Calidad($line, $fIni, $fFin);
                    for($i = 0; $i < count($dattop3); $i++){
                        $pzasT[$i] = 0;
                        $operacionCali[$i] = $dattop3[$i][0];
                        $problemaCali [$i] = $dattop3[$i][1];
                        if (isset($operacionCali[$i])){
                            $descripcion[$i] = $operacionCali[$i].', '.$problemaCali[$i];
                        } else { 
                            $descripcion[$i] = $problemaCali[$i];
                        } 
                        $dat1[$i] = $dattop3[$i][2]; 
                        $dat2[$i] = $dattop3[$i][3]; 
                    } 
                    $dattop3 = t3Calidad($line, $fIni, $fFin);
                } else {
                    $dattop3 = t3CalidadYOP($line, $fIni, $fFin, $operacion);
                    for($i = 0; $i < count($dattop3); $i++){
                        $pzasT[$i] = 0;
                        $operacionCali[$i] = $dattop3[$i][0];
                        $problemaCali [$i] = $dattop3[$i][1]; 
                        if (isset($operacionCali[$i])){
                            $descripcion[$i] = $operacionCali[$i].', '.$problemaCali[$i];
                        } else { 
                            $descripcion[$i] = $problemaCali[$i];
                        } 
                        $dat1[$i] = $dattop3[$i][2];      
                        $dat2[$i] = $dattop3[$i][3];      
                    }
                    $dattop3 = t3CalidadYOP($line, $fIni, $fFin, $operacion);
                } 
                $titulo[0] = "Top 5: Calidad (Piezas)"; 
                $band = 1; 
                break; 
                
            case 2;// por frecuencia
                if ($operacion == 0){ //SELECCION DE OPERACION
                    $dattop3 = t3CalidadFrec($line, $fIni, $fFin);
                    for($i = 0; $i < count($dattop3); $i++){
                        $pzasT[$i] = 0;
                        $operacionCali[$i] = $dattop3[$i][0];
                        $problemaCali[$i] = $dattop3[$i][1];
                        if (isset($operacionCali[$i])){
                            $descripcion[$i] = $operacionCali[$i].', '.$problemaCali[$i];
                        } else { 
                            $descripcion[$i] = $problemaCali[$i];
                        }                         
                        $dat1[$i] = $dattop3[$i][2];
                        $dat2[$i] = $dattop3[$i][3]; 
                    }
                    $dattop3 = t3CalidadFrec($line, $fIni, $fFin);
                } else { 
                    $dattop3 = t3CalidadFrecYOP($line, $fIni, $fFin, $operacion);
                    for($i = 0; $i < count($dattop3); $i++){
                        $pzasT[$i] = 0;
                        $operacionCali[$i] = $dattop3[$i][0];
                        $problemaCali[$i] = $dattop3[$i][1];
                        if (isset($operacionCali[$i])){
                            $descripcion[$i] = $operacionCali[$i].', '.$problemaCali[$i];
                        } else { 
                            $descripcion[$i] = $problemaCali[$i];
                        } 
                        $dat1[$i] = $dattop3[$i][2]; 
                        $dat2[$i] = $dattop3[$i][3]; 
                    }
                    $dattop3 = t3CalidadFrecYOP($line, $fIni, $fFin, $operacion);
                }
                $titulo[0] = "Top 5: Calidad (Frecuencia)"; 
                $band = 2; 
                break; 
        }
        
    ?>
    
    <div aling = "center" class="col-lg-12 col-md-12 col-xs-12 col-sh-12 contenidoCentrado" > 
         <h5 aling = "center" class="contenidoCentrado" style="margin-top: -1%; align-items: center; align-content: center "><b><?php echo $titulo[0] ?></b></h5>
    </div> 
    
        <div aling = "center" id="ptca" class="grafTop3" style="width: 90%; height: 50%"> 
            <script> 
                var chart = AmCharts.makeChart("ptca", { 
                    "type": "serial",
                    "theme": "light",
                    "rotate": true,
                    "dataProvider": [ 
                    <?PHP for($i = 0 ;$i < count($dattop3);$i++){ ?> 
                        {
                            "country": "<?php echo $descripcion[$i]; ?>", 
                            "visits": "<?php echo $dat1[$i]; ?>" 
                        }, 
                    <?php } ?> 
                    ], 
                    "graphs": [{ 
                        "fillAlphas": 0.9,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "visits",
                        "lineColor": "#02538b"
                    }],
                    "categoryField": "country",
                    "categoryAxis": {
                        "reverse": true
                    }
                });

            </script>
        </div>
        <br><br>
        <div aling = "center" style="align-content: center; text-align: center; height: 70%" class="col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
            <table style="width: 75%; margin-left: 12.5%" class="table table-bordered col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
                <thead style="background-color: #eaeded;" class="contenidoCentrado"> 
                    <tr> 
                        <th >Operacion </th> 
                        <th >Descripción </th> 
                    <?php switch ($tipo){ 
                        case 1:  case 3: ?>
                        <th >Piezas</th> 
                        <th >Frecuencia </th>                          
                        <?php break; 
                        case 2: ?>
                        <th >Frecuencia </th>                       
                        <th >Piezas</th>
                        <?php break;  
                        default :?>                        
                        <th >Piezas</th> 
                        <th >Frecuencia </th> 
                    <?php 
                        break;
                    } ?>       
                    </tr> 
                </thead> 
                <tbody > 
                <?php for ($i = 0; $i < count($dattop3); $i++) { ?> 
                    <tr> 
                        <td  > <?php echo $operacionCali[$i] ?> </td> 
                        <td width='55%' > <?php echo $problemaCali[$i]; ?> </td> 
                        <td > <?php echo $dat1[$i]; ?> </td> 
                        <td > <?php echo $dat2[$i]; ?> </td> 
                    </tr> 
                <?php } ?> 
                </tbody> 
            </table> 
        </div> 
