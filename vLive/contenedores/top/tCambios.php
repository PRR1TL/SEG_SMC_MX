<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
    <!--<LINK REL=StyleSheet HREF="../../css/tops.css" TYPE="text/css" MEDIA=screen>-->
<!--    <title>Top 5: Cambio de modelo</title>
    <link href="../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

    <?php     
        #METODO BURBUJA  
        function burbuja($vo ,$n) { 
            for($i = 1; $i < $n; $i++) { 
                for($j = 0; $j < $n-$i; $j++) { 
                    if($vo[$j] < $vo[$j+1]) { 
                        $k = $vo[$j+1]; 
                        $vo[$j+1] = $vo[$j]; 
                        $vo[$j] = $k; 
                    } 
                } 
            }  
            return $vo;
        } 
    
        #CONEXION A BASE DE DATOS Y TODO EL ANALISIS     
        include '../../db/ServerFunctions.php';
        session_start();
        
        //PRIMER APARTADO EL ACTUAL
        $line = $_SESSION["linea"];        
        $tipo = $_POST["tipoDato"];
        $fI = $_POST["fIni"];
        $fF = $_POST["fFin"];    
        
        //CONVERSION DE FECHA
        $fIni = date("Y-m-d", strtotime($fI));
        $fFin = date("Y-m-d", strtotime($fF));
        
        if ($fIni > $fFin) { 
            $fIni = date("Y-m-d", strtotime($fF)); 
            $fFin = date("Y-m-d", strtotime($fI)); 
        } 
        
        //echo '<br>',$line,', ', $fIni,', ', $fFin;
        
        switch ($tipo){
            case 0; //por defecto
            case 1;// por duracion                
                $dattop3 = t3CambioModelo($line, $fIni, $fFin);
                for($i = 0; $i < count($dattop3); $i++){ 
                    $pzasT[$i] = 0; 
                    $x = 0;
                    $problemaTec [$i] = $dattop3[$i][0]; 
                    $dat1[$i] = $dattop3[$i][1]; 
                    $dat2[$i] = $dattop3[$i][2]; 
                    $dat3[$i] = 0; 
                    $datNoParte = t3CambioNoParte($line, $fIni, $fFin, $problemaTec[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1];
                        $seg[$x] = $durNoParte[$x] * 60;

                        $datTcNoParte = t3Tc($line, $noParte[$x]);
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = ceil($datTcNoParte[$k][0]);
                            // TRANSFORMACION DE TIEMPO A PZAS
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
                            $pzasT[$i] = $pzasT[$i] + $pzasNP[$k];
                        }  
                        $x++;
                    } 
                    $dat3[$i] = $pzasT[$i]; 
                } 

                $titulo [0] = "Top 5: Cambio de modelo (Duración)";
                $band = 1;
                break;
                
            case 2;// por frecuencia                
                $dattop3 = t3CambioModeloFrec($line,$fIni,$fFin); 
                
                for($i = 0; $i < count($dattop3); $i++) {  
                    $x = 0;                        
                    $pzasT[$i] = 0; 
                    $problemaTec[$i] = $dattop3[$i][0]; 
                    $dat1[$i] = $dattop3[$i][1]; 
                    $dat2[$i] = $dattop3[$i][2];                                                

                    $datNoParte = t3CambioNoParte($line, $fIni, $fFin, $problemaTec[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1];
                        $seg[$x] = $durNoParte[$x] * 60;

                        $datTcNoParte = t3Tc($line, $noParte[$x]);
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = ceil($datTcNoParte[$k][0]);
                            // TRANSFORMACION DE TIEMPO A PZAS
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
                            $pzasT[$i] = $pzasT[$i] + $pzasNP[$k];
                        }  
                        $x++;
                    } 
                    $dat3[$i] = $pzasT[$i];                     
                } 
                
                $dattop3 = t3CambioModeloFrec($line,$fIni,$fFin); 
                $titulo [0] = "Top 5: Cambio de modelo (Frecuencia)";
                $band = 2;
                break;
                
            case 3;// por piezas 
                $arrayPzas = array();
                $dattop3 = t3CambioModelo($line, $fIni, $fFin);
                for($i = 0; $i < count($dattop3); $i++){ 
                    $pzasT[$i] = 0; 
                    $x = 0;
                    $problemaTec [$i] = $dattop3[$i][0]; 
//                    $dat2[$i] = $dattop3[$i][1]; 
//                    $dat3[$i] = $dattop3[$i][2]; 
                    
                    $dat1[$i] = 0; 
                    $datNoParte = t3CambioNoParte($line, $fIni, $fFin, $problemaTec[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1];
                        $seg[$x] = $durNoParte[$x] * 60;

                        $datTcNoParte = t3Tc($line, $noParte[$x]);
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = ceil($datTcNoParte[$k][0]);
                            // TRANSFORMACION DE TIEMPO A PZAS
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
                            $pzasT[$i] = $pzasT[$i] + $pzasNP[$k];
                        }  
                        $x++;
                    } 
                    array_push($arrayPzas, $pzasT[$i]);
                    //$dat1[$i] = $pzasT[$i]; 
                } 
                $titulo [0] = "Top 5: Cambio de modelo (Piezas)"; 
                $band = 3; 
                
                #LLAMANOS AL METODO BURBUJA 
                $VectorB = burbuja($arrayPzas,sizeof($arrayPzas)); 
                
                #ASIGNACION DE VALORES
                for($i = 0; $i < sizeof($VectorB); $i++){ 
                    $dat1[$i] = $VectorB[$i]; 
                    for ($j = 0; $j < sizeof($VectorB); $j++){ 
                        if($dat1[$i] == $pzasT[$j] ){ 
                            $dat2[$i] = $dattop3[$j][1]; 
                            $dat3[$i] = $dattop3[$j][2]; 
                        } 
                    } 
                } 
                
                break;
        }
        
    ?>     
    
    <div aling = "center" class="col-lg-12 col-md-12 col-xs-12 col-sh-12 contenidoCentrado" style="margin-top: -1.8%" >
        <h5 aling = "center" class="contenidoCentrado" style="margin-top: -1%; align-items: center; align-content: center "><b><?php echo $titulo[0] ?></b></h5>
    </div> 
    
    <div aling = "center" id="ptc" class="grafTop3" style="width: 90%; height: 50%"> 
        <script>
            var chart = AmCharts.makeChart("ptc", {
                "type": "serial",
                "theme": "light",
                "rotate": true,
                "dataProvider": [
                <?PHP for($i = 0 ;$i < count($dattop3);$i++){?>
                    {
                        "country": "<?php echo "$problemaTec[$i]";?>",
                        "visits": "<?php echo $dat1[$i]; ?>"
                    },
                <?php } ?>
                ],
                "graphs": [{
                    "fillAlphas": 0.9,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "visits",
                    "lineColor": "#02538b"
                }],
                "categoryField": "country",
                "categoryAxis": {
                    "reverse": true
                }
            });
        </script>
    </div>

    <br>
    <div aling = "center" style="align-content: center; text-align: center; height: 70%" class="col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
        <table style="width: 75%; margin-left: 12.5%" class="table table-bordered col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
            <thead style="background-color: #eaeded;" class="contenidoCentrado"> 
                <tr> 
                    <th >Descripción </th> 
                <?php switch ($tipo){ 
                    case 2: ?>
                    <th >Frecuencia </th> 
                    <th >Duracion </th> 
                    <th >Piezas</th> 
                    <?php break; 
                    case 3:
                        ?>
                    <th >Piezas</th>                         
                    <th >Duracion </th>
                    <th >Frecuencia </th> 
                    <?php break;  
                    default :?>
                    <th >Duracion </th> 
                    <th >Frecuencia </th> 
                    <th >Piezas</th> 
                <?php 
                    break;
                } ?>                        </tr> 
            </thead> 
            <tbody > 
            <?php for ($i = 0; $i < count($dattop3); $i++) { ?> 
                <tr>
                    <td width='55%' > <?php echo $problemaTec[$i]; ?> </td> 
                    <td > <?php echo $dat1[$i]; ?> </td> 
                    <td > <?php echo $dat2[$i]; ?> </td> 
                    <td > <?php echo $dat3[$i]; ?> </td> 
                </tr> 
            <?php } ?> 
            </tbody> 
        </table> 
        <br><br>
    </div> 