<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<script src="//www.amcharts.com/lib/3/amcharts.js"></script>
<script src="//www.amcharts.com/lib/3/serial.js"></script>
<script src="//www.amcharts.com/lib/3/themes/light.js"></script>

<style>
    html, body {
        width: 100%;
        height: 100%;
        margin: 0px;
    }

    #chartdiv {
	width: 100%;
	height: 100%;
    }
</style>

<div id="chartdiv">
    <script>
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "light",
            "dataDateFormat": "YYYY-MM-DD",
            "dataProvider": [{
                "country": "2012-01-01",
                "value1": 2025,
                "value2": 1752
            }, {
                "country": "China",
                "value1": 1882,
                "value2": 1220
            }, {
                "country": "Japan",
                "value1": 1809,
                "value2": 900
            }, {
                "country": "Germany",
                "value1": 1322,
                "value2": 1121
            }, {
                "country": "UK",
                "value1": 1122,
                "value2": 452
            }, {
                "country": "France",
                "value1": 1114,
                "value2": 845
            }, {
                "country": "India",
                "value1": 984,
                "value2": 354
            }, {
                "country": "Spain",
                "value1": 711,
                "value2": 250
            }, {
                "country": "Netherlands",
                "value1": 665,
                "value2": 500
            }, {
                "country": "Russia",
                "value1": 580,
                "value2": 399
            }, {
                "country": "South Korea",
                "value1": 443,
                "value2": 254
            }, {
                "country": "Canada",
                "value1": 441,
                "value2": 269
            }, {
                "country": "Brazil",
                "value1": 395,
                "value2": 210
            }],
            "graphs": [{
                "fillAlphas": 0.3,
                "lineAlpha": 1,
                "type": "column",
                "lineColor": "#5c5",
                "valueField": "value1",
                "clustered": false,
                "labelText": "[[value]]"
            }, {
                "fillAlphas": 1,
                "lineAlpha": 1,
                "type": "column",
                "lineColor": "#5c5",
                "valueField": "value2",
                "clustered": false
            }],
            "categoryField": "country",
            "categoryAxis": {
                "gridPosition": "start"
            }
          });
    </script>
</div>


