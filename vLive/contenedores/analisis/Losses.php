<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1"> 
        <!--LIBRERIAS PARA DISEÑO-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>  
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn3.devexpress.com/jslib/18.2.8/css/dx.common.css" />         
        <link rel="dx-theme" data-theme="generic.light" href="https://cdn3.devexpress.com/jslib/18.2.8/css/dx.light.css" />
        <script src="https://cdn3.devexpress.com/jslib/18.2.8/js/dx.all.js"></script> 
        <link rel="stylesheet" href="../../css/losses.css"/> 
        
        <!--LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR--> 
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
        <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
        
    <?php 
        include '../../db/ServerFunctions.php';
        session_start();

        if (isset($_SESSION['linea'])){
            $line = $_SESSION['linea'];
            $anio = $_SESSION['anio'];
            $mes = $_SESSION['mes'];
        } else {
            $line = 'L001';
            $anio = date('Y');
            $mes = date('m');
        }      
        
        $fecha1 = $anio.'-01-01';
        $fecha2 = date("Y-m-d"); 
        
        if (isset($_SESSION['jFIni'])){
            $fechaTIni = date("m/d/Y",strtotime($_SESSION['jFIni']));
            $fechaTFin = date("m/d/Y",strtotime($_SESSION['jFFin']));
        } else {
            $fechaTIni = date("m/d/Y",strtotime($_SESSION['fIniT']));
            $fechaTFin = date("m/d/Y",strtotime($_SESSION['fFinT']));
        } 
                
        //TIPO DE CALCULO QUE SE VA REALIZAR DE ACUERDO A LA OPCION DEL COMBO 
        if (isset($_SESSION["jTipoDato"])) { 
            $tipo = $_SESSION["jTipoDato"]; 
        } else { 
            $tipo = 1;
        }
        
        //echo $line,', fi:',$fechaTIni,' ff: ', $fechaTFin,' t:',$tipo; 
    
        for ($i = 0; $i < 7; $i++) { 
            $opTec[$i] = 0;
            for ($j = 0; $j < 15; $j++ ) { 
                $subProbTec[$i][$j] = "-"; 
                $subPercentTec[$i][$j] = 0;
                $subProbOrg[$i][$j] = ""; 
                $subPercentOrg[$j][$i] = 0;
                $subProbCali[$i][$j] = "-";
                $subProbCali[$i][$j] = "";
                $subPzasCali[$i][$j] = 0;
            } 
        } 
        
        //CONTADORES
        $acumPercentTec = 0;
        $acumPercentOrg = 0;
        
        //TIEMPOS TOTALES REGISTRADOS  
        $datMinTec = lossesTimeTechnicals($line, $fechaTIni, $fechaTFin); 
        $datMinOrg = lossesTimeOrganizational($line, $fechaTIni, $fechaTFin); 
        $datMinCali = lossesTimeQuality($line, $fechaTIni, $fechaTFin); 
        
        switch ($tipo) { 
            case 1:
            case 3: 
                //OBTENCION DE TOPS POR TEMA
                //TECNICAS
                $acumPercentTec = 0;
                $datTop3DiaTecOp = t3TecDiaOpLosses($line, $fechaTIni, $fechaTFin);
                $contTec = count($datTop3DiaTecOp); 
                for ($i = 0; $i < count($datTop3DiaTecOp); $i++ ) {
                    $opTec[$i] = $datTop3DiaTecOp[$i][0];
                    $probTec[$i] = $datTop3DiaTecOp[$i][2];
                    $durTec[$i] = $datTop3DiaTecOp[$i][3];
                    $frecTec[$i] = $datTop3DiaTecOp[$i][4]; 
                    $percentTec[$i] = @round(($durTec[$i]*100)/$datMinTec[0][0],2); 
                    $acumPercentTec += $percentTec[$i]; 
                    $sCTec[$i] = 0; 
                    $acumSubPercentTec[$i] = 0; 
                    //echo '<br> acum ',$i,': ',$acumPercentTec; 
                    //echo '<br>* ', $opTec[$i], ', ',$probTec[$i],', ',$durTec[$i],', ', $percentTec[$i]; 
                    $datTop3DiaTecProblemaOp = t3ProblemTecDiaOpLosses($line, $fechaTIni, $fechaTFin, $opTec[$i]); 
                    $sCTec[$i] = count($datTop3DiaTecProblemaOp); 
                    for ($j = 0; $j < count($datTop3DiaTecProblemaOp); $j++) { 
                        $subProbTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][0]; 
                        $subDurTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][1]; 
                        $subFrecTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][2]; 
                        $subPercentTec[$i][$j] = @round(($subDurTec[$i][$j]*$percentTec[$i])/$durTec[$i],2); 
                        $acumSubPercentTec[$i] += $subPercentTec[$i][$j]; 
                        $sNPTec[$i][$j] = 0;
                        //echo '<br>  -', $subProbTec[$i][$j], ', ',$subDurTec[$i][$j],', ',$subPercentTec[$i][$j]; 
                        
                        //TOP DE NUMERO DE PARTE
                        $datTopNoParteTec = t3NoParteTecDiaOpLosses($line, $fechaTIni, $fechaTFin, $opTec[$i], $subProbTec[$i][$j]);
                        $sNPTec[$i][$j] = count($datTopNoParteTec);
                        for ($k = 0; $k < count($datTopNoParteTec); $k++){
                            $noPartTec[$i][$j][$k] = $datTopNoParteTec[$k][0];
                            $durNoPartTec[$i][$j][$k] = $datTopNoParteTec[$k][1];
                            $frecNoPartTec[$i][$j][$k] = $datTopNoParteTec[$k][2];                            
                            $percentNoPartTec[$i][$j][$k] = @round(($durNoPartTec[$i][$j][$k]*$subPercentTec[$i][$j])/$subDurTec[$i][$j],2);
                            //$sNPTec[$i][$j] = $k+1;
                            //echo '<br>    .', $noPartTec[$i][$j][$k], ', ',$durNoPartTec[$i][$j][$k],', ',$percentNoPartTec[$i][$j][$k];
                        } 
                    }
                    
                    //COMPARACION PARA SACAR CANTIDADES DE OTROS
                    if ($acumSubPercentTec[$i] < $percentTec[$i] && $acumSubPercentTec[$i] > 0 ) {                         
                        $contPOTec = $sCTec[$i]; 
                        //echo '<br>c ',$i,' -> ', $contPOTec; 
                        $subProbTec[$i][$contPOTec] = "Otros"; 
                        $subPercentTec[$i][$contPOTec] = @round($percentTec[$i] - $acumSubPercentTec[$i]); 
                        $sNPTec[$i][$contPOTec] = 0; 
                        $sCTec[$i] = $contPOTec+1; 
                        //echo '<br>',$opTec[$i], ', ',$probTec[$i],', ',$acumSubPercentTec[$i],', ',$percentTec[$i],' -> ', $subPercentTec[$i][$contPOTec];
                    } 
                    
                } 
                
                //echo "acumuladoTec: ",$acumPercentTec;                
                if ($acumPercentTec != 100) {                     
                    $opTec[$contTec] = "OTROS"; 
                    $probTec[$contTec] = ""; 
                    $percentTec[$contTec] = @round(100 - $acumPercentTec); 
                    $sCTec[$contTec] = 0; 
                    $acumPercentSubOtrosTec = 0; 
                    
                    //HACEMOS EL TOP DE LOS 10 PROBLEMAS DISTINTOS DE LAS OPERACIONES QUE YA TENEMOS
                    //OBENEMOS EL TOTAL DE PROBLEMAS RESTANTES
                    $datMinOtrosTec = tiempoTecOtrosLosses($line, $fechaTIni, $fechaTFin, $opTec[0], $opTec[1], $opTec[2], $opTec[3], $opTec[4]); 
                    $minOtrosTec = $datMinOtrosTec[0][0]; 
                    
                    $datTopOtrosTec = topProblemOtrosTecDiaOpLosses($line, $fechaTIni, $fechaTFin, $opTec[0], $opTec[1], $opTec[2], $opTec[3], $opTec[4]); 
                    for ($i = 0; $i < count($datTopOtrosTec); $i++) { 
                        $subProbTec[$contTec][$i] = $datTopOtrosTec[$i][1]; 
                        $subDurTec[$contTec][$i] = $datTopOtrosTec[$i][2]; 
                        $subFrecTec[$contTec][$i] = $datTopOtrosTec[$i][3]; 
                        $subPercentTec[$contTec][$i] = @round(($subDurTec[$contTec][$i]*$percentTec[$contTec])/$minOtrosTec,2); 
                        $acumPercentSubOtrosTec += $subPercentTec[$contTec][$i]; 
                        $sCTec[$contTec] = $i+1; 
                        $sNPTec[$contTec][$i] = 0;
                        //echo "<br> ",$subProbTec[$contTec][$i],', ',$subDurTec[$contTec][$i],', ',$subPercentTec[$contTec][$i] ;
                    }
                    
                    if ($acumPercentSubOtrosTec < $percentTec[$contTec] && $acumPercentSubOtrosTec > 0 ) {                         
                        $contPOTec = count($datTopOtrosTec);    
                        $subProbTec[$contTec][$contPOTec] = "Otros"; 
                        $subPercentTec[$contTec][$contPOTec] = @round($percentTec[$contTec] - $acumPercentSubOtrosTec);
                        $sNPTec[$contTec][$contPOTec] = 0;
                        $sCTec[$contTec] = $contPOTec+1; 
                    }                    
                    $contTec++;//= count($datTop3DiaTecOp)+1;
                } 
                
                //ORGANIZACIONALES
                $datTop3DiaOrgArea = t3OrgDiaAreaLosses($line, $fechaTIni, $fechaTFin);
                for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) { 
                    $areaOrg[$i] = $datTop3DiaOrgArea[$i][0]; 
                    $durOrg[$i] = $datTop3DiaOrgArea[$i][1]; 
                    $frecOrg[$i] = $datTop3DiaOrgArea[$i][2]; 
                    $percentOrg[$i] = @round(($durOrg[$i]*100)/$datMinOrg[0][0],2); 
                    $sCOrg[$i] = 0; 
                    $acumSubPercentOrg[$i] = 0; 
                    $acumPercentOrg += $percentOrg[$i]; 
                    //echo '<br>*', $areaOrg[$i],', ',$durOrg[$i],', ',$frecOrg[$i],', ',$percentOrg[$i]; 
                    $datTop3DiaOrgProblemaOp = t3ProblemOrgDiaAreaLosses($line, $fechaTIni, $fechaTFin, $areaOrg[$i]); 
                    $sCOrg[$i] = count($datTop3DiaOrgProblemaOp);
                    //echo "<br>entra ",$i,': ',count($datTop3DiaOrgProblemaOp); 
                    for ($j = 0; $j < count($datTop3DiaOrgProblemaOp); $j++) {                        
                        $sProbOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][0]; 
                        $sMatOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][1]; 
                        if ($sMatOrg[$i][$j] != "") { 
                            $subProbOrg[$i][$j] = $sProbOrg[$i][$j].' ['.$sMatOrg[$i][$j].']'; 
                        } else { 
                            $subProbOrg[$i][$j] = $sProbOrg[$i][$j]; 
                        }                         
                        $subDurOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][2]; 
                        $subFrecOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][3]; 
                        $subPercentOrg[$i][$j] = @round(($subDurOrg[$i][$j]*$percentOrg[$i])/$durOrg[$i],2); 
                        $acumSubPercentOrg[$i] += $subPercentOrg[$i][$j];
                        //$sCOrg[$i] = $j+1; 
                        $sNPOrg[$i][$j] = 0; 
                        //echo '<br>  -', $subProbOrg[$i][$j], ', ',$subDurOrg[$i][$j],', ',$subPercentOrg[$i][$j]; 
                        //TOP DE NUMERO DE PARTE
                        $datTopNoParteOrg = t3NoParteOrgDiaAreaLosses($line, $fechaTIni, $fechaTFin, $areaOrg[$i], $subProbOrg[$i][$j]); 
                        $sNPOrg[$i][$j] = count($datTopNoParteOrg);
                        for ($k = 0; $k < count($datTopNoParteOrg); $k++ ) { 
                            $noPartOrg[$i][$j][$k] = $datTopNoParteOrg[$k][0]; 
                            $durNoPartOrg[$i][$j][$k] = $datTopNoParteOrg[$k][1]; 
                            $frecNoPartOrg[$i][$j][$k] = $datTopNoParteOrg[$k][2]; 
                            $percentNoPartOrg[$i][$j][$k] = @round(($durNoPartOrg[$i][$j][$k]*$subPercentOrg[$i][$j])/$subDurOrg[$i][$j],2); 
                            //echo '<br>    .', $noPartOrg[$i][$j][$k], ', ',$durNoPartOrg[$i][$j][$k],', ',$percentNoPartOrg[$i][$j][$k];
                        }                        
                    }
                    //echo '<br> nI:',count($datTop3DiaOrgProblemaOp);
                    if ($acumSubPercentOrg[$i] != $percentOrg[$i] && $acumSubPercentOrg[$i] > 0 ) {
                        $contOrg = count($datTop3DiaOrgProblemaOp);
                        $subProbOrg[$i][$contOrg] = "OTROS";
                        $subPercentOrg[$i][$contOrg] = @round($percentOrg[$i]-$acumSubPercentOrg[$i]);
                        //echo "<br>",$areaOrg[$i],': ',$percentOrg[$i],', ',$acumSubPercentOrg[$i], ' -> ', $subPercentOrg[$i][$contOrg],', ',$sCOrg[$i];
                        $sNPOrg[$i][$contOrg] = 0; 
                        $sCOrg[$i] = count($datTop3DiaOrgProblemaOp)+1;                        
                    }                    
                }
                
                //CALIDAD
                $datTop3DiaCaliArea = t3CaliDiaOpLosses($line, $fechaTIni, $fechaTFin);
                for ($i = 0; $i < count($datTop3DiaCaliArea); $i++ ) { 
                    $opC[$i] = $datTop3DiaCaliArea[$i][0]; 
                    $opCali[$i] = $datTop3DiaCaliArea[$i][0]; 
                    $maCali[$i] = $datTop3DiaCaliArea[$i][1]; 
                    $descCali[$i] = $datTop3DiaCaliArea[$i][2];                    
                    if (isset($maCali[$i])){
                        $opCali[$i] = $opCali[$i].' ['.$maCali[$i].']';
                    } else if (isset($descCali[$i])){
                        $opCali[$i] = $opCali[$i].' '.$descCali[$i];
                    }                         
                    
                    $pzasCali[$i] = $datTop3DiaCaliArea[$i][3]; 
                    $frecCali[$i] = $datTop3DiaCaliArea[$i][4]; 
                    $sCCali[$i] = 0; 
                    //$percentCali[$i] = @round(($durCali[$i]*100)/$datMinCali[0][0],2); 
                    //echo '<br>* ', $opCali[$i],', ',$descCali[$i],', ',$pzasCali[$i];//,', ',$percentCali[$i]; 
                    $datTop3DiaCaliProblemaOp = t3ProblemCaliDiaOpLosses($line, $fechaTIni, $fechaTFin, $opC[$i]); 
                    //echo '<br> , ',count($datTop3DiaCaliProblemaOp); 
                    for ($j = 0; $j < count($datTop3DiaCaliProblemaOp); $j++) { 
                        $subProbCali[$i][$j] = $datTop3DiaCaliProblemaOp[$j][0]; 
                        $subPzasCali[$i][$j] = $datTop3DiaCaliProblemaOp[$j][1]; 
                        $subFrecCali[$i][$j] = $datTop3DiaCaliProblemaOp[$j][2]; 
                        $subPercentCali[$i][$j] = @round(($subPzasCali[$i][$j] * $percentCali[$i])/$durCali[$i],2); 
                        $sCCali[$i] = $j+1; 
                        $sNPCali[$i][$j] = 0;
                        //echo '<br>- ', $subProbCali[$i][$j],', ',$subPzasCali[$i][$j],', ',$subPercentCali[$i][$j];//,', ',$percentOrg[$i]; 
                        //OBTENCION DE NUMERO DE PARTE
                        $datTopNoParteCali = t3NoParteCaliDiaOpLosses($line, $fechaTIni, $fechaTFin, $opC[$i], $subProbCali[$i][$j]);
                        for ($k = 0; $k < count($datTopNoParteCali); $k++) {
                            $noPartCali[$i][$j][$k] = $datTopNoParteCali[$k][0]; 
                            $pzasNoPartCali[$i][$j][$k] = $datTopNoParteCali[$k][1]; 
                            $frecNoPartCali[$i][$j][$k] = $datTopNoParteCali[$k][2]; 
                            //echo '<br>- ', $noPartCali[$i][$j][$k] ;
                            $sNPCali[$i][$j] = $k+1; 
                        } 
                    } 
                } 
                
                break;
            case 2:  
                $datTop3DiaTecOp = t3TecDiaOpLossesFrec($line, $pDiaI, $pDiaF);
                $datTop3DiaOrgArea = t3OrgDiaAreaLossesFrec($line, $pDiaI, $pDiaF); 
                $datTop3DiaCaliOp = t3CaliDiaOpLossesFrec($line, $pDiaI, $pDiaF); 

                //HASTA AQUI MODIFIQUE
                $datTop3MesTecOp = t3TecMesopLossesFrec($line, $year, $month); 
                $datTop3MesOrgArea = t3OrgMesAreaLossesFrec($line, $year, $month); 
                $datTop3MesCaliOp = t3CaliMesOpLossesFrec($line, $year, $month); 
                
                break;
        }
    ?>
    
        <a align=center id="headerFixed" class="contenedor">   
            <div class='fila0'> 
            </div> 
            <h4 class="tituloPareto"> 
                <?php echo 'Losses <br> Linea:&nbsp'.$line.' - '.$_SESSION['nameLinea'];?> 
            </h4> 
            <div class="fila1"> 
                <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%"> 
                    <form action="../../incio.php" method="POST"> 
                        <button class="btn btn-primary active btn-sm btnRegresar" 
                                onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" 
                                onmouseout="this.style.background='#02538B'" >
                            Inicio
                        </button>
                    </form> 
                    <form action="../jidokas/oee.php" method="POST">
                        <button  class="btn btn-success btn-sm btnTop3" onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer'" onmouseout="this.style.background='#008C5B'" >
                            Regresar
                        </button>
                    </form>
                    
                    <div class="col-lg-12 col-sm-12 col-md-12" style="margin-top: 87%" > 
                        <div class="col-lg-6 col-sm-6 col-md-3">
                        </div>
                        <div class="col-lg-6 col-sm-6 col-md-6" style="margin-left: 60%"> 
                            <input class="col-md-5 btn btn-secondary dropdown-toggle" type="text" name="dateIni" id="dateIni" value="<?php echo $fechaTIni; ?>" > 
                            
                            <input class="col-md-5 btn btn-secondary dropdown-toggle" style="margin-left: 3.5%" type="text" name="dateEnd" id="dateEnd" placeholder="Dia Final" value="<?php echo $fechaTFin; ?>">
                        </div> 
                        <div class="col-lg-6 col-sm-6 col-md-3">
                        </div>
                    </div> 
                   
                    <!--FUNCIONALIDAD DE LOS DATEPICKET-->
                    <script>
                        jQuery().ready(            
                            function() {
                                getResult();
                                setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
                            }
                        );

                        function getResult() { 
                            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                            var ini = document.getElementById("dateIni").value;
                            var fin = document.getElementById("dateEnd").value;
                            $.ajax({
                                url: "../../contenedores/jSesiones.php",
                                type: "post",
                                data: { fIni: ini, fFin: fin },
                                success: function (result) {
                                    //location.reload(); 
                                    //jQuery("#pnlGraficas").html(result);
                                }
                            }); 
                        }

                        function setTipoDatos() {
                            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                            var ini = document.getElementById("dateIni").value;
                            var fin = document.getElementById("dateEnd").value;
                            $.ajax({
                                url: "../../contenedores/jSesiones.php",
                                type: "post",
                                data: {  fIni: ini, fFin: fin },
                                success: function (result) { 
                                    location.reload(); 
                                    //jQuery("#pnlGraficas").html(result);
                                }
                            }); 
                        }
            
                        $(function() {
                            $("#dateIni").datepicker({
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                maxDate: new Date(),
                                //OYENTES DE LOS PICKERS 
                                onSelect: function(date) {
                                    //Cuando se seleccione una fecha se cierra el panel
                                    $("#ui-datepicker-div").hide();                                

                                    //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                                    var ini = document.getElementById("dateIni").value;
                                    var fin = document.getElementById("dateEnd").value; 

                                    //console.log("tipo: "+tipo+" op: "+op+" ini: "+ini+" fin: "+fin);
                                    if (new Date(fin) < new Date(date) ){
                                        alert("OPS! REVISAR RANGO DE FECHA ");
                                    } else {
                                        //Mandamos el ajax para la actualizacion de la tabla                                     
                                        $.ajax({
                                            url: "../../contenedores/jSesiones.php",
                                            type: "post",
                                            data: { fIni: ini, fFin: fin },
                                            success: function (result) { 
                                                location.reload(); 
                                            }
                                        }); 
                                    }
                                }
                            });

                            $("#dateEnd").datepicker({
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                //OYENTES DE LOS PICKERS 
                                maxDate: new Date(),
                                onSelect: function ( date ) { 
                                    //cuando se seleccione una fecha se cierra el panel
                                    $("#ui-datepicker-div").hide(); 
                                    //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                                    var ini = document.getElementById("dateIni").value;
                                    var fin = document.getElementById("dateEnd").value;                
                                    //console.log("tipo: "+tipo+" op: "+op+" ini: "+ini+" fin: "+fin);
                                    if (new Date(ini) > new Date(date) ) {
                                        alert("OPS! REVISAR RANGO DE FECHA ");
                                    } else {                                
                                        //mandamos el ajax para la actuilizacion de la tabla 
                                        $.ajax({
                                            url: "../../contenedores/jSesiones.php",
                                            type: "post",
                                            data: { fIni: ini, fFin: fin },
                                            success: function (result) { 
                                                location.reload(); 
                                            }
                                        });
                                    }
                                } 
                            }); 
                        }); 
                    </script>   
                   
                </form> 
            </div> 
        </a> 
    </head>
   
    <body >
        <div>
            <div id ="pnlHoja" style="overflow-y: auto;" >                 
                <div class="panel-group col-lg-12" id="accordion" style="margin-top: 5%">
                    <?php 
                            echo "<br>dat:",$fechaTIni,' a ',$fechaTFin; 
                    ?> 
                    <div class="panel panel-default"> 
                        <div class="panel-heading"> 
                            <h4 class="panel-title"> 
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Technical Losses</a> 
                            </h4> 
                        </div> 
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="demo-containerTec" style="width: 100%">
                                    <div id="grafTec" style="height: 25vh; margin-top: -1.2%; width: 100%">
                                        <script>
                                            $(function () {
                                                var isFirstLevel = true,
                                                    chartContainer = $("#grafTec"),
                                                    chart = chartContainer.dxChart({
                                                        dataSource: filterData(""),
                                                        series: {
                                                            type: "bar"
                                                        },
                                                        legend: {
                                                            visible: false
                                                        },
                                                        valueAxis: {
                                                            showZero: false
                                                        },
                                                        onPointClick: function (e) {
                                                            if (isFirstLevel) {
                                                                isFirstLevel = false;
                                                                removePointerCursor(chartContainer);
                                                                chart.option({
                                                                    dataSource: filterData(e.target.originalArgument)
                                                                });
                                                                $("#backButtonTec").dxButton("instance").option("visible", true);
                                                            }
                                                        },
                                                        tooltip: {
                                                            enabled: true,
                                                            customizeTooltip: function (arg) {
                                                                return {
                                                                    text: arg.argumentText + ": " + arg.valueText
                                                                };
                                                            }
                                                        },
                                                        customizePoint: function () {
                                                            var pointSettings = {
                                                                color: colors[Number(isFirstLevel)]
                                                            };

                                                            if (!isFirstLevel) {
                                                                pointSettings.hoverStyle = {
                                                                    hatching: "none"
                                                                };
                                                            }

                                                            return pointSettings;
                                                        }
                                                    }).dxChart("instance");

                                                $("#backButtonTec").dxButton({
                                                    text: "Back",
                                                    icon: "chevronleft",
                                                    visible: false,
                                                    onClick: function () {
                                                        if (!isFirstLevel) {
                                                            isFirstLevel = true;
                                                            addPointerCursor(chartContainer);
                                                            chart.option("dataSource", filterData(""));
                                                            this.option("visible", false);
                                                        }
                                                    }
                                                });
                                                addPointerCursor(chartContainer);
                                            });

                                            function filterData(name) {
                                                return data.filter(function (item) {
                                                    return item.parentID === name;
                                                });
                                            }

                                            function addPointerCursor(container) {
                                                container.addClass("pointer-on-bars");
                                            }

                                            function removePointerCursor(container) {
                                                container.removeClass("pointer-on-bars");
                                            }

                                            var colors = ["#1B4F72", "#21618C","#2874A6"];

                                            var data = [
                                                // PRIMER NIVEL
                                                <?php for ($i = 0; $i < $contTec; $i++ ) { ?> 
                                                    { arg: "<?php echo $opTec[$i],', ', $probTec[$i]; ?>", val: <?php echo $percentTec[$i]; ?>, parentID: "" }, 
                                                    // SEGUNDO NIVEL 
                                                    <?php for ($j = 0; $j < $sCTec[$i]; $j++){ ?> 
                                                        { arg: "<?php echo $subProbTec[$i][$j] ?>", val: <?php echo $subPercentTec[$i][$j] ?>, parentID: "<?php echo $opTec[$i],', ', $probTec[$i]; ?>" }, 
                                                        // TERCER NIVEL 
                                                        <?php for ($k = 0; $k < $sNPTec[$i][$j]; $k++) { ?> 
                                                            { arg: "<?php echo $noPartTec[$i][$j][$k] ?>", val: <?php echo $percentNoPartTec[$i][$j][$k] ?>, parentID: "<?php echo $subProbTec[$i][$j]; ?>" }, 
                                                        <?php } 
                                                    } 
                                                } ?>                     
                                            ];
                                        </script>
                                    </div>
                                    <div class="button-container">
                                        <div id="backButtonTec" style="visibility: visible"></div>
                                    </div>

                                    <!-- TABLA DE DESCRIPTIVA -->
                                    <table style="border: 1px solid #BABABA; width: 100%; font-size: 11px; margin-top: 15px" >
                                        <tr >
                                            <?php for ($i = 1; $i <= $contTec; $i++ ) { 
                                                if (strlen($opTec[$i-1]) > 1){ ?>
                                                    <th style="<?PHP if ($i%2 == 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center;" ><?php echo $opTec[$i-1],', ', $probTec[$i-1] ?></th>
                                                    <th style="<?PHP if ($i%2 == 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center; width: 2%; "> <?php echo $percentTec[$i-1].'%' ?> </th>                                         
                                            <?php } } ?>                                        
                                        </tr>
                                        <?php for ($i = 1; $i <= $contTec; $i++ ) { ?> 
                                        <tr> 
                                            <?php for ($j = 0; $j < 6; $j++) { ?> 
                                                <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA;"><?php echo $subProbTec[$j][$i-1] ?></td> 
                                                <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA; width: 2%; "><?php echo $subPercentTec[$j][$i-1].'%' ?></td> 
                                            <?php } ?> 
                                        </tr> 
                                        <?php } ?>
                                    </table> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Organizational Losses</a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse" > 
                            <div class="panel-body" > 
                                <!-- ORGANIZACIONALES --> 
                                <div class="demo-containerOrg" > 
                                    <div id="grafOrg" style="height: 25vh; margin-top: -1.2%; width: 1300%" > 
                                        <script> 
                                            $(function () {
                                                var isFirstLevel = true,
                                                    chartContainerOrg = $("#grafOrg"),
                                                    chart = chartContainerOrg.dxChart({
                                                        dataSource: filterDataOrg(""),
        //                                                title: "Organizational Losses",
                                                        series: {
                                                            type: "bar"
                                                        },
                                                        legend: {
                                                            visible: false
                                                        },
                                                        valueAxis: {
                                                            showZero: false
                                                        },
                                                        onPointClick: function (e) {
                                                            if (isFirstLevel) {
                                                                isFirstLevel = false;
                                                                removePointerCursorOrg(chartContainerOrg);
                                                                chart.option({
                                                                    dataSource: filterDataOrg(e.target.originalArgument)
                                                                });
                                                                $("#backButtonOrg").dxButton("instance").option("visible", true);
                                                            }
                                                        },
                                                        tooltip: {
                                                            enabled: true,
                                                            customizeTooltip: function (arg) {
                                                                return {
                                                                    text: arg.argumentText + ": " + arg.valueText
                                                                };
                                                            }
                                                        },
                                                        customizePoint: function () {
                                                            var pointSettings = {
                                                                color: colorsOrg[Number(isFirstLevel)]
                                                            };

                                                            if (!isFirstLevel) {
                                                                pointSettings.hoverStyle = {
                                                                    hatching: "none"
                                                                };
                                                            }
                                                            return pointSettings;
                                                        }
                                                    }).dxChart("instance");

                                                $("#backButtonOrg").dxButton({
                                                    text: "Back",
                                                    icon: "chevronleft",
                                                    visible: false,
                                                    onClick: function () {
                                                        if (!isFirstLevel) {
                                                            isFirstLevel = true;
                                                            addPointerCursorOrg(chartContainerOrg);
                                                            chart.option("dataSource", filterDataOrg(""));
                                                            this.option("visible", false);
                                                        }
                                                    }
                                                });
                                                addPointerCursorOrg(chartContainerOrg);
                                            });

                                            function filterDataOrg(name) { 
                                                return dataOrg.filter(function (item) { 
                                                    return item.parentID === name; 
                                                }); 
                                            } 

                                            function addPointerCursorOrg(container) {
                                                container.addClass("pointer-on-bars");
                                            }

                                            function removePointerCursorOrg(container) {
                                                container.removeClass("pointer-on-bars");
                                            }

                                            var colorsOrg = ["#F24130", "#FF5733","#FF6C4D"];

                                            var dataOrg = [
                                                // PRIMER NIVEL
                                                <?php for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) {  ?> 
                                                    { arg: "<?php echo $areaOrg[$i]; ?>", val: <?php echo $percentOrg[$i]; ?>, parentID: "" }, 
                                                    // SEGUNDO NIVEL 
                                                    <?php for ($j = 0; $j < $sCOrg[$i]; $j++){ ?> 
                                                        { arg: "<?php echo $subProbOrg[$i][$j]; ?>", val: <?php echo $subPercentOrg[$i][$j] ?>, parentID: "<?php echo $areaOrg[$i]; ?>" }, 
                                                        // TERCER NIVEL 
                                                        <?php for ($k = 0; $k < $sNPOrg[$i][$j]; $k++) { ?> 
                                                            { arg: "<?php echo $noPartOrg[$i][$j][$k] ?>", val: <?php echo $percentNoPartOrg[$i][$j][$k] ?>, parentID: "<?php echo $subProbOrg[$i][$j]; ?>" }, 
                                                        <?php } 
                                                    } 
                                                } ?> 
                                            ];
                                        </script>
                                    </div>
                                    <div class="button-containerOrg">
                                        <div id="backButtonOrg"></div>
                                    </div>
                                    <!-- TABLA DE DESCRIPTIVA -->
                                    <table style="border: 1px solid #BABABA; width: 100%; font-size: 11px; margin-top: 15px" >
                                        <tr >
                                            <?php for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) { ?>
                                                <th style="<?PHP if ($i%2 != 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center;" ><?php echo $areaOrg[$i]; ?></th>
                                                <th style="<?PHP if ($i%2 != 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center; width: 2%; "> <?php echo $percentOrg[$i].'%' ?> </th>                                         
                                            <?php } ?>

                                        </tr>
                                        <?php for ($i = 0; $i < 5;  $i++ ) { ?>
                                        <tr> 
                                            <?php for ($j = 0; $j < count($datTop3DiaOrgArea); $j++) { ?> 
                                                <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA;"><?php echo strtoupper(utf8_decode($subProbOrg[$j][$i])) ?></td> 
                                                <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA; width: 2%; "><?php echo $subPercentOrg[$j][$i].'%' ?></td> 
                                            <?php } ?> 
                                        </tr> 
                                        <?php } ?> 
                                    </table> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Quality Losses</a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="demo-containerCali">
                                    <div id="grafCali" style="height: 25vh; margin-top: -1.2%; width: 1400%" >
                                        <script>
                                            $(function () {
                                                var isFirstLevel = true,
                                                    chartContainerCali = $("#grafCali"),
                                                    chart = chartContainerCali.dxChart({
                                                        dataSource: filterDataCali(""),
                                                        title: "Quality Losses",
                                                        series: {
                                                            type: "bar"
                                                        },
                                                        legend: {
                                                            visible: false
                                                        },
                                                        valueAxis: {
                                                            showZero: false
                                                        },
                                                        onPointClick: function (e) {
                                                            if (isFirstLevel) {
                                                                isFirstLevel = false;
                                                                removePointerCursorCali(chartContainerCali);
                                                                chart.option({
                                                                    dataSource: filterDataCali(e.target.originalArgument)
                                                                });
                                                                $("#backButtonCali").dxButton("instance").option("visible", true);
                                                            }
                                                        },
                                                        tooltip: {
                                                            enabled: true,
                                                            customizeTooltip: function (arg) {
                                                                return {
                                                                    text: arg.argumentText + ": " + arg.valueText
                                                                };
                                                            }
                                                        },
                                                        customizePoint: function () {
                                                            var pointSettings = {
                                                                color: colorsCali[Number(isFirstLevel)]
                                                            };

                                                            if (!isFirstLevel) {
                                                                pointSettings.hoverStyle = {
                                                                    hatching: "none"
                                                                };
                                                            }
                                                            return pointSettings;
                                                        }
                                                    }).dxChart("instance");

                                                $("#backButtonCali").dxButton({
                                                    text: "Back",
                                                    icon: "chevronleft",
                                                    visible: false,
                                                    onClick: function () {
                                                        if (!isFirstLevel) {
                                                            isFirstLevel = true;
                                                            addPointerCursorCali(chartContainerCali);
                                                            chart.option("dataSource", filterDataCali(""));
                                                            this.option("visible", false);
                                                        }
                                                    }
                                                });
                                                addPointerCursorCali(chartContainerCali);
                                            });

                                            function filterDataCali(name) { 
                                                return dataCali.filter(function (item) { 
                                                    return item.parentID === name; 
                                                }); 
                                            } 

                                            function addPointerCursorCali(container) {
                                                container.addClass("pointer-on-bars");
                                            }

                                            function removePointerCursorCali(container) {
                                                container.removeClass("pointer-on-bars");
                                            }

                                            var colorsCali = ["#640000", "#7D1910","#913720"]; 

                                            var dataCali = [
                                                // PRIMER NIVEL
                                                <?php for ($i = 0; $i < count($datTop3DiaCaliArea); $i++ ) {  ?> 
                                                    { arg: "<?php echo $opCali[$i]; ?>", val: <?php echo $pzasCali[$i]; ?>, parentID: "" }, 
                                                    // SEGUNDO NIVEL 
                                                    <?php for ($j = 0; $j < $sCCali[$i]; $j++){ ?> 
                                                        { arg: "<?php echo $subProbCali[$i][$j]; ?>", val: <?php echo $subPzasCali[$i][$j] ?>, parentID: "<?php echo $opCali[$i]; ?>" },                             
                                                    <?php } 
                                                } ?> 
                                            ];
                                        </script>
                                    </div>
                                    <div class="button-containerCali"> 
                                        <div id="backButtonCali"></div> 
                                    </div> 
                                    <!-- TABLA DE DESCRIPTIVA -->
                                    <table style="border: 1px solid #BABABA; width: 100%; font-size: 11px; margin-top: 15px" >
                                        <tr >
                                            <?php for ($i = 0; $i < count($datTop3DiaCaliArea); $i++ ) { ?>
                                                <th style="<?PHP if ($i%2 != 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center;" ><?php echo strtoupper(utf8_decode($opCali[$i])); ?></th>
                                                <th style="<?PHP if ($i%2 != 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center; width: 2%; "> <?php echo $pzasCali[$i].'%' ?> </th>                                         
                                            <?php } ?>

                                        </tr>
                                        <?php for ($i = 0; $i < 5;  $i++ ) { ?>
                                        <tr> 
                                            <?php for ($j = 0; $j < count($datTop3DiaCaliArea); $j++) { ?> 
                                            <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA;"><?php echo strtoupper(utf8_decode($subProbCali[$j][$i])) ?></td> 
                                                <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA; width: 2%; "><?php echo $subPzasCali[$j][$i].'%' ?></td> 
                                            <?php } ?> 
                                        </tr> 
                                        <?php } ?>
                                    </table>                            
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>    
    </body>
</html>
