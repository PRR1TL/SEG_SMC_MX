<HTML>    
    <link REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen> 
    <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">-->
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script> 

    <!-- LIBRERIAS Y CONFIGURACION PARA TABLA -->
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script> 
    <script src="../../js/table-scroll.min.js"></script> 
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 

        function getFixedColumnsData() {} 

        function setTipoDatos() { 
            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON
            var dato = document.getElementById("tipoDatos").value; 

            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 
            $.ajax({ 
                url: "../../db/sesionReportes_1.php", 
                type: "post", 
                data: { tipoVista: 1 , tipoDato: 1, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    location.reload(); 
                } 
            }); 
        } 
        
    </script> 
    
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../db/ServerFunctions.php';
    session_start();
    $date = new DateTime;

    $fIni = date("Y-m-d", strtotime($_SESSION["FIni"])); 
    $fFin = date("Y-m-d", strtotime($_SESSION["FFin"])); 
    
    # MESES
    $lblMonth[1] = (string) "Jun"; 
    $lblMonth[2] = (string) "Feb"; 
    $lblMonth[3] = (string) "Mar"; 
    $lblMonth[4] = (string) "Apr"; 
    $lblMonth[5] = (string) "May"; 
    $lblMonth[6] = (string) "Jun"; 
    $lblMonth[7] = (string) "Jul"; 
    $lblMonth[8] = (string) "Aug"; 
    $lblMonth[9] = (string) "Sep"; 
    $lblMonth[10] = (string) "Oct"; 
    $lblMonth[11] = (string) "Nov"; 
    $lblMonth[12] = (string) "Dec"; 
    
    for ($i = 1; $i < 13; $i++) { 
        $tMonth[$i] = 0; 
        $contOpMonth[$i] = 0; 
    } 
    
    //CONSULTAS PARA MESES
    $cMiscMonth = cMiscellaneousMonthly($_SESSION['costCenterL'], $_SESSION['anio'] ); 
    for ($i = 0; $i < count($cMiscMonth); $i++ ){ 
        $month = $cMiscMonth[$i][0]; 
        $tMonth[$month] = $cMiscMonth[$i][1]; 
        
        $cCostMiscMonth = cCostCenterMiscMonthly($_SESSION['costCenterL'], $_SESSION['anio'], $month); 
        $contOpMonth[$month] = count($cCostMiscMonth);
        for ($j = 0; $j < count($cCostMiscMonth); $j++) { 
            $opMonth[$month][$j] = $lblMonth[$month].'_'.$cCostMiscMonth[$j][0]; 
            $tOpMonth[$month][$j] = $cCostMiscMonth[$j][1]; 
            
            $cReasonMiscMonth = cReasonMiscMonthly($_SESSION['costCenterL'], $cCostMiscMonth[$j][0], $_SESSION['anio'], $month); 
            $contSCodOpMonth[$month][$j] = count($cReasonMiscMonth);
            for ($k = 0; $k < count($cReasonMiscMonth); $k++ ){ 
                $sCodMonth[$month][$j][$k] = $cReasonMiscMonth[$k][0]; 
                $tCodOpMonth[$month][$j][$k] = $cReasonMiscMonth[$k][1]; 
            } 
        } 
    }
    
    #SEMANAL
    $date->setISODate($_SESSION['anio'], 53);
        
    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
    if($date->format("W") == 53){ 
        $numSemanas = 53; 
    } else { 
        $numSemanas = 52; 
    } 
    
    //ULTIMO DIA DEL MES
    $ultimoDiaMes = date("t",mktime(0,0,0,$_SESSION['mes'],1,$_SESSION['anio'])); 
    
    #DIA DE LAS SEMANAS
    $sP = date("W",mktime(0,0,0,$_SESSION['mes'],01,$_SESSION['anio']));//date("w",mktime(0,0,0,$_SESSION['mes'],01,$_SESSION['anio']));
    $sL = date("W",mktime(0,0,0,$_SESSION['mes'],$ultimoDiaMes,$_SESSION['anio'])); 
    
    //INICIALIZAMOS VARIABLES PARA SEMANA 
    if ($sP > $sL){ 
        for ($i = $sP; $i <= $numSemanas; $i++ ) { 
            $lblWeek[$i] = "CW-".$i; 
            $contOpWeek[$i] = 0; 
            $tWeek[$i] = 0; 
        } 

        for($i = 1; $i <= $sL; $i++) { 
            $lblWeek[$i] = "CW-".$i; 
            $contOpWeek[$i] = 0; 
            $tWeek[$i] = 0; 
        } 
    } else { 
        for ($i = $sP; $i <= $sL; $i++) { 
            $lblWeek[$i] = "CW-".$i; 
            $contOpWeek[$i] = 0; 
            $tWeek[$i] = 0; 
        } 
    } 
    
    $dSI = date("w",mktime(0,0,0,$_SESSION['mes'],01,$_SESSION['anio']));
    $dSL = date("w",mktime(0,0,0,$_SESSION['mes'],$ultimoDiaMes,$_SESSION['anio']));

    if ($sP == 0)
        $dSI = 7; 

    if ($sL == 0) 
        $dSL = 7; 

    $fP = date("Y-m-d",mktime(0,0,0,$_SESSION['mes'],01-$dSI,$_SESSION['anio'])); 
    $fL = date("Y-m-d",mktime(0,0,0,$_SESSION['mes'],$ultimoDiaMes+(6-$dSL),$_SESSION['anio'])); 
    
    $cMiscWeek = cMiscellaneousWeekly($_SESSION['costCenterL'], $fP, $fL);
    for ($i = 0; $i < count($cMiscWeek); $i++){ 
        $nW = $cMiscWeek[$i][0];
        $tWeek[$nW] = $cMiscWeek[$i][1];        
        $contOpWeek[$nW] = 0;
        $cCostMiscWeek = cCostCenterMiscWeekly($_SESSION['costCenterL'], $_SESSION['anio'], $nW);
        $contOpWeek[$nW] = count($cCostMiscWeek);
        for ($j = 0; $j < count($cCostMiscWeek);$j++){
            $opWeek[$nW][$j] = $lblWeek[$nW].'_'.$cCostMiscWeek[$j][0]; 
            $tOpWeek[$nW][$j] = $cCostMiscWeek[$j][1];
            $cReasonMiscWeek = cReasonMiscWeekly($_SESSION['costCenterL'], $cCostMiscWeek[$j][0], $_SESSION['anio'], $nW);
            $contSCodOpWeek[$nW][$j] = count($cReasonMiscWeek);
            for ($k = 0; $k < count($cReasonMiscWeek); $k++ ){ 
                $sCodWeek[$nW][$j][$k] = $cReasonMiscWeek[$k][0];
                $tCodWeek[$nW][$j][$k] = $cReasonMiscWeek[$k][1];
            } 
        } 
    } 
    
    # PERIODO DE DIAS 
    $sTotalD = 0;
    for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 

        $tDay[$vDate] = 0; 
        $contOpDay[$vDate] = 0; 
        $lblday[$vDate] = date("M d", strtotime($i));
    } 
    
    $cMiscDay = cMiscellaneousDaily($_SESSION['costCenterL'], $fIni, $fFin); 
    for ($i = 0; $i < count($cMiscDay); $i++ ) { 
        $date = explode("-", $cMiscDay[$i][0]); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        $tDay[$vDate] = $cMiscDay[$i][1]; 
        $contOpDay[$vDate] = 0; 
        $sTotalD += $tDay[$vDate]; 
        
        $cCostMiscDay = cCostCenterMiscDaily($_SESSION['costCenterL'], $cMiscDay[$i][0]);
        $contOpDay[$vDate] = count($cCostMiscDay);
        for ($j = 0; $j < count($cCostMiscDay); $j++){ 
            $opDay[$vDate][$j] = $lblday[$vDate].'_'.$cCostMiscDay[$j][0];
            $tOpDay[$vDate][$j] = $cCostMiscDay[$j][1]; 
            
            $cReasonMiscDay = cReasonMiscDaily($cCostMiscDay[$j][0], $cMiscDay[$i][0]);
            $contSCodOpDay[$vDate][$j] = count($cReasonMiscDay);
            for ($k = 0; $k < count($cReasonMiscDay); $k++){
                $sCodDay[$vDate][$j][$k] = $cReasonMiscDay[$k][0];
                $tCodOpDay[$vDate][$j][$k] = $cReasonMiscDay[$k][1]; 
            } 
        } 
    } 
      
    $dias = (strtotime($fIni)- strtotime($fFin))/86400;
    $dias = abs($dias); 
    $dias = floor($dias)+1; 
    //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
    //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS 
    if ($dias > 11 ) { 
        $rowspan = 12;
    } else { 
        //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
        //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS 
        $rowspan = $dias;
    } 

?>

<body> 
    <div style="width: 96%; margin-left: 2%" > 
        <div id="jTMonth" name="jTMonth" class="jidokaMonth" > 
            <script>
                var dataMonth = [
                <?php  for($i = 1; $i <= 12; $i++) {  ?> 
                { 
                    "category": "<?php echo $lblMonth[$i]; ?>", 
                    "income": <?php echo $tMonth[$i] ?>, 
                    "url":"#", 
                    "description":"click to drill-down", 
                    "months": [ 
                    <?php for ($j = 0; $j < $contOpMonth[$i]; $j++){ ?> 
                        { 
                            "category": "<?php echo $opMonth[$i][$j]; ?>", 
                            "income": <?php echo $tOpMonth[$i][$j]; ?>, 
                            "url":"#", 
                            "description":"click to drill-down", 
                            "nivel3": [ 
                            <?php for ($k = 0; $k < $contSCodOpMonth[$i][$j]; $k++ ){ ?> 
                                { "category": "<?php echo $sCodMonth[$i][$j][$k]; ?>", 
                                  "income": <?php echo $tCodOpMonth[$i][$j][$k]; ?> 
                                },
                            <?php } ?> 
                            ] 
                        }, 
                    <?php } ?> 
                    ] 
                },
                <?php } ?>
                ];

                var chartMonth = AmCharts.makeChart("jTMonth", {
                    "type": "serial",
                    "theme": "none",
                    "pathToImages": "/lib/3/images/",
                    "autoMargins": false,
                    "marginLeft": 53,
                    "marginRight": 0,
                    "marginTop": 10,
                    "marginBottom": 26,
                    "titles": [{
                        "text": ""
                    }],
                    "dataProvider": dataMonth,
                    "graphs": [{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Income",
                        "lineColor": "#fddb35",
                        "type": "column",
                        "valueField": "income","urlField":"url"
                    }],
                    "categoryField": "category",
                    "categoryAxis": { 
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0
                    }, "chartScrollbar": {
                        "graph": "g2",
                        "oppositeAxis": false,
                        "offset": 30,
                        "scrollbarHeight": 20,
                        "backgroundAlpha": 0,
                        "selectedBackgroundAlpha": 0.1,
                        "selectedBackgroundColor": "#888888",
                        "graphFillAlpha": 0,
                        "graphLineAlpha": 0.5,
                        "selectedGraphFillAlpha": 0,
                        "selectedGraphLineAlpha": 1,
                        "autoGridCount": true,
                        "color": "#AAAAAA"
                    },
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2
                    }

                });

                chartMonth.addListener("clickGraphItem", function (event) {
                    if ( 'object' === typeof event.item.dataContext.months ) {
                        // set the monthly data for the clicked month
                        event.chart.dataProvider = event.item.dataContext.months;
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';
                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGMonth();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                    if ( 'object' === typeof event.item.dataContext.nivel3 ) {

                        // set the monthly data for the clicked month 
                        event.chart.dataProvider = event.item.dataContext.nivel3; 
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';

                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGMonth2();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                });

                // function which resets the chart back to yearly data
                function resetGMonth() {
                    chartMonth.dataProvider = dataMonth;
                    chartMonth.titles[0].text = '';

                    // remove the "Go back" label
                    chartMonth.allLabels = [];

                    chartMonth.validateData();
                    chartMonth.animateAgain();
                }

                function resetGMonth2() {
                    chartMonth.dataProvider = dataMonth; 
                    chartMonth.titles[0].text = ''; 

                    // remove the "Go back" label 
                    chartMonth.allLabels = []; 

                    chartMonth.validateData(); 
                    chartMonth.animateAgain(); 
                }
            </script>
        </div>

        <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
            <script>
                var dataWeek = [
                <?php  if ($sP > $sL){ 
                    for ($i = $sP; $i <= $numSemanas; $i++ ) {
                ?>
                    {
                        "category": "<?php echo $lblWeek[$i]; ?>",
                        "income": <?php echo $tWeek[$i] ?>,
                        "url":"#",
                        "description":"click to drill-down",
                        "months": [
                        <?php for ($j = 0; $j < $contOpWeek[$i]; $j++) { ?>
                            { 
                                "category": "<?php echo $opWeek[$i][$j]; ?>", 
                                "income": <?php echo $tOpWeek[$i][$j]; ?>,
                                "url":"#",
                                "description":"click to drill-down",
                                "nivel3": [
                                <?php for ($k = 0; $k < $contSCodOpWeek[$i][$j]; $k++){ ?>
                                    { "category": "<?php echo $sCodWeek[$i][$j][$k]; ?>", 
                                      "income": <?php echo $tCodWeek[$i][$j][$k]; ?> 
                                    },
                                <?php } ?>
                                ] 
                            },
                        <?php } ?>                
                        ]
                    },                
                <?php
                    } 
                    for($i = 1; $i <= $sL; $i++) {
                    ?>
                    {
                        "category": "<?php echo $lblWeek[$i]; ?>",
                        "income": <?php echo $tWeek[$i] ?>,
                        "url":"#",
                        "description":"click to drill-down",
                        "months": [
                        <?php for ($j = 0; $j < $contOpWeek[$i]; $j++) { ?>
                            { 
                                "category": "<?php echo $opWeek[$i][$j]; ?>", 
                                "income": <?php echo $tOpWeek[$i][$j]; ?>,
                                "url":"#",
                                "description":"click to drill-down",
                                "nivel3": [
                                <?php for ($k = 0; $k < $contSCodOpWeek[$i][$j]; $k++){ ?>
                                    { "category": "<?php echo $sCodWeek[$i][$j][$k]; ?>", 
                                      "income": <?php echo $tCodWeek[$i][$j][$k]; ?> 
                                    },
                                <?php } ?>
                                ] 
                            },
                        <?php } ?>                
                        ]
                    },
                <?php 
                    }  
                } else { 
                    for ($i = $sP; $i <= $sL; $i++) {
                    ?>    
                    {
                        "category": "<?php echo $lblWeek[$i]; ?>",
                        "income": <?php echo $tWeek[$i] ?>,
                        "url":"#",
                        "description":"click to drill-down",
                        "months": [
                        <?php for ($j = 0; $j < $contOpWeek[$i]; $j++) { ?>
                            { 
                                "category": "<?php echo $opWeek[$i][$j]; ?>", 
                                "income": <?php echo $tOpWeek[$i][$j]; ?>, 
                                "url":"#", 
                                "description":"click to drill-down", 
                                "nivel3": [
                                <?php for ($k = 0; $k < $contSCodOpWeek[$i][$j]; $k++){ ?> 
                                    { "category": "<?php echo $sCodWeek[$i][$j][$k]; ?>", 
                                      "income": <?php echo $tCodWeek[$i][$j][$k]; ?> 
                                    },
                                <?php } ?>
                                ] 
                            },
                        <?php } ?>                
                        ]
                    },
                <?php }
                } ?>
                ];

                var chartWeek = AmCharts.makeChart("jTWeek", {
                    "type": "serial",
                    "theme": "none",
                    "pathToImages": "/lib/3/images/",
                    "autoMargins": false,
                    "marginLeft": 53,
                    "marginRight": 0,
                    "marginTop": 10,
                    "marginBottom": 26,
                    "titles": [{
                        "text": ""
                    }],
                    "dataProvider": dataWeek,
                    "graphs": [{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Income",
                        "lineColor": "#fddb35",
                        "type": "column",
                        "valueField": "income","urlField":"url"
                    }],
                    "categoryField": "category",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0
                    }, "chartScrollbar": {
                        "graph": "g2",
                        "oppositeAxis": false,
                        "offset": 30,
                        "scrollbarHeight": 20,
                        "backgroundAlpha": 0,
                        "selectedBackgroundAlpha": 0.1,
                        "selectedBackgroundColor": "#888888",
                        "graphFillAlpha": 0,
                        "graphLineAlpha": 0.5,
                        "selectedGraphFillAlpha": 0,
                        "selectedGraphLineAlpha": 1,
                        "autoGridCount": true,
                        "color": "#AAAAAA"
                    },
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2
                    }

                });

                chartWeek.addListener("clickGraphItem", function (event) {
                    if ( 'object' === typeof event.item.dataContext.months ) {
                        // set the monthly data for the clicked month
                        event.chart.dataProvider = event.item.dataContext.months;
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';
                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGWeek();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                    if ( 'object' === typeof event.item.dataContext.nivel3 ) {

                        // set the monthly data for the clicked month 
                        event.chart.dataProvider = event.item.dataContext.nivel3; 
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';

                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGWeek2();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                });
                    
                // function which resets the chart back to yearly data
                function resetGWeek() {
                    chartWeek.dataProvider = dataWeek;
                    chartWeek.titles[0].text = '';

                    // remove the "Go back" label
                    chartWeek.allLabels = [];

                    chartWeek.validateData();
                    chartWeek.animateAgain();
                }

                function resetGWeek2() {
                    chartWeek.dataProvider = dataWeek; 
                    chartWeek.titles[0].text = 'IFK'; 

                    // remove the "Go back" label 
                    chartWeek.allLabels = []; 

                    chartWeek.validateData(); 
                    chartWeek.animateAgain(); 
                }
            </script>
        </div>
        
        <!-- TABLA DE MESES -->
        <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA; font-size: 13px;" >
            <tr >
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >Mes</th>
                <?php for ($i = 1; $i < 13; $i++ ) { ?>
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblMonth[$i] ?></th>
                <?php } ?> 
            </tr> 
            <tr> 
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >IFK</th>
                <?php for ($i = 1; $i < 13; $i++ ) { ?>
                    <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $tMonth[$i] ?></td>      
                <?php } ?>
            </tr>                
        </table>

        <!-- TABLA DE SEMANA -->
        <table style="width: 35%; margin-left: 63%; margin-top: -70px; border: 1px solid #BABABA; font-size: 13px;">
            <tr>
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                <?php 
                if ($sP > $sL){
                    for ($i = $sP; $i <= $numSemanas; $i++ ) { ?>
                        <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblWeek[$i] ?></th>
                    <?php } 
                     for($i = 1; $i <= $sL; $i++) {
                    ?>
                        <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblWeek[$i] ?></th>
                    <?php
                    }
                } else {                        
                    for ($i = $sP; $i <= $sL; $i++) { ?>
                        <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblWeek[$i] ?></th>
                <?php }} ?>                            
            </tr>
            <tr>    
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">IFK</th>
                <?php 
                if ($sP > $sL){
                    for ($i = $sP; $i <= $numSemanas; $i++ ) { ?>
                        <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tWeek[$i] ?></td>
                    <?php } 
                    for($i = 1; $i <= $sL; $i++) { 
                    ?>
                        <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tWeek[$i] ?></td>
                    <?php
                    }
                } else {                        
                    for ($i = $sP; $i <= $sL; $i++) { ?>
                        <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tWeek[$i] ?></td>
                <?php }} ?>  
            </tr>                
        </table>  
        <br>        
        
        <div id="grafDays" name="grafDays" class="jidokaDay" >
            <script>
                var dataDay = [
                <?php  for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                    $date = explode("-", $i); 
                    $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?>    
                {
                    "category": "<?php echo $lblday[$vDate]; ?>",
                    "income": <?php echo $tDay[$vDate] ?>,
                    "url":"#",
                    "description":"click to drill-down",
                    "months": [
                    <?php for ($j = 0; $j < $contOpDay[$vDate]; $j++){ ?>
                        { 
                            "category": "<?php echo $opDay[$vDate][$j]; ?>", 
                            "income": <?php echo $tOpDay[$vDate][$j]; ?>,
                            "url":"#",
                            "description":"click to drill-down",
                            "nivel3": [
                            <?php for ($k = 0; $k < $contSCodOpDay[$vDate][$j]; $k++ ){ ?>
                                { "category": "<?php echo $sCodDay[$vDate][$j][$k]; ?>", 
                                  "income": <?php echo $tCodOpDay[$vDate][$j][$k]; ?> 
                                },
                            <?php } ?>
                            ] 
                        },
                    <?php } ?>                
                    ]
                },
                <?php } ?>
                ];

                var chartDay = AmCharts.makeChart("grafDays", {
                    "type": "serial",
                    "theme": "none",
                    "pathToImages": "/lib/3/images/",
                    "autoMargins": false,
                    "marginLeft": 53,
                    "marginRight": 0,
                    "marginTop": 10,
                    "marginBottom": 26,
                    "titles": [{
                        "text": ""
                    }],
                    "dataProvider": dataDay,
                    "graphs": [{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Income",
                        "lineColor": "#fddb35",
                        "type": "column",
                        "valueField": "income","urlField":"url"
                    }],
                    "categoryField": "category",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0
                    }, "chartScrollbar": {
                        "graph": "g2",
                        "oppositeAxis": false,
                        "offset": 30,
                        "scrollbarHeight": 20,
                        "backgroundAlpha": 0,
                        "selectedBackgroundAlpha": 0.1,
                        "selectedBackgroundColor": "#888888",
                        "graphFillAlpha": 0,
                        "graphLineAlpha": 0.5,
                        "selectedGraphFillAlpha": 0,
                        "selectedGraphLineAlpha": 1,
                        "autoGridCount": true,
                        "color": "#AAAAAA"
                    },
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2
                    }

                });

                chartDay.addListener("clickGraphItem", function (event) {
                    if ( 'object' === typeof event.item.dataContext.months ) {
                        // set the monthly data for the clicked month
                        event.chart.dataProvider = event.item.dataContext.months;
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';
                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGDay();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                    if ( 'object' === typeof event.item.dataContext.nivel3 ) {

                        // set the monthly data for the clicked month 
                        event.chart.dataProvider = event.item.dataContext.nivel3; 
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';

                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGDay2();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                });

                // function which resets the chart back to yearly data
                function resetGDay() {
                    chartDay.dataProvider = dataDay;
                    chartDay.titles[0].text = '';

                    // remove the "Go back" label
                    chartDay.allLabels = [];

                    chartDay.validateData();
                    chartDay.animateAgain();
                }

                function resetGDay2() {
                    chartDay.dataProvider = dataDay; 
                    chartDay.titles[0].text = ''; 

                    // remove the "Go back" label 
                    chartDay.allLabels = []; 

                    chartDay.validateData(); 
                    chartDay.animateAgain(); 
                }
            </script>
        </div>
        
        <!-- APARTADO PARA PICKERS --> 
        <div id="pnlPikers" style="margin-top: 34%; " > 
            <script>
                $(function() { 
                    $("#dateIni").datepicker({ 
                        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                        maxDate: new Date(), 
                        //OYENTES DE LOS PICKERS 
                        onSelect: function(date) { 
                            //Cuando se seleccione una fecha se cierra el panel
                            $("#ui-datepicker-div").hide(); 
                        } 
                    }); 

                    $("#dateEnd").datepicker({ 
                        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                        maxDate: new Date(), 
                        //OYENTES DE LOS PICKERS 
                        onSelect: function ( date ) { 
                            //cuando se seleccione una fecha se cierra el panel 
                            $("#ui-datepicker-div").hide(); 
                        } 
                    }); 
                }); 
            </script> 

            <div >
                <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" >                    
                    <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3 contenidoCentrado input-group" style="margin-left: 11%; margin-top: -2.8%" > 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo date("m/d/Y", strtotime($fIni)); ?>">
                        <span class="input-group-addon"> a </span> 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($fFin)); ?>">
                    </div>
                    <div class="col-xs-1 col-sh-1 col-md-1 col-lg-1 contenidoCentrado" style="margin-left: 34%; margin-top: -2.8%" > 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0;" onclick="setTipoDatos()"> 
                            <img src="../../imagenes/confirmar.png"> 
                        </button> 
                    </div> 
                    <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2 contenidoCentrado" style="margin-left: 87%; margin-top: -2.3%"> 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0; " onclick="setTipoDatos2()"> 
                            <img src="../../imagenes/bAll.png"> 
                        </button> 
                    </div> 
                    <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2 contenidoCentrado" > 
                        
                    </div> 
                </div> 
            </div> 
        </div> 
        <br>        
        
        <div id="holder-semple-1">
            <script id="tamplate-semple-1" type="text/mustache"> 
                <table style="width:97%; margin-left: 1.5%; font-size: 13px;" class="inner-table"> 
                    <thead> 
                        <tr> 
                            <td colspan="2"> </td> 
                            <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                            <td rowspan="2" align="center" >Total</td> 
                        </tr> 
                        <tr> 
                            <td>&nbsp;</td> 
                            <td align="center" >PROBLEMA</td>
                            <?php for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
                                $date = explode("-", $i); 
                                $vDate = $date[0].$date[1].(int)$date[2]; ?>
                                <td align="center" ><?php echo $lblday[$vDate]; ?></td>
                            <?php } ?> 
                        </tr>
                    </thead>
                    <tbody>                            
                        <tr >
                            <td align="right" > </td>
                            <td >IFK</td>
                            <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                                $date = explode("-", $j); 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                            ?>
                            <td align="center" ><?php echo $tDay[$vDate]; ?></td>                            
                            <?php } ?>
                            <td align="center" ><?php echo $sTotalD; ?></td> 
                        </tr >                            
                    </tbody>
                    <tfoot>

                    </tfoot> 
                </table> 
            </script>
        </div> 
    </div>
</body>

