<HTML>    
    <link REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen> 
    <script src="//www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="//www.amcharts.com/lib/3/serial.js"></script> 
    
    <!-- LIBRERIAS Y CONFIGURACION PARA TABLA --> 
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script> 
    <script src="../../js/table-scroll.min.js"></script> 
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
        
        function setTipoDatos() { 
            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON
            var dato = document.getElementById("tipoDatos").value; 

            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 
            $.ajax({ 
                url: "../../db/sesionReportes_1.php", 
                type: "post", 
                data: { tipoVista: 1 , tipoDato: dato, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    location.reload(); 
                } 
            }); 
        } 
        
    </script> 
    
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../db/ServerFunctions.php';
    session_start();
    $date = new DateTime;

    $costCenter = $_SESSION["costCenterL"]; 
    $fIni = date("Y-m-d", strtotime($_SESSION["FIni"])); 
    $fFin = date("Y-m-d", strtotime($_SESSION["FFin"])); 
    
    $mes = date("m", strtotime($fIni));    
    $anio = date("Y", strtotime($fFin));

    # MESES
    $lblMonth[1] = (string) "Jun";
    $lblMonth[2] = (string) "Feb";
    $lblMonth[3] = (string) "Mar";
    $lblMonth[4] = (string) "Apr";
    $lblMonth[5] = (string) "May";
    $lblMonth[6] = (string) "Jun";
    $lblMonth[7] = (string) "Jul";
    $lblMonth[8] = (string) "Aug";
    $lblMonth[9] = (string) "Sep";
    $lblMonth[10] = (string) "Oct";
    $lblMonth[11] = (string) "Nov";
    $lblMonth[12] = (string) "Dec";
    
    for ($i = 1; $i < 13; $i++) { 
        $valMonthIFK[$i] = 0; 
        $valMonthMisc[$i] = 0; 
        $targetM[$i] = 0; 
    } 
    
    #SEMANAL
    $date->setISODate("$anio", 53);
        
    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
    if($date->format("W") == 53){ 
        $numSemanas = 53; 
    } else { 
        $numSemanas = 52; 
    } 
    
    //ULTIMO DIA DEL MES
    $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio)); 
    
    #DIA DE LAS SEMANAS
    $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
    $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

    //INICIALIZAMOS VARIABLES PARA SEMANA
    if ($sP > $sL){
        for ($i = $sP; $i <= $numSemanas; $i++ ) {
            $lblWeek[$i] = "CW-".$i;
            $valWeekIFK[$i] = 0;
            $valWeekMisc[$i] = 0;
            $targetW[$i] = 0; 
        }

        for($i = 1; $i <= $sL; $i++) {
            $lblWeek[$i] = "CW-".$i;
            $valWeekIFK[$i] = 0;
            $valWeekMisc[$i] = 0; 
            $targetW[$i] = 0; 
        }            
    } else {
        for ($i = $sP; $i <= $sL; $i++) {
            $lblWeek[$i] = "CW-".$i;
            $valWeekIFK[$i] = 0;
            $valWeekMisc[$i] = 0; 
            $targetW[$i] = 0; 
        } 
    } 
    
    $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
    $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

    if ($sP == 0)
        $dSI = 7; 

    if ($sL == 0) 
        $dSL = 7; 

    $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio)); 
    $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));     
    
    # PERIODO DE DIAS 
    $sTotalDIFK = 0;
    $sTotalDMisc = 0;
    for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 

        $valDayIFK[$vDate] = 0; 
        $valDayMisc[$vDate] = 0; 
        $valCummDay[$vDate] = 0;
        $lblday[$vDate] = date("M d", strtotime($i)); 
        $targetD[$vDate] = 0;
        
    } 

    $titulo = "COST-CENTER ".$costCenter; 
    //CONSULTAS PARA MESES POR COSTCENTER 
    $cIFKMonth = cIFKCostMonthG($costCenter, $anio); 
    for($i = 0; $i < count($cIFKMonth); $i++) { 
        $month = $cIFKMonth[$i][0]; 
        $valMonthIFK[$month] = $cIFKMonth[$i][1]; 
    } 

    $cIFKWeek = cIFKCostWeekG($costCenter, $fP, $fL); 
    for($i = 0; $i < count($cIFKWeek); $i++) { 
        $nW = $cIFKWeek[$i][0]; 
        $valWeekIFK[$nW] = $cIFKWeek[$i][1]; 
    } 

    $cIFKDay = cIFKCostDayG($costCenter, $fIni, $fFin); 
    for($i = 0; $i < count($cIFKDay); $i++) { 
        $date = explode("-", $cIFKDay[$i][0]); 
        $vDate = $date[0].$date[1].(int)$date[2]; 

        $valDayIFK[$vDate] = $cIFKDay[$i][1]; 
        $sTotalDIFK += $valDayIFK[$vDate]; 
    } 

    #MISCELLANEOUS 
    $cMiscMonth = cMiscCostMonthG($costCenter, $anio); 
    for ($i = 0; $i < count($cMiscMonth); $i++ ) { 
        $month = $cMiscMonth[$i][0]; 
        $valMonthMisc[$month] = $cMiscMonth[$i][1]; 
    } 

    $cMiscWeek = cMiscCostWeekG($costCenter, $fP, $fL); 
    for ($i = 0; $i < count($cMiscWeek); $i++) { 
        $nW = $cMiscWeek[$i][0]; 
        $valWeekMisc[$nW] = $cMiscWeek[$i][1]; 
    } 

    $cMiscDay = cMiscCostDayG($costCenter, $fIni, $fFin); 
    for ($i = 0; $i < count($cMiscDay); $i++ ) { 
        $date = explode("-", $cMiscDay[$i][0]); 
        $vDate = $date[0].$date[1].(int)$date[2]; 

        $valDayMisc[$vDate] = $cMiscDay[$i][1]; 
        $sTotalDMisc += $valDayMisc[$vDate]; 
    } 

    for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 

        $valCummDay[$vDate] = $valDayMisc[$vDate] + $valDayIFK[$vDate]; 
    }
    
    //TARGETS 
    $cTargetMes = cTargetScrapMonth($_SESSION['linea'], $anio); 
    $cTargetSemana = cTargetScrapWeek($_SESSION['linea'], $fP, $fL); 
    $cTargetDia = cTargetScrapDay($_SESSION['linea'], $fIni, $fFin); 
    
    for ($i = 0; $i < count($cTargetMes) ; $i++){
        $m = $cTargetMes[$i][0]; 
        $targetM[$m] = $cTargetMes[$i][1]; 
    }
    
    for ($i = 0; $i < count($cTargetSemana); $i++){
        $w = $cTargetSemana[$i][0]; 
        $targetW[$w] = $cTargetSemana[$i][1]; 
    }
    
    for ($i = 0; $i < count($cTargetDia); $i++ ){
        $date = explode("-", $cTargetDia[$i][0]); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $targetD[$vDate] = $cTargetDia[$i][1]; 
    }
    
    $tTabla = $sTotalDMisc + $sTotalDIFK; 
    
    $dias = (strtotime($fIni) - strtotime($fFin))/86400;
    $dias = abs($dias); 
    $dias = floor($dias)+1; 
    //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
    //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS 
    if ($dias > 11 ) { 
        $rowspan = 12; 
    } else { 
        //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
        //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS 
        $rowspan = $dias; 
    } 
?>

<body id="pnlHoja" > 
    <div > 
        <div class="col-lg-12 col-md-12 col-xs-12 contenidoCentrado" >
            <h6 align="center" class="contenidoCentrado"><strong><?php echo $titulo ?></strong></h6>
        </div>
        
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" style="margin-top: 0.5%" > 
            <div id="jTMonth" name="jTMonth" class="jidokaMonth" > 
                <script>
                    var chart = AmCharts.makeChart("jTMonth", {
                        "type": "serial", 
                        "theme": "none", 
                        "legend": { 
                            "autoMargins": false,
                            "borderAlpha": 0.2,
                            "equalWidths": false,
                            "horizontalGap": 20,
                            "verticalGap": 5,
                            "markerSize": 10,
                            "useGraphSettings": true,
                            "valueAlign": "left",
                            "valueWidth": 0
                        }, 
                        "dataProvider": [                     
                            <?php for ($i = 1; $i <= 12; $i++ ) { ?>
                            {
                                "month": "<?php echo $lblMonth[$i];?> ", 
                                "ifk": <?php echo $valMonthIFK[$i]; ?>, 
                                "misc": <?php echo $valMonthMisc[$i]; ?>, 
                                "meta": <?php echo $targetM[$i]; ?> 
                            }, 
                            <?php } ?> 
                       ], 
                        "valueAxes": [{ 
                            "stackType": "regular", 
                            "axisAlpha": 0.3, 
                            "gridAlpha": 0.2 
                        }], 
                        "graphs": [{ 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 0.8, 
                            "labelText": "[[value]]", 
                            "lineAlpha": 0.3, 
                            "title": "IFK", 
                            "type": "column", 
                            "color": "#000000", 
                            "valueField": "ifk" 
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>", 
                            "fillAlphas": 0.8, 
                            "labelText": "[[value]]", 
                            "lineAlpha": 0.3, 
                            "title": "Miscellaneous", 
                            "type": "column", 
                            "color": "#000000", 
                            "valueField": "misc" 
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>", 
                            "lineThickness": 2, 
                            "lineColor": "#62cf73",
                            "title": "Meta", 
                            "type": "line",  
                            "valueField": "meta" 
                        }], 
                        "categoryField": "month", 
                        "categoryAxis": { 
                            "gridPosition": "start", 
                            "axisAlpha": 0, 
                            "gridAlpha": 0.2, 
                            "position": "left" 
                        }, 
                        "export": { 
                            "enabled": true 
                        } 
                    });                     
                </script>
            </div>

            <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                <script>                
                    var chart = AmCharts.makeChart("jTWeek", {
                        "type": "serial", 
                        "theme": "none", 
                        "legend": { 
                            "autoMargins": false,
                            "borderAlpha": 0.2,
                            "equalWidths": false,
                            "horizontalGap": 10,
                            "verticalGap": 5,
                            "markerSize": 10,
                            "useGraphSettings": true,
                            "valueAlign": "left",
                            "valueWidth": 0
                        }, 
                        "dataProvider": [ 
                        <?php  if ($sP > $sL){ 
                            for ($i = $sP; $i <= $numSemanas; $i++ ) { 
                            ?>
                            {
                                "week": "<?php echo $lblWeek[$i];?> ",
                                "ifk": <?php echo $valWeekIFK[$i]; ?>,
                                "misc": <?php echo $valWeekMisc[$i]; ?>,
                                "meta": <?php echo $targetW[$i]; ?>
                            }, 
                            <?php } 
                            for($i = 1; $i <= $sL; $i++) {
                            ?> 
                            {
                                "week": "<?php echo $lblWeek[$i];?> ",
                                "ifk": <?php echo $valWeekIFK[$i]; ?>,
                                "misc": <?php echo $valWeekMisc[$i]; ?>,
                                "meta": <?php echo $targetW[$i]; ?>
                            }, 
                            <?php }
                        } else {
                            for ($i = $sP; $i <= $sL; $i++) {
                        ?> 
                            {
                                "week": "<?php echo $lblWeek[$i];?> ",
                                "ifk": <?php echo $valWeekIFK[$i]; ?>,
                                "misc": <?php echo $valWeekMisc[$i]; ?>,
                                "meta": <?php echo $targetW[$i]; ?>
                            }, 
                        <?php } 
                        } ?> 
                       ], 
                        "valueAxes": [{ 
                            "stackType": "regular", 
                            "axisAlpha": 0.3, 
                            "gridAlpha": 0.2 
                        }], 
                        "graphs": [{ 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 0.8, 
                            "labelText": "[[value]]", 
                            "lineAlpha": 0.3, 
                            "title": "IFK", 
                            "type": "column", 
                            "color": "#000000", 
                            "valueField": "ifk" 
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>", 
                            "fillAlphas": 0.8, 
                            "labelText": "[[value]]", 
                            "lineAlpha": 0.3, 
                            "title": "Miscellaneous", 
                            "type": "column", 
                            "color": "#000000", 
                            "valueField": "misc" 
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>", 
                            "lineThickness": 2, 
                            "lineColor": "#62cf73",
                            "title": "Meta", 
                            "type": "line",  
                            "valueField": "meta" 
                        } ], 
                        "categoryField": "week", 
                        "categoryAxis": { 
                            "gridAlpha": 0.2, 
                            "gridPosition": "start", 
                            "axisAlpha": 0, 
                            "gridAlpha": 0.2, 
                            "position": "left" 
                        }, 
                        "export": { 
                            "enabled": true 
                        } 
                    });

                </script>
            </div> 

            <!-- TABLA DE MESES --> 
            <table style="width: 59%; margin-left: 20px; border: 1px solid #BABABA; font-size: 14px;" >
                <tr >
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >Mes</th>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" ><?php echo $lblMonth[$i] ?></th>
                    <?php } ?> 
                </tr> 
                <tr> 
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >IFK</th>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td align="center" style="width: 7.5%; border: 1px solid #BABABA;" ><?php echo $valMonthIFK[$i] ?></td>      
                    <?php } ?>
                </tr> 
                <tr> 
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >MISC</th>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td align="center" style="width: 7.5%; border: 1px solid #BABABA;" ><?php echo $valMonthMisc[$i] ?></td>      
                    <?php } ?>
                </tr> 
            </table>

            <!-- TABLA DE SEMANA --> 
            <table style="width: 35%; margin-left: 63%; margin-top: -65px; border: 1px solid #BABABA; font-size: 14px;" >
                <tr>
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" > Semana </th>
                    <?php 
                    if ($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++ ) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" ><?php echo $lblWeek[$i] ?></th>
                        <?php } 
                         for($i = 1; $i <= $sL; $i++) {
                        ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" ><?php echo $lblWeek[$i] ?></th>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" ><?php echo $lblWeek[$i] ?></th>
                    <?php }} ?>                            
                </tr> 
                <tr> 
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >IFK</th>
                    <?php 
                    if ($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++ ) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $valWeekIFK[$i] ?></td>
                        <?php } 
                        for($i = 1; $i <= $sL; $i++) { 
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $valWeekIFK[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $valWeekIFK[$i] ?></td>
                    <?php }} ?>  
                </tr> 
                <tr> 
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >MISC</th>
                    <?php 
                    if ($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++ ) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $valWeekMisc[$i] ?></td>
                        <?php } 
                        for($i = 1; $i <= $sL; $i++) { 
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $valWeekMisc[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $valWeekMisc[$i] ?></td>
                    <?php }} ?>  
                </tr>    
            </table> 
            <br>        

            <div id="grafDays" name="grafDays" class="jidokaDay" > 
                <script>
                    var chart = AmCharts.makeChart("grafDays", {
                        "type": "serial", 
                        "theme": "none", 
                        "legend": { 
                            "autoMargins": false,
                            "borderAlpha": 0.2,
                            "equalWidths": false,
                            "horizontalGap": 50,
                            "verticalGap": 15,
                            "markerSize": 10,
                            "useGraphSettings": true,
                            "valueAlign": "left",
                            "valueWidth": 0
                        }, 
                        "dataProvider": [                     
                            <?php for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                $date = explode("-", $i); 
                                $vDate = $date[0].$date[1].(int)$date[2];  ?>
                            {
                                "day": "<?php echo $lblday[$vDate];?> ",
                                "ifk": <?php echo $valDayIFK[$vDate]; ?>,
                                "misc": <?php echo $valDayMisc[$vDate]; ?>, 
                                "meta": <?php echo $targetD[$vDate]; ?>
                            },
                            <?php } ?>             
                       ], 
                        "valueAxes": [{ 
                            "stackType": "regular", 
                            "axisAlpha": 0.3,
                        }], 
                        "graphs": [{ 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1, 
                            "labelText": "[[value]]", 
                            "lineAlpha": 1, 
                            "title": "IFK", 
                            "type": "column", 
                            "color": "#000000", 
                            "valueField": "ifk" 
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>", 
                            "fillAlphas": 1, 
                            "labelText": "[[value]]", 
                            "lineAlpha": 1, 
                            "title": "Miscellaneous", 
                            "type": "column", 
                            "color": "#000000", 
                            "valueField": "misc" 
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>", 
                            "lineThickness": 2, 
                            "lineColor": "#62cf73",
                            "title": "Meta", 
                            "type": "line",  
                            "valueField": "meta" 
                        } ], 
                        "chartScrollbar": {
                            "graph": "g1",
                            "oppositeAxis": false,
                            "offset": 30,
                            "scrollbarHeight": 20,
                            "backgroundAlpha": 0,
                            "selectedBackgroundAlpha": 0.1,
                            "selectedBackgroundColor": "#888888",
                            "graphFillAlpha": 0, 
                            "graphLineAlpha": 0.5, 
                            "selectedGraphFillAlpha": 0, 
                            "selectedGraphLineAlpha": 1, 
                            "autoGridCount": true, 
                            "color": "#AAAAAA" 
                        },
                        "categoryField": "day", 
                        "categoryAxis": { 
                            "gridAlpha": 0.2, 
                            "gridPosition": "start", 
                            "axisAlpha": 0, 
                            "position": "left" 
                        }, 
                        "export": { 
                            "enabled": true 
                        } 
                    }); 
                </script>
            </div>
        </div>
        
        <!-- APARTADO PARA PICKERS --> 
        <div class="row" style="margin-top: 1.4%" > 
            <div id="pnlPikers" style="margin-top: 34%" > 
                <script>
                    $(function() { 
                        $("#dateIni").datepicker({ 
                            //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                            maxDate: new Date(), 
                            //OYENTES DE LOS PICKERS 
                            onSelect: function(date) { 
                                //Cuando se seleccione una fecha se cierra el panel
                                $("#ui-datepicker-div").hide(); 
                            } 
                        }); 

                        $("#dateEnd").datepicker({ 
                            //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                            maxDate: new Date(), 
                            //OYENTES DE LOS PICKERS 
                            onSelect: function ( date ) { 
                                //cuando se seleccione una fecha se cierra el panel 
                                $("#ui-datepicker-div").hide(); 
                            } 
                        }); 
                    }); 
                </script>
                
                <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado container container-fluid" >
<!--                    <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2 contenidoCentrado" style="margin-left: 30px; float: left;" > 
                        <select id="tipoDatos" style="border-color: #888; " class="form-control " >
                            <option value="1" <?php //if ($tipo == 1 ){ ?> selected <?php //} ?> >Cost Center</option>     
                            <option value="2" <?php //if ($tipo == 2 ){ ?> selected <?php //} ?> >Profit Center</option>                            
                        </select> 
                    </div> -->
                    <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3 contenidoCentrado input-group" style="float: left;" > 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo date("m/d/Y", strtotime($fIni)); ?>" >
                        <span class="input-group-addon"> a </span> 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($fFin)); ?>" >
                    </div> 
                    <div class="col-xs-1 col-sh-1 col-md-1 col-lg-1 contenidoCentrado" style="float: left;" > 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0;" onclick="setTipoDatos()" > 
                            <img src="../../imagenes/confirmar.png" > 
                        </button> 
                    </div> 
                    <div class="col-xs-5 col-sh-5 col-md-5 col-lg-5 contenidoCentrado" style="float: left;" > 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0; margin-left: 90%; " onclick="setTipoDatos2()" > 
                            <img src="../../imagenes/bAll.png" > 
                        </button> 
                    </div> 
                </div> 
            </div> 
        </div> 
        <br>        
        
        <div class="row " style="margin-top: 15px" >
            <div id="holder-semple-1" style="margin-left: 1.2%; width: 98%" > 
                <script id="tamplate-semple-1" type="text/mustache"> 
                    <table style="width:97%; margin-left: 1.5%; font-size: 14px;" class="inner-table" > 
                        <thead> 
                            <tr> 
                                <td colspan="2"> </td> 
                                <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                                <td rowspan="2" align="center" >Total</td> 
                            </tr> 
                            <tr> 
                                <td>&nbsp;</td> 
                                <td align="center" >PROBLEMA</td>
                                <?php for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
                                    $date = explode("-", $i); 
                                    $vDate = $date[0].$date[1].(int)$date[2]; ?>
                                    <td align="center" ><?php echo $lblday[$vDate]; ?></td>
                                <?php } ?> 
                            </tr>
                        </thead>
                        <tbody>                            
                            <tr >
                                <td align="right" > </td>
                                <td >IFK</td>
                                <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                                    $date = explode("-", $j); 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                                ?>
                                <td align="center" ><?php echo $valDayIFK[$vDate]; ?></td>                            
                                <?php } ?>
                                <td align="center" ><?php echo $sTotalDIFK; ?></td> 
                            </tr >    
                            <tr >
                                <td align="right" > </td>
                                <td >MISC</td>
                                <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                                    $date = explode("-", $j); 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                                ?>
                                <td align="center" ><?php echo $valDayMisc[$vDate]; ?></td>                            
                                <?php } ?>
                                <td align="center" ><?php echo $sTotalDMisc; ?></td> 
                            </tr >    
                        </tbody>
                        <tfoot>
                            <tr>                            
                                <td colspan="2">Sold Total</td>
                                <?php for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                        $date = explode("-", $i); 
                                        $vDate = $date[0].$date[1].(int)$date[2];
                                ?>
                                    <td align="center" ><?php echo $valCummDay[$vDate]; ?></td>
                                <?php if ($i == $fFin) { ?>                            
                                    <!--CUADRE DE INFORMACION -->
                                    <td align="center" ><?php echo $tTabla; ?></td>
                                <?php }} ?> 
                            </tr> 
                        </tfoot> 
                    </table> 
                </script>
            </div> 
        </div> 
        <br><br>
    </div>
</body>
</HTML>
