<meta  >

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../db/ServerFunctions.php';
    
        $line = 'L001';
        $month = '12';
        $fIni = '2019-03-01';
        $fFin = '2019-03-31';
        $tipo = 1;        
        
        for($i = 0; $i < 3; $i++) {
            //DIA
            $areaOrgDia[$i] = "";
            $duracionAreaOrgDia[$i] = 0;
            
            $opMacTecDia[$i] = "";
            $duracionOpTecDia[$i] = 0;
            
            $opMacCaliDia[$i] = "";
            $duracionOpCaliDia[$i] = 0; 
            
            $porcentajeAreaOrgDia[$i] = 0; 
            $porcentajeOpTecDia[$i] = 0; 
            $porcentajeOpCaliDia[$i] = 0; 
            
            //MES
            $areaOrgMes[$i] = "";
            $opMacTecMes[$i] = "";
            $opMacCaliMes[$i] = "";
            
            $duracionAreaOrgMes[$i] = 0;
            $duracionOpTecMes[$i] = 0;
            $duracionOpCaliMes[$i] = 0;  
                        
            $porcentajeAreaOrgMes[$i] = 0;
            $porcentajeOpTecMes[$i] = 0;
            $porcentajeOpCaliMes[$i] = 0;
        }
        
        for($i = 0; $i < 9; $i++ ){
            $problemaOrgDia[$i] = "";
            $duracionProblemaOrgDia[$i] = 0;
            $problemaOpTecDia[$i] = " ";
            $duracionProblemaOpTecDia[$i] = 0;
            $problemaOpCaliDia[$i] = " ";
            $duracionProblemaOpCaliDia[$i] = 0;
            
            $porcentajeProblemaOrgDia[$i] = 0;
            $porcentajeProblemaOpTecDia[$i] = 0;
            $porcentajeProblemaOpCaliDia[$i] = 0;
            //MES
            $problemaOrgMes[$i] = "";
            $duracionProblemaOrgMes[$i] = 0;
            $problemaOpTecMes[$i] = " ";
            $duracionProblemaOpTecMes[$i] = 0;
            $problemaOpCaliMes[$i] = " ";
            $duracionProblemaOpCaliMes[$i] = 0;
            
            $porcentajeProblemaOrgMes[$i] = 0;
            $porcentajeProblemaOpTecMes[$i] = 0;
            $porcentajeProblemaOpCaliMes[$i] = 0;
        } 
        
        //INCIAMOS LOS COLORES
        //TECNICAS        
        $colorTec[0] = "#1F5AA6";
        $colorTec[1] = "#3B82BF";
        $colorTec[2] = "#5A90BF";
        $colorTec[3] = "#ACD1F2";
        $colorTec[4] = "#BDE3F2";
        //ORGANIZACIONALES
        $colorOrg[0] = "#DA7A7F";
        $colorOrg[1] = "#EEA7A9";
        $colorOrg[2] = "#F9C6C1";
        $colorOrg[3] = "#F6D8CC";
        $colorOrg[4] = "#FCECE0";        
        //CALIDAD
        $colorCali[0] = "#640000";
        $colorCali[1] = "#7D1910";
        $colorCali[2] = "#913720";
        $colorCali[3] = "#A55030";
        $colorCali[4] = "#B96440";
        
        //TIEMPOS TOTALES REGISTRADOS         
        //$datTimeTotal = tiempoTotalMes($line, $fIni, $fFin);        
        //$datLossesTimeTotal = tiempoTotalPeriodo($line, $fIni, $fFin); 
        
        //PORCENTAJE DE PERDIDA
        //$percentLossesGeneral = @round(($datLossesTimeTotal[0][0] * 100 ) / $datTimeTotal[0][0], 2);
        
        $datMinTec = lossesTimeTechnicals($line, $fIni, $fFin); 
        $datMinOrg = lossesTimeOrganizational($line, $fIni, $fFin); 
        $datMinCali = lossesTimeQuality($line, $fIni, $fFin); 
        
//        $percentLTec = @round(($datMinTec[0][0] * 100 ) / $datLossesTimeTotal[0][0], 2);
//        $percentLOrg = @round(($datMinOrg[0][0] * 100 ) / $datLossesTimeTotal[0][0], 2);
//        $percentLCali = @round(($datMinCali[0][0] * 100 ) / $datLossesTimeTotal[0][0], 2);
              
        switch ($tipo) { 
            case 1:
            case 3: 
                //OBTENCION DE TOPS POR TEMA
                $datTop3DiaTecOp = t3TecDiaOpLosses($line, $fIni, $fFin);
                for ($i = 0; $i < count($datTop3DiaTecOp); $i++ ) {
                    $opTec[$i] = $datTop3DiaTecOp[$i][0];
                    $probTec[$i] = $datTop3DiaTecOp[$i][2];
                    $durTec[$i] = $datTop3DiaTecOp[$i][3];
                    $frecTec[$i] = $datTop3DiaTecOp[$i][4];                    
                    $percentTec[$i] = @round(($durTec[$i]*100)/$datMinTec[0][0],2); 
                    //echo '<br>*', $opTec[$i], ', ',$probTec[$i],', ',$durTec[$i],', ', $percentTec[$i];
                    $datTop3DiaTecProblemaOp = t3ProblemTecDiaOpLosses($line, $fIni, $fFin, $opTec[$i]);
                    for ($j = 0; $j < count($datTop3DiaTecProblemaOp); $j++){
                        $subProbTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][0];
                        $subDurTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][1];
                        $subFrecTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][2];
                        $subPercentTec[$i][$j] = @round(($subDurTec[$i][$j]*$percentTec[$i])/$durTec[$i],2); 
                        $sCTec[$i] = $j+1;
                        //echo '<br>-', $subProbTec[$i][$j], ', ',$subDurTec[$i][$j],', ',$subPercentTec[$i][$j];
                    }
                } 
                
                $datTop3DiaOrgArea = t3OrgDiaAreaLosses($line, $fIni, $fFin);
                for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) { 
                    $areaOrg[$i] = $datTop3DiaOrgArea[$i][0]; 
                    $durOrg[$i] = $datTop3DiaOrgArea[$i][1]; 
                    $frecOrg[$i] = $datTop3DiaOrgArea[$i][2]; 
                    $percentOrg[$i] = @round(($durOrg[$i]*100)/$datMinOrg[0][0],2); 
                    //echo '<br>*', $areaOrg[$i],', ',$durOrg[$i],', ',$frecOrg[$i],', ',$percentOrg[$i]; 
                    $datTop3DiaOrgProblemaOp = t3ProblemOrgDiaAreaLosses($line, $fIni, $fFin, $areaOrg[$i]); 
                    for ($j = 0; $j < count($datTop3DiaOrgProblemaOp); $j++) { 
                        $sProbOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][0]; 
                        $sMatOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][1]; 
                        if ($sMatOrg[$i][$j] != "") { 
                            $subProbOrg[$i][$j] = $sProbOrg[$i][$j].' ['.$sMatOrg[$i][$j].']'; 
                        } else { 
                            $subProbOrg[$i][$j] = $sProbOrg[$i][$j]; 
                        }  
                        
                        $subDurOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][2]; 
                        $subFrecOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][3]; 
                        $subPercentOrg[$i][$j] = @round(($subDurOrg[$i][$j]*$percentOrg[$i])/$durOrg[$i],2); 
                        $sCOrg[$i] = $j+1; 
                        //echo '<br>-', $subProbOrg[$i][$j],', ',$subDurOrg[$i][$j],', ',$subPercentOrg[$i][$j];//,', ',$percentOrg[$i]; 
                    }
                }
                
                break;
            case 2: 
                $datTop3DiaTecOp = t3TecDiaOpLossesFrec($line, $pDiaI, $pDiaF);
                $datTop3DiaOrgArea = t3OrgDiaAreaLossesFrec($line, $pDiaI, $pDiaF);
                $datTop3DiaCaliOp = t3CaliDiaOpLossesFrec($line, $pDiaI, $pDiaF);

                //HASTA AQUI MODIFIQUE
                $datTop3MesTecOp = t3TecMesopLossesFrec($line, $year, $month);
                $datTop3MesOrgArea = t3OrgMesAreaLossesFrec($line, $year, $month); 
                $datTop3MesCaliOp = t3CaliMesOpLossesFrec($line, $year, $month); 
                
                break;
        }
?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<style>
    #lTec {
        position: relative;
        width : 100%;
        height : 60%;
    }
    
    #lOrg {
        position: relative; 
        width : 100%; 
        height : 60%; 
    } 
    
    #lCali {
        position: relative;
        width : 100%;
        height : 60%;
    }
</style>

<div class="container">     
    <div class="row" class="col-lg-12" >
        <div id="lTec" class="col-md-4 ">
            <script>
            // Create chart instance
            var chart = am4core.create("lTec", am4charts.PieChart);
            // Set data
            var selected;
            //SUB-NIVELES
            var types = [
                <?php for ($i = 0; $i < count($datTop3DiaTecOp); $i++){ ?>
                {
                    type: "<?php echo $opTec[$i],', ', $probTec[$i]; ?>",
                    percent: <?php echo $percentTec[$i]; ?> ,
                    color: am4core.color("<?php echo $colorTec[$i]; ?>"),
                    subs: [
                        <?php for ($j = 0; $j < $sCTec[$i]; $j++){ ?>
                        {
                            type: "<?php echo $subProbTec[$i][$j]?>",
                            percent: <?php echo $subPercentTec[$i][$j] ?>
                        },
                        <?php } ?> 
                    ]   
                }, 
                <?php } ?>];

            // Add data
            chart.data = generateChartData();

            // Add and configure Series
            var pieSeries = chart.series.push(new am4charts.PieSeries());
            pieSeries.dataFields.value = "percent";
            pieSeries.dataFields.category = "type";
            pieSeries.slices.template.propertyFields.fill = "color";
            pieSeries.slices.template.propertyFields.isActive = "pulled";
            pieSeries.slices.template.strokeWidth = 0;

            function generateChartData() {
                var chartData = [];
                for (var i = 0; i < types.length; i++) { 
                    if (i == selected) {
                        for (var x = 0; x < types[i].subs.length; x++) { 
                            chartData.push({ 
                                type: types[i].subs[x].type, 
                                percent: types[i].subs[x].percent, 
                                color: types[i].color, 
                                pulled: true 
                            }); 
                        } 
                    } else { 
                        chartData.push({ 
                            type: types[i].type, 
                            percent: types[i].percent, 
                            color: types[i].color, 
                            id: i 
                        }); 
                    } 
                } 
                return chartData; 
            } 

            pieSeries.slices.template.events.on("hit", function(event) {
                if (event.target.dataItem.dataContext.id != undefined) {
                    selected = event.target.dataItem.dataContext.id;
                } else {
                    selected = undefined;
                }
                chart.data = generateChartData();
            });
            </script>
        </div>        
    </div> 
</div>

