<?php 
    include '../../db/ServerFunctions.php';
    
        $line = 'L612';
        $month = '12';
        $fIni = '2019-03-01';
        $fFin = '2019-03-31';
        $tipo = 1; 
        
        for ($i = 0; $i < 6; $i++) { 
            $opTec[$i] = 0;
            for ($j = 0; $j < 15; $j++ ) { 
                $subProbTec[$i][$j] = "-"; 
                $subProbOrg[$i][$j] = ""; 
            } 
        } 
        
        //INCIAMOS LOS COLORES
        //TECNICAS        
        $colorTec[0] = "#1B4F72"; 
        $colorTec[1] = "#21618C";        
        $colorTec[2] = "#2874A6";        
        $colorTec[3] = "#2E86C1";                
        $colorTec[4] = "#3498DB";
        $colorTec[5] = "#5DADE2";
        $subColorTec[0] = "#34495E";
        $subColorTec[1] = "#1B4F72";
        $subColorTec[2] = "#21618C";
        $subColorTec[3] = "#2874A6"; 
        $subColorTec[4] = "#2E86C1";
        $subColorTec[5] = "#3498DB";
        $subColorTec[6] = "#5DADE2"; 
        $subColorTec[7] = "#85C1E9";
        $subColorTec[8] = "#AED6F1";         
        $subColorTec[9] = "#D6EAF8"; 
        $subColorTec[10] = "#EBF5FB"; 
        
        //ORGANIZACIONALES        
        $colorOrg[0] = "#F24130"; 
        $colorOrg[1] = "#FF5733";        
        $colorOrg[2] = "#FF6C4D";        
        $colorOrg[3] = "#FF8166";                
        $colorOrg[4] = "#FF9680";
        $colorOrg[5] = "#FFAB99";
        
        $subColorOrg[0] = "#F24130"; 
        $subColorOrg[1] = "#F25830";
        $subColorOrg[2] = "#FF5733";        
        $subColorOrg[3] = "#FF6C4D";        
        $subColorOrg[4] = "#FF8166";
        $subColorOrg[5] = "#FF9680"; 
        $subColorOrg[6] = "#FFAB99";
        $subColorOrg[7] = "#FFC0C5";
        $subColorOrg[8] = "#FFD5CC";
        $subColorOrg[9] = "#FFEAE5";
        $subColorOrg[10] = "#EBF5FB"; 
        
        //CALIDAD
        $colorCali[0] = "#640000";
        $colorCali[1] = "#7D1910";
        $colorCali[2] = "#913720";
        $colorCali[3] = "#A55030";
        $colorCali[4] = "#B96440";
        $subColorCali[0] = "#C7481D";
        $subColorCali[1] = "#FF5829";
        $subColorCali[2] = "#A0542F";
        $subColorCali[3] = "#CA351C";
        $subColorCali[4] = "#D8702B"; 
        
        //CONTADORES
        $acumPercentTec = 0;
        $acumPercentOrg = 0;
        
        //TIEMPOS TOTALES REGISTRADOS         
        //$datTimeTotal = tiempoTotalMes($line, $fIni, $fFin);        
        //$datLossesTimeTotal = tiempoTotalPeriodo($line, $fIni, $fFin);         
        //PORCENTAJE DE PERDIDA
        //$percentLossesGeneral = @round(($datLossesTimeTotal[0][0] * 100 ) / $datTimeTotal[0][0], 2);
            
        $datMinTec = lossesTimeTechnicals($line, $fIni, $fFin); 
        $datMinOrg = lossesTimeOrganizational($line, $fIni, $fFin); 
        $datMinCali = lossesTimeQuality($line, $fIni, $fFin); 
        
//        $percentLTec = @round(($datMinTec[0][0] * 100 ) / $datLossesTimeTotal[0][0], 2);
//        $percentLOrg = @round(($datMinOrg[0][0] * 100 ) / $datLossesTimeTotal[0][0], 2);
//        $percentLCali = @round(($datMinCali[0][0] * 100 ) / $datLossesTimeTotal[0][0], 2);
              
        //echo "linea: ",$line,'<br>';
        
        switch ($tipo) { 
            case 1:
            case 3: 
                //OBTENCION DE TOPS POR TEMA
                //TECNICAS
                $acumPercentTec = 0;
                $datTop3DiaTecOp = t3TecDiaOpLosses($line, $fIni, $fFin);
                for ($i = 0; $i < count($datTop3DiaTecOp); $i++ ) {
                    $opTec[$i] = $datTop3DiaTecOp[$i][0];
                    $probTec[$i] = $datTop3DiaTecOp[$i][2];
                    $durTec[$i] = $datTop3DiaTecOp[$i][3];
                    $frecTec[$i] = $datTop3DiaTecOp[$i][4]; 
                    $percentTec[$i] = @round(($durTec[$i]*100)/$datMinTec[0][0],2); 
                    $acumPercentTec += $percentTec[$i]; 
                    $sCTec[$i] = 0; 
                    $acumSubPercentTec[$i] = 0; 
                    //echo '<br> acum ',$i,': ',$acumPercentTec; 
                    //echo '<br>* ', $opTec[$i], ', ',$probTec[$i],', ',$durTec[$i],', ', $percentTec[$i]; 
                    $datTop3DiaTecProblemaOp = t3ProblemTecDiaOpLosses($line, $fIni, $fFin, $opTec[$i]); 
                    $sCTec[$i] = count($datTop3DiaTecProblemaOp); 
                    for ($j = 0; $j < count($datTop3DiaTecProblemaOp); $j++) { 
                        $subProbTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][0]; 
                        $subDurTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][1]; 
                        $subFrecTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][2]; 
                        $subPercentTec[$i][$j] = @round(($subDurTec[$i][$j]*$percentTec[$i])/$durTec[$i],2); 
                        $acumSubPercentTec[$i] += $subPercentTec[$i][$j]; 
                        $sNPTec[$i][$j] = 0;
                        //echo '<br>  -', $subProbTec[$i][$j], ', ',$subDurTec[$i][$j],', ',$subPercentTec[$i][$j]; 
                        
                        //TOP DE NUMERO DE PARTE
                        $datTopNoParteTec = t3NoParteTecDiaOpLosses($line, $fIni, $fFin, $opTec[$i], $subProbTec[$i][$j]);
                        $sNPTec[$i][$j] = count($datTopNoParteTec);
                        for ($k = 0; $k < count($datTopNoParteTec); $k++){
                            $noPartTec[$i][$j][$k] = $datTopNoParteTec[$k][0];
                            $durNoPartTec[$i][$j][$k] = $datTopNoParteTec[$k][1];
                            $frecNoPartTec[$i][$j][$k] = $datTopNoParteTec[$k][2];                            
                            $percentNoPartTec[$i][$j][$k] = @round(($durNoPartTec[$i][$j][$k]*$subPercentTec[$i][$j])/$subDurTec[$i][$j],2);
                            //$sNPTec[$i][$j] = $k+1;
                            //echo '<br>    .', $noPartTec[$i][$j][$k], ', ',$durNoPartTec[$i][$j][$k],', ',$percentNoPartTec[$i][$j][$k];
                        } 
                    }
                    
                    //COMPARACION PARA SACAR CANTIDADES DE OTROS
                    if ($acumSubPercentTec[$i] < $percentTec[$i] && $acumSubPercentTec[$i] > 0 ) {                         
                        $contPOTec = $sCTec[$i]; 
                        //echo '<br>c ',$i,' -> ', $contPOTec; 
                        $subProbTec[$i][$contPOTec] = "Otros"; 
                        $subPercentTec[$i][$contPOTec] = $percentTec[$i] - $acumSubPercentTec[$i]; 
                        $sNPTec[$i][$contPOTec] = 0; 
                        $sCTec[$i] = $contPOTec+1; 
                        //echo '<br>',$opTec[$i], ', ',$probTec[$i],', ',$acumSubPercentTec[$i],', ',$percentTec[$i],' -> ', $subPercentTec[$i][$contPOTec];
                    } 
                    
                } 
                
                //echo "acumuladoTec: ",$acumPercentTec;                
                if ($acumPercentTec != 100){
                    $contTec = count($datTop3DiaTecOp);
                    $opTec[$contTec] = "OTROS";
                    $probTec[$contTec] = "";
                    $percentTec[$contTec] = 100 - $acumPercentTec;
                    $sCTec[$contTec] = 0;                    
                    $acumPercentSubOtrosTec = 0; 
                    
                    //HACEMOS EL TOP DE LOS 10 PROBLEMAS DISTINTOS DE LAS OPERACIONES QUE YA TENEMOS
                    //OBENEMOS EL TOTAL DE PROBLEMAS RESTANTES
                    $datMinOtrosTec = tiempoTecOtrosLosses($line, $fIni, $fFin, $opTec[0], $opTec[1], $opTec[2], $opTec[3], $opTec[4]); 
                    $minOtrosTec = $datMinOtrosTec[0][0]; 
                    
                    $datTopOtrosTec = topProblemOtrosTecDiaOpLosses($line, $fIni, $fFin, $opTec[0], $opTec[1], $opTec[2], $opTec[3], $opTec[4]); 
                    for ($i = 0; $i < count($datTopOtrosTec); $i++) { 
                        $subProbTec[$contTec][$i] = $datTopOtrosTec[$i][1]; 
                        $subDurTec[$contTec][$i] = $datTopOtrosTec[$i][2]; 
                        $subFrecTec[$contTec][$i] = $datTopOtrosTec[$i][3]; 
                        $subPercentTec[$contTec][$i] = @round(($subDurTec[$contTec][$i]*$percentTec[$contTec])/$minOtrosTec,2); 
                        $acumPercentSubOtrosTec += $subPercentTec[$contTec][$i]; 
                        $sCTec[$contTec] = $i+1; 
                        $sNPTec[$contTec][$i] = 0;
                        //echo "<br> ",$subProbTec[$contTec][$i],', ',$subDurTec[$contTec][$i],', ',$subPercentTec[$contTec][$i] ;
                    }
                    
                    if ($acumPercentSubOtrosTec < $percentTec[$contTec] && $acumPercentSubOtrosTec > 0 ) {                         
                        $contPOTec = count($datTopOtrosTec);    
                        $subProbTec[$contTec][$contPOTec] = "Otros"; 
                        $subPercentTec[$contTec][$contPOTec] = $percentTec[$contTec] - $acumPercentSubOtrosTec;
                        $sNPTec[$contTec][$contPOTec] = 0;
                        $sCTec[$contTec] = $contPOTec+1; 
                    }                    
                    $contTec++;//= count($datTop3DiaTecOp)+1;
                } 
                
                //ORGANIZACIONALES
                $datTop3DiaOrgArea = t3OrgDiaAreaLosses($line, $fIni, $fFin);
                for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) { 
                    $areaOrg[$i] = $datTop3DiaOrgArea[$i][0]; 
                    $durOrg[$i] = $datTop3DiaOrgArea[$i][1]; 
                    $frecOrg[$i] = $datTop3DiaOrgArea[$i][2]; 
                    $percentOrg[$i] = @round(($durOrg[$i]*100)/$datMinOrg[0][0],2); 
                    $sCOrg[$i] = 0; 
                    $acumSubPercentOrg[$i] = 0; 
                    $acumPercentOrg += $percentOrg[$i]; 
                    //echo '<br>*', $areaOrg[$i],', ',$durOrg[$i],', ',$frecOrg[$i],', ',$percentOrg[$i]; 
                    $datTop3DiaOrgProblemaOp = t3ProblemOrgDiaAreaLosses($line, $fIni, $fFin, $areaOrg[$i]); 
                    $sCOrg[$i] = count($datTop3DiaOrgProblemaOp);
                    //echo "<br>entra ",$i,': ',count($datTop3DiaOrgProblemaOp); 
                    for ($j = 0; $j < count($datTop3DiaOrgProblemaOp); $j++) {                        
                        $sProbOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][0]; 
                        $sMatOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][1]; 
                        if ($sMatOrg[$i][$j] != "") { 
                            $subProbOrg[$i][$j] = $sProbOrg[$i][$j].' ['.$sMatOrg[$i][$j].']'; 
                        } else { 
                            $subProbOrg[$i][$j] = $sProbOrg[$i][$j]; 
                        }                         
                        $subDurOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][2]; 
                        $subFrecOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][3]; 
                        $subPercentOrg[$i][$j] = @round(($subDurOrg[$i][$j]*$percentOrg[$i])/$durOrg[$i],2); 
                        $acumSubPercentOrg[$i] += $subPercentOrg[$i][$j];
                        //$sCOrg[$i] = $j+1; 
                        $sNPOrg[$i][$j] = 0; 
                        //echo '<br>  -', $subProbOrg[$i][$j], ', ',$subDurOrg[$i][$j],', ',$subPercentOrg[$i][$j]; 
                        //TOP DE NUMERO DE PARTE
                        $datTopNoParteOrg = t3NoParteOrgDiaAreaLosses($line, $fIni, $fFin, $areaOrg[$i], $subProbOrg[$i][$j]); 
                        $sNPOrg[$i][$j] = count($datTopNoParteOrg);
                        for ($k = 0; $k < count($datTopNoParteOrg); $k++ ) { 
                            $noPartOrg[$i][$j][$k] = $datTopNoParteOrg[$k][0]; 
                            $durNoPartOrg[$i][$j][$k] = $datTopNoParteOrg[$k][1]; 
                            $frecNoPartOrg[$i][$j][$k] = $datTopNoParteOrg[$k][2]; 
                            $percentNoPartOrg[$i][$j][$k] = @round(($durNoPartOrg[$i][$j][$k]*$subPercentOrg[$i][$j])/$subDurOrg[$i][$j],2); 
                            //echo '<br>    .', $noPartOrg[$i][$j][$k], ', ',$durNoPartOrg[$i][$j][$k],', ',$percentNoPartOrg[$i][$j][$k];
                        }                        
                    }
                    //echo '<br> nI:',count($datTop3DiaOrgProblemaOp);
                    if ($acumSubPercentOrg[$i] != $percentOrg[$i] && $acumSubPercentOrg[$i] > 0 ) {
                        $contOrg = count($datTop3DiaOrgProblemaOp);
                        $subProbOrg[$i][$contOrg] = "OTROS";
                        $subPercentOrg[$i][$contOrg] = $percentOrg[$i]-$acumSubPercentOrg[$i];
                        //echo "<br>",$areaOrg[$i],': ',$percentOrg[$i],', ',$acumSubPercentOrg[$i], ' -> ', $subPercentOrg[$i][$contOrg],', ',$sCOrg[$i];
                        $sNPOrg[$i][$contOrg] = 0; 
                        $sCOrg[$i] = count($datTop3DiaOrgProblemaOp)+1;                        
                    }                    
                }
                
                //CALIDAD
                $datTop3DiaCaliArea = t3CaliDiaOpLosses($line, $fIni, $fFin);
                for ($i = 0; $i < count($datTop3DiaCaliArea); $i++ ) { 
                    $opC[$i] = $datTop3DiaCaliArea[$i][0]; 
                    $opCali[$i] = $datTop3DiaCaliArea[$i][0]; 
                    $maCali[$i] = $datTop3DiaCaliArea[$i][1]; 
                    $descCali[$i] = $datTop3DiaCaliArea[$i][2];                    
                    if (isset($maCali[$i])){
                        $opCali[$i] = $opCali[$i].' ['.$maCali[$i].']';
                    } else if (isset($descCali[$i])){
                        $opCali[$i] = $opCali[$i].' '.$descCali[$i];
                    }                         
                    
                    $pzasCali[$i] = $datTop3DiaCaliArea[$i][3]; 
                    $frecCali[$i] = $datTop3DiaCaliArea[$i][4]; 
                    $sCCali[$i] = 0; 
                    //$percentCali[$i] = @round(($durCali[$i]*100)/$datMinCali[0][0],2); 
                    //echo '<br>* ', $opCali[$i],', ',$descCali[$i],', ',$pzasCali[$i];//,', ',$percentCali[$i]; 
                    $datTop3DiaCaliProblemaOp = t3ProblemCaliDiaOpLosses($line, $fIni, $fFin, $opC[$i]); 
                    //echo '<br> , ',count($datTop3DiaCaliProblemaOp); 
                    for ($j = 0; $j < count($datTop3DiaCaliProblemaOp); $j++) { 
                        $subProbCali[$i][$j] = $datTop3DiaCaliProblemaOp[$j][0]; 
                        $subPzasCali[$i][$j] = $datTop3DiaCaliProblemaOp[$j][1]; 
                        $subFrecCali[$i][$j] = $datTop3DiaCaliProblemaOp[$j][2]; 
                        $subPercentCali[$i][$j] = @round(($subPzasCali[$i][$j] * $percentCali[$i])/$durCali[$i],2); 
                        $sCCali[$i] = $j+1; 
                        $sNPCali[$i][$j] = 0;
                        //echo '<br>- ', $subProbCali[$i][$j],', ',$subPzasCali[$i][$j],', ',$subPercentCali[$i][$j];//,', ',$percentOrg[$i]; 
                        //OBTENCION DE NUMERO DE PARTE
                        $datTopNoParteCali = t3NoParteCaliDiaOpLosses($line, $fIni, $fFin, $opC[$i], $subProbCali[$i][$j]);
                        for ($k = 0; $k < count($datTopNoParteCali); $k++) {
                            $noPartCali[$i][$j][$k] = $datTopNoParteCali[$k][0]; 
                            $pzasNoPartCali[$i][$j][$k] = $datTopNoParteCali[$k][1]; 
                            $frecNoPartCali[$i][$j][$k] = $datTopNoParteCali[$k][2]; 
                            //echo '<br>- ', $noPartCali[$i][$j][$k] ;
                            $sNPCali[$i][$j] = $k+1; 
                        } 
                    } 
                } 
                
                break;
            case 2: 
                $datTop3DiaTecOp = t3TecDiaOpLossesFrec($line, $pDiaI, $pDiaF);
                $datTop3DiaOrgArea = t3OrgDiaAreaLossesFrec($line, $pDiaI, $pDiaF);
                $datTop3DiaCaliOp = t3CaliDiaOpLossesFrec($line, $pDiaI, $pDiaF);

                //HASTA AQUI MODIFIQUE
                $datTop3MesTecOp = t3TecMesopLossesFrec($line, $year, $month);
                $datTop3MesOrgArea = t3OrgMesAreaLossesFrec($line, $year, $month); 
                $datTop3MesCaliOp = t3CaliMesOpLossesFrec($line, $year, $month); 
                
                break;
        }
?>

<style>
    body {
        font-family: Verdana;
        font-size: 12px;
        padding: 10px;
    }

    #chartdiv {
        width: 500px;
        height: 500px;
        margin: 0 auto;
    }
    
    #lTec {
        width: 50%;
        height: 50%;
    }
    
    #lOrg {
        width: 50%;
        height: 50%;
    }
    
    #lCali {
        width: 50%;
        height: 50%;
    }
    
</style>

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
<script src="https://www.amcharts.com/lib/3/pie.js"></script> 

<div class="container" >
    <div class="row"  > 
        <div id="lTec" style="float:left;" >
            <script>
                var lTec = [
                    <?php  for ($i = 0; $i < $contTec; $i++ ) { ?>
                    {
                        "title": "<?php echo $opTec[$i],', ', $probTec[$i]; ?>",
                        "value": <?php echo $percentTec[$i]; ?>,
                        "url":"#",
                        "description":"click to drill-down",
                        "color": "<?php echo $colorTec[$i]; ?>",
                        "data": [
                            <?php for ($j = 0; $j < $sCTec[$i]; $j++){ ?>
                            { 
                                "title": "<?php echo $subProbTec[$i][$j]?>", 
                                "value": <?php echo $subPercentTec[$i][$j] ?>,
                                "color": "<?php echo $subColorTec[$j]; ?>",
                                    "data": [
                                    <?php for ($k = 0; $k < $sNPTec[$i][$j]; $k++) { ?>
                                        { 
                                            "title": "<?php echo $noPartTec[$i][$j][$k] ?>",
                                            "value": <?php echo $percentNoPartTec[$i][$j][$k] ?>,
                                            "color": "<?php echo $subColorTec[$k]; ?>"
                                        }, 
                                    <?php } ?> 
                                    ] 
                            },
                            <?php } ?> 
                        ]
                    },
                <?php } ?> 
                ];
                // create pie chart
                var chart = AmCharts.makeChart("lTec", {
                    "type": "pie",
                    "dataProvider": lTec,
                    "valueField": "value",
                    "titleField": "title",
                    "colorField": "color",
                    "labelText": "[[title]]: [[value]]",
                    "pullOutOnlyOne": true,
                    "titles": [{
                        "text": "Técnicas"
                    }],
                    "allLabels": []
                });
                // initialize step array
                chart.drillLevels = [{
                    "title": Técnicas,
                    "data": lTec
                }];
                // add slice click handler
                chart.addListener("clickSlice", function (event) {
                // get chart object
                var chart = event.chart;
                // check if drill-down data is avaliable
                if (event.dataItem.dataContext.data !== undefined) {
                    // save for back button
                    chart.drillLevels.push(event.dataItem.dataContext);

                    // replace data
                    chart.dataProvider = event.dataItem.dataContext.data;

                    // replace title
                    chart.titles[0].text = event.dataItem.dataContext.title;

                    // add back link
                    // let's add a label to Regresar to yearly data
                    event.chart.addLabel(
                        0, 25, 
                        "< Regresar",
                        undefined, 
                        undefined, 
                        undefined, 
                        undefined, 
                        undefined, 
                        undefined, 
                        'javascript:drillUp();');

                      // take in data and animate
                      chart.validateData();
                      chart.animateAgain();
                    }
                });
                function drillUp() {
                    // get level
                    chart.drillLevels.pop();
                    var level = chart.drillLevels[chart.drillLevels.length - 1];
                    // replace data
                    chart.dataProvider = level.data;
                    // replace title
                    chart.titles[0].text = level.title;
                    // remove labels
                    if (chart.drillLevels.length === 1)
                        chart.clearLabels();

                    // take in data and animate
                    chart.validateData();
                    chart.animateAgain();
                }
            </script>
        </div>
        
        <style>
            #div1 {
                overflow:scroll;
                height:50%;
                width:100%;
            }
            #div1 table {
                width:100%;        
            }
        </style>       
        
<!--        <div id="tTec" style="float:left; width: 50%" >
            <div id="div1" class="table-responsive" >
                <table border="1" >
                    <tr>
                        <?php //for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) {  ?>
                            <td>Area</td> 
                            <td>Duracion</td> 
                            <td>Frecuencia</td> 
                        <?php //} ?>
                    </tr> 
                    <tr>
                        <?php //for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) {  ?>
                            <td><?php //echo $areaOrg[$i] ?></td>
                            <td><?php //echo $durOrg[$i] ?></td>
                            <td><?php //echo $frecOrg[$i] ?></td> 
                        <?php //} ?>
                    </tr>                         
                </table>
            </div>
        </div> -->
    </div>
    <div class="row"  > 
        <div id="lOrg" >
            <script>
                var lOrg = [<?php  for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) { ?>
                    {
                        "title": "<?php echo $areaOrg[$i]; ?>",
                        "value": <?php echo $percentOrg[$i]; ?>,
                        "url":"#",
                        "description":"click to drill-down",
                        "color": "<?php echo $colorOrg[$i]; ?>",
                        "data": [
                            <?php for ($j = 0; $j < $sCOrg[$i]; $j++){ ?>
                            { 
                                "title": "<?php echo $subProbOrg[$i][$j] ?>", 
                                "value": <?php echo $subPercentOrg[$i][$j] ?>,
                                "color": "<?php echo $subColorOrg[$j]; ?>",
                                "data": [
                                    <?php for ($k = 0; $k < $sNPOrg[$i][$j]; $k++) { ?>
                                        { 
                                            "title": "<?php echo $noPartOrg[$i][$j][$k] ?>",
                                            "value": <?php echo $percentNoPartOrg[$i][$j][$k] ?>
                                        }, 
                                    <?php } ?> 
                                    ] 
                            },
                            <?php } ?> 
                        ]
                    },
                <?php } ?> 
                ];

                // create pie chart
                var chartLT = AmCharts.makeChart("lOrg", {
                    "type": "pie",
                    "dataProvider": lOrg,
                    "valueField": "value",
                    "titleField": "title",
                    "colorField": "color",
                    "labelText": "[[title]]: [[value]]",
                    "pullOutOnlyOne": true,
                    "titles": [{
                        "text": "Organizacionales"
                    }],
                    "allLabels": []
                });

                // initialize step array
                chartLT.drillLevels = [{
                    "title": "Organizacionales",
                    "data": lOrg
                }];

                // add slice click handler
                chartLT.addListener("clickSlice", function (event) {
                // get chart object
                    var chart = event.chart;
                    // check if drill-down data is avaliable
                    if (event.dataItem.dataContext.data !== undefined) {
                        // save for back button
                        chart.drillLevels.push(event.dataItem.dataContext);
                        // replace data
                        chart.dataProvider = event.dataItem.dataContext.data;
                        // replace title
                        chart.titles[0].text = event.dataItem.dataContext.title;
                        // add back link
                        // let's add a label to Regresar to yearly data
                        event.chart.addLabel(
                          0, 25, 
                          "< Regresar",
                          undefined, 
                          undefined, 
                          undefined, 
                          undefined, 
                          undefined, 
                          undefined, 
                          'javascript:drillUpOr();');
                        // take in data and animate
                        chart.validateData();
                        chart.animateAgain();
                    }
                });

                function drillUpOr() {
                    // get level
                    chartLT.drillLevels.pop();
                    var level = chartLT.drillLevels[chartLT.drillLevels.length - 1];
                    // replace data
                    chartLT.dataProvider = level.data;
                    // replace title
                    chartLT.titles[0].text = level.title;
                    // remove labels
                    if (chartLT.drillLevels.length === 1)
                      chartLT.clearLabels();
                    // take in data and animate
                    chartLT.validateData();
                    chartLT.animateAgain();
                }
            </script>
        </div>
        
        <div id="tOrg" style="float:left" > 
            
        </div>
    </div>
    <div class="row"  > 
        <div id="lCali" >
            <script>
                var lCali = [<?php  for ($i = 0; $i < count($datTop3DiaCaliArea); $i++ ) { ?>
                    {
                        "title": "<?php echo $opCali[$i]; ?>",
                        "value": <?php echo $pzasCali[$i]; ?>,
                        "url":"#",
                        "description":"click to drill-down",
                        "color": "<?php echo $colorCali[$i]; ?>",
                        "data": [
                            <?php for ($j = 0; $j < $sCCali[$i]; $j++){ ?>
                            { 
                                "title": "<?php echo $subProbCali[$i][$j] ?>", 
                                "value": <?php echo $subPzasCali[$i][$j] ?>,
                                "color": "<?php echo $colorCali[$j]; ?>",
                                "data": [
                                    <?php for ($k = 0; $k < $sNPCali[$i][$j]; $k++) { ?>
                                        { 
                                            "title": "<?php echo $noPartCali[$i][$j][$k] ?>",
                                            "value": <?php echo $pzasNoPartCali[$i][$j][$k] ?>
                                        }, 
                                    <?php } ?> 
                                    ] 
                            }, 
                            <?php } ?> 
                        ]
                    },
                <?php } ?> 
                ];

                // create pie chart
                var chartLC = AmCharts.makeChart("lCali", {
                    "type": "pie",
                    "dataProvider": lCali,
                    "valueField": "value",
                    "titleField": "title",
                    "colorField": "color",
                    "labelText": "[[title]]: [[value]]",
                    "pullOutOnlyOne": true,
                    "titles": [{
                        "text": "Calidad"
                    }],
                    "allLabels": []
                });

                // initialize step array
                chartLC.drillLevels = [{
                    "title": "Calidad",
                    "data": lCali
                }];

                // add slice click handler
                chartLC.addListener("clickSlice", function (event) {
                // get chart object
                    var chart = event.chart;
                    // check if drill-down data is avaliable
                    if (event.dataItem.dataContext.data !== undefined) { 
                        // save for back button
                        chart.drillLevels.push(event.dataItem.dataContext);
                        // replace data
                        chart.dataProvider = event.dataItem.dataContext.data;
                        // replace title
                        chart.titles[0].text = event.dataItem.dataContext.title;
                        // add back link
                        // let's add a label to Regresar to yearly data
                        event.chart.addLabel(
                          0, 25, 
                          "< Regresar",
                          undefined, 
                          undefined, 
                          undefined, 
                          undefined, 
                          undefined, 
                          undefined, 
                          'javascript:drillUpCali();');
                        // take in data and animate
                        chart.validateData();
                        chart.animateAgain();
                    }
                });

                function drillUpCali() {
                    // get level
                    chartLC.drillLevels.pop();
                    var level = chartLC.drillLevels[chartLC.drillLevels.length - 1];
                    // replace data
                    chartLC.dataProvider = level.data;
                    // replace title
                    chartLC.titles[0].text = level.title;
                    // remove labels
                    if (chartLC.drillLevels.length === 1)
                      chartLC.clearLabels();
                    // take in data and animate
                    chartLC.validateData();
                    chartLC.animateAgain();
                }
            </script>
        </div>
        
        <div id="tCali" style="float:left" >
            
        </div>
    </div>
</div>

