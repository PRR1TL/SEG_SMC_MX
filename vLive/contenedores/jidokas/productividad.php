<HTML>
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script>  
    <script>        
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 0, //CABECERA FIJAS 
                columnsInScrollableArea: 13, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
        
        function setTipoDatos() { 
            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON 
            //Obtenemos la informacion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes.php", 
                type: "post", 
                data: { tipoVista: 1 , tipoDato: 1, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 
        
//        function setTipoDatos2() { 
//            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON 
//            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla 
//            var ini = document.getElementById("dateIni").value; 
//            var fin = document.getElementById("dateEnd").value; 
//
//            $.ajax({ 
//                url: "../../db/sesionReportes_1.php", 
//                type: "post", 
//                data: { tipoVista: 2, tipoDato: 1, fIni: ini, fFin: fin }, 
//                success: function (result) { 
//                    //Actualizamos el apartado de graficas 
//                    location.reload(); 
//                } 
//            }); 
//        } 
        
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
    
    <!-- LIBRERIAS Y CONFIGURACION PARA PICKERS --> 
    <!-- LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR --> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" > 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <?php 
        include '../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
        $line = $_SESSION['linea']; 
        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes']; 

        $lblM[1] = (string) "Jun";
        $lblM[2] = (string) "Feb";
        $lblM[3] = (string) "Mar";
        $lblM[4] = (string) "Apr";
        $lblM[5] = (string) "May";
        $lblM[6] = (string) "Jun";
        $lblM[7] = (string) "Jul";
        $lblM[8] = (string) "Aug";
        $lblM[9] = (string) "Sep";
        $lblM[10] = (string) "Oct";
        $lblM[11] = (string) "Nov";
        $lblM[12] = (string) "Dec"; 
        
        /******************** NUMERO DE SEMANAS *****************************/
        # Obtenemos el ultimo dia del mes
        $ultimoDiaMes=date("t",mktime(0,0,0,$mes,1,$anio)); 

        //DIAS DE MESES (PARA PRODUCTIVIDAD ANUAL)
        for($i = 1; $i < 13; $i++){
            $lastDayMonth[$i] = date("t",mktime(0,0,0,$i,1,$anio));
        }

        //MES
        $firstDayYear = $anio.'-01-01';

        $date->setISODate("$anio", 53);
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) { 
                $pzasW[$i] = 0; 
                $minW[$i] = 0; 
                $hrsW[$i] = 0; 
                $mensW[$i] = 0; 
                $turnosW[$i] = 0; 
                $hrsHombreW[$i] = 0; 
                $productivityWeek[$i] = 0; 
                $targetWeek[$i] = 0; 
            } 
            for($i = 1; $i <= $sL; $i++) { 
                $pzasW[$i] = 0; 
                $minW[$i] = 0; 
                $hrsW[$i] = 0; 
                $mensW[$i] = 0; 
                $turnosW[$i] = 0; 
                $hrsHombreW[$i] = 0; 
                $productivityWeek[$i] = 0; 
                $targetWeek[$i] = 0; 
            } 
        } else { 
            for ($i = $sP; $i <= $sL; $i++) { 
                $cw[$i] = 0; 
                $pzasW[$i] = 0; 
                $minW[$i] = 0; 
                $hrsW[$i] = 0; 
                $mensW[$i] = 0; 
                $turnosW[$i] = 0; 
                $hrsHombreW[$i] = 0; 
                $productivityWeek[$i] = 0; 
                $targetWeek[$i] = 0; 
            }            
        } 
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

        $x = 0;
        
        /**********************************************************************/
        //ARRAYS
        $productivityPzasDay = productividadPzasDia($line, $_SESSION['FIni'], $_SESSION['FFin']); 
        $datPzasPSemana = productividadPzasSemana($line, $anio, $mes); 
        $productivityPzasMonth = productividadPzasMensual($line, $anio); 
        
        //TARGETS
        /********* DECLARACIONES DE VARIABLES, PARA QUE NO MARQUE ERRROR ******/

        //APARTAMOS ESPACIO PARA MESES
        for($i = 1; $i <= 12; $i++) { 
            $mt = $i;
            $mesProd[$i] = $i;
            $pzasMonth[$i] = 0;
            $minMonth[$i] = 0;
            $hoursMonth[$i] = 0;
            $mensMonth[$i] = 0;
            $hrsMensMonth[$i] = 0;
            $turnsMonth[$i] = 0;
            $productivityMonth[$i] = 0;
            $targetMonth[$i] = 0;
            $targetPromMonth[$i] = 0;
        }
        /******************************************************************/

        /****************** PRODUCTIVIDAD POR MES **************************/        
        for($i = 0; $i < count($productivityPzasMonth); $i++){ 
            $m = $productivityPzasMonth[$i][0]; 
            $mesProd[$m] = $productivityPzasMonth[$i][0]; 
            $pzasMonth[$m] = $productivityPzasMonth[$i][1]; 
            $minMonth[$m] = $productivityPzasMonth[$i][2]; 
            $hoursMonth[$m] = $minMonth[$m]/60; 
        } 
        
        # CONSULTA DE TIEMPO TURNO 
        $productivityTurnsMonth = productividadTurnoMensual($line, $anio); 
        for($i = 0; $i< count($productivityTurnsMonth); $i++){ 
            $m = $productivityTurnsMonth[$i][0]; 
            $mensMonth[$m] = $productivityTurnsMonth[$i][1]; 
            $turnsMonth[$m] = $productivityTurnsMonth[$i][2]; 
        } 
        
        # PRODUCTIVIDAD MENSUAL 
        for ($i = 1; $i < 13; $i++) { 
            if($mensMonth[$i] && $hoursMonth[$i] && $turnsMonth[$i] > 0){ 
                $hrsMensMonth[$i] = @(@$mensMonth[$i]*@$hoursMonth[$i])/@$turnsMonth[$i];
                
                $hrsMensMonth[$i]  ;
                $productivityMonth[$i] = @round($pzasMonth[$i]/@($hrsMensMonth[$i]),2,PHP_ROUND_HALF_DOWN); 
            } 
        } 
        
        #TARGET
        $cTargetMonth = cTargetProductividadMonthy($line, $anio); 
        for ($i = 0; $i < count($cTargetMonth); $i++){
            $m = $cTargetMonth[$i][0];
            $targetMonth[$m] = $cTargetMonth[$i][1];
        }
        
        /******************** SEMANAL *****************************************/                        
        # PIEZAS Y MINUTOS REPORTADOS 
        for ($i = 0; $i < count($datPzasPSemana); $i++){ 
            $s = $datPzasPSemana[$i][0]; 
            $pzasW[$s] = $datPzasPSemana[$i][1]; 
            $minW[$s] = $datPzasPSemana[$i][2]; 
            $hrsW[$s] = $datPzasPSemana[$i][2]/60; 
        } 
        
        # HOMBRES SEMANA 
        $datHombresSemana = productividadHombresSemana($line, $_SESSION['FIni'], $_SESSION['FFin']); 
        for ($i = 0; $i < count($datHombresSemana); $i++){ 
            $s = $datPzasPSemana[$i][0]; 
            $mensW[$s] = $datHombresSemana[$i][1]; 
            $turnosW[$s] = $datHombresSemana[$i][2]; 
        } 

        # PRODUCTIVIDAD SEMANAL 
        for ($i = 0; $i < count($datPzasPSemana); $i++) { 
            $s = $datPzasPSemana[$i][0]; 
            $hrsHombreW[$s] = (($mensW[$s]*$hrsW[$s])/$turnosW[$s]); 
            $productivityWeek[$s] = @round(($pzasW[$s]/ $hrsHombreW[$s]),2); 
        } 
        
        # TARGET
        $cTargetWeek = cTargetProductividadWeekly($line, $anio, $mes); 
        for ($i = 0; $i < count($cTargetWeek); $i++ ){ 
            $w = $cTargetWeek[$i][0]; 
            $targetWeek[$w] = $cTargetWeek[$i][1];
        }
        
        /**************************** DIARIA *********************************/
        # PIEZAS Y MINUTOS REPORTADOS 
        for($i = $firstDayYear; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $fecha[$vDate] = $i; 
            $dateT[$vDate] = date("M, d",strtotime($i)); 
            $pzasD[$vDate] = 0; 
            $minD[$vDate] = 0; 
            $hrsHombreD[$vDate] = 0; 
            $hrsD[$vDate] = 0; 
            $mensD[$vDate] = 0; 
            $turnosD[$vDate] = 0; 
            $productivityDay[$vDate] = 0; 
            $targetDay[$vDate] = 0; 
        }
        
        for($i = 0; $i < count($productivityPzasDay); $i++){ 
            $date = explode("-", $productivityPzasDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $pzasD[$vDate] = $productivityPzasDay[$i][1]; 
            $minD[$vDate] = $productivityPzasDay[$i][2]; 
            $hrsD[$vDate] = $minD[$vDate]/60; 
        } 
        
        # HOMBRES DIA 
        $datHombresDia = productividadHombresDia($line, $_SESSION['FIni'], $_SESSION['FFin']); 
        for ($i = 0; $i < count($datHombresDia); $i++ ){ 
            $date = explode("-", $datHombresDia[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2];  
            $mensD[$vDate] = $datHombresDia[$i][1]; 
            $turnosD[$vDate] = $datHombresDia[$i][2]; 
        }        
        
        for($i = $firstDayYear; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $hrsHombreD[$vDate] = @(($mensD[$vDate]*$hrsD[$vDate])/$turnosD[$vDate]); 
            $productivityDay[$vDate] = @round(($pzasD[$vDate]/ $hrsHombreD[$vDate]),2);             
        }
        
        #TARGET         
        $cTargetDay = cTargetProductividadDaily($line, $_SESSION['FIni'], $_SESSION['FFin']);
        for ($i = 0; $i < count($cTargetDay); $i++){
            $date = explode("-", $cTargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            
            $targetDay[$vDate] = $cTargetDay[$i][1];
        } 
        
        $dias = (strtotime($_SESSION['FIni'])- strtotime($_SESSION['FFin']))/86400;
        
    ?>
    <body>
        <br>
        <div class=" row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" >
            <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                <script>
                    var chart = AmCharts.makeChart("jTMonth", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": [
                            <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            {
                                "country": "<?php echo $lblM[$i] ?>",
                                "visits": <?php echo $productivityMonth[$i] ?>,
                                "meta": <?php echo $targetMonth[$i] ?>
                            },
                            <?php } ?>
                        ],
                        "graphs": [{
                            "fillAlphas": 0.9,
                            "lineAlpha": 0.2,
                            "lineColor": "#02538b",
                            "type": "column",
                            "valueField": "visits"
                        }, { 
                            "valueAxis": "v2",                             
                            "lineThickness": 2, 
                            "lineColor": "#62cf73", 
                            "type": "line", 
                            "title": "Meta", 
                            "valueField": "meta", 
                            "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                        }],
                        "categoryField": "country",
                        "valueAxes": [{
                            "title": "Indicador"
                        }]
                    });
                </script>
            </div>              
            
            <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                <script>
                    var chart = AmCharts.makeChart("jTWeek", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": [
                            <?php 
                                if($sP > $sL){
                                    for ($i = $sP; $i <= $numSemanas; $i++) { 
                                ?>
                                    { 
                                        "country": "<?php echo 'CW-'.$i ?>", 
                                        "visits": <?php echo $productivityWeek[$i] ?>, 
                                        "meta": <?php echo $targetWeek[$i] ?> 
                                    },
                                <?php }  for($i = 1; $i <= $sL; $i++) {  ?> 
                                    { 
                                        "country": "<?php echo 'CW-'.$i ?>", 
                                        "visits": <?php echo $productivityWeek[$i] ?>, 
                                        "meta": <?php echo $targetWeek[$i] ?> 
                                    },
                            <?php }} else { 
                                for ($i = $sP; $i <= $sL; $i++) {                                
                                ?>
                                    {
                                        "country": "<?php echo 'CW-'.$i ?>", 
                                        "visits": <?php echo $productivityWeek[$i] ?>, 
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    },
                                <?php } } ?> 
                        ],
                        "graphs": [{
                            "fillAlphas": 0.9,
                            "lineAlpha": 0.2,
                            "lineColor": "#02538b",
                            "type": "column",
                            "valueField": "visits"
                        }, { 
                            "valueAxis": "v2",                             
                            "lineThickness": 2, 
                            "lineColor": "#62cf73", 
                            "type": "line", 
                            "title": "Meta", 
                            "valueField": "meta", 
                            "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                        }],
                        "categoryField": "country",
                        "valueAxes": [{
                            "title": "Inidicador"
                        }]
                    });
                </script>                
            </div>    
            <!-- TABLA DE MESES -->            
            <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                <tr >
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">Mes</th>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblM[$i] ?></th>
                    <?php } ?>
                </tr>
                <tr>
                    <td align="center" style="border: 1px solid #BABABA;" > Indicador </td>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $productivityMonth[$i] ?></td>      
                    <?php } ?>
                </tr>  
            </table>
            
            <!-- TABLA DE SEMANA -->            
            <table style="width: 35%; margin-left: 63%; margin-top: -33px; border: 1px solid #BABABA; ">
                <tr>
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) { 
                        ?> 
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                    <?php }} ?>                            
                </tr>
                <tr>
                    <td align="center" style="border: 1px solid #BABABA;" > Indicador </td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $productivityWeek[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $productivityWeek[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $productivityWeek[$i] ?></td>
                    <?php }} ?>  
                </tr>                
            </table> 
            <br> 
            
            <div id="productivityDay" class="jidokaDay" >
                <script>
                    var chart = AmCharts.makeChart("productivityDay", {
                        "type": "serial",
                        "theme": "light",
                        "dataDateFormat": "YYYY-MM-DD",
                        "precision": 1,
                        "dataProvider": [
                        <?php  
                            for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                $date = explode("-", $i);                                 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                        ?>    
                        {
                            "date": "<?php echo "$fecha[$vDate]" ?>",                            
                            "production": <?php echo $productivityDay[$vDate]; ?>,
                            "meta": "<?php echo $targetDay[$vDate]; ?>"                            
                        },
                        <?php } ?>
                        ],
                        "valueAxes": [{
                            "id": "v1",
                            "title": "Indicador",
                            "position": "left",
                            "autoGridCount": false,
                            "labelFunction": function(value) {
                                return Math.round(value);
                            }
                        }],
                        "graphs": [{
                            "valueAxis": "v1",
                            "fillAlphas": 1,                            
                            "lineThickness": 2,
                            "lineColor": "#02538b",
                            "fillColors": "#02538b",
                            "type": "column",
                            "title": "Indicador",
                            "valueField": "production",
                            "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                        }, {
                            "valueAxis": "v2",                            
                            "lineThickness": 2,
                            "lineColor": "#62cf73",
                            "type": "smoothedLine",
                            "title": "Meta",
                            "valueField": "meta",
                            "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                        }],    
                        "chartScrollbar": {
                            "graph": "g1",
                            "oppositeAxis": false,
                            "offset": 30,
                            "scrollbarHeight": 20,
                            "backgroundAlpha": 0,
                            "selectedBackgroundAlpha": 0.1,
                            "selectedBackgroundColor": "#888888",
                            "graphFillAlpha": 0,
                            "graphLineAlpha": 0.5,
                            "selectedGraphFillAlpha": 0,
                            "selectedGraphLineAlpha": 1,
                            "autoGridCount": true,
                            "color": "#AAAAAA"
                        },
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha": 0,
                            "valueLineAlpha": 0.2
                        },
                        "categoryField": "date",
                        "categoryAxis": {
                            "parseDates": true,
                            "dashLength": 1,
                            "minorGridEnabled": true
                        },
                        "legend": {                            
                            "autoMargins": false,
                            "borderAlpha": 0.2,
                            "equalWidths": false,
                            "horizontalGap": 50,
                            "verticalGap": 15,
                            "markerSize": 10,
                            "useGraphSettings": true,
                            "valueAlign": "left",
                            "valueWidth": 0
                        },
                        "balloon": {
                            "borderThickness": 1,
                            "shadowAlpha": 0
                        },
                        "panelsSettings": {
                          "usePrefixes": true
                        },                        
                        "export": {
                            "enabled": false
                        } 
                    });
                </script> 
                <br><br><br> 
            </div> 
            <br><br><br> 
        </div> 
        
        <!-- APARTADO PARA PICKERS --> 
        <div class="row" style="margin-top: 1.4%" >
            <div id="pnlPikers" class="col-lg-12 col-md-12 col-xs-12 col-sh-12" >  
                <script>
                    $(function() { 
                        $("#dateIni").datepicker({ 
                            //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                            maxDate: new Date(), 
                            //OYENTES DE LOS PICKERS 
                            onSelect: function(date) { 
                                //Cuando se seleccione una fecha se cierra el panel
                                $("#ui-datepicker-div").hide(); 
                            } 
                        }); 

                        $("#dateEnd").datepicker({ 
                            //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                            maxDate: new Date(), 
                            //OYENTES DE LOS PICKERS 
                            onSelect: function ( date ) { 
                                //cuando se seleccione una fecha se cierra el panel 
                                $("#ui-datepicker-div").hide(); 
                            } 
                        }); 
                    }); 
                </script> 

                <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado container container-fluid" >
                    <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3 contenidoCentrado input-group" style="float: left;" > 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo date("m/d/Y", strtotime($_SESSION['FIni'])); ?>" >
                        <span class="input-group-addon"> a </span> 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($_SESSION['FFin'])); ?>" >
                    </div>
                    <div class="col-xs-1 col-sh-1 col-md-1 col-lg-1 contenidoCentrado" style="float: left;" > 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0;" onclick="setTipoDatos()" > 
                            <img src="../../imagenes/confirmar.png"> 
                        </button> 
                    </div> 
                    <div class="col-xs-5 col-sh-5 col-md-5 col-lg-5 contenidoCentrado" style="float: left;" > 
<!--                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0; margin-left: 100%; " onclick="setTipoDatos2()"> 
                            <img src="../../imagenes/bAll.png"> 
                        </button> -->
                    </div> 
                </div> 
            </div> 
        </div> 
            
        <div class="row" style="margin-top: 25px;" > 
            <div id="holder-semple-1" style="margin-top: 8%; width: 98%;" >
                <script id="tamplate-semple-1" type="text/mustache"> 
                    <table style="width: 95%; margin-left: 3%; margin-top: 2%; " class="inner-table"> 
                        <thead> 
                            <tr> 
                                <td colspan="2"> </td> 
                                <td colspan="<?php echo 13; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                            </tr> 
                            <tr> 
                                <td>&nbsp;</td> 
                                <td align="center" >PROBLEMA</td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                                ?>
                                    <td align="center" ><?php echo $dateT[$vDate]; ?></td>
                                <?php } ?> 
                            </tr> 
                        </thead> 
                        <tbody>                     
                            <tr >
                                <td align="right" ><?php ?></td>
                                <td >Productividad</td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2];                                  
                                ?>
                                <td align="center" ><?php echo $productivityDay[$vDate]; ?></td>
                                <?php } ?>                                
                            </tr >  
                            <tr> 
                                <td align="right" ><?php ?></td>
                                <td >Meta</td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
                                    $date = explode("-", $i); 
                                    $vDate = $date[0].$date[1].(int)$date[2];                                  
                                ?> 
                                <td align="center" ><?php echo $targetDay[$vDate]; ?></td>
                                <?php } ?>
                            </tr>    
                        </tbody>                
                    </table>
                </script>
            </div>
        </div>
        
        <br>
        
        
        
        