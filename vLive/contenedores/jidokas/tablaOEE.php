<html>
<head>
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script>  
    <script>        
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({ 
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
</head>

<?php 
    include '../../db/ServerFunctions.php';
    session_start();
    $date = new DateTime;

    //RECIBIMOS LOS VALORES DEL AJAX
    if (isset($_POST["tema"])){
        $tema = $_POST["tema"];
    } else {
        $tema = "Técnicas";
    }

    if (isset($_SESSION['linea'])) {
        $line = $_SESSION['linea'];
    } else {
        $line = 'L001';
    }            

    if (isset($_POST["fIni"]) && isset($_POST["fFin"])){            
        $fI = explode("/", $_POST["fIni"]);
        $fecha1 = (string) $fI[2].'-'.$fI[0].'-'.$fI[1];            

        $fF = explode("/", $_POST["fFin"]); 
        $fecha2 = (string) $fF[2].'-'.$fF[0].'-'.$fF[1];  
    } else {   
        $anio = date('Y');
        $mes = date('m');

        $fecha1 = $anio.'-'.$mes.'-01';
        $fecha2 = date("Y-m-d");
    } 

    //SE HACE LA CONSULTA SEGUN SEA EL TEMA, 
    //YA QUE EN PAROS PLANEADOS SOLO SE TIENE INFORMACION HASTA AREA, 
    //Y PROBLEMA SE TIENE EN BLANCO A COMPARACION DE LOS OTROS PROBLEMAS
    if(isset($_POST["tipoDato"])){
        $tipo = $_POST["tipoDato"];
    } else {
        $tipo = 1;
    }

    $countProblem = 0;
    $problema[1] = "OEE (%)";
    $problema[2] = "NO Calida (%)";
    $problema[3] = "Organizacionales (%)";
    $problema[4] = "Tecnicas (%)";
    $problema[5] = "Cammbio de Modelo (%)";
    $problema[6] = "Desempeño (%)";
    $countX = 1;
    $total = 0;
    $sumProblemas = 0;

    //INCIALIZAMOS LAS VARIABLES
    //TABLA
    for ($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) {        
        $countX++;
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        switch($date[1]){
            case 1:
                $cadDate[$i] = 'Jan&nbsp;'.$date[2];
                break;
            case 2:
                $cadDate[$i] = 'Feb&nbsp;'.$date[2];
                break;
            case 3:
                $cadDate[$i] = 'Mar&nbsp;'.$date[2];
                break;
            case 4:
                $cadDate[$i] = 'Apr&nbsp;'.$date[2];
                break;
            case 5:
                $cadDate[$i] = 'May&nbsp;'.$date[2];
                break;
            case 6:
                $cadDate[$i] = 'Jun&nbsp;'.$date[2];
                break;
            case 7:
                $cadDate[$i] = 'Jul&nbsp;'.$date[2];
                break;
            case 8:
                $cadDate[$i] = 'Aug&nbsp;'.$date[2];
                break;
            case 9:
                $cadDate[$i] = 'Sep&nbsp;'.$date[2];
                break;
            case 10:
                $cadDate[$i] = 'Oct&nbsp;'.$date[2];
                break;
            case 11:
                $cadDate[$i] = 'Nov&nbsp;'.$date[2];
                break;
            case 12:
                $cadDate[$i] = 'Dec&nbsp;'.$date[2];
                break;
        }

        //INICIALIZAMOS LAS VARIABLES PARA LOS SUBTOTALES DIA(_) 
//        $targetDayT[$vDate] = 80;        
        $pTopic[1][$vDate] = 0;
        $pTopic[2][$vDate] = 0;
        $pTopic[3][$vDate] = 0;
        $pTopic[4][$vDate] = 0;
        $pTopic[5][$vDate] = 0;   
        $pTopic[6][$vDate] = 0;
        $pTotalDay[$vDate] = 0;
    }
    
    //echo $line,', ' ,$fecha1,', ',$fecha2;
    $datOEEDay = percentOEERangeDays($line, $fecha1, $fecha2);  
    
    //EVALUACION PARA EL TIPO DE DATO QUE SE TIENE 
    for ($i = 0; $i < count($datOEEDay); $i++  ){        
        //$d = $datOEEDay[$i][0];       
        $date = explode("-", $datOEEDay[$i][0]);
        $vDate = $date[0].$date[1].(int)$date[2];
        
        $pProdDay[1][$vDate] = @round($datOEEDay[$i][1],2);
        $pTecDay[2][$vDate] = @round($datOEEDay[$i][2],2);
        $pOrgDay[3][$vDate] = @round($datOEEDay[$i][3],2);
        $pCaliDay[4][$vDate] = @round($datOEEDay[$i][4],2);
        $pCamDay[5][$vDate] = @round($datOEEDay[$i][5],2);
        //$pPlanDay[$d] = $datOEEDay[$i][5];        
        $pTFalDay[6][$vDate] = @round($datOEEDay[$i][6],2);
        
        $pTopic[1][$vDate] = @round($datOEEDay[$i][1],2);
        $pTopic[2][$vDate] = @round($datOEEDay[$i][2],2);
        $pTopic[3][$vDate] = @round($datOEEDay[$i][3],2);
        $pTopic[4][$vDate] = @round($datOEEDay[$i][4],2);
        $pTopic[5][$vDate] = @round($datOEEDay[$i][5],2);
        $pTopic[6][$vDate] = @round($datOEEDay[$i][6],2);
        $pTotalDay[$vDate] = @round($pTopic[1][$vDate]+$pTopic[2][$vDate]+$pTopic[3][$vDate]+$pTopic[4][$vDate]+$pTopic[5][$vDate]+$pTopic[6][$vDate],1);
        //echo $datOEEDay[$i][0],': ',$pTopic[1][$vDate],', ', $pTopic[2][$vDate],'<br>';
    }     

    //DATOS PARA TAMAÑO DE TABLA    
    $dias = (strtotime($fecha1)- strtotime($fecha2))/86400;
    $dias = abs($dias);
    $dias = floor($dias);    
    
    //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
    //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
    if ($dias > 11 ){
        $rowspan = 12;
    } else {
        //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
        //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS
        $rowspan = $dias+1;
    }
       
    //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
    if($countProblem == 0) { 
        $totalProblema[1] = 0;
        $countProblem = 1;
    }
    
?>

<body>
    <div id="holder-semple-1" style="margin-top: -1%">
        <script id="tamplate-semple-1" type="text/mustache">
            <table width="98%" class="inner-table">
                <thead>
                    <tr>
                        <td colspan="2"> </td>
                        <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+1; ?>" > PERIODO </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td align="center" >TOPIC</td>
                        <?php for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ ?>
                            <td align="center" ><?php echo $cadDate[$i]; ?></td>
                        <?php } ?> 
                    </tr>
                </thead>
                <tbody>
                    <?php for($i = 1; $i <= 6; $i++) { ?>
                    <tr > 
                        <td align="right" ><?php echo $i ?></td>  
                        <td ><?php echo $problema[$i]?></td> 
                        <?php for($j = $fecha1; $j <= $fecha2; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                            $date = explode("-", $j); 
                            $vDate = $date[0].$date[1].(int)$date[2]; 
                        ?> 
                        <td align="center" ><?php echo floor($pTopic[$i][$vDate]); ?></td> 
                    <?php }} ?> 
                </tbody> 
            </table> 
        </script>
    </div>
</body>
</html>
