<html>
<head>
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script>  
    <script>        
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
</head>

<?php 
    include '../../db/ServerFunctions.php';
    session_start();
    $date = new DateTime;

    //RECIBIMOS LOS VALORES DEL AJAX
//    if (isset($_POST["tema"])){
//        $tema = $_POST["tema"];
//    } else {
//        $tema = Técnicas;
//    }
//
//    if (isset($_SESSION['linea'])) {
//        $line = $_SESSION['linea'];
//    } else {
//        $line = 'L001';
//    }            
//
//    //echo $_POST["fIni"],'<br>';
//    if (isset($_POST["fIni"]) && isset($_POST["fFin"])){ 
//        $fI = explode("/", $_POST["fIni"]); 
//        $fecha1 = (string) $fI[2].'-'.$fI[0].'-'.$fI[1]; 
//
//        $fF = explode("/", $_POST["fFin"]); 
//        $fecha2 = (string) $fF[2].'-'.$fF[0].'-'.$fF[1]; 
//    } else {   
//        $anio = date('Y');
//        $mes = date('m');
//
//        $fecha1 = $anio.'-'.$mes.'-01';
//        $fecha2 = date("Y-m-d");
//    }        

    $tema = $_SESSION['jTema'];
    $line = $_SESSION['linea'];
    $fecha1 = $_SESSION['jFIni'];
    $fecha2 = $_SESSION['jFFin'];
    $tipo = $_SESSION["jTipo"];
    
    echo '<br>',$tema,', ',$line,', ',$tipo,': ',$fecha1,', ',$fecha2 ;
    //SE HACE LA CONSULTA SEGUN SEA EL TEMA, 
    //YA QUE EN PAROS PLANEADOS SOLO SE TIENE INFORMACION HASTA AREA, 
    //Y PROBLEMA SE TIENE EN BLANCO A COMPARACION DE LOS OTROS PROBLEMAS
    
    
    //echo '<br>',$tema,', ',$line,' fi: ', $fecha1,' ff: ',$fecha2,' t: ',$tipo ;
    
    if ($tema == "Paros Planeados") { 
        switch ($tipo){
            case "1":
                $jTDayTable = tablaJidokaPlaneados ($line, $fecha1, $fecha2);
                break;
            case "2":
                $jTDayTable = tablaJidokaPlaneadosF ($line, $fecha1, $fecha2);
                break;
            case "3":
                $jTDayTable = tablaJidokaNOPlaneadosP($tema, $line, $fecha1, $fecha2);
                break;
        }
    } else if ($tema == "Cambio de Modelo") { 
        switch ($tipo){            
            case "1":
                $jTDayTable = tablaJidokaNOPlaneados($tema, $line, $fecha1, $fecha2);
                break;
            case "2":
                $jTDayTable = tablaJidokaNOPlaneadosF($tema, $line, $fecha1, $fecha2);
                break;
            case "3":
                $jTDayTable = tablaJidokaNOPlaneadosP($tema, $line, $fecha1, $fecha2);
                break;
        }  
    } else {
        switch ($tipo){            
            case "1":
                $jTDayTable = tablaJidokaNOPlaneados($tema, $line, $fecha1, $fecha2);
                break;
            case "2":
                $jTDayTable = tablaJidokaNOPlaneadosF($tema, $line, $fecha1, $fecha2);
                break;
            case "3":
                $jTDayTable = tablaJidokaNOPlaneadosP($tema, $line, $fecha1, $fecha2);
                break;
        }        
    }       

    $countProblem = 0;
    $problema[0] = "";
    $countX = 1;
    $total = 0;
    $sumProblemas = 0;

    //INCIALIZAMOS LAS VARIABLES
    //TABLA
    for ($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) {        
        $countX++;
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        switch($date[1]){
            case 1:
                $cadDate[$i] = 'Jan&nbsp;'.$date[2];
                break;
            case 2:
                $cadDate[$i] = 'Feb&nbsp;'.$date[2];
                break;
            case 3:
                $cadDate[$i] = 'Mar&nbsp;'.$date[2];
                break;
            case 4:
                $cadDate[$i] = 'Apr&nbsp;'.$date[2];
                break;
            case 5:
                $cadDate[$i] = 'May&nbsp;'.$date[2];
                break;
            case 6:
                $cadDate[$i] = 'Jun&nbsp;'.$date[2];
                break;
            case 7:
                $cadDate[$i] = 'Jul&nbsp;'.$date[2];
                break;
            case 8:
                $cadDate[$i] = 'Aug&nbsp;'.$date[2];
                break;
            case 9:
                $cadDate[$i] = 'Sep&nbsp;'.$date[2];
                break;
            case 10:
                $cadDate[$i] = 'Oct&nbsp;'.$date[2];
                break;
            case 11:
                $cadDate[$i] = 'Nov&nbsp;'.$date[2];
                break;
            case 12:
                $cadDate[$i] = 'Dec&nbsp;'.$date[2];
                break;
        }

        //INICIALIZAMOS LAS VARIABLES DE LA TABLA
        if (count($jTDayTable) > 0 ){
            for ($j = 1; $j <= count($jTDayTable); $j++ ) {
                $valueProblemTable[$vDate][$j] = 0;
                
                //VARIABLE DEFINIDA SOLO PARA CUANDO EL TIPO DE DATO SEA PIEZAS
                $valueDuracionTable[$vDate][$j] = 0;
            }
        } else {
            $valueProblemTable[$vDate][1] = 0;
        }
        
        //INICIALIZAMOS LAS VARIABLES PARA LOS SUBTOTALES DIA(_)            
        $totalDia[$vDate] = 0;        
        $fecha[$vDate] = $i;
        $valueDayT[$vDate] = 0;
        $targetDayT[$vDate] = 80;
    }

    //EVALUACION PARA EL TIPO DE DATO QUE SE TIENE        
    if($tipo != 3 ){
        for ($i = 0; $i < count($jTDayTable); $i++ ) {
            $date = explode("-", $jTDayTable[$i][1]);
            $vDate = $date[0].$date[1].(int)$date[2];

            if ($i == 0){               
                $countProblem = 1;
                $problema[$countProblem] = $jTDayTable[$i][0];
                
                //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                $totalProblema[$countProblem] = 0;                
            } else {
                if ($problema[$countProblem] != $jTDayTable[$i][0] ){
                    $countProblem++;
                    $problema[$countProblem] = $jTDayTable[$i][0];
                    
                    //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                    $totalProblema[$countProblem] = 0;
                }
            }            
            //PASO DE VALORES A VARIABLE DE SUBTOTAL PROBLEMA(|) 
            $totalProblema[$countProblem] += $jTDayTable[$i][2];

            //PASO DE VALORES A VARIABLE DE PROBLEMA DIA
            $valueProblemTable[$vDate][$countProblem] = $jTDayTable[$i][2]; 

            //ASIGNACION DE VALORES PARA SUBTOTALES DE DIA(_)
            $totalDia[$vDate] += $jTDayTable[$i][2];

            //TOTAL QUE SE MOSTRARA EN LA ESQUINA INFERIOR DERECHA
            $total += $jTDayTable[$i][2];
            
            //echo '<br>',$totalProblema[$countProblem],', ',$valueProblemTable[$vDate][$countProblem];
        }
    } else { //CONSULTA DE BASE DE DATOS CUANDO LA OPCION ES PIEZAS
        for ($i = 0; $i < count($jTDayTable); $i++ ) {
            $date = explode("-", $jTDayTable[$i][1]);
            $vDate = $date[0].$date[1].(int)$date[2];

            if ($i == 0) {
                $tc = 1;
                $countProblem = 1;
                $problema[$countProblem] = $jTDayTable[$i][0];
                
                //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                $totalProblema[$countProblem] = 0;
                
                //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA
                $cTC = tcLineaNoParte($line, $jTDayTable[$i][2]);
                $valueProblemTable[$vDate][$countProblem] = ($jTDayTable[$i][3] *  60) / $cTC[0][0];
                
                //ASIGNACION DE VALORES PARA SUBTOTALES DE PROBLEMA (|)
                //PASA EL PRIMER VALOR QUE SE TIENE EN EL PRIMER ENCUENTRO DEL PROBLEMA
                $totalProblema[$countProblem] = floor(($jTDayTable[$i][3] *  60) / $cTC[0][0]);
            } else {
                if ($problema[$countProblem] != $jTDayTable[$i][0] ){
                    $countProblem++;
                    $tc = 1;
                    $problema[$countProblem] = $jTDayTable[$i][0];
                    
                    //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                    $totalProblema[$countProblem] = 0;
                    
                    //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA
                    $cTC = tcLineaNoParte($line, $jTDayTable[$i][2]); 
                    $valueProblemTable[$vDate][$countProblem] = @(($jTDayTable[$i][3] *  60) / $cTC[0][0]);
                    
                } else {
                    $tc++;
                    
                    //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA
                    $cTC = tcLineaNoParte($line, $jTDayTable[$i][2]);
                    $valueProblemTable[$vDate][$countProblem] += @(($jTDayTable[$i][3] *  60) / $cTC[0][0]);
                }
                //SUBTOTALES DE CUADRO DE INFROMACION
                //SUMATORIA DE TOTAL POR PROBLEMA (|)
                $totalProblema[$countProblem] += @floor(($jTDayTable[$i][3] *  60) / $cTC[0][0]);  
                //SUMATORIA DE TOTAL POR DIA (_)
                $totalDia[$vDate] += @floor(($jTDayTable[$i][3] *  60) / $cTC[0][0]);
                
            } 
            //PASO DE VALORES A VARIABLE DE PROBLEMA - DIA
            $valueProblemTable[$vDate][$countProblem] = @floor($valueProblemTable[$vDate][$countProblem]);
            
            //TOTAL DE LA INFORMACION ( VA EN LA ESQUINA INFERIOR DERECHA)
            $total += @floor(($jTDayTable[$i][3] *  60) / $cTC[0][0]);
        }
    }        

    //DATOS PARA TAMAÑO DE TABLA    
    $dias = (strtotime($fecha1)- strtotime($fecha2))/86400;
    $dias = abs($dias);
    $dias = floor($dias);    
    
    //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
    //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
    if ($dias > 11 ){
        $rowspan = 12;
    } else {
        //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
        //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS
        $rowspan = $dias+1;
    }
       
    //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
    if($countProblem == 0){
        $problema[1] = "-";        
        $totalProblema[1] = 0;
        $countProblem = 1;
    }
    
?>

<body>
    <div id="holder-semple-1">
        <script id="tamplate-semple-1" type="text/mustache"> 
            <table width="98%" class="inner-table"> 
                <thead> 
                    <tr> 
                        <td colspan="2"> </td> 
                        <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                        <td rowspan="2" align="center" >Total</td> 
                    </tr> 
                    <tr> 
                        <td>&nbsp;</td> 
                        <td align="center" >PROBLEMA</td>
                        <?php for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ ?>
                            <td align="center" ><?php echo $cadDate[$i]; ?></td>
                        <?php } ?> 
                    </tr>
                </thead>
                <tbody>
                    <?php for($i = 1; $i <= $countProblem; $i++) { ?>
                    <tr >
                        <td align="right" ><?php echo $i ?></td>
                        <td ><?php echo $problema[$i]?></td>
                        <?php for($j = $fecha1; $j <= $fecha2; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                            $date = explode("-", $j); 
                            $vDate = $date[0].$date[1].(int)$date[2];                                  
                        ?>
                        <td align="center" ><?php echo floor($valueProblemTable[$vDate][$i]); ?></td>
                        <?php if ( $j == $fecha2 ) {?>   
                        <td align="center" ><?php echo $totalProblema[$i]; ?> </td>    
                    </tr >
                    <?php }}} ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">Sold Total</td>
                        <?php for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                $date = explode("-", $i); 
                                $vDate = $date[0].$date[1].(int)$date[2];
                        ?>
                            <td align="center" ><?php echo $totalDia[$vDate]; ?></td>
                        <?php if ($i == $fecha2) { ?>                            
                            <!--CUADRE DE INFORMACION -->
                            <td align="center" ><?php echo $total; ?></td>
                        <?php }} ?>   
                    </tr>
                </tfoot>
            </table>
        </script>
    </div>
</body>
</html>
