<HTML>
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    
    <title>OEE</title>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script>  
    <script>        
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({ 
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 0, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 


        function setTipoDatos() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes.php", 
                type: "post", 
                data: { tipoVista: 1 , fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas                    
                    location.reload(); 
                } 
            }); 
        } 

        function getFixedColumnsData() {} 
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
    
    
    <?php 
        include '../../db/ServerFunctions.php'; 
        session_start(); 
        $date = new DateTime; 
        
        //RECIBE LOS VALORES DEL AJAX 
        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes']; 
                
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio)); 

        $date->setISODate("$anio", 53);

        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }

        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        # Obtenemos el numero de la semana
        $semana = date("W",mktime(0,0,0,$mes,01,$anio));

        # Obtenemos el día de la semana de la fecha dada
        $nWI = date("w",mktime(0,0,0,$mes,01,$anio));

        # el 0 equivale al domingo...
        if($nWI == 0)
            $nWI=7;

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
        $fWI = date("Y-m-d",mktime(0,0,0,$mes,01-$nWI+1,$anio));

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
        $fWF = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(7-$nWI),$anio));

        //INICIALIZAMOS VARIABLES PARA SEMANA 
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0;
                $prodW[$i] = 0;
                $tecW[$i] = 0;
                $orgW[$i] = 0;
                $calW[$i] = 0;
                $cambW[$i] = 0;
                $tFalW[$i] = 0;
                $targetW[$i] = 0;
            }

            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0; 
                $prodW[$i] = 0; 
                $tecW[$i] = 0; 
                $orgW[$i] = 0; 
                $calW[$i] = 0; 
                $cambW[$i] = 0; 
                $tFalW[$i] = 0; 
                $targetW[$i] = 0; 
            }            
        } else { 
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $prodW[$i] = 0;
                $tecW[$i] = 0;
                $orgW[$i] = 0;
                $calW[$i] = 0;
                $cambW[$i] = 0;
                $tFalW[$i] = 0; 
                $targetW[$i] = 0;
            } 
        } 
        
        $lblM[1] = (string) "Jun";
        $lblM[2] = (string) "Feb";
        $lblM[3] = (string) "Mar";
        $lblM[4] = (string) "Apr";
        $lblM[5] = (string) "May";
        $lblM[6] = (string) "Jun";
        $lblM[7] = (string) "Jul";
        $lblM[8] = (string) "Aug";
        $lblM[9] = (string) "Sep";
        $lblM[10] = (string) "Oct";
        $lblM[11] = (string) "Nov";
        $lblM[12] = (string) "Dec";     

        #INCIALIZACION DE VARIABLES PARA LAS FECHAS
        for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            
            switch($date[1]){
                case 1:
                    $cadDate[$i] = 'Jan&nbsp;'.$date[2];
                    break;
                case 2:
                    $cadDate[$i] = 'Feb&nbsp;'.$date[2];
                    break;
                case 3:
                    $cadDate[$i] = 'Mar&nbsp;'.$date[2];
                    break;
                case 4:
                    $cadDate[$i] = 'Apr&nbsp;'.$date[2];
                    break;
                case 5:
                    $cadDate[$i] = 'May&nbsp;'.$date[2];
                    break;
                case 6:
                    $cadDate[$i] = 'Jun&nbsp;'.$date[2];
                    break;
                case 7:
                    $cadDate[$i] = 'Jul&nbsp;'.$date[2];
                    break;
                case 8:
                    $cadDate[$i] = 'Aug&nbsp;'.$date[2];
                    break;
                case 9:
                    $cadDate[$i] = 'Sep&nbsp;'.$date[2];
                    break;
                case 10:
                    $cadDate[$i] = 'Oct&nbsp;'.$date[2];
                    break;
                case 11:
                    $cadDate[$i] = 'Nov&nbsp;'.$date[2];
                    break;
                case 12:
                    $cadDate[$i] = 'Dec&nbsp;'.$date[2];
                    break;
            }
            
            $fecha[$vDate] = date("M, d",strtotime($i));//$i; 
            $prodDay[$vDate] = 0; 
            $caliDay[$vDate] = 0; 
            $orgDay[$vDate] = 0; 
            $tecDay[$vDate] = 0; 
            $cambioDay[$vDate] = 0; 
            $desDay[$vDate] = 0; 
            $targetDay[$vDate] = 0; 
            $pTotalDay[$vDate] = 0;            
        } 

        #CONSULTA PARA OEE DIARIO
        $oeeProductionDay = oeeProductionDay($_SESSION['linea'] , $_SESSION['FIni'], $_SESSION['FFin']);     
        for($i = 0; $i < count($oeeProductionDay); $i++) {     
            $date = explode("-", $oeeProductionDay[$i][0],3);     
            $vDate = $date[0].$date[1].(int)$date[2];  
            $prodDay[$vDate] = @round($oeeProductionDay[$i][1],2); 
            $caliDay[$vDate] = @round($oeeProductionDay[$i][4],2); 
            $orgDay[$vDate] = @round($oeeProductionDay[$i][3],2); 
            $tecDay[$vDate] = @round($oeeProductionDay[$i][2],2); 
            $cambioDay[$vDate] = @round($oeeProductionDay[$i][5],2); 
            $desDay[$vDate] = @round($oeeProductionDay[$i][6],2); 
            
            //DATOS PARA LA TABLA 
            $pTopic[1][$vDate] = @round($oeeProductionDay[$i][1],2); 
            $pTopic[2][$vDate] = @round($oeeProductionDay[$i][4],2); 
            $pTopic[3][$vDate] = @round($oeeProductionDay[$i][3],2);
            $pTopic[4][$vDate] = @round($oeeProductionDay[$i][2],2); 
            $pTopic[5][$vDate] = @round($oeeProductionDay[$i][5],2); 
            $pTopic[6][$vDate] = @round($oeeProductionDay[$i][6],2); 
            $pTotalDay[$vDate] = @round($pTopic[1][$vDate]+$pTopic[2][$vDate]+$pTopic[3][$vDate]+$pTopic[4][$vDate]+$pTopic[5][$vDate]+$pTopic[6][$vDate],1);
            //echo $oeeProductionDay[$i][0],': ',$pTopic[1][$vDate],', ', $pTopic[2][$vDate],'<br>';
        }
        
        #TARGET
        $ctargetDay = cTargetOEEDaily($_SESSION['linea'] , $_SESSION['FIni'], $_SESSION['FFin']);
        for ($i = 0; $i < count($ctargetDay); $i++){
            $date = explode("-", $ctargetDay[$i][0],3);         
            $vDate = $date[0].$date[1].(int)$date[2];  
            $targetDay[$vDate] = $ctargetDay[$i][1];
        }             
        
        /***************** MENSUAL **********************************/ 
        for ($i = 0; $i < 13; $i++){
            $prodM[$i] = 0;
            $tecM[$i] = 0;
            $orgM[$i] = 0;
            $caliM[$i] = 0;
            $camM[$i] = 0;
            $tFalM[$i] = 0;
            $targetM[$i] = 0;
        }

        $cPercentTopicMonthly = cPercentTopicMonthly($_SESSION['linea'] , $anio); 
        
        for ($i = 0; $i < count($cPercentTopicMonthly); $i++){ 
            $m = $cPercentTopicMonthly[$i][0]; 
            $prodM[$m] = @round($cPercentTopicMonthly[$i][1],2); 
            $tecM[$m] = @round($cPercentTopicMonthly[$i][2],2); 
            $orgM[$m] = @round($cPercentTopicMonthly[$i][3],2); 
            $caliM[$m] = @round($cPercentTopicMonthly[$i][4],2); 
            $camM[$m] = @round($cPercentTopicMonthly[$i][5],2); 
            $tFalM[$m] = @round($cPercentTopicMonthly[$i][6],2); 
            //echo "<br>", $m,', ',$prodM[$m],', ',$tecM[$m],', ',$orgM[$m];
        } 
        
//        #TARGET 
        $cTargetMonth = cTargetOEEMonthly($_SESSION['linea'] , $anio); 
        for ($i = 0; $i < count($cTargetMonth); $i++ ){ 
            $m = $cTargetMonth[$i][0]; 
            $targetM[$m] = $cTargetMonth[$i][1]; 
        }     
        
        /************************** SEMANAL ****************************/
        $datOEEWeek = cPercentTopicWekly($_SESSION['linea'] , $fWI, $fWF);
        for ($i = 0; $i < count($datOEEWeek); $i++){
            $s = $datOEEWeek[$i][0];
            $prodW[$s] = @round($datOEEWeek[$i][1],2);
            $tecW[$s] = @round($datOEEWeek[$i][2],2);
            $orgW[$s] = @round($datOEEWeek[$i][3],2);
            $calW[$s] = @round($datOEEWeek[$i][4],2);
            $cambW[$s] = @round($datOEEWeek[$i][5],2);
            $tFalW[$s] = @round($datOEEWeek[$i][6],2);
        } 
        
        #TARGET
        $cTargetWeek = cTargetOEEWeekly($_SESSION['linea'] , $anio, $mes);
        for($i = 0; $i < count($cTargetWeek); $i++){
            $w = $cTargetWeek[$i][0];
            $targetW[$w] = $cTargetWeek[$i][1];
        }
        
        /************************* TABLA DIA *********************************/
        $countProblem = 0;
        $problema[1] = "OEE (%)";
        $problema[2] = "NO Calida (%)";
        $problema[3] = "Organizacionales (%)";
        $problema[4] = "Tecnicas (%)";
        $problema[5] = "Cammbio de Modelo (%)";
        $problema[6] = "Desempeño (%)";
        $countX = 1;
        $total = 0;
        $sumProblemas = 0;

        //DATOS PARA TAMAÑO DE TABLA    
        $dias = (strtotime($_SESSION['FIni'])- strtotime($_SESSION['FFin']))/86400;
        $dias = abs($dias);
        $dias = floor($dias);    

        //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
        //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
        if ($dias > 11 ){ 
            $rowspan = 12; 
        } else { 
            //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
            //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS
            $rowspan = $dias+1;
        }

        //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
        if($countProblem == 0) { 
            $totalProblema[1] = 0; 
            $countProblem = 1; 
        }
        
    ?>
    
    <body>
        <div >  
            <div class=" row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" style="margin-top: 2.1%" > 
                <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                    <script>
                        var chart = AmCharts.makeChart("jTMonth", {
                            "type": "serial",
                            "theme": "none",
                            "precision": 2,
                            "titles": [{
                                "text": "Mensual",
                                "size": 12
                            }],                             
                            "dataProvider": [
                                <?php for($i = 1; $i <= 12; $i++) { ?>
                                {
                                    "date": "<?php echo $lblM[$i]; ?>", 
                                    "prod": <?php echo $prodM[$i]; ?>, 
                                    "calidad": <?php echo $caliM[$i]; ?>,
                                    "org": <?php echo $orgM[$i]; ?>,
                                    "tec": <?php echo $tecM[$i]; ?>,
                                    "cambio": <?php echo $camM[$i]; ?>,
                                    "desempenio": <?php echo $tFalM[$i]; ?>,
                                    "Meta": <?php echo $targetM[$i]; ?>
                                },
                                <?php } ?>        
                            ], 
                            "valueAxes": [{ 
                                "title": "Indicador" ,
                                "maximum": 100,
                                "minimum": 0,
                                "stackType": "regular",
                                "unit": "%",
                                "axisAlpha": 0.5,
                                "gridAlpha": 0.2,
                                "labelsEnabled": true,
                                "position": "left"
                            }], 
                            "graphs": [ {
                                "fillAlphas": 1,
                                "fillColors": "#03A64A",
                                "lineColor": "#03A64A",
//                                "fillColors": "#62cf73",
//                                "lineColor": "#62cf73",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "id":"mProd",
                                "title": "OEE",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "prod", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, {
                                "fillAlphas": 1,
                                "fillColors": "#0477BF",
                                "lineColor": "#0477BF",
//                                "fillColors": "#311B92",
//                                "lineColor": "#311B92",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Técnicas",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "tec", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, {
                                "fillAlphas": 1,
                                "fillColors": "#F06292",
                                "lineColor": "#F06292C",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Organizacionales",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "org", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "fillAlphas": 1,
                                "fillColors": "#F20505",
                                "lineColor": "#F20505",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Calidad",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "calidad", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "fillAlphas": 1,
                                "fillColors": "#000000",
                                "lineColor": "#000000",
//                                "fillColors": "#3498db",
//                                "lineColor": "#3498db",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Cambios",
                                "type": "column",
                                "color": "#FFFFFF",
                                "valueField": "cambio", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "fillAlphas": 1,
                                "fillColors": "#9E9E9E", 
                                "lineColor": "#9E9E9E", 
                                "labelText": "[[value]]", 
                                "lineAlpha": 1,
                                "title": "Desempeño", 
                                "type": "column", 
                                "color": "#FFFFFF",
                                "valueField": "desempenio",
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "id": "graph2",                                 
                                "lineThickness": 2,                 
                                "lineColor": "#F25C5C",
                                "fillAlphas": 0, 
                                "lineAlpha": 2, 
                                "title": "Meta", 
                                "valueField": "Meta", 
                                "dashLengthField": "dashLengthLine",
                                "balloonText": "<b>[[category]]</b><br>Meta: </b><span style='font-size:100%'><b>[[value]]%</b></span><br>OEE: </b><span style='font-size:100%'><b>[[prod]]%</b></span><br>Técnicas: </b><span style='font-size:100%'><b>[[tec]]%</b></span><br>Organizacional: </b><span style='font-size:100%'><b>[[org]]%</b></span><br>Calidad: </b><span style='font-size:100%'><b>[[calidad]]%</b></span><br>Cambio: </b><span style='font-size:100%'><b>[[cambio]]%</b></span><br>Desempenio: </b><span style='font-size:100%'><b>[[desempenio]]%</b></span>"
                            }],                
                            "chartCursor": { 
                                "pan": true, 
                                "valueLineEnabled": true, 
                                "cursorAlpha": 0, 
                                "valueLineAlpha": 0.2 
                            }, 
                            "categoryField": "date", 
                            "categoryAxis": { 
                                "gridPosition": "start",
                                "axisAlpha": 0,
                                "gridAlpha": 0.2,
                                "position": "left"
                            }
                        });
                    </script>
                </div>     

                <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                    <script>
                        var chart = AmCharts.makeChart("jTWeek", {
                            "type": "serial",
                            "theme": "none",
                            "precision": 2, 
                            "titles": [{
                                "text": "Semanal",
                                "size": 12
                            }],
                            
                            "dataProvider": [
                            <?php 
                                if ($sP > $sL) { 
                                    for ($i = $sP; $i <= $numSemanas; $i++ ) { 
                                    ?>
                                        {
                                            "date": "<?php echo 'CW-'.$i ?>",
                                            "prod": <?php echo $prodW[$i] ?>, 
                                            "calidad": <?php echo $calW[$i] ?>,
                                            "org": <?php echo $orgW[$i] ?>,
                                            "tec": <?php echo $tecW[$i] ?>,
                                            "cambio": <?php echo $cambW[$i] ?>,
                                            "desempenio": <?php echo $tFalW[$i] ?>,
                                            "Meta": <?php echo $targetW[$i] ?>
                                        },
                                    <?php } 
                                        for($i = 1; $i <= $sL; $i++) { 
                                    ?>
                                        {
                                            "date": "<?php echo 'CW-'.$i ?>",
                                            "prod": <?php echo $prodW[$i] ?>, 
                                            "calidad": <?php echo $calW[$i] ?>,
                                            "org": <?php echo $orgW[$i] ?>,
                                            "tec": <?php echo $tecW[$i] ?>,
                                            "cambio": <?php echo $cambW[$i] ?>,
                                            "desempenio": <?php echo $tFalW[$i] ?>,
                                            "Meta": <?php echo $targetW[$i] ?>
                                        },
                                    <?php } ?>                            
                                    <?php } else { 
                                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                        {
                                            "date": "<?php echo 'CW-'.$i ?>",
                                            "prod": <?php echo $prodW[$i] ?>, 
                                            "calidad": <?php echo $calW[$i] ?>,
                                            "org": <?php echo $orgW[$i] ?>,
                                            "tec": <?php echo $tecW[$i] ?>,
                                            "cambio": <?php echo $cambW[$i] ?>,
                                            "desempenio": <?php echo $tFalW[$i] ?>,
                                            "Meta": <?php echo $targetW[$i] ?>
                                        },
                                    <?php } } ?>
                            ], 
                            "valueAxes": [{
                                "title": "Indicador", 
                                "maximum": 100,
                                "stackType": "regular",
                                "unit": "%",
                                "axisAlpha": 0.5,
                                "gridAlpha": 0.2,
                                "labelsEnabled": true,
                                "position": "left",
                                "balloon": {
                                    "enabled": false
                                },
                            }],
                            "graphs": [ {
                                "fillAlphas": 1,
                                "fillColors": "#03A64A",
                                "lineColor": "#03A64A",
//                                "fillColors": "#62cf73",
//                                "lineColor": "#62cf73",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "id":"mProd",
                                "title": "OEE",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "prod", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, {
                                "fillAlphas": 1,
                                "fillColors": "#0477BF",
                                "lineColor": "#0477BF",
//                                "fillColors": "#311B92",
//                                "lineColor": "#311B92",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Técnicas",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "tec", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, {
                                "fillAlphas": 1,
                                "fillColors": "#F06292",
                                "lineColor": "#F06292C",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Organizacionales",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "org", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "fillAlphas": 1,
                                "fillColors": "#F20505",
                                "lineColor": "#F20505",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Calidad",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "calidad", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "fillAlphas": 1,
                                "fillColors": "#000000",
                                "lineColor": "#000000",
//                                "fillColors": "#3498db",
//                                "lineColor": "#3498db",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Cambios",
                                "type": "column",
                                "color": "#FFFFFF",
                                "valueField": "cambio", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "fillAlphas": 1,
                                "fillColors": "#9E9E9E", 
                                "lineColor": "#9E9E9E", 
                                "labelText": "[[value]]", 
                                "lineAlpha": 1,
                                "title": "Desempeño", 
                                "type": "column", 
                                "color": "#FFFFFF",
                                "valueField": "desempenio",
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "id": "graph2",                                 
                                "lineThickness": 2,                 
                                "lineColor": "#F25C5C",
                                "fillAlphas": 0, 
                                "lineAlpha": 2, 
                                "title": "Meta", 
                                "valueField": "Meta", 
                                "dashLengthField": "dashLengthLine",
                                "balloonText": "<b>[[category]]</b><br>Meta: </b><span style='font-size:100%'><b>[[value]]%</b></span><br>OEE: </b><span style='font-size:100%'><b>[[prod]]%</b></span><br>Técnicas: </b><span style='font-size:100%'><b>[[tec]]%</b></span><br>Organizacional: </b><span style='font-size:100%'><b>[[org]]%</b></span><br>Calidad: </b><span style='font-size:100%'><b>[[calidad]]%</b></span><br>Cambio: </b><span style='font-size:100%'><b>[[cambio]]%</b></span><br>Desempenio: </b><span style='font-size:100%'><b>[[desempenio]]%</b></span>"
                            }],                  
                            "chartCursor": { 
                                "pan": true, 
                                "valueLineEnabled": true, 
                                "cursorAlpha": 0, 
                                "valueLineAlpha": 0.2 
                            }, 
                            "categoryField": "date",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisAlpha": 0,
                                "gridAlpha": 0.2,
                                "position": "left"
                            }
                        });
                    </script>                
                </div> 

                <!-- TABLA DE MESES -->
                <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                    <thead>
                        <tr >
                            <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">Mes</th>
                            <?php for ($i = 1; $i < 13; $i++ ) { ?>
                                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblM[$i] ?></th>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                    <tr >
                        <td style="border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/OEE.png" style="height: 13px;" ></td>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td style=" border: 1px solid #BABABA; text-align: center;"><?php echo $prodM[$i].'%' ?></td>
                        <?php } ?>
                    </tr>
                    <tr >
                        <td style="border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/Tec.png" style="height: 13px;" ></th>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td style=" border: 1px solid #BABABA; text-align: center;"><?php echo $tecM[$i].'%' ?></th>
                        <?php } ?>
                    </tr>
                    <tr >
                        <td style="border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/Org.png" style="height: 13px;" ></th>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td style="border: 1px solid #BABABA; text-align: center;"><?php echo $orgM[$i].'%' ?></th>
                        <?php } ?>
                    </tr>
                    <tr >
                        <td style="border: 1px solid #BABABA; "><img src="../../imagenes/nomenclaturas/cambio.png" style="height: 13px;" ></td>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td style="border: 1px solid #BABABA; text-align: center;"><?php echo $camM[$i].'%' ?></td>
                        <?php } ?>
                    </tr>
                    <tr >
                        <td style="border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/Cali.png" style="height: 13px;" ></td>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td style="border: 1px solid #BABABA; text-align: center;"><?php echo $caliM[$i].'%' ?></td>
                        <?php } ?>
                    </tr>
                    <tr >
                        <td style="border: 1px solid #BABABA; "><img src="../../imagenes/nomenclaturas/desempenio.png" style="height: 13px;" > </td>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td style=" border: 1px solid #BABABA; text-align: center;"><?php echo $tFalM[$i].'%' ?></td>
                        <?php } ?>
                    </tr> 
                    </tbody>
                </table>

                <!-- TABLA DE SEMANA -->
                <table style="width: 35%; margin-left: 63%; margin-top: -115px; border: 1px solid #BABABA; ">
                    <tr>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php }} ?>                            
                    </tr>
                    <tr>
                        <td style="border: 1px solid #BABABA; "><img src="../../imagenes/nomenclaturas/OEE.png" style="height: 13px;" ></td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $prodW[$i].'%'  ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $prodW[$i].'%'  ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $prodW[$i].'%'  ?></td>
                        <?php }} ?>  
                    </tr>   
                    <tr>
                        <td style="border: 1px solid #BABABA; "><img src="../../imagenes/nomenclaturas/Tec.png" style="height: 13px;"> </td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tecW[$i].'%'  ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tecW[$i].'%'  ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tecW[$i].'%'  ?></td>
                        <?php }} ?>  
                    </tr>
                    <tr>
                        <td style="border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/Org.png" style="height: 13px;" ></td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $orgW[$i].'%'  ?></td>
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $orgW[$i].'%'  ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $orgW[$i].'%'  ?></td>
                        <?php }} ?>  
                    </tr>
                    <tr>
                        <td style="border: 1px solid #BABABA; "><img src="../../imagenes/nomenclaturas/cambio.png" style="height: 13px;" > </td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cambW[$i].'%'  ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cambW[$i].'%'  ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cambW[$i].'%'  ?></td>
                        <?php }} ?> 
                    </tr> 
                    <tr> 
                        <td style="border: 1px solid #BABABA; "><img src="../../imagenes/nomenclaturas/Cali.png" style="height: 13px;" ></td> 
                        <?php 
                        if($sP > $sL){ 
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?> 
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $calW[$i].'%' ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $calW[$i].'%' ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $calW[$i].'%' ?></td>
                        <?php }} ?>  
                    </tr>
                    <tr>
                        <td style="border: 1px solid #BABABA; "><img src="../../imagenes/nomenclaturas/desempenio.png" style="height: 14px;" ></td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tFalW[$i].'%' ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tFalW[$i].'%' ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tFalW[$i].'%' ?></td>
                        <?php }} ?>  
                    </tr>
                </table> 
            
                <div id="grafDays" class="jidokaDay" >                
                    <script>
                        var chart = AmCharts.makeChart("grafDays", {
                            "type": "serial", 
                            "theme": "none", 
                            "dataDateFormat": "YYYY-MM-DD", 
                            "precision": 2,
                            "titles": [{
                                "text": "Diaria",
                                "size": 12
                            }],
                            "legend": {
                                "autoMargins": false, 
                                "borderAlpha": 0.2, 
                                "equalWidths": false, 
                                "horizontalGap": 0, 
                                "verticalGap": 5, 
                                "markerSize": 10, 
                                "useGraphSettings": true, 
                                "valueAlign": "left", 
                                "valueWidth": 0 
                            },
                            "dataProvider": [
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                        $date = explode("-", $i); 
                                        $vDate = $date[0].$date[1].(int)$date[2];                 
                                    ?>
                                { 
                                    "date": "<?php echo $i; ?>", 
                                    "prod": <?php echo $prodDay[$vDate]; ?>, 
                                    "calidad": <?php echo $caliDay[$vDate]; ?>, 
                                    "org": <?php echo $orgDay[$vDate]; ?>, 
                                    "tec": <?php echo $tecDay[$vDate]; ?>, 
                                    "cambio": <?php echo $cambioDay[$vDate]; ?>, 
                                    "Meta": <?php echo $targetDay[$vDate]; ?>, 
                                    "desempenio": <?php echo $desDay[$vDate]; ?> 
                                }, 
                                <?php } ?> 
                            ], 
                            "valueAxes": [{
                                "title": "Indicador",  
                                "maximum": 100, 
                                "minimum": 0, 
                                "stackType": "regular",
                                "unit": "%",
                                "axisAlpha": 0.3,
                                "gridAlpha": 0.2
                            }],
                            "graphs": [ {
                                "fillAlphas": 1,
                                "fillColors": "#03A64A",
                                "lineColor": "#03A64A",
//                                "fillColors": "#62cf73",
//                                "lineColor": "#62cf73",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "id":"mProd",
                                "title": "OEE",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "prod", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, {
                                "fillAlphas": 1,
                                "fillColors": "#0477BF",
                                "lineColor": "#0477BF",
//                                "fillColors": "#311B92",
//                                "lineColor": "#311B92",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Técnicas",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "tec", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, {
                                "fillAlphas": 1,
                                "fillColors": "#F06292",
                                "lineColor": "#F06292C",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Organizacionales",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "org", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "fillAlphas": 1,
                                "fillColors": "#F20505",
                                "lineColor": "#F20505",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Calidad",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "calidad", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "fillAlphas": 1,
                                "fillColors": "#000000",
                                "lineColor": "#000000",
//                                "fillColors": "#3498db",
//                                "lineColor": "#3498db",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "Cambios",
                                "type": "column",
                                "color": "#FFFFFF",
                                "valueField": "cambio", 
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "fillAlphas": 1,
                                "fillColors": "#9E9E9E", 
                                "lineColor": "#9E9E9E", 
                                "labelText": "[[value]]", 
                                "lineAlpha": 1,
                                "title": "Desempeño", 
                                "type": "column", 
                                "color": "#FFFFFF",
                                "valueField": "desempenio",
                                "balloon": {
                                    "enabled": false
                                },
                            }, { 
                                "id": "graph2",                                 
                                "lineThickness": 2,                 
                                "lineColor": "#F25C5C",
                                "fillAlphas": 0, 
                                "lineAlpha": 2, 
                                "title": "Meta", 
                                "valueField": "Meta", 
                                "dashLengthField": "dashLengthLine",
                                "balloonText": "<b>[[category]]</b><br>Meta: </b><span style='font-size:100%'><b>[[value]]%</b></span><br>OEE: </b><span style='font-size:100%'><b>[[prod]]%</b></span><br>Técnicas: </b><span style='font-size:100%'><b>[[tec]]%</b></span><br>Organizacional: </b><span style='font-size:100%'><b>[[org]]%</b></span><br>Calidad: </b><span style='font-size:100%'><b>[[calidad]]%</b></span><br>Cambio: </b><span style='font-size:100%'><b>[[cambio]]%</b></span><br>Desempenio: </b><span style='font-size:100%'><b>[[desempenio]]%</b></span>"
                            }], 
                            "chartCursor": { 
                                "pan": true, 
                                "valueLineEnabled": true, 
                                "cursorAlpha": 0, 
                                "valueLineAlpha": 0.2 
                            }, 
                            "chartScrollbar": {
                                "graph": "g1",
                                "oppositeAxis": false,
                                "offset": 30,
                                "scrollbarHeight": 20,
                                "backgroundAlpha": 0,
                                "selectedBackgroundAlpha": 0.1,
                                "selectedBackgroundColor": "#888888",
                                "graphFillAlpha": 0,
                                "graphLineAlpha": 0.5,
                                "selectedGraphFillAlpha": 0,
                                "selectedGraphLineAlpha": 1,
                                "autoGridCount": true,
                                "color": "#AAAAAA"
                            },                            
                            "categoryField": "date",
                            "categoryAxis": {
                                "parseDates": true, 
                                "dashLength": 1, 
                                "minorGridEnabled": true 
                            }
                         });
                    </script>
                </div>
            </div>
            
            <div class="row" style="margin-top: 1.4% " >
                <div id="pnlPikers" class="col-lg-12 col-md-12 col-xs-12 col-sh-12" > 
                    <script>
                        $(function() {
                            $("#dateIni").datepicker({ 
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                maxDate: new Date(), 
                                //OYENTES DE LOS PICKERS 
                                onSelect: function(date) { 
                                    //Cuando se seleccione una fecha se cierra el panel
                                    $("#ui-datepicker-div").hide(); 
                                } 
                            }); 

                            $("#dateEnd").datepicker({
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                maxDate: new Date(),
                                //OYENTES DE LOS PICKERS 
                                onSelect: function ( date ) {
                                    //cuando se seleccione una fecha se cierra el panel
                                    $("#ui-datepicker-div").hide(); 
                                } 
                            }); 
                        }); 
                    </script> 

                    <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado container container-fluid" >
                        <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3 contenidoCentrado input-group" style="margin-left: 30px; float: left;" > 
                            <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo date("m/d/Y", strtotime($_SESSION['FIni'])); ?>" >
                            <span class="input-group-addon" > a </span> 
                            <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($_SESSION['FFin'])); ?>" >
                        </div>
                        <div class="col-xs-1 col-sh-1 col-md-1 col-lg-1 contenidoCentrado" style=" float: left;" > 
                            <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0;" onclick="setTipoDatos()" > 
                                <img src="../../imagenes/confirmar.png" > 
                            </button> 
                        </div> 
                        <div class="col-xs-7 col-sh-7 col-md-7 col-lg-7 contenidoCentrado" style=" float: left;" > 
<!--                            <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0; margin-left: 90% " onclick="setTipoDatos2()" > 
                                <img src="../../imagenes/bAll.png" > 
                            </button> -->
                        </div> 
                    </div> 
                </div>            
            </div>
            
            <div class="row" style="margin-top: 15px;" > 
                <div id="holder-semple-1" style="margin-left: 2%; width: 95%" > 
                    <script id="tamplate-semple-1" type="text/mustache" > 
                        <table width="98%" class="inner-table"> 
                            <thead> 
                                <tr> 
                                    <td colspan="2"> </td> 
                                    <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+1; ?>" > PERIODO </td>
                                </tr> 
                                <tr> 
                                    <td>&nbsp;</td> 
                                    <td align="center" >TOPIC</td> 
                                    <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ ?>
                                        <td align="center" ><?php echo $cadDate[$i]; ?></td> 
                                    <?php } ?> 
                                </tr> 
                            </thead> 
                            <tbody> 
                                <?php for($i = 1; $i <= 6; $i++) { ?>
                                <tr > 
                                    <td align="right" ><?php echo $i ?></td>  
                                    <td ><?php echo $problema[$i]?></td> 
                                    <?php for($j = $_SESSION['FIni']; $j <= $_SESSION['FFin']; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                                        $date = explode("-", $j); 
                                        $vDate = $date[0].$date[1].(int)$date[2]; 
                                    ?> 
                                    <td align="center" ><?php echo @round($pTopic[$i][$vDate],2).'%'; ?></td> 
                                <?php }} ?> 
                            </tbody> 
                        </table> 
                    </script>
                </div>
            </div> 
            <br>
        </div> 
    </body>