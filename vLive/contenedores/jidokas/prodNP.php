<HTML>
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script>  
    <script>        
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
        
        function setTipoDatos() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes_1.php", 
                type: "post", 
                data: { tipoVista: 1 , fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 
        
        function setTipoDatos2() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla 
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes_1.php", 
                type: "post", 
                data: { tipoVista: 2, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas 
                    location.reload(); 
                } 
            }); 
        } 
        
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
    
    <?php 
        include '../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
        $line = $_SESSION['linea'];
        $anio = $_SESSION['anio'];
        $mes = $_SESSION['mes'];         
        
        $date->setISODate("$anio", 53);
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //INICIALIZAMOS VARIABLES PARA MES
        for($i = 1; $i <= 12; $i++){
            $mCant[$i] = 0;
            $targetMonth[$i] = 0;
        }  
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
                $targetWeek[$i] = 0;
            }
            
            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
                $targetWeek[$i] = 0;
            }            
        } else {
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
                $targetWeek[$i] = 0;
            }            
        }   
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        if ($sP == 0)
            $dSI = 7;          
        
        if ($sL == 0) 
            $dSL = 7;        
        
        $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio));
        $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));           
        
        //OBTENEMOS LA ULTIMA SEMANA DEL MES        
        $diaSemanaU = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        $m[1] = (string) "Jun";
        $m[2] = (string) "Feb";
        $m[3] = (string) "Mar";
        $m[4] = (string) "Apr";
        $m[5] = (string) "May";
        $m[6] = (string) "Jun";
        $m[7] = (string) "Jul";
        $m[8] = (string) "Aug";
        $m[9] = (string) "Sep";
        $m[10] = (string) "Oct";
        $m[11] = (string) "Nov";
        $m[12] = (string) "Dec";
        
        //LISTA DE COLORES
        $color[0] = "#FFA7A6";            
        $color[1] = "#425944";
        $color[2] = "#97A676";
        $color[3] = "#D0DED4";
        $color[4] = "#03A6A6";
        $color[5] = "#026873";          
        $color[6] = "#D96E48";
        $color[7] = "#FFD747";
        $color[8] = "#8CEEEE";
        $color[9] = "#FC6170"; 
        $color[10] = "#D97014";
        $color[11] = "#8C4C27";
        $color[12] = "#F2CCB6";
        $color[13] = "#BF7960"; 
        $color[14] = "#2A3659";
        $color[15] = "#FFF9D9";
        $color[16] = "#65A688";  
        $color[17] = "#03A678";
        $color[18] = "#EE82EE";
        $color[26] = "#F2E3D5";
        $color[19] = "#CFC8C4";
        $color[20] = "#E8DCD7";            
        $color[21] = "#ff5722";
        $color[22] = "#3f51b5";
        $color[23] = "#795548";
        $color[24] = "#8bc34a";
        $color[25] = "#1e88e5";
            
        //INCIALIZAMOS LAS VARIABLES
        //TABLA
        $countProblem = 0; 
        $problema[0] = ""; 
        for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            
            $fecha[$vDate] = date("M, d",strtotime($i)); 
            $valueDayT[$vDate] = 0; 
            $targetDayT[$vDate] = 0; 
            
            $prodDay[$vDate] = @round($oeeProductionDay[$i][1],2); 
            $caliDay[$vDate] = @round($oeeProductionDay[$i][4],2); 
            $orgDay[$vDate] = @round($oeeProductionDay[$i][3],2); 
            $tecDay[$vDate] = @round($oeeProductionDay[$i][2],2); 
            $cambioDay[$vDate] = @round($oeeProductionDay[$i][5],2); 
            $desDay[$vDate] = @round($oeeProductionDay[$i][6],2); 
        } 
        
        //TIPO DE CALCULO QUE SE VA REALIZAR DE ACUERDO A LA OPCION DEL COMBO 
        $tipo = $_SESSION['tipoDato']; 
        
        switch ($tipo) { 
            case 1: //DURACION
            case 3: 
                $jTDay = jidokaEntregasDia($line, $anio); 
                //$jTWeek = jidokaSemana($tema, $line, $fP, $fL);     
                $jTWeek = semanaEntrega($line, $fP, $fL); 
                $jTMonth = mesEntrega($line, $anio); 
                break; 
            case 2: //FRECUENCIA 
                $jTDay = jidokaDiaF($tema, $line, $anio); 
                //$jTWeek = jidokaSemanaF($tema, $line, $fP, $fL);
                $jTWeek = semanaEntrega($line, $fP, $fL); 
                $jTMonth = mesEntrega($line, $anio); 
                break; 
        } 
        
        /******************* DIA *********************/
        for ($i = 0; $i < count($jTDay); $i++ ) { 
            $date = explode("-", $jTDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $valueDayT[$vDate] = $jTDay[$i][1]; 
            //echo '<br>',$jTDay[$i][0],', ',$jTDay[$i][1];
        } 
        
        #TARGET
        $cTargetDay = cTargetPzasDaily($line, $_SESSION['FIni'], $_SESSION['FFin']); 
        for ($i = 0; $i < count($cTargetDay); $i++){ 
            $date = explode("-", $cTargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $targetDayT[$vDate] = (int) $cTargetDay[$i][1]; 
        } 
        
        /****************** SEMANA *****************/
        for($i = 0; $i < count($jTWeek); $i++ ) { 
            $sem = $jTWeek[$i][0]; 
            $cwTec[$sem] = $jTWeek[$i][1]; 
        } 
        
        #TARGET
        $cTargetWeek = cTargetPzasWeekly($line, $anio, $mes); 
        for ($i = 0; $i < count($cTargetWeek); $i++){ 
            $s = $cTargetWeek[$i][0]; 
            $targetWeek[$s] = $cTargetWeek[$i][1]; 
        } 
        
        /******************* MES ********************/
        for($i = 0 ; $i < count($jTMonth); $i++) { 
            $nMes = $jTMonth[$i][0]; 
            $mCant[$nMes] = $jTMonth[$i][1]; 
        } 
        
        #TARGET 
        $cTargetMonth = cTargetPzasMonthy($line, $anio); 
        for ($i = 0; $i < count($cTargetMonth); $i++ ){ 
            $m = $cTargetMonth[$i][0]; 
            $targetMonth[$m] = $cTargetMonth[$i][1]; 
        } 
        
        //DIA 
        for($i = 0; $i < 100; $i++){ 
            $noParte[$i] = (string) ''; 
        } 
        
        $cNP = 0; 
        $datNPAnio = pzasProdNoParteAnio($line, $anio); 
        for ($i = 0; $i < count($datNPAnio); $i++){ 
            if ($datNPAnio[$i][0] != $line ){ 
                $noParte[$cNP] = (string)$datNPAnio[$i][0]; 
                $totalNP[$cNP] = 0; 
                $totalNPT[$cNP] = 0; 
                $cNP++; 
            } 
        } 
        
        for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            for ($j = 0; $j < $cNP; $j++){
                //$cantidadProdNoParteDia [$vDate][$j] = 0;
                $pzasProdNoParteDia[$vDate][$j] = 0; 
                $pzasProdNoParteDiaT[$vDate][$j] = 0; 
                $totalNP[$j] = 0; 
                $totalNPDia[$vDate] = 0; 
                $totalNPDiaT[$vDate] = 0; 
            } 
        } 
        
        $total = 0; 
        $totalT = 0; 
        $datNPAnioDia = pzasProdNoParteAnioDia($line, $_SESSION['FIni'], $_SESSION['FFin']); 
        for ($i = 0; $i < count($datNPAnioDia); $i++){ 
            $date = explode("-", $datNPAnioDia[$i][0],3);
            $vDate = $date[0].$date[1].(int)$date[2];
            for ($j = 0; $j < $cNP; $j++) { 
                if ($noParte[$j] == $datNPAnioDia[$i][1]) { 
                    $totalNP[$j] += $datNPAnioDia[$i][2];
                    $totalNPDia[$vDate] += $datNPAnioDia[$i][2];
                    $total += $totalNPDia[$vDate];
                    $pzasProdNoParteDia[$vDate][$j] = $datNPAnioDia[$i][2]; 
                } 
            }
        }        
        
        $datNPAnioDiaT = pzasProdNoPartePeriod($line, $_SESSION['FIni'], $_SESSION['FFin']);   
        for ($i = 0; $i < count($datNPAnioDiaT); $i++){ 
            $date = explode("-", $datNPAnioDiaT[$i][0],3);
            $vDate = $date[0].$date[1].(int)$date[2]; 
            for ($j = 0; $j < $cNP; $j++) { 
                if ($noParte[$j] == $datNPAnioDiaT[$i][1]) { 
                    $totalNPT[$j] += $datNPAnioDiaT[$i][2]; 
                    $totalNPDiaT[$vDate] += $datNPAnioDiaT[$i][2]; 
                    $totalT += $datNPAnioDiaT[$i][2]; 
                    $pzasProdNoParteDiaT[$vDate][$j] = $datNPAnioDiaT[$i][2]; 
                } 
            } 
        } 
        
        $dias = (strtotime($_SESSION['FIni'])- strtotime($_SESSION['FFin']))/86400;
        $dias = abs($dias); 
        $dias = floor($dias); 
        //echo '<br>',$dias;
        
        if ($dias > 11 ) { 
            $rowspan = 12;
        } else { 
            //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
            //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS
            $rowspan = $dias+1;
        } 

        //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
        if($countProblem == 0) { 
            $countProblem = 1;
            $problema[1] = "";
            $totalProblema[1] = 0;
        } 
        
    ?>    
    <body>
        <div > 
            <div class=" row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" style="margin-top: 1.5%" > 
                <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                    <script>
                        var chart = AmCharts.makeChart("jTMonth", {
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": [
                                <?php for ($i = 1; $i < 13; $i++ ) { ?>
                                {
                                    "country": "<?php echo $m[$i] ?>",
                                    "visits": <?php echo $mCant[$i] ?>,
                                    "meta": <?php echo $targetMonth[$i]; ?>
                                },
                                <?php } ?>
                            ],
                            "graphs": [{
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "lineColor": "#02538b",
                                "type": "column",
                                "valueField": "visits"
                            }, { 
                                "valueAxis": "v2",
                                "bullet": "round",
                                "bulletBorderAlpha": 1,
                                "bulletColor": "#FFFFFF",
                                "bulletSize": 5,
                                "hideBulletsCount": 50,
                                "lineThickness": 2,
                                "lineColor": "#62cf73",
                                "type": "line",
                                "title": "Meta",
                                "useLineColorForBulletBorder": true,
                                "valueField": "meta",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],
                            "categoryField": "country",
                            "valueAxes": [{
                                "title": "Indicador"
                            }]
                        });
                    </script>
                </div> 

                <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                    <script>
                        var chart = AmCharts.makeChart("jTWeek", {
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": [
                                <?php 
                                if($sP > $sL){
                                    for ($i = $sP; $i <= $numSemanas; $i++) {                                
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i]; ?>
                                    },
                                <?php } 
                                    for($i = 1; $i <= $sL; $i++) { 
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i]; ?>
                                    },
                                <?php } ?>                            
                                <?php } else { 
                                    for ($i = $sP; $i <= $sL; $i++) {                                
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i]; ?>
                                    },
                                <?php } } ?>
                            ],
                            "graphs": [{
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "lineColor": "#02538b",
                                "type": "column",
                                "valueField": "visits"
                            }, { 
                                "valueAxis": "v2",
                                "bullet": "round",
                                "bulletBorderAlpha": 1,
                                "bulletColor": "#FFFFFF",
                                "bulletSize": 5,
                                "hideBulletsCount": 50,
                                "lineThickness": 2,
                                "lineColor": "#62cf73",
                                "type": "line",
                                "title": "Meta",
                                "useLineColorForBulletBorder": true,
                                "valueField": "meta",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],
                            "categoryField": "country",
                            "valueAxes": [{
                                "title": "Inidicador"
                            }]
                        });
                    </script>                
                </div> 

                <!-- TABLA DE MESES -->
                <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                    <tr >
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">Mes</th>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $m[$i] ?></th>
                        <?php } ?>
                    </tr>
                    <tr>
                        <?php switch ($tipo) { case 1: ?>
                            <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
                        <?php break;  case 2 : ?>
                            <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">No. Fallas</td>
                        <?php break;  case 3 : ?>
                            <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Piezas</td>
                        <?php break;  } ?> 
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $mCant[$i] ?></td>      
                        <?php } ?>
                    </tr>                
                </table> 

                <!-- TABLA DE SEMANA -->
                <table style="width: 35%; margin-left: 63%; margin-top: -40px; border: 1px solid #BABABA; ">
                    <tr>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php }} ?>                            
                    </tr>
                    <tr>
                        <?php switch ($tipo) { case 1: ?>
                            <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
                        <?php break;  case 2 : ?>
                            <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">No. Fallas</td>
                        <?php break;  case 3 : ?>
                            <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Piezas</td>
                        <?php break;  } ?> 
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                        <?php }} ?>  
                    </tr>                
                </table> 
                <br> 
                <div id="grafDays" class="jidokaDay" > 
                    <script> 
                        var chart = AmCharts.makeChart("grafDays", {
                            "type": "serial",
                            "theme": "none",
                            "dataDateFormat": "YYYY-MM-DD",
                            "precision": 0,
                            "legend": {
                                "horizontalGap": 10,
                                "maxColumns": 1,
                                "position": "right",
                                "useGraphSettings": true,
                                "markerSize": 10,
                            },
                            "dataProvider": [
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                        $date = explode("-", $i); 
                                        $vDate = $date[0].$date[1].(int)$date[2];
                                    ?>
                                { 
                                    "date": "<?php echo $fecha[$vDate] ?>", 
                                    <?php for ($j = 0; $j < $cNP; $j++){ ?> 
                                        "<?php echo $noParte[$j] ?>": <?php echo $pzasProdNoParteDia[$vDate][$j] ?>,
                                    <?php } ?>
                                    "meta": <?php echo $targetDayT[$vDate]; ?> 
                                },
                                <?php } ?> 
                            ],
                            "valueAxes": [{
                                "stackType": "regular",
                                "axisAlpha": 0.3,
                                "gridAlpha": 0.1
                            }],
                            "graphs": [ 
                                <?php for ($j = 0; $j < $cNP; $j++){ ?>
                                {
                                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                "fillAlphas": 1, 
                                "fillColors": "<?php echo $color[$j] ?>", 
                                "lineColor": "<?php echo $color[$j] ?>", 
                                "labelText": "[[value]]", 
                                "lineAlpha": 1, 
                                "title": "<?php echo $noParte[$j] ?>", 
                                "type": "column", 
                                "color": "#FFFF", 
                                "valueField": "<?php echo $noParte[$j] ?>" 
                            },
                            <?php } ?>
                            { 
                                "valueAxis": "v2",
                                "bullet": "round",
                                "bulletBorderAlpha": 1,
                                "bulletColor": "#FFFFFF",
                                "bulletSize": 5,
                                "hideBulletsCount": 50,
                                "lineThickness": 2,
                                "lineColor": "#62cf73",
                                "type": "line",
                                "title": "Meta",
                                "useLineColorForBulletBorder": true,
                                "valueField": "meta",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],
                            "chartScrollbar": { 
                                "graph": "g1",
                                "oppositeAxis": false,
                                "offset": 30,
                                "scrollbarHeight": 20,
                                "backgroundAlpha": 0,
                                "selectedBackgroundAlpha": 0.1,
                                "selectedBackgroundColor": "#888888",
                                "graphFillAlpha": 0,
                                "graphLineAlpha": 0.5,
                                "selectedGraphFillAlpha": 0,
                                "selectedGraphLineAlpha": 1,
                                "autoGridCount": true,
                                "color": "#AAAAAA"
                            },
                            "categoryField": "date",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisAlpha": 0.1,
                                "gridAlpha": 0.1,
                                "position": "left"
                            }
                         });
                    </script> 
                </div> 
            </div> 
        </div> 
        
        <!-- APARTADO PARA PICKERS --> 
        <div class="row" style="margin-top: 1.2%" >
            <div id="pnlPikers" class="col-lg-12 col-md-12 col-xs-12 col-sh-12" > 
                <script>
                $(function() {
                    $("#dateIni").datepicker({ 
                        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                        maxDate: new Date(), 
                        //OYENTES DE LOS PICKERS 
                        onSelect: function(date) { 
                            //Cuando se seleccione una fecha se cierra el panel
                            $("#ui-datepicker-div").hide(); 
                        } 
                    }); 

                    $("#dateEnd").datepicker({
                        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                        maxDate: new Date(),
                        //OYENTES DE LOS PICKERS 
                        onSelect: function ( date ) {
                            //cuando se seleccione una fecha se cierra el panel
                            $("#ui-datepicker-div").hide(); 
                        } 
                    }); 
                }); 
            </script> 
                
                <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado container container-fluid" >                    
                    <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3 contenidoCentrado input-group" style="margin-left: 30px; float: left;" > 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo date("m/d/Y", strtotime($_SESSION['FIni'])); ?>">
                        <span class="input-group-addon"> a </span> 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($_SESSION['FFin'])); ?>">
                    </div>
                    <div class="col-xs-1 col-sh-1 col-md-1 col-lg-1 contenidoCentrado" style="float: left;" > 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0;" onclick="setTipoDatos()"> 
                            <img src="../../imagenes/confirmar.png"> 
                        </button> 
                    </div> 
                    <div class="col-xs-7 col-sh-7 col-md-7 col-lg-7" style="float: left;" > 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0; margin-left: 90%;  " onclick="setTipoDatos2()"> 
                            <img src="../../imagenes/bAll.png"> 
                        </button> 
                    </div> 
                </div> 
            </div> 
        </div> 
        
        <div class="row" style="margin-top: 65px;" > 
            <div id="holder-semple-1" style="margin-left:3% ; width: 100%;" > 
                <script id="tamplate-semple-1" type="text/mustache"> 
                    <table class="inner-table" style="width: 90%; margin-top: -3%" > 
                        <thead> 
                            <tr> 
                                <td colspan="2"> </td> 
                                <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                                <td rowspan="2" align="center" >Total</td> 
                            </tr> 
                            <tr> 
                                <td>&nbsp;</td> 
                                <td align="center" >No Parte</td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                    $date = explode("-", $i); 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                                ?>
                                    <td align="center" ><?php echo $fecha[$vDate]; ?></td>
                                <?php } ?> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php for ($j = 0; $j < $cNP; $j++){ ?>
                            <tr >
                                <td align="right" ><?php echo $j ?></td>
                                <td ><?php echo $noParte[$j] ?></td>  

                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                    $date = explode("-", $i); 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                                ?>
                                    <td align="center" ><?php echo $pzasProdNoParteDiaT[$vDate][$j]; ?></td>
                                <?php } ?>
                                    <td align="center" ><?php echo $totalNPT[$j]; ?> </td>
                            </tr >
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">Sold Total</td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                        $date = explode("-", $i); 
                                        $vDate = $date[0].$date[1].(int)$date[2];
                                ?>
                                    <td align="center" ><?php echo $totalNPDiaT[$vDate]; ?></td>                        
                                <?php } ?>  
                                <td align="center" ><?php echo $totalT; ?></td>  
                            </tr>
                        </tfoot>
                    </table>
                </script> 
            </div> 
            <br> 
        </div> 
    </body>
            
            
 