<HTML>
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>     
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script> 
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script> 
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script>  
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({ 
                fixedColumnsLeft: 1, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 11, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 

        function getFixedColumnsData() {} 
        
        function setTipoDatos() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                type: "POST", 
                url: "../../db/sesionReportes.php", 
                data: { tipoVista: 1, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 
        
        function setTipoDatos2() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                type: "POST",
                url: "../../db/sesionReportes.php", 
                data: { tipoVista: 2 , fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 
        
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
    
    <?php 
        include '../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes']; 
     
        $date->setISODate("$anio", 53);
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //INICIALIZAMOS VARIABLES PARA MES
        for($i = 1; $i <= 12; $i++){
            $mCant[$i] = 0;
            $targetMonth[$i] = 0;
        }  
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
                $targetWeek[$i] = 0;
            }
            
            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
                $targetWeek[$i] = 0;
            }            
        } else {
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
                $targetWeek[$i] = 0;
            }            
        } 
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        if ($sP == 0)
            $dSI = 7;          
        
        if ($sL == 0) 
            $dSL = 7;        
        
        $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio));
        $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));   
        
        //OBTENEMOS LA ULTIMA SEMANA DEL MES 
        $diaSemanaU = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        $lblM[1] = (string) "Jun";
        $lblM[2] = (string) "Feb";
        $lblM[3] = (string) "Mar";
        $lblM[4] = (string) "Apr";
        $lblM[5] = (string) "May";
        $lblM[6] = (string) "Jun";
        $lblM[7] = (string) "Jul";
        $lblM[8] = (string) "Aug";
        $lblM[9] = (string) "Sep";
        $lblM[10] = (string) "Oct";
        $lblM[11] = (string) "Nov";
        $lblM[12] = (string) "Dec";
        
        //INCIALIZAMOS LAS VARIABLES
        //TABLA
        $countProblem = 0;
        $problema[0] = "";
        
        for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            $fecha[$vDate] = $i;
            $dateT[$vDate] = date("M, d",strtotime($i));
            $valueDayT[$vDate] = 0;
            $valuePlanDayT[$vDate] = 0;
            $cumRealDay[$vDate] = 0;
            $cumMetaDay[$vDate] = 0;
            $totalReales = 0;
            $totalMeta = 0;
            $totalCumReales = 0;
            $totalCumMeta = 0;
        }   
        
        //TIPO DE CALCULO QUE SE VA REALIZAR DE ACUERDO A LA OPCION DEL COMBO 
        if (isset($_POST["tipoDato"])) {
            $tipo = $_POST["tipoDato"];
        } else { 
            $tipo = 1;
        }
        
        $jTDay = jidokaEntregasDia($_SESSION['linea'], $anio); 
        $jTWeek = semanaEntrega($_SESSION['linea'], $fP, $fL); 
        $jTMonth = mesEntrega($_SESSION['linea'], $anio);  
        
        $aux = 1;
        $auxCumReal = 0;
        $auxCumMeta = 0;
        
        /*********************** DIA **************************/
        for ($i = 0; $i < count($jTDay); $i++ ) { 
            $date = explode("-", $jTDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $valueDayT[$vDate] = $jTDay[$i][1]; 
        }          
        
        #TARGET
        $cTargetDay = cTargetPzasDaily($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']);
        for ($i = 0; $i < count($cTargetDay); $i++){
            $date = explode("-", $cTargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $valuePlanDayT[$vDate] = (int) $cTargetDay[$i][1]; 
        }         
        
        for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $d = (int)$date[2]; 
            
            #APARTADO PARA ACUMULADO REAL
            if ($d != $aux ){ #EVALUA SI EL DIA ES DISTINTO DE 1 ENTONCES SE HACE EL ACUMULADO
                #SE HACE EL ACUMULADO DE PIEZAS 
                $cumRealDay[$vDate] = $auxCumReal + $valueDayT[$vDate]; 
                $cumMetaDay[$vDate] = $auxCumMeta + $valuePlanDayT[$vDate]; 
                
                $auxCumReal += $valueDayT[$vDate]; 
                $auxCumMeta += $valuePlanDayT[$vDate];                
            } else {                 
                #SE REINICIA EL CONTEO, YA QUE ES POR MES
                $cumRealDay[$vDate] = $valueDayT[$vDate]; 
                $cumMetaDay[$vDate] = $valuePlanDayT[$vDate]; 
                
                $auxCumReal = $valueDayT[$vDate]; 
                $auxCumMeta = $valuePlanDayT[$vDate]; 
                //$aux++; 
            } 
            $totalReales = $cumRealDay[$vDate];
            $totalMeta = $cumMetaDay[$vDate];
            //echo '<br>', $fecha[$vDate],', (',$date[0],', ', $date[1],', ',$date[2],', ->',$d,' - ',$aux,' )',$valueDayT[$vDate],' -> ',' auxCum ',$auxCumReal,', cumReal ' ,$cumRealDay[$vDate];
        } 
        
        /************************ SEMANA *******************************/
        for($i = 0; $i < count($jTWeek); $i++ ) { 
            $sem = $jTWeek[$i][0]; 
            $cwTec[$sem] = $jTWeek[$i][1]; 
        } 
        
        #TARGET 
        $cTargetWeek = cTargetPzasWeekly($_SESSION['linea'], $fP, $fL); 
        for ($i = 0; $i < count($cTargetWeek); $i++){ 
            $s = $cTargetWeek[$i][0]; 
            $targetWeek[$s] = $cTargetWeek[$i][1]; 
            $difW[$s] = $targetWeek[$s]-$cwTec[$s];
        } 
        
        /************************* MES *************************/
        for($i = 0 ; $i < count($jTMonth); $i++) { 
            $nMes = $jTMonth[$i][0]; 
            $mCant[$nMes] = $jTMonth[$i][1]; 
        } 
        
        #TARGET
        $cTargetMonth = cTargetPzasMonthy($_SESSION['linea'], $anio); 
        for ($i = 0; $i < count($cTargetMonth); $i++ ){
            $m = $cTargetMonth[$i][0];
            $targetMonth[$m] = $cTargetMonth[$i][1]; 
        } 
        
        $dias = (strtotime($_SESSION['FIni'])- strtotime($_SESSION['FFin']))/86400; 
              
    ?> 
    
    <body>
        <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" style="margin-top: 1.5%" > 
            <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                <script>
                    var chart = AmCharts.makeChart("jTMonth", { 
                        "type": "serial", 
                        "theme": "light", 
                        "dataProvider": [ 
                            <?php for ($i = 1; $i < 13; $i++ ) { ?> 
                            { 
                                "country": "<?php echo $m[$i]; ?>", 
                                "visits": <?php echo $mCant[$i]; ?>,
                                "meta": <?php echo $targetMonth[$i]; ?>
                            }, 
                            <?php } ?> 
                        ], 
                        "graphs": [{ 
                            "fillAlphas": 0.9, 
                            "lineAlpha": 0.2, 
                            "lineColor": "#02538b", 
                            "type": "column", 
                            "valueField": "visits" 
                        }, { 
                            "valueAxis": "v2",                            
                            "lineThickness": 2,
                            "lineColor": "#62cf73",
                            "type": "line",
                            "title": "Meta",
                            "valueField": "meta",
                            "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                        }], 
                        "categoryField": "country", 
                        "valueAxes": [{ 
                            "title": "Indicador" 
                        }]
                    });
                </script>
            </div> 
            
            <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                <script>
                    var chart = AmCharts.makeChart("jTWeek", {
                        "type": "serial", 
                        "theme": "light", 
                        "dataProvider": [ 
                            <?php 
                            if($sP > $sL){ 
                                for ($i = $sP; $i <= $numSemanas; $i++) { 
                            ?>
                                {
                                    "cweek": "<?php echo 'CW '.$i ?>",
                                    "realw": <?php echo $cwTec[$i] ?>,
                                    "metaw": <?php echo $targetWeek[$i] ?>,
                                    "difW": <?php echo $difW[$i] ?>
                                },
                            <?php } 
                                for($i = 1; $i <= $sL; $i++) { 
                            ?>
                                { 
                                    "cweek": "<?php echo 'CW '.$i ?>", 
                                    "realw": <?php echo $cwTec[$i] ?>, 
                                    "metaw": <?php echo $targetWeek[$i] ?>,
                                    "difW": <?php echo $difW[$i] ?>
                                }, 
                            <?php } ?>                            
                            <?php } else { 
                                for ($i = $sP; $i <= $sL; $i++) { 
                            ?>
                                { 
                                    "cweek": "<?php echo 'CW '.$i ?>",
                                    "realw": <?php echo $cwTec[$i] ?>,
                                    "metaw": <?php echo $targetWeek[$i] ?>,
                                    "difW": <?php echo $difW[$i] ?>
                                },
                            <?php } } ?>
                        ],
                        "graphs": [{ 
                            "fillAlphas": 0.9, 
                            "lineAlpha": 0.2, 
                            "lineColor": "#02538b", 
                            "type": "column", 
                            "valueField": "realw",
                            "balloonText": "<b style='font-size: 100%'>[[category]]</b><br>Planeadas: <b style='font-size: 100%'>[[metaw]]</b> <br>Reales: <b style='font-size: 100%'>[[realw]]</b> <br>Diferencia: <b style='font-size: 100%'>[[difW]]</b> "
                        }, { 
                            "valueAxis": "v2", 
                            "lineThickness": 2, 
                            "lineColor": "#62cf73", 
                            "type": "line", 
                            "valueField": "metaw", 
                            "balloon": {
                                "enabled": false
                            } 
                        }, { 
                            "valueAxis": "v2", 
                            "lineColor": "#02538b", 
                            "fillColors": "#02538b", 
                            "fillAlphas": 1, 
                            "type": "none", 
                            "visible": false, 
                            "valueField": "difW", 
                            "clustered": false, 
                            "columnWidth": 0.3, 
                            "enabled": false,
                            "balloon": {
                                "enabled": false
                            } 
                        }],
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha": 0,
                            "valueLineAlpha": 0.2
                        }, 
                        "categoryField": "cweek",
                        "valueAxes": [{
                            "title": "Inidicador"
                        }]
                    });
                </script>                
            </div> 
            
            <!-- TABLA DE MESES --> 
            <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                <tr >
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">Mes</th>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblM[$i] ?></th>
                    <?php } ?>
                </tr>
                <tr>
                    <td style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/p_reales.png" style="height: 20px;" ></td>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $mCant[$i] ?></td>      
                    <?php } ?>
                </tr>                
                <tr>
                    <td style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/meta.png" style="height: 11px;" ></td>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $targetMonth[$i] ?></td>      
                    <?php } ?>
                </tr>                
            </table> 
            
            <!-- TABLA DE SEMANA --> 
            <table style="width: 35%; margin-left: 63%; margin-top: -56px; border: 1px solid #BABABA; ">
                <tr>
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                    <?php }} ?>                            
                </tr>
                <tr>
                    <td style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/p_reales.png" style="height: 20px;" ></td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                    <?php }} ?>  
                </tr> 
                <tr>
                    <td style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/meta.png" style="height: 11px;" > </td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $targetWeek[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $targetWeek[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $targetWeek[$i] ?></td>
                    <?php }} ?>  
                </tr> 
            </table>  
                        
            <div id="grafEntregas" class="jidokaDay" >
                <script>
                    var chart = AmCharts.makeChart("grafEntregas", {
                        "type": "serial",
                        "theme": "light",
                        "dataDateFormat": "YYYY-MM-DD",
                        "precision": 0,
                        "valueAxes": [{
                            "id": "v1", 
                            "title": "Reales", 
                            "position": "left", 
                            "autoGridCount": false
                        }, {
                            "id": "v2", 
                            "title": "Meta", 
                            "gridAlpha": 0.1,
                            "position": "right", 
                            "autoGridCount": false,
                            "minimum": 0
                        }],
                        "graphs": [{ 
                            "valueAxis": "v1",
                            "lineColor": "#e1ede9",
                            "fillColors": "#e1ede9",
                            "fillAlphas": 1,
                            "type": "column",
                            "title": "Planeadas",
                            "valueField": "dPlan",
                            "clustered": false,
                            "columnWidth": 0.5,
                            "balloon": { 
                                "enabled": false 
                            } 
                        }, { 
                            "valueAxis": "v1", 
                            "lineColor": "#02538b", 
                            "fillColors": "#02538b", 
                            "fillAlphas": 1, 
                            "type": "column", 
                            "title": "Reales", 
                            "valueField": "dReales", 
                            "clustered": false, 
                            "columnWidth": 0.3, 
                            "balloonText": "<b style='font-size: 100%'>[[category]]</b><br>Planeadas: <b style='font-size: 100%'>[[dPlan]]</b> <br>Reales: <b style='font-size: 100%'>[[dReales]]</b> <br>Diferencia: <b style='font-size: 100%'>[[difD]]</b><br><br>Cumm Planeado: <b style='font-size: 100%'>[[cumPlan]]</b><br>Cumm Real: <b style='font-size: 100%'>[[cumReal]]</b> <br>Diferencia Cumm: <b style='font-size: 100%'>[[difCumm]]</b>"
                        }, { 
                            "valueAxis": "v2", 
                            "lineColor": "#02538b", 
                            "fillColors": "#02538b", 
                            "fillAlphas": 1, 
                            "type": "none", 
                            "visible": false, 
                            "valueField": "difD", 
                            "clustered": false, 
                            "columnWidth": 0.3, 
                            "enabled": false,
                            "balloon": {
                                "enabled": false
                            } 
                        }, { 
                            "valueAxis": "v2",
                            "lineColor": "#02538b", 
                            "fillColors": "#02538b", 
                            "fillAlphas": 1, 
                            "type": "none", 
                            "valueField": "difCumm", 
                            "clustered": false, 
                            "columnWidth": 0.3, 
                            "enabled": false, 
                            "balloon": {
                                "enabled": false
                            } 
                        }, { 
                            "valueAxis": "v2", 
                            "lineThickness": 2, 
                            "lineColor": "#bc2727", 
                            "type": "smoothedLine", 
                            "dashLength": 5,
                            "title": "Cumm Plan", 
                            "useLineColorForBulletBorder": true, 
                            "valueField": "cumPlan", 
                            "balloon": { 
                                "enabled": false 
                            } 
                        },{ 
                            "valueAxis": "v2", 
                            "lineThickness": 2, 
                            "lineColor": "#000000", 
                            "type": "smoothedLine",
                            "dashLength": 5,
                            "title": "Cumm Real",
                            "useLineColorForBulletBorder": true,
                            "valueField": "cumReal",
                            "balloon": {
                                "enabled": false
                            } 
                        }],
                        "chartScrollbar": {
                            "graph": "g2", 
                            "oppositeAxis": false,
                            "offset": 30,
                            "scrollbarHeight": 20,
                            "backgroundAlpha": 0,
                            "selectedBackgroundAlpha": 0.1,
                            "selectedBackgroundColor": "#888888",
                            "graphFillAlpha": 0,
                            "graphLineAlpha": 0.5,
                            "selectedGraphFillAlpha": 0,
                            "selectedGraphLineAlpha": 1,
                            "autoGridCount": true,
                            "color": "#AAAAAA"
                        },
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha": 0,
                            "valueLineAlpha": 0.2
                        }, 
                        "categoryField": "date",
                        "categoryAxis": {
                            "parseDates": true,
                            "dashLength": 1,
                            "gridAlpha": 0.2,
                            "minorGridEnabled": true
                        },                        
                        "balloon": {
                            "borderThickness": 1,
                            "shadowAlpha": 0
                        },
                        "dataProvider": [
                          <?php  
                              for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                  $date = explode("-", $i);                                 
                                  $vDate = $date[0].$date[1].(int)$date[2]; 
                          ?>  {
                            "date": "<?php echo "$fecha[$vDate]" ?>",
                            "cumReal": <?php echo "$cumRealDay[$vDate]" ?>,
                            "cumPlan": <?php echo "$cumMetaDay[$vDate]" ?>, 
                            "dReales": <?php echo $valueDayT[$vDate]; ?>, 
                            "dPlan": <?php echo $valuePlanDayT[$vDate]; ?>, 
                            "difCumm": <?php echo $cumMetaDay[$vDate] - $cumRealDay[$vDate]; ?>, 
                            "difD": <?php echo $valuePlanDayT[$vDate] - $valueDayT[$vDate]; ?>
                        }, 
                         <?php } ?>        
                        ]
                    });
                </script>
                
            </div> 
            <img src="../../imagenes/nomenclaturas/Entregas.png" style="margin-left: 40px; height: 45px;" >
        </div>        
        
        <!-- APARTADO PARA PICKERS --> 
        <div class="row" style="margin-top: 1.4%" >
            <div class="col-lg-12 col-md-12 col-xs-12 col-sh-12" > 
                <script>
                    $(function() {
                        $("#dateIni").datepicker({ 
                            //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                            maxDate: new Date(), 
                            //OYENTES DE LOS PICKERS 
                            onSelect: function(date) { 
                                //Cuando se seleccione una fecha se cierra el panel
                                $("#ui-datepicker-div").hide(); 
                            } 
                        }); 

                        $("#dateEnd").datepicker({
                            //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                            maxDate: new Date(),
                            //OYENTES DE LOS PICKERS 
                            onSelect: function ( date ) {
                                //cuando se seleccione una fecha se cierra el panel
                                $("#ui-datepicker-div").hide(); 
                            } 
                        }); 
                    }); 
                </script> 
            
                <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado container container-fluid" >
                    <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3 contenidoCentrado input-group" style=" margin-left: 30px; float: left;" > 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo date("m/d/Y", strtotime($_SESSION['FIni'])); ?>" >
                        <span class="input-group-addon"> a </span> 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($_SESSION['FFin'])); ?>" >
                    </div> 
                    <div class="col-xs-1 col-sh-1 col-md-1 col-lg-1 contenidoCentrado" style="float: left;" > 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0;" onclick="setTipoDatos()" > 
                            <img src="../../imagenes/confirmar.png" > 
                        </button> 
                    </div> 
                    <div class="col-xs-7 col-sh-7 col-md-7 col-lg-7 contenidoCentrado" style="float: left;" > 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0; margin-left: 90%; " onclick="setTipoDatos2()" > 
                            <img src="../../imagenes/bAll.png" > 
                        </button> 
                    </div> 
                </div> 
            </div> 
        </div> 
        
        <div class="row " style="margin-top: 15px" >
            <div id="holder-semple-1" style="margin-left: 1.2%; width: 100%" >
                <script id="tamplate-semple-1" type="text/mustache" > 
                    <table class="inner-table" style="width: 95%; margin-left: 2.5%; margin-top: 1.0%" > 
                        <thead> 
                            <tr> 
                                <td > </td> 
                                <td colspan="<?php echo 12; ?>" data-scroll-span="<?php echo $dias+3; ?>" > PERIODO </td> 
                            </tr> 
                            <tr> 
                                <td align="center" >PROBLEMA</td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                                ?>
                                    <td align="center" ><?php echo $dateT[$vDate]; ?></td>
                                <?php } ?> 
                                <td>TOTAL</td>    
                            </tr> 
                        </thead> 
                        <tbody>                     
                            <tr >
                                <td> <img src="../../imagenes/nomenclaturas/p_reales.png" style="height: 25px;" > </td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2];                                  
                                ?>
                                <td align="center" ><?php echo $valueDayT[$vDate]; ?></td>
                                <?php } ?>
                                <td align="center" ><?php echo $cumRealDay[$vDate]; ?></td>
                            </tr > 
                            <tr >
                                <td > <img src="../../imagenes/nomenclaturas/p_plan.png" style="height: 20px;" > </td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2];                                  
                                ?>
                                <td align="center" ><?php echo $valuePlanDayT[$vDate]; ?></td>
                                <?php } ?>
                                <td align="center" ><?php echo $totalMeta; ?></td>
                            </tr >  
                            <tr >
                                <td > <img src="../../imagenes/nomenclaturas/c_plan.png" style="height: 20px;" > </td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2];                                  
                                ?>
                                <td align="center" ><?php echo $cumRealDay[$vDate]; ?></td>
                                <?php } ?>
                                <td align="center" ><?php echo $cumRealDay[$vDate]; ?></td>
                            </tr >  
                             <tr >
                                <td > <img src="../../imagenes/nomenclaturas/c_real.png" style="height: 17px;" > </td>
                                <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                    $date = explode("-", $i); 
                                    $vDate = $date[0].$date[1].(int)$date[2];                                  
                                ?>
                                <td align="center" ><?php echo $cumMetaDay[$vDate]; ?></td>
                                <?php } ?>
                                <td align="center" ><?php echo $cumMetaDay[$vDate]; ?></td>
                            </tr >  
                        </tbody>
                    </table>
                </script>
            </div>
        </div>
    </body>
</HTML>    
            
            