<HTML>
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/30/themes/light.js"></script>    
    
    <?php 
        include '../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
        //RECIBE LOS VALORES DEL AJAX 
        $tema = "Técnicas";
        
        if (isset($_SESSION['linea'])){
            $line = $_SESSION['linea'];
            $anio = $_SESSION['anio'];
            $mes = $_SESSION['mes'];
        } else {
            $line = 'L001';
            $anio = date('Y');
            $mes = date('m');
        }   
        
        $date->setISODate("$anio", 53);
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //INICIALIZAMOS VARIABLES PARA MES
        for($i = 1; $i <= 12; $i++){
            $mCant[$i] = 0;
        }  
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
            }
            
            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
            }            
        } else {
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
            }            
        }        
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        if ($sP == 0)
            $dSI = 7;          
        
        if ($sL == 0) 
            $dSL = 7;        
        
        $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio));
        $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));            
        
        //OBTENEMOS LA ULTIMA SEMANA DEL MES        
        $diaSemanaU = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

        //CONSULTA DE SCRAP(IFK, MISCELLANEOUS), SOLO POR CENTRO DE COSTOS   
        $mes2 = $mes-1; 
        $dI = 01;
        $dF = date("t",mktime(0,0,0,$mes,1,$anio));

        $fecha1 = $anio.'-01-01';
        $fecha2 = date("Y-m-d"); 

        $dateI = explode("-", $fecha1); 
        $dateF = explode("-", $fecha2); 
        
        $m[1] = (string) "Jun";
        $m[2] = (string) "Feb";
        $m[3] = (string) "Mar";
        $m[4] = (string) "Apr";
        $m[5] = (string) "May";
        $m[6] = (string) "Jun";
        $m[7] = (string) "Jul";
        $m[8] = (string) "Aug";
        $m[9] = (string) "Sep";
        $m[10] = (string) "Oct";
        $m[11] = (string) "Nov";
        $m[12] = (string) "Dec";
        
        //INCIALIZAMOS LAS VARIABLES
        //TABLA
        $countProblem = 0;
        $problema[0] = "";
        for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0;
            $targetDayT[$vDate] = 80; 
        } 
        
        //TIPO DE CALCULO QUE SE VA REALIZAR DE ACUERDO A LA OPCION DEL COMBO 
        if (isset($_POST["tipoDato"])) {
            $tipo = $_POST["tipoDato"];
        } else { 
            $tipo = 1;
        }
        
//        if (isset($_POST["tipo"])) {
        switch ($tipo) {
            case 1: //DURACION
            case 3: 
                $jTDay = jidokaDia($tema, $line, $anio);
                $jTWeek = jidokaSemana($tema, $line, $fP, $fL);
                $jTMonth = jidokaMonth($tema, $line, $anio) ;
                break;
            case 2: //FRECUENCIA
                $jTDay = jidokaDiaF($tema, $line, $anio);
                $jTWeek = jidokaSemanaF($tema, $line, $fP, $fL);
                $jTMonth = jidokaMonthF($tema, $line, $anio) ;
                break;
        }
//        } else {
//            $jTDay = jidokaDia($tema, $line, $anio);
//            $jTWeek = jidokaSemana($tema, $line, $fP, $fL);
//            $jTMonth = jidokaMonth($tema, $line, $anio) ;
//        }    
        
        //DIA
        for ($i = 0; $i < count($jTDay); $i++ ){
            $date = explode("-", $jTDay[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2];         
            $valueDayT[$vDate] = $jTDay[$i][1]; 
        }   
        
        //SEMANA
        for($i = 0; $i < count($jTWeek); $i++ ){
            $sem = $jTWeek[$i][0];
            $cwTec[$sem] = $jTWeek[$i][1];
        }
        
        //MES
        for($i = 0 ; $i < count($jTMonth); $i++){
            $nMes = $jTMonth[$i][0];            
            $mCant[$nMes] = $jTMonth[$i][1];    
        }
        
    ?>
    
    <head>       
	<script type="text/javascript">               
            window.onload = function () {
                var gridViewScroll = new GridViewScroll({
                    elementID : "gvMain",
                    freezeColumn : true,
                    freezeFooter : true,
                    freezeColumnCssClass : "GridViewScrollItemFreeze",
                    freezeFooterCssClass : "GridViewScrollFooterFreeze"
                });
                gridViewScroll.enhance();
            }
	</script>    
    </head>
   
    <body>
        <div >      
            <form action="aScrapProd.php" method="POST">
                <?php
                    echo "<input type="."\"hidden\" name="."\"line\""."value=".$line.">";
                    echo "<input type="."\"hidden\" name="."\"month\""."value=".$line.">";
                ?>                
            </form> 
            <br>
            <?php //echo $tipo,'<br>'; ?>
            
            <div style="align-content: center; alignment-adjust: central; align-items: center; margin-top: -1% ">
                <?php switch ($tipo) { case 1: ?>
                    <h5 align="center" >Indicadores por duración</h5>
                <?php break;  case 2 : ?>
                    <h5 align="center" >Indicadores por frecuencia</h5>
                <?php break;  case 3 : ?>
                    <h5 align="center" >Indicaros por piezas</h5>
                <?php break; } ?>
            </div>
            
            <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                <script>
                    var chart = AmCharts.makeChart("jTMonth", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": [
                            <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            {
                                "country": "<?php echo $m[$i] ?>",
                                "visits": <?php echo $mCant[$i] ?>
                            },
                            <?php } ?>
                        ],
                        "graphs": [{
                            "fillAlphas": 0.9,
                            "lineAlpha": 0.2,
                            "lineColor": "#02538b",
                            "type": "column",
                            "valueField": "visits"
                        }],
                        "categoryField": "country",
                        "valueAxes": [{
                            "title": "Indicador"
                        }]
                    });
                </script>
            </div>     
            
            <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                <script>
                    var chart = AmCharts.makeChart("jTWeek", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": [
                            <?php 
                            if($sP > $sL){
                                for ($i = $sP; $i <= $numSemanas; $i++) {                                
                            ?>
                                {
                                    "country": "<?php echo 'CW '.$i ?>",
                                    "visits": <?php echo $cwTec[$i] ?>
                                },
                            <?php } 
                                for($i = 1; $i <= $sL; $i++) {                                
                            ?>
                                {
                                    "country": "<?php echo 'CW '.$i ?>",
                                    "visits": <?php echo $cwTec[$i] ?>
                                },
                            <?php } ?>                            
                            <?php } else { 
                                for ($i = $sP; $i <= $sL; $i++) {                                
                            ?>
                                {
                                    "country": "<?php echo 'CW '.$i ?>",
                                    "visits": <?php echo $cwTec[$i] ?>
                                },
                            <?php } } ?>
                        ],
                        "graphs": [{
                            "fillAlphas": 0.9,
                            "lineAlpha": 0.2,
                            "lineColor": "#02538b",
                            "type": "column",
                            "valueField": "visits"
                        }],
                        "categoryField": "country",
                        "valueAxes": [{
                            "title": "Inidicador"
                        }]
                    });
                </script>                
            </div>    
            
            <!-- TABLA DE MESES -->
            <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                <tr >
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">Mes</th>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $m[$i] ?></th>
                    <?php } ?>
                </tr>
                <tr>
                    <?php switch ($tipo) { case 1: ?>
                        <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
                    <?php break;  case 2 : ?>
                        <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">No. Fallas</td>
                    <?php break;  case 3 : ?>
                        <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Piezas</td>
                    <?php break;  } ?> 
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $mCant[$i] ?></td>      
                    <?php } ?>
                </tr>                
            </table>
            
            <!-- TABLA DE SEMANA -->
            <table style="width: 35%; margin-left: 63%; margin-top: -70px; border: 1px solid #BABABA; ">
                <tr>
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                    <?php }} ?>                            
                </tr>
                <tr>
                    <?php switch ($tipo) { case 1: ?>
                        <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
                    <?php break;  case 2 : ?>
                        <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">No. Fallas</td>
                    <?php break;  case 3 : ?>
                        <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Piezas</td>
                    <?php break;  } ?> 
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                    <?php }} ?>  
                </tr>                
            </table>             
            <br>
            
            <div id="grafDays" class="jidokaDay" >                
                <script>
                    var chart = AmCharts.makeChart("grafDays", {
                        "type": "serial",
                        "theme": "light",
                        "dataDateFormat": "YYYY-MM-DD",
                        "precision": 0,
                        "dataProvider": [
                        <?php  
                            for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                $date = explode("-", $i);                                 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                        ?>    
                        {
                            <?php if ($valueDayT[$vDate] > $targetDayT[$vDate]){?>
                                "lineColor": "#CD4C47",
                            <?php } else {?>
                                "lineColor": "#02538b",
                            <?php }?>
                            "date": "<?php echo "$fecha[$vDate]" ?>",                            
                            "production": <?php echo $valueDayT[$vDate]; ?>,
                            "meta": "<?php echo $targetDayT[$vDate]; ?>"                            
                        },
                        <?php } ?>
                        ],
                        "valueAxes": [{
                            "id": "v1",
                            "title": "Indicador",
                            "position": "left",
                            "autoGridCount": false,
                            "labelFunction": function(value) {
                                return Math.round(value);
                            }
                        }],
                        "graphs": [{
                            "valueAxis": "v1",
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletColor": "#FFFFFF",
                            "bulletSize": 5,
                            "hideBulletsCount": 50,
                            "lineThickness": 2,
                            "lineColor": "#02538b",
                            "fillColorsField": "lineColor",
                            "lineColorField": "lineColor",
                            "type": "smoothedLine",
                            "title": "Indicador",
                            "useLineColorForBulletBorder": true,
                            "valueField": "production",
                            "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                        }, {
                            "valueAxis": "v2",
                            "bullet": "round",
                            "bulletBorderAlpha": 1,
                            "bulletColor": "#FFFFFF",
                            "bulletSize": 5,
                            "hideBulletsCount": 50,
                            "lineThickness": 2,
                            "lineColor": "#62cf73",
                            "type": "smoothedLine",
                            "title": "Meta",
                            "useLineColorForBulletBorder": true,
                            "valueField": "meta",
                            "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                        }],    
                        "chartScrollbar": {
                            "graph": "g1",
                            "oppositeAxis": false,
                            "offset": 30,
                            "scrollbarHeight": 40,
                            "backgroundAlpha": 0,
                            "selectedBackgroundAlpha": 0.1,
                            "selectedBackgroundColor": "#888888",
                            "graphFillAlpha": 0,
                            "graphLineAlpha": 0.5,
                            "selectedGraphFillAlpha": 0,
                            "selectedGraphLineAlpha": 1,
                            "autoGridCount": true,
                            "color": "#AAAAAA"
                        },
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha": 0,
                            "valueLineAlpha": 0.2
                        },
                        "categoryField": "date",
                        "categoryAxis": {
                            "parseDates": true,
                            "dashLength": 1,
                            "minorGridEnabled": true
                        },
                        "legend": {                            
                            "autoMargins": false,
                            "borderAlpha": 0.2,
                            "equalWidths": false,
                            "horizontalGap": 50,
                            "verticalGap": 15,
                            "markerSize": 10,
                            "useGraphSettings": true,
                            "valueAlign": "left",
                            "valueWidth": 0
                        },
                        "balloon": {
                            "borderThickness": 1,
                            "shadowAlpha": 0
                        },
                        "panelsSettings": {
                          "usePrefixes": true
                        },                        
                        "export": {
                            "enabled": false
                        },                        
                        "listeners": [{
                            "event": "rendered",
                            "method": function(e) {
                                chart.zoomToDates(new Date(<?php echo "$anio,$mes2,$dI" ?>), new Date(<?php echo "$anio,$mes2,$dF" ?>));
                            }
                        }]                        
                    });
                </script>
            </div>  
        </div> 
    </body>