<HTML>
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    
    <title>OEE</title>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script>  
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({ 
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 0, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function setTipoDatos() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla 
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes_1.php", 
                type: "post", 
                data: { tipoVista: 1 , fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 
        
        function setTipoDatos2() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes_1.php", 
                type: "post", 
                data: { tipoVista: 2 , fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 
        
        function getFixedColumnsData() {} 
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" />     
    
    <?php 
        include '../../db/ServerFunctions.php'; 
        session_start(); 
        $date = new DateTime; 
        
        //RECIBE LOS VALORES DEL AJAX         
        $line = $_SESSION['linea']; 
        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes']; 
        $fIni = date("Y-m-d", strtotime($_SESSION["FIni"])); 
        $fFin = date("Y-m-d", strtotime($_SESSION["FFin"])); 
        
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio)); 

        $date->setISODate("$anio", 53);

        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){ 
            $numSemanas = 53; 
        } else { 
            $numSemanas = 52; 
        } 
        
        # Obtenemos el numero de la semana 
        $semana = date("W",mktime(0,0,0,$mes,01,$anio)); 
        
        # Obtenemos el día de la semana de la fecha dada 
        $nWI = date("w",mktime(0,0,0,$mes,01,$anio)); 
        
        # el 0 equivale al domingo...
        if($nWI == 0) 
            $nWI=7; 

        # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
        $fWI = date("Y-m-d", strtotime("-7 week", mktime(0,0,0,$mes,01-$nWI+1,$anio))); 

        # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
        $fWF = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(7-$nWI),$anio)); 
        
        $sP = date("W",mktime(0,0,0, date("m", strtotime($fWI)),$nWI,date("Y", strtotime($fWI))));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0, $mes,$nWI,date("Y", strtotime($fWF)))); 
        
        //VARIABLES
        $oeeLastW = 0;
        $techLastW = 0;
        $orgLastW = 0;
        $coLastW = 0;
        $quaLastW = 0;
        $perfLastW = 0; 
        $targetLastW = 0; 
        
        //INICIALIZAMOS VARIABLES PARA SEMANA 
        if ($sP > $sL){ 
            for ($i = $sP; $i <= $numSemanas; $i++ ) { 
                $cw[$i] = 0; 
                $prodW[$i] = 0; 
                $tecW[$i] = 0; 
                $orgW[$i] = 0;
                $calW[$i] = 0;
                $cambW[$i] = 0;
                $tFalW[$i] = 0;
                $targetW[$i] = 0;
            }

            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0; 
                $prodW[$i] = 0; 
                $tecW[$i] = 0; 
                $orgW[$i] = 0; 
                $calW[$i] = 0; 
                $cambW[$i] = 0; 
                $tFalW[$i] = 0; 
                $targetW[$i] = 0; 
            }            
        } else { 
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $prodW[$i] = 0;
                $tecW[$i] = 0;
                $orgW[$i] = 0;
                $calW[$i] = 0;
                $cambW[$i] = 0;
                $tFalW[$i] = 0; 
                $targetW[$i] = 0;
            } 
        } 
        
        /************************** SEMANAL ****************************/
        $datOEEWeek = cPercentTopicWekly($line, $fWI, $fWF);
        $cWP = 0;
        for ($i = 0; $i < count($datOEEWeek); $i++){
            $s = $datOEEWeek[$i][0];
            $prodW[$s] = @round($datOEEWeek[$i][1],2);
            $tecW[$s] = @round($datOEEWeek[$i][2],2);
            $orgW[$s] = @round($datOEEWeek[$i][3],2);
            $calW[$s] = @round($datOEEWeek[$i][4],2);
            $cambW[$s] = @round($datOEEWeek[$i][5],2);
            $tFalW[$s] = @round($datOEEWeek[$i][6],2); 
            
            if ($prodW[$s] != 0 || $tecW[$s] != 0 || $orgW[$s] != 0 || $calW[$s] != 0 || $cambW[$s] != 0 || $tFalW[$s] != 0 ){
                $cWP++;
            }
            
            $oeeLastW += $prodW[$s];
            $techLastW += $tecW[$s];
            $orgLastW += $orgW[$s];
            $coLastW += $cambW[$s];
            $quaLastW += $calW[$s]; 
            $perfLastW += $tFalW[$s]; 
        } 
        
        $oeeLastW = $oeeLastW/$cWP;
        $techLastW = $techLastW/$cWP;
        $orgLastW = $orgLastW /$cWP; 
        $coLastW = $coLastW/$cWP;
        $quaLastW = $quaLastW/$cWP; 
        $perfLastW = $perfLastW/$cWP; 
        
        #TARGET
        $cWT = 0;
        $cTargetWeek = cTargetOEEWeekly_IP($line, $fWI, $fWF);
        for($i = 0; $i < count($cTargetWeek); $i++){
            $w = $cTargetWeek[$i][0];
            $targetW[$w] = $cTargetWeek[$i][1];
            if ($targetW[$w] != 0 ){
                $cWT++;
            }
            $targetLastW += $targetW[$w] ;
        }
        
        $targetLastW = $targetLastW/$cWT ;
        
        /************************* TABLA DIA *********************************/
        $countProblem = 0;
        $problema[1] = "OEE (%)";
        $problema[2] = "NO Calida (%)";
        $problema[3] = "Organizacionales (%)";
        $problema[4] = "Tecnicas (%)";
        $problema[5] = "Cammbio de Modelo (%)";
        $problema[6] = "Desempeño (%)";
        $countX = 1;
        $total = 0;
        $sumProblemas = 0;

        //DATOS PARA TAMAÑO DE TABLA    
        $dias = (strtotime($fIni)- strtotime($fFin))/86400;
        $dias = abs($dias);
        $dias = floor($dias);    

        //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
        //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
        if ($dias > 11 ){
            $rowspan = 12;
        } else {
            //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
            //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS
            $rowspan = $dias+1;
        }

        //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
        if($countProblem == 0) { 
            $totalProblema[1] = 0;
            $countProblem = 1;
        }
        
    ?>
    
    <body>
        <div >  
            <div class=" row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" style="margin-top: 2.1%" > 
                <div id="jTWeek" name="jTWeek" style="width: 100%; height: 35vh; " >
                    <script>
                        var chart = AmCharts.makeChart("jTWeek", {
                            "type": "serial",
                            "theme": "none",
                            "precision": 2, 
                            "titles": [{
                                "text": "Semanal",
                                "size": 12
                            }],
                            
                            "dataProvider": [
                            <?php 
                                if ($sP > $sL) { 
                                    for ($i = $sP; $i <= $numSemanas; $i++ ) { 
                                    ?>
                                        {
                                            "date": "<?php echo 'CW-'.$i ?>",
                                            "prod": <?php echo $prodW[$i] ?>, 
                                            "calidad": <?php echo $calW[$i] ?>,
                                            "org": <?php echo $orgW[$i] ?>,
                                            "tec": <?php echo $tecW[$i] ?>,
                                            "cambio": <?php echo $cambW[$i] ?>,
                                            "desempenio": <?php echo $tFalW[$i] ?>,
                                            "Meta": <?php echo $targetW[$i] ?>
                                        },
                                    <?php } 
                                        for($i = 1; $i <= $sL; $i++) { 
                                    ?>
                                        {
                                            "date": "<?php echo 'CW-'.$i ?>",
                                            "prod": <?php echo $prodW[$i] ?>, 
                                            "calidad": <?php echo $calW[$i] ?>,
                                            "org": <?php echo $orgW[$i] ?>,
                                            "tec": <?php echo $tecW[$i] ?>,
                                            "cambio": <?php echo $cambW[$i] ?>,
                                            "desempenio": <?php echo $tFalW[$i] ?>,
                                            "Meta": <?php echo $targetW[$i] ?>
                                        },
                                    <?php } ?>                            
                                    <?php } else { 
                                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                        {
                                            "date": "<?php echo 'CW-'.$i ?>",
                                            "prod": <?php echo $prodW[$i] ?>, 
                                            "calidad": <?php echo $calW[$i] ?>,
                                            "org": <?php echo $orgW[$i] ?>,
                                            "tec": <?php echo $tecW[$i] ?>,
                                            "cambio": <?php echo $cambW[$i] ?>,
                                            "desempenio": <?php echo $tFalW[$i] ?>,
                                            "Meta": <?php echo $targetW[$i] ?>
                                        },
                                    <?php } ?>
                                    { 
                                        "date": "<?php echo 'Av. Last 8 weeks' ?>",
                                        "prod": <?php echo $oeeLastW ?>, 
                                        "calidad": <?php echo $quaLastW ?>,
                                        "org": <?php echo $orgLastW ?>,
                                        "tec": <?php echo $techLastW ?>,
                                        "cambio": <?php echo $coLastW ?>,
                                        "desempenio": <?php echo $perfLastW ?>,
                                        "Meta": <?php echo $targetLastW ?>
                                    }, { 
                                        "date": "<?php echo 'Target Av. 19 ' ?>",
                                        "prod": <?php echo 80 ?>, 
                                        "calidad": <?php echo 2 ?>,
                                        "org": <?php echo 2 ?>,
                                        "tec": <?php echo 2?>,
                                        "cambio": <?php echo 2 ?>,
                                        "desempenio": <?php echo 2 ?>,
                                        "Meta": <?php echo 85 ?>
                                    }  
                                <?php } ?>    
                                    
                            ], 
                            "valueAxes": [{
                                "title": "Indicador", 
                                "maximum": 100,
                                "minumum": 50,
                                "stackType": "regular",
                                "unit": "%",
                                "axisAlpha": 0.5,
                                "gridAlpha": 0.2,
                                "labelsEnabled": true,
                                "position": "left"
                            }],
                            "graphs": [ {
                                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                "fillAlphas": 1,
                                "fillColors": "#0DF205",
                                "lineColor": "#0DF205",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "id":"mProd",
                                "title": "Produccion",
                                "type": "column",
                                "color": "#000",
                                "valueField": "prod"
                            }, {
                                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                "fillAlphas": 1,
                                "fillColors": "#30348C",
                                "lineColor": "#30348C",
                                "lineAlpha": 1,
                                "title": "Tech",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "tec"
                            }, {
                                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                "fillAlphas": 1,
                                "fillColors": "#31859C",
                                "lineColor": "#31859C",
                                "lineAlpha": 1,
                                "title": "Org",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "org"
                            }, {
                                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                "fillAlphas": 1,
                                "fillColors": "#8C3063",
                                "lineColor": "#8C3063",
                                "lineAlpha": 1,
                                "title": "  Quality",
                                "type": "column",
                                "color": "#FFFF",
                                "valueField": "calidad"
                            }, {
                                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                "fillAlphas": 1,
                                "fillColors": "#92A0B9",
                                "lineColor": "#92A0B9",
                                "labelText": "[[value]]",
                                "lineAlpha": 1,
                                "title": "C/O",
                                "type": "column",
                                "color": "#FFFFFF",
                                "valueField": "cambio"
                            }, {
                                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                "fillAlphas": 1,
                                "fillColors": "#F29F05", 
                                "lineColor": "#F29F05", 
                                "lineAlpha": 1,
                                "title": "Perf", 
                                "type": "column", 
                                "color": "#FFFFFF",
                                "valueField": "desempenio"
                            }, {
                                "id": "graph2",
                                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                                "lineThickness": 2,                                
                                "lineColor": "#000000",                                
                                "fillAlphas": 0,
                                "lineAlpha": 2,
                                "title": "Meta",
                                "valueField": "Meta",
                                "dashLengthField": "dashLengthLine"
                            }],                                                    
                            "categoryField": "date",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "axisAlpha": 0,
                                "gridAlpha": 0.2,
                                "position": "left"
                            }
                        });
                    </script>                
                </div> 

                <!-- TABLA DE SEMANA -->
                <table style="width: 100%; margin-top: 5px; border: 1px solid #BABABA; ">
                    <tr>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php }} ?>                            
                    </tr>
                    <tr>
                        <td style="border: 1px solid #BABABA; text-align: center;">OEE %</td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $prodW[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $prodW[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $prodW[$i] ?></td>
                        <?php }} ?>  
                    </tr>   
                    <tr>
                        <td style="border: 1px solid #BABABA; text-align: center;">Tecnicas %</td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tecW[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tecW[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tecW[$i] ?></td>
                        <?php }} ?>  
                    </tr>
                    <tr>
                        <td style="border: 1px solid #BABABA; text-align: center;">Organizacionales %</td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $orgW[$i] ?></td>
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $orgW[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $orgW[$i] ?></td>
                        <?php }} ?>  
                    </tr>
                    <tr>
                        <td style="border: 1px solid #BABABA; text-align: center;">Cambios %</td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cambW[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cambW[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cambW[$i] ?></td>
                        <?php }} ?> 
                    </tr> 
                    <tr> 
                        <td style="border: 1px solid #BABABA; text-align: center;">Calidad %</td> 
                        <?php 
                        if($sP > $sL){ 
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?> 
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $calW[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $calW[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $calW[$i] ?></td>
                        <?php }} ?>  
                    </tr>
                    <tr>
                        <td style="border: 1px solid #BABABA; text-align: center;">Desempeño %</td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tFalW[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tFalW[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tFalW[$i] ?></td>
                        <?php }} ?>  
                    </tr>
                </table>            
            </div> 
            <br>
        </div> 
    </body>