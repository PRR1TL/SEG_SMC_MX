
<?php 

        include '../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
        //RECIBE LOS VALORES DEL AJAX 
        $tema = "Tecnicas"; 
        
        $line = $_SESSION['linea']; 
        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes'];         
       
        $date->setISODate("$anio", 53);
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //INICIALIZAMOS VARIABLES PARA MES
        for($i = 1; $i <= 12; $i++){
            $mCant[$i] = 0;
            $targetMonth[$i] = 0;
        }  
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
                
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0; 
                $cwTec[$i] = 0; 
                $targetWeek[$i] = 0; 
            }
            
            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0; 
                $cwTec[$i] = 0; 
                $targetWeek[$i] = 0; 
            }            
        } else {
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0; 
                $cwTec[$i] = 0; 
                $targetWeek[$i] = 0; 
            }            
        }        
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        if ($sP == 0)
            $dSI = 7;          
        
        if ($sL == 0) 
            $dSL = 7;        
        
        $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio));
        $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));            
        
        //OBTENEMOS LA ULTIMA SEMANA DEL MES        
        $diaSemanaU = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

        //CONSULTA DE SCRAP(IFK, MISCELLANEOUS), SOLO POR CENTRO DE COSTOS   
        $mes2 = $mes-1; 
        $dI = 01;
        $dF = date("t",mktime(0,0,0,$mes,1,$anio)); 
        
        $m[1] = (string) "Jun";
        $m[2] = (string) "Feb";
        $m[3] = (string) "Mar";
        $m[4] = (string) "Apr";
        $m[5] = (string) "May";
        $m[6] = (string) "Jun";
        $m[7] = (string) "Jul";
        $m[8] = (string) "Aug";
        $m[9] = (string) "Sep";
        $m[10] = (string) "Oct";
        $m[11] = (string) "Nov";
        $m[12] = (string) "Dec";
        
        //INCIALIZAMOS LAS VARIABLES 
        $countProblem = 0;
        for($i = $_SESSION["FIni"]; $i <= date("Y-m-d", strtotime($_SESSION["FFin"] ."+ 1 days")); $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0; 
            $targetDayT[$vDate] = 0; 
        } 
        
        $tipo = $_SESSION['tipoDato']; 
        
        switch ($tipo) { 
            case 1: //DURACION 
                $jTDay = jidokaDia($tema, $line, $anio); 
                $jTDayTable = tablaJidokaNOPlaneados($tema, $line, $_SESSION["FIni"], $_SESSION["FFin"] ); 
                $jTWeek = jidokaSemana($tema, $line, $fP, $fL); 
                $jTMonth = jidokaMonth($tema, $line, $anio); 
                break; 
            case 2: //FRECUENCIA 
                $jTDay = jidokaDiaF($tema, $line, $anio); 
                $jTDayTable = tablaJidokaNOPlaneadosF($tema, $line, $_SESSION["FIni"], $_SESSION["FFin"] ); 
                $jTWeek = jidokaSemanaF($tema, $line, $fP, $fL); 
                $jTMonth = jidokaMonthF($tema, $line, $anio); 
                break;
            case 3: //PIEZAS //$_SESSION["FFin"] =  date("Y-m-d", strtotime($_SESSION["FIni"] ."- 1 day")); 
                $jTDay = tablaJidokaNOPlaneados($tema, $line, $_SESSION["FIni"], $_SESSION["FFin"] ); 
                $jTDayTable = tablaJidokaNOPlaneados($tema, $line, $_SESSION["FIni"], $_SESSION["FFin"] ); 
                $jTWeek = jidokaSemana($tema, $line, $fP, $fL); 
                $jTMonth = jidokaMonth($tema, $line, $anio); 
                break;            
        } 
        
        for ($i = $_SESSION["FIni"]; $i <= date("Y-m-d", strtotime($_SESSION['FFin'] ."+ 1 days")); $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            //$countX++;
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2];         
            switch($date[1]){
                case 1:
                    $cadDate[$i] = 'Jan&nbsp;'.$date[2];
                    break;
                case 2:
                    $cadDate[$i] = 'Feb&nbsp;'.$date[2];
                    break;
                case 3:
                    $cadDate[$i] = 'Mar&nbsp;'.$date[2];
                    break;
                case 4:
                    $cadDate[$i] = 'Apr&nbsp;'.$date[2];
                    break;
                case 5:
                    $cadDate[$i] = 'May&nbsp;'.$date[2];
                    break;
                case 6:
                    $cadDate[$i] = 'Jun&nbsp;'.$date[2];
                    break;
                case 7:
                    $cadDate[$i] = 'Jul&nbsp;'.$date[2];
                    break;
                case 8:
                    $cadDate[$i] = 'Aug&nbsp;'.$date[2];
                    break;
                case 9:
                    $cadDate[$i] = 'Sep&nbsp;'.$date[2];
                    break;
                case 10:
                    $cadDate[$i] = 'Oct&nbsp;'.$date[2];
                    break;
                case 11:
                    $cadDate[$i] = 'Nov&nbsp;'.$date[2];
                    break;
                case 12:
                    $cadDate[$i] = 'Dec&nbsp;'.$date[2];
                    break;
            }

            //INICIALIZAMOS LAS VARIABLES DE LA TABLA
            if (count($jTDayTable) > 0 ){
                for ($j = 1; $j <= count($jTDayTable); $j++ ) {
                    $valueProblemTable[$vDate][$j] = 0; 
                    //VARIABLE DEFINIDA SOLO PARA CUANDO EL TIPO DE DATO SEA PIEZAS
                    $valueDuracionTable[$vDate][$j] = 0;
                    $totalProblema[$j] = 0;
                }
            } else {
                $valueProblemTable[$vDate][1] = 0;
            }

            //INICIALIZAMOS LAS VARIABLES PARA LOS SUBTOTALES DIA(_)            
            $totalDia[$vDate] = 0; 
            $targetDayT[$vDate] = 0; 
        } 
        
        /************************ DIA **********************/
        for ($i = 0; $i < count($jTDay); $i++ ){
            $date = explode("-", $jTDay[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2];         
            $valueDayT[$vDate] = $jTDay[$i][1]; 
            //echo '<br>',$i, ' (',$vDate,') -> ', $date[0], ', ',$date[1],', ',(int)$date[2];
        } 
        
        #TARGET
        $ctargetDay = cTargetTecDaily($line, $_SESSION["FIni"], $_SESSION["FFin"]);
        for ($i = 0; $i < count($ctargetDay) ; $i++) { 
            $date = explode("-", $ctargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $targetDayT[$vDate] = $ctargetDay[$i][1]; 
        } 
        
        /********************* SEMANA *************************/
        for($i = 0; $i < count($jTWeek); $i++ ){
            $sem = $jTWeek[$i][0];
            $cwTec[$sem] = $jTWeek[$i][1];
        } 
        
        #TARGET 
        $cTargetWeek = cTargetTecWeekly($line, $anio, $mes);
        for ($i = 0; $i < count($cTargetWeek); $i++){
            $s = $cTargetWeek[$i][0];
            $targetWeek[$s] = $cTargetWeek[$i][1];
        }
        
        /********************** MES ***********************/
        for($i = 0 ; $i < count($jTMonth); $i++){
            $nMes = $jTMonth[$i][0];            
            $mCant[$nMes] = $jTMonth[$i][1];    
        }         
        
        #TARGET
        $cTargetMonth = cTargetTecMonthy($line, $anio);
        for ($i = 0; $i < count($cTargetMonth); $i++ ){
            $ms = $cTargetMonth[$i][0];
            $targetMonth[$ms] = $cTargetMonth[$i][1];
        }
        
        //INCIALIZAMOS LAS VARIABLES
        //TABLA 
        $countX = 1;
        $total = 0;
        $sumProblemas = 0;
        
        //EVALUACION PARA EL TIPO DE DATO QUE SE TIENE  
        if($tipo != 3 ) {     
            for ($i = 0; $i < count($jTDayTable); $i++ ) { 
                $date = explode("-", $jTDayTable[$i][0]); 
                $vDate = $date[0].$date[1].(int)$date[2]; 
                
                if ($i == 0) { 
                    $countProblem = 1; 
                    $operacion[$countProblem] = $jTDayTable[$i][1]; 
                    $code[$countProblem] = $jTDayTable[$i][2]; 
                    $problema[$countProblem] = $jTDayTable[$i][3]; 
                     
                    //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                    $totalProblema[$countProblem] = 0; 
                    
                }  else {                     
                    if ($problema[$countProblem] != $jTDayTable[$i][3] && $problema[$countProblem] != $jTDayTable[$i][2]) { 
                        $countProblem++; 
                        $operacion[$countProblem] = $jTDayTable[$i][1]; 
                        $code[$countProblem] = $jTDayTable[$i][2]; 
                        $problema[$countProblem] = $jTDayTable[$i][3];

                        //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                        //$totalProblema[$countProblem] = 0;                         
                    } 
                }   
                
                //echo "<br>* ", $jTDayTable[$i][1],', ',$jTDayTable[$i][2]; 
                
                //PASO DE VALORES A VARIABLE DE SUBTOTAL PROBLEMA(|) 
                $totalProblema[$countProblem] += $jTDayTable[$i][4];

                //PASO DE VALORES A VARIABLE DE PROBLEMA DIA
                $valueProblemTable[$vDate][$countProblem] += $jTDayTable[$i][4]; 

                //ASIGNACION DE VALORES PARA SUBTOTALES DE DIA(_)
                $totalDia[$vDate] += $jTDayTable[$i][4];

                //TOTAL QUE SE MOSTRARA EN LA ESQUINA INFERIOR DERECHA
                $total += $jTDayTable[$i][4];              
            }
        } else { //CONSULTA DE BASE DE DATOS CUANDO LA OPCION ES PIEZAS    
            for ($i = 0; $i < count($jTDayTable); $i++ ) { 
                $date = explode("-", $jTDayTable[$i][0]); 
                $vDate = $date[0].$date[1].(int)$date[2]; 
                
                if ($i == 0) { 
                    $tc = 1; 
                    $countProblem = 1; 
                    $problema[$countProblem] = $jTDayTable[$i][1];                     

                    //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA 
                    $totalProblema[$countProblem] = 0; 

                    //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA 
                    $cTC = tcLineaNoParte($line, $jTDayTable[$i][3]);
                    $valueProblemTable[$vDate][$countProblem] += ($jTDayTable[$i][2] *  60) / $cTC[0][0];
                    
                    //ASIGNACION DE VALORES PARA SUBTOTALES DE PROBLEMA (|)
                    //PASA EL PRIMER VALOR QUE SE TIENE EN EL PRIMER ENCUENTRO DEL PROBLEMA
                    $totalProblema[$countProblem] = @floor($valueProblemTable[$vDate][$countProblem]);
                } else { 
                    if ($problema[$countProblem] != $jTDayTable[$i][1] ){
                        $countProblem++;
                        $tc = 1;
                        $problema[$countProblem] = $jTDayTable[$i][1];

                        //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                        //$totalProblema[$countProblem] = 0;

                        //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA
                        $cTC = tcLineaNoParte($line, $jTDayTable[$i][3]); 
                        $valueProblemTable[$vDate][$countProblem] += @(($jTDayTable[$i][2] *  60) / $cTC[0][0]);                        
                    
                        //SUBTOTALES DE CUADRO DE INFROMACION 
                        //SUMATORIA DE TOTAL POR PROBLEMA (|) 
                        $totalProblema[$countProblem] = @floor($valueProblemTable[$vDate][$countProblem]); 
                    } else {
                        $tc++;
                        
                        //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA 
                        $cTC = tcLineaNoParte($line, $jTDayTable[$i][3]);
                        $valueProblemTable[$vDate][$countProblem] += @(($jTDayTable[$i][2] *  60) / $cTC[0][0]);                        
                        
                        //SUBTOTALES DE CUADRO DE INFROMACION 
                        //SUMATORIA DE TOTAL POR PROBLEMA (|) 
                        $totalProblema[$countProblem] += @floor($valueProblemTable[$vDate][$countProblem]); 
                    }                     
                    //SUMATORIA DE TOTAL POR DIA (_) 
                    //$totalDia[$vDate] += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]);                     
                } 
                //echo "<br>+ ", $jTDayTable[$i][1]; 
                //SUMATORIA DE TOTAL POR DIA (_) 
                $valueDayT[$vDate] += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]); 
                $totalDia[$vDate] += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]); 
                
                //TOTAL DE LA INFORMACION ( VA EN LA ESQUINA INFERIOR DERECHA)
                $total += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]);
            }
        } 
        
        //DATOS PARA TAMAÑO DE TABLA  
        //$dias = (strtotime($_SESSION["FIni"])- strtotime($_SESSION["FFin"]))/86400;
        $dias = (strtotime($_SESSION["FIni"])- strtotime(date("Y-m-d", strtotime($_SESSION['FFin'] ."+ 1 days"))))/86400;
        $dias = abs($dias); 
        $dias = floor($dias)+1; 
        //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
        //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS 
        if ($dias > 11 ) { 
            $rowspan = 12;
        } else { 
            //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
            //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS 
            $rowspan = $dias-1;
        } 

        //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
        if($countProblem == 0) { 
            $countProblem = 1;
            $problema[1] = "";
            $totalProblema[1] = 0;
        } 
        
?>

<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" > 
    <button type="submit" id="export_data" name='export_data' value="Export to excel" class="btn btn-info">Export to excel</button>
</form> 

<table id="" class="table table-striped table-bordered">
<thead> 
    <?php for($x = 0; $x <= 5; $x++ ){ ?> 
    <tr>
        <th>&nbsp;</th>  
        <th>Code</th> 
        <th>Operacion</th>                                     
        <th align="center" >PROBLEMA</th>
        <?php for($i = $_SESSION["FIni"]; $i < date("Y-m-d", strtotime($_SESSION['FFin'] ."+ 1 days")); $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ ?>
            <th align="center" ><?php echo $cadDate[$i]; ?></th>
        <?php } ?> 
        <th>Total</th> 
    </tr> 
    <?php } ?> 
</thead>
<tbody>

</tbody>
</table>

<?php 
    if(isset($_POST["export_data"])) {
        $filename = "tecnicas_.xls";
        
        header("Content-Type: application/vnd.ms-excel; charset=iso-8859-15");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $show_coloumn = false;
        if(!empty($developer_records)) {
            foreach($developer_records as $record) {
                if(!$show_coloumn) {
                // display field/column names in first row
                    echo implode("\t", array_keys($record)) . "\n";
                    $show_coloumn = true;
                }
                echo implode("\t", array_values($record)) . "\n";
            }
        }
    exit;
    }

?>

