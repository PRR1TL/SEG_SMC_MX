<HTML>
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <!-- LIBRERIAS Y CONFIGURACION PARA TABLA -->
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
<!--    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>-->
    <script src="../../js/table-scroll.min.js"></script>  
    <script>        
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
        
         function setTipoDatos() { 
            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON
            var dato = document.getElementById("tipoDatos").value; 

            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes.php", 
                type: "post", 
                data: { tipoVista: 1 , tipoDato: dato, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 
        
        function setTipoDatos2() { 
            //alert("ENTRA DE SET TIPO DATOS"); 
            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON 
            var dato = document.getElementById("tipoDatos").value; 

            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla 
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes_1.php", 
                type: "post", 
                data: { tipoVista: 2, tipoDato: dato, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas 
                    location.reload(); 
                } 
            }); 
        } 
        
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
    
    <!-- LIBRERIAS Y CONFIGURACION PARA PICKERS -->
    <!-- LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR -->     
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <?php 
        include '../../db/ServerFunctions.php'; 
        session_start(); 
        $date = new DateTime; 
        
        //RECIBIMOS LOS VALORES DEL AJAX 
        $tema = "Cambio de Modelo"; 
        
        $line = $_SESSION['linea']; 
        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes']; 
        $fIni = date("Y-m-d", strtotime($_SESSION["FIni"])); 
        $fFin = date("Y-m-d", strtotime($_SESSION["FFin"])); 
        
        $date->setISODate("$anio", 53); 
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //INICIALIZAMOS VARIABLES PARA MES
        for($i = 1; $i <= 12; $i++){
            $mCant[$i] = 0;
            $targetMonth[$i] = 0;
        }  
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
                
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
                $targetWeek[$i] = 0;
            }
            
            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
                $targetWeek[$i] = 0;
            }            
        } else {
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwTec[$i] = 0;
                $targetWeek[$i] = 0;
            }            
        }        
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        if ($sP == 0)
            $dSI = 7;          
        
        if ($sL == 0) 
            $dSL = 7;        
        
        $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio));
        $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));            
        
        //OBTENEMOS LA ULTIMA SEMANA DEL MES        
        $diaSemanaU = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

        //CONSULTA DE SCRAP(IFK, MISCELLANEOUS), SOLO POR CENTRO DE COSTOS   
        $mes2 = $mes-1; 
        $dI = 01;
        $dF = date("t",mktime(0,0,0,$mes,1,$anio)); 
        
        $lblM[1] = (string) "Jun";
        $lblM[2] = (string) "Feb";
        $lblM[3] = (string) "Mar";
        $lblM[4] = (string) "Apr";
        $lblM[5] = (string) "May";
        $lblM[6] = (string) "Jun";
        $lblM[7] = (string) "Jul";
        $lblM[8] = (string) "Aug";
        $lblM[9] = (string) "Sep";
        $lblM[10] = (string) "Oct";
        $lblM[11] = (string) "Nov";
        $lblM[12] = (string) "Dec";
        
        //INCIALIZAMOS LAS VARIABLES
        //TABLA
        $countProblem = 0;
        for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){        
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0;
            $targetDayT[$vDate] = 0;       
        }  
        
        //TIPO DE CALCULO QUE SE VA REALIZAR DE ACUERDO A LA OPCION DEL COMBO 
        if (isset($_SESSION["jTipoDato"])) {
            $tipo = $_SESSION["jTipoDato"];
        } else { 
            $tipo = 1;
        }
            
        $jProblemDayTable = tablaProblemasJidokaCambios($line, $fIni, $fFin);
        //echo $line,', ', $fIni,', ', $fFin, ' -> ', count($jProblemDayTable);
        switch ($tipo) {
            case 1:
                $jTDay = jidokaDia($tema, $line, $anio);
                //$jProblemDayTable = tablaProblemasJidokaCambios($line, $fIni, $fFin);
                $jTDayTable = tablaJidokaCambios( $line, $fIni, $fFin );
                $jTWeek = jidokaSemana($tema, $line, $fP, $fL);
                $jTMonth = jidokaMonth($tema, $line, $anio) ;
                break;
            case 2:
                $jTDay = jidokaDiaF($tema, $line, $anio);
                $jTDayTable = tablaJidokaCambiosF($line, $fIni, $fFin );
                $jTWeek = jidokaSemanaF($tema, $line, $fP, $fL);
                $jTMonth = jidokaMonthF($tema, $line, $anio) ;
                break;
            case 3:
                //$jTDay = tablaJidokaCambios( $line, $fIni, $fFin );
                $jTDay = jidokaDiaCambios( $line, $anio );
                $jTDayTable = tablaJidokaCambios( $line, $fIni, $fFin );
                $jTWeek = jidokaSemana($tema, $line, $fP, $fL);
                $jTMonth = jidokaMonth($tema, $line, $anio) ;
                break;
        }
        
        #OBTENCION DE LOS DISTINTOS TIPOS DE CAMBIOS REGISTRADOS  
        for ($i = 0 ; $i < count($jProblemDayTable); $i++){
            $desCambio[$i+1] = $jProblemDayTable[$i][0];
            $totalProblema[$i+1] = 0; 
        } 
        
        /******************** DIA *********************/
        for ($i = 0; $i < count($jTDay); $i++ ){
            $date = explode("-", $jTDay[$i][0],3); 
            $vDate = $date[0].$date[1].(int)$date[2];         
            $valueDayT[$vDate] = $jTDay[$i][1]; 
        } 
        
        #TARGET 
        $cTargetDay = cTargetCambioDaily($line, $fIni, $fFin); 
        for ($i = 0; $i < count($cTargetDay); $i++){
            $date = explode("-", $cTargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $targetDayT[$vDate] = $cTargetDay[$i][1];
        }
        
        /***************** SEMANA ******************/
        for($i = 0; $i < count($jTWeek); $i++ ){
            $sem = $jTWeek[$i][0];
            $cwTec[$sem] = $jTWeek[$i][1];
        } 
        
        #TARGET
        $cTargetWeek = cTargetCambioWeekly($line, $anio, $mes);
        for ($i = 0; $i < count($cTargetWeek); $i++ ){ 
            $s = $cTargetWeek[$i][0];
            $targetWeek[$s] = $cTargetWeek[$i][1];
            //echo "<br>", $s, ', ', $targetWeek[$s];
        }
        
        /***************** MES ********************/
        for($i = 0 ; $i < count($jTMonth); $i++){
            $nMes = $jTMonth[$i][0];            
            $mCant[$nMes] = $jTMonth[$i][1]; 
        }  
        
        #TARGET
        $cTargetMonth = cTargetOrgMonthy($line, $anio);
        for ($i = 0; $i < count($cTargetMonth); $i++){
            $m = $cTargetMonth[$i][0];
            $targetMonth[$m] = $cTargetMonth[$i][1];
        }
        
        //INCIALIZAMOS LAS VARIABLES
        //TABLA
        $problema[0] = ""; 
        $countX = 1; 
        $total = 0; 
        $sumProblemas = 0; 
        
        for ($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $countX++;
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            switch($date[1]){
                case 1:
                    $cadDate[$i] = 'Jan&nbsp;'.$date[2];
                    break;
                case 2:
                    $cadDate[$i] = 'Feb&nbsp;'.$date[2];
                    break;
                case 3:
                    $cadDate[$i] = 'Mar&nbsp;'.$date[2];
                    break;
                case 4:
                    $cadDate[$i] = 'Apr&nbsp;'.$date[2];
                    break; 
                case 5: 
                    $cadDate[$i] = 'May&nbsp;'.$date[2];
                    break;
                case 6:
                    $cadDate[$i] = 'Jun&nbsp;'.$date[2];
                    break;
                case 7:
                    $cadDate[$i] = 'Jul&nbsp;'.$date[2];
                    break;
                case 8:
                    $cadDate[$i] = 'Aug&nbsp;'.$date[2];
                    break;
                case 9:
                    $cadDate[$i] = 'Sep&nbsp;'.$date[2];
                    break;
                case 10:
                    $cadDate[$i] = 'Oct&nbsp;'.$date[2];
                    break;
                case 11:
                    $cadDate[$i] = 'Nov&nbsp;'.$date[2];
                    break;
                case 12:
                    $cadDate[$i] = 'Dec&nbsp;'.$date[2];
                    break;
            }

            //INICIALIZAMOS LAS VARIABLES DE LA TABLA
            for ($j = 1 ; $j <= count($jProblemDayTable); $j++){ 
                $valueProblemTable[$vDate][$j] = 0;
                $valueDuracionTable[$vDate][$j] = 0;
            } 
            
            //INICIALIZAMOS LAS VARIABLES PARA LOS SUBTOTALES DIA(_)            
            $totalDia[$vDate] = 0; 
        } 
        
        //EVALUACION PARA EL TIPO DE DATO QUE SE TIENE 
        //echo 'cTabla ',count($jTDayTable); 
        if($tipo != 3 ){
            for ($i = 0; $i < count($jTDayTable); $i++ ) {
                $date = explode("-", $jTDayTable[$i][1]);
                $vDate = $date[0].$date[1].(int)$date[2];

                for ($j = 1; $j <= count($jProblemDayTable); $j++) { 
                    if ( $desCambio[$j] == $jTDayTable[$i][0]) {  
                        $valueProblemTable[$vDate][$j] = $jTDayTable[$i][2];
                        $valueDuracionTable[$vDate][$j] = $jTDayTable[$i][2];
                        $totalProblema[$j] += $jTDayTable[$i][2]; 
                        $totalDia[$vDate] += $jTDayTable[$i][2];                         
                    } 
                }                 
                $total += $jTDayTable[$i][2]; 
                //TOTAL QUE SE MOSTRARA EN LA ESQUINA INFERIOR DERECHA                 
            }             
        } else { //CONSULTA DE BASE DE DATOS CUANDO LA OPCION ES PIEZAS
            for ($i = 0; $i < count($jTDayTable); $i++ ) {
                $date = explode("-", $jTDayTable[$i][1]); 
                $vDate = $date[0].$date[1].(int)$date[2]; 
                
                if ($i == 0) { 
                    $tc = 1; 
                    $countProblem = 1; 
                    $problema[$countProblem] = $jTDayTable[$i][0]; 
                    
                    //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA 
                    $totalProblema[$countProblem] = 0; 

                    //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA 
                    $cTC = tcLineaNoParte($line, $jTDayTable[$i][3]);
                    $valueProblemTable[$vDate][$countProblem] = ($jTDayTable[$i][2] *  60) / $cTC[0][0];

                    //ASIGNACION DE VALORES PARA SUBTOTALES DE PROBLEMA (|)
                    //PASA EL PRIMER VALOR QUE SE TIENE EN EL PRIMER ENCUENTRO DEL PROBLEMA
                    $totalProblema[$countProblem] = floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]);                 
                } else {
                    if ($problema[$countProblem] != $jTDayTable[$i][0] ){
                        $countProblem++;
                        $tc = 1;
                        $problema[$countProblem] = $jTDayTable[$i][0];

                        //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                        $totalProblema[$countProblem] = 0;

                        //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA
                        $cTC = tcLineaNoParte($line, $jTDayTable[$i][3]); 
                        $valueProblemTable[$vDate][$countProblem] = @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]);
                        //echo '<br> * ',$countProblem,', ',$vDate,' -> ',$jTDayTable[$i][2],', ',$jTDayTable[$i][3],', ', $cTC[0][0],', ', $valueProblemTable[$vDate][$countProblem];
                    } else {
                        $tc++;

                        //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA
                        $cTC = tcLineaNoParte($line, $jTDayTable[$i][3]); 
                        $valueProblemTable[$vDate][$countProblem] += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]); 
                    }
                    //SUBTOTALES DE CUADRO DE INFROMACION
                    //SUMATORIA DE TOTAL POR PROBLEMA (|)
                    $totalProblema[$countProblem] += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]);  
                    //SUMATORIA DE TOTAL POR DIA (_)
                    $totalDia[$vDate] += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]);
                    $valueDayT[$vDate] = $totalDia[$vDate];
                } 
                //PASO DE VALORES A VARIABLE DE PROBLEMA - DIA
                $valueProblemTable[$vDate][$countProblem] = @floor($valueProblemTable[$vDate][$countProblem]);

                //TOTAL DE LA INFORMACION ( VA EN LA ESQUINA INFERIOR DERECHA)
                $total += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]);
            }
        }        

        //DATOS PARA TAMAÑO DE TABLA 
        $dias = (strtotime($fIni)- strtotime($fFin))/86400;
        $dias = abs($dias); 
        $dias = floor($dias); 
        //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
        //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
        if ($dias > 11 ){
            $rowspan = 12;
        } else {
            //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
            //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS
            $rowspan = $dias+1;
        } 

        //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
        $countProblem = count($jProblemDayTable); 
        if($countProblem == 0) { 
            $problema[1] = "-"; 
            $totalProblema[1] = 0; 
            $countProblem = 1; 
            $desCambio[1] = ' - '; 
            for ($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                $date = explode("-", $i); 
                $vDate = $date[0].$date[1].(int)$date[2]; 
                $valueProblemTable[$vDate][1] = 0; 
            } 
        } 
        
    ?>
    
    <body>
        <div >     
            <div class="row" style="align-content: center; alignment-adjust: central; align-items: center; margin-top: 2.9%; " > 
                <div class="col-lg-3 col-md-3 col-sh-3" >
                    <button style=" margin-left: 5%;  height: 35px; width: 35px; background-color: transparent; border: 0; " onclick="location.href='./organizacional.php'" > 
                        <img src="../../imagenes/return.png" style="height: 100%; width: 100%; " > 
                    </button>
                </div>
                <div class="col-lg-6 col-md-6 col-sh-6" >
                    <?php switch ($tipo) { case 1: ?> 
                        <h5 align="center" >Indicador por duración</h5> 
                    <?php break;  case 2 : ?> 
                        <h5 align="center" >Indicador por frecuencia</h5> 
                    <?php break;  case 3 : ?> 
                        <h5 align="center" >Indicador por piezas</h5> 
                    <?php break; } ?> 
                </div>
                <div class="col-lg-3 col-md-3 col-sh-3" >
                    <button style=" margin-left: 85%; height: 35px; width: 35px; background-color: transparent; border: 0; " onclick="location.href='./calidad.php'" > 
                        <img src="../../imagenes/next.png" style="height: 100%; width: 100%; " > </button>
                </div>
            </div> 
            
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" style="margin-top: 0.5%" > 
                <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                    <script>
                        var chart = AmCharts.makeChart("jTMonth", {
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": [
                                <?php for ($i = 1; $i < 13; $i++ ) { ?>
                                {
                                    "country": "<?php echo $m[$i] ?>",
                                    "visits": <?php echo $mCant[$i] ?>,
                                    "meta": <?php echo $targetMonth[$i] ?>
                                },
                                <?php } ?>
                            ],
                            "graphs": [{
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "lineColor": "#02538b",
                                "type": "column",
                                "valueField": "visits"
                            } , {
                                "valueAxis": "v2",                                
                                "lineThickness": 2,
                                "lineColor": "#62cf73",
                                "type": "line",
                                "title": "Meta", 
                                "valueField": "meta",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],
                            "categoryField": "country",
                            "valueAxes": [{
                                "title": "Indicador"
                            }]
                        });
                    </script>
                </div>     

                <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                    <script>
                        var chart = AmCharts.makeChart("jTWeek", {
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": [
                                <?php 
                                if($sP > $sL){
                                    for ($i = $sP; $i <= $numSemanas; $i++) {                                
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    },
                                <?php } 
                                    for($i = 1; $i <= $sL; $i++) {                                
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    },
                                <?php } ?>                            
                                <?php } else { 
                                    for ($i = $sP; $i <= $sL; $i++) {                                
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    },
                                <?php } } ?>
                            ],
                            "graphs": [{
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "lineColor": "#02538b",
                                "type": "column",
                                "valueField": "visits"
                            }, {
                                "valueAxis": "v2",                                
                                "lineThickness": 2,
                                "lineColor": "#62cf73",
                                "type": "line",
                                "title": "Meta",
                                "valueField": "meta",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],
                            "categoryField": "country",
                            "valueAxes": [{
                                "title": "Inidicador"
                            }]
                        });
                    </script>                
                </div>    

                <!-- TABLA DE MESES -->
                <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                    <tr >
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >Mes</th>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblM[$i] ?></th>
                        <?php } ?>
                    </tr>
                    <tr>
                        <?php switch ($tipo) { case 1: ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/Indicador.png" style="height: 15px;"></td>
                        <?php break;  case 2 : ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/noFallas.png" style="height: 13px;"></td>
                        <?php break;  case 3 : ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/minutos.png" style="height: 13px;"></td>
                        <?php break;  } ?> 
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $mCant[$i] ?></td>      
                        <?php } ?>
                    </tr>                
                    <tr>
                        <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/meta.png" style="height: 9px;"></td>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $targetMonth[$i] ?></td>      
                        <?php } ?>
                    </tr>                
                </table> 

                <!-- TABLA DE SEMANA -->
                <table style="width: 35%; margin-left: 63%; margin-top: -35px; border: 1px solid #BABABA; ">
                    <tr>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php }} ?>                            
                    </tr>
                    <tr>
                        <?php switch ($tipo) { case 1: ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/Indicador.png" style="height: 15px;"></td>
                        <?php break;  case 2 : ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/noFallas.png" style="height: 15px;"></td>
                        <?php break;  case 3 : ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/minutos.png" style="height: 15px;"></td>
                        <?php break;  } ?> 
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                        <?php }} ?>  
                    </tr>                
                    <tr>
                        <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/meta.png" style="height: 9px; " ></td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $targetWeek[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $targetWeek[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $targetWeek[$i] ?></td>
                        <?php }} ?>  
                    </tr>                
                </table> 
                <br>

                <div id="grafDays" class="jidokaDay" >                
                    <script>
                        var chart = AmCharts.makeChart("grafDays", {
                            "type": "serial",
                            "theme": "light",
                            "dataDateFormat": "YYYY-MM-DD",
                            "precision": 2,
                            "dataProvider": [
                            <?php  
                                for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                            ?>    
                            {
                                <?php if ($valueDayT[$vDate] > $targetDayT[$vDate]){?>
                                    "lineColor": "#CD4C47",
                                <?php } else {?>
                                    "lineColor": "#02538b",
                                <?php }?>
                                "date": "<?php echo $fecha[$vDate]; ?>",                            
                                "production": <?php echo $valueDayT[$vDate]; ?>,
                                "meta": "<?php echo $targetDayT[$vDate]; ?>"                            
                            },
                            <?php } ?>
                            ],
                            "valueAxes": [{
                                "id": "v1",
                                "title": "Indicador",
                                "position": "left",
                                "autoGridCount": false,
                                "labelFunction": function(value) {
                                    return Math.round(value);
                                }
                            }],
                            "graphs": [{
                                "valueAxis": "v1",                                
                                "lineThickness": 2,
                                "lineColor": "#02538b",
                                "fillColorsField": "lineColor",
                                "lineColorField": "lineColor",
                                "type": "smoothedLine",
                                "title": "Indicacdor",
                                "valueField": "production",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }, {
                                "valueAxis": "v2",                                
                                "lineThickness": 2,
                                "lineColor": "#62cf73",
                                "type": "smoothedLine",
                                "title": "Meta",
                                "valueField": "meta",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],    
                            "chartScrollbar": {
                                "graph": "g1",
                                "oppositeAxis": false,
                                "offset": 30,
                                "scrollbarHeight": 40,
                                "backgroundAlpha": 0,
                                "selectedBackgroundAlpha": 0.1,
                                "selectedBackgroundColor": "#888888",
                                "graphFillAlpha": 0,
                                "graphLineAlpha": 0.5,
                                "selectedGraphFillAlpha": 0,
                                "selectedGraphLineAlpha": 1,
                                "autoGridCount": true,
                                "color": "#AAAAAA"
                            },
                            "chartCursor": {
                                "pan": true,
                                "valueLineEnabled": true,
                                "valueLineBalloonEnabled": true,
                                "cursorAlpha": 0,
                                "valueLineAlpha": 0.2
                            },
                            "categoryField": "date",
                            "categoryAxis": {
                                "parseDates": true,
                                "dashLength": 1,
                                "minorGridEnabled": true
                            },
                            "legend": {                            
                                "autoMargins": false,
                                "borderAlpha": 0.2,
                                "equalWidths": false,
                                "horizontalGap": 50,
                                "verticalGap": 15,
                                "markerSize": 10,
                                "useGraphSettings": true,
                                "valueAlign": "left",
                                "valueWidth": 0
                            },
                            "balloon": {
                                "borderThickness": 1,
                                "shadowAlpha": 0
                            },
                            "panelsSettings": {
                              "usePrefixes": true
                            },                        
                            "export": {
                                "enabled": false
                            }
                        });
                    </script>
                </div>  
            </div> 
            
            <!-- APARTADO PARA PICKERS --> 
            <div class="row" style="margin-top: 1.4%" >
                <div id="pnlPikers" class="col-lg-12 col-md-12 col-xs-12 col-sh-12" > 
                    <script>
                        $(function() {
                            $("#dateIni").datepicker({ 
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                maxDate: new Date(), 
                                //OYENTES DE LOS PICKERS 
                                onSelect: function(date) { 
                                    //Cuando se seleccione una fecha se cierra el panel
                                    $("#ui-datepicker-div").hide(); 
                                } 
                            }); 

                            $("#dateEnd").datepicker({
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                maxDate: new Date(),
                                //OYENTES DE LOS PICKERS 
                                onSelect: function ( date ) {
                                    //cuando se seleccione una fecha se cierra el panel
                                    $("#ui-datepicker-div").hide(); 
                                } 
                            }); 
                        }); 
                    </script> 
                    
                    <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado container container-fluid" >
                        <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2 contenidoCentrado" style="float: left;" > 
                            <select id="tipoDatos" style="border-color: #888;" class="form-control pickers" >
                                <option value="1" <?php if ($tipo == 1) { ?> selected <?php } ?> >Duración</option>
                                <option value="2" <?php if ($tipo == 2) { ?> selected <?php } ?> >Frecuencia</option>                             
                                <option value="3" <?php if ($tipo == 3) { ?> selected <?php } ?> >Piezas</option>
                            </select>
                        </div> 
                        <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3 contenidoCentrado input-group" style="float: left;" > 
                            <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo date("m/d/Y", strtotime($fIni)); ?>">
                            <span class="input-group-addon"> a </span> 
                            <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($fFin)); ?>">
                        </div>
                        <div class="col-xs-1 col-sh-1 col-md-1 col-lg-1 contenidoCentrado" style="float: left;" > 
                            <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0;" onclick="setTipoDatos()"> 
                                <img src="../../imagenes/confirmar.png"> 
                            </button> 
                        </div> 
                        <div class="col-xs-5 col-sh-5 col-md-5 col-lg-5 contenidoCentrado" style="float: left;"> 
                            <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0; margin-left: 90%; " onclick="setTipoDatos2()"> 
                                <img src="../../imagenes/bAll.png"> 
                            </button> 
                        </div> 
                    </div> 
                </div> 
            </div> 
            
            <div class="row " style="margin-top: 15px" > 
                <div id="holder-semple-1" style="margin-left: 1.2%; width: 98%" > 
                    <script id="tamplate-semple-1" type="text/mustache"> 
                        <table style="width:97%; margin-left: 1.5%; "class="inner-table"> 
                            <thead> 
                                <tr> 
                                    <td colspan="2"> </td> 
                                    <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                                    <td rowspan="2" align="center" >Total</td> 
                                </tr> 
                                <tr> 
                                    <td>&nbsp;</td> 
                                    <td align="center" >PROBLEMA</td>
                                    <?php for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ ?>
                                        <td align="center" ><?php echo $cadDate[$i]; ?></td>
                                    <?php } ?> 
                                </tr>
                            </thead>
                            <tbody>
                                <?php for($i = 1; $i <= $countProblem; $i++) { ?>
                                <tr >
                                    <td align="right" ><?php echo $i ?></td>
                                    <td ><?php echo $desCambio[$i]?></td>
                                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                                        $date = explode("-", $j); 
                                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                                    ?>
                                    <td align="center" ><?php echo floor($valueProblemTable[$vDate][$i]); ?></td>
                                    <?php if ( $j == $fFin ) {?>   
                                    <td align="center" ><?php echo $totalProblema[$i]; ?> </td> 
                                </tr >
                                <?php }}} ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="2">Sold Total</td>
                                    <?php for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                            $date = explode("-", $i); 
                                            $vDate = $date[0].$date[1].(int)$date[2]; 
                                    ?> 
                                        <td align="center" ><?php echo $totalDia[$vDate]; ?></td> 
                                    <?php if ($i == $fFin) { ?> 
                                        <!--CUADRE DE INFORMACION -->
                                        <td align="center" ><?php echo $total; ?></td>
                                    <?php }} ?>   
                                </tr>
                            </tfoot>
                        </table>
                    </script>
                </div> 
            </div> 
        </div> 
    </body> 
</HTML>
    
    