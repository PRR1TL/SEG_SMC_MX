<HTML>
    
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <!--<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>--> 
    
    <!-- LIBRERIAS Y CONFIGURACION PARA TABLA --> 
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script> 
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 4, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
        
        function setTipoDatos() { 
            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON
            var dato = document.getElementById("tipoDatos").value; 

            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes.php", 
                type: "post", 
                data: { tipoVista: 1, tipoDato: dato, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 
        
        var child;
        var childname = 'CHILD';
        function direccionExcel(){             
            child = window.open('tblExcel.php', childname); 
            let c= document.getElementById('txtChildName'); 
            c.value = childname;            
        }
        
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
    
    <!-- LIBRERIAS Y CONFIGURACION PARA PICKERS -->
    <!-- LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR -->     
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <?php 
        include '../../db/ServerFunctions.php';
        session_start();
        $_SESSION['tema'] = 'Organizacionales';
        $date = new DateTime;
        
        //RECIBE LOS VALORES DEL AJAX 
        $tema = "Organizacionales"; 
        
        $line = $_SESSION['linea']; 
        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes'];         
       
        $date->setISODate("$anio", 53);
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //INICIALIZAMOS VARIABLES PARA MES
        for($i = 1; $i <= 12; $i++){
            $mCant[$i] = 0;
            $targetMonth[$i] = 0;
        }  
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
                
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0; 
                $cwTec[$i] = 0; 
                $targetWeek[$i] = 0; 
            }
            
            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0; 
                $cwTec[$i] = 0; 
                $targetWeek[$i] = 0; 
            }            
        } else {
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0; 
                $cwTec[$i] = 0; 
                $targetWeek[$i] = 0; 
            }            
        }        
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        if ($sP == 0)
            $dSI = 7;          
        
        if ($sL == 0) 
            $dSL = 7;        
        
        $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio));
        $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));            
        
        //OBTENEMOS LA ULTIMA SEMANA DEL MES        
        $diaSemanaU = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

        //CONSULTA DE SCRAP(IFK, MISCELLANEOUS), SOLO POR CENTRO DE COSTOS   
        $mes2 = $mes-1; 
        $dI = 01;
        $dF = date("t",mktime(0,0,0,$mes,1,$anio)); 
        
        $m[1] = (string) "Jun";
        $m[2] = (string) "Feb";
        $m[3] = (string) "Mar";
        $m[4] = (string) "Apr";
        $m[5] = (string) "May";
        $m[6] = (string) "Jun";
        $m[7] = (string) "Jul";
        $m[8] = (string) "Aug";
        $m[9] = (string) "Sep";
        $m[10] = (string) "Oct";
        $m[11] = (string) "Nov";
        $m[12] = (string) "Dec";
        
        //INCIALIZAMOS LAS VARIABLES 
        $countProblem = 0;
        for($i = $_SESSION["FIni"]; $i <= date("Y-m-d", strtotime($_SESSION["FFin"] ."+ 1 days")); $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0; 
            $targetDayT[$vDate] = 0; 
        } 
        
        $tipo = $_SESSION['tipoDato']; 
        
        switch ($tipo) { 
            case 1: //DURACION 
                $jTDay = jidokaDia($tema, $line, $anio); 
                $jTDayTable = tablaJidokaNOPlaneados($tema, $line, $_SESSION["FIni"], $_SESSION["FFin"] ); 
                $jTWeek = jidokaSemana($tema, $line, $fP, $fL); 
                $jTMonth = jidokaMonth($tema, $line, $anio); 
                break; 
            case 2: //FRECUENCIA 
                $jTDay = jidokaDiaF($tema, $line, $anio); 
                $jTDayTable = tablaJidokaNOPlaneadosF($tema, $line, $_SESSION["FIni"], $_SESSION["FFin"] ); 
                $jTWeek = jidokaSemanaF($tema, $line, $fP, $fL); 
                $jTMonth = jidokaMonthF($tema, $line, $anio); 
                break;
            case 3: //PIEZAS //$_SESSION["FFin"] =  date("Y-m-d", strtotime($_SESSION["FIni"] ."- 1 day")); 
                $jTDay = tablaJidokaNOPlaneados($tema, $line, $_SESSION["FIni"], $_SESSION["FFin"] ); 
                $jTDayTable = tablaJidokaNOPlaneados($tema, $line, $_SESSION["FIni"], $_SESSION["FFin"] ); 
                $jTWeek = jidokaSemana($tema, $line, $fP, $fL); 
                $jTMonth = jidokaMonth($tema, $line, $anio); 
                break;            
        } 
        
        for ($i = $_SESSION["FIni"]; $i <= date("Y-m-d", strtotime($_SESSION['FFin'] ."+ 1 days")); $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            //$countX++;
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2];         
            switch($date[1]){
                case 1:
                    $cadDate[$i] = 'Jan&nbsp;'.$date[2];
                    break;
                case 2:
                    $cadDate[$i] = 'Feb&nbsp;'.$date[2];
                    break;
                case 3:
                    $cadDate[$i] = 'Mar&nbsp;'.$date[2];
                    break;
                case 4:
                    $cadDate[$i] = 'Apr&nbsp;'.$date[2];
                    break;
                case 5:
                    $cadDate[$i] = 'May&nbsp;'.$date[2];
                    break;
                case 6:
                    $cadDate[$i] = 'Jun&nbsp;'.$date[2];
                    break;
                case 7:
                    $cadDate[$i] = 'Jul&nbsp;'.$date[2];
                    break;
                case 8:
                    $cadDate[$i] = 'Aug&nbsp;'.$date[2];
                    break;
                case 9:
                    $cadDate[$i] = 'Sep&nbsp;'.$date[2];
                    break;
                case 10:
                    $cadDate[$i] = 'Oct&nbsp;'.$date[2];
                    break;
                case 11:
                    $cadDate[$i] = 'Nov&nbsp;'.$date[2];
                    break;
                case 12:
                    $cadDate[$i] = 'Dec&nbsp;'.$date[2];
                    break;
            }

            //INICIALIZAMOS LAS VARIABLES DE LA TABLA
            if (count($jTDayTable) > 0 ){
                for ($j = 1; $j <= count($jTDayTable); $j++ ) {
                    $valueProblemTable[$vDate][$j] = 0; 
                    //VARIABLE DEFINIDA SOLO PARA CUANDO EL TIPO DE DATO SEA PIEZAS
                    $valueDuracionTable[$vDate][$j] = 0;
                    $totalProblema[$j] = 0; 
                } 
            } else { 
                $valueProblemTable[$vDate][1] = 0; 
            } 

            //INICIALIZAMOS LAS VARIABLES PARA LOS SUBTOTALES DIA(_)            
            $totalDia[$vDate] = 0; 
            $targetDayT[$vDate] = 0; 
        } 
        
        /************************ DIA **********************/
        for ($i = 0; $i < count($jTDay); $i++ ){
            $date = explode("-", $jTDay[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2];         
            $valueDayT[$vDate] = $jTDay[$i][1]; 
            //echo '<br>',$i, ' (',$vDate,') -> ', $date[0], ', ',$date[1],', ',(int)$date[2];
        } 
        
        #TARGET
        $ctargetDay = cTargetTecDaily($line, $_SESSION["FIni"], $_SESSION["FFin"]);
        for ($i = 0; $i < count($ctargetDay) ; $i++) { 
            $date = explode("-", $ctargetDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $targetDayT[$vDate] = $ctargetDay[$i][1]; 
        } 
        
        /********************* SEMANA *************************/
        for($i = 0; $i < count($jTWeek); $i++ ){
            $sem = $jTWeek[$i][0];
            $cwTec[$sem] = $jTWeek[$i][1];
        } 
        
        #TARGET 
        $cTargetWeek = cTargetTecWeekly($line, $anio, $mes);
        for ($i = 0; $i < count($cTargetWeek); $i++){
            $s = $cTargetWeek[$i][0];
            $targetWeek[$s] = $cTargetWeek[$i][1];
        }
        
        /********************** MES ***********************/
        for($i = 0 ; $i < count($jTMonth); $i++){
            $nMes = $jTMonth[$i][0];            
            $mCant[$nMes] = $jTMonth[$i][1];    
        }         
        
        #TARGET
        $cTargetMonth = cTargetTecMonthy($line, $anio);
        for ($i = 0; $i < count($cTargetMonth); $i++ ){
            $ms = $cTargetMonth[$i][0];
            $targetMonth[$ms] = $cTargetMonth[$i][1];
        }
        
        //INCIALIZAMOS LAS VARIABLES
        //TABLA 
        $countX = 1;
        $total = 0;
        $sumProblemas = 0;
        
        //EVALUACION PARA EL TIPO DE DATO QUE SE TIENE  
        if($tipo != 3 ) {     
            for ($i = 0; $i < count($jTDayTable); $i++ ) { 
                $date = explode("-", $jTDayTable[$i][0]); 
                $vDate = $date[0].$date[1].(int)$date[2]; 
                
                if ($i == 0) { 
                    $countProblem = 1; 
                    $operacion[$countProblem] = $jTDayTable[$i][1]; 
                    $code[$countProblem] = $jTDayTable[$i][2]; 
                    $problema[$countProblem] = $jTDayTable[$i][3]; 
                     
                    //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                    $totalProblema[$countProblem] = 0;                     
                }  else { 
                    
                    if ($problema[$countProblem] != $jTDayTable[$i][3] || $code[$countProblem] != $jTDayTable[$i][2] || $operacion[$countProblem] != $jTDayTable[$i][1] ) { 
                        //echo "<br>",$problema[$countProblem],', ', $jTDayTable[$i][3],', ', $code[$countProblem],', ', $jTDayTable[$i][2];
                        $countProblem++; 
                        $operacion[$countProblem] = $jTDayTable[$i][1]; 
                        $code[$countProblem] = $jTDayTable[$i][2]; 
                        $problema[$countProblem] = $jTDayTable[$i][3];
                        // "<br>",$countProblem,'; ',$code[$countProblem],', ',$problema[$countProblem];
                    } 
                }   
                
                //echo "<br>* ", $jTDayTable[$i][1],', ',$jTDayTable[$i][2]; 
                
                //PASO DE VALORES A VARIABLE DE SUBTOTAL PROBLEMA(|) 
                $totalProblema[$countProblem] += $jTDayTable[$i][4];

                //PASO DE VALORES A VARIABLE DE PROBLEMA DIA
                $valueProblemTable[$vDate][$countProblem] += $jTDayTable[$i][4]; 

                //ASIGNACION DE VALORES PARA SUBTOTALES DE DIA(_)
                $totalDia[$vDate] += $jTDayTable[$i][4];

                //TOTAL QUE SE MOSTRARA EN LA ESQUINA INFERIOR DERECHA
                $total += $jTDayTable[$i][4];              
            }
        } else { //CONSULTA DE BASE DE DATOS CUANDO LA OPCION ES PIEZAS    
            for ($i = 0; $i < count($jTDayTable); $i++ ) { 
                $date = explode("-", $jTDayTable[$i][0]); 
                $vDate = $date[0].$date[1].(int)$date[2]; 
                
                if ($i == 0) { 
                    $tc = 1; 
                    $countProblem = 1; 
                    $problema[$countProblem] = $jTDayTable[$i][1];                     

                    //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA 
                    $totalProblema[$countProblem] = 0; 

                    //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA 
                    $cTC = tcLineaNoParte($line, $jTDayTable[$i][3]);
                    $valueProblemTable[$vDate][$countProblem] += ($jTDayTable[$i][2] *  60) / $cTC[0][0];
                    
                    //ASIGNACION DE VALORES PARA SUBTOTALES DE PROBLEMA (|)
                    //PASA EL PRIMER VALOR QUE SE TIENE EN EL PRIMER ENCUENTRO DEL PROBLEMA
                    $totalProblema[$countProblem] = @floor($valueProblemTable[$vDate][$countProblem]);
                } else { 
                    if ($problema[$countProblem] != $jTDayTable[$i][1] ){
                        $countProblem++;
                        $tc = 1;
                        $problema[$countProblem] = $jTDayTable[$i][1];

                        //INICIALIZACION DE VARIABLE PARA SUBTOTAL POR PROBLEMA
                        //$totalProblema[$countProblem] = 0;

                        //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA
                        $cTC = tcLineaNoParte($line, $jTDayTable[$i][3]); 
                        $valueProblemTable[$vDate][$countProblem] += @(($jTDayTable[$i][2] *  60) / $cTC[0][0]);                        
                    
                        //SUBTOTALES DE CUADRO DE INFROMACION 
                        //SUMATORIA DE TOTAL POR PROBLEMA (|) 
                        $totalProblema[$countProblem] = @floor($valueProblemTable[$vDate][$countProblem]); 
                    } else {
                        $tc++;
                        
                        //OBTENEMOS EL TC DE NUMERO DE PARTE DE ESE PROBLEMA 
                        $cTC = tcLineaNoParte($line, $jTDayTable[$i][3]);
                        $valueProblemTable[$vDate][$countProblem] += @(($jTDayTable[$i][2] *  60) / $cTC[0][0]);                        
                        
                        //SUBTOTALES DE CUADRO DE INFROMACION 
                        //SUMATORIA DE TOTAL POR PROBLEMA (|) 
                        $totalProblema[$countProblem] += @floor($valueProblemTable[$vDate][$countProblem]); 
                    }                     
                    //SUMATORIA DE TOTAL POR DIA (_) 
                    //$totalDia[$vDate] += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]);                     
                } 
                //echo "<br>+ ", $jTDayTable[$i][1]; 
                //SUMATORIA DE TOTAL POR DIA (_) 
                $valueDayT[$vDate] += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]); 
                $totalDia[$vDate] += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]); 
                
                //TOTAL DE LA INFORMACION ( VA EN LA ESQUINA INFERIOR DERECHA)
                $total += @floor(($jTDayTable[$i][2] *  60) / $cTC[0][0]);
            }
        } 
        
        //DATOS PARA TAMAÑO DE TABLA  
        //$dias = (strtotime($_SESSION["FIni"])- strtotime($_SESSION["FFin"]))/86400;
        $dias = (strtotime($_SESSION["FIni"])- strtotime(date("Y-m-d", strtotime($_SESSION['FFin'] ."+ 1 days"))))/86400;
        $dias = abs($dias); 
        $dias = floor($dias)+1; 
        //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
        //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS 
        if ($dias > 11 ) { 
            $rowspan = 12;
        } else { 
            //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
            //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS 
            $rowspan = $dias-1;
        } 

        //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
        if($countProblem == 0) { 
            $countProblem = 1;
            $code[1] = "-"; 
            $operacion[1] = "-"; 
            $problema[1] = "-";
            $totalProblema[1] = 0;
        } 
        
        //$listOperations = operationsLine($_SESSION['linea']);
        
    ?> 
    <body> 
        <div>             
            <div class="row" style="align-content: center; alignment-adjust: central; align-items: center; margin-top: 2.9%; " > 
                <div class="col-lg-3 col-md-3 col-sh-3" >
                    <button style=" margin-left: 5%;  height: 35px; width: 35px; background-color: transparent; border: 0; " onclick="location.href='./calidad.php'" > 
                        <img src="../../imagenes/return.png" style="height: 100%; width: 100%; " > 
                    </button>
                </div>
                <div class="col-lg-6 col-md-6 col-sh-6" >
                    <?php switch ($tipo) { case 1: ?> 
                        <h5 align="center" >Indicador por duración</h5> 
                    <?php break;  case 2 : ?> 
                        <h5 align="center" >Indicador por frecuencia</h5> 
                    <?php break;  case 3 : ?> 
                        <h5 align="center" >Indicador por piezas</h5> 
                    <?php break; } ?> 
                </div>
                <div class="col-lg-3 col-md-3 col-sh-3" >
                    <button style=" margin-left: 85%; height: 35px; width: 35px; background-color: transparent; border: 0; " onclick="location.href='./organizacional.php'" > 
                        <img src="../../imagenes/next.png" style="height: 100%; width: 100%; " > </button>
                </div>
            </div> 
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" style="margin-top: 0.5%" > 
                <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                    <script>
                        var chart = AmCharts.makeChart("jTMonth", {
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": [
                                <?php for ($i = 1; $i < 13; $i++ ) { ?>
                                {
                                    "country": "<?php echo $m[$i]; ?>",
                                    "visits": <?php echo $mCant[$i]; ?>,
                                    "meta": <?php echo $targetMonth[$i]; ?>
                                },
                                <?php } ?>
                            ],
                            "graphs": [{
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "lineColor": "#02538b",
                                "type": "column",
                                "valueField": "visits"
                            }, { 
                                "valueAxis": "v2", 
                                "lineThickness": 2, 
                                "lineColor": "#62cf73", 
                                "type": "line", 
                                "title": "Meta", 
                                "valueField": "meta", 
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],
                            "categoryField": "country",
                            "valueAxes": [{
                                "title": "Indicador"
                            }]
                        });
                    </script>
                </div>     
                
                <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                    <script>
                        var chart = AmCharts.makeChart("jTWeek", {
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": [
                                <?php 
                                if($sP > $sL){
                                    for ($i = $sP; $i <= $numSemanas; $i++) { 
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    },
                                <?php } 
                                    for($i = 1; $i <= $sL; $i++) { 
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    },
                                <?php } ?> 
                                <?php } else { 
                                    for ($i = $sP; $i <= $sL; $i++) { 
                                ?> 
                                    { 
                                        "country": "<?php echo 'CW '.$i ?>", 
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    }, 
                                <?php } } ?> 
                            ], 
                            "graphs": [{ 
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "lineColor": "#02538b",
                                "type": "column",
                                "valueField": "visits"
                            }, { 
                                "valueAxis": "v2",                                 
                                "lineThickness": 2, 
                                "lineColor": "#62cf73", 
                                "type": "line", 
                                "title": "Meta", 
                                "valueField": "meta", 
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],
                            "categoryField": "country",
                            "valueAxes": [{
                                "title": "Inidicador"
                            }]
                        });
                    </script>                
                </div>    
                
                <!-- TABLA DE MESES -->
                <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                    <tr >
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >Mes</th>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $m[$i] ?></th>
                        <?php } ?>
                    </tr>
                    <tr>
                        <?php switch ($tipo) { case 1: ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/Indicador.png" style="height: 15px;"></td>
                        <?php break;  case 2 : ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/noFallas.png" style="height: 13px;"></td>
                        <?php break;  case 3 : ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/minutos.png" style="height: 13px;"></td>
                        <?php break;  } ?> 
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $mCant[$i] ?></td>      
                        <?php } ?>
                    </tr>                
                    <tr>
                        <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/meta.png" style="height: 9px;"></td>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $targetMonth[$i] ?></td>      
                        <?php } ?>
                    </tr>                
                </table> 
                
                <!-- TABLA DE SEMANA -->
                <table style="width: 35%; margin-left: 63%; margin-top: -51px; border: 1px solid #BABABA; ">
                    <tr>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php }} ?>                            
                    </tr>
                    <tr>
                        <?php switch ($tipo) { case 1: ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/Indicador.png" style="height: 15px;"></td>
                        <?php break;  case 2 : ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/noFallas.png" style="height: 15px;"></td>
                        <?php break;  case 3 : ?>
                            <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/minutos.png" style="height: 15px;"></td>
                        <?php break;  } ?> 
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                        <?php }} ?>  
                    </tr>                
                    <tr>
                        <td align="center" style=" margin-left: 2%; border: 1px solid #BABABA;"><img src="../../imagenes/nomenclaturas/meta.png" style="height: 9px; " ></td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $targetWeek[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $targetWeek[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $targetWeek[$i] ?></td>
                        <?php }} ?>  
                    </tr>                
                </table> 
                <br>
                
                <div id="grafDays" class="jidokaDay" >                
                    <script>
                        var chart = AmCharts.makeChart("grafDays", {
                            "type": "serial",
                            "theme": "light",
                            "dataDateFormat": "YYYY-MM-DD",
                            "precision": 0,
                            "dataProvider": [
                            <?php  
                                for($i = $_SESSION["FIni"]; $i <= $_SESSION["FFin"]; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                            ?>    
                            {
                                <?php if ($valueDayT[$vDate] > $targetDayT[$vDate]){?>
                                    "lineColor": "#CD4C47",
                                <?php } else {?>
                                    "lineColor": "#02538b",
                                <?php }?>
                                "date": "<?php echo "$fecha[$vDate]" ?>",                            
                                "production": <?php echo $valueDayT[$vDate]; ?>,
                                "meta": "<?php echo $targetDayT[$vDate]; ?>"                            
                            },
                            <?php } ?>
                            ],
                            "valueAxes": [{
                                "id": "v1",
                                "title": "Indicador",
                                "position": "left",
                                "autoGridCount": false,
                                "labelFunction": function(value) {
                                    return Math.round(value);
                                }
                            }],
                            "graphs": [{
                                "valueAxis": "v1", 
                                "lineThickness": 2,
                                "lineColor": "#02538b",
                                "fillColorsField": "lineColor",
                                "lineColorField": "lineColor",
                                "type": "line",
                                "title": "Indicador", 
                                "valueField": "production",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }, {
                                "valueAxis": "v2", 
                                "lineThickness": 2,
                                "lineColor": "#62cf73",
                                "type": "line",
                                "title": "Meta", 
                                "valueField": "meta",
                                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                            }],    
                            "chartScrollbar": {
                                "graph": "g1",
                                "oppositeAxis": false,
                                "offset": 30,
                                "scrollbarHeight": 40,
                                "backgroundAlpha": 0,
                                "selectedBackgroundAlpha": 0.1,
                                "selectedBackgroundColor": "#888888",
                                "graphFillAlpha": 0,
                                "graphLineAlpha": 0.5,
                                "selectedGraphFillAlpha": 0,
                                "selectedGraphLineAlpha": 1,
                                "autoGridCount": true,
                                "color": "#AAAAAA"
                            },
                            "chartCursor": {
                                "pan": true,
                                "valueLineEnabled": true,
                                "valueLineBalloonEnabled": true,
                                "cursorAlpha": 0,
                                "valueLineAlpha": 0.2
                            },
                            "categoryField": "date",
                            "categoryAxis": {
                                "parseDates": true,
                                "dashLength": 1,
                                "minorGridEnabled": true
                            },
                            "legend": {                            
                                "autoMargins": false,
                                "borderAlpha": 0.2,
                                "equalWidths": false,
                                "horizontalGap": 50,
                                "verticalGap": 15,
                                "markerSize": 10,
                                "useGraphSettings": true,
                                "valueAlign": "left",
                                "valueWidth": 0
                            },
                            "balloon": {
                                "borderThickness": 1,
                                "shadowAlpha": 0
                            },
                            "panelsSettings": {
                              "usePrefixes": true
                            },                        
                            "export": {
                                "enabled": false
                            }
                        });
                    </script> 
                </div>  
            </div> 
            
            <!-- APARTADO PARA PICKERS --> 
            <div class="row" style="margin-top: 1.4%" >
                <div id="pnlPikers" class="col-lg-12 col-md-12 col-xs-12 col-sh-12" >  
                    <script>
                        $(function() { 
                            $("#dateIni").datepicker({ 
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                maxDate: new Date(), 
                                //OYENTES DE LOS PICKERS 
                                onSelect: function(date) { 
                                    //Cuando se seleccione una fecha se cierra el panel
                                    $("#ui-datepicker-div").hide(); 
                                } 
                            }); 

                            $("#dateEnd").datepicker({ 
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                maxDate: new Date(), 
                                //OYENTES DE LOS PICKERS 
                                onSelect: function ( date ) { 
                                    //cuando se seleccione una fecha se cierra el panel 
                                    $("#ui-datepicker-div").hide(); 
                                } 
                            }); 
                        }); 
                    </script> 
                    
                    <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado container container-fluid" >
                        <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2 row" style="float: left;" >                          
                            <select id="tipoDatos" style="border-color: #888; " class="form-control " style=" float: left;" >
                                <option value="1" <?php if ($tipo == 1) { ?> selected <?php } ?> >Duración</option>
                                <option value="2" <?php if ($tipo == 2) { ?> selected <?php } ?> >Frecuencia</option>                             
                                <option value="3" <?php if ($tipo == 3) { ?> selected <?php } ?> >Piezas</option>
                            </select> 
                        </div> 
                        <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3 contenidoCentrado input-group" style="float: left;" > 
                            <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo date("m/d/Y", strtotime($_SESSION["FIni"])); ?>" >
                            <span class="input-group-addon"> a </span> 
                            <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($_SESSION["FFin"])); ?>" >
                        </div>
                        <div class="col-xs-1 col-sh-1 col-md-1 col-lg-1 contenidoCentrado" style="float: left;" > 
                            <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0;" onclick="setTipoDatos()" > 
                                <img src="../../imagenes/confirmar.png"> 
                            </button> 
                        </div> 
                        
                        <div class="col-xs-5 col-sh-5 col-md-5 col-lg-5 contenidoCentrado" style="float: left;" > 
                            <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0; margin-left: 90%; " onclick="direccionExcel()" > 
                                <img src="../../imagenes/buttons/dXLS.png" style=" width: 40px; height: 40px; "> 
                            </button> 
                        </div> 
                    </div> 
                </div> 
            </div> 
            
            <div class="row" style="margin-top: 25px;" > 
                <div id="holder-semple-1" style="margin-left:1% ; width: 98%;" > 
                    <script id="tamplate-semple-1" type="text/mustache" > 
                        <table id='tblData' style="width:97%; margin-left: 1.5%; "class="inner-table" > 
                            <thead> 
                                <tr> 
                                    <td colspan="4"> </td> 
                                    <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                                    <td rowspan="2" align="center" >Total</td> 
                                </tr> 
                                <tr> 
                                    <td>&nbsp;</td> 
                                    <td>Code</td> 
                                    <td>Operacion</td>                                     
                                    <td align="center" >Problema</td>
                                    <?php for($i = $_SESSION["FIni"]; $i < date("Y-m-d", strtotime($_SESSION['FFin'] ."+ 1 days")); $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ ?>
                                        <td align="center" ><?php echo $cadDate[$i]; ?></td>
                                    <?php } ?> 
                                </tr>
                            </thead>
                            <tbody>
                                <?php for($i = 1; $i <= $countProblem; $i++) { ?>
                                <tr >
                                    <td align="right" ><?php echo $i ?></td> 
                                    <td ><?php echo $code[$i]?></td>
                                    <td ><?php echo $operacion[$i]?></td>                                    
                                    <td ><?php echo $problema[$i]?></td>
                                    <?php for($j = $_SESSION["FIni"]; $j < date("Y-m-d", strtotime($_SESSION['FFin'] ."+ 1 days")); $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                                        $date = explode("-", $j); 
                                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                                    ?>
                                    <td align="center" ><?php echo floor($valueProblemTable[$vDate][$i]); ?></td>
                                    <?php if ( $j == $_SESSION["FFin"] ) {?>   
                                    <td align="center" ><?php echo $totalProblema[$i]; ?> </td>    
                                </tr >
                                <?php }}} ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="4">Sold Total</td>
                                    <?php for($i = $_SESSION["FIni"]; $i < date("Y-m-d", strtotime($_SESSION['FFin'] ."+ 1 days")); $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                            $date = explode("-", $i); 
                                            $vDate = $date[0].$date[1].(int)$date[2];
                                    ?>
                                        <td align="center" ><?php echo $totalDia[$vDate]; ?></td>
                                    <?php if ($i == $_SESSION["FFin"]) { ?>                            
                                        <!--CUADRE DE INFORMACION -->
                                        <td align="center" ><?php echo $total; ?></td>
                                    <?php }} ?> 
                                </tr> 
                            </tfoot> 
                        </table> 
                    </script>
                </div>                 
            </div> 
        </div> 
    </body>