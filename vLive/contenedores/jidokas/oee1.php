<HTML>
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>    
    
    <?php 
        include '../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
        //RECIBE LOS VALORES DEL AJAX         
        if (isset($_SESSION['linea'])){
            $line = $_SESSION['linea'];
            $anio = $_SESSION['anio'];
            $mes = $_SESSION['mes'];
            if ($mes < 10) {
                $mes = (string) '0'.$mes;
            }
        } else {
            $line = 'L001';
            $anio = date('Y');
            $mes = date('m');
        } 
        
        $date->setISODate("$anio", 53);
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //INICIALIZAMOS VARIABLES PARA MES
        for($i = 1; $i <= 12; $i++){
            $mCant[$i] = 0;
        }  
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0;
                $cwProd[$i] = 0;
            }
            
            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwProd[$i] = 0;
            }            
        } else {
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $cwProd[$i] = 0;
            }            
        }        
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        if ($sP == 0)
            $dSI = 7;          
        
        if ($sL == 0) 
            $dSL = 7;        
        
        $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio));
        $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));            
        
        //OBTENEMOS LA ULTIMA SEMANA DEL MES        
        $diaSemanaU = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

        //CONSULTA DE SCRAP(IFK, MISCELLANEOUS), SOLO POR CENTRO DE COSTOS   
        $mes2 = $mes-1; 
        $dI = 01;
        $dF = date("t",mktime(0,0,0,$mes,1,$anio));

        $fecha1 = $anio.'-01-01';
        $fecha2 = date("Y-m-d"); 

        $dateI = explode("-", $fecha1); 
        $dateF = explode("-", $fecha2);                
        
        //INCIALIZAMOS LAS VARIABLES
        //TABLA
        $countProblem = 0;
        $problema[0] = "";
        for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0;
            $targetDayT[$vDate] = 80; 
        } 
         
        //DATOS SEMANALES
        //PIEZAS PRODUCIDAS
        $datPzasProdW = pzasRealesWeek($line, $anio, $mes); 
        $id = date("W",mktime(0,0,0,$mes,01,$anio));
        for ($i = 0; $i < count($datPzasProdW); $i++){
            $s = $datPzasProdW[$i][0];
            if($s == $id){
                $pzasProdW[$i] = $datPzasProdW[$i][1];
            } else {
                $dif = (int) ($s - $id) + $i;            
                $pzasProdW[$dif] = $datPzasProdW[$i][1];
            }
            $id++;
        }
        
        //PIEZAS ESPERADAS POR SEMANA
        $datPzasEspProdW = pzasEsperadasWeek($line, $anio, $mes); 
        $id = date("W",mktime(0,0,0,$mes,01,$anio));
        //echo count($datPzasEspProdW);
        for ($i = 0; $i < count($datPzasEspProdW); $i++){
            $s = $datPzasEspProdW[$i][0];
            
            if($s == $id){   
                $pzasEspW[$i] = $datPzasEspProdW[$i][1];   
                //OBTENEMOS EL % PRODUCCION DE CADA SEMANA 

                $percentProdW[$i] =  @round(($pzasProdW[$i]*100)/$pzasEspW[$i],2);
                $cwProd[$s] = @round(($pzasProdW[$i]*100)/$pzasEspW[$i],2);
                //echo '<br> 2. ',$s,': ',$cwProd[$s];
                if(! $percentProdW[$i] > 0 ){
                    $percentProdW[$i] = 0;
                    $percentOrgW[$i] = 0;
                    $percentCaliW[$i] = 0;
                    $percentCamW[$i] = 0;
                    $percentTecW[$i] = 0;
                } else {
                    $percentResW[$i] = 100 - $percentProdW[$i]; 

                    //PORCENTAJES PARA AREAS
                    $percentCaliW[$i] = @round(($minCaliW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
                    $percentOrgW[$i] = @round(($minOrgW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
                    $percentTecW[$i] = @round(($minTecW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
                    $percentCamW[$i] = @round(($minCamW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
                    $percentTFalW[$i] = @round(($minTFalW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
                }

                //PORCENTAJES PARA AREAS 
            } else {
                $dif = (int) ($s - $id) + $i;

                $pzasEspW[$dif] = $datPzasEspProdW[$i][1];
                //OBTENEMOS EL % PRODUCCION DE CADA SEMANA 

                $percentProdW[$dif] =  @(($pzasProdW[$dif]*100)/$pzasEspW[$dif]);
                $cwProd[$s] =  @(($pzasProdW[$dif]*100)/$pzasEspW[$dif]);
                //echo '<br> 3. ',$s,': ',$cwProd[$s];
                if(! $percentProdW[$dif] > 0 ){
                    $percentProdW[$dif] = 0;
                    $percentOrgW[$dif] = 0;
                    $percentCaliW[$dif] = 0;
                    $percentCamW[$dif] = 0;
                    $percentTecW[$dif] = 0;
                } else {
                    $percentResW[$dif] = 100 - $percentProdW[$dif];

                    //PORCENTAJES PARA AREAS
                    $percentCaliW[$dif] = @(($minCaliW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
                    $percentOrgW[$dif] = @(($minOrgW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
                    $percentTecW[$dif] = @(($minTecW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
                    $percentCamW[$dif] = @(($minCamW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
                    $percentTFalW[$dif] = @(($minTFalW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
                } 
            }            
            $id++;
        }   
        
        $cPercentTopicMonthly = cPercentTopicMonthly($line, $anio);
        for ($i = 0; $i < count($cPercentTopicMonthly); $i++){
            $m = $cPercentTopicMonthly[$i][0];        
            $mCant[$m] = @round($cPercentTopicMonthly[$i][1],2);
            $percentTecM[$m] = @round($cPercentTopicMonthly[$i][2],2);
            $percentOrgM[$m] = @round($cPercentTopicMonthly[$i][3],2);
            $percentCaliM[$m] = @round($cPercentTopicMonthly[$i][4],2);
            $percentCamM[$m] = @round($cPercentTopicMonthly[$i][5],2);
            $percentTFalM[$m] = @round($cPercentTopicMonthly[$i][6],2);
        }

        if ($line == 'L001' && $anio == '2019') {
            $mCant[1] = @round(77.8,2);
            $percentTecM[1] = @round(9.4,2);
            $percentOrgM[1] = @round(5.6,2);
            $percentCaliM[1] = @round(0,2);
            $percentCamM[1] = @round(5.2,2);
            $percentTFalM[1] = @round(0,2);
        } else if ($line == 'L002' && $anio == '2019' ) {
            $mCant[1] = @round(84,2);
            $percentTecM[1] = @round(11,2);
            $percentOrgM[1] = @round(3,2);
            $percentCaliM[1] = @round(0,2);
            $percentCamM[1] = @round(3,2);
            $percentTFalM[1] = @round(0,2);
        } else if ($line == 'L003' && $anio == '2019') {
            $mCant[1] = @round(83,2);
            $percentTecM[1] = @round(7,2);
            $percentOrgM[1] = @round(2,2);
            $percentCaliM[1] = @round(0,2);
            $percentCamM[1] = @round(8,2);
            $percentTFalM[1] = @round(0,2);
        } 
//        //MES
//        for($i = 0 ; $i < count($jTMonth); $i++){
//            $nMes = $jTMonth[$i][0];            
//            $mCant[$nMes] = $jTMonth[$i][1];    
//        }
        
        /********************** DIARIO ************************************/

        //CONSULTA DE SCRAP(IFK, MISCELLANEOUS), SOLO POR CENTRO DE COSTOS 
        $fecha1 = $anio.'-01-01';
        $fecha2 = date('Y-m-d'); 

        for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){        
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $vF[$vDate] = $i;

            //SCRAP
            $fecha[$vDate] = $i;
            $scrapIfkDay[$vDate] = 0;
            $scrapMiscDay[$vDate] = 0; 

            //PRODUCCION
            $pzasEntrega[$vDate] = 0;
            $acumReal[$vDate] = 0;        
            $acumPlan[$vDate] = 0; 
            $percentProduction[$vDate] = 0;

            /*******************JIDOKAS**********************/
            //TECNICAS
            $valueDayT[$vDate] = 0;
            $targetDayT[$vDate] = 80;

            //ORGANIZACIONALES
            $valueDayO[$vDate] = 0;
            $targetDayO[$vDate] = 70;

            //CAMBIO DE MODELO
            $valueDayC[$vDate] = 0;
            $targetDayC[$vDate] = 80;

            //CALIDAD
            $valueDayQ[$vDate] = 0;
            $targetDayQ[$vDate] = 0;
        }    

        $mesActual = date("M");
        $anioActual = date("Y");
        $fecha = $mesActual.", ".$anioActual;
    
    
        $oeeProductionDay = oeeProductionDay($line,$anio); 
        for($i = 0; $i < count($oeeProductionDay); $i++) {        
            $date = explode("-", $oeeProductionDay[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2];         
            $percentProduction[$vDate] = round($oeeProductionDay[$i][1],3);                            
        } 
    /**********************************************************************/    
    
    $lblM[1] = (string) "Jun";
    $lblM[2] = (string) "Feb";
    $lblM[3] = (string) "Mar";
    $lblM[4] = (string) "Apr";
    $lblM[5] = (string) "May";
    $lblM[6] = (string) "Jun";
    $lblM[7] = (string) "Jul";
    $lblM[8] = (string) "Aug";
    $lblM[9] = (string) "Sep";
    $lblM[10] = (string) "Oct";
    $lblM[11] = (string) "Nov";
    $lblM[12] = (string) "Dec";     
        
    ?>
    
    <head>       
	<script type="text/javascript">               
            window.onload = function () {
                var gridViewScroll = new GridViewScroll({
                    elementID : "gvMain",
                    freezeColumn : true,
                    freezeFooter : true,
                    freezeColumnCssClass : "GridViewScrollItemFreeze",
                    freezeFooterCssClass : "GridViewScrollFooterFreeze"
                });
                gridViewScroll.enhance();
            }
	</script>    
    </head>
   
    <body>
        <div> 
            <br>
            <?php //echo $tipo,'<br>'; ?>
            
<!--            <div style="align-content: center; alignment-adjust: central; align-items: center; margin-top: -1% ">
                <?php //switch ($tipo) { case 1: ?>
                    <h5 align="center" >Indicadores por duración</h5>
                <?php //break;  case 2 : ?>
                    <h5 align="center" >Indicadores por frecuencia</h5>
                <?php //break;  case 3 : ?>
                    <h5 align="center" >Indicaros por piezas</h5>
                <?php //break; } ?>
            </div>-->
            
            <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                <script>
                    var chart = AmCharts.makeChart("jTMonth", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": [
                            <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            {
                                "country": "<?php echo $m[$i] ?>",
                                "visits": <?php echo $mCant[$i] ?>
                            },
                            <?php } ?>
                        ],
                        "graphs": [{
                            "fillAlphas": 0.9,
                            "lineAlpha": 0.2,
                            "lineColor": "#02538b",
                            "fillColors": "#62cf73",
                            "type": "column",
                            "valueField": "visits"
                        }],
                        "categoryField": "country",
                        "valueAxes": [{
                            "title": "Indicador"
                        }]
                    });
                </script>
            </div>     
            
            <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                <script>
                    var chart = AmCharts.makeChart("jTWeek", {
                        "type": "serial",
                        "theme": "light",
                        "dataProvider": [
                            <?php 
                            if($sP > $sL) { 
                                for ($i = $sP; $i <= $numSemanas; $i++) {                                
                            ?>
                                {
                                    "country": "<?php echo 'CW '.$i ?>",
                                    "visits": <?php echo $cwProd[$i] ?>
                                },
                            <?php } 
                                for($i = 1; $i <= $sL; $i++) {                                
                            ?>
                                {
                                    "country": "<?php echo 'CW '.$i ?>",
                                    "visits": <?php echo $cwProd[$i] ?>
                                },
                            <?php } ?>                            
                            <?php } else { 
                                for ($i = $sP; $i <= $sL; $i++) {                                
                            ?>
                                {
                                    "country": "<?php echo 'CW '.$i ?>",
                                    "visits": <?php echo $cwProd[$i] ?>
                                },
                            <?php } } ?>
                        ],
                        "graphs": [{
                            "fillAlphas": 0.9,
                            "lineAlpha": 0.2,
                            "lineColor": "#02538b",
                            "fillColors": "#62cf73",
                            "type": "column",
                            "valueField": "visits"
                        }],
                        "categoryField": "country",
                        "valueAxes": [{
                            "title": "Inidicador"
                        }]
                    });
                </script>                
            </div>    
            
            <!-- TABLA DE MESES -->
            <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                <tr >
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">Mes</th>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">
                            <?php echo $lblM[$i] ?>
                        </th>
                    <?php } ?>
                </tr>
                <tr>
                     <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $mCant[$i] ?></td>    
                    <?php } ?>
                </tr>                
            </table>
            
            <!-- TABLA DE SEMANA -->
            <table style="width: 35%; margin-left: 63%; margin-top: -70px; border: 1px solid #BABABA; ">
                <tr>
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
               <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                    <?php }} ?>                            
                </tr>
                <tr>
                    <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwProd[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwProd[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwProd[$i] ?></td>
                    <?php }} ?>  
                </tr>                
            </table>             
            <br>
            
            <div id="oeeD" class="jidokaDay" >                
                <script>
                    var chart = AmCharts.makeChart("oeeD", {
                        "type": "serial",
                        "theme": "light",
                        "dataDateFormat": "YYYY-MM-DD",
                        "precision": 2,
                        "legend": {
                            "autoMargins": false,
                            "borderAlpha": 0.2,
                            "equalWidths": false,
                            "horizontalGap": 10,
                            "verticalGap": 25,
                            "markerSize": 10,
                            "useGraphSettings": true,
                            "valueAlign": "left",
                            "valueWidth": 0
                        },
                        "dataProvider": [
                        <?php  for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                $date = explode("-", $i);                                 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                        ?>    
                        {
                            "date": "<?php echo $vF[$vDate]; ?>",                            
                            "production": <?php echo $percentProduction[$vDate]; ?>,
                            "meta": "<?php echo 80; ?>"
                        },
                        <?php } ?>
                        ],
                        "valueAxes": [{
                            "id": "v1",
                            "title": "Percent OEE",
                            "position": "left",
                            "autoGridCount": false,
                            "labelFunction": function(value) {
                                return Math.round(value);
                            }
                        }],                    
                        "graphs": [{
                            "lineColor": "#62cf73",
                            "fillColors": "#62cf73",
                            "fillAlphas": 1,
                            "type": "column",
                            "title": "Indicador",
                            "valueField": "production",
                            "clustered": false,
                            "columnWidth": 0.3,
                            "legendValueText": "[[value]]%",
                            "balloonText": "[[title]]<br /><b style='font-size: 130%'>[[value]]%</b>"
                        }, {                             
                            "lineThickness": 2,
                            "lineColor": "#CD4C47",
                            "type": "smoothedLine",
                            "title": "Meta",
                            "valueField": "meta",
                            "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
                        }],
                        "categoryAxis": {
                            "parseDates": true,
                            "dashLength": 1,
                            "minorGridEnabled": true
                        },
                        "chartScrollbar": {
                            "graph": "g1",
                            "oppositeAxis": false,
                            "offset": 30,
                            "scrollbarHeight": 15,
                            "backgroundAlpha": 0,
                            "selectedBackgroundAlpha": 0.1,
                            "selectedBackgroundColor": "#888888",
                            "graphFillAlpha": 0,
                            "graphLineAlpha": 0.5,
                            "selectedGraphFillAlpha": 0,
                            "selectedGraphLineAlpha": 1,
                            "autoGridCount": true,
                            "color": "#AAAAAA"
                        },
                        "chartCursor": {
                            "pan": true,
                            "valueLineEnabled": true,
                            "valueLineBalloonEnabled": true,
                            "cursorAlpha": 0,
                            "valueLineAlpha": 0.2
                        },
                        "categoryField": "date",                                                                                
                        "balloon": { 
                            "borderThickness": 1,
                            "shadowAlpha": 0
                        },
                        "export": { 
                            "enabled": false
                        } 
                    });  
                </script>
            </div>  
        </div> 
    </body>