
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />

<script> 
    //CONFIGURACION DE LA TABLA 
    $(function () { 
        var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
        $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
            fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
            fixedColumnsRight: 0, //CABECERA FIJAS 
            columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
            scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
            scrollY: 0 //INICIO DE SCROLL LATERAL | 
        }); 
    }); 

    function getFixedColumnsData() {} 
</script>
        
<?php 
    session_start();
    include '../../db/ServerFunctions.php';

    $linea = $_SESSION["linea"]; 

    $fIni = date("Y-m-d", strtotime($_SESSION["aFIni"])); 
    $fFin = date("Y-m-d", strtotime($_SESSION["aFFin"]."+ 1 days")); 

    
    //RECIBIMOS TURNO 
    if (isset($_SESSION["turno"])){
        $turno = $_SESSION["turno"];
    } else {
        $turno = 0;
    }
    
    
    //CONSULTA SOLO DE USUARIOS REGISTRADOS 
    //LLAMADA POR TURNOS
    switch ($turno) {
        case 0: //ALL 
            //echo "ALL" ;
            #CONSULTA PARA LISTA DE USUARIOS 
            $cUsuariosAsistencia = cUsuariosListaAsistencia($linea, $fIni, $fFin);
            #CONSULTA PARA HORAS
            $cLAsistencia = cListaAsistencia ($linea, $fIni, $fFin); 
            break; 
        
        case 1: 
        case 2: 
        case 3: 
            $cUsuariosAsistencia = cUsuariosListaAsistencia($linea, $fIni, $fFin, $turno); 
            #CONSULTA PARA HORAS 
            $cLAsistencia = cListaAsistencia ($linea, $fIni, $fFin, $turno); 
            break; 
        
        default: //TODOS LOS TURNOS 
            //echo "ALL" ; 
            #CONSULTA PARA LISTA DE USUARIOS 
            $cUsuariosAsistencia = cUsuariosListaAsistencia($linea, $fIni, $fFin); 
            #CONSULTA PARA HORAS 
            $cLAsistencia = cListaAsistencia ($linea, $fIni, $fFin); 
            break; 
    } 
    
    #LISTA DE USUARIOS 
    for ($i = 0; $i < count($cUsuariosAsistencia); $i++) { 
        $usuario[$i] = $cUsuariosAsistencia[$i][0]; 
    } 
    
    #INICIALIZACION DE VARIABLES 
    for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){        
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $fecha[$vDate] = $i; 
        $cadDate[$vDate] = date("M d", strtotime($i)); 
        for ($j = 0; $j <= count($cUsuariosAsistencia); $j++) { 
            $tipoAsistencia[$vDate][$j] = '-'; 
            $horas[$vDate][$j] = 0; 
            $puesto[$vDate][$j] = 0; 
        } 
    } 
    
    //HACE CONSULTA DE TODA LA TABLA DE LISTA DE ASISTENCIA 
    for ($i = 0; $i < count($cLAsistencia); $i++ ){ 
        $date = explode("-", $cLAsistencia[$i][1]->format('Y-m-d')); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        //RECORRIDO DE USUARIOS PARA HACER LE MATCH 
        for($j = 0; $j < count($cUsuariosAsistencia); $j++){ 
            if ($cLAsistencia[$i][2] == $usuario[$j] ){ 
                $tipoAsistencia[$vDate][$j] = $cLAsistencia[$i][3]; 
                $horas[$vDate][$j] = $cLAsistencia[$i][4]; 
            } 
        } 
    } 
    
    //DATOS PARA TAMAÑO DE TABLA  
    $dias = (strtotime($fIni)- strtotime($fFin))/86400; 
    $dias = abs($dias); 
    $dias = floor($dias); 
    //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
    //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
    if ($dias > 11 ) { 
        $rowspan = 12;
    } else { 
        $rowspan = $dias+1;
    } 

?>

<style>
/*    #chartdiv {
        width: 100%;
        height: 500px;
    }	
    */
    table,td {
    	border: 1px solid black;
    }
    
    thead{
        background: #eaeded;
    }
    
</style>

<div id="holder-semple-1" > 
    <script id="tamplate-semple-1" type="text/mustache" > 
        <table style="width:97%; margin-left: 1.5%; <?php if (count($cUsuariosAsistencia) > 10){ ?> height: 88%; <?php } ?>" class="inner-table" > 
            <thead> 
                <tr> 
                    <td colspan="2" > </td> 
                    <td align="center" colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > <strong> PERIODO </strong> </td>                    
                </tr> 
                <tr>                     
                    <td align="center"> <strong> No. </strong></td> 
                    <td align="center" > <strong> Nombre </strong></td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?> 
                    <td align="center" > <strong><?php echo $cadDate[$vDate]; ?></strong></td> 
                    <?php } ?> 
                </tr> 
            </thead> 
            <tbody> 
                <?php for ($i = 0; $i< count($cUsuariosAsistencia); $i++ ) { ?>
                <tr > 
                    <td align="left" ><?php echo $i+1 ?></td> 
                    <td align="left" ><?php echo $usuario[$i] ?></td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?> 
                    <td align="center" ><?php echo $horas[$vDate][$i]; ?></td> 
                    <?php } ?> 
                </tr > 
                <?php } ?> 
            </tbody> 
        </table> 
    </script> 
</div>  



