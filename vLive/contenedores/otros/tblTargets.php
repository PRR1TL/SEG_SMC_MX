<script> 
    //CONFIGURACION DE LA TABLA 
    $(function () { 
        var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
        $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({ 
            fixedColumnsLeft: 1, //CONTADOR Y FILA FIJOS 
            fixedColumnsRight: 0, //CABECERA FIJAS 
            columnsInScrollableArea: 20, //CANTIDAD DE DIAS A VER 
            scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
            scrollY: 0 //INICIO DE SCROLL LATERAL | 
        }); 
    }); 

    function getFixedColumnsData() {} 
</script> 
        
<?php 
    session_start(); 
    include '../../db/ServerFunctions.php'; 
    
    $linea = $_SESSION["linea"]; 
    
    $mes = $_SESSION['mes']; 
    $anio = $_SESSION['anio']; 
    $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio)); 
    
    $fIni = $_SESSION['anio'].'-'.$_SESSION['mes'].'-01'; 
    $fFin = $anio.'-'.$mes.'-'.$ultimoDiaMes; 
    
    for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        $fecha[$vDate] = $i; 
        $cadDate[$vDate] = date("M d", strtotime($i)); 
        $oee[$vDate] = 0; 
        $tec[$vDate] = 0; 
        $org[$vDate] = 0; 
        $calidad[$vDate] = 0; 
        $cambio[$vDate] = 0; 
        $scrap[$vDate] = 0; 
        $produ[$vDate] = 0; 
        $mtaPzas[$vDate] = 0; 
    } 

    $cTblTargets = cTargetsMesLosses ($linea, $anio, $mes); 
    for ($i = 0; $i < count($cTblTargets); $i++){ 
        $date = explode("-", $cTblTargets[$i][0]); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        $oee[$vDate] = $cTblTargets[$i][1]; 
        $tec[$vDate] = $cTblTargets[$i][2]; 
        $org[$vDate] = $cTblTargets[$i][3]; 
        $cambio[$vDate] = $cTblTargets[$i][4]; 
        $calidad[$vDate] = $cTblTargets[$i][5]; 
        $scrap[$vDate] = $cTblTargets[$i][6]; 
        $produ[$vDate] = $cTblTargets[$i][7]; 
    } 
    
    //TARGETS PRODUCCION 
    $cTblTProduccion = cTargetsMesProduccion($linea, $anio, $mes); 
    for ($i = 0; $i < count($cTblTProduccion); $i++){ 
        $date = explode("-", $cTblTProduccion[$i][0]); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        $mtaPzas[$vDate] = $cTblTProduccion[$i][1]; 
    } 
    
    //DATOS PARA TAMAÑO DE TABLA 
    $dias = (strtotime($fIni)- strtotime($fFin))/86400; 
    $dias = abs($dias); 
    $dias = floor($dias); 
    //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS 
    //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS 
    if ($dias > 19 ) { 
        $rowspan = 20; 
    } else { 
        $rowspan = $dias+1; 
    } 

?>

<style>
    .table,.td { 
    	border: 1px solid black; 
    } 
    
    .thead { 
        background: #eaeded; 
    } 
    
</style>

<br>
<div id="holder-semple-1">
    <script id="tamplate-semple-1" type="text/mustache"> 
        <table style="width:99%; margin-left: 1.0%;" class="" > 
            <thead class="thead" > 
                <tr> 
                    <td class="td" > </td> 
                    <td class="td" align="center" colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" ><strong> PERIODO </strong></td>                    
                </tr> 
                <tr> 
                    <td class="td" >&nbsp;</td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?>
                    <td class="td" align="center" ><strong> <?php echo $cadDate[$vDate]; ?> </strong></td> 
                    <?php } ?> 
                </tr> 
            </thead> 
            <tbody> 
                <tr > 
                    <td class="td" align="right" style=" width: 2.5%" ><strong> OEE </strong></td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?> 
                    <td class="td" align="center" ><?php echo $oee[$vDate]; ?></td> 
                    <?php } ?> 
                </tr > 
                <tr > 
                    <td class="td" align="right" style=" width: 2.5%" ><strong> TÉCNICAS</strong></td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?> 
                    <td class="td" align="center" ><?php echo $tec[$vDate]; ?></td> 
                    <?php } ?> 
                </tr > 
                <tr > 
                    <td class="td" align="right" style=" width: 2.5%;" ><strong> ORGANIZACIONALES</strong></td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?> 
                    <td class="td" align="center" ><?php echo $org[$vDate]; ?></td> 
                    <?php } ?> 
                </tr > 
                <tr >
                    <td class="td" align="right" style=" width: 2.5%;" ><strong> CAMBIO DE MODELO</strong></td>
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td class="td" align="center" ><?php echo $cambio[$vDate]; ?></td>
                    <?php } ?>      
                </tr > 
                <tr >
                    <td class="td" align="right" style=" width: 2.5%;"  ><strong> NO CALIDAD</strong></td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?> 
                    <td class="td" align="center" ><?php echo $calidad[$vDate]; ?></td>
                    <?php } ?> 
                </tr > 
                <tr > 
                    <td class="td" align="right" style=" width: 2.5%;" ><strong> SCRAP</strong></td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td class="td" align="center" ><?php echo $scrap[$vDate]; ?></td>
                    <?php } ?>      
                </tr > 
                <tr > 
                    <td class="td" align="right" style=" width: 2.5%;"  ><strong> PRODUCTIVIDAD</strong></td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?>
                    <td class="td" align="center" ><?php echo $produ[$vDate]; ?></td> 
                    <?php } ?>      
                </tr > 
                <tr > 
                    <td class="td" align="right" style=" width: 2.5%;"  ><strong> PRODUCCIÓN</strong></td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?>
                    <td class="td" align="center" ><?php echo $mtaPzas[$vDate]; ?></td> 
                    <?php } ?>      
                </tr > 
            </tbody>
        </table>
    </script>
</div> 



