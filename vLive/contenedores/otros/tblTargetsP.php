<!-- LIBRERIAS Y CONFIGURACION PARA TABLA --> 
<!--<script src="http://code.jquery.com/jquery-1.12.2.min.js"></script> 
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script> 
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script> 
<script src="../../js/table-scroll.min.js"></script> -->
<script> 
    //CONFIGURACION DE LA TABLA 
    $(function () { 
        var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
        $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
            fixedColumnsLeft: 1, //CONTADOR Y FILA FIJOS 
            fixedColumnsRight: 0, //CABECERA FIJAS 
            columnsInScrollableArea: 20, //CANTIDAD DE DIAS A VER 
            scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
            scrollY: 0 //INICIO DE SCROLL LATERAL | 
        }); 
    }); 

    function getFixedColumnsData() {} 
</script> 
        
<?php 
    session_start(); 
    include '../../db/ServerFunctions.php'; 
 
    $linea = $_SESSION["linea"]; 
    $privilegio = $_SESSION['privilegio']; 
    
    $mes = $_SESSION["mes"]; 
    $anio = $_SESSION["anio"];         
    
    for($i = $_SESSION["FIni"]; $i <= $_SESSION["FFin"]; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        
        $fecha[$vDate] = date("Y-m-d", strtotime($i));
        $cadDate[$vDate] = date("M d", strtotime($i)); 
        
        $mtaPzas[$vDate] = '-'; 
    } 
    
    //TARGETS PRODUCCION
    $cTblTProduccion = cTargetsMesProduccion($linea, $anio, $mes);
    for ($i = 0; $i < count($cTblTProduccion); $i++){
        $date = explode("-", $cTblTProduccion[$i][0],3); 
        $vDate = $date[0].$date[1].(int)$date[2];
        
        $mtaPzas[$vDate] = $cTblTProduccion[$i][1];
    }
    
    //DATOS PARA TAMAÑO DE TABLA  
    $dias = (strtotime($_SESSION["FIni"])- strtotime($_SESSION["FFin"]))/86400; 
    $dias = abs($dias); 
    $dias = floor($dias); 
    //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
    //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
    if ($dias > 19 ) { 
        $rowspan = 20; 
    } else { 
        $rowspan = $dias+1; 
    } 

?>

<style>
    .table,.td { 
    	border: 1px solid black; 
    } 
    
    .thead { 
        background: #eaeded; 
    } 
    
</style>

<br>
<div id="holder-semple-1">
    <script id="tamplate-semple-1" type="text/mustache"> 
        <table style="width:99%; margin-left: 1.0%;" class="inner-table table"> 
            <thead class="thead" > 
                <tr> 
                    <td class="td" > </td> 
                    <td class="td" align="center" colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" ><strong> PERIODO </strong></td>                    
                </tr> 
                <tr> 
                    <td class="td" >&nbsp;</td> 
                    <?php for($j = $_SESSION["FIni"]; $j <= $_SESSION["FFin"]; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?>
                    <td class="td" align="center" ><strong> <?php echo $cadDate[$vDate]; ?> </strong></td> 
                    <?php } ?> 
                </tr> 
            </thead> 
            <tbody>                 
                <tr >
                    <td class="td" align="right" style=" width: 2.5%;"  ><strong> PRODUCCIÓN</strong></td> 
                    <?php for($j = $_SESSION["FIni"]; $j <= $_SESSION["FFin"]; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td class="td" align="center" ><?php echo $mtaPzas[$vDate]; ?></td>
                    <?php } ?>      
                </tr >
                <?php if ($privilegio >= 2 ){ ?> 
                <tr > 
                    <td class="td" align="right" style=" width: 2.5%;" > <strong> ACCION</strong> </td> 
                    <?php for($j = $_SESSION["FIni"]; $j <= $_SESSION["FFin"]; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];  
                    ?> 
                        <td class="td" > <button type="button" class="btn btn-info " data-toggle="modal" data-target="#mUTProd" 
                            data-pzas = "<?php echo $mtaPzas[$vDate]; ?>"                             
                            data-dia = "<?php echo $cadDate[$vDate]; ?>"
                            data-linea = "<?php echo $linea; ?>"
                            >  
                        <i class='glyphicon glyphicon-edit'></i> </button> </td>
                    <?php } ?> 
                </tr >
                <?php } ?> 
            </tbody>
        </table>
    </script>
</div> 



