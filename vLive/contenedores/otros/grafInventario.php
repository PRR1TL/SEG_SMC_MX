
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">-->

<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script> 
        
<?php 
    session_start(); 
    include '../../db/ServerFunctions.php'; 

    $linea = $_SESSION["linea"]; 
    for($i = $_SESSION["fIni"]; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){        
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 

        $fecha[$vDate] = $i;
        $cadDate[$vDate] = date("M d", strtotime($i)); 
        $vMax[$vDate] = 0;
        $vMin[$vDate] = 0; 
        $vReal[$vDate] = 0; 
    } 

    $cInventario = cInventario($_SESSION["fIni"], $_SESSION['fFin'], $linea);
    for ($i = 0; $i < count($cInventario); $i++ ){
        $date = explode("-", $cInventario[$i][0]->format('Y-m-d'));
        $vDate = $date[0].$date[1].(int)$date[2]; 

        $vMax[$vDate] = $cInventario[$i][1];
        $vMin[$vDate] = $cInventario[$i][2]; 
        $vReal[$vDate] = $cInventario[$i][3];  
    } 

    //DATOS PARA TAMAÑO DE TABLA  
    $dias = (strtotime($_SESSION["fIni"])- strtotime($_SESSION['fFin']))/86400;
    $dias = abs($dias); 
    $dias = floor($dias); 
    //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
    //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
    if ($dias > 11 ) { 
        $rowspan = 12;
    } else { 
        $rowspan = $dias+1;
    } 

?>

<style>
    #gInventario {
        width: 100%;
        height: 500px;
    }	
    
    table,td {
    	border: 1px solid black;
    }
    
    thead{
        background: #eaeded;
    }
    
</style>

<div id="gInventario">
    <script>
        var chart = AmCharts.makeChart("gInventario", {
            "type": "serial",
            "theme": "none",
            "dataDateFormat": "YYYY-MM-DD",
            "precision": 0,
            "valueAxes": [{
                "id": "v1",
                "title": "Sales",
                "position": "left",
                "autoGridCount": false
            }],
            "graphs": [{
                "valueAxis": "v1",
                "lineColor": "#62cf73",
                "fillColors": "#62cf73",
                "fillAlphas": 1,
                "type": "column",
                "title": "Real",
                "valueField": "real",
                "clustered": false,
                "columnWidth": 0.3,
                "legendValueText": "[[value]]",
                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
            }, {
                "valueAxis": "v1",            
                "lineThickness": 2,
                "lineColor": "#731F35",
                "type": "smoothedLine",
                "dashLength": 1,
                "title": "Max",
                "valueField": "max",
                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
            }, {
                "valueAxis": "v1",            
                "lineThickness": 2,
                "lineColor": "#0D0D0D",
                "type": "smoothedLine",
                "dashLength": 1,
                "title": "Min",
                "valueField": "min",
                "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
            }], 
            "chartScrollbar": { 
                "graph": "g1", 
                "oppositeAxis": false, 
                "offset": 30, 
                "scrollbarHeight": 20, 
                "backgroundAlpha": 0, 
                "selectedBackgroundAlpha": 0.1, 
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 0,
                "valueLineAlpha": 0.2
            },
            "categoryField": "date",
            "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
            },
            "legend": {
                "useGraphSettings": true,
                "position": "top"
            },
            "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0 
            },
            "dataProvider": [
                <?php for($i = $_SESSION["fIni"]; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){        
                      $date = explode("-", $i); 
                      $vDate = $date[0].$date[1].(int)$date[2]; ?>  
                {
                "date": "<?php echo $fecha[$vDate] ?>",
                "max": <?php echo $vMax[$vDate] ?>,
                "min": <?php echo $vMin[$vDate] ?>,
                "real": <?php echo $vReal[$vDate] ?>
                }, <?php } ?>  
            ] 
        }); 
    </script>    
</div> 

<div id="holder-semple-1">
    <script id="tamplate-semple-1" type="text/mustache"> 
        <table style="width:97%; margin-left: 1.5%; "class="inner-table"> 
            <thead> 
                <tr> 
                    <td > </td>
                    <td align="center" colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td>                    
                </tr> 
                <tr> 
                    <td>&nbsp;</td> 
                    <?php for($j = $_SESSION["fIni"]; $j <= $_SESSION['fFin']; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td align="center" ><?php echo $cadDate[$vDate]; ?></td>
                    <?php } ?>      
                </tr>
            </thead>
            <tbody>
                <tr >
                    <td align="right" >REAL</td>
                    <?php for($j = $_SESSION["fIni"]; $j <= $_SESSION['fFin']; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td align="center" ><?php echo $vReal[$vDate]; ?></td>
                    <?php } ?>      
                </tr >
                <tr >
                    <td align="right" >MAXIMO</td>
                    <?php for($j = $_SESSION["fIni"]; $j <= $_SESSION['fFin']; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td align="center" ><?php echo $vMax[$vDate]; ?></td>
                    <?php } ?>      
                </tr >
                <tr >
                    <td align="right" >MINIMO</td>
                    <?php for($j = $_SESSION["fIni"]; $j <= $_SESSION['fFin']; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td align="center" ><?php echo $vMin[$vDate]; ?></td>
                    <?php } ?>      
                </tr >
            </tbody>
        </table>
    </script>
</div> 



