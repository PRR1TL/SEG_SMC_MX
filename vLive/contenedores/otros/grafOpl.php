<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    date_default_timezone_set("America/Mexico_City");  
        
    $fActual = date("Y-m-d"); 
    
    //FUNCIONES   
    include '../../db/ServerFunctions.php';

    $contDatos = 0; 
    $pTotales = 0; 
    $pTotalesL = 0; 
    $pTotalesP = 0; 
    $pNotClose = 0; 
    $pNotCloseL = 0; 
    $pNotCloseP = 0; 
    $pExtraTime = 0; 
    $pExtraTimeL = 0; 
    $pExtraTimeP = 0; 
    $pAtTime = 0; 
    $pAtTimeL = 0; 
    $pAtTimeP = 0; 
    $pWait = 0; 
    $pWaitL = 0; 
    $pWaitP = 0; 

    $cPSinCerrarL = cPSinCerradosL($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin'], $fActual);
    $pNotCloseL = $cPSinCerrarL[0][0];

    $cPSinCerrarP = cPSinCerradosP($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin'], $fActual);
    $pNotCloseP = $cPSinCerrarP[0][0];

    $cPCerradosATiempoL = cPCerradosATiempoL($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']);
    $pAtTimeL = $cPCerradosATiempoL[0][0];

    $cPCerradosATiempoP = cPCerradosATiempoP($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']);
    $pAtTimeP = $cPCerradosATiempoP[0][0];

    $cPCerradosFTiempoL = cPCerradosFTiempoL($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']);
    $pExtraTimeL = $cPCerradosFTiempoL[0][0];

    $cPCerradosFTiempoP = cPCerradosFTiempoP($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']);
    $pExtraTimeP = $cPCerradosFTiempoP[0][0];

    $cPEsperaL = cPFuturosL($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin'], $fActual);
    $pWaitL = $cPEsperaL[0][0];

    $cPEsperaP = cPFuturosP($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin'], $fActual);
    $pWaitP = $cPEsperaP[0][0];

    $cPTotalesL = cTotalPuntosL($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']);
    $pTotalesL = $cPTotalesL[0][0];

    $cPTotalesP = cTotalPuntosP($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']);
    $pTotalesP = $cPTotalesP[0][0];

    $pNotClose = $pNotCloseL+$pNotCloseP;
    $pAtTime = $pAtTimeL+$pAtTimeP;
    $pExtraTime = $pExtraTimeL+$pExtraTimeP;
    $pWait = $pWaitL+$pWaitP;
        
?>

<head> 
    <style>
        #chartdiv {
            width: 70%;
            height: 75%;
            font-size	: 11px;
        } 
        
        .psncerrar .amcharts-pie-slice{
            fill: #ff0f00;
        } 
        .pcerradosatiempo .amcharts-pie-slice{
            fill: #23B14D;
        } 
        .pcerradosftiempo .amcharts-pie-slice{
            fill: #ff6600;
        } 
        .pespera .amcharts-pie-slice{
            fill: #fcd202;
        }         
    </style>
</head>

<body>        
    <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" > 
        <div id="chartdiv" class="col-lg-8 col-md-8" >
            <script> 
                var chart = AmCharts.makeChart( "chartdiv", {
                    "type": "pie",
                    "addClassNames": true,
                    "classNameField": "class",                     
                    "dataProvider": [ { 
                        "title": "Cerrados en tiempo",
                        "value": <?php echo $pAtTime ?>,
                        "class": "pcerradosatiempo"
                    }, { 
                        "title": "Espera / Abiertos", 
                        "value": <?php echo $pWait ?>, 
                        "class": "pespera" 
                    }, { 
                        "title": "Cerrados fuera de tiempo", 
                        "value": <?php echo $pExtraTime ?>, 
                        "class": "pcerradosftiempo"
                    }, {                        
                        "title": "Sin cerrar", 
                        "value": <?php echo $pNotClose ?>, 
                        "class": "psncerrar" 
                    }],
                    "titleField": "title", 
                    "valueField": "value", 
                    "labelRadius": 5, 
                    "radius": "40%", 
                    "innerRadius": "50%", 
                    "labelText": "[[title]]: ([[value]])", 
                    "export": { 
                        "enabled": true 
                    } 
                } ); 
            </script> 
        </div> 
        <div class="col-lg-3 col-md-3" >
            <table style="width: 100%; margin-top: 37%" class="table table-bordered" > 
                <thead style="background-color: #eaeded; "> 
                    <tr> 
                        <th class="contenidoCentrado" ></th> 
                        <th ></th> 
                        <th >No. Puntos </th>                                                                  
                    </tr> 
                </thead> 
                <tbody > 
                    <tr >
                        <td style="background: #23B14D" ></td> 
                        <td > P. Cerrados en tiempo </td>
                        <td > <?php echo $pAtTime ?> </td>                             
                    </tr>
                    <tr >
                        <td style="background: #fcd202" ></td> 
                        <td > P. Espera / Abiertos </td>
                        <td > <?php echo $pWait ?> </td>                             
                    </tr>
                    <tr  >
                        <td style="background: #ff6600" ></td> 
                        <td > P. Cerrados fuera de tiempo </td> 
                        <td > <?php echo $pExtraTime ?> </td> 
                    </tr>
                    <tr >
                        <td style="background: #ff0f00" ></td> 
                        <td > P. Sin cerrar </td> 
                        <td > <?php echo $pNotClose ?> </td> 
                    </tr>
                </tbody>
            </table>
        </div>
    </div>  
</body>
