<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../db/ServerFunctions.php';
    session_start();
    date_default_timezone_set("America/Mexico_City"); 
    
    if (isset($_SESSION["filtro"])) {
        $filtroPuntos = $_SESSION["filtro"];
    } else {
        $filtroPuntos = 0;
    } 
    
    if (isset($_SESSION['privilegio'])){
        $privilegio = $_SESSION['privilegio'];
    } else { 
        $privilegio = 0;
    } 
    
    $dia = date("Y-m-d"); 

    $contDatos = 0; 
    
    //CONSULTA DE OPL LINEA PENDIENTES     
    switch ($filtroPuntos){
        case 0: //TODOS
            $cOplProyecto = cOplProyecto(date("Y-m-d", strtotime($_SESSION['FIni'])), date("Y-m-d", strtotime($_SESSION['FFin'])), $_SESSION['linea']); 
            break; 
        case 1: // ABIERTOS
            $cOplProyecto = cOPLPFuturosP($_SESSION['linea'], date("Y-m-d", strtotime($_SESSION['FIni'])), date("Y-m-d", strtotime($_SESSION['FFin'])), $dia); 
            break; 
        case 2: // CERRADOS EN TIEMPO
            $cOplProyecto = cOPLCerradosATiempoP($_SESSION['linea'], date("Y-m-d", strtotime($_SESSION['FIni'])), date("Y-m-d", strtotime($_SESSION['FFin']))); 
            break; 
        case 3: // CERRADOS FUERA DE TIEMPO
            $cOplProyecto = cOPLCerradosFTiempoP($_SESSION['linea'], date("Y-m-d", strtotime($_SESSION['FIni'])), date("Y-m-d", strtotime($_SESSION['FFin']))); 
            break; 
        case 4: // SIN CERRAR
            $cOplProyecto = cOPLSinCerrarP($_SESSION['linea'], date("Y-m-d", strtotime($_SESSION['FIni'])), date("Y-m-d", strtotime($_SESSION['FFin'])), $dia); 
            break; 
        default :
            $cOplProyecto = cOplProyecto(date("Y-m-d", strtotime($_SESSION['FIni'])), date("Y-m-d", strtotime($_SESSION['FFin'])), $_SESSION['linea']); 
            break; 
    } 
    
    $contDatos = count($cOplProyecto); 
        
    //echo '<br>',$contDatos,': ',$_SESSION['FIni'],', ', $_SESSION['FFin'], ', ', $_SESSION['linea'];
    
?>    
    <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" > 
        <br>
        <table style="width: 100%; " class="table table-bordered" > 
            <thead style="background-color: #eaeded; "> 
                <tr> 
                    <th class="contenidoCentrado" >NO.</th> 
                    <th class="contenidoCentrado" >F. REGISTRO</th> 
                    <th >ACCI&Oacute;N REQUERIDA</th> 
                    <th >RACI-A</th> 
                    <th >ÁREA</th> 
                    <th >F.COMPROMISO</th> 
                    <th >ESTATUS</th> 
                    <th >EVALUACION</th>                                                         
                    <?php if ($privilegio != 0){ ?>
                    <th > </th> 
                    <?php } ?>
                </tr>
            </thead>
            <tbody >
            <?php for ($i = 0; $i < $contDatos; $i++) { ?>
                <tr style="background: <?php 
                        if ($cOplProyecto[$i][6] == 4) { 
                            if ($cOplProyecto[$i][9]->format('Y-m-d') <= $cOplProyecto[$i][8]->format('Y-m-d') ) { //CERRADO EN TIEMPO 
                                echo '#b4fac0'; 
                            } else { //if ($cOplProyecto[$i][9]->format('Y-m-d') > $cOplProyecto[$i][8]->format('Y-m-d') ){ //CERRADOS FUERA DE TIEMPO
                                echo '#f7b076'; 
                            } 
                        } else { 
                            if ($cOplProyecto[$i][8]->format('Y-m-d') >= $dia ) { //EN ESPERA 
                                echo '#faf3b4'; 
                            } else { //if ($cOplProyecto[$i][8]->format('Y-m-d') < $dia ) { //SIN CERRAR 
                                echo '#ED604E'; 
                            } 
                        } 
                    ?>" >
                    <td > <?php echo $i+1 ?> </td> 
                    <td > <?php echo $cOplProyecto[$i][5]->format('d-M'); ?> </td> 
                    <td > <?php echo $cOplProyecto[$i][2]; ?> </td> 
                    <td > <?php echo $cOplProyecto[$i][3]; ?> </td> 
                    <td > <?php echo $cOplProyecto[$i][4]; ?> </td> 
                    <td > <?php echo $cOplProyecto[$i][8]->format('d-M'); ?> </td> 
                    <td > 
                        <?php                                     
                            switch ($cOplProyecto[$i][6]){
                                case '0';
                                ?>
                                    <img src="../../imagenes/opl/opl0.png" style="width: 3vh">
                                <?php
                                    break;
                                case '1';
                                ?>
                                    <img src="../../imagenes/opl/opl1.png" style="width: 3vh">
                                <?php
                                    break;
                                case '2';
                                ?>
                                    <img src="../../imagenes/opl/opl2.png" style="width: 3vh">
                                <?php
                                    break; 
                                case '3'; 
                                ?>
                                    <img src="../../imagenes/opl/opl3.png" style="width: 3vh" > 
                                <?php
                                    break; 
                                default;
                                ?>
                                <img src="../../imagenes/opl/opl0.png" style="width: 3vh">
                                <?php
                                    break;
                            } 
                        ?> 
                    </td> 
                    <td > <?php 
                            switch ($cOplProyecto[$i][7]){
                                case '0';
                                ?>
                                    
                                <?php
                                    break;
                                case '1';
                                ?>
                                    <img src="../../imagenes/opl/ev1.jpg" style="width: 3vh">                                    
                                <?php
                                    break;
                                case '2';
                                ?>
                                    <img src="../../imagenes/opl/ev2.jpg" style="width: 3vh">
                                <?php
                                    break; 
                                case '3'; 
                                ?>
                                    <img src="../../imagenes/opl/opl3.png" style="width: 3vh" > 
                                <?php
                                    break; 
                                default;
                                ?>
                                
                                <?php
                                    break;
                            } 
                        ?> 
                    </td> 

                    <?php if ($privilegio > 0) { ?>
                    <td > 
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mUOplP" 
                            data-idpr = "<?php echo $cOplProyecto[$i][0]; ?>" 
                            data-id = "<?php echo $cOplProyecto[$i][1]; ?>"                                     
                            data-accion = "<?php echo $cOplProyecto[$i][2]; ?>" 
                            data-resp = "<?php echo $cOplProyecto[$i][3]; ?>"                                     
                            data-area = "<?php echo $cOplProyecto[$i][4]; ?>" 
                            data-fcompromiso = "<?php echo $cOplProyecto[$i][8]->format('d-M'); ?>" 
                            data-fini = "<?php echo $cOplProyecto[$i][5]->format('d-M'); ?>" 
                            data-estatus = "<?php echo $cOplProyecto[$i][6] ?>" 
                            data-evaluacion = "<?php echo $cOplProyecto[$i][7] ?>"
                            data-soporte = "<?php echo $cOplProyecto[$i][10]; ?>" 
                            <i class='glyphicon glyphicon-edit'></i> Modificar </button> 
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#mDOplP" 
                            data-idPr = "<?php echo $cOplProyecto[$i][0]; ?>" 
                            data-id = "<?php echo $cOplProyecto[$i][1]; ?>" 
                            data-accion = "<?php echo $cOplProyecto[$i][2]; ?>" 
                            data-resp = "<?php echo $cOplProyecto[$i][3]; ?>" 
                            data-area = "<?php echo $cOplProyecto[$i][4]; ?>" 
                            data-fcompromiso = "<?php echo $cOplProyecto[$i][8]->format('d-M'); ?>" 
                            data-fini = "<?php echo $cOplProyecto[$i][5]->format('d-M'); ?>" 
                            data-estatus = "<?php echo $cOplProyecto[$i][6] ?>" 
                            data-evaluacion = "<?php echo $cOplProyecto[$i][7] ?>" 
                            data-soporte = "<?php echo $cOplProyecto[$i][10]; ?>" 
                            <i class='glyphicon glyphicon-trash'></i> Eliminar </button> 
                    </td> 
                    <?php } ?>
                </tr> 
            <?php } ?> 
            </tbody> 
        </table> 
    </div> 