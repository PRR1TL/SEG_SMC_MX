<!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script> 
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script> -->

 <!--LINK PARA ICONOS-->  
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" >  -->

<!--ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS--> 
<!--<link rel="stylesheet" href="../../css/opl.css">--> 
<!--<script src="../../js/opl.js"> </script> -->

<?PHP 
    include '../../db/ServerFunctions.php'; 
    session_start(); 
    
    date_default_timezone_set("America/Mexico_City"); 
    $_SESSION['nivelReporte'] = 3; 
    $_SESSION['path'] = $_SERVER['PHP_SELF']; 
    
    if (isset($_SESSION['usuario']) && isset($_SESSION['privilegio'])){ 
        $producto = $_SESSION['productoU']; 
        $usuario = $_SESSION['usuario']; 
        $nombre = $_SESSION['nameUsuario']; 
        $nickName = $_SESSION['nickName']; 
        $privilegio = $_SESSION['privilegio']; 
        $correo = $_SESSION['correo']; 
    } else { 
        $userName = ''; 
        $nombre = 'INICIAR SESION'; 
        $privilegio = 0; 
        $typeEv = 0;  
    } 
    
    
        
    $cLista = cLJuntaLinea($_SESSION['linea'], $_SESSION['dia'])
    
    
?>


<div class="col-lg-10 col-md-10 col-sh-10 col-sm-10 col-xs-10 " >
    <table style="width: 100%; margin-top: 1%" class="table table-bordered" > 
        <thead style="background-color: #eaeded;" > 
            <tr> 
                <th >No.</th> 
                <th >Puesto</th> 
                <th >Nombre</th> 
                <th >Asistencia </th> 
                <th > </th> 
            </tr> 
        </thead> 
        <tbody > 
            <?php for($i = 0; $i < count($cLista); $i++ ){ ?>
            <tr >
                <td > <?php echo $i+1; ?> </td> 
                <td > <?php echo $cLista[$i][3]; ?> </td>  
                <td > <?php echo $cLista[$i][2]; ?> </td> 
                <td > <?php echo $cLista[$i][4]; ?> </td>  
                <td > 
                    <?php if ($privilegio >= 3 ){ ?>  
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mULJunta" 
                        data-linea = "<?php echo $cLista[$i][1] ?>" 
                        data-fecha = "<?php echo $cLista[$i][0]->format('M d, Y') ?>" 
                        data-nombre = "<?php echo $cLista[$i][2] ?>" 
                        data-tasist = "<?php echo $cLista[$i][4] ?>" 
                        data-puesto = "<?php echo $cLista[$i][3] ?>" > 
                        <i class='glyphicon glyphicon-edit'></i> Modificar </button> 
                            
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#mDLJunta" 
                        data-linea = "<?php echo $cLista[$i][1] ?>" 
                        data-fecha = "<?php echo $cLista[$i][0]->format('M d, Y') ?>" 
                        data-nombre = "<?php echo $cLista[$i][2] ?>" 
                        data-tasist = "<?php echo $cLista[$i][4] ?>" 
                        data-puesto = "<?php echo $cLista[$i][3] ?>" >  
                            <i class='glyphicon glyphicon-trash'></i> Eliminar </button> 
                    <?php } ?>
                </td>                             
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>