<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../../db/ServerFunctions.php';
    session_start();
    date_default_timezone_set("America/Mexico_City");     
       
    if (isset($_SESSION["filtro"])) { 
        $filtroPuntos = $_SESSION["filtro"]; 
    } else { 
        $filtroPuntos = 0; 
    } 
        
    if (isset($_SESSION['privilegio'])){ 
        $privilegio = $_SESSION['privilegio']; 
    } else { 
        $privilegio = 0; 
    } 
    
    $dia = date("Y-m-d"); 
    switch ($filtroPuntos) { 
        case 0: //TODOS 
            $cOplLinea = cOplLineaAll(date("Y-m-d", strtotime($_SESSION["FIni"])), date("Y-m-d", strtotime($_SESSION["FFin"])), $_SESSION["linea"]); 
            break;
        case 1: // ABIERTOS
            $cOplLinea = cOPLPFuturosL($_SESSION["linea"], date("Y-m-d", strtotime($_SESSION["FIni"])), date("Y-m-d", strtotime($_SESSION["FFin"])), $dia); 
            break;
        case 2: // CERRADOS EN TIEMPO
            $cOplLinea = cOPLCerradosATiempoL($_SESSION["linea"], date("Y-m-d", strtotime($_SESSION["FIni"])), date("Y-m-d", strtotime($_SESSION["FFin"]))); 
            break;
        case 3: // CERRADOS FUERA DE TIEMPO
            $cOplLinea = cOPLCerradosFTiempoL($_SESSION["linea"], date("Y-m-d", strtotime($_SESSION["FIni"])), date("Y-m-d", strtotime($_SESSION["FFin"]))); 
            break;
        case 4: // SIN CERRAR
            $cOplLinea = cOPLSinCerrarL($_SESSION["linea"], date("Y-m-d", strtotime($_SESSION["FIni"])), date("Y-m-d", strtotime($_SESSION["FFin"])), $dia); 
            break;  
        default :
            $cOplLinea = cOplLineaAll(date("Y-m-d", strtotime($_SESSION["FIni"])), date("Y-m-d", strtotime($_SESSION["FFin"])), $_SESSION["linea"]);  
            break;
    } 
     
?>    

    <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" > 
        <br> 
        <table style="width: 100%; " class="table table-bordered" > 
            <thead style="background-color: #eaeded; "> 
                <tr> 
                    <th class="contenidoCentrado" rowspan="2" >NO.</th> 
                    <th rowspan="2" >FECHA</th> 
                    <th rowspan="2" >DETECTO</th> 
                    <th rowspan="2" >ESTACION</th> 
                    <th colspan="2" >DESVIACIO / PROBLEMA</th> 
                    <th colspan="2" >ACCION / MEDIDA</th> 
                    <th rowspan="2" >F.COMP</th> 
                    <th rowspan="2" >F.TER</th>
                    <th rowspan="2" >ESTADO</th>                      
                    <?php if ($privilegio > 0) { ?>
                        <th rowspan="2"> </th> 
                    <?php } ?> 
                </tr>
                <tr class="contenidoCentrado" > 
                    <th >DESCRIPCION</th> 
                    <th >CAUSA</th> 
                    <th >DESCRIPCION</th> 
                    <th >RESPONSABLE</th>  
                </tr> 
            </thead> 
            <tbody > 
            <?php for ($i = 0; $i < count($cOplLinea); $i++) { ?>
                <tr style="background: 
                    <?php 
                        if ($cOplLinea[$i][9] == 4) { 
                            if ($cOplLinea[$i][11]->format('Y-m-d') <= $cOplLinea[$i][8]->format('Y-m-d') ) { //CERRADO EN TIEMPO 
                                echo '#b4fac0'; 
                            } else { //if ($cOplLinea[$i][11]->format('Y-m-d') > $cOplLinea[$i][8]->format('Y-m-d') ){ //CERRADOS FUERA DE TIEMPO
                                echo '#f7b076'; 
                            } 
                        } else { 
                            if ($cOplLinea[$i][8]->format('Y-m-d') >= $dia ) { //EN ESPERA 
                                echo '#faf3b4'; 
                            } else { //if ($cOplLinea[$i][8]->format('Y-m-d') < $dia ) { //SIN CERRAR 
                                echo '#d44949'; 
                            } 
                        } 
                    ?>" > 
                    <td > <?php echo $i+1 ?> </td> 
                    <td > <?php echo $cOplLinea[$i][1]->format('d-M'); ?> </td> 
                    <td > <?php echo $cOplLinea[$i][2]; ?> </td> 
                    <td > <?php echo $cOplLinea[$i][3]; ?> </td> 
                    <td > <?php echo $cOplLinea[$i][4]; ?> </td> 
                    <td > <?php echo $cOplLinea[$i][5]; ?> </td> 
                    <td > <?php echo $cOplLinea[$i][6]; ?> </td> 
                    <td > <?php echo $cOplLinea[$i][7]; ?> </td> 
                    <td > <?php echo $cOplLinea[$i][8]->format('d-M'); ?> </td> 
                    <td > <?php if ($cOplLinea[$i][9] == 4 ){ echo $cOplLinea[$i][11]->format('d-M'); } else { echo '-'; } ?> </td> 
                    <td > 
                        <?php                                     
                            switch ($cOplLinea[$i][9]){ 
                                case '0';
                                ?>
                                    <img src="../../imagenes/opl/opl0.png" style="width: 3vh">
                                <?php
                                    break;
                                case '1';
                                ?>
                                    <img src="../../imagenes/opl/opl1.png" style="width: 3vh">
                                <?php
                                    break;
                                case '2';
                                ?>
                                    <img src="../../imagenes/opl/opl2.png" style="width: 3vh">
                                <?php
                                    break; 
                                case '3'; 
                                ?>
                                    <img src="../../imagenes/opl/opl3.png" style="width: 3vh" > 
                                <?php 
                                    break; 
                                case '4'; 
                                ?>
                                    <img src="../../imagenes/opl/opl4.png" style="width: 3vh" > 
                                <?php
                                    break; 
                                default;
                                ?>
                                <img src="../../imagenes/opl/opl0.png" style="width: 3vh">
                                <?php
                                    break;
                            } 
                        ?>
                    </td>
                    <?php if ($privilegio > 0) { ?>
                    <td > 
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#mUOplL" 
                            data-id = "<?php echo $cOplLinea[$i][0]; ?>" 
                            data-linea = "<?php echo $cOplLinea[$i][10] ?>" 
                            data-finicio = "<?php echo $cOplLinea[$i][1]->format('d-M') ?>" 
                            data-fcompromiso = "<?php echo $cOplLinea[$i][8]->format('d-M') ?>" 
                            data-op = "<?php echo $cOplLinea[$i][3] ?> " 
                            data-hallazgo = "<?php echo $cOplLinea[$i][4] ?>" 
                            data-causa = "<?php echo $cOplLinea[$i][5] ?>" 
                            data-accion = "<?php echo $cOplLinea[$i][6] ?>" 
                            data-resp = "<?php echo $cOplLinea[$i][7] ?>" 
                            data-sop = "<?php echo $cOplLinea[$i][2] ?>" 
                            data-estado = "<?php echo $cOplLinea[$i][9] ?>" > 
                            <i class='glyphicon glyphicon-edit'></i> </button> 
                    <?php if ($privilegio >= 3) { ?>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#mDOplL" 
                            data-id = "<?php echo $cOplLinea[$i][0]; ?>" 
                            data-linea = "<?php echo $cOplLinea[$i][10] ?>" 
                            data-finicio = "<?php echo $cOplLinea[$i][1]->format('d-M') ?>" 
                            data-fcompromiso = "<?php echo $cOplLinea[$i][8]->format('d-M') ?>" 
                            data-op = "<?php echo $cOplLinea[$i][3] ?> " 
                            data-hallazgo = "<?php echo $cOplLinea[$i][4] ?>" 
                            data-causa = "<?php echo $cOplLinea[$i][5] ?>" 
                            data-accion = "<?php echo $cOplLinea[$i][6] ?>" 
                            data-resp = "<?php echo $cOplLinea[$i][7] ?>" 
                            data-sop = "<?php echo $cOplLinea[$i][2] ?>" 
                            data-estado = "<?php echo $cOplLinea[$i][9] ?>" > 
                            <i class='glyphicon glyphicon-trash'></i> </button> 
                    <?php } ?>
                    </td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>