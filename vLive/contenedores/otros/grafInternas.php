<HTML>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
        
    <?php  
        //FUNCIONES   
        include '../../db/ServerFunctions.php';         
        session_start();
        date_default_timezone_set("America/Mexico_City");   
        
        $linea = $_SESSION["linea"]; 
        $mes = $_SESSION["mes"];
        $anio = $_SESSION["anio"];

        if (strlen($mes) < 2){
            $mes = '0'.$mes;
        } 
        
        if (isset($_SESSION['privilegio'])){ 
            $privilegio = $_SESSION['privilegio']; 
        } else { 
            $privilegio = 0; 
        } 
        
        
        $cCalidadIMonth = cCalidadInternaMes($linea, $_SESSION['fIni'], $_SESSION['fFin']); 
        $cCalidadKmMonth = cCalidadKmMes($linea, $_SESSION['fIni'], $_SESSION['fFin']); 
        
        /**************************** DIARIA *********************************/
        # PIEZAS Y MINUTOS REPORTADOS 
        for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $fecha[$vDate] = $i; 
            $dateT[$vDate] = date("M, d",strtotime($i)); 
            $cantFI_D[$vDate] = 0; 
            $cantFK_D[$vDate] = 0; 
            $target_D[$vDate] = 0;
        }
        
        #CALIDAD INTERNA
        for ($i = 0; $i < count($cCalidadIMonth); $i++){ 
            $vDate = $anio.$mes.(int)$cCalidadIMonth[$i][1]; 
            $cantFI_D[$vDate] = $cCalidadIMonth[$i][0]; 
        } 
        
        #CALIDAD 0-KM 
        for ($i = 0; $i < count($cCalidadKmMonth); $i++){ 
            $vDate = $anio.$mes.(int)$cCalidadKmMonth[$i][1]; 
            $cantFK_D[$vDate] = $cCalidadKmMonth[$i][0]; 
        } 
                
        $dias = (strtotime($_SESSION['fIni'])- strtotime($_SESSION['fFin']))/86400;        
        
        
        /****************************** MES ***************************/
        #INICIALIZACION DE VARIABLES
        $lblMonth[1] = 'Jan'; 
        $lblMonth[2] = 'Feb'; 
        $lblMonth[3] = 'Mar'; 
        $lblMonth[4] = 'Apr'; 
        $lblMonth[5] = 'May'; 
        $lblMonth[6] = 'Jun'; 
        $lblMonth[7] = 'Jul'; 
        $lblMonth[8] = 'Aug'; 
        $lblMonth[9] = 'Sep'; 
        $lblMonth[10] = 'Oct'; 
        $lblMonth[11] = 'Nov'; 
        $lblMonth[12] = 'Dec'; 
        for($i = 1; $i <= 12; $i++){
            $cantFI_M[$i] = 0; 
            $cantFK_M[$i] = 0; 
            $target_M[$i] = 0;             
        } 
        
        $cCKmYear = cCalidadKmAnio($_SESSION['linea'], $_SESSION['anio']);
        for ($i = 0; $i < count($cCKmYear); $i++){
            $m = $cCKmYear[$i][0]; 
            $cantFK_M[$m] = $cCKmYear[$i][1]; 
        } 
        
        $cCIYear = cCalidadInternaAnio($_SESSION['linea'], $_SESSION['anio']);
        for ($i = 0; $i < count($cCIYear); $i++){
            $m = $cCIYear[$i][0]; 
            $cantFI_M[$m] = $cCIYear[$i][1]; 
        }
        
        /********************* SEMANA *********************/ 
        $date = new DateTime;
        $date->setISODate("$anio", 53);
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //INICIALIZAMOS VARIABLES PARA MES
        for($i = 1; $i <= 12; $i++){
            $mCant[$i] = 0;
            $targetMonth[$i] = 0;
        }  
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
                
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) { 
                $cantI_W[$i] = 0; 
                $cantK_W[$i] = 0; 
                $targetWeek[$i] = 0; 
            }
            
            for($i = 1; $i <= $sL; $i++) { 
                $cantI_W[$i] = 0; 
                $cantK_W[$i] = 0; 
                $targetWeek[$i] = 0; 
            }            
        } else {
            for ($i = $sP; $i <= $sL; $i++) { 
                $cantI_W[$i] = 0; 
                $cantK_W[$i] = 0; 
                $targetWeek[$i] = 0;             }            
        }        
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        if ($sP == 0)
            $dSI = 7;          
        
        if ($sL == 0) 
            $dSL = 7;        
        
        $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio));
        $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));            
                
        $cCIWeek = cCalidadInternaSemana($_SESSION['linea'], $fP, $fL);
        for ($i = 0; $i < count($cCIWeek); $i++){
            $w  = $cCIWeek[$i][0];
            $cantI_W[$w]  = $cCIWeek[$i][1]; 
        } 
        
        $cCKWeek = cCalidadKmSemana($_SESSION['linea'], $fP, $fL);
        for ($i = 0; $i < count($cCKWeek); $i++){
            $w  = $cCKWeek[$i][0];
            $cantK_W[$w]  = $cCKWeek[$i][1]; 
        } 
        
    ?> 
<body> 
    <div class=" row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" > 
        <div id="jTMonth" style="width: 60%; min-height: 175px; max-height: 300px; margin-top: -12px;" >  
            <script>
                var chart = AmCharts.makeChart("jTMonth", { 
                    "type": "serial", 
                    "theme": "none",                      
                    "dataProvider": [ 
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        {
                            "country": "<?php echo $lblMonth[$i]; ?>",
                            "internas": <?php echo $cantFI_M[$i]; ?>,
                            "km": <?php echo $cantFK_M[$i]; ?>, 
                            "meta": <?php echo $target_M[$i]; ?>
                        },
                        <?php } ?>
                    ], 
                    "valueAxes": [{ 
                        "minimum": 0,
                        "fontSize": 8, 
                        "stackType": "regular", 
                        "axisAlpha": 0.3, 
                        "gridAlpha": 0.2 
                    }], 
                    "graphs": [{
                        "balloonText": "<b>[[title]]: </b><span style='font-size:11px'><b>[[value]]</b></span>",
                        "fillAlphas": 1, 
                        "fillColors": "#311B92", 
                        "lineColor": "#311B92", 
                        "lineAlpha": 1, 
                        "title": "Fallas Internas", 
                        "type": "column", 
                        "color": "#FFFF",
                        "valueField": "internas" 
                    }, { 
                        "balloonText": "<b>[[title]]: </b><span style='font-size:11px'><b>[[value]]</b></span>", 
                        "fillAlphas": 1, 
                        "fillColors": "#FF5C4D", 
                        "lineColor": "#FF5C4D", 
                        "lineAlpha": 1, 
                        "title": "0-KM", 
                        "type": "column", 
                        "color": "#FFFF", 
                        "valueField": "km" 
                    },  {                         
                        "balloonText": "<b>[[title]]: </b><span style='font-size:11px'><b>[[value]]</b></span>", 
                        "lineThickness": 2, 
                        "fillAlphas": 0, 
                        "lineAlpha": 2, 
                        "title": "Meta", 
                        "valueField": "meta", 
                        "dashLengthField": "dashLengthLine" 
                    }], 
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2
                    }, 
                    "categoryField": "country", 
                    "categoryAxis": { 
                        "gridPosition": "start", 
                        "axisAlpha": 0, 
                        "gridAlpha": 0, 
                        "position": "left" 
                    }                     
                });  
            </script>
        </div>     

        <div id="jTWeek" style="width: 35%; min-height: 175px; max-height: 300px; margin-top: -12px; margin-left: 20px; " >
            <script>
                var chart = AmCharts.makeChart("jTWeek", {
                    "type": "serial", 
                    "theme": "none",                      
                    "dataProvider": [ 
                     <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { 
                        ?>
                            {
                                "country": "<?php echo 'CW-'.$i; ?>", 
                                "internas": <?php echo $cantI_W[$i]; ?>, 
                                "km": <?php echo $cantK_W[$i]; ?>, 
                                "meta": <?php echo $targetWeek[$i]; ?> 
                            },
                        <?php } 
                            for($i = 1; $i <= $sL; $i++) { 
                        ?>
                            {
                                "country": "<?php echo 'CW-'.$i; ?>", 
                                "internas": <?php echo $cantI_W[$i]; ?>, 
                                "km": <?php echo $cantK_W[$i]; ?>, 
                                "meta": <?php echo $targetWeek[$i]; ?> 
                            },
                        <?php } ?> 
                        <?php } else { 
                            for ($i = $sP; $i <= $sL; $i++) { 
                        ?> 
                            { 
                                "country": "<?php echo 'CW-'.$i; ?>", 
                                "internas": <?php echo $cantI_W[$i]; ?>, 
                                "km": <?php echo $cantK_W[$i]; ?>, 
                                "meta": <?php echo $targetWeek[$i]; ?> 
                            }, 
                        <?php } } ?> 
                    ], 
                    "valueAxes": [{ 
                        "minimum": 0,
                        "fontSize": 8, 
                        "stackType": "regular", 
                        "axisAlpha": 0.3, 
                        "gridAlpha": 0.2 
                    }], 
                    "graphs": [{
                        "balloonText": "<b>[[title]]: </b><span style='font-size:11px'><b>[[value]]</b></span>",
                        "fillAlphas": 1, 
                        "fillColors": "#311B92", 
                        "lineColor": "#311B92", 
                        "lineAlpha": 1, 
                        "title": "Fallas Internas", 
                        "type": "column", 
                        "color": "#FFFF",
                        "valueField": "internas" 
                    }, { 
                        "balloonText": "<b>[[title]]: </b><span style='font-size:11px'><b>[[value]]</b></span>", 
                        "fillAlphas": 1, 
                        "fillColors": "#FF5C4D", 
                        "lineColor": "#FF5C4D", 
                        "lineAlpha": 1, 
                        "title": "0-KM", 
                        "type": "column", 
                        "color": "#FFFF", 
                        "valueField": "km" 
                    },  {                         
                        "balloonText": "<b>[[title]]: </b><span style='font-size:11px'><b>[[value]]</b></span>", 
                        "lineThickness": 2, 
                        "fillAlphas": 0, 
                        "lineAlpha": 2, 
                        "title": "Meta", 
                        "valueField": "meta", 
                        "dashLengthField": "dashLengthLine" 
                    }], 
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2
                    }, 
                    "categoryField": "country", 
                    "categoryAxis": { 
                        "gridPosition": "start", 
                        "axisAlpha": 0, 
                        "gridAlpha": 0, 
                        "position": "left" 
                    }                     
                });  
            </script>                
        </div>    

        <!-- TABLA DE MESES -->
        <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
            <tr >
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >Mes</th>
                <?php for ($i = 1; $i < 13; $i++ ) { ?>
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblMonth[$i] ?></th>
                <?php } ?>
            </tr>
            <tr>
                <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
            <?php for ($i = 1; $i < 13; $i++ ) { ?>
                <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $cantFI_M[$i]+$cantFK_M[$i] ?></td>      
            <?php } ?>
            </tr>                
        </table> 

        <!-- TABLA DE SEMANA -->
        <table style="width: 35%; margin-left: 63%; margin-top: -49px; border: 1px solid #BABABA; ">
            <tr>
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                <?php 
                if($sP > $sL){
                    for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                        <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                    <?php } 
                    for ($i = 1; $i <= $sL; $i++ ) {
                    ?>
                        <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                    <?php
                    }
                } else {                        
                    for ($i = $sP; $i <= $sL; $i++) { ?>
                        <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                <?php }} ?>                            
            </tr>
            <tr>
                <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
                <?php 
                if($sP > $sL){
                    for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                        <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cantI_W[$i]+$cantK_W[$i]; ?></td>
                    <?php } 
                    for ($i = 1; $i <= $sL; $i++ ) {
                    ?>
                        <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cantI_W[$i]+$cantK_W[$i]; ?></td>
                    <?php
                    }
                } else {                        
                    for ($i = $sP; $i <= $sL; $i++) { ?>
                        <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cantI_W[$i]+$cantK_W[$i]; ?></td>
                <?php }} ?>  
            </tr>                
        </table> 
        <br>
        
        <div id="fallasD" style="width: 105%; min-height: 300px; max-height: 900px; margin-top: 12px;" >
            <script> 
                var chart = AmCharts.makeChart("fallasD", { 
                    "type": "serial", 
                    "theme": "light", 
                    "dataDateFormat": "YYYY-MM-DD", 
                    "precision": 0, 
                    "dataProvider": [
                        <?php for($i = $_SESSION['fIni']; $i <= $_SESSION['fFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                $date = explode("-", $i); 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                            ?> 
                        { 
                            "date": "<?php echo $fecha[$vDate]; ?>", 
                            "FInternas": <?php echo $cantFI_D[$vDate]; ?>, 
                            "0km": <?php echo $cantFK_D[$vDate]; ?>, 
                            "meta": <?php echo $target_D[$vDate]; ?> 
                        }, 
                        <?php } ?> 
                    ],
                    "valueAxes": [{ 
                        "minimum": 0,
                        "fontSize": 8, 
                        "stackType": "regular", 
                        "axisAlpha": 0.3, 
                        "gridAlpha": 0.2 
                    }], 
                    "graphs": [{ 
                        "balloonText": "<b>[[title]]: </b><span style='font-size:11px'><b>[[value]]</b></span>", 
                        "fillAlphas": 1, 
                        "fillColors": "#311B92", 
                        "lineColor": "#311B92", 
                        "labelText": "[[value]]", 
                        "lineAlpha": 1, 
                        "title": "Fallas Internas", 
                        "type": "column", 
                        "color": "#FFFF",
                        "valueField": "FInternas" 
                    }, { 
                        "balloonText": "<b>[[title]]: </b><span style='font-size:11px'><b>[[value]]</b></span>",
                        "fillAlphas": 1, 
                        "fillColors": "#FF5C4D", 
                        "lineColor": "#FF5C4D", 
                        "labelText": "[[value]]", 
                        "lineAlpha": 1, 
                        "title": "0-KM", 
                        "type": "column", 
                        "color": "#FFFF", 
                        "valueField": "0km" 
                    }, { 
                        "balloonText": "<b>[[title]]: </b><span style='font-size:11px'><b>[[value]]</b></span>",
                        "lineThickness": 2,                         
                        "fillAlphas": 0, 
                        "lineAlpha": 2, 
                        "title": "Meta", 
                        "valueField": "meta", 
                        "dashLengthField": "dashLengthLine" 
                    }],
                    "chartCursor": { 
                        "pan": true, 
                        "valueLineEnabled": true, 
                        "valueLineBalloonEnabled": true, 
                        "cursorAlpha": 0, 
                        "valueLineAlpha": 0.2 
                    }, 
                    "legend": {                            
                        "autoMargins": false,
                        "borderAlpha": 0.2,
                        "equalWidths": false,
                        "horizontalGap": 50,
                        "verticalGap": 15,
                        "markerSize": 10,
                        "useGraphSettings": true,
                        "valueAlign": "left",
                        "valueWidth": 0
                    },
                    "categoryField": "date",
                    "categoryAxis": {
                        "parseDates": true, 
                        "dashLength": 1, 
                        "minorGridEnabled": true 
                    } 
                });
            </script> 
        </div> 
    </div> 
</body>    


        