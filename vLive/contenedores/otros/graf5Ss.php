<HTML>
    
    <LINK REL=StyleSheet HREF="../../css/jGraficas.css" TYPE="text/css" MEDIA=screen > 
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script> 
    
    <!-- LIBRERIAS Y CONFIGURACION PARA TABLA -->
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script> 
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 20, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
        
        function setTipoDatos() { 
            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON
            var dato = document.getElementById("tipoDatos").value; 

            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes.php", 
                type: "post", 
                data: { tipoVista: 1, tipoDato: dato, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 
        
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
    
    <!-- LIBRERIAS Y CONFIGURACION PARA PICKERS -->
    <!-- LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR -->     
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <?php 
        include '../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
        $_SESSION['linea'] = $_SESSION['linea']; 
        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes'];         
       
        $date->setISODate("$anio", 53);
        
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }
        
        //INICIALIZAMOS VARIABLES PARA MES
        for($i = 1; $i <= 12; $i++){
            $mCant[$i] = 0;
            $targetMonth[$i] = 0;
        }  
        
        //ULTIMO DIA DEL MES
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        
        
        #DIA DE LAS SEMANAS
        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
                
        //INICIALIZAMOS VARIABLES PARA SEMANA
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0; 
                $cwTec[$i] = 0; 
                $targetWeek[$i] = 0; 
            }
            
            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0; 
                $cwTec[$i] = 0; 
                $targetWeek[$i] = 0; 
            }            
        } else {
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0; 
                $cwTec[$i] = 0; 
                $targetWeek[$i] = 0; 
            }            
        }        
        
        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));
        
        if ($sP == 0)
            $dSI = 7;          
        
        if ($sL == 0) 
            $dSL = 7;        
        
        $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio));
        $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio));            
        
        //OBTENEMOS LA ULTIMA SEMANA DEL MES        
        $diaSemanaU = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

        //CONSULTA DE SCRAP(IFK, MISCELLANEOUS), SOLO POR CENTRO DE COSTOS   
        $mes2 = $mes-1; 
        $dI = 01;
        $dF = date("t",mktime(0,0,0,$mes,1,$anio)); 
        
        $m[1] = (string) "Jun";
        $m[2] = (string) "Feb";
        $m[3] = (string) "Mar";
        $m[4] = (string) "Apr";
        $m[5] = (string) "May";
        $m[6] = (string) "Jun";
        $m[7] = (string) "Jul";
        $m[8] = (string) "Aug";
        $m[9] = (string) "Sep";
        $m[10] = (string) "Oct";
        $m[11] = (string) "Nov";
        $m[12] = (string) "Dec";
        
        //INCIALIZAMOS LAS VARIABLES 
        $countProblem = 2;
        $problema[1] = 'Indicador';
        $problema[2] = 'Meta';
        
        $totalProblema[1] = 0;
        $totalProblema[2] = 0;
            
        for($i = $_SESSION['FIni']; $i <= date("Y-m-d", strtotime($_SESSION['FFin'] ."+ 1 days")); $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $fecha[$vDate] = $i;
            $valueDayT[$vDate] = 0; 
            $targetDayT[$vDate] = 0; 
        } 
        
        $jTDay = c5SDay($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']); 
        $jTWeek = c5SWeek($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']); 
        $jTMonth = c5SMonth($_SESSION['linea'], $_SESSION['anio']); 
        
        for ($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) {  
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2];         
            switch($date[1]){
                case 1:
                    $cadDate[$i] = 'Jan&nbsp;'.$date[2];
                    break;
                case 2:
                    $cadDate[$i] = 'Feb&nbsp;'.$date[2];
                    break;
                case 3:
                    $cadDate[$i] = 'Mar&nbsp;'.$date[2];
                    break;
                case 4:
                    $cadDate[$i] = 'Apr&nbsp;'.$date[2];
                    break;
                case 5:
                    $cadDate[$i] = 'May&nbsp;'.$date[2];
                    break;
                case 6:
                    $cadDate[$i] = 'Jun&nbsp;'.$date[2];
                    break;
                case 7:
                    $cadDate[$i] = 'Jul&nbsp;'.$date[2];
                    break;
                case 8:
                    $cadDate[$i] = 'Aug&nbsp;'.$date[2];
                    break;
                case 9:
                    $cadDate[$i] = 'Sep&nbsp;'.$date[2];
                    break;
                case 10:
                    $cadDate[$i] = 'Oct&nbsp;'.$date[2];
                    break;
                case 11:
                    $cadDate[$i] = 'Nov&nbsp;'.$date[2];
                    break;
                case 12:
                    $cadDate[$i] = 'Dec&nbsp;'.$date[2];
                    break;
            }
            
            //INICIALIZAMOS LAS VARIABLES PARA LOS SUBTOTALES DIA(_)            
            $totalDia[$vDate] = 0; 
            $targetDayT[$vDate] = 0; 
        } 
        
        /************************ DIA **********************/
        for ($i = 0; $i < count($jTDay); $i++ ){
            $date = explode("-", $jTDay[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2];         
            $valueDayT[$vDate] = $jTDay[$i][1]; 
        } 
        
        #TARGET
//        $ctargetDay = cTargetTecDaily($_SESSION['linea'], $_SESSION['FIni'], $_SESSION['FFin']);
//        for ($i = 0; $i < count($ctargetDay) ; $i++) { 
//            $date = explode("-", $ctargetDay[$i][0]); 
//            $vDate = $date[0].$date[1].(int)$date[2]; 
//            $targetDayT[$vDate] = $ctargetDay[$i][1]; 
//        } 
        
        /********************* SEMANA *************************/
        for($i = 0; $i < count($jTWeek); $i++ ){
            $sem = $jTWeek[$i][0];
            $cwTec[$sem] = $jTWeek[$i][1];
        } 
        
        #TARGET 
//        $cTargetWeek = cTargetTecWeekly($_SESSION['linea'], $anio, $mes);
//        for ($i = 0; $i < count($cTargetWeek); $i++){
//            $s = $cTargetWeek[$i][0];
//            $targetWeek[$s] = $cTargetWeek[$i][1];
//        }
        
        /********************** MES ***********************/
        for($i = 0 ; $i < count($jTMonth); $i++){
            $nMes = $jTMonth[$i][0];            
            $mCant[$nMes] = $jTMonth[$i][1];    
        }         
        
        #TARGET
//        $cTargetMonth = cTargetTecMonthy($_SESSION['linea'], $anio);
//        for ($i = 0; $i < count($cTargetMonth); $i++ ){
//            $ms = $cTargetMonth[$i][0];
//            $targetMonth[$ms] = $cTargetMonth[$i][1];
//        }
        
        //INCIALIZAMOS LAS VARIABLES
        //TABLA 
        $countX = 1;
        $total = 0;
        $sumProblemas = 0;
        
        //DATOS PARA TAMAÑO DE TABLA  
        //$dias = (strtotime($_SESSION['FIni'])- strtotime($_SESSION['FFin']))/86400;
        $dias = (strtotime($_SESSION['FIni'])- strtotime($_SESSION['FFin']))/86400;
        $dias = abs($dias); 
        $dias = floor($dias)+1; 
        //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
        //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS 
        if ($dias > 19 ) { 
            $rowspan = 20;
        } else { 
            //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
            //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS 
            $rowspan = $dias;
        } 

        //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
        if($countProblem == 0) { 
            $countProblem = 1;
            $problema[1] = "";
            $totalProblema[1] = 0;
            $totalProblema[2] = 0;
        } 
        
    ?> 
    <body> 
        <div>  
            <div class="row col-lg-12 col-md-12 col-sm-12 col-xs-12 col-sh-12" style="margin-top: 10px;" > 
                <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                    <script>
                        var chart = AmCharts.makeChart("jTMonth", {
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": [
                                <?php for ($i = 1; $i < 13; $i++ ) { ?>
                                {
                                    "country": "<?php echo $m[$i]; ?>",
                                    "visits": <?php echo $mCant[$i]; ?>,
                                    "meta": <?php echo $targetMonth[$i]; ?>
                                },
                                <?php } ?>
                            ],
                            "graphs": [{
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "lineColor": "#02538b",
                                "type": "column",
                                "valueField": "visits", 
                                "title": "Indicador",
                                "balloonText": "[[title]]: <b style='font-size: 130%'>[[value]]</b>"
                            }, { 
                                "valueAxis": "v2",                                 
                                "lineThickness": 2, 
                                "lineColor": "#62cf73", 
                                "type": "line", 
                                "title": "Meta", 
                                "valueField": "meta", 
                                "balloonText": "[[title]]: <b style='font-size: 130%'>[[value]]</b>"
                            }],
                            "categoryField": "country",
                            "valueAxes": [{
                                "title": "Indicador"
                            }], 
                            "chartCursor": {
                                "pan": true,
                                "valueLineEnabled": true,
                                "valueLineBalloonEnabled": true,
                                "cursorAlpha": 0,
                                "valueLineAlpha": 0.2
                            },
                        });
                    </script>
                </div>     
                
                <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                    <script>
                        var chart = AmCharts.makeChart("jTWeek", {
                            "type": "serial",
                            "theme": "light",
                            "dataProvider": [
                                <?php 
                                if($sP > $sL){
                                    for ($i = $sP; $i <= $numSemanas; $i++) { 
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    },
                                <?php } 
                                    for($i = 1; $i <= $sL; $i++) { 
                                ?>
                                    {
                                        "country": "<?php echo 'CW '.$i ?>",
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    },
                                <?php } ?> 
                                <?php } else { 
                                    for ($i = $sP; $i <= $sL; $i++) { 
                                ?> 
                                    { 
                                        "country": "<?php echo 'CW '.$i ?>", 
                                        "visits": <?php echo $cwTec[$i] ?>,
                                        "meta": <?php echo $targetWeek[$i] ?>
                                    }, 
                                <?php } } ?> 
                            ], 
                            "graphs": [{ 
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "lineColor": "#02538b",
                                "type": "column",
                                "title": "Indicador",
                                "valueField": "visits",
                                "balloonText": "[[title]]: <b style='font-size: 130%'>[[value]]</b>"
                            }, { 
                                "valueAxis": "v2",                                 
                                "lineThickness": 2, 
                                "lineColor": "#62cf73", 
                                "type": "line", 
                                "title": "Meta", 
                                "valueField": "meta", 
                                "balloonText": "[[title]]: <b style='font-size: 130%'>[[value]]</b>"
                            }],
                            "categoryField": "country",
                            "valueAxes": [{
                                "title": "Inidicador"
                            }],
                            "chartCursor": {
                                "pan": true,
                                "valueLineEnabled": true,
                                "valueLineBalloonEnabled": true,
                                "cursorAlpha": 0,
                                "valueLineAlpha": 0.2
                            },
                        });
                    </script>                
                </div>    
                
                <!-- TABLA DE MESES -->
                <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                    <tr >
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >Mes</th>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $m[$i] ?></th>
                        <?php } ?>
                    </tr>
                    <tr>
                        <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $mCant[$i] ?></td>      
                    <?php } ?>
                    </tr>                
                </table> 
                
                <!-- TABLA DE SEMANA -->
                <table style="width: 35%; margin-left: 63%; margin-top: -35px; border: 1px solid #BABABA; ">
                    <tr>
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php }} ?>                            
                    </tr>
                    <tr>
                        <td align="center" style=" margin-left: 2%; background: #E8E8E8; border: 1px solid #BABABA;">Indicador</td>
                        <?php 
                        if($sP > $sL){
                            for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                            <?php } 
                            for ($i = 1; $i <= $sL; $i++ ) {
                            ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                            <?php
                            }
                        } else {                        
                            for ($i = $sP; $i <= $sL; $i++) { ?>
                                <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cwTec[$i] ?></td>
                        <?php }} ?>  
                    </tr>                
                </table> 
                <br>
                
                <div id="grafDays" class="jidokaDay" >                
                    <script>
                        var chart = AmCharts.makeChart("grafDays", {
                            "type": "serial",
                            "theme": "light",
                            "dataDateFormat": "YYYY-MM-DD",
                            "precision": 0,
                            "dataProvider": [
                            <?php  
                                for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){
                                    $date = explode("-", $i);                                 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                            ?>    
                            {
                                <?php if ($valueDayT[$vDate] > $targetDayT[$vDate]){?>
                                    "lineColor": "#CD4C47",
                                <?php } else {?>
                                    "lineColor": "#02538b",
                                <?php }?>
                                "date": "<?php echo "$fecha[$vDate]" ?>",                            
                                "production": <?php echo $valueDayT[$vDate]; ?>,
                                "meta": "<?php echo $targetDayT[$vDate]; ?>"                            
                            },
                            <?php } ?>
                            ],
                            "valueAxes": [{
                                "id": "v1",
                                "title": "Indicador",
                                "position": "left",
                                "autoGridCount": false,
                                "labelFunction": function(value) {
                                    return Math.round(value);
                                }
                            }],
                            "graphs": [{
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "lineColor": "#02538b",
                                "type": "column",
                                "title": "Indicador",
                                "valueField": "production",
                                "balloonText": "[[title]]: <b style='font-size: 130%'>[[value]]</b>"
                            }, {
                                "valueAxis": "v2",                                
                                "lineThickness": 2,
                                "lineColor": "#62cf73",
                                "type": "line",
                                "title": "Meta", 
                                "valueField": "meta",
                                "balloonText": "[[title]]: <b style='font-size: 130%'>[[value]]</b>"
                            }],    
                            "chartScrollbar": {
                                "graph": "g1",
                                "oppositeAxis": false,
                                "offset": 30,
                                "scrollbarHeight": 40,
                                "backgroundAlpha": 0,
                                "selectedBackgroundAlpha": 0.1,
                                "selectedBackgroundColor": "#888888",
                                "graphFillAlpha": 0,
                                "graphLineAlpha": 0.5,
                                "selectedGraphFillAlpha": 0,
                                "selectedGraphLineAlpha": 1,
                                "autoGridCount": true,
                                "color": "#AAAAAA"
                            },
                            "chartCursor": {
                                "pan": true,
                                "valueLineEnabled": true,
                                "valueLineBalloonEnabled": true,
                                "cursorAlpha": 0,
                                "valueLineAlpha": 0.2
                            },
                            "categoryField": "date",
                            "categoryAxis": {
                                "parseDates": true,
                                "dashLength": 1,
                                "minorGridEnabled": true
                            },
                            "legend": {                            
                                "autoMargins": false,
                                "borderAlpha": 0.2,
                                "equalWidths": false,
                                "horizontalGap": 50,
                                "verticalGap": 15,
                                "markerSize": 10,
                                "useGraphSettings": true,
                                "valueAlign": "left",
                                "valueWidth": 0
                            },
                            "balloon": {
                                "borderThickness": 1,
                                "shadowAlpha": 0
                            },
                            "panelsSettings": {
                              "usePrefixes": true
                            }
                        });
                    </script> 
                </div>  
                
                <div class="row" style="margin-top: 25px; width: 100%; " > 
                    <div id="holder-semple-1" style=" width: 100%; margin-left: 20px; " > 
                        <script id="tamplate-semple-1" type="text/mustache" > 
                            <table style="width:97%; margin-left: 1.5%; "class="inner-table" > 
                                <thead> 
                                    <tr> 
                                        <td colspan="2"> </td> 
                                        <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                                        <td rowspan="2" align="center" >Total</td> 
                                    </tr> 
                                    <tr> 
                                        <td>&nbsp;</td> 
                                        <td align="center" ></td>
                                        <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ ?>
                                            <td align="center" ><?php echo $cadDate[$i]; ?></td>
                                        <?php } ?> 
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php for($i = 1; $i <= $countProblem; $i++) { ?>
                                    <tr >
                                        <td align="right" ><?php echo $i ?></td>
                                        <td ><?php echo $problema[$i]?></td>
                                        <?php for($j = $_SESSION['FIni']; $j <= $_SESSION['FFin']; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                                            $date = explode("-", $j); 
                                            $vDate = $date[0].$date[1].(int)$date[2];                                  
                                        ?>
                                        <td align="center" ><?php echo $valueDayT[$vDate]; ?></td>
                                        <?php if ( $j == $_SESSION['FFin'] ) {?>   
                                        <td align="center" ><?php echo $totalProblema[$i]; ?> </td>    
                                    </tr >
                                    <?php }}} ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2">Sold Total</td>
                                        <?php for($i = $_SESSION['FIni']; $i <= $_SESSION['FFin']; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                                $date = explode("-", $i); 
                                                $vDate = $date[0].$date[1].(int)$date[2];
                                        ?>
                                            <td align="center" ><?php echo $totalDia[$vDate]; ?></td>
                                        <?php if ($i == $_SESSION['FFin']) { ?>                            
                                            <!--CUADRE DE INFORMACION -->
                                            <td align="center" ><?php echo $total; ?></td>
                                        <?php }} ?> 
                                    </tr> 
                                </tfoot> 
                            </table> 
                        </script>
                    </div>                     
                </div> 
            </div>  
        </div> 
    </body>