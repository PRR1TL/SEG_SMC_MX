
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">-->

<!-- LIBRERIAS Y CONFIGURACION PARA TABLA -->
<!--<script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
<script src="../../js/table-scroll.min.js"></script> -->
<script> 
    //CONFIGURACION DE LA TABLA 
    $(function () { 
        var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
        $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
            fixedColumnsLeft: 1, //CONTADOR Y FILA FIJOS 
            fixedColumnsRight: 0, //CABECERA FIJAS 
            columnsInScrollableArea: 22, //CANTIDAD DE DIAS A VER 
            scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
            scrollY: 0 //INICIO DE SCROLL LATERAL | 
        }); 
    }); 

    function getFixedColumnsData() {} 
</script>
        
<?php 
session_start();
include '../../db/ServerFunctions.php';

if (isset($_SESSION["aLinea"])) { 
    $linea = $_SESSION["aLinea"]; 
} else { 
    $linea = $_SESSION["linea"]; 
} 

//$fIni = date("Y-m-d", strtotime($_SESSION["aFIni"])); 
//$fFin = date("Y-m-d", strtotime($_SESSION["aFFin"]));     
$fIni = date("2019-06-01"); 
$fFin = date("Y-m-d");     

for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){        
    $date = explode("-", $i); 
    $vDate = $date[0].$date[1].(int)$date[2]; 

    $fecha[$vDate] = $i;
    $cadDate[$vDate] = date("M d", strtotime($i));
    $vMax[$vDate] = 0;
    $vMin[$vDate] = 0; 
    $vReal[$vDate] = 0; 
} 

$cInventario = cInventario($fIni, $fFin, $linea);
for ($i = 0; $i < count($cInventario); $i++ ){
    $date = explode("-", $cInventario[$i][0]->format('Y-m-d'));
    $vDate = $date[0].$date[1].(int)$date[2]; 
    
    $vMax[$vDate] = $cInventario[$i][1];
    $vMin[$vDate] = $cInventario[$i][2]; 
    $vReal[$vDate] = $cInventario[$i][3];  
    //echo '<br>',$vDate;
} 

//DATOS PARA TAMAÑO DE TABLA  
$dias = (strtotime($fIni)- strtotime($fFin))/86400;
$dias = abs($dias); 
$dias = floor($dias); 
//DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
//NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
if ($dias > 21 ) { 
    $rowspan = 22;
} else { 
    $rowspan = $dias+1;
} 

?>

<style>
    #chartdiv {
        width: 100%;
        height: 500px;
    }	
    
    table,td {
    	border: 1px solid black;
    }
    
    thead{
        background: #eaeded;
    }
    
</style>
<br>
<div id="holder-semple-1"> 
    <script id="tamplate-semple-1" type="text/mustache"> 
        
        <table style="width:97%; margin-left: 1.5%; "class="inner-table"> 
            <thead> 
                <tr> 
                    <td > </td> 
                    <td align="center" colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                </tr> 
                <tr> 
                    <td>&nbsp;</td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?> 
                    <td align="center" ><?php echo $cadDate[$vDate]; ?></td> 
                    <?php } ?> 
                </tr> 
            </thead> 
            <tbody> 
                <tr >
                    <td align="right" >OEE</td> 
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td align="center" ><?php echo $vReal[$vDate]; ?></td>
                    <?php } ?>      
                </tr >
                <tr >
                    <td align="right" >TECNICAS</td>
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td align="center" ><?php echo $vMax[$vDate]; ?></td>
                    <?php } ?>      
                </tr >
                <tr >
                    <td align="right" >ORGANIZACIONALES</td>
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td align="center" ><?php echo $vMin[$vDate]; ?></td>
                    <?php } ?>      
                </tr >
                <tr >
                    <td align="right" >CALIDAD</td>
                    <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        $date = explode("-", $j); 
                        $vDate = $date[0].$date[1].(int)$date[2];                                  
                    ?>
                    <td align="center" ><?php echo $vMin[$vDate]; ?></td>
                    <?php } ?>      
                </tr >
            </tbody>
        </table>
    </script>
</div> 



