<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
    session_start(); 
    $date = new DateTime; 
    
    if (isset($_SESSION["mesTarget"]) && isset($_SESSION["anioTarget"])) { 
        $mes = $_SESSION["mesTarget"]; 
        $anio = $_SESSION["anioTarget"]; 
    } else { 
        $mes = date("m"); 
        $anio = date("Y"); 
    } 
    
    $day = 1; 
    
    $date->setISODate("$anio", 53); 
    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52 
    if($date->format("W") == 53) { 
        $nWY = 53; 
    } else { 
        $nWY = 52; 
    } 
        
    $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio)); 
    # Obtenemos la semana del primer dia del mes 
    $primeraSemana = date("W",mktime(0,0,0,$mes,1,$anio)); 
    # Obtenemos la semana del ultimo dia del mes 
    $ultimaSemana = date("W",mktime(0,0,0,$mes,$ultimoDiaMes+1,$anio)); 
    $cW = $ultimaSemana - $primeraSemana; 
    
    if ($ultimaSemana > $primeraSemana ){
        for ($i = 0; $i <= $cW; $i++) { 
            $day = ($i * 7) +1 ; 
            $semana = date("W",mktime(0,0,0,$mes,$day,$anio)); 
            $diaSemana = date("w",mktime(0,0,0,$mes,$day,$anio)); 

            # el 0 equivale al domingo... 
            if($diaSemana == 0) 
                $diaSemana = 7; 
            
            # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
            $primerDia = date("d-m-Y",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
            
            # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
            $ultimoDia = date("d-m-Y",mktime(0,0,0,$mes,$day+(7-$diaSemana-1),$anio)); 
            
            $lblDI = date("d", strtotime($primerDia)); 
            if (date("d", strtotime($primerDia)) > 20 && $i == 0 ){ 
                $lblDI = '01'; 
            } 

            $lblDF = date("d", strtotime($ultimoDia)); 
           //echo $i,', ',$cW,': ',$ ;
            if ( date("d", strtotime($ultimoDia)) < 8 && $i == $cW ) { 
                $lblDF = $ultimoDiaMes; 
            } 

            ?> 
            <div class="form-row" > 
                <div align="center" class="col-md-2" id="linea" > 
                    <label class="my-1 mr-2" > <?php echo $semana; ?> </label> 
                </div> 
                <div align="center" class="col-md-3" id="linea" > 
                    <label class="my-1 mr-2" > <?php echo $lblDI.'-'.$lblDF; ?> </label> 
                </div> 
                <div class="col-md-4" id="linea" > 
                    <input class="form-control" id="<?php echo "meta".$semana; ?>" name="<?php echo "meta".$semana; ?>" minlength="1" maxlength="4" style="width: 80%" placeholder="550" type="text" />
                </div> 
                <div align="center" class="col-md-1 mb-2" > 
                    <input id="<?php echo "cW".$semana; ?>" name="<?php echo "cW".$semana; ?>" style="margin-left: -15px" type="radio" value="1" checked > 
                </div> 
                <div align="center" class="col-md-1 mb-2" > 
                    <input id="<?php echo "cW".$semana; ?>" name="<?php echo "cW".$semana; ?>" style="margin-left: 15px" type="radio" name="cWeek" value="0" > 
                </div> 
            </div> 

            <?php 
        } 
    } else { 
        $cont = 0; 
        for ($i = $primeraSemana; $i <= $nWY; $i++) { 
            $day = ($cont * 7) + 1; 
            $semana = date("W",mktime(0,0,0,$mes,$day,$anio)); 
            $diaSemana = date("w",mktime(0,0,0,$mes,$day,$anio)); 
            
            # el 0 equivale al domingo... 
            if($diaSemana == 0) 
                $diaSemana = 7; 
            
            # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
            $primerDia = date("d-m-Y",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
            
            # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
            $ultimoDia = date("d-m-Y",mktime(0,0,0,$mes,$day+(7-$diaSemana-1),$anio)); 
            
            $lblDI = date("d", strtotime($primerDia)); 
            if (date("d", strtotime($primerDia)) > 20 && $i == 0 ){ 
                $lblDI = '01'; 
            } 
            
            $lblDF = date("d", strtotime($ultimoDia)); 
            if ( date("d", strtotime($ultimoDia)) < 8 && $i == $cW ){ 
                $lblDF = $ultimoDiaMes; 
            } 

            ?> 
            <div class="form-row" > 
                <div align="center" class="col-md-2" id="linea" > 
                    <label class="my-1 mr-2" > <?php echo $semana; ?> </label> 
                </div> 
                <div align="center" class="col-md-3" id="linea" > 
                    <label class="my-1 mr-2" > <?php echo $lblDI.'-'.$lblDF; ?> </label> 
                </div> 
                <div class="col-md-4" id="linea" > 
                    <input class="form-control" id="<?php echo "meta".$semana; ?>" name="<?php echo "meta".$semana; ?>" minlength="1" maxlength="4" style="width: 80%" placeholder="80" type="text" />
                </div> 
                <div align="center" class="col-md-1 mb-2" > 
                    <input id="<?php echo "cW".$semana; ?>" name="<?php echo "cW".$semana; ?>" style="margin-left: -15px" type="radio" value="1" checked > 
                </div> 
                <div align="center" class="col-md-1 mb-2" > 
                    <input id="<?php echo "cW".$semana; ?>" name="<?php echo "cW".$semana; ?>" style="margin-left: 15px" type="radio" name="cWeek" value="0" > 
                </div> 
            </div>

            <?php 
            $cont++;
        } 
        
        for ($i = 1; $i <= $ultimaSemana; $i++) { 
            $day = ($cont * 7) +1 ; 
            $semana = date("W",mktime(0,0,0,$mes,$day,$anio)); 
            $diaSemana = date("w",mktime(0,0,0,$mes,$day,$anio)); 
            
            # el 0 equivale al domingo... 
            if($diaSemana == 0) 
                $diaSemana = 7; 
            
            # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
            $primerDia = date("d-m-Y",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 

            # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
            $ultimoDia = date("d-m-Y",mktime(0,0,0,$mes,$day+(7-$diaSemana-1),$anio)); 
            
            $lblDI = date("d", strtotime($primerDia)); 
            if (date("d", strtotime($primerDia)) > 20 && $i == 0 ){ 
                $lblDI = '01'; 
            } 

            $lblDF = date("d", strtotime($ultimoDia)); 
            if ( date("d", strtotime($ultimoDia)) < 8 && $i == $cW ){ 
                $lblDF = $ultimoDiaMes; 
            } 

            ?> 
            <div class="form-row" > 
                <div align="center" class="col-md-2" id="linea" > 
                    <label class="my-1 mr-2" > <?php echo $semana; ?> </label> 
                </div> 
                <div align="center" class="col-md-3" id="linea" > 
                    <label class="my-1 mr-2" > <?php echo $lblDI.'-'.$lblDF; ?> </label> 
                </div> 
                <div class="col-md-4" id="linea" > 
                    <input class="form-control" id="<?php echo "meta".$semana; ?>" name="<?php echo "meta".$semana; ?>" minlength="1" maxlength="4" style="width: 80%" placeholder="80" type="text" />
                </div> 
                <div align="center" class="col-md-1 mb-2" > 
                    <input id="<?php echo "cW".$semana; ?>" name="<?php echo "cW".$semana; ?>" style="margin-left: -15px" type="radio" value="1" checked > 
                </div> 
                <div align="center" class="col-md-1 mb-2" > 
                    <input id="<?php echo "cW".$semana; ?>" name="<?php echo "cW".$semana; ?>" style="margin-left: 15px" type="radio" name="cWeek" value="0" > 
                </div> 
            </div>

            <?php 
            $cont++;
        } 
        
    } 
    
?> 