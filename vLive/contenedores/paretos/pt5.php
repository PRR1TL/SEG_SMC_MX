<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?> 
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <?php
        include '../../db/ServerFunctions.php';
        session_start();
        //PRIMER APARTADO EL ACTUAL
        $linea = $_SESSION["linea"];
        $tipo = $_POST["tipoDato"];
        $fI = $_POST["fIni"];
        $fF = $_POST["fFin"];
        
        //$linea = 'L024';
  
        //CONVERSION DE FECHA
        $fIni = date("Y-m-d", strtotime($fI));
        $fFin = date("Y-m-d", strtotime($fF));
        
        if ($fIni > $fFin){
            $fIni = date("Y-m-d", strtotime($fF));
            $fFin = date("Y-m-d", strtotime($fI));            
        } 
        
        switch ($tipo){
            case 0; //por defecto
            case 1;// por duracion 
                $dattop3 = t3Calidad($linea,$fIni,$fFin);
                for($i = 0; $i < count($dattop3); $i++){ 
                    $opCali[$i] = $dattop3[$i][0]; 
                    $problemCali[$i] = $dattop3[$i][1]; 
                    $dat1Cali[$i] = $dattop3[$i][2]; 
                    $dat2Cali[$i] = $dattop3[$i][3]; 
                }

                //echo "<br><br><br><br>";
                $dattop5 = t5TecnicasYOrganizacionales($linea,$fIni,$fFin); 
                for ($i = 0; $i < count($dattop5); $i++) { 
                    $pzasTecyOrg[$i] = 0;
                    $x = 0;
                    $opTecOrg[$i] = $dattop5[$i][0]; 
                    $problemTecOrg[$i] = $dattop5[$i][1]; 
                    $dat1TecOrg[$i] = $dattop5[$i][2]; 
                    $dat2TecOrg[$i] = $dattop5[$i][3]; 
                    $dat3TecOrg[$i] = 0; 
                    
                    #APARTADO PARA PIEZAS                     
                    $datNoParte = t5TecnicasYOrganizacionalesNoParte($linea, $fIni, $fFin, $problemTecOrg[$i], $opTecOrg[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] = $durNoParte[$x] * 60; 
                        $datTcNoParte = t3Tc($linea, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = (int) floor($datTcNoParte[$k][0]); 
                             // TRANSFORMACION DE TIEMPO A PZAS 
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN); 
                            $pzasTecyOrg[$i] = $pzasTecyOrg[$i] + $pzasNP[$k]; 
                        } 
                        $dat3TecOrg[$i] = $pzasTecyOrg[$i]; 
                        $x++; 
                    }
                    //echo '<br>',$linea,', ', $fIni,', ', $fFin,', ', $problemTecOrg[$i],', ', $dat1TecOrg[$i],', ', $dat2TecOrg[$i],', ', $dat3TecOrg[$i];
                } 
                
                //echo '<br><br><br><br><br>';
                $dattop1 = t1pareto($linea,$fIni,$fFin); 
                for ($i = 0; $i < count($dattop1); $i++) { 
                    $pzasCam[$i] = 0; 
                    $x = 0;
                    $cambio[$i] = $dattop1[$i][0];
                    $dat1Cam[$i] = $dattop1[$i][1];
                    $dat2Cam[$i] = $dattop1[$i][2];
                    $dat3Cam[$i] = 0;
                    $seg[$x] = 0;
                    
                    #APARTADO PARA PIEZAS
                    //echo $line, $fIni, $fFin, $problemaTec[$i];
                    $datNoParte = t3CambioNoParte($linea, $fIni, $fFin, $cambio[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++) { 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] += $durNoParte[$x] * 60; 

                        $datTcNoParte = t3Tc($linea, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++) { 
                            $tcTec[$k] = ceil($datTcNoParte[$k][0]); 
                            // TRANSFORMACION DE TIEMPO A PZAS
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
                            $pzasCam[$i] = $pzasCam[$i] + $pzasNP[$k];
                            //echo '<br>',$seg[$x],', ',$tcTec[$k],', ',$pzasCam[$i];
                        }  
                        $x++;
                    } 
                    $dat3Cam[$i] = $pzasCam[$i]; 
                }  
                                
                $tituloTop1[0] = "Top 1: Cambio de modelo (Duración)";
                $tituloTop3[0] = "Top 3: NO Calidad (Duración)";
                $tituloTop5[0] = "Top 5: Técnicas y organizacionales (Duración)";
                $band = 1;
                break;
                
            case 2;// por frecuencia                  
                $dattop3 = t3CalidadFrec($linea,$fIni,$fFin);
                for($i = 0; $i < count($dattop3); $i++) {
                    $opCali[$i] = $dattop3[$i][0]; 
                    $problemCali[$i] = $dattop3[$i][1]; 
                    $dat2Cali[$i] = $dattop3[$i][3]; 
                    $dat1Cali[$i] = $dattop3[$i][2]; 
                    
                    #APARTADO PARA PIEZAS
                    
                }

                $dattop5 = t5TecnicasYOrganizacionalesFrec($linea,$fIni,$fFin);
                for ($i = 0; $i < count($dattop5); $i++) { 
                    $pzasTecyOrg[$i] = 0; 
                    $x = 0; 
                    $opTecOrg[$i] = $dattop5[$i][0]; 
                    $problemTecOrg[$i] = $dattop5[$i][1]; 
                    $dat2TecOrg[$i] = $dattop5[$i][3]; 
                    $dat1TecOrg[$i] = $dattop5[$i][2]; 
                    $dat3TecOrg[$i] = 0; 
                    
                    #APARTADO PARA PIEZAS                     
                    $datNoParte = t5TecnicasYOrganizacionalesNoParte($linea, $fIni, $fFin, $problemTecOrg[$i], $opTecOrg[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] = $durNoParte[$x] * 60; 
                        $datTcNoParte = t3Tc($linea, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = $datTcNoParte[$k][0]; 
                            //TRANSFORMACION DE TIEMPO A PZAS 
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN); 
                            $pzasTecyOrg[$i] = $pzasTecyOrg[$i] + $pzasNP[$k]; 
                        } 
                        $dat3TecOrg[$i] = $pzasTecyOrg[$i]; 
                        $x++; 
                    }
                }

                $dattop1 = t1paretoFrec($linea,$fIni,$fFin);
                for ($i = 0; $i < count($dattop1); $i++){
                    $pzasCali[$i] = 0;
                    $cambio[$i] = $dattop1[$i][0];
                    $dat2Cam[$i] = $dattop1[$i][2];
                    $dat1Cam[$i] = $dattop1[$i][1];
                    $dat3Cam[$i] = 0;
                    
                    #APARTADO PARA PIEZAS
                    $dat3[$i] = 0; 
                    $datNoParte = t3CambioNoParte($linea, $fIni, $fFin, $cambio[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1];
                        $seg[$x] = $durNoParte[$x] * 60;

                        $datTcNoParte = t3Tc($linea, $noParte[$x]);
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = ceil($datTcNoParte[$k][0]);
                            // TRANSFORMACION DE TIEMPO A PZAS
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
                            $pzasCali[$i] = $pzasCali[$i] + $pzasNP[$k];
                            
                        }  
                        $x++;
                    } 
                    $dat3Cam[$i] = $pzasCali[$i]; 
                }  
                
                $tituloTop1[0] = "Top 1: Cambio de modelo (Frecuencia)"; 
                $tituloTop3[0] = "Top 3: NO Calidad (Frecuencia)"; 
                $tituloTop5[0] = "Top 5: Tecnicas y organizacionales (Frecuencia)"; 
                $band = 2;
                break;
                
            case 3;// por piezas$dattop3 = t3CambioModelo($linea,$fIni,$fFin);
                $dattop3 = t3Calidad($linea,$fIni,$fFin);
                for($i = 0; $i < count($dattop3); $i++){ 
                    $opCali[$i] = $dattop3[$i][0]; 
                    $problemCali[$i] = $dattop3[$i][1]; 
                    $dat1Cali[$i] = $dattop3[$i][2]; 
                    $dat2Cali[$i] = $dattop3[$i][3]; 
                }

                //echo "<br><br><br><br>";
                $dattop5 = t5TecnicasYOrganizacionales($linea,$fIni,$fFin); 
                for ($i = 0; $i < count($dattop5); $i++) { 
                    $pzasTecyOrg[$i] = 0;
                    $x = 0;
                    $opTecOrg[$i] = $dattop5[$i][0]; 
                    $problemTecOrg[$i] = $dattop5[$i][1]; 
                    $dat2TecOrg[$i] = $dattop5[$i][2]; 
                    $dat3TecOrg[$i] = $dattop5[$i][3]; 
                    $dat1TecOrg[$i] = 0; 
                    
                    #APARTADO PARA PIEZAS                     
                    $datNoParte = t5TecnicasYOrganizacionalesNoParte($linea, $fIni, $fFin, $problemTecOrg[$i], $opTecOrg[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] = $durNoParte[$x] * 60; 
                        $datTcNoParte = t3Tc($linea, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = (int) floor($datTcNoParte[$k][0]); 
                             // TRANSFORMACION DE TIEMPO A PZAS 
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN); 
                            $pzasTecyOrg[$i] = $pzasTecyOrg[$i] + $pzasNP[$k]; 
                        } 
                        $dat1TecOrg[$i] = $pzasTecyOrg[$i]; 
                        $x++; 
                    }
                    //echo '<br>',$linea,', ', $fIni,', ', $fFin,', ', $problemTecOrg[$i],', ', $dat1TecOrg[$i],', ', $dat2TecOrg[$i],', ', $dat3TecOrg[$i];
                } 
                
                //echo '<br><br><br><br><br>';
                $dattop1 = t1pareto($linea,$fIni,$fFin); 
                for ($i = 0; $i < count($dattop1); $i++) { 
                    $pzasCam[$i] = 0; 
                    $x = 0;
                    $cambio[$i] = $dattop1[$i][0];
                    $dat2Cam[$i] = $dattop1[$i][1];
                    $dat3Cam[$i] = $dattop1[$i][2];
                    $dat1Cam[$i] = 0;
                    $seg[$x] = 0;
                    
                    #APARTADO PARA PIEZAS
                    //echo $line, $fIni, $fFin, $problemaTec[$i];
                    $datNoParte = t3CambioNoParte($linea, $fIni, $fFin, $cambio[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++) { 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] += $durNoParte[$x] * 60; 

                        $datTcNoParte = t3Tc($linea, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++) { 
                            $tcTec[$k] = ceil($datTcNoParte[$k][0]); 
                            // TRANSFORMACION DE TIEMPO A PZAS
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
                            $pzasCam[$i] = $pzasCam[$i] + $pzasNP[$k];
                            //echo '<br>',$seg[$x],', ',$tcTec[$k],', ',$pzasCam[$i];
                        }  
                        $x++;
                    } 
                    $dat1Cam[$i] = $pzasCam[$i]; 
                }  
                                
                $tituloTop1[0] = "Top 1: Cambio de modelo (Piezas)";
                $tituloTop3[0] = "Top 3: NO Calidad (Piezas)";
                $tituloTop5[0] = "Top 5: Técnicas y organizacionales (Piezas)";
                $band = 3;
                break;
        }         
        
    ?>   
    
    <div class="container" style="margin-top: 5%; width: 110%; height: 150%; " > 
        <div class="row" > 
            <div class="col-lg-6 col-md-6 col-xs-6 col-sh-6" style="border-style: solid; border-width: 1px; "> 
                <h6 class="contenidoCentrado"> <strong> <?php echo $tituloTop5[0] ?> </strong></h6>
                <div aling = "center" id="pttecorg" class="grafTop3" style="width: 90%; height: 50%"> 
                    <script>
                        var chart = AmCharts.makeChart("pttecorg", {
                            "type": "serial",
                            "theme": "light",
                            "rotate": true,
                            "dataProvider": [
                            <?PHP for($i = 0 ;$i < count($dattop5);$i++){?>
                                {
                                    "country": "<?php echo "$opTecOrg[$i],$problemTecOrg[$i]";?>",
                                    "visits": "<?php echo $dat1TecOrg[$i]; ?>"
                                },
                            <?php } ?>
                            ],
                            "graphs": [{
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "type": "column",
                                "valueField": "visits",
                                "lineColor": "#02538b"
                            }],

                            "categoryField": "country",
                            "categoryAxis": {
                                "reverse": true
                            }
                        });
                    </script>
                </div>

                <div class="col-md-12" style=" margin-top: -1.5%; width: 100%; ">
                    <div aling = "center" style="align-content: center; text-align: center; font-size: 12px;" class="col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
                        <table style="width: 100%; font-size: 12px; " class="table table-bordered col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
                            <thead style="background-color: #eaeded;" class="contenidoCentrado"> 
                                <tr> 
                                    <th >Descripción </th> 
                                <?php switch ($tipo){ 
                                    case 2: ?>
                                    <th >Frecuencia </th> 
                                    <th >Duracion </th>                        
                                    <th >Piezas</th> 
                                    <?php break; 
                                    case 3:
                                        ?>
                                    <th >Piezas</th>                         
                                    <th >Duracion </th>
                                    <th >Frecuencia </th> 
                                    <?php break;  
                                    default :?>
                                    <th >Duracion </th> 
                                    <th >Frecuencia </th> 
                                    <th >Piezas</th> 
                                <?php 
                                    break;
                                } ?>                        </tr> 
                            </thead> 
                            <tbody > 
                            <?php for ($i = 0; $i < count($dattop5); $i++) { ?> 
                                <tr> 
                                    <td width='55%' > <?php echo $problemTecOrg[$i]; ?> </td> 
                                    <td > <?php echo $dat1TecOrg[$i]; ?> </td> 
                                    <td > <?php echo $dat2TecOrg[$i]; ?> </td> 
                                    <td > <?php echo $dat3TecOrg[$i]; ?> </td> 
                                </tr> 
                            <?php } ?> 
                            </tbody> 
                        </table> 
                    </div>                 
                </div>
            </div> 
            <div class="col-lg-6 col-md-6 col-xs-6 col-sh-6" style="border-style: solid; border-width: 1px;"> 
                <h6 class="contenidoCentrado "><strong> <?php echo $tituloTop1[0] ?> </strong></h6> 
                <div aling = "center" id="ptCamb" class="grafTop3" style="width: 90%; height: 9%"> 
                    <script> 
                        var chart = AmCharts.makeChart("ptCamb", {
                            "type": "serial",
                            "theme": "light",
                            "rotate": true,
                            "dataProvider": [
                            <?PHP for($i = 0 ;$i < count($dattop1);$i++){?>
                                {
                                    "country": "<?php echo "$cambio[$i]";?>",
                                    "visits": "<?php echo $dat1Cam[$i]; ?>"
                                },
                            <?php } ?>
                            ],
                            "graphs": [{ 
                                "fillAlphas": 0.9, 
                                "lineAlpha": 0.2, 
                                "type": "column", 
                                "valueField": "visits", 
                                "lineColor": "#02538b" 
                            }], 
                            "categoryField": "country", 
                            "categoryAxis": { 
                                "reverse": true
                            } 
                        }); 
                    </script> 
                </div> 
                
                <!-- TABLA DE CAMBIO --> 
                <div class="col-md-12" > 
                    <div aling = "center" style="align-content: center; text-align: center; font-size: 12px; " class="col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
                        <table style="width: 100%; font-size: 12px;" class="table table-bordered col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
                            <thead style="background-color: #eaeded;" class="contenidoCentrado"> 
                                <tr> 
                                    <th >Descripción </th> 
                                <?php switch ($tipo){ 
                                    case 2: ?>
                                    <th >Frecuencia </th> 
                                    <th >Duracion </th>                        
                                    <th >Piezas</th> 
                                    <?php break; 
                                    case 3:
                                        ?>
                                    <th >Piezas</th>                         
                                    <th >Duracion </th>
                                    <th >Frecuencia </th> 
                                    <?php break;  
                                    default :?>
                                    <th >Duracion </th> 
                                    <th >Frecuencia </th> 
                                    <th >Piezas</th> 
                                <?php 
                                    break;
                                } ?>
                                </tr> 
                            </thead> 
                            <tbody > 
                            <?php for ($i = 0; $i < count($dattop1); $i++) { ?> 
                                <tr> 
                                    <td width='60%' > <?php echo $cambio[$i] ?> </td> 
                                    <td > <?php echo $dat1Cam[$i]; ?> </td> 
                                    <td > <?php echo $dat2Cam[$i]; ?> </td> 
                                    <td > <?php echo $dat3Cam[$i]; ?> </td> 
                                </tr> 
                            <?php } ?> 
                            </tbody> 
                        </table> 
                    </div> 
                </div>
                
                <h6 class="contenidoCentrado"> <strong> <?php echo $tituloTop3[0] ?> </strong></h6> 
                <div aling = "center" id="ptCali" class="grafTop3" style="width: 90%; height: 26%; "> 
                    <script>
                        var chart = AmCharts.makeChart("ptCali", { 
                            "type": "serial",
                            "theme": "light",
                            "rotate": true,
                            "dataProvider": [
                            <?PHP for($i = 0 ; $i < count($dattop3); $i++){ ?>
                                {
                                    "country": "<?php echo "$problemCali[$i]";?>",
                                    "visits": "<?php echo $dat1Cali[$i]; ?>"
                                },
                            <?php } ?>
                            ],
                            "graphs": [{ 
                                "fillAlphas": 0.9,
                                "lineAlpha": 0.2,
                                "type": "column",
                                "valueField": "visits",
                                "lineColor": "#02538b"
                            }],
                            "categoryField": "country",
                            "categoryAxis": {
                                "reverse": true
                            }
                        });
                    </script>
                </div> 
                
                <!--TABLA DE CALIDAD--> 
                <div class="col-md-12" > 
                    <div aling = "center" style="align-content: center; text-align: center; font-size: 12px; " class="col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
                        <table style="width: 100%; font-size: 12px; " class="table table-bordered col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
                            <thead style="background-color: #eaeded;" class="contenidoCentrado"> 
                                <tr> 
                                    <th >Descripción </th> 
                                <?php switch ($tipo){ 
                                    case 2: ?>
                                    <th >Frecuencia </th>                        
                                    <th >Piezas</th> 
                                    <?php break; 
                                    case 3:
                                        ?>
                                    <th >Piezas</th>           
                                    <th >Frecuencia </th> 
                                    <?php break;  
                                    default :?>                                    
                                    <th >Piezas</th> 
                                    <th >Frecuencia </th> 
                                <?php 
                                    break;
                                } ?>                        </tr> 
                            </thead> 
                            <tbody > 
                            <?php for ($i = 0; $i < count($dattop3); $i++) { ?> 
                                <tr> 
                                    <td width='55%' > <?php echo $problemCali[$i]; ?> </td> 
                                    <td > <?php echo $dat1Cali[$i]; ?> </td> 
                                    <td > <?php echo $dat2Cali[$i]; ?> </td> 
                                </tr> 
                            <?php } ?> 
                            </tbody> 
                        </table>      
                    </div>
                </div> 
            <br><br><br><br><br><br>
            </div> 
        <br><br><br><br><br><br>
        </div> 
        <br><br><br><br><br><br>
    </div> 
    