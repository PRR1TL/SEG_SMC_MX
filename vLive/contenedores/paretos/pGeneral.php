<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

    <!--<LINK REL=StyleSheet HREF="../../css/tops.css" TYPE="text/css" MEDIA=screen>-->
    <title>Top 5: Cambio de modelo</title>
    <link href="../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <?php
        include '../../db/ServerFunctions.php';
        session_start();
        //PRIMER APARTADO EL ACTUAL
        $linea = $_SESSION["linea"];        
        
        $tipo = $_POST["tipoDato"]; 
        $fI = $_POST["fIni"]; 
        $fF = $_POST["fFin"]; 
        
        //CONVERSION DE FECHA
        $fIni = date("Y-m-d", strtotime($fI));
        $fFin = date("Y-m-d", strtotime($fF));
        
        if ($fIni > $fFin) {
            $fIni = date("Y-m-d", strtotime($fF));
            $fFin = date("Y-m-d", strtotime($fI)); 
        } 
        
        switch ($tipo){
            case 0; //por defecto
            case 1;// por duracion 
                $dattop5 = t5General($linea, $fIni, $fFin);
                for ($i = 0; $i < count($dattop5); $i++) { 
                    $x = 0;
                    $pzasTecyOrg[$i] = 0;
                    $tema[$i] = $dattop5[$i][0];
                    $op[$i] = $dattop5[$i][1];
                    $probl[$i] = $dattop5[$i][2];
                    $detalleM[$i] = $dattop5[$i][3];
                    $dat1[$i] = $dattop5[$i][4];
                    $dat2[$i] = $dattop5[$i][5];
                    $dat3[$i] = 0; 
                    
                    #APARTADO PARA PIEZAS
                    //echo '<br>',$linea,', ', $fIni,', ', $fFin,', ', $probl[$i],', ', $op[$i],', ';
                    $datNoParte = t5TecnicasYOrganizacionalesNoParte($linea, $fIni, $fFin, $probl[$i], $op[$i]); 
                    //echo count($datNoParte);
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] = $durNoParte[$x] * 60; 
                        $datTcNoParte = t3Tc($linea, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = (int) floor($datTcNoParte[$k][0]); 
                            
                            //TRANSFORMACION DE TIEMPO A PZAS 
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN); 
                            $pzasTecyOrg[$i] = $pzasTecyOrg[$i] + $pzasNP[$k]; 
                        } 
                        $dat3[$i] = $pzasTecyOrg[$i]; 
                        $x++; 
                    } 
                } 
                
                $band = 1;
                $titulo[0] = "TOP General (Duración)";
                break;
                
            case 2;// por frecuencia 
                $dattop5 = t5GeneralFrec($linea, $fIni, $fFin);
                for ($i = 0; $i < count($dattop5); $i++){
                    $x = 0;
                    $pzasTecyOrg[$i] = 0;
                    $tema[$i] = $dattop5[$i][0]; 
                    $op[$i] = $dattop5[$i][1]; 
                    $probl[$i] = $dattop5[$i][2]; 
                    $detalleM[$i] = $dattop5[$i][3]; 
                    $dat1[$i] = $dattop5[$i][4];
                    $dat2[$i] = $dattop5[$i][5];
                    $dat3[$i] = 0;
                    
                    #APARTADO PARA PIEZAS
                    $datNoParte = t5TecnicasYOrganizacionalesNoParte($linea, $fIni, $fFin, $probl[$i], $op[$i]); 
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] = $durNoParte[$x] * 60; 
                        $datTcNoParte = t3Tc($linea, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = (int) floor($datTcNoParte[$k][0]); 
                             // TRANSFORMACION DE TIEMPO A PZAS 
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN); 
                            $pzasTecyOrg[$i] = $pzasTecyOrg[$i] + $pzasNP[$k]; 
                        } 
                        $dat3[$i] = $pzasTecyOrg[$i]; 
                        $x++; 
                    }
                }
                
                $band = 2;
                $titulo[0] = "TOP General (Frecuencia)";
                break;
                
            case 3;// por piezas
                $dattop5 = t5General($linea, $fIni, $fFin);
                for ($i = 0; $i < count($dattop5); $i++) { 
                    $x = 0;
                    $pzasTecyOrg[$i] = 0;
                    $tema[$i] = $dattop5[$i][0];
                    $op[$i] = $dattop5[$i][1];
                    $probl[$i] = $dattop5[$i][2];
                    $detalleM[$i] = $dattop5[$i][3];
                    $dat2[$i] = $dattop5[$i][4];
                    $dat3[$i] = $dattop5[$i][5];
                    $dat1[$i] = 0; 
                    
                    #APARTADO PARA PIEZAS
                    //echo '<br>',$linea,', ', $fIni,', ', $fFin,', ', $probl[$i],', ', $op[$i],', ';
                    $datNoParte = t5TecnicasYOrganizacionalesNoParte($linea, $fIni, $fFin, $probl[$i], $op[$i]); 
                    //echo count($datNoParte);
                    for($j = 0;  $j < count($datNoParte); $j++){ 
                        $noParte[$x] = (string) $datNoParte[$j][0]; 
                        $durNoParte[$x] = $datNoParte[$j][1]; 
                        $seg[$x] = $durNoParte[$x] * 60; 
                        $datTcNoParte = t3Tc($linea, $noParte[$x]); 
                        for($k = 0; $k < count($datTcNoParte); $k++){ 
                            $tcTec[$k] = (int) floor($datTcNoParte[$k][0]); 
                            
                            //TRANSFORMACION DE TIEMPO A PZAS 
                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN); 
                            $pzasTecyOrg[$i] = $pzasTecyOrg[$i] + $pzasNP[$k]; 
                        } 
                        $dat1[$i] = $pzasTecyOrg[$i]; 
                        $x++; 
                    } 
                } 
                
                $band = 3;
                $titulo[0] = "TOP General (Piezas)";
                break;
        }
        
        //echo $band,', ', count($dattop1),', ', count($dattop3),', ', count($dattop5),'<br>';
        
    ?>
    <br>
    <div class="col">
        <div class="col-md-12">
            <div aling = "center" id="ptc" class="grafTop3" style="width: 90%; height: 50%"> 
                <script>
                    var chart = AmCharts.makeChart("ptc", {
                        "type": "serial",
                        "theme": "light",
                        "rotate": true,
                        "dataProvider": [
                        <?PHP for($i = 0 ;$i < count($dattop5);$i++){?>
                            {
                                "country": "<?php echo "$op[$i], $probl[$i]";?>",
                                "visits": "<?php echo $dat1[$i]; ?>"
                            },
                        <?php } ?>
                        ],
                        "graphs": [{
                            "fillAlphas": 0.9,
                            "lineAlpha": 0.2,
                            "type": "column",
                            "valueField": "visits",
                            "lineColor": "#02538b"
                        }],
                    
                        "categoryField": "country",
                        "categoryAxis": {
                            "reverse": true
                        }
                    });
                </script>
            </div>
        </div>  
    </div>
    
    <br>
    
    <div class="col-lg-12" >
        <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2 contenidoCentrado" ></div>
        <div aling = "center" style="align-content: center; text-align: center; margin-left: 9% " class="col-xs-10 col-sh-10 col-md-10 col-lg-10 contenidoCentrado" >
            <table style="width: 100%; " class="table table-bordered col-xs-12 col-sh-12 col-md-12 col-lg-12 " >
                <thead style="background-color: #eaeded;" class="contenidoCentrado"> 
                    <tr> 
                        <th >Tema</th> 
                        <th >Operacion</th> 
                        <th >Descripción </th> 
                    <?php switch ($tipo){ 
                        case 1: ?>
                        <th >Duracion </th>                        
                        <th >Frecuencia </th>                        
                        <th >Piezas </th> 
                        <?php break; 
                        case 2:
                            ?>
                        <th >Frecuencia </th> 
                        <th >Duracion </th>                                                
                        <th >Piezas </th> 
                        <?php break;  
                        case 3:
                            ?>
                        <th >Piezas </th> 
                        <th >Duracion </th>                        
                        <th >Frecuencia </th> 
                        <?php break;  
                        default :?> 
                        <th >Duracion </th>                         
                        <th >Frecuencia </th> 
                        <th >Piezas</th> 
                    <?php 
                        break;
                    } ?> 
                    </tr> 
                </thead> 
                <tbody > 
                <?php for ($i = 0; $i < count($dattop5); $i++) { ?> 
                    <tr> 
                        <td > <?php echo $tema[$i] ; ?> </td> 
                        <td > <?php echo $op[$i]; ?> </td> 
                        <td width='55%' > <?php echo $probl[$i]; ?> </td> 
                        <td > <?php echo $dat1[$i]; ?> </td> 
                        <td > <?php echo $dat2[$i]; ?> </td> 
                        <td > <?php echo $dat3[$i]; ?> </td> 
                    </tr> 
                <?php } ?> 
                </tbody> 
            </table>      
        </div>
    </div>
    
