<script src="//www.amcharts.com/lib/4/core.js"></script>
<script src="//www.amcharts.com/lib/4/charts.js"></script>
<script src="//www.amcharts.com/lib/4/themes/animated.js"></script>

<table>
    <thead>
    </thead>
    <tbody>
        <td style="width: 150px; " >
            <?php for ($i = 1; $i < 5; $i++){ ?>
            <div id="<?php echo "chartdiv".$i; ?>" style="height: 50px">
                <script>
                    // Themes begin
                    am4core.useTheme(am4themes_animated); 
                    // Themes end
                    var container = am4core.create("<?php echo "chartdiv".$i; ?>", am4core.Container); 
                    container.width = am4core.percent(100); 
                    container.height = am4core.percent(100);
                    container.layout = "vertical";
                    container.hideCredits = true;
                    
                    /* Create chart instance */
                    var chart = container.createChild(am4charts.XYChart);
                    chart.paddingRight = 10;
                    
                    /* Add data */
                    chart.data = [{
                      "category": "Evaluation",
                      "value": 65,
                      "Meta": 240
                    }]; 
                    
                    /* Create axes */
                    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
                    categoryAxis.renderer.labels.template.disabled = true; 
                    categoryAxis.dataFields.category = "category";
                    categoryAxis.renderer.minGridDistance = 10;
                    categoryAxis.renderer.grid.template.disabled = true;
                    
                    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
                    valueAxis.renderer.minGridDistance = 10;
                    valueAxis.renderer.labels.template.disabled = true; 
                    valueAxis.renderer.grid.template.disabled = true;
                    valueAxis.min = 0;
                    valueAxis.strictMinMax = true;
                    valueAxis.renderer.labels.template.adapter.add("text", function(text) {
                      return text ;
                    }); 
                    
                    /* Create series */
                    var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueX = "value";
                    series.dataFields.categoryY = "category";
                    series.columns.template.fill = am4core.color("#23B14D");
                    series.columns.template.stroke = am4core.color("#334EFF");
                    series.columns.template.strokeWidth = .1;
                    series.columns.template.strokeOpacity = 0.5;
                    series.columns.template.height = am4core.percent(100);
                    series.columns.template.tooltipText = "[bold]Real: [/] {value}pzas [bold]Meta: [/]{target}pzas"; 
                    
                    var series2 = chart.series.push(new am4charts.LineSeries()); 
                    series2.dataFields.valueX = "Meta"; 
                    series2.dataFields.categoryY = "category"; 
                    series2.strokeWidth = 4; 
                    
                    var bullet = series2.bullets.push(new am4charts.Bullet()); 
                    var line = bullet.createChild(am4core.Line); 
                    line.x1 = 0; 
                    line.y1 = -200;
                    line.x2 = 0;
                    line.y2 = 200;
                    line.stroke = am4core.color("#ff0f00");
                    line.strokeWidth = 4; 
                    
                </script>
            </div>
            <?php } ?> 
        </td>
    </tbody>
</table>  