<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>



<!-- Styles -->
<style>
    #chartdiv {
      width: 100%;
      height: 500px;
    }
    
    #charM {
      width: 100%;
      height: 500px;
    }
    
    #charW {
      width: 100%;
      height: 500px;
    }
    
</style>

<!-- Resources -->
<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>

<!-- Chart code -->

<?php 
    
    session_start();
    include '../db/ServerFunctions.php';
    $date = new DateTime;
    
    if (isset($_SESSION['linea'])){
        $line = $_SESSION['linea'];
        $anio = $_SESSION['anio'];
        $mes = $_SESSION['mes'];
    } else {
        $line = 'L001';
        $anio = date('Y');
        $mes = date('m');
    }   
    
    $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        

    $fecha1 = $anio.'-01-01' ;
    $fecha2 = date("Y-m-d") ;
        
    $date->setISODate("$anio", 53);
        
    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
    if($date->format("W") == 53){
        $numSemanas = 53;
    }else{
        $numSemanas = 52;
    }

    $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
    $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

    //INICIALIZAMOS VARIABLES PARA SEMANA
    echo $sP,' - ',$sL,'<br>';
    if ($sP > $sL){
        for ($i = $sP; $i <= $numSemanas; $i++ ) {
            $cw[$i] = 0;
            $prodW[$i] = 0;
            $tecW[$i] = 0;
            $orgW[$i] = 0;
            $calW[$i] = 0;
            $cambW[$i] = 0;
            $tFalW[$i] = 0;
        }

        for($i = 1; $i <= $sL; $i++) {
            $cw[$i] = 0; 
            $prodW[$i] = 0; 
            $tecW[$i] = 0; 
            $orgW[$i] = 0; 
            $calW[$i] = 0; 
            $cambW[$i] = 0; 
            $tFalW[$i] = 0; 
        }            
    } else { 
        for ($i = $sP; $i <= $sL; $i++) {
            $cw[$i] = 0;
            $prodW[$i] = 0;
            $tecW[$i] = 0;
            $orgW[$i] = 0;
            $calW[$i] = 0;
            $cambW[$i] = 0;
            $tFalW[$i] = 0;
        } 
    } 

    $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
    $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

    $lblM[1] = (string) "Jun";
    $lblM[2] = (string) "Feb";
    $lblM[3] = (string) "Mar";
    $lblM[4] = (string) "Apr";
    $lblM[5] = (string) "May";
    $lblM[6] = (string) "Jun";
    $lblM[7] = (string) "Jul";
    $lblM[8] = (string) "Aug";
    $lblM[9] = (string) "Sep";
    $lblM[10] = (string) "Oct";
    $lblM[11] = (string) "Nov";
    $lblM[12] = (string) "Dec";     
        
    #INCIALIZACION DE VARIABLES PARA LAS FECHAS
    for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 
        $fecha[$vDate] = date("M, d",strtotime($i));//$i; 
        $prodDay[$vDate] = 0; 
        $caliDay[$vDate] = 0; 
        $orgDay[$vDate] = 0; 
        $tecDay[$vDate] = 0; 
        $cambioDay[$vDate] = 0; 
        $desDay[$vDate] = 0; 
        $targetDayT[$vDate] = 80; 
    }
    
    #CONSULTA PARA OEE DIARIO
    $oeeProductionDay = oeeProductionDay($line, $anio);     
    for($i = 0; $i < count($oeeProductionDay); $i++) {     
        $date = explode("-", $oeeProductionDay[$i][0],3);         
        $vDate = $date[0].$date[1].(int)$date[2];  
        $prodDay[$vDate] = @round($oeeProductionDay[$i][1],2); 
        $caliDay[$vDate] = @round($oeeProductionDay[$i][4],2); 
        $orgDay[$vDate] = @round($oeeProductionDay[$i][3],2); 
        $tecDay[$vDate] = @round($oeeProductionDay[$i][2],2); 
        $cambioDay[$vDate] = @round($oeeProductionDay[$i][5],2); 
        $desDay[$vDate] = @round($oeeProductionDay[$i][6],2); 
    }
    
    //MENSUAL    
    for ($i = 0; $i < 13; $i++){
        $prodM[$i] = 0;
        $tecM[$i] = 0;
        $orgM[$i] = 0;
        $caliM[$i] = 0;
        $camM[$i] = 0;
        $tFalM[$i] = 0;
    }
    
    $cPercentTopicMonthly = cPercentTopicMonthly($line, $anio);
    echo count($cPercentTopicMonthly);
    for ($i = 0; $i < count($cPercentTopicMonthly); $i++){
        $m = $cPercentTopicMonthly[$i][0];        
        $prodM[$m] = @round($cPercentTopicMonthly[$i][1],2);
        $tecM[$m] = @round($cPercentTopicMonthly[$i][2],2);
        $orgM[$m] = @round($cPercentTopicMonthly[$i][3],2);
        $caliM[$m] = @round($cPercentTopicMonthly[$i][4],2);
        $camM[$m] = @round($cPercentTopicMonthly[$i][5],2);
        $tFalM[$m] = @round($cPercentTopicMonthly[$i][6],2);
    }
    
    /************************** SEMANAL ****************************/
    $datOEEWeek = cPercentTopicWekly($line, $anio, $mes);
    for ($i = 0; $i < count($datOEEWeek); $i++){
        $s = $datOEEWeek[$i][0];
        $prodW[$s] = @round($datOEEWeek[$i][1],2);
        $tecW[$s] = @round($datOEEWeek[$i][2],2);
        $orgW[$s] = @round($datOEEWeek[$i][3],2);
        $calW[$s] = @round($datOEEWeek[$i][4],2);
        $cambW[$s] = @round($datOEEWeek[$i][5],2);
        $tFalW[$s] = @round($datOEEWeek[$i][6],2);
    } 
    
    if ($sP > $sL) { 
        for ($i = $sP; $i <= $numSemanas; $i++ ) { 
            echo '<br>* pd: ',$prodW[$i],', t: ',$tecW[$i],', o: ',$orgW[$i],', c: ',$calW[$i],', cm: ',$cambW[$i],', ds: ',$tFalW[$i];
        } 

        for($i = 1; $i <= $sL; $i++) { 
            echo '<br>+ pd: ',$prodW[$i],', t: ',$tecW[$i],', o: ',$orgW[$i],', c: ',$calW[$i],', cm: ',$cambW[$i],', ds: ',$tFalW[$i];
        } 
    } else { 
        for ($i = $sP; $i <= $sL; $i++) {             
            echo '<br>- pd: ',$prodW[$i],', t: ',$tecW[$i],', o: ',$orgW[$i],', c: ',$calW[$i],', cm: ',$cambW[$i],', ds: ',$tFalW[$i];
        } 
    } 
    
?>
<!-- HTML -->
<div id="chartdiv"> 
    <script>
        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "none",
            "dataDateFormat": "YYYY-MM-DD",
            "precision": 2,
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "dataProvider": [
                <?php for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                        $date = explode("-", $i); 
                        $vDate = $date[0].$date[1].(int)$date[2];                 
                    ?>
                {
                    "date": "<?php echo $fecha[$vDate] ?>",
                    "prod": <?php echo $prodDay[$vDate] ?>,
                    "calidad": <?php echo $caliDay[$vDate] ?>,
                    "org": <?php echo $orgDay[$vDate] ?>,
                    "tec": <?php echo $tecDay[$vDate] ?>,
                    "cambio": <?php echo $cambioDay[$vDate] ?>,
                    "desempenio": <?php echo $desDay[$vDate] ?>
                },
                <?php } ?>        
            ],
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0
            }],
            "graphs": [ {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#62cf73",
                "lineColor": "#62cf73",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Produccion",
                "type": "column",
                "color": "#FFFF",
                "valueField": "prod"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#311B92",
                "lineColor": "#311B92",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Técnicas",
                "type": "column",
                "color": "#FFFF",
                "valueField": "tec"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#F06292",
                "lineColor": "#F06292C",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Organizacionales",
                "type": "column",
                "color": "#FFFF",
                "valueField": "org"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#B71C1C",
                "lineColor": "#B71C1C",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Calidad",
                "type": "column",
                "color": "#FFFF",
                "valueField": "calidad"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#3498db",
                "lineColor": "#3498db",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Cambios",
                "type": "column",
                "color": "#FFFFFF",
                "valueField": "cambio"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#9E9E9E", 
                "lineColor": "#9E9E9E", 
                "labelText": "[[value]]", 
                "lineAlpha": 1,
                "title": "Desempeño", 
                "type": "column", 
                "color": "#FFFFFF",
                "valueField": "desempenio"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 40,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left"
            }
         });
    </script>
</div>
	
<div id="charM"> 
    <script>
        var chart = AmCharts.makeChart("charM", {
            "type": "serial",
            "theme": "none",
            "precision": 2,
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "dataProvider": [
                <?php for($i = 1; $i <= 12; $i++) { ?>
                {
                    "date": "<?php echo $lblM[$i] ?>", 
                    "prod": <?php echo $prodM[$i] ?>, 
                    "calidad": <?php echo $caliM[$i] ?>,
                    "org": <?php echo $orgM[$i] ?>,
                    "tec": <?php echo $tecM[$i] ?>,
                    "cambio": <?php echo $camM[$i] ?>,
                    "desempenio": <?php echo $tFalM[$i] ?>
                },
                <?php } ?>        
            ],
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0
            }],
            "graphs": [ {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#62cf73",
                "lineColor": "#62cf73",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Produccion",
                "type": "column",
                "color": "#FFFF",
                "valueField": "prod"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#311B92",
                "lineColor": "#311B92",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Técnicas",
                "type": "column",
                "color": "#FFFF",
                "valueField": "tec"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#F06292",
                "lineColor": "#F06292C",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Organizacionales",
                "type": "column",
                "color": "#FFFF",
                "valueField": "org"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#B71C1C",
                "lineColor": "#B71C1C",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Calidad",
                "type": "column",
                "color": "#FFFF",
                "valueField": "calidad"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#3498db",
                "lineColor": "#3498db",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Cambios",
                "type": "column",
                "color": "#FFFFFF",
                "valueField": "cambio"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#9E9E9E", 
                "lineColor": "#9E9E9E", 
                "labelText": "[[value]]", 
                "lineAlpha": 1,
                "title": "Desempeño", 
                "type": "column", 
                "color": "#FFFFFF",
                "valueField": "desempenio"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 40,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left"
            }
         });
    </script>
</div>

<div id="charW"> 
    <script>
        var chart = AmCharts.makeChart("charW", {
            "type": "serial",
            "theme": "none",
            "precision": 2,
            "legend": {
                "horizontalGap": 10,
                "maxColumns": 1,
                "position": "right",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "dataProvider": [
                <?php 
                        if ($sP > $sL) { 
                            for ($i = $sP; $i <= $numSemanas; $i++ ) { 
                            ?>
                                {
                                    "date": "<?php echo 'CW-'.$i ?>",
                                    "prod": <?php echo $prodW[$i] ?>, 
                                    "calidad": <?php echo $calW[$i] ?>,
                                    "org": <?php echo $orgW[$i] ?>,
                                    "tec": <?php echo $tecW[$i] ?>,
                                    "cambio": <?php echo $cambW[$i] ?>,
                                    "desempenio": <?php echo $tFalW[$i] ?>
                                },
                            <?php } 
                                for($i = 1; $i <= $sL; $i++) { 
                            ?>
                                {
                                    "date": "<?php echo 'CW-'.$i ?>",
                                    "prod": <?php echo $prodW[$i] ?>, 
                                    "calidad": <?php echo $calW[$i] ?>,
                                    "org": <?php echo $orgW[$i] ?>,
                                    "tec": <?php echo $tecW[$i] ?>,
                                    "cambio": <?php echo $cambW[$i] ?>,
                                    "desempenio": <?php echo $tFalW[$i] ?>
                                },
                            <?php } ?>                            
                            <?php } else { 
                                    for ($i = $sP; $i <= $sL; $i++) { ?>
                                {
                                    "date": "<?php echo 'CW-'.$i ?>",
                                    "prod": <?php echo $prodW[$i] ?>, 
                                    "calidad": <?php echo $calW[$i] ?>,
                                    "org": <?php echo $orgW[$i] ?>,
                                    "tec": <?php echo $tecW[$i] ?>,
                                    "cambio": <?php echo $cambW[$i] ?>,
                                    "desempenio": <?php echo $tFalW[$i] ?>
                                },
                            <?php } } ?>
                        ], 
            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0
            }],
            "graphs": [ {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#62cf73",
                "lineColor": "#62cf73",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Produccion",
                "type": "column",
                "color": "#FFFF",
                "valueField": "prod"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#311B92",
                "lineColor": "#311B92",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Técnicas",
                "type": "column",
                "color": "#FFFF",
                "valueField": "tec"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#F06292",
                "lineColor": "#F06292C",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Organizacionales",
                "type": "column",
                "color": "#FFFF",
                "valueField": "org"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#B71C1C",
                "lineColor": "#B71C1C",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Calidad",
                "type": "column",
                "color": "#FFFF",
                "valueField": "calidad"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#3498db",
                "lineColor": "#3498db",
                "labelText": "[[value]]",
                "lineAlpha": 1,
                "title": "Cambios",
                "type": "column",
                "color": "#FFFFFF",
                "valueField": "cambio"
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 1,
                "fillColors": "#9E9E9E", 
                "lineColor": "#9E9E9E", 
                "labelText": "[[value]]", 
                "lineAlpha": 1,
                "title": "Desempeño", 
                "type": "column", 
                "color": "#FFFFFF",
                "valueField": "desempenio"
            }],
            "chartScrollbar": {
                "graph": "g1",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 40,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "categoryField": "date",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left"
            }
        });
    </script>
</div>
