<HTML>
    <LINK REL=StyleSheet HREF="../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <title>OEE</title>
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>    
    
    <?php 
        include '../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
        //RECIBE LOS VALORES DEL AJAX 
        $tema = "Técnicas";
        
        if (isset($_SESSION['linea'])){
            $line = $_SESSION['linea'];
            $anio = $_SESSION['anio'];
            $mes = $_SESSION['mes'];
        } else {
            $line = 'L001';
            $anio = date('Y');
            $mes = date('m');
        }   
        
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));        

        $fecha1 = $anio.'-01-01' ;
        $fecha2 = date("Y-m-d") ;

        $date->setISODate("$anio", 53);

        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
        if($date->format("W") == 53){
            $numSemanas = 53;
        }else{
            $numSemanas = 52;
        }

        $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
        $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

        //INICIALIZAMOS VARIABLES PARA SEMANA
        //echo $sP,' - ',$sL,'<br>';
        if ($sP > $sL){
            for ($i = $sP; $i <= $numSemanas; $i++ ) {
                $cw[$i] = 0;
                $prodW[$i] = 0;
                $tecW[$i] = 0;
                $orgW[$i] = 0;
                $calW[$i] = 0;
                $cambW[$i] = 0;
                $tFalW[$i] = 0;
            }

            for($i = 1; $i <= $sL; $i++) {
                $cw[$i] = 0; 
                $prodW[$i] = 0; 
                $tecW[$i] = 0; 
                $orgW[$i] = 0; 
                $calW[$i] = 0; 
                $cambW[$i] = 0; 
                $tFalW[$i] = 0; 
            }            
        } else { 
            for ($i = $sP; $i <= $sL; $i++) {
                $cw[$i] = 0;
                $prodW[$i] = 0;
                $tecW[$i] = 0;
                $orgW[$i] = 0;
                $calW[$i] = 0;
                $cambW[$i] = 0;
                $tFalW[$i] = 0;
            } 
        } 

        $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
        $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

        $lblM[1] = (string) "Jun";
        $lblM[2] = (string) "Feb";
        $lblM[3] = (string) "Mar";
        $lblM[4] = (string) "Apr";
        $lblM[5] = (string) "May";
        $lblM[6] = (string) "Jun";
        $lblM[7] = (string) "Jul";
        $lblM[8] = (string) "Aug";
        $lblM[9] = (string) "Sep";
        $lblM[10] = (string) "Oct";
        $lblM[11] = (string) "Nov";
        $lblM[12] = (string) "Dec";     

        #INCIALIZACION DE VARIABLES PARA LAS FECHAS
        for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 
            $fecha[$vDate] = date("M, d",strtotime($i));//$i; 
            $prodDay[$vDate] = 0; 
            $caliDay[$vDate] = 0; 
            $orgDay[$vDate] = 0; 
            $tecDay[$vDate] = 0; 
            $cambioDay[$vDate] = 0; 
            $desDay[$vDate] = 0; 
            $targetDayT[$vDate] = 80; 
        }

        #CONSULTA PARA OEE DIARIO
        $oeeProductionDay = oeeProductionDay($line, $anio);     
        for($i = 0; $i < count($oeeProductionDay); $i++) {     
            $date = explode("-", $oeeProductionDay[$i][0],3);         
            $vDate = $date[0].$date[1].(int)$date[2];  
            $prodDay[$vDate] = @round($oeeProductionDay[$i][1],2); 
            $caliDay[$vDate] = @round($oeeProductionDay[$i][4],2); 
            $orgDay[$vDate] = @round($oeeProductionDay[$i][3],2); 
            $tecDay[$vDate] = @round($oeeProductionDay[$i][2],2); 
            $cambioDay[$vDate] = @round($oeeProductionDay[$i][5],2); 
            $desDay[$vDate] = @round($oeeProductionDay[$i][6],2); 
        }

        //MENSUAL    
        for ($i = 0; $i < 13; $i++){
            $prodM[$i] = 0;
            $tecM[$i] = 0;
            $orgM[$i] = 0;
            $caliM[$i] = 0;
            $camM[$i] = 0;
            $tFalM[$i] = 0;
        }

        $cPercentTopicMonthly = cPercentTopicMonthly($line, $anio);
        echo count($cPercentTopicMonthly);
        for ($i = 0; $i < count($cPercentTopicMonthly); $i++){
            $m = $cPercentTopicMonthly[$i][0];        
            $prodM[$m] = @round($cPercentTopicMonthly[$i][1],2);
            $tecM[$m] = @round($cPercentTopicMonthly[$i][2],2);
            $orgM[$m] = @round($cPercentTopicMonthly[$i][3],2);
            $caliM[$m] = @round($cPercentTopicMonthly[$i][4],2);
            $camM[$m] = @round($cPercentTopicMonthly[$i][5],2);
            $tFalM[$m] = @round($cPercentTopicMonthly[$i][6],2);
        }

        /************************** SEMANAL ****************************/
        $datOEEWeek = cPercentTopicWekly($line, $anio, $mes);
        for ($i = 0; $i < count($datOEEWeek); $i++){
            $s = $datOEEWeek[$i][0];
            $prodW[$s] = @round($datOEEWeek[$i][1],2);
            $tecW[$s] = @round($datOEEWeek[$i][2],2);
            $orgW[$s] = @round($datOEEWeek[$i][3],2);
            $calW[$s] = @round($datOEEWeek[$i][4],2);
            $cambW[$s] = @round($datOEEWeek[$i][5],2);
            $tFalW[$s] = @round($datOEEWeek[$i][6],2);
        } 

//        if ($sP > $sL) { 
//            for ($i = $sP; $i <= $numSemanas; $i++ ) { 
//                echo '<br>* pd: ',$prodW[$i],', t: ',$tecW[$i],', o: ',$orgW[$i],', c: ',$calW[$i],', cm: ',$cambW[$i],', ds: ',$tFalW[$i];
//            } 
//
//            for($i = 1; $i <= $sL; $i++) { 
//                echo '<br>+ pd: ',$prodW[$i],', t: ',$tecW[$i],', o: ',$orgW[$i],', c: ',$calW[$i],', cm: ',$cambW[$i],', ds: ',$tFalW[$i];
//            } 
//        } else { 
//            for ($i = $sP; $i <= $sL; $i++) { 
//                echo '<br>- pd: ',$prodW[$i],', t: ',$tecW[$i],', o: ',$orgW[$i],', c: ',$calW[$i],', cm: ',$cambW[$i],', ds: ',$tFalW[$i];
//            } 
//        } 
        
        /************************* TABLA DIA *********************************/
        $countProblem = 0;
        $problema[1] = "OEE (%)";
        $problema[2] = "NO Calida (%)";
        $problema[3] = "Organizacionales (%)";
        $problema[4] = "Tecnicas (%)";
        $problema[5] = "Cammbio de Modelo (%)";
        $problema[6] = "Desempeño (%)";
        $countX = 1;
        $total = 0;
        $sumProblemas = 0;

        //INCIALIZAMOS LAS VARIABLES
        //TABLA
        for ($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) {        
            $countX++;
            $date = explode("-", $i); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            switch($date[1]){
                case 1:
                    $cadDate[$i] = 'Jan&nbsp;'.$date[2];
                    break;
                case 2:
                    $cadDate[$i] = 'Feb&nbsp;'.$date[2];
                    break;
                case 3:
                    $cadDate[$i] = 'Mar&nbsp;'.$date[2];
                    break;
                case 4:
                    $cadDate[$i] = 'Apr&nbsp;'.$date[2];
                    break;
                case 5:
                    $cadDate[$i] = 'May&nbsp;'.$date[2];
                    break;
                case 6:
                    $cadDate[$i] = 'Jun&nbsp;'.$date[2];
                    break;
                case 7:
                    $cadDate[$i] = 'Jul&nbsp;'.$date[2];
                    break;
                case 8:
                    $cadDate[$i] = 'Aug&nbsp;'.$date[2];
                    break;
                case 9:
                    $cadDate[$i] = 'Sep&nbsp;'.$date[2];
                    break;
                case 10:
                    $cadDate[$i] = 'Oct&nbsp;'.$date[2];
                    break;
                case 11:
                    $cadDate[$i] = 'Nov&nbsp;'.$date[2];
                    break;
                case 12:
                    $cadDate[$i] = 'Dec&nbsp;'.$date[2];
                    break;
            }

            //INICIALIZAMOS LAS VARIABLES PARA LOS SUBTOTALES DIA(_) 
    //        $targetDayT[$vDate] = 80;        
            $pTopic[1][$vDate] = 0;
            $pTopic[2][$vDate] = 0;
            $pTopic[3][$vDate] = 0;
            $pTopic[4][$vDate] = 0;
            $pTopic[5][$vDate] = 0;   
            $pTopic[6][$vDate] = 0;
            $pTotalDay[$vDate] = 0;
        }

        //echo $line,', ' ,$fecha1,', ',$fecha2;
        $datOEEDay = percentOEERangeDays($line, $fecha1, $fecha2);  

        //EVALUACION PARA EL TIPO DE DATO QUE SE TIENE 
        for ($i = 0; $i < count($datOEEDay); $i++  ){        
            //$d = $datOEEDay[$i][0];       
            $date = explode("-", $datOEEDay[$i][0]);
            $vDate = $date[0].$date[1].(int)$date[2];

            $pProdDay[1][$vDate] = @round($datOEEDay[$i][1],2);
            $pTecDay[2][$vDate] = @round($datOEEDay[$i][2],2);
            $pOrgDay[3][$vDate] = @round($datOEEDay[$i][3],2);
            $pCaliDay[4][$vDate] = @round($datOEEDay[$i][4],2);
            $pCamDay[5][$vDate] = @round($datOEEDay[$i][5],2);
            //$pPlanDay[$d] = $datOEEDay[$i][5];        
            $pTFalDay[6][$vDate] = @round($datOEEDay[$i][6],2);

            $pTopic[1][$vDate] = @round($datOEEDay[$i][1],2);
            $pTopic[2][$vDate] = @round($datOEEDay[$i][2],2);
            $pTopic[3][$vDate] = @round($datOEEDay[$i][3],2);
            $pTopic[4][$vDate] = @round($datOEEDay[$i][4],2);
            $pTopic[5][$vDate] = @round($datOEEDay[$i][5],2);
            $pTopic[6][$vDate] = @round($datOEEDay[$i][6],2);
            $pTotalDay[$vDate] = @round($pTopic[1][$vDate]+$pTopic[2][$vDate]+$pTopic[3][$vDate]+$pTopic[4][$vDate]+$pTopic[5][$vDate]+$pTopic[6][$vDate],1);
            //echo $datOEEDay[$i][0],': ',$pTopic[1][$vDate],', ', $pTopic[2][$vDate],'<br>';
        }     

        //DATOS PARA TAMAÑO DE TABLA    
        $dias = (strtotime($fecha1)- strtotime($fecha2))/86400;
        $dias = abs($dias);
        $dias = floor($dias);    

        //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
        //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS
        if ($dias > 11 ){
            $rowspan = 12;
        } else {
            //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
            //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS
            $rowspan = $dias+1;
        }

        //VALIDACION PARA CUANDO NO SE TIENEN DATOS, PARA QUE NO ROMPA DISEÑO DE LA TABLA
        if($countProblem == 0) { 
            $totalProblema[1] = 0;
            $countProblem = 1;
        }
        
        //echo '<br> l: ',$line;
    ?>
    
    <head>       
	<script type="text/javascript">               
            window.onload = function () {
                var gridViewScroll = new GridViewScroll({
                    elementID : "gvMain",
                    freezeColumn : true,
                    freezeFooter : true,
                    freezeColumnCssClass : "GridViewScrollItemFreeze",
                    freezeFooterCssClass : "GridViewScrollFooterFreeze"
                });
                gridViewScroll.enhance();
            }
	</script>    
    </head>
   
    <body>
        <div >  
            <br>            
            <div id="jTMonth" name="jTMonth" class="jidokaMonth" >  
                <script>
                    var chart = AmCharts.makeChart("jTMonth", {
                        "type": "serial",
                        "theme": "none",
                        "precision": 2,
                        "legend": {
                            "horizontalGap": 10,
                            "maxColumns": 1,
                            "position": "right",
                            "useGraphSettings": true,
                            "markerSize": 10
                        },
                        "dataProvider": [
                            <?php for($i = 1; $i <= 12; $i++) { ?>
                            {
                                "date": "<?php echo $lblM[$i] ?>", 
                                "prod": <?php echo $prodM[$i] ?>, 
                                "calidad": <?php echo $caliM[$i] ?>,
                                "org": <?php echo $orgM[$i] ?>,
                                "tec": <?php echo $tecM[$i] ?>,
                                "cambio": <?php echo $camM[$i] ?>,
                                "desempenio": <?php echo $tFalM[$i] ?>
                            },
                            <?php } ?>        
                        ], 
                        "graphs": [ {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#62cf73",
                            "lineColor": "#62cf73",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Produccion",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "prod"
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#311B92",
                            "lineColor": "#311B92",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Técnicas",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "tec"
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#F06292",
                            "lineColor": "#F06292C",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Organizacionales",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "org"
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#B71C1C",
                            "lineColor": "#B71C1C",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Calidad",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "calidad"
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#3498db",
                            "lineColor": "#3498db",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Cambios",
                            "type": "column",
                            "color": "#FFFFFF",
                            "valueField": "cambio"
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#9E9E9E", 
                            "lineColor": "#9E9E9E", 
                            "labelText": "[[value]]", 
                            "lineAlpha": 1,
                            "title": "Desempeño", 
                            "type": "column", 
                            "color": "#FFFFFF",
                            "valueField": "desempenio"
                        }],
                        "valueAxes": [{ 
                            "stackType": "100%",
                            "axisAlpha": 0.5,
                            "gridAlpha": 0.5,
                            "labelsEnabled": true,
                            "position": "left"
                        }], 
                        "chartScrollbar": { 
                            "graph": "g1",
                            "oppositeAxis": false,
                            "offset": 30,
                            "scrollbarHeight": 20,
                            "backgroundAlpha": 0,
                            "selectedBackgroundAlpha": 0.1,
                            "selectedBackgroundColor": "#888888",
                            "graphFillAlpha": 0,
                            "graphLineAlpha": 0.5,
                            "selectedGraphFillAlpha": 0, 
                            "selectedGraphLineAlpha": 1, 
                            "autoGridCount": true, 
                            "color": "#AAAAAA"
                        }, 
                        "categoryField": "date", 
                        "categoryAxis": { 
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "position": "left"
                        }
                    });
                </script>
            </div>     
            
            <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
                <script>
                    var chart = AmCharts.makeChart("jTWeek", {
                        "type": "serial",
                        "theme": "none",
                        "precision": 2,
                        "legend": {
                            "horizontalGap": 10,
                            "maxColumns": 1,
                            "position": "right",
                            "useGraphSettings": true,
                            "markerSize": 10
                        },
                        "dataProvider": [
                        <?php 
                            if ($sP > $sL) { 
                                for ($i = $sP; $i <= $numSemanas; $i++ ) { 
                                ?>
                                    {
                                        "date": "<?php echo 'CW-'.$i ?>",
                                        "prod": <?php echo $prodW[$i] ?>, 
                                        "calidad": <?php echo $calW[$i] ?>,
                                        "org": <?php echo $orgW[$i] ?>,
                                        "tec": <?php echo $tecW[$i] ?>,
                                        "cambio": <?php echo $cambW[$i] ?>,
                                        "desempenio": <?php echo $tFalW[$i] ?>
                                    },
                                <?php } 
                                    for($i = 1; $i <= $sL; $i++) { 
                                ?>
                                    {
                                        "date": "<?php echo 'CW-'.$i ?>",
                                        "prod": <?php echo $prodW[$i] ?>, 
                                        "calidad": <?php echo $calW[$i] ?>,
                                        "org": <?php echo $orgW[$i] ?>,
                                        "tec": <?php echo $tecW[$i] ?>,
                                        "cambio": <?php echo $cambW[$i] ?>,
                                        "desempenio": <?php echo $tFalW[$i] ?>
                                    },
                                <?php } ?>                            
                                <?php } else { 
                                        for ($i = $sP; $i <= $sL; $i++) { ?>
                                    {
                                        "date": "<?php echo 'CW-'.$i ?>",
                                        "prod": <?php echo $prodW[$i] ?>, 
                                        "calidad": <?php echo $calW[$i] ?>,
                                        "org": <?php echo $orgW[$i] ?>,
                                        "tec": <?php echo $tecW[$i] ?>,
                                        "cambio": <?php echo $cambW[$i] ?>,
                                        "desempenio": <?php echo $tFalW[$i] ?>
                                    },
                                <?php } } ?>
                        ], 
                        "graphs": [ {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#62cf73",
                            "lineColor": "#62cf73",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "id":"mProd",
                            "title": "Produccion",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "prod"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#311B92",
                            "lineColor": "#311B92",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Técnicas",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "tec"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#F06292",
                            "lineColor": "#F06292C",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Organizacionales",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "org"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#B71C1C",
                            "lineColor": "#B71C1C",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Calidad",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "calidad"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#3498db",
                            "lineColor": "#3498db",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Cambios",
                            "type": "column",
                            "color": "#FFFFFF",
                            "valueField": "cambio"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#9E9E9E", 
                            "lineColor": "#9E9E9E", 
                            "labelText": "[[value]]", 
                            "lineAlpha": 1,
                            "title": "Desempeño", 
                            "type": "column", 
                            "color": "#FFFFFF",
                            "valueField": "desempenio"
                        }],
                        "valueAxes": [{
                            "stackType": "100%",
                            "axisAlpha": 0.5,
                            "gridAlpha": 0.5,
                            "labelsEnabled": true,
                            "position": "left"
                        }],
                        "chartScrollbar": {
                            "graph": "g1",
                            "oppositeAxis": false,
                            "offset": 30,
                            "scrollbarHeight": 20,
                            "backgroundAlpha": 0,
                            "selectedBackgroundAlpha": 0.1,
                            "selectedBackgroundColor": "#888888",
                            "graphFillAlpha": 0,
                            "graphLineAlpha": 0.5,
                            "selectedGraphFillAlpha": 0,
                            "selectedGraphLineAlpha": 1,
                            "autoGridCount": true,
                            "color": "#AAAAAA"
                        },
                        "categoryField": "date",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "position": "left"
                        }
                    });
                </script>                
            </div>    
            
            <!-- TABLA DE MESES -->
            <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
                <thead>
                    <tr >
                        <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">Mes</th>
                        <?php for ($i = 1; $i < 13; $i++ ) { ?>
                            <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblM[$i] ?></th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                <tr >
                    <td style="border: 1px solid #BABABA; text-align: center;">OEE %</td>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td style=" border: 1px solid #BABABA; text-align: center;"><?php echo $prodM[$i] ?></td>
                    <?php } ?>
                </tr>
                <tr >
                    <td style="border: 1px solid #BABABA; text-align: center;">Tecnicas %</th>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td style=" border: 1px solid #BABABA; text-align: center;"><?php echo $tecM[$i] ?></th>
                    <?php } ?>
                </tr>
                <tr >
                    <td style="border: 1px solid #BABABA; text-align: center;">Organizacionales %</th>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td style="border: 1px solid #BABABA; text-align: center;"><?php echo $orgM[$i] ?></th>
                    <?php } ?>
                </tr>
                <tr >
                    <td style="border: 1px solid #BABABA; text-align: center;">Cambios % </td>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td style="border: 1px solid #BABABA; text-align: center;"><?php echo $camM[$i] ?></td>
                    <?php } ?>
                </tr>
                <tr >
                    <td style="border: 1px solid #BABABA; text-align: center;">Calidad % </td>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td style="border: 1px solid #BABABA; text-align: center;"><?php echo $caliM[$i] ?></td>
                    <?php } ?>
                </tr>
                <tr >
                    <td style="border: 1px solid #BABABA; text-align: center;">Desempeño % </td>
                    <?php for ($i = 1; $i < 13; $i++ ) { ?>
                        <td style=" border: 1px solid #BABABA; text-align: center;"><?php echo $tFalM[$i] ?></td>
                    <?php } ?>
                </tr> 
                </tbody>
            </table>
            
            <!-- TABLA DE SEMANA -->
            <table style="width: 35%; margin-left: 63%; margin-top: -185px; border: 1px solid #BABABA; ">
                <tr>
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo 'CW-'.$i ?></th>
                    <?php }} ?>                            
                </tr>
                <tr>
                    <td style="border: 1px solid #BABABA; text-align: center;">OEE %</td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $prodW[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $prodW[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $prodW[$i] ?></td>
                    <?php }} ?>  
                </tr>   
                <tr>
                    <td style="border: 1px solid #BABABA; text-align: center;">Tecnicas %</td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tecW[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tecW[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tecW[$i] ?></td>
                    <?php }} ?>  
                </tr>
                <tr>
                    <td style="border: 1px solid #BABABA; text-align: center;">Organizacionales %</td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $orgW[$i] ?></td>
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $orgW[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $orgW[$i] ?></td>
                    <?php }} ?>  
                </tr>
                <tr>
                    <td style="border: 1px solid #BABABA; text-align: center;">Cambios %</td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $calW[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $calW[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $calW[$i] ?></td>
                    <?php }} ?>  
                </tr>
                <tr>
                    <td style="border: 1px solid #BABABA; text-align: center;">Calidad %</td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cambW[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cambW[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $cambW[$i] ?></td>
                    <?php }} ?>  
                </tr>
                <tr>
                    <td style="border: 1px solid #BABABA; text-align: center;">Desempeño %</td>
                    <?php 
                    if($sP > $sL){
                        for ($i = $sP; $i <= $numSemanas; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tFalW[$i] ?></td>
                        <?php } 
                        for ($i = 1; $i <= $sL; $i++ ) {
                        ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tFalW[$i] ?></td>
                        <?php
                        }
                    } else {                        
                        for ($i = $sP; $i <= $sL; $i++) { ?>
                            <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tFalW[$i] ?></td>
                    <?php }} ?>  
                </tr>
            </table>             
            <br>
            
            <div id="grafDays" class="jidokaDay" >                
                <script>
                    var chart = AmCharts.makeChart("grafDays", {
                        "type": "serial",
                        "theme": "none",
                        "dataDateFormat": "YYYY-MM-DD",
                        "precision": 2,
                        "legend": {
                            "horizontalGap": 10,
                            "maxColumns": 1,
                            "position": "right",
                            "useGraphSettings": true,
                            "markerSize": 10
                        },
                        "dataProvider": [
                            <?php for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                    $date = explode("-", $i); 
                                    $vDate = $date[0].$date[1].(int)$date[2];                 
                                ?>
                            { 
                                "date": "<?php echo $fecha[$vDate] ?>", 
                                "prod": <?php echo $prodDay[$vDate] ?>, 
                                "calidad": <?php echo $caliDay[$vDate] ?>, 
                                "org": <?php echo $orgDay[$vDate] ?>, 
                                "tec": <?php echo $tecDay[$vDate] ?>, 
                                "cambio": <?php echo $cambioDay[$vDate] ?>, 
                                "desempenio": <?php echo $desDay[$vDate] ?> 
                            }, 
                            <?php } ?> 
                        ], 
                        "valueAxes": [{
                            "stackType": "100%",
                            "axisAlpha": 0.3,
                            "gridAlpha": 0
                        }],
                        "graphs": [ {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#62cf73",
                            "lineColor": "#62cf73",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Produccion",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "prod"
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1, 
                            "fillColors": "#311B92", 
                            "lineColor": "#311B92", 
                            "labelText": "[[value]]", 
                            "lineAlpha": 1, 
                            "title": "Técnicas", 
                            "type": "column", 
                            "color": "#FFFF", 
                            "valueField": "tec" 
                        }, { 
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#F06292",
                            "lineColor": "#F06292C",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Organizacionales",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "org"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#B71C1C",
                            "lineColor": "#B71C1C",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Calidad",
                            "type": "column",
                            "color": "#FFFF",
                            "valueField": "calidad"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#3498db",
                            "lineColor": "#3498db",
                            "labelText": "[[value]]",
                            "lineAlpha": 1,
                            "title": "Cambios",
                            "type": "column",
                            "color": "#FFFFFF",
                            "valueField": "cambio"
                        }, {
                            "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                            "fillAlphas": 1,
                            "fillColors": "#9E9E9E", 
                            "lineColor": "#9E9E9E", 
                            "labelText": "[[value]]", 
                            "lineAlpha": 1,
                            "title": "Desempeño", 
                            "type": "column", 
                            "color": "#FFFFFF",
                            "valueField": "desempenio"
                        }],
                        "chartScrollbar": {
                            "graph": "g1",
                            "oppositeAxis": false,
                            "offset": 30,
                            "scrollbarHeight": 20,
                            "backgroundAlpha": 0,
                            "selectedBackgroundAlpha": 0.1,
                            "selectedBackgroundColor": "#888888",
                            "graphFillAlpha": 0,
                            "graphLineAlpha": 0.5,
                            "selectedGraphFillAlpha": 0,
                            "selectedGraphLineAlpha": 1,
                            "autoGridCount": true,
                            "color": "#AAAAAA"
                        },
                        "categoryField": "date",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "position": "left"
                        }
                     });
                </script>
            </div>
            
            <br>
            <br>
            
            <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
            <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
            <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
            <script src="../js/table-scroll.min.js"></script>  
            <script>        
                //CONFIGURACION DE LA TABLA 
                $(function () { 
                    var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
                    $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({ 
                        fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                        fixedColumnsRight: 1, //CABECERA FIJAS 
                        columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                        scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                        scrollY: 0 //INICIO DE SCROLL LATERAL | 
                    }); 
                }); 

                function getFixedColumnsData() {} 
            </script> 
            <link rel="stylesheet" href="../css/demo.css" /> 
            
            <div id="holder-semple-1" style="margin-top: -1%; margin-left: 2%; width: 95%">
                <script id="tamplate-semple-1" type="text/mustache">
                    <table width="98%" class="inner-table">
                        <thead>
                            <tr>
                                <td colspan="2"> </td>
                                <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+1; ?>" > PERIODO </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td align="center" >TOPIC</td>
                                <?php for($i = $fecha1; $i <= $fecha2; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ ?>
                                    <td align="center" ><?php echo $cadDate[$i]; ?></td>
                                <?php } ?> 
                            </tr>
                        </thead>
                        <tbody>
                            <?php for($i = 1; $i <= 6; $i++) { ?>
                            <tr > 
                                <td align="right" ><?php echo $i ?></td>  
                                <td ><?php echo $problema[$i]?></td> 
                                <?php for($j = $fecha1; $j <= $fecha2; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                                    $date = explode("-", $j); 
                                    $vDate = $date[0].$date[1].(int)$date[2]; 
                                ?> 
                                <td align="center" ><?php echo floor($pTopic[$i][$vDate]); ?></td> 
                            <?php }} ?> 
                        </tbody> 
                    </table> 
                </script>
            </div>
        </div> 
    </body>