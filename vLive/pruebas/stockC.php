<script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<style>
    body {
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    }

    #chartdiv {
        width: 100%;
        height: 500px; font-size: 11px;
    }			
</style>

<div id="chartdiv">
    <script>
        var chart = AmCharts.makeChart("chartdiv", {
        "type": "serial",
        "theme": "none",
        "legend": {
            "autoMargins": false,
            "borderAlpha": 0.2,
            "equalWidths": false,
            "horizontalGap": 10,
            "markerSize": 10,
            "useGraphSettings": true,
            "valueAlign": "left",
            "valueWidth": 0
        },
        "dataProvider": [{
            "year": "2003",
            "europe": 17,
            "namerica": 50,
            "asia": 2.5,
            "lamerica": 20,
            "meast": 5.5,
            "africa": 5,
            "meta": 15
        }, {
            "year": "2004",
            "europe": 50,
            "namerica": 20,
            "asia": 5,
            "lamerica": 2.5,
            "meast": 5.5,
            "africa": 17,
            "meta": 15
        }, {
            "year": "2005",
            "europe": 2.5,
            "namerica": 5,
            "asia": 5.5,
            "lamerica": 50,
            "meast": 17,
            "africa": 20,
            "meta": 15
        }],
        "valueAxes": [{
            "stackType": "regular",
            "axisAlpha": 0.3,
            "gridAlpha": 0
        }],
        "graphs": [{
            "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
            "fillAlphas": 0.9,
            "labelText": "[[percents]]%",
            "lineAlpha": 0.5,
            "title": "Europe",
            "type": "column",
            "fontSize": 11,
            "valueField": "europe"
        }, {
            "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
            "fillAlphas": 0.9,
            "fontSize": 11,
            "labelText": "[[percents]]%",
            "lineAlpha": 0.5,
            "title": "North America",
            "type": "column",
            "valueField": "namerica"
        }, {
            "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
            "fillAlphas": 0.9,
            "fontSize": 11,
            "labelText": "[[percents]]%",
            "lineAlpha": 0.5,
            "title": "Asia-Pacific",
            "type": "column",
            "valueField": "asia"
        }, {
            "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
            "fillAlphas": 0.9,
            "fontSize": 11,
            "labelText": "[[percents]]%",
            "lineAlpha": 0.5,
            "title": "Latin America",
            "type": "column",
            "valueField": "lamerica"
        }, {
            "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
            "fillAlphas": 0.9,
            "fontSize": 11,
            "labelText": "[[percents]]%",
            "lineAlpha": 0.5,
            "title": "Middle-East",
            "type": "column",
            "valueField": "meast"
        }, {
            "balloonText": "[[title]], [[category]]<br><span style='font-size:14px;'><b>[[value]]</b> ([[percents]]%)</span>",
            "fillAlphas": 0.9,
            "fontSize": 11,
            "labelText": "[[percents]]%",
            "lineAlpha": 0.5,
            "title": "Africa",
            "type": "column",
            "valueField": "africa"
        }, { 
            "valueAxis": "v2",
            "bullet": "round",
            "bulletBorderAlpha": 1,
            "bulletColor": "#FFFFFF",
            "bulletSize": 5,
            "hideBulletsCount": 50,
            "lineThickness": 2,
            "lineColor": "#62cf73",
            "type": "line",
            "title": "Meta",
            "useLineColorForBulletBorder": true,
            "valueField": "meta",
            "balloonText": "[[title]]: <b style='font-size: 100%'>[[value]]</b>"
        }],
        "marginTop": 30,
        "marginRight": 0,
        "marginLeft": 0,
        "marginBottom": 40,
        "autoMargins": false,
        "categoryField": "year",
        "categoryAxis": {
            "gridPosition": "start",
            "axisAlpha": 0,
            "gridAlpha": 0,
            "position": "left"
        },
        "export": {
            "enabled": false
        }
    });
    </script>
</div>
