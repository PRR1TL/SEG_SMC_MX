<HTML>
    <head>
        <LINK REL=StyleSheet HREF="../css/estiloProducidas.css" TYPE="text/css" MEDIA=screen>
        <!--<LINK REL=StyleSheet HREF="est.css" TYPE="text/css" MEDIA=screen>-->
        <title>Pareto Producción</title>
        <link href="../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        
        <script src="https://code.jquery.com/jquery.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/pareto.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        
        <link rel="stylesheet" href="css/jq.css" type="text/css" media="print, projection, screen" />
        <script type="text/javascript" src="js/jquery-latest.js"></script>
        <script type="text/javascript" src="js/jquery.tablesorter.js"></script>
        
        <!--------CONSULTAS------------->
        <?php
        
            require_once '../db/ServerFunctions.php';
//            $line = $_REQUEST['line'];
//            $month = $_REQUEST['month'];
//            $year = $_REQUEST['year'];
            
            $line = 'L003';
            $month = 4;
            $year = 2019;
            
            //$varMesStr = listarMeses();
            
            $ultimoDiaMes=date("t",mktime(0,0,0,$month,1,$year));
            # Obtenemos la semana del primer dia del mes
            $primeraSemana= (int) date("W",mktime(0,0,0,$month,1,$year));
                //echo $primeraSemana;
                # Obtenemos la semana del ultimo dia del mes
            $ultimaSemana= (int) date("W",mktime(0,0,0,$month,$ultimoDiaMes,$year));
                        //ECHO $ultimaSemana;
            if ($ultimaSemana < $primeraSemana) {
                $numSemanas = ((52 + $ultimaSemana) - $primeraSemana) + 1;
                    //echo $numSemanas;
            } else {
                $numSemanas = ($ultimaSemana - $primeraSemana) + 1;     
                    //echo $numSemanas;
            }
            
            $x = 0;
            //echo "i:",$primeraSemana," f:",$ultimaSemana," n:",$numSemanas,'<br>';
            for($i = 0; $i < $numSemanas; $i++ ){
                if($primeraSemana < $ultimaSemana){
                    $sem[$i] = $primeraSemana + $i;
                }else {
                    if($ultimaSemana < 52){
                        $sem[$i] = $primeraSemana + $i;
                        if($sem[$i] > 52){
                            $sem[$i] = $x + 1;
                            $x++;
                        }
                    }
                }
            }
            
            $datProdMes = pzasProdMes($line, $year);
            $datProdAnio = pzasProdAnual($line, $month, $year);             
            $datProdNoPMes = pzasProdNoParte($line, $month, $year); //SACA LOS NUMEROS DE PARTE PARA LOS QUE SE PRODUCIERON PIEZAS
            $datProdNoPDia = pzasProdNoParteDia($line, $month, $year);            
            $datTargetProdAnio = targetProdAnio($line, $year);
            $datTargetProdMes = targetProdMes($line, $year);
            $datProducidasTabla = pzasProdTabla($line,$month,$year);
            $datSemana = semanaProducidas($line, $year, $month);
            //$datTSemana = tSemanaProdu($line, $year, $month);
            
            //CONSULTAS PARA EL CUADRE DE LOS DATOS EN TABLA
            $datPzasNoParte = pzasNoParteMes($line, $year, $month);
            $datPzasDia = pzasDiaMes($line, $year, $month) ;
                    
            $dia;
            $numPartDia;
            $numPartCant;
            $numPart;
            $mes;
            $anio;
            $noParte;
            $prodMes;
            $prodAnio;   
            $targetProdAnio;
            $targetProdMes;
            
            //SEMANA            
            for($i = 0; $i < $numSemanas; $i++){
                $cantSemana[$i] = 0;
                $tSemana[$i] = 0;
            }
        
            $id = date("W",mktime(0,0,0,$month,01,$year));
            for ($i = 0; $i < count($datSemana); $i++){
                $s = $datSemana[$i][0];
                //echo $s,' a ', $id,'<br>';
                if($s == $id){
                    $cantSemana[$i] = $datSemana[$i][1];
                } else {
                    $dif = (int) ($s - $id) + $i;
                    $cantSemana[$dif] = $datSemana[$i][1];
                    //echo $i,'- ',$dif, ': ',$cantSemana[$dif],'<br>';
                }
                $id++;
            }
            
            //MESES            
            for ($i = 0; $i < 13; $i++){
                $prodMes[$i] = 0;
                $targetProdMes[$i] = 0;                
            }
            
            for ($i = 0; $i < 52; $i++){
                $prodMes[$i] = 0;
            }
            
            for($i = 0; $i<count($datProdAnio); $i++){
                $anio[$i] = $datProdAnio[$i][0];
                $prodAnio[$i]= $datProdAnio[$i][1]; 
            }
            
            for ($i = 0; $i < count($datTargetProdAnio); $i++){
                $targetProdAnio[$i] = $datTargetProdAnio [$i][1];
            }
            
            for ($i = 0; $i < count($datTargetProdMes); $i++){
                $m[$i] = $datTargetProdMes[$i][0];
                $targetProdMes[$m[$i]] = $datTargetProdMes[$i][1];
            }
            for($i = 0 ;$i<count($datProdMes);$i++){
                $mes[$i] = $datProdMes[$i][0];
                $prodMes[$mes[$i]]= $datProdMes[$i][1]; 
            }
            
            for($i = 0; $i < 100; $i++){
                $noParte[$i] = (string) ''; 
            }
            
            //LISTA DE COLORES
            $color[0] = "#641e16";//marron machin
            $color[1] = "#abb2b9";//GRIS
            $color[2] = "#dc7633";//ROSA CLARO
            $color[3] = "#a9dfbf";//verde muy claro
            $color[4] = "#7d6608";//cafe
            $color[5] = "#d2b4de";//Verde            
            $color[6] = "#7b7d7d";
            $color[7] = "#1b2631";
            $color[8] = "#2980b9";
            $color[9] = "#c0392b"; 
            $color[10] = "#512e5f";//purpura
            $color[11] = "#edbb99";
            $color[12] = "#154360";
            $color[13] = "#0e6251"; 
            $color[14] = "#a9cce3";
            $color[15] = "#7e5109";//cafe
            $color[16] = "#e6b0aa";  
            $color[17] = "#d4e157";
            $color[18] = "#EE82EE";
            $color[26] = "#34495e";
            $color[19] = "#16a085";
            $color[20] = "#27ae60";
            $color[21] = "#ff5722";
            $color[22] = "#3f51b5";
            $color[23] = "#795548";
            $color[24] = "#8bc34a";
            $color[25] = "#1e88e5";
            
            //Para cachar los numero de parte por Mes
            for($i = 0; $i < count($datProdNoPMes);$i++){
                $noParteMes[$i] = $datProdNoPMes[$i][0]; 
                //echo $i,': ',$noParteMes[$i],'<br>';
            }            
            
            for($j = 0; $j < count($datProdNoPMes); $j++ ){
                for ($i = 1; $i <= $ultimoDiaMes; $i++ ){
                    $cantidadProdNoParteDia [$j][$i] = 0;     
                    $cantTotalDia[$i] = 0;
                }                
            }                       
            
            $cantTotal = 0;
            
            //ARREGLO PARA PRODUCCION POR DIA
            for($i = 0; $i < count($datPzasDia); $i++){
                $d = $datPzasDia[$i][0];
                $cantTotalDia[$d] = $datPzasDia[$i][1];
                $cantTotal = $cantTotal + $datPzasDia[$i][1];
                //echo $d,': ', $cantTotalDia[$d],'<br> ';
            }
           
            $cNP = count($datProdNoPMes);
            $cantTotalNP[$cNP] = $cantTotal;
            //ARREGLO PARA PRODUCCION POR NUMERO DE PARTE 
            for($j = 0; $j < count($datProdNoPMes); $j++ ){ 
                $cantTotalNP[$j] = 0;
                for($i = 0; $i < count($datPzasNoParte); $i++){
                    if($datProdNoPMes[$j][0] == (string) $noParteMes[$j]){
                        $cantTotalNP[$i] = $datPzasNoParte[$i][1];
                    }
                } 
            }
            
            //ARREGLO PARA LA TABLA
            for($j = 0; $j < count($datProdNoPMes); $j++ ){
                for($i = 0; $i < count($datProdNoPDia); $i++ ){
                    $dia = $datProdNoPDia[$i][0];
                    $noParteDia = $datProdNoPDia[$i][1];
                    if($noParteDia == $noParteMes[$j] ){                        
                        $cantidadProdNoParteDia [$j][$dia] = $datProdNoPDia[$i][2];     
                        //echo $j,' - ',$dia, ': ',$cantidadProdNoParteDia [$j][$dia],'<br>';
                    } 
                    
                }
            } 
 
        ?>    
<BODY>
    <head>
        <a align=center id="headerFixed" class="contenedor">   
            <div class='fila0'>                
            </div>             
            <h5 class="tituloPareto">
                <?php echo 'Piezas Producidas<br>','Linea:&nbsp'.$line.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp Mes: '. $varMesStr[$month - 1]; ?>
            </h5>            
            <div class="fila1">
                <img src="../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
                <form action="../index.php" method="POST">
                    <?php
                        echo "<input type="."\"hidden\" name="."\"contEntrada\""."value=".'1'.">";
                        echo "<input type="."\"hidden\" name="."\"line\""."value=".$line.">";
                        echo "<input type="."\"hidden\" name="."\"month\""."value=".$month.">";
                        echo "<input type="."\"hidden\" name="."\"year\""."value=".$year.">";
                    ?>
                    <button class="btn btn-primary active btn-sm btnRegresarPareto" 
                            onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" onmouseout="this.style.background='#02538B'">
                        Regresar
                    </button> 
                </form>                 
            </div>                
        </a> 
    
        <script type="text/javascript">
            $(function() {
                $("table").tablesorter({widthFixed: true, widgets: ['zebra']}).tablesorterPager({container: $("#pager")});
            });
	</script>
    </head>
    
    <div id = "graficas">
        <!--------------GRAFICA----dia-------------->
        <div>
            <div aling = "center" id="mensual" class="grafMonths">
                <script>
                    chartCPU = new Highcharts.chart('mensual', {
                    title: {
                        text: 'Mes '
                    },
                    xAxis: {
                        title: {
                            text: 'Mes'
                        },
                        gridLineWidth: 1, 
                        
                        gridLineColor:'#000000',                        
                        categories:  ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
                    },
                    yAxis: [{
                        title: {
                            text: 'Cantidad Pzas'
                        },
                        gridLineWidth: 1,     
                        tickInterval: 1000,
                        gridLineColor:'#000000',
                    }],
                    series: [{ //BARRAS reales
                        color: '#1A06AF',
                        name: 'Indicadores',
                        type: 'column',
                        data: (function() {
                                var data = [];
                                <?php
                                    for($i = 1; $i < 13; $i++){
                                ?>
                                data.push([<?php echo $prodMes[$i];?>]);
                                <?php } ?>
                                return data;
                            })()
                    }, { //LINEA Meta
                        color: '#2ECC71',
                        type: 'spline',
                        name: 'Meta',
                        data: (function() {
                                var data = [];
                                <?php
                                    for($i = 1; $i<13; $i++){
                                ?>
                                data.push([<?php echo $targetProdMes[$i];?>]);
                                <?php } ?>
                                return data;
                            })()
                    }],
                    credits: {
                            enabled: false
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }                
                });
                </script> 
            </div>

            <div aling = "center" id="semanal" class = "grafWeek">
                <script>
                    chartCPU = new Highcharts.chart('semanal', {
                    title: {
                        text: 'Semana '
                    },
                    xAxis: {
                        gridLineWidth: 1,                        
                        gridLineColor:'#000000',
                        title: {
                            text: 'Semana'
                        },
                        categories: (function() {
                                var data = [];
                                <?php
                                    for($i = 0; $i < $numSemanas; $i++){
                                ?>
                                    data.push([<?php echo $sem[$i];?>]);
                                <?php } ?>
                                return data;
                            })()
                    },
                    yAxis: [{
                        gridLineWidth: 1,
                        tickInterval: 1000,
                        gridLineColor:'#000000',
                        title: {
                            text: 'Cantidad Pzas'
                        },
                    }],
                    series: [{ //BARRAS CHUNDAS
                        color: '#1A06AF',
                        name: 'Indicadores',
                        type: 'column',
                        data: (function() {
                                var data = [];
                                <?php
                                    for($i = 0; $i< $numSemanas; $i++){
                                ?>
                                data.push([<?php echo $cantSemana[$i];?>]);
                                <?php } ?>
                                return data;
                            })()
                    }, { //LINEA CHUNDA
                        color: '#2ECC71',
                        type: 'spline',
                        name: 'Meta',
                        data: (function() {
                                var data = [];
                                <?php
                                    for($i = 0; $i < $numSemanas; $i++){
                                ?>
                                data.push([<?php echo $tSemana[$i];?>]);
                                <?php } ?>
                                return data;
                            })()
                    }],
                    credits: {
                            enabled: false
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }                
                });
                </script> 
            </div>
            
            <div aling = "center" id="dia" class = "grafDays">
                <script>                
                    Highcharts.chart('dia', {
                        chart: {
                          type: 'column'
                        },
                        title: {
                          text: 'Día'
                        },
                        xAxis: {
                            gridLineWidth: 1,                        
                            gridLineColor:'#000000',
                            categories: (function() {
                                var data = [];
                                <?php
                                    for($i = 1; $i < $ultimoDiaMes; $i++){
                                ?>
                                data.push([<?php echo $i;?>]);
                                <?php } ?>
                                return data;
                            })()
                        },
                        yAxis: {
                            min: 0,
                            gridLineWidth: 1,                        
                            gridLineColor:'#000000',
                            tickInterval: 500,
                            title: {
                                text: 'Cantidad Pzas'
                            },
                            stackLabels: {
                                enabled: true,
                                style: {
                                    fontWeight: 'bold',
                                    color: (Highcharts.theme && Highcharts.theme.textColor) || <?php echo "'black'" ?>
                                }
                            }
                        },
                        legend: {
                            layout: 'vertical',
                            align: 'right',
                            verticalAlign: 'middle'                    
                        },
                        tooltip: {
                            headerFormat: '<b> Día: {point.x}</b><br/>',
                            pointFormat: 'No.Parte: {series.name}<br/> Cantidad: {point.y}'
                        },
                        plotOptions: {
                            column: {
                                stacking: 'normal',                            
                            }
                        },                        
                        series: [
                            <?php for($j = 0; $j < count($datProdNoPMes); $j++ ){ ?>
                            {      
                                color: <?php echo "'$color[$j]'" ?>,
                                name: <?php echo "'$noParteMes[$j]'";?>,    
                                data: (function() {
                                    var data = [];
                                    <?php
                                        for ($i = 1; $i < $ultimoDiaMes; $i++ ){
                                    ?>
                                        data.push([<?php echo $cantidadProdNoParteDia[$j][$i]?>]);
                                    <?php } ?>
                                    return data;
                                })()                            
                            },
                        <?php }?>
                        ],credits: {
                            enabled: false
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            }
                        }]
                    }
                });//                
                </script> 
            </div>           
        </div>
        
        <div style="margin-top: 50%">
            <div style="margin-left: 140px; width: 82%">
                <table cellspacing="0" class="table-bordered table-hover table-responsive ">
                    <thead>
                        <tr style="background: #F2F2F2">
                            <th class="fixed" style="left: 30px; background: #F2F2F2; width: 109px; ">DÍA</th>
                            <?php
                                for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                            ?>
                                <th><?php echo $i ?></th>
                            <?php 
                                }
                            ?>
                            <th class="fixedF" style="background: #F2F2F2; ">TOTAL</th>        
                        </tr>
                    </thead>                    
                    <tbody>
                        <?php for ($i = 0; $i < count($datProdNoPMes); $i++ ){?>
                            <tr> 
                                <td class="fixed" style="left: 30px; width: 109px;"><?php echo $noParteMes[$i] ?></td>
                                <?php
                                    for ($j = 1; $j <= $ultimoDiaMes; $j++) {
                                ?>
                                    <td>
                                        <?php  
                                            if ($cantidadProdNoParteDia[$i][$j] != ""){ 
                                               echo $cantidadProdNoParteDia[$i][$j];                                   
                                            } else {
                                                echo "0";                                    
                                            } 
                                        ?> 
                                    </td>
                                <?php }
                                    if ($i != $cNP-1 ){ 
                                ?>                                    
                                    <td class="fixedF" ><?php echo $cantTotalNP[$i] ?></td>
                                <?php }else { ?>
                                    <td class="fixedF" ><?php echo $cantTotalNP[$i] ?></td>
                                    <tr>
                                        <td class="fixedFF" ><?php echo $cantTotalNP[$cNP] ?></td>
                                    </tr>
                                <?php  } ?>
                            </tr>
                        <?php }?>
                            
                        <tr>
                            <td class="fixed" style="left: 30px; width: 109px; background: #f5f4ec; ">  TOTAL DÍA </td>
                            <?php
                                for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                            ?>
                                <td style="background: #f5f4ec; "> 
                                    <?php echo $cantTotalDia[$i] ?> 
                                </td>
                            <?php 
                                }
                            ?>                            
                        </tr>                            
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</BODY>
</html>
