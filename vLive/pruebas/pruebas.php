<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto">



<script>
Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Basic drilldown'
        },
        xAxis: {
            type: 'category'
        },

        legend: {
            enabled: true
        },

        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    style: {
                        color: 'white',
                        textShadow: '0 0 2px black, 0 0 2px black'
                    }
                },
                stacking: 'normal'
            }
        },

        series: [{
            name: 'Things',
            data: [{
                
                name: 'Animals',
                y: 5,
                drilldown: 'animals', 
                color: '#5D7044'
            }, {
                name: 'Fruits',
                y: 2,
                drilldown: 'fruits'
            }, {
                name: 'Cars',
                y: 4,
                drilldown: 'cars'
            }]
        }, {
            name: 'Things2',
            data: [{
                name: 'Animals',
                y: 1,
                drilldown: 'animals2'
            }, {
                name: 'Fruits',
                y: 5,
                drilldown: 'fruits2'
            }, {
                name: 'Cars',
                y: 2,
                drilldown: 'cars2'
            }]
        }, {
            name: 'Things3',
            stack: 1,
            data: [{
                name: 'Animals',
                y: 8,
                drilldown: 'animals3'
            }, {
                name: 'Fruits',
                y: 7,
                drilldown: 'fruits3'
            }, {
                name: 'Cars',
                y: 10,
                drilldown: 'cars3'
            }]
        }],
        drilldown: {
            activeDataLabelStyle: {
                color: 'white',
                textShadow: '0 0 2px black, 0 0 2px black'
            },
            series: [{
                id: 'animals',
                name: 'Animals',
                color: '#FFF9D9',
                data: [
                    ['Cats', 4],
                    ['Dogs', 2],
                    ['Cows', 1],
                    ['Sheep', 2],
                    ['Pigs', 1]
                ]
            }, {
                id: 'fruits',
                name: 'Fruits',
                data: [
                    ['Apples', 4],
                    ['Oranges', 2]
                ]
            }, {
                id: 'cars',
                name: 'Cars',
                data: [
                    ['Toyota', 4],
                    ['Opel', 2],
                    ['Volkswagen', 2]
                ]
            },{
                id: 'animals2',
                name: 'Animals2',
                data: [
                    ['Cats', 3],
                    ['Dogs', 5],
                    ['Cows', 6],
                    ['Sheep', 2],
                    ['Pigs', 2]
                ]
            }, {
                id: 'fruits2',
                name: 'Fruits2',
                data: [
                    ['Apples', 1],
                    ['Oranges', 5]
                ]
            }, {
                id: 'cars2',
                name: 'Cars2',
                data: [
                    ['Toyota', 2],
                    ['Opel', 3],
                    ['Volkswagen', 2]
                ]
            },{
                id: 'animals3',
                name: 'Animals3',
                stack: 1,
                data: [
                    ['Cats', 2],
                    ['Dogs', 3],
                    ['Cows', 1],
                    ['Sheep', 1],
                    ['Pigs', 1]
                ]
            }, {
                id: 'fruits3',
                name: 'Fruits3',
                stack: 1,
                data: [
                    ['Apples', 4],
                    ['Oranges', 3]
                ]
            }, {
                id: 'cars3',
                name: 'Cars3',
                stack: 1,
                data: [
                    ['Toyota', 4],
                    ['Opel', 3],
                    ['Volkswagen', 3]
                ]
            }]
        }
    });
    
</script>
</div>




