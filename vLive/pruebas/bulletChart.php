<script src="//www.amcharts.com/lib/4/core.js"></script>
<script src="//www.amcharts.com/lib/4/charts.js"></script>
<script src="//www.amcharts.com/lib/4/themes/animated.js"></script>
<style>
    #chartdiv { 
        width: 100%; 
        height: 580px; 
    } 
</style>


<table>
    <thead>
    </thead>
    <tbody>
        <td style="width: 500px " >
            <div id="chartdiv">
                <script>
                    am4core.useTheme(am4themes_animated);
                    /* Create chart instance */
                    var chart = am4core.create("chartdiv", am4charts.XYChart);
                    chart.paddingRight = 5;

                    /* Add data */
                    chart.data = [{
                      "category": "Research",
                      "value": 45,
                      "Meta": 80
                    }, {
                      "category": "Marketing",
                      "value": 60,
                      "Meta": 75
                    }, {
                      "category": "Distribution",
                      "value": 92,
                      "Meta": 96
                    }];

                    /* Create axes */
                    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
                    categoryAxis.dataFields.category = "category";
                    categoryAxis.renderer.minGridDistance = 30;
                    categoryAxis.renderer.grid.template.disabled = true;

                    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
                    valueAxis.renderer.minGridDistance = 30;
                    valueAxis.renderer.grid.template.disabled = true;
                    valueAxis.min = 0;
                    valueAxis.max = 100;
                    valueAxis.strictMinMax = true;
                    valueAxis.renderer.labels.template.adapter.add("text", function(text) {
                      return text + "%";
                    });

                    /* Create ranges */
                    function createRange(axis, from, to, color) {
                      var range = axis.axisRanges.create();
                      range.value = from;
                      range.endValue = to;
                      range.axisFill.fill = color;
                      range.axisFill.fillOpacity = 0.8;
                      range.label.disabled = true;
                    }

                    createRange(valueAxis, 0, 20, am4core.color("#19d228"));
                    createRange(valueAxis, 20, 40, am4core.color("#b4dd1e"));
                    createRange(valueAxis, 40, 60, am4core.color("#f4fb16"));
                    createRange(valueAxis, 60, 80, am4core.color("#f6d32b"));
                    createRange(valueAxis, 80, 100, am4core.color("#fb7116"));

                    /* Create series */
                    var series = chart.series.push(new am4charts.ColumnSeries());
                    series.dataFields.valueX = "value";
                    series.dataFields.categoryY = "category";
                    series.columns.template.fill = am4core.color("#000");
                    series.columns.template.stroke = am4core.color("#fff");
                    series.columns.template.strokeWidth = 1;
                    series.columns.template.strokeOpacity = 0.5;
                    series.columns.template.height = am4core.percent(25);

                    var series2 = chart.series.push(new am4charts.LineSeries());
                    series2.dataFields.valueX = "Meta";
                    series2.dataFields.categoryY = "category";
                    series2.strokeWidth = 0;

                    var bullet = series2.bullets.push(new am4charts.Bullet());
                    bullet.layout = "absolute"
                    var line = bullet.createChild(am4core.Line);
                    line.x1 = 0;
                    line.y1 = -15;
                    line.x2 = 0;
                    line.y2 = 15;
                    line.stroke = am4core.color("#000");
                    line.strokeWidth = 4;

                </script>
            </div>
        </td>
    </tbody>
</table>  