<?php
 
    function burbuja($vo ,$n) { 
        for($i = 1; $i < $n; $i++) { 
            for($j = 0; $j<$n-$i; $j++) { 
                if($vo[$j] > $vo[$j+1]) { 
                    $k = $vo[$j+1]; 
                    $vo[$j+1] = $vo[$j]; 
                    $vo[$j] = $k; 
                } 
            } 
        }  
        return $vo;
    } 
 
    function main() { 
        $VectorA = array(5,4,3,2,1); 
        $VectorB = burbuja($VectorA,sizeof($VectorA));
        for($i = 0; $i < sizeof($VectorB); $i++){
            echo $VectorB[$i]."\n";
        } 
    } 
 
    main();
?>