<HTML>    
    <link REL=StyleSheet HREF="../css/jGraficas.css" TYPE="text/css" MEDIA=screen>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script> 

    <!-- LIBRERIAS Y CONFIGURACION PARA TABLA -->
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="../js/table-scroll.min.js"></script> 
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 

        function getFixedColumnsData() {} 

        function setTipoDatos() { 
            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON
            var dato = document.getElementById("tipoDatos").value; 

            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes_1.php", 
                type: "post", 
                data: { tipoVista: 1 , tipoDato: dato, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    location.reload(); 
                } 
            }); 
        } 

        function setTipoDatos2() { 
            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON 
            var dato = document.getElementById("tipoDatos").value; 

            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla 
            var ini = document.getElementById("dateIni").value; 
            var fin = document.getElementById("dateEnd").value; 

            $.ajax({ 
                url: "../../db/sesionReportes_1.php", 
                type: "post", 
                data: { tipoVista: 2, tipoDato: dato, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas 
                    location.reload(); 
                } 
            }); 
        } 

    </script> 

    
    
<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../db/ServerFunctions.php';
    session_start();
    $date = new DateTime;

    $costCenter = '000069C162'; 
    $profitCenter = 'P08075'; 
    $fIni = '2018-08-01'; 
    $fFin = '2018-09-30'; 
    
    //$anio = $_SESSION['anio']; 
    //$anio = date("Y", strtotime($fIni));
    //$mes = $_SESSION['mes']; 
    $mes = date("m", strtotime($fIni));
    //echo "<br>",$anio,', ',$mes; 
    
    $anio = date("Y", strtotime($fFin));

    
    //INICIALIZACION DE  VARIABLES 
    # MESES
    $lblMonth[1] = (string) "Jun";
    $lblMonth[2] = (string) "Feb";
    $lblMonth[3] = (string) "Mar";
    $lblMonth[4] = (string) "Apr";
    $lblMonth[5] = (string) "May";
    $lblMonth[6] = (string) "Jun";
    $lblMonth[7] = (string) "Jul";
    $lblMonth[8] = (string) "Aug";
    $lblMonth[9] = (string) "Sep";
    $lblMonth[10] = (string) "Oct";
    $lblMonth[11] = (string) "Nov";
    $lblMonth[12] = (string) "Dec";
    
    for ($i = 1; $i < 13; $i++) { 
        $tMN1[$i] = 0; 
        $countMN2[$i] = 0; 
    } 
    
    #SEMANAL
    $date->setISODate("$anio", 53);
        
    # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52
    if($date->format("W") == 53){ 
        $numSemanas = 53; 
    } else { 
        $numSemanas = 52; 
    } 
    
    //ULTIMO DIA DEL MES
    $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio)); 
    
    #DIA DE LAS SEMANAS
    $sP = date("W",mktime(0,0,0,$mes,01,$anio));//date("w",mktime(0,0,0,$mes,01,$anio));
    $sL = date("W",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

    //INICIALIZAMOS VARIABLES PARA SEMANA
    if ($sP > $sL){
        for ($i = $sP; $i <= $numSemanas; $i++ ) {
            $lblWeek[$i] = "CW-".$i; 
            $contWN2[$i] = 0; 
            $cw[$i] = 0;
            $cwTec[$i] = 0;
            $tWN1[$i] = 0;
        } 
        for($i = 1; $i <= $sL; $i++) {
            $lblWeek[$i] = "CW-".$i;
            $contWN2[$i] = 0;
            $cw[$i] = 0;
            $cwTec[$i] = 0;
            $tWN1[$i] = 0;
        }            
    } else {
        for ($i = $sP; $i <= $sL; $i++) {
            $lblWeek[$i] = "CW-".$i;
            $contWN2[$i] = 0;
            $cw[$i] = 0;
            $cwTec[$i] = 0;
            $tWN1[$i] = 0;
        } 
    } 
    
    $dSI = date("w",mktime(0,0,0,$mes,01,$anio));
    $dSL = date("w",mktime(0,0,0,$mes,$ultimoDiaMes,$anio));

    if ($sP == 0)
        $dSI = 7; 

    if ($sL == 0) 
        $dSL = 7; 

    $fP = date("Y-m-d",mktime(0,0,0,$mes,01-$dSI,$anio)); 
    $fL = date("Y-m-d",mktime(0,0,0,$mes,$ultimoDiaMes+(6-$dSL),$anio)); 
    
    $sTotalD = 0;
    # PERIODO DE DIAS 
    for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
        $date = explode("-", $i); 
        $vDate = $date[0].$date[1].(int)$date[2]; 

        $tDN1[$vDate] = 0; 
        $contDN2[$vDate] = 0; 
        $lblday[$vDate] = date("M d", strtotime($i));
    } 

    $tipoDato = 2;
    
    // CONSULTAS POR COST-CENTER
    if ($tipoDato == 1){         
        //CONSULTAS PARA MESES
        $cIFKMonth = cIFKMonthlyProd($costCenter, $anio );
        for ($i = 0; $i < count($cIFKMonth); $i++ ){
            $month= $cIFKMonth[$i][0];
            $tMN1[$month] = $cIFKMonth[$i][1];

            $cIFKOpMonth = cIFKOperationMonthlyProd($costCenter, $anio, $month); 
            for ($j = 0; $j < count($cIFKMonth); $j++) { 
                $descMN2[$month][$j] = $lblMonth[$month].'_'.$cIFKOpMonth[$j][0]; 
                $tMN2[$month][$j] = $cIFKOpMonth[$j][1]; 
                $countMN2[$month]++; 
                $countMN3[$month][$j] = 0; 
                $cIFKSubCodMonth = cIFKCodeOperationMonthlyProd($costCenter, $anio, $month, $cIFKOpMonth[$j][0]);  
                for ($k = 0; $k < count($cIFKSubCodMonth); $k++ ){ 
                    $descMN3[$month][$j][$k] = $cIFKMonth[$k][0]; 
                    $tMN3[$month][$j][$k] = $cIFKMonth[$k][1]; 
                    $countMN3[$month][$j]++; 
                } 
            } 
        }
        
        //CONSULTA SEMANAL
        $cIFKWeek = cIFKWeeklyProd($costCenter, $fP, $fL); 
        for ($i = 0; $i < count($cIFKWeek); $i++){ 
            $nW = $cIFKWeek[$i][0]; 
            $tWN1[$nW] = $cIFKWeek[$i][1]; 
            $contWN2[$nW] = 0; 
            $cIFKOpWeek = cIFKOperationWeeklyProd($costCenter, $anio, $nW); 
            for ($j = 0; $j < count($cIFKOpWeek);$j++){ 
                $contWN2[$nW]++; 
                $descWN2[$nW][$j] = $lblWeek[$nW].'_'.$cIFKOpWeek[$j][0]; 
                $tWN2[$nW][$j] = $cIFKOpWeek[$j][1]; 
                $contWN3[$nW][$j] = 0; 
                $cIFKSubCodWeek = cIFKCodeOperationWeeklyProd($costCenter, $anio, $nW, $cIFKOpWeek[$j][0]); 
                for ($k = 0; $k < count($cIFKSubCodWeek); $k++ ){ 
                    $contWN3[$nW][$j]++; 
                    $descWN3[$nW][$j][$k] = $cIFKSubCodWeek[$k][0]; 
                    $tWN3[$nW][$j][$k] = $cIFKSubCodWeek[$k][1]; 
                } 
            } 
        } 

        //CONSULTA DIARIA
        $cIFKDay = cIFKDailyProd($costCenter, $fIni, $fFin); 
        for ($i = 0; $i < count($cIFKDay); $i++ ) { 
            $date = explode("-", $cIFKDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            $tDN1[$vDate] = $cIFKDay[$i][1]; 
            $contDN2[$vDate] = 0; 
            $cIFKOpDay = cIFKOperationDailyProd($costCenter, $cIFKDay[$i][0]);
            for ($j = 0; $j < count($cIFKOpDay); $j++){ 
                $descDN2[$vDate][$j] = $lblday[$vDate].'_'.$cIFKOpDay[$j][0];
                $tDN2[$vDate][$j] = $cIFKOpDay[$j][1];
                $contDN2[$vDate]++; 
                $contDN3[$vDate][$j] = 0; 
                $cIFKSubCodDay = cIFKCodeOperationDailyProd($costCenter, $cIFKDay[$i][0], $cIFKOpDay[$j][0]);
                for ($k = 0; $k < count($cIFKSubCodDay); $k++){
                    $descDN3[$vDate][$j][$k] = $cIFKSubCodDay[$k][0];
                    $tDN3[$vDate][$j][$k] = $cIFKSubCodDay[$k][1];
                    $contDN3[$vDate][$j]++;
                } 
            } 
        } 
    
    } else {
        // CONSULTAS POR PROFITCENTER 
        
        #MENSUAL
        $cIFKMonth = cIFKMonthlyCFA($profitCenter, $anio ); 
        for ($i = 0; $i < count($cIFKMonth); $i++ ) { 
            $month = $cIFKMonth[$i][0]; 
            $tMN1[$month] = $cIFKMonth[$i][1]; 

            $cIFKCostMonth = cIFKCostMonthlyCFA($profitCenter, $anio, $month); 
            $countMN2[$month] = count($cIFKCostMonth); 
            //echo "<br>", $month,', ', count($cIFKCostMonth); 
            for ($j = 0; $j < count($cIFKCostMonth); $j++) { 
                $descMN2[$month][$j] = $lblMonth[$month].'_'.$cIFKCostMonth[$j][0]; 
                $tMN2[$month][$j] = $cIFKCostMonth[$j][1]; 

                $cIFKOpMonth = cIFKOperationMonthlyCFA($profitCenter, $cIFKCostMonth[$j][0], $anio, $month); 
                $countMN3[$month][$j] = count($cIFKOpMonth); 
                //echo ", ",count($cIFKOpMonth); 
                for ($k = 0; $k < count($cIFKOpMonth); $k++ ){ 
                    $descMN3[$month][$j][$k] = $cIFKOpMonth[$k][0]; 
                    $tMN3[$month][$j][$k] = $cIFKOpMonth[$k][1]; 
                    //echo "<br><br>", $month,', ',$j,'(',count($cIFKOpMonth),')','.',$k,' -> ',$profitCenter,', ', $cIFKCostMonth[$j][0],', ', $anio,', ',$month,', ', count($cIFKOpMonth),'<br> ',$descMN3[$month][$j][$k],', ',$tMN3[$month][$j][$k]; 
                } 
            } 
        }
        
        #SEMANAL
        $cIFKWeek = cIFKWeeklyCFA($profitCenter, $fP, $fL);  
        for ($i = 0; $i < count($cIFKWeek); $i++){ 
            $nW = $cIFKWeek[$i][0]; 
            $tWN1[$nW] = $cIFKWeek[$i][1]; 

            $cIFKCCostWeek = cIFKCostWeeklyCFA($profitCenter, $anio, $nW); 
            $contWN2[$nW] = count($cIFKCCostWeek);
            for ($j = 0; $j < count($cIFKCCostWeek);$j++){ 
                $descWN2[$nW][$j] = 'CW-'.$nW.'_'.$cIFKCCostWeek[$j][0]; 
                $tWN2[$nW][$j] = $cIFKCCostWeek[$j][1]; 
                $cIFKOpWeek = cIFKOperationWeeklyCFA($profitCenter, $cIFKCCostWeek[$j][0], $anio, $nW); 
                $contWN3[$nW][$j] = count($cIFKOpWeek);
                for ($k = 0; $k < count($cIFKOpWeek); $k++ ){ 
                    $descWN3[$nW][$j][$k] = $cIFKOpWeek[$k][0]; 
                    $tWN3[$nW][$j][$k] = $cIFKOpWeek[$k][1]; 
                } 
            } 
        } 
        
        #DIARIO
        $cIFKDay = cIFKDailyCFA($profitCenter, $fIni, $fFin); 
        for ($i = 0; $i < count($cIFKDay); $i++ ) { 
            $date = explode("-", $cIFKDay[$i][0]); 
            $vDate = $date[0].$date[1].(int)$date[2]; 

            $tDN1[$vDate] = $cIFKDay[$i][1];  
            $contDN2[$vDate] = 0; 
            $sTotalD += $tDN1[$vDate]; 
            //echo '<br><br>',$cIFKDay[$i][0],' -> ', $tDN1[$vDate]; 
            $cIFKCostDay = cIFKCostDailyCFA($profitCenter, $cIFKDay[$i][0]); 
            for ($j = 0; $j < count($cIFKCostDay); $j++){ 
                $descDN2[$vDate][$j] = $lblday[$vDate].'_'.$cIFKCostDay[$j][0]; 
                $tDN2[$vDate][$j] = $cIFKCostDay[$j][1]; 
                $contDN2[$vDate]++; 
                $contDN3[$vDate][$j] = 0; 
                //echo '<br> op: ',$descDN2[$vDate][$j],', ', $tDN2[$vDate][$j]; 
                $cIFKOpDay = cIFKOperationDailyCFA($profitCenter, $cIFKCostDay[$j][0], $cIFKDay[$i][0]); 
                for ($k = 0; $k < count($cIFKOpDay); $k++){ 
                    $descDN3[$vDate][$j][$k] = $cIFKOpDay[$k][0]; 
                    $tDN3[$vDate][$j][$k] = $cIFKOpDay[$k][1]; 
                    $contDN3[$vDate][$j]++; 
                    //echo ', sCo: ', $descDN3[$vDate][$j][$k],', ',$tDN3[$vDate][$j][$k]; 
                } 
            } 
        } 
    }
    
    
    $dias = (strtotime($fIni)- strtotime($fFin))/86400;
    $dias = abs($dias); 
    $dias = floor($dias)+1; 
    //DEFINIMOS LA DIMENCION DE LA CABECERA PARA FECHAS
    //NO PUEDE SER MAYOR A 12 PORQUE SOLO PUEDE ACEPTAR LA TABLA 12 COLUMNAS 
    if ($dias > 11 ) { 
        $rowspan = 12;
    } else { 
        //CUANDO EL RANGO DE DIAS SELECCIONADO ES MENOR A 11 SE REDIMENCIONAN LAS COLUMNAS 
        //A SOLO EL CONTADOR ENTRE LOS DIAS SELECCIONADOS 
        $rowspan = $dias;
    } 


?>

<body>     
    <?php echo "<br>", $tipoDato,"<br>"; ?> 
    
    <div> 
        <div id="jTMonth" name="jTMonth" class="jidokaMonth" > 
            <script>
                var dataMonth = [ 
                <?php  for($i = 1; $i <= 12; $i++) {  ?> 
                { 
                    "category": "<?php echo $lblMonth[$i]; ?>", 
                    "income": <?php echo $tMN1[$i] ?>, 
                    "url":"#", 
                    "description":"click to drill-down", 
                    "months": [ 
                    <?php for ($j = 0; $j < $countMN2[$i]; $j++){ ?>
                        { 
                            "category": "<?php echo $descMN2[$i][$j]; ?>", 
                            "income": <?php echo $tMN2[$i][$j]; ?>,
                            "url":"#",
                            "description":"click to drill-down",
                            "nivel3": [
                            <?php for ($k = 0; $k < $countMN3[$i][$j]; $k++ ){ ?>
                                { "category": "<?php echo $descMN3[$i][$j][$k]; ?>", 
                                  "income": <?php echo $tMN3[$i][$j][$k]; ?> 
                                },
                            <?php } ?>
                            ] 
                        },
                    <?php } ?>                
                    ]
                },
                <?php } ?>
                ];

                var chartMonth = AmCharts.makeChart("jTMonth", {
                    "type": "serial",
                    "theme": "none",
                    "pathToImages": "/lib/3/images/",
                    "autoMargins": false,
                    "marginLeft": 30,
                    "marginRight": 8,
                    "marginTop": 10,
                    "marginBottom": 26,
                    "titles": [{
                        "text": "IFK"
                    }],
                    "dataProvider": dataMonth,
                    "startDuration": 1,
                    "graphs": [{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Income",
                        "type": "column",
                        "valueField": "income","urlField":"url"
                    }],
                    "categoryField": "category",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0
                    }, "chartScrollbar": {
                        "graph": "g2",
                        "oppositeAxis": false,
                        "offset": 30,
                        "scrollbarHeight": 20,
                        "backgroundAlpha": 0,
                        "selectedBackgroundAlpha": 0.1,
                        "selectedBackgroundColor": "#888888",
                        "graphFillAlpha": 0,
                        "graphLineAlpha": 0.5,
                        "selectedGraphFillAlpha": 0,
                        "selectedGraphLineAlpha": 1,
                        "autoGridCount": true,
                        "color": "#AAAAAA"
                    },
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2
                    }

                });

                chartMonth.addListener("clickGraphItem", function (event) {
                    if ( 'object' === typeof event.item.dataContext.months ) {
                        // set the monthly data for the clicked month
                        event.chart.dataProvider = event.item.dataContext.months;
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';
                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGMonth();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                    if ( 'object' === typeof event.item.dataContext.nivel3 ) {

                        // set the monthly data for the clicked month 
                        event.chart.dataProvider = event.item.dataContext.nivel3; 
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';

                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGMonth2();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                });

                // function which resets the chart back to yearly data
                function resetGMonth() {
                    chartMonth.dataProvider = dataMonth;
                    chartMonth.titles[0].text = 'IFK';

                    // remove the "Go back" label
                    chartMonth.allLabels = [];

                    chartMonth.validateData();
                    chartMonth.animateAgain();
                }

                function resetGMonth2() {
                    chartMonth.dataProvider = dataMonth; 
                    chartMonth.titles[0].text = 'IFK'; 

                    // remove the "Go back" label 
                    chartMonth.allLabels = []; 

                    chartMonth.validateData(); 
                    chartMonth.animateAgain(); 
                }
            </script>
        </div>

        <div id="jTWeek" name="jTWeek" class="jidokaWeek" >
            <script>
                var dataWeek = [
                <?php  if ($sP > $sL){ 
                    for ($i = $sP; $i <= $numSemanas; $i++ ) {
                ?>
                    {
                        "category": "<?php echo $lblWeek[$i]; ?>",
                        "income": <?php echo $tWN1[$i] ?>,
                        "url":"#",
                        "description":"click to drill-down",
                        "months": [
                        <?php for ($j = 0; $j < $contWN2[$i]; $j++) { ?>
                            { 
                                "category": "<?php echo $descWN2[$i][$j]; ?>", 
                                "income": <?php echo $tWN2[$i][$j]; ?>,
                                "url":"#",
                                "description":"click to drill-down",
                                "nivel3": [
                                <?php for ($k = 0; $k < $contWN3[$i][$j]; $k++){ ?>
                                    { "category": "<?php echo $descWN3[$i][$j][$k]; ?>", 
                                      "income": <?php echo $tWN3[$i][$j][$k]; ?> 
                                    },
                                <?php } ?>
                                ] 
                            },
                        <?php } ?>                
                        ]
                    },                
                <?php
                    } 
                    for($i = 1; $i <= $sL; $i++) {
                    ?>
                    {
                        "category": "<?php echo $lblWeek[$i]; ?>",
                        "income": <?php echo $tWN1[$i] ?>,
                        "url":"#",
                        "description":"click to drill-down",
                        "months": [
                        <?php for ($j = 0; $j < $contWN2[$i]; $j++) { ?>
                            { 
                                "category": "<?php echo $descWN2[$i][$j]; ?>", 
                                "income": <?php echo $tWN2[$i][$j]; ?>,
                                "url":"#",
                                "description":"click to drill-down",
                                "nivel3": [
                                <?php for ($k = 0; $k < $contWN3[$i][$j]; $k++){ ?>
                                    { "category": "<?php echo $descWN3[$i][$j][$k]; ?>", 
                                      "income": <?php echo $tWN3[$i][$j][$k]; ?> 
                                    },
                                <?php } ?>
                                ] 
                            },
                        <?php } ?>                
                        ]
                    },
                <?php 
                    }                
                } else {    
                    for ($i = $sP; $i <= $sL; $i++) {
                    ?>    
                    {
                        "category": "<?php echo $lblWeek[$i]; ?>",
                        "income": <?php echo $tWN1[$i] ?>,
                        "url":"#",
                        "description":"click to drill-down",
                        "months": [
                        <?php for ($j = 0; $j < $contWN2[$i]; $j++) { ?>
                            { 
                                "category": "<?php echo $descWN2[$i][$j]; ?>", 
                                "income": <?php echo $tWN2[$i][$j]; ?>,
                                "url":"#",
                                "description":"click to drill-down",
                                "nivel3": [
                                <?php for ($k = 0; $k < $contWN3[$i][$j]; $k++){ ?>
                                    { "category": "<?php echo $descWN3[$i][$j][$k]; ?>", 
                                      "income": <?php echo $tWN3[$i][$j][$k]; ?> 
                                    },
                                <?php } ?>
                                ] 
                            },
                        <?php } ?>                
                        ]
                    },
                <?php }
                } ?>
                ];

                var chartWeek = AmCharts.makeChart("jTWeek", {
                    "type": "serial",
                    "theme": "none",
                    "pathToImages": "/lib/3/images/",
                    "autoMargins": false,
                    "marginLeft": 30,
                    "marginRight": 8,
                    "marginTop": 10,
                    "marginBottom": 26,
                    "titles": [{
                        "text": "IFK"
                    }],
                    "dataProvider": dataWeek,
                    "startDuration": 1,
                    "graphs": [{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Income",
                        "type": "column",
                        "valueField": "income","urlField":"url"
                    }],
                    "categoryField": "category",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0
                    }, "chartScrollbar": {
                        "graph": "g2",
                        "oppositeAxis": false,
                        "offset": 30,
                        "scrollbarHeight": 20,
                        "backgroundAlpha": 0,
                        "selectedBackgroundAlpha": 0.1,
                        "selectedBackgroundColor": "#888888",
                        "graphFillAlpha": 0,
                        "graphLineAlpha": 0.5,
                        "selectedGraphFillAlpha": 0,
                        "selectedGraphLineAlpha": 1,
                        "autoGridCount": true,
                        "color": "#AAAAAA"
                    },
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2
                    }

                });

                chartWeek.addListener("clickGraphItem", function (event) {
                    if ( 'object' === typeof event.item.dataContext.months ) {
                        // set the monthly data for the clicked month
                        event.chart.dataProvider = event.item.dataContext.months;
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';
                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGWeek();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                    if ( 'object' === typeof event.item.dataContext.nivel3 ) {

                        // set the monthly data for the clicked month 
                        event.chart.dataProvider = event.item.dataContext.nivel3; 
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';

                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGWeek2();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    } 

                });
                    
                // function which resets the chart back to yearly data
                function resetGWeek() { 
                    chartWeek.dataProvider = dataWeek; 
                    chartWeek.titles[0].text = 'IFK'; 

                    // remove the "Go back" label
                    chartWeek.allLabels = []; 

                    chartWeek.validateData();
                    chartWeek.animateAgain();
                }

                function resetGWeek2() {
                    chartWeek.dataProvider = dataWeek; 
                    chartWeek.titles[0].text = 'IFK'; 

                    // remove the "Go back" label 
                    chartWeek.allLabels = []; 

                    chartWeek.validateData(); 
                    chartWeek.animateAgain(); 
                }
            </script>
        </div>
        
        <!-- TABLA DE MESES -->
        <table style="width: 58%; margin-left: 1%; border: 1px solid #BABABA;" >
            <tr >
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >Mes</th>
                <?php for ($i = 1; $i < 13; $i++ ) { ?>
                    <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblMonth[$i] ?></th>
                <?php } ?> 
            </tr> 
            <tr> 
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;" >IFK</th>
                <?php for ($i = 1; $i < 13; $i++ ) { ?>
                    <td align="center" style="width: 7.5%; border: 1px solid #BABABA;"><?php echo $tMN1[$i] ?></td>      
                <?php } ?>
            </tr>                
        </table>

        <!-- TABLA DE SEMANA -->
        <table style="width: 35%; margin-left: 63%; margin-top: -70px; border: 1px solid #BABABA; ">
            <tr>
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"> Semana </th>
                <?php 
                if ($sP > $sL){
                    for ($i = $sP; $i <= $numSemanas; $i++ ) { ?>
                        <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblWeek[$i] ?></th>
                    <?php } 
                     for($i = 1; $i <= $sL; $i++) {
                    ?>
                        <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblWeek[$i] ?></th>
                    <?php
                    }
                } else {                        
                    for ($i = $sP; $i <= $sL; $i++) { ?>
                        <th align="center" style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;"><?php echo $lblWeek[$i] ?></th>
                <?php }} ?>                            
            </tr>
            <tr>    
                <th style="background: #E8E8E8; border: 1px solid #BABABA; text-align: center;">IFK</th>
                <?php 
                if ($sP > $sL){
                    for ($i = $sP; $i <= $numSemanas; $i++ ) { ?>
                        <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tWN1[$i] ?></td>
                    <?php } 
                    for($i = 1; $i <= $sL; $i++) { 
                    ?>
                        <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tWN1[$i] ?></td>
                    <?php
                    }
                } else {                        
                    for ($i = $sP; $i <= $sL; $i++) { ?>
                        <td align="center" style="border: 1px solid #BABABA;" ><?php echo $tWN1[$i] ?></td>
                <?php }} ?>  
            </tr>                
        </table>  
        <br> 
        
        <div id="grafDays" name="grafDays" class="jidokaDay" >
            <script>
                var dataDay = [
                <?php  for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                    $date = explode("-", $i); 
                    $vDate = $date[0].$date[1].(int)$date[2]; 
                    ?>    
                {
                    "category": "<?php echo $lblday[$vDate]; ?>",
                    "income": <?php echo $tDN1[$vDate] ?>,
                    "url":"#",
                    "description":"click to drill-down",
                    "months": [
                    <?php for ($j = 0; $j < $contDN2[$vDate]; $j++){ ?>
                        { 
                            "category": "<?php echo $descDN2[$vDate][$j]; ?>", 
                            "income": <?php echo $tDN2[$vDate][$j]; ?>,
                            "url":"#",
                            "description":"click to drill-down",
                            "nivel3": [
                            <?php for ($k = 0; $k < $contDN3[$vDate][$j]; $k++ ){ ?>
                                { "category": "<?php echo $descDN3[$vDate][$j][$k]; ?>", 
                                  "income": <?php echo $tDN3[$vDate][$j][$k]; ?> 
                                },
                            <?php } ?>
                            ] 
                        },
                    <?php } ?>                
                    ]
                },
                <?php } ?>
                ];

                var chartDay = AmCharts.makeChart("grafDays", {
                    "type": "serial",
                    "theme": "none",
                    "pathToImages": "/lib/3/images/",
                    "autoMargins": false,
                    "marginLeft": 30,
                    "marginRight": 8,
                    "marginTop": 10,
                    "marginBottom": 26,
                    "titles": [{
                        "text": "IFK"
                    }],
                    "dataProvider": dataDay,
                    "startDuration": 1,
                    "graphs": [{
                        "alphaField": "alpha",
                        "balloonText": "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b> [[additional]]</span>",
                        "dashLengthField": "dashLengthColumn",
                        "fillAlphas": 1,
                        "title": "Income",
                        "type": "column",
                        "valueField": "income","urlField":"url"
                    }],
                    "categoryField": "category",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "axisAlpha": 0,
                        "tickLength": 0
                    }, "chartScrollbar": {
                        "graph": "g2",
                        "oppositeAxis": false,
                        "offset": 30,
                        "scrollbarHeight": 20,
                        "backgroundAlpha": 0,
                        "selectedBackgroundAlpha": 0.1,
                        "selectedBackgroundColor": "#888888",
                        "graphFillAlpha": 0,
                        "graphLineAlpha": 0.5,
                        "selectedGraphFillAlpha": 0,
                        "selectedGraphLineAlpha": 1,
                        "autoGridCount": true,
                        "color": "#AAAAAA"
                    },
                    "chartCursor": {
                        "pan": true,
                        "valueLineEnabled": true,
                        "valueLineBalloonEnabled": true,
                        "cursorAlpha": 0,
                        "valueLineAlpha": 0.2
                    }

                });

                chartDay.addListener("clickGraphItem", function (event) {
                    if ( 'object' === typeof event.item.dataContext.months ) {
                        // set the monthly data for the clicked month
                        event.chart.dataProvider = event.item.dataContext.months;
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';
                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGDay();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                    if ( 'object' === typeof event.item.dataContext.nivel3 ) { 
                        // set the monthly data for the clicked month 
                        event.chart.dataProvider = event.item.dataContext.nivel3; 
                        // update the chart title
                        event.chart.titles[0].text = event.item.dataContext.category + ' ';

                        // let's add a label to go back to yearly data
                        event.chart.addLabel(
                            "!10", 25, 
                            "Go back >",
                            "right", 
                            undefined, 
                            undefined, 
                            undefined, 
                            undefined, 
                            true, 
                            'javascript:resetGDay2();');

                        // validate the new data and make the chart animate again
                        event.chart.validateData();
                        event.chart.animateAgain();
                    }

                });

                // function which resets the chart back to yearly data
                function resetGDay() {
                    chartDay.dataProvider = dataDay;
                    chartDay.titles[0].text = 'IFK';

                    // remove the "Go back" label
                    chartDay.allLabels = [];

                    chartDay.validateData();
                    chartDay.animateAgain();
                }

                function resetGDay2() {
                    chartDay.dataProvider = dataDay; 
                    chartDay.titles[0].text = 'IFK'; 

                    // remove the "Go back" label 
                    chartDay.allLabels = []; 

                    chartDay.validateData(); 
                    chartDay.animateAgain(); 
                }
            </script>
        </div>
        
        <!-- APARTADO PARA PICKERS --> 
        <div id="pnlPikers" style="margin-top: 34%" > 
            <script>
                $(function() { 
                    $("#dateIni").datepicker({ 
                        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                        maxDate: new Date(), 
                        //OYENTES DE LOS PICKERS 
                        onSelect: function(date) { 
                            //Cuando se seleccione una fecha se cierra el panel
                            $("#ui-datepicker-div").hide(); 
                        } 
                    }); 

                    $("#dateEnd").datepicker({ 
                        //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                        maxDate: new Date(), 
                        //OYENTES DE LOS PICKERS 
                        onSelect: function ( date ) { 
                            //cuando se seleccione una fecha se cierra el panel 
                            $("#ui-datepicker-div").hide(); 
                        } 
                    }); 
                }); 
            </script> 

            <div >
                <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" > 
                    <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2 contenidoCentrado" > 
                        <select id="tipoDatos" style="border-color: #888; width: 135px;" class="form-control pickers" > 
                            <option value="1" >Profit Center</option> 
                            <option value="2" >Cost Center</option> 
                        </select> 
                    </div> 
                    <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3 contenidoCentrado input-group" style="margin-left: 11%; margin-top: -2.8%" > 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo date("m/d/Y", strtotime($fIni)); ?>">
                        <span class="input-group-addon"> a </span> 
                        <input type="text" class="form-control btn-sm" style="border-color: #888;" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($fFin)); ?>">
                    </div>
                    <div class="col-xs-1 col-sh-1 col-md-1 col-lg-1 contenidoCentrado" style="margin-left: 34%; margin-top: -2.8%" > 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0;" onclick="setTipoDatos()"> 
                            <img src="../../imagenes/confirmar.png"> 
                        </button> 
                    </div> 
                    <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2 contenidoCentrado" style="margin-left: 87%; margin-top: -2.3%"> 
                        <button type="button" class="btn btn-sm" style="background-color: transparent; border: 0; " onclick="setTipoDatos2()"> 
                            <img src="../../imagenes/bAll.png"> 
                        </button> 
                    </div> 
                </div> 
            </div> 
        </div> 
        <br> 
        <div id="holder-semple-1"> 
            <script id="tamplate-semple-1" type="text/mustache"> 
                <table style="width:97%; margin-left: 1.5%; "class="inner-table"> 
                    <thead> 
                        <tr> 
                            <td colspan="2"> </td> 
                            <td colspan="<?php echo $rowspan; ?>" data-scroll-span="<?php echo $dias+2; ?>" > PERIODO </td> 
                            <td rowspan="2" align="center" >Total</td> 
                        </tr> 
                        <tr> 
                            <td>&nbsp;</td> 
                            <td align="center" >PROBLEMA</td> 
                            <?php for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))){ 
                                $date = explode("-", $i); 
                                $vDate = $date[0].$date[1].(int)$date[2]; ?> 
                                <td align="center" ><?php echo $lblday[$vDate]; ?></td> 
                            <?php } ?>  
                        </tr> 
                    </thead> 
                    <tbody> 
                        <tr > 
                            <td align="right" ><?php echo $i ?></td> 
                            <td >IFK</td> 
                            <?php for($j = $fIni; $j <= $fFin; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                                $date = explode("-", $j); 
                                $vDate = $date[0].$date[1].(int)$date[2]; 
                            ?> 
                            <td align="center" ><?php echo $tDN1[$vDate]; ?></td> 
                            <?php if ( $j == $fFin ) {?> 
                            <td align="center" ><?php echo $tDN1[$vDate]; ?> </td> 
                            <?php }} ?> 
                        </tr > 
                    </tbody> 
                    <tfoot> 
                        <tr> 
                            <td colspan="2">Sold Total</td> 
                            <?php for($i = $fIni; $i <= $fFin; $i = date("Y-m-d", strtotime($i ."+ 1 days"))) { 
                                    $date = explode("-", $i); 
                                    $vDate = $date[0].$date[1].(int)$date[2];
                            ?> 
                                <td align="center" ><?php echo $tDN1[$vDate]; ?></td> 
                            <?php if ($i == $fFin) { ?> 
                                <!--CUADRE DE INFORMACION -->
                                <td align="center" ><?php echo $tDN1[$vDate]; ?></td> 
                            <?php }} ?> 
                        </tr> 
                    </tfoot> 
                </table> 
            </script> 
        </div> 
    </div> 
</body> 




