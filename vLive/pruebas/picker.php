<!DOCTYPE html>
<html lang="en">
   <head>
      <title>Review Sample Month Only</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
      <script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.css">
      
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script> 
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
      <script type="text/javascript">
         $(function () {
            $('#datetimepicker').datetimepicker({ 
                format: 'MM/YYYY', 
                viewMode: 'months' 
            });
         });
      </script>
   </head>
   <body >
      <div class="container"  >
            <div class='input-group date'>
                <input type='text' class="form-control" id='datetimepicker' name= "datetimepicker" />
                <span class="input-group-addon">
                <span class="glyphicon glyphicon-calendar"></span>
                </span>
           </div>
          <br><br><br><br><br><br><br><br><br>
          
         <div class="panel-group">
            <div class="panel panel-primary"  >
               <div class="panel-heading" >
                  <h3 class="panel-title" style="text-align: center;">Review Sample Month Only</h3>
               </div>
               <div class="panel-body">
                  <form class="form-horizontal" >
                     <div class="form-group">
                        <label class="control-label col-sm-2"  for="datetimepicker" >Month:</label>
                        <div class="col-sm-2">
                           
                        </div>
                     </div>
                     <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                           <button type="submit" id="button1" class="btn btn-primary">Save</button>                           
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </body>
</html>