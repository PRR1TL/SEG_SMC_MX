<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<html>
    <head>
        <!--LIBRERIAS PARA DISEÑO-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
        
        <style>
            table.scroll {
                width: 100%; /* Optional */
                /* border-collapse: collapse; */
                border-spacing: 0;
                border: 1px solid black;
            }

            table.scroll tbody,
            table.scroll thead { display: block; }

            thead tr th { 
                height: 30px;
                line-height: 30px;
                /*text-align: left;*/
            }

            table.scroll tbody {
                height: 100px;
                overflow-y: auto;
                overflow-x: hidden;
            }

            tbody { border-top: 1px solid black; }

            tbody td, thead th {
                /*width: 20%;  Optional */
                border-right: 1px solid black;
            }

            tbody td:last-child, thead th:last-child {
                border-right: none;
            }
        </style> 
        
        <script> 
            var $table = $('table.scroll'),
                $bodyCells = $table.find('tbody tr:first').children(), colWidth;
//                $bodyCells = 23;

            // Adjust the width of thead cells when window resizes
            $(window).resize(function() {
                // Get the tbody columns width array
                colWidth = $bodyCells.map(function() {
                    return $(this).width();
                }).get();

                // Set the width of thead columns
                $table.find('thead tr').children().each(function(i, v) {
                    $(v).width(colWidth[i]);
                });    
            }).resize(); // Trigger resize handler
        </script>         
    </head>
    <body>
        <div class="scroll-div-tablas" id="TablaContenedor" style="height: 115px;">  
            <table class="scroll">
                <thead>
                    <tr> 
                        <th rowspan="2"> Hour</th> 
                        <th >Time</th> 
                        <th colspan="2">Target</th> 
                        <th colspan="2">Real Est</th> 
                        <th colspan="2">Real Prod</th> 
                        <th >Pces / Hour</th> 
                        <th >Type</th> 
                        <th colspan="2" >Quality(Pces)</th> 
                        <th colspan="4" >Availability Losses</th> 
                        <th colspan="4" data-scroll-span="15" >Ocurrences</th> 
                        <th rowspan="2">OEE</th> 
                        <th rowspan="2">OEE</th> 
                        <th > </th> 
                    </tr> 
                    <tr> 
                        <th >Min</th> 
                        <th >Units</th> 
                        <th >Cum</th> 
                        <th >Units</th> 
                        <th >Cum</th> 
                        <th >Units</th> 
                        <th >Cum</th> 
                        <th ></th> 
                        <th >Type</th> 
                        <th >Scrap</th> 
                        <th >Rework</th> 
                        <th >Changeover</th> 
                        <th >Tecnical</th> 
                        <th >Organizat</th> 
                        <th >Perform</th> 
                        <th >Code</th> 
                        <th >Deviation</th> 
                        <th >Code</th> 
                        <th >Deviation</th> 
                    </tr> 
                </thead>
                <tbody>
                    <?php for ($i = 0; $i < 15; $i++){ ?> 
                    <tr> 
                        <?php for ($j = 0; $j < 7; $j++) { ?> 
                            <td> 0 </td> 
                        <?php } ?> 
                            <td> - </td> 
                        <?php for ($j = 0; $j < 15; $j++) { ?> 
                            <td> 0 </td> 
                        <?php } ?> 
                    </tr> 
                    <?php } ?> 
                </tbody>
            </table>
        </div>
    </body>
</html>

