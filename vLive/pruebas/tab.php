<html>
<head>
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../js/table-scroll.min.js"></script>  
    <script>         
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({ 
                fixedColumnsLeft: 1, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 3, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
    </script> 
    <link rel="stylesheet" href="../css/demo.css" /> 
</head>

<?php 
    include '../db/ServerFunctions.php';
    session_start();
    $date = new DateTime;

?>

<body>
    <div id="holder-semple-1">
        <script id="tamplate-semple-1" type="text/mustache">
            <table width="98%" class="inner-table">
                <thead>
                    <tr>
                        <td > </td> 
                        <td colspan="<?php echo 3; ?>" > PERIODO </td> 
                    </tr> 
                    <tr> 
                        <td>&nbsp;</td>
                        <td align="center" > PROBLEMA </td>
                        <?php for($i = 1; $i <= 16; $i++) { ?>
                            <td align="center" > <?php echo "problema ".$i; ?> </td>
                        <?php } ?> 
                    </tr>
                </thead>
                <tbody>
                    <?php for($i = 1; $i <= 16; $i++) { ?>
                    <tr >
                        <td align="right" ><?php echo $i ?></td> 
                        <td ><?php echo "probl".$i ?></td> 
                        <?php for($j = 1; $j <= 16; $j++ ) { ?> 
                            <td align="center" ><?php echo "valP ".$j; ?></td> 
                        <?php } ?>
                    </tr >
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td >Sold Total</td>
                        <td colspan="1">Problema</td>   
                        <?php for($i = 1; $i <= 16; $i++) { ?>
                            <td align="center" ><?php echo "totalD ".$i; ?></td>
                        <?php } ?>   
                    </tr>
                </tfoot>
            </table>
        </script>
    </div>
</body>
</html>
