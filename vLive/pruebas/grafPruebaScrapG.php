<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>

<style>
    #gTec {
        width : 100%;
        height : 300px;
    }
    
    #chartdiv {
        width : 100%;
        height : 300px;
    }
    
    
</style>

<div id="gTec" >
    <script>
        var dataTec = [
           {
                "year": 2005,
                "income": 23.5,
                "url":"1",
                "description":"click to drill-down",
                "months": [
                    { "year": "1_1", "income": 1,
                        "url":"2",
                        "description":"click to drill-down",
                        "nivel3": [
                            { "datC": 1, "income": 1 }
                        ] 
                    }], 
                "expenses": 18.1,
                "url":"2",
                "description":"click to drill-down",
                "months": [
                    { "year": "2_1", "income": 10,
                        "url":"2",
                        "description":"click to drill-down",
                        "nivel3": [
                            { "datC": 1, "income": 1 }
                        ] 
                    }]
                
                
            },
            {
                "year": 2006,
                "income": 26.2,
                "expenses": 22.8
            },
            {
                "year": 2007,
                "income": 30.1,
                "expenses": 23.9
            },
            {
                "year": 2008,
                "income": 29.5,
                "expenses": 25.1
            },
            {
                "year": 2009,
                "income": 24.6,
                "expenses": 25
            }
	]; 

        var chartTec = AmCharts.makeChart("gTec", {
            "type": "serial",
            "theme": "none",
            "pathToImages": "/lib/3/images/",
            "autoMargins": false,
            "marginLeft": 53,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,
            "titles": [{
                "text": "LOSSES: Técnicas"
            }],
            "dataProvider": dataTec,
            "startDuration": 1,
            "graphs": [
                {
                    "balloonText": "Income:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-1",
                    "lineAlpha": 0.2,
                    "title": "Income",
                    "type": "column",
                    "valueField": "income"
                }, {
                    "balloonText": "Expenses:[[value]]",
                    "fillAlphas": 0.8,
                    "id": "AmGraph-2",
                    "lineAlpha": 0.2,
                    "title": "Expenses",
                    "type": "column",
                    "valueField": "expenses"
                }
            ],
            "categoryField": "year",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }, "chartScrollbar": {
                "graph": "g2",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 20,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 0,
                "valueLineAlpha": 0.2
            }
            
        });

        chartTec.addListener("clickGraphItem", function (event) {
            if ( 'object' === typeof event.item.dataContext.months ) {
                // set the monthly data for the clicked month
                event.chart.dataProvider = event.item.dataContext.months;
                // update the chart title
                event.chart.titles[0].text = event.item.dataContext.category + ' ';
                // let's add a label to go back to yearly data
                event.chart.addLabel(
                    "!10", 25, 
                    "Go back >",
                    "right", 
                    undefined, 
                    undefined, 
                    undefined, 
                    undefined, 
                    true, 
                    'javascript:resetGTec();');

                // validate the new data and make the chart animate again
                event.chart.validateData();
                event.chart.animateAgain();
            }
            
            if ( 'object' === typeof event.item.dataContext.nivel3 ) {

                // set the monthly data for the clicked month 
                event.chart.dataProvider = event.item.dataContext.nivel3; 
                // update the chart title
                event.chart.titles[0].text = event.item.dataContext.category + ' ';

                // let's add a label to go back to yearly data
                event.chart.addLabel(
                    "!10", 25, 
                    "Go back >",
                    "right", 
                    undefined, 
                    undefined, 
                    undefined, 
                    undefined, 
                    true, 
                    'javascript:resetGTec2();');

                // validate the new data and make the chart animate again
                event.chart.validateData();
                event.chart.animateAgain();
            }
            
        });

        // function which resets the chart back to yearly data
        function resetGTec() {
            chartTec.dataProvider = dataTec;
            chartTec.titles[0].text = 'LOSSES: TECNICAS';

            // remove the "Go back" label
            chartTec.allLabels = [];

            chartTec.validateData();
            chartTec.animateAgain();
        }
        
        function resetGTec2() {
            chartTec.dataProvider = dataTec; 
            chartTec.titles[0].text = 'LOSSES: TECNICAS'; 

            // remove the "Go back" label 
            chartTec.allLabels = []; 

            chartTec.validateData(); 
            chartTec.animateAgain(); 
        }
    </script>
</div>





