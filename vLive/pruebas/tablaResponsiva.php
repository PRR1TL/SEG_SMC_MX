<style>
.intro {
    max-width:1280px;
    margin:1em auto;
}
.table-scroll {
    position: relative;
    max-width: 1280px;
    width:100%;
    margin: auto;
    display:table;
}
.table-wrap {
    width: 100%;
    display:block;
    height: 300px;
    overflow: auto;
    position:relative;
    z-index:1;
}
.table-scroll table {
    width: 100%;
    margin: auto;
    border-collapse: separate;
    border-spacing: 0;
}
.table-scroll th, .table-scroll td {
    padding: 5px 10px;
    border: 1px solid #000;
    background: #fff;
    vertical-align: top;
}
.faux-table table {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    pointer-events: none;
}
.faux-table table + table {
    top: auto;
    bottom: 0;
}
.faux-table table tbody, .faux-table  tfoot {
    visibility: hidden;
    border-color: transparent;
}
.faux-table table + table thead {
    visibility: hidden;
    border-color: transparent;
}
.faux-table table + table  tfoot{
    visibility:visible;
    border-color:#000;
}
.faux-table thead th, .faux-table tfoot th, .faux-table tfoot td {
    background: #ccc;
}
.faux-table {
    position:absolute;
    top:0;
    right:0;
    left:0;
    bottom:0;
    overflow-y:scroll;
}
.faux-table thead, .faux-table tfoot, .faux-table thead th, .faux-table tfoot th, .faux-table tfoot td {
    position:relative;
    z-index:2;
}
</style>

<script>
(function() {
    var fauxTable = document.getElementById("faux-table");
    var mainTable = document.getElementById("main-table");
    var clonedElement = mainTable.cloneNode(true);
    var clonedElement2 = mainTable.cloneNode(true);
    clonedElement.id = "";
    clonedElement2.id = "";
    fauxTable.appendChild(clonedElement);
    fauxTable.appendChild(clonedElement2);
})();

</script>

<div style="width: 80% ">
<div id="table-scroll" class="table-scroll">
  <div id="faux-table" class="faux-table" aria="hidden"></div>
  <div class="table-wrap">
    <table id="main-table" class="main-table">
      <thead>
          <tr style="position:absolute;" >
                <th></th>
                <th > Time</th>
                <th COLSPAN=2 > Target</th>
                <th COLSPAN=2 > Real Est</th>
                <th COLSPAN=2 > Real Prod</th>
                <th > Pces / Hour</th>
                <th > Type</th>
                <th COLSPAN=2 > Quality[Pces]</th>
                <th COLSPAN=4 > Availability Losses</th>
                <th COLSPAN=4 > Ocurrences</th>
                <th COLSPAN=2 > OEE%</th>
            </tr>
            <tr style="position:absolute;" > 
                 <th > Period</th> 
                 <th > Mins</th> 
                 <th > Units</th> <th > Cum</th> 
                 <th > Units</th> <th > Cum</th> 
                 <th > Units</th> <th > Cum</th> 
                 <th > </th>
                 <th > Type / TC</th>
                 <th > Scrap</th> <th > Rework</th>
                 <th > Changeover</th> <th > Technical</th> <th > Organizat</th> <th > Perform</th>
                 <th > Code</th><th > Deviation</th> <th > Code</th> <th > Deviation</th> 
                 <th > PERIODO OEE</th> <th > OEE Cumm</th>                         
            </tr>
      </thead>
      <tbody>
        <tr>
          <th>Left Column</th>
          <td>Cell content<br>
            test </td>
          <td><a href="#">Cell content longer</a></td>
          <td>Cell content with more content and more content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
        <tr>
          <th>Left Column</th>
          <td>Cell content</td>
          <td>Cell content longer</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
          <td>Cell content</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <th>Footer 1</th>
          <td>Footer 2</td>
          <td>Footer 3</td>
          <td>Footer 4</td>
          <td>Footer 5</td>
          <td>Footer 6</td>
          <td>Footer 7</td>
          <td>Footer 8</td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>
</div>