<html xmlns="http://www.w3.org/1999/xhtml">
    <head> 
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> 
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> 
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script> 
        <script src="table-scroll.min.js"></script> 
        
        <script src="//www.amcharts.com/lib/4/core.js"></script>
        <script src="//www.amcharts.com/lib/4/charts.js"></script>
        <script src="//www.amcharts.com/lib/4/themes/animated.js"></script>
        
        <link rel="stylesheet" href="../css/demo.css" /> 
        
    </head> 
    
    <script>
        $(function () {
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({ 
                fixedColumnsLeft: 9, 
                fixedColumnsRight: 2, 
                columnsInScrollableArea: 5, 
                scrollX: 1, 
                scrollY: 10 
            });
            
            function getFixedColumnsData() {} 
        });

    </script>

    <body> 
        <div id="holder-semple-1" style="height: 50px" > 
            <script id="tamplate-semple-1" type="text/mustache" > 
            <table width="100%" class="inner-table"> 
                <thead> 
                    <tr> 
                        <td rowspan="2" style="width: 20px">Period</td> 
                        <td >Time</td> 
                        <td colspan="2">Target</td> 
                        <td colspan="2">Real Est</td> 
                        <td colspan="2">Real Prod</td> 
                        <td >Pces / Hour</td> 
                        <td colspan="5" data-scroll-span="15">Periods</td> 
                        <td rowspan="2">OEE</td> 
                        <td rowspan="2">OEE</td> 
                    </tr> 
                    <tr> 
                        <td style="width: 20px" >Min</td> 
                        <td style="width: 20px" >Units</td> 
                        <td style="width: 20px" >Cum</td> 
                        <td style="width: 20px" >Units</td> 
                        <td style="width: 20px" >Cum</td> 
                        <td style="width: 20px" >Units</td> 
                        <td style="width: 20px" >Cum</td> 
                        <td style="width: 100px" ></td> 
                        <?php for ($i = 0; $i < 11; $i++){ ?> 
                        <td style="width: 90px;"><?php echo $i; ?></td> 
                        <?php } ?>
                    </tr> 
                </thead> 
                <tbody> 
                    <?php for ($i = 0; $i < 74; $i++){ ?>
                        <tr> 
                            <td> <?php echo $i; ?></td>  
                            <td> <?php echo 60; ?> </td> 
                            <?php for ($j = 0; $j < 6; $j++){ ?> 
                            <td> 0</td> 
                            <?php } ?>
                            <td >
                                <div id="<?php echo "chartdiv".$i; ?>" style="height: 50px" >
                                    <script>
                                        // Themes begin
                                        am4core.useTheme(am4themes_animated); 
                                        // Themes end
                                        var container = am4core.create("<?php echo "chartdiv".$i; ?>", am4core.Container); 
                                        container.width = am4core.percent(100); 
                                        container.height = am4core.percent(100);
                                        container.layout = "vertical";
                                        container.hideCredits = true;

                                        /* Create chart instance */
                                        var chart = container.createChild(am4charts.XYChart);
                                        chart.paddingRight = 10;

                                        /* Add data */
                                        chart.data = [{
                                          "category": "Evaluation",
                                          "value": 65,
                                          "Meta": 240
                                        }]; 

                                        /* Create axes */
                                        var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
                                        categoryAxis.renderer.labels.template.disabled = true; 
                                        categoryAxis.dataFields.category = "category";
                                        categoryAxis.renderer.minGridDistance = 10;
                                        categoryAxis.renderer.grid.template.disabled = true;

                                        var valueAxis = chart.xAxes.push(new am4charts.ValueAxis());
                                        valueAxis.renderer.minGridDistance = 10;
                                        valueAxis.renderer.labels.template.disabled = true; 
                                        valueAxis.renderer.grid.template.disabled = true;
                                        valueAxis.min = 0;
                                        valueAxis.strictMinMax = true;
                                        valueAxis.renderer.labels.template.adapter.add("text", function(text) {
                                          return text ;
                                        }); 

                                        /* Create series */
                                        var series = chart.series.push(new am4charts.ColumnSeries());
                                        series.dataFields.valueX = "value";
                                        series.dataFields.categoryY = "category";
                                        series.columns.template.fill = am4core.color("#23B14D");
                                        series.columns.template.stroke = am4core.color("#334EFF");
                                        series.columns.template.strokeWidth = .1;
                                        series.columns.template.strokeOpacity = 0.5;
                                        series.columns.template.height = am4core.percent(100);
                                        series.columns.template.tooltipText = "[bold]Real: [/] {value}pzas [bold]Meta: [/]{target}pzas"; 

                                        var series2 = chart.series.push(new am4charts.LineSeries()); 
                                        series2.dataFields.valueX = "Meta"; 
                                        series2.dataFields.categoryY = "category"; 
                                        series2.strokeWidth = 4; 

                                        var bullet = series2.bullets.push(new am4charts.Bullet()); 
                                        var line = bullet.createChild(am4core.Line); 
                                        line.x1 = 0; 
                                        line.y1 = -200;
                                        line.x2 = 0;
                                        line.y2 = 200;
                                        line.stroke = am4core.color("#ff0f00");
                                        line.strokeWidth = 4; 

                                    </script>
                                </div>
                            </td> 
                            <?php for ($j = 0; $j < 11; $j++){ ?> 
                            <td> 0</td> 
                            <?php } ?>
                            <td style="width: 40px" >88.88%</td> 
                            <td style="width: 40px" >100.00%</td> 
                        </tr> 
                        <?php } ?> 
                </tbody> 
                <tfoot> 
                    <tr> 
                        <td>&nbsp;</td> 
                        <td colspan="2">Sold Total</td> 
                        <?php for ($j = 0; $j < 17; $j++){ ?> 
                        <td> 0</td> 
                        <?php } ?>
                        <td>&nbsp;</td> 
                    </tr> 
                </tfoot> 
            </table> 
            </script>
        </div> 
    </body> 
</html>
