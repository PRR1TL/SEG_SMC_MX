<?php            
            require_once '../db/ServerFunctions.php'; 
            session_start();

            if (isset($_SESSION["linea"])){
                $line = $_SESSION["linea"];
            } else if (isset($_SESSION["lineaH"])){
                 $line = $_SESSION["lineaH"];
                 $fecha = $_SESSION["fechaH"];
                 $fechaP = date("m/d/Y", strtotime($fecha));
            } else {
                $line = 'L001';
                $_SESSION["lineaH"] = 'L001';
                //$fecha = date('2019-05-23');
                $fecha = date('Y-m-d');
                $_SESSION["fechaH"] = date('Y-m-d');                
                $fechaP = date("m/d/Y", strtotime($fecha));
            }   
            $targetOEE = 80;
            
            $line = 'L003';
            $fecha = '2019-06-17';
            
            $lineasArrObj = listarLineas();
            for ($i = 0; $i < count($lineasArrObj); $i++) {
                $lineaArr[$i] = $lineasArrObj[$i][0];
            }
        
            //INCIAMOS EL CONTEO PARA LAS HORAS
            for( $i = 0; $i < 25; $i++){
                $pzasAcumTarget[$i] = 0;
                $pzasProdH[$i] = 0;
                $cumPzasProdH[$i] = 0;
                $noParteTcH[$i] = '';
                $typeH[$i] = '';
                $tcH[$i] = '';
                $pzasScrapH[$i] = 0;

                $reworkH[$i] = 0;
                $durCambioH[$i] = 0;
                $durTecnicasH[$i] = 0;
                $durCalidadH[$i] = 0;
                $durOrgH[$i] = 0;
                $durDesempenioH[$i] = 0;
                
                //ETIQUETA DE PRODUCCION
                $typeH[$i] = '';

                //LOS CODIGOS X & DESCRIPCION X SON DE CAMBIOS & TECNICAS
                $codeHX[$i] = '';
                $descriptionHX[$i]= '';

                //LOS CODIGOS Y DESCRIPCION Y SON ORGANIZACIONALES & DESEMPENIO
                $codeHY[$i] = '';
                $descriptionHY[$i]= '';

                $periodOEEH[$i] = 0;
                $OEEAcumH[$i] = 0;
                
                $pzasTargetH[$i] = 0;
            }
           
            $datHourlyPzas = hourlyDiaPzasProd($line, $fecha);
            if (count($datHourlyPzas) > 0){
                for($i = 0; $i < count($datHourlyPzas); $i++ ){
                    $h = $datHourlyPzas[$i][0];
                    $hour[$i] = $h;

                    if ($i != 0 && $hour[$i-1] == $h ){
                        //echo $i,': ', $hour[$i-1],', ', $hour[$i],' - ',$pzasProdH[$h-1],'<br>';
                        $pzasProdH[$h] = $pzasProdH[$h] + $datHourlyPzas[$i][1];
                    } else {
                        //echo $i,': ', $hour[$i],', ','<br>';
                        $pzasProdH[$h] = $datHourlyPzas[$i][1];
                    }
                    
                    if ($i != 0){
                        if ($h == 6 || $h == 15 || $h == 0){
                            $cumPzasProdH[$h] = $pzasProdH[$h];
                        } else {
                            $cumPzasProdH[$h] = $cumPzasProdH[$h-1] + $pzasProdH[$h];
                        }
                    } else {                
                        $cumPzasProdH[$h] =  $pzasProdH[$h];
                    }
                    //echo $h,': ',$pzasProdH[$h],', ',$noParteTcH[$h],', ',$cumPzasProdH[$h],'<br>';
                }                
                
                $datNoPartTc = hourlyNoParteTC($line, $fecha);
                for ($i = 0; $i < count($datNoPartTc); $i++){
                    $h = $datNoPartTc[$i][0];                        
                    if (isset($typeH[$h])){
                        $typeH[$h] = $typeH[$h].'<br>'.$datNoPartTc[$i][1].' / '.$datNoPartTc[$i][2];
                    } else {
                        $typeH[$h] = $datNoPartTc[$i][1].' / '.$datNoPartTc[$i][2];
                    }
                }
                        
                //MINUTOS A TRABAJAR CONSIDERANDO LOS MINUTOS DE COMEDOR E INICIO DE TURNO
                for($i = 0; $i < 25; $i++){
                    $minT[$i] = 60;
                    
                    if ($pzasProdH[$i] != 0 && $pzasProdH[$h] > 0){
                        //TIEMPO CICLO GENERAL PARA PRODUCCION                       
                        $datTCPonderadoGeneralH = hourlyTCPonderadoHGeneral($line, $fecha, $i);
                        for ($j = 0; $j < count($datTCPonderadoGeneralH); $j++){
                            $h = $datTCPonderadoGeneralH[$j][0];
                            $tcH[$h] = $datTCPonderadoGeneralH[$j][1];
                            $typeH[$h] = $typeH[$h].'<br> ('.$tcH[$h].')';
                        }
                    } else {
                        //TIEMPO CICLO GENERAL PARA FALLAS                       
                        $datTCPonderadoFallaH= hourlyTCPonderadoFallaHGeneral($line, $fecha, $i);
                        for ($j = 0; $j < count($datTCPonderadoFallaH); $j++){
                            $h = $datTCPonderadoFallaH[$j][0];
                            $tcH[$h] = $datTCPonderadoFallaH[$j][1];
                            $typeH[$h] = $typeH[$h].'<br> ('.$tcH[$h].')';
                        }
                    }
                }
//
                $datMinDescDia = minDescHoraHourly($line, $fecha);
                for ( $i = 0; $i < count($datMinDescDia); $i++){
                    $h = $datMinDescDia[$i][0];
                    $minT[$h] = 60 - $datMinDescDia[$i][1];
                }

                //PIEZAS PLANEADAS POR HORA
                $datHPzasEspPercent = piezasEsperadasyPercentHora($line, $fecha);
                for ($i = 0; $i < count($datHPzasEspPercent); $i++){
                    $h = $datHPzasEspPercent[$i][0];
                    $pzasTargetH[$h] = $datHPzasEspPercent[$i][1];
                    $periodOEEH[$h] = @round($datHPzasEspPercent[$i][2],2);     
                }
                    
                $pzasAcum = 0;
                for( $i = 0; $i < 25 ; $i++){
                    if($i == 0 || $i == 6 || $i == 15){
                        $pzasAcumTarget[$i] = $pzasTargetH[$i];  
                    }else {
                        if ($i == 0){
                            $pzasAcumTarget[$i] = $pzasTargetH[$i]; 
                        }else {
                            if ($typeH[$i] != ''){
                                $pzasAcumTarget[$i] = $pzasAcumTarget[$i-1] + $pzasTargetH[$i]; 
                            } else {
                                $pzasAcumTarget[$i] = $pzasAcumTarget[$i-1] + 0; 
                            }                            
                        }
                    }
                    $pzasAcum = $pzasAcumTarget[$i];
                }

                //CONSULTAS PARA LE HOURLY                
                for ($i = 0; $i < 24; $i++){
                    if ($i != 0 && $i != 6 && $i != 15  ){
                        if ($cumPzasProdH[$i] != 0){
                            $acumuladoPzasProdH[$i] = $acumuladoPzasProdH[$i-1] + $pzasProdH[$i];
                        }else {                        
                            $acumuladoPzasProdH[$i] =  $acumuladoPzasProdH[$i-1];
                        }        
                    }else {
                        $acumuladoPzasProdH[$i] =  $cumPzasProdH[$i];
                    }                    
                }

                //CONSULTAS POR TEMAS (PRIMARIOS)
                //SCRAP POR HORAS
                $datHourlyScrap = hourlyDiaPzasCalidad($line, $fecha);
                for($i = 0; $i < count($datHourlyScrap); $i++){
                    $h = $datHourlyScrap[$i][0];
                    $pzasScrapH[$h] = $datHourlyScrap[$i][1];
                }
                
                //CALIDAD
                $datCalidad = hourlyDiaDurCalidad($line, $fecha);
                for($i = 0; $i < count($datCalidad); $i++){
                    $h = $datCalidad[$i][0];
                    $problemaCalidad[$h] = $datCalidad[$i][1];
                    $opCalidad[$h] = $datCalidad[$i][2];
                    $durCalidadH[$h] = $datCalidad[$i][3];
                }
                
                //REWORK
                //CAMBIOS
                $datCambios = hourlyDiaDurCambios($line, $fecha);
                for($i = 0; $i < count($datCambios); $i++ ){
                    $h = $datCambios[$i][0];
                    $problemaCambioH[$h] = 'Cambio de Modelo '.$datCambios[$i][1].' '. $datCambios[$i][5] .': '.$datCambios[$i][3]. ' a '.$datCambios[$i][4];
                    $opCambioH[$h] = $datCambios[$i][2];
                    $durCambioH[$h] = $datCambios[$i][6];
                }

                //TECNICAS
                $datTecnicas = hourlyDiaDurTecnicas($line, $fecha);
                for($i = 0; $i < count($datTecnicas); $i++ ){
                    $h = $datTecnicas[$i][0];                   
                    $problemaTecnicas[$h] = $datTecnicas[$i][1];
                    $opTecnicas[$h] = $datTecnicas[$i][2];
                    $durTecnicasH[$h] = $datTecnicas[$i][3];
                }
//
                //ORGANIZACIONALES        
                $datOrganizacionales = hourlyDiaOrg($line, $fecha);
                for($i = 0; $i < count($datOrganizacionales); $i++){
                    $h = $datOrganizacionales[$i][0];
                    $problemOrgH[$h] = $datOrganizacionales[$i][1];
                    $opOrgH[$h] = $datOrganizacionales[$i][2];
                    $durOrgH[$h] = $datOrganizacionales[$i][3];
                }        
                
                /*********** APARTADO PARA PROBLEMAS SECUENDARIOS *************/
                //CALIDAD
                $datCalidadSec = hourlyDiaDurCalidadSec($line, $fecha);
                for($i = 0; $i < count($datCalidadSec); $i++){
                    $h = $datCalidadSec[$i][0];
                    $problemaCalidadSec[$h] = $datCalidadSec[$i][1].' ('.$datCalidadSec[$i][3].')';
                    $opCalidadSec[$h] = $datCalidadSec[$i][2];                    
                }
                
                //REWORK
                //CAMBIOS
                $datCambiosSec = hourlyDiaDurCambiosSec($line, $fecha);
                for($i = 0; $i < count($datCambiosSec); $i++ ){
                    $h = $datCambiosSec[$i][0];
                    $problemaCambioSecH[$h] = 'Cambio de Modelo '.$datCambiosSec[$i][1].' '. $datCambiosSec[$i][5] .': '.$datCambiosSec[$i][3]. ' a '.$datCambiosSec[$i][4].' ('.$datCambiosSec[$i][6].')';
                    $opCambioSecH[$h] = $datCambiosSec[$i][2];
                }

                //TECNICAS
                $datTecnicasSec = hourlyDiaDurTecnicasSec($line, $fecha);
                for($i = 0; $i < count($datTecnicasSec); $i++ ){
                    $h = $datTecnicasSec[$i][0];                   
                    $problemaTecnicasSec[$h] = $datTecnicasSec[$i][1].' ('. $datTecnicasSec[$i][3].')';
                    $opTecnicasSec[$h] = $datTecnicasSec[$i][2];
                }

                //ORGANIZACIONALES        
                $datOrganizacionalesSec = hourlyDiaOrgSec($line, $fecha);
                for($i = 0; $i < count($datOrganizacionalesSec); $i++){
                    $h = $datOrganizacionalesSec[$i][0];
                    $problemOrgSecH[$h] = $datOrganizacionalesSec[$i][1].' ('.$datOrganizacionalesSec[$i][3].')';
                    $opOrgSecH[$h] = $datOrganizacionalesSec[$i][2];
                    
                }    

                /**************************************************************/
                
                //DESEMPENIO
                $datDesempenio = hourlyDiaDesempenio($line, $fecha);
                for($i = 0; $i < count($datDesempenio); $i++){
                    $h = $datDesempenio[$i][0];
                    $opDesempenio[$h] = $line;
                    $problemaDesempenioH[$h] = 'Perdida Por Desempenio';
                    $durDesempenioH[$h] = $datDesempenio[$i][1];
                }
                
                // ARREGLO PARA JUNTAS LOS CODIGOS Y LOS PROBLEMAS POR X & Y
                for($i = 0; $i < 25; $i++ ){
                    
                    //X: CALIDAD Y CAMBIO DE MODELO 
                    if (isset($opCambioH[$i]) && isset($opScrap[$i]) ){
                        $codeHX[$i] = $opCalidad[$i].', '.$opCambioH[$i].'<br>';
                        $descriptionHX[$i]= $problemaCalidad[$i].', '.$problemaCambioH[$i].' <br> ';
                    } else if(isset($opCambioH[$i]) && !isset($opScrap[$i])){
                        $codeHX[$i] = $opCambioH[$i].'<br>';
                        $descriptionHX[$i]= $problemaCambioH[$i].' <br> ';
                    } else if(!isset($opCambioH[$i]) && isset($opScrap[$i])){
                        $codeHX[$i] = $opScrap[$i].'<br>';
                        $descriptionHX[$i]= $problemaScrap[$i].' <br> ';
                    }
                    
                    //CODIGOS SECUNDARIOS
                    if (isset($opCalidadSec[$i]) && isset($opCambioSecH[$i])){
                        $codeHX[$i] = $codeHX[$i].$opCalidadSec[$i].'<br>'.$opCambioSecH[$i]; 
                    } else if (!isset($opCalidadSec[$i]) && isset($opCambioSecH[$i])){
                        $codeHX[$i] = $codeHX[$i].'<br>'.$opCambioSecH[$i]; 
                    } else if (isset($opCalidadSec[$i]) && isset($opCambioSecH[$i])){
                        $codeHX[$i] = $codeHX[$i].'<br>'.$opCalidadSec[$i]; 
                    }
                    
                    //PROBLEMAS SECUNDARIOS
                    if (isset($problemaCalidadSec[$i]) && isset($problemaCambioSecH)){
                        $descriptionHX[$i] = $descriptionHX[$i].$problemaCalidadSec[$i].'<br>'.$problemaCambioSecH[$i]; 
                    } else if (!isset($problemaCalidadSec[$i]) && isset($problemaCambioSecH[$i])){
                        $descriptionHX[$i] = $descriptionHX[$i].$problemaCambioSecH[$i]; 
                    } else if (isset($problemaCalidadSec[$i]) && !isset($problemaCambioSec[$i])){
                        $descriptionHX[$i] = $descriptionHX[$i].$problemaCalidadSec[$i]; 
                    }
                    
                    //SEPARACION DE PROBLEMAS
                    if (isset($opOrgH[$i]) && isset($opTecnicas[$i]) && isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opOrgH[$i].', '.$opTecnicas[$i].', '.$opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].', '.$problemaTecnicas[$i].', '.$problemaDesempenioH[$i].' <br> ';
                    } else if (isset($opOrgH[$i]) && isset($problemaTecnicas[$i]) && !isset($opDesempenio[$i])){
                        $codeHY[$i] = $opOrgH[$i].', '.$opTecnicas[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].', '.$problemaTecnicas[$i].' <br> ';
                    } else if (isset($opOrgH[$i]) && !isset($problemaTecnicas[$i]) && isset($opDesempenio[$i])){
                        $codeHY[$i] = $opOrgH[$i].','.$opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].','.$problemaDesempenioH[$i].' <br> ';
                    } else if (!isset($opOrgH[$i]) && isset($problemaTecnicas[$i]) && isset($opDesempenio[$i])){
                        $codeHY[$i] = $opTecnicas[$i].', '.$opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemaTecnicas[$i].', '.$problemaDesempenioH[$i].' <br> ';
                    } else if (isset($opOrgH[$i]) && !isset($opTecnicas[$i]) && !isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opOrgH[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].' <br> ';
                    } else if (!isset($opOrgH[$i]) && isset($opTecnicas[$i]) && !isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opTecnicas[$i].'<br>';
                        $descriptionHY[$i] = $problemaTecnicas[$i].' <br> ';
                    } else if (!isset($opOrgH[$i]) && !isset($opTecnicas[$i]) && isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemaDesempenioH[$i].' <br> ';
                    }  
                    
                    //echo $opTecnicasSec[$i],' , ',$opOrgSecH[$i], '<br>';
                    //CODIGOS SECUNDARIOS
                    if (isset($opTecnicasSec[$i]) && isset($opOrgSecH[$i])){
                        //echo '<br>+<br><br><br><br><br><br>';
                        $codeHY[$i] = $codeHY[$i].$opTecnicasSec[$i].'<br>'.$opOrgSecH[$i]; 
                    } else if (!isset($opTecnicasSec[$i]) && isset($opOrgSecH[$i])){
                        //echo '<br>-<br><br><br><br><br><br>';
                        $codeHY[$i] = $codeHY[$i].$opOrgSecH[$i];
                    } else if (isset($opTecnicasSec[$i]) && !isset($opOrgSecH[$i])){
                        $codeHY[$i] = $codeHY[$i].$opTecnicasSec[$i]; 
                        //echo '<br>.<br><br><br><br><br><br>';
                    }
                    
                    //PROBLEMAS SECUNDARIOS
                    if (isset($problemOrgSecH[$i]) && isset($problemaTecnicasSec[$i])){                       
                        $descriptionHY[$i] = $descriptionHY[$i].$problemOrgSecH[$i].'<br>'.$problemaTecnicasSec[$i]; 
                    } else if (!isset($problemOrgSecH[$i]) && isset($problemaTecnicasSec[$i])){
                        $descriptionHY[$i] = $descriptionHY[$i].$problemaTecnicasSec[$i]; 
                    } else if (isset($problemOrgSecH[$i]) && !isset($problemaTecnicasSec[$i])){
                        $descriptionHY[$i] = $descriptionHY[$i].$problemOrgSecH[$i]; 
                    } 
                    
                }        

                //PARA EL TARGET DE ACUERDO REAL (QUITA LOS MINUTOS EN AUTOMATICO LAS PIEZAS DE ACUERDO A LOS MINUTOS DE LOS PAROS)
                // Y TOMANDO EL TIEMPO PLANEADO QUITANDO COMEDOR, INICIO DE COMEDOR Y ASI            
                for($i = 0; $i < 24; $i++){
                    $tiempoReal[$i] = $minT[$i] - ($durCambioH[$i] + $durTecnicasH[$i] + $durOrgH[$i] + $durDesempenioH[$i]);
                    
                    // COMPARAMOS SI EL TIEMPO PERDIDO ES MAYOR AL TIEMPLO PLANEADO (TIEMPO QUE SE LE QUITA COMEDOR Y JUNTAS)
                    if($tiempoReal[$i] >= 0 ){
                        if ($tcH[$i] != '') {
                            //$pzasTargetRealH[$i] = @round((60 * $tiempoReal[$i]) / $tcH[$i]) - $pzasScrapH[$i];
                            $pzasTargetRealH[$i] = @round((60 * $tiempoReal[$i]) / $tcH[$i]) - $pzasScrapH[$i];
                        } else {                   
                            $pzasTargetRealH[$i] = 0;
                        }                       
                    } else {
                        $pzasTargetRealH[$i] = 0;
                    }
                }

                $pzasAcum2 = 0;
                for( $i = 0; $i < 24 ; $i++){
                    if($i == 0 || $i == 6 || $i == 15 ){
                        $pzasAcumTargetRealH[$i] = $pzasTargetRealH[$i]; 
                    }else {
                        if ($i == 0){
                            $pzasAcumTargetRealH[$i] = $pzasTargetRealH[$i]; 
                        }else {
                            $pzasAcumTargetRealH[$i] = $pzasAcumTargetRealH[$i-1] + $pzasTargetRealH[$i]; 
                        }
                    }
                    $pzasAcum2 = $pzasAcumTargetRealH[$i];
                }

                //TOTALES
//                if (isset($finT1) && isset($finT2) &&  isset($finT3) ){
//                    $totalPzasTarget = $pzasAcumTarget[$finT1-1] + $pzasAcumTarget[$finT2-1] + $pzasAcumTarget[$finT3-1];
//                    $totalPzasTargetReal = $pzasAcumTargetRealH[$finT1-1] + $pzasAcumTargetRealH[$finT2-1] + $pzasAcumTargetRealH[$finT3-1];
//                    $totalPzasProducidas = $acumuladoPzasProdH[$finT1-1] + $acumuladoPzasProdH[$finT2-1] + $acumuladoPzasProdH[$finT3-1];
//                }else if(isset($finT1) && isset($finT2) &&  !isset($finT3)) {
//                    $totalPzasTarget = $pzasAcumTarget[$finT1-1] + $pzasAcumTarget[$finT2-1];
//                    $totalPzasTargetReal = $pzasAcumTargetRealH[$finT1-1] + $pzasAcumTargetRealH[$finT2-1];
//                    $totalPzasProducidas = $acumuladoPzasProdH[$finT1-1] + $acumuladoPzasProdH[$finT2-1];
//                }else if(isset($finT1) && !isset($finT2) &&  isset($finT3)){
//                    $totalPzasTarget = $pzasAcumTarget[$finT1-1] + $pzasAcumTarget[$finT3-1];
//                    $totalPzasTargetReal = $pzasAcumTargetRealH[$finT1-1] + $pzasAcumTargetRealH[$finT3-1];
//                    $totalPzasProducidas = $acumuladoPzasProdH[$finT1-1] + $acumuladoPzasProdH[$finT3-1];
//                } else if (isset($finT1) && !isset($finT2) &&  !isset($finT3) ){
//                    $totalPzasTarget = $pzasAcumTarget[$finT1-1];
//                    $totalPzasTargetReal = $pzasAcumTargetRealH[$finT1-1];
//                    $totalPzasProducidas = $acumuladoPzasProdH[$finT1-1];
//                }
                
                    $totalPzasTarget = $pzasAcumTarget[5] + $pzasAcumTarget[14] + $pzasAcumTarget[23];
                    $totalPzasTargetReal = $pzasAcumTargetRealH[5] + $pzasAcumTargetRealH[14] + $pzasAcumTargetRealH[23] ;
                    $totalPzasProducidas = $acumuladoPzasProdH[5] + $acumuladoPzasProdH[14] + $acumuladoPzasProdH[23];
                
                $percentOEE = @round(($totalPzasProducidas*100)/$totalPzasTarget,2,PHP_ROUND_HALF_EVEN);
                
                //TOTAL DE FALLAS 
                for ($i = 0; $i < 24; $i++){
                    if ($i == 0){                 
                        $pzasTotalScrap = $pzasScrapH[$i];
                        $durTotalCambios = $durCambioH[$i];
                        $durTotalTecnicas = $durTecnicasH[$i];
                        $durTotalOrg = $durOrgH[$i];
                        $durTotalDesempenio = $durDesempenioH[$i];
                    }else {
                        $pzasTotalScrap = $pzasTotalScrap + $pzasScrapH[$i];
                        $durTotalCambios = $durTotalCambios + $durCambioH[$i];
                        $durTotalTecnicas = $durTotalTecnicas + $durTecnicasH[$i];
                        $durTotalOrg = $durTotalOrg + $durOrgH[$i];
                        $durTotalDesempenio = $durTotalDesempenio + $durDesempenioH[$i];
                    }
                }

                $percentLosses = 100 - $percentOEE;
                $timeLosses = $pzasTotalScrap + $durTotalCambios + $durTotalTecnicas + $durTotalOrg + $durTotalDesempenio;
                //$percentCalidad = ;
                $percentCambios = @round(($durTotalCambios * $percentLosses)/$timeLosses,2,PHP_ROUND_HALF_DOWN);
                $percentTecnicas = @round(($durTotalTecnicas * $percentLosses)/$timeLosses,2,PHP_ROUND_HALF_DOWN);
                $percentOrg = @round(($durTotalOrg * $percentLosses)/$timeLosses,2,PHP_ROUND_HALF_DOWN);
                $percentDesempenio = @round(($durTotalDesempenio * $percentLosses)/$timeLosses,2,PHP_ROUND_HALF_DOWN);

                //PORCENTAJES OEE 
                $div = 0;
                $acum[0] = 0;
                for ($i = 0; $i < 24; $i++){
                    if ($i == 0){
                        if ($pzasProdH[$i] != 0){
                            $acum[$i] = ($acumuladoPzasProdH[$i] * 100 ) / $pzasAcumTarget[$i];
                        } else {
                            $acum[$i] = 0;
                        }
                            $div = 1;
                    }else {
                        if ($i == 0 || $i == 6 || $i == 15 ){
                            if ($pzasProdH[$i] > 0){
                                $acum[$i] = ($acumuladoPzasProdH[$i] * 100 ) / $pzasAcumTarget[$i];
                            } else {
                                $acum[$i] = 0;
                            }
                            $div = 1;
                        } else { 
                            if ($pzasProdH[$i] != 0){ 
                                $acum[$i] = ($acumuladoPzasProdH[$i] * 100 ) / $pzasAcumTarget[$i]; 
                            } else { 
                                $acum[$i] = $acum[$i-1]; 
                            } 
                            $div++; 
                        } 
                    } 
                    $OEEAcumH[$i] = @round($acum[$i],2,PHP_ROUND_HALF_UP) ; 
                } 
            } else { 
                $percentCambios = 0;
                $percentTecnicas = 0;
                $percentOrg = 0;
                $percentDesempenio = 0;
                $percentOEE = 0;

                //INDICADORES DE PIEZAS (DAOLY TOTAL)
                $totalPzasTarget = 0;
                $totalPzasTargetReal = 0;
                $totalPzasProducidas = 0;
                $pzasTotalScrap = 0;
                $durTotalCambios = 0;
                $durTotalTecnicas = 0;
                $durTotalOrg = 0;
                $durTotalDesempenio = 0;
            }        
            
            //PIE DE HOURLY 
            $datOEEDay = percentHourlyDay ($line, $fecha);             
            
            $pFnProd = 0;
            $pFnTec = 0;
            $pFnOrg = 0;
            $pFnCal = 0;
            $pFnCam = 0;
            $pFnTFal = 0;
            
            for ($i = 0; $i < count($datOEEDay); $i++){
                $pFnProd = @round($datOEEDay [$i][0],2);
                $pFnTec = @round($datOEEDay [$i][1],2);
                $pFnOrg = @round($datOEEDay [$i][2],2);
                $pFnCal = @round($datOEEDay [$i][3],2);
                $pFnCam = @round($datOEEDay [$i][4],2);
                $pFnTFal = @round($datOEEDay [$i][5],2);
            } 
            
            //APARTADO PARA LINETACK
            //CONSULTA PARA EL TOTAL DE PIEZAS PRODUCIDAS ESE DIA
            $pzasTotalDia = 0;
            $datPzasTotalDia = piezasTotalesDiaHourly($line, $fecha);
            for ($i = 0; $i < count($datPzasTotalDia); $i++ ){
                $pzasTotalDia = $datPzasTotalDia[$i][0];
            }
            
            $lineTakt = 0;            
            //CONSULTA DE CANTIDAD DE PIEZAS POR TIEMPO CICLO           
            $datPzasTC = piezasTCHourly($line, $fecha);
            for ($i = 0; $i < count($datPzasTC); $i++){
                $pzasTc[$i] = $datPzasTC[$i][0]; 
                $tcPzas[$i] = $datPzasTC[$i][1];                
                //HACEMOS EL PORCENTAJE POR TIEMPO CICLO                
                $percentTc[$i] = @round(($pzasTc[$i]*100)/$pzasTotalDia, 0, PHP_ROUND_HALF_UP )*0.010; 
                
                //OBTENEMOS EL PRODUCTO DE LOS PORCENTAJES POR EL TIEMPO CICLO
                $prodTc[$i] = $tcPzas[$i] * $percentTc[$i];
                
                //SE HACE LA SUMATORIA DE LOS PRODUCTOS (SUMPROD)
                $lineTakt += $prodTc[$i];
            }             
        ?>

<html>
    <head>
        <!--LIBRERIAS PARA DISEÑO-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
        
        <script src="//www.amcharts.com/lib/4/core.js"></script> 
        <script src="//www.amcharts.com/lib/4/charts.js"></script> 
        <script src="//www.amcharts.com/lib/4/themes/animated.js"></script> 
        
        <style> 
            table { 
                margin:0 auto; 
                border-collapse:separate; 
                width: 98%; 
                font-size:11.5px; 
            } 
            thead { 
                background:#CCCCCC; 
                display:block; 
            } 
            tbody { 
                height: 63em; 
                overflow-y:scroll; 
                display:block; 
            } 
        </style> 
        
        <script> 
//            var colNumber = 25; //number of table columns 
//
//            for (var i=0; i<colNumber; i++) { 
//                var thWidth=$("#myTable").find("th:eq("+i+")").width(); 
//                var tdWidth=$("#myTable").find("td:eq("+i+")").width();  
//                if (thWidth<tdWidth)
//                    $("#myTable").find("th:eq("+i+")").width(tdWidth); 
//                else 
//                    $("#myTable").find("td:eq("+i+")").width(thWidth); 
//            } 
        </script>
        
    </head>
    <body>
        <div class="scroll-div-tablas" id="TablaContenedor" style="height: 50%; " >             
            <table  border="1" cellpadding="0" cellspacing="0" bordercolor="#000000" >
                <thead>
                    <tr> 
                        <th rowspan="2" style="width: 2.4vw" > Hour</th> 
                        <th style="width: 2.2vw" >Time</th> 
                        <th colspan="2" style="width: 4.9vw" >Target</th> 
                        <th colspan="2" style="width: 4.9vw" >Real Est</th> 
                        <th colspan="2" style="width: 4.9vw" >Real Prod</th> 
                        <th style="width: 8.0vw;" >Pces / Hour</th> 
                        <th style="width: 6.8vw" >Type</th> 
                        <th colspan="2" style="width: 5.6vw" >Quality(Pces)</th> 
                        <th colspan="4" style="width: 13.8vw" >Availability Losses</th> 
                        <th colspan="4" style="width: 37.5vw;" >Ocurrences</th> 
                        <th rowspan="2" style="width: 3.4vw" >OEE</th> 
                        <th rowspan="2" style="width: 4.1vw" >OEE</th>
                    </tr> 
                    <tr> 
                        <th >Min</th> 
                        <th >Units</th> 
                        <th >Cum</th> 
                        <th >Units</th> 
                        <th >Cum</th> 
                        <th >Units</th> 
                        <th >Cum</th> 
                        <th ></th> 
                        <th >Type</th> 
                        <th >Scrap</th> 
                        <th >Rework</th> 
                        <th >Change</th> 
                        <th >Tecnical</th> 
                        <th >Organizat</th> 
                        <th >Perform</th> 
                        <th style="width: 2.3vw" >Code</th> 
                        <th >Deviation</th> 
                        <th style="width: 2.3vw" >Code</th> 
                        <th >Deviation</th> 
                    </tr>    
                </thead>
                <tbody>
                    <?php for ($i = 6; $i < 24; $i++){ ?> 
                    <tr> 
                        <td style="width: 2.4vw" > <?php echo $i,'-',$i+1; ?> </td> 
                        <td style="width: 2.2vw" > <?php echo $minT[$i]; ?> </td> 
                        <td style="width: 2.5vw" > <?php echo $pzasTargetH[$i]; ?> </td> 
                        <td style="width: 2.4vw" > <?php echo $pzasAcumTarget[$i]; ?> </td> 
                        <td style="width: 2.4vw" > <?php echo $pzasTargetRealH[$i]; ?> </td> 
                        <td style="width: 2.4vw" > <?php echo $pzasAcumTargetRealH[$i]; ?> </td> 
                        <td style="width: 2.4vw" > <?php echo $pzasProdH[$i]; ?> </td> 
                        <td style="width: 2.3vw" > <?php echo $acumuladoPzasProdH[$i]; ?> </td>                         
                        <td style="width: 8.0vw" > 
                            <div id="<?php echo "chartdiv".$i; ?>" style="height: 40px; margin-top: -15px;" > 
                                <script>
                                    // Themes begin
                                    //am4core.useTheme(am4themes_animated); 
                                    // Themes end
                                    var container = am4core.create("<?php echo "chartdiv".$i; ?>", am4core.Container); 
                                    container.width = am4core.percent(100); 
                                    container.height = am4core.percent(200); 
                                    container.layout = "vertical"; 
                                    container.hideCredits = true; 

                                    /* Create chart instance */
                                    var chart = container.createChild(am4charts.XYChart); 
                                    chart.paddingRight = 10; 

                                    /* Add data */
                                    chart.data = [{ 
                                        "category": "Evaluation", 
                                        "value": <?php echo $pzasProdH[$i]; ?>, 
                                        "Meta": <?php echo $pzasTargetH[$i]; ?> 
                                    }]; 

                                    /* Create axes */
                                    var categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis()); 
                                    categoryAxis.renderer.labels.template.disabled = true; 
                                    categoryAxis.dataFields.category = "category"; 
                                    categoryAxis.renderer.minGridDistance = 10; 
                                    categoryAxis.renderer.grid.template.disabled = true; 

                                    var valueAxis = chart.xAxes.push(new am4charts.ValueAxis()); 
                                    valueAxis.renderer.minGridDistance = 10; 
                                    valueAxis.renderer.labels.template.disabled = true; 
                                    valueAxis.renderer.grid.template.disabled = true; 
                                    valueAxis.min = 0; 
                                    valueAxis.strictMinMax = true; 
                                    valueAxis.renderer.labels.template.adapter.add("text", function(text) { 
                                        return text ; 
                                    }); 

                                    /* Create series */
                                    var series = chart.series.push(new am4charts.ColumnSeries()); 
                                    series.dataFields.valueX = "value"; 
                                    series.dataFields.categoryY = "category"; 
                                    series.columns.template.fill = am4core.color("#23B14D"); 
                                    series.columns.template.stroke = am4core.color("#334EFF"); 
                                    series.columns.template.strokeWidth = .1; 
                                    series.columns.template.strokeOpacity = 0.5; 
                                    series.columns.template.height = am4core.percent(100); 
                                    series.columns.template.tooltipText = "[bold]Real: [/] {value}pzas [bold]Meta: [/]{target}pzas"; 

                                    var series2 = chart.series.push(new am4charts.LineSeries()); 
                                    series2.dataFields.valueX = "Meta"; 
                                    series2.dataFields.categoryY = "category"; 
                                    series2.strokeWidth = 4; 

                                    var bullet = series2.bullets.push(new am4charts.Bullet()); 
                                    var line = bullet.createChild(am4core.Line); 
                                    line.x1 = 0; 
                                    line.y1 = -200;
                                    line.x2 = 0;
                                    line.y2 = 200;
                                    line.stroke = am4core.color("#ff0f00");
                                    line.strokeWidth = 4; 
                                </script> 
                            </div> 
                        </td>                         
                        <td style="width: 6.8vw" > <?php echo $typeH[$i]; ?> </td> 
                        <td style="width: 2.4vw" > <?php echo $pzasScrapH[$i]; ?> </td> 
                        <td style="width: 3.1vw" > <?php echo $reworkH[$i]; ?> </td> 
                        <td style="width: 2.9vw" > <?php echo $durCambioH[$i]; ?> </td> 
                        <td style="width: 3.2vw" > <?php echo $durTecnicasH[$i]; ?> </td> 
                        <td style="width: 4.0vw" > <?php echo $durOrgH[$i]; ?> </td> 
                        <td style="width: 3.3vw" > <?php echo $durDesempenioH[$i]; ?> </td> 
                        <td style="width: 2.3vw" > <?php echo $codeHX[$i]; ?> </td> 
                        <td style="width: 16.4vw" > <?php echo $descriptionHX[$i]; ?> </td> 
                        <td style="width: 2.3vw" > <?php echo $codeHY[$i]; ?> </td> 
                        <td style="width: 16.3vw" > <?php echo $descriptionHY[$i]; ?> </td> 
                        <td style="width: 3.3vw" > <?php echo $periodOEEH[$i].'%'; ?> </td> 
                        <td style="width: 3.1vw" > <?php echo $OEEAcumH[$i].'%';  ?> </td> 
                    </tr> 
                    <?php } ?> 
                </tbody>
            </table>
        </div>
    </body>
</html>

