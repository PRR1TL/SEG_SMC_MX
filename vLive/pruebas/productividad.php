<HTML>
    <LINK REL=StyleSheet HREF="../css/estiloProductividad.css" TYPE="text/css" MEDIA=screen>
    <title>Pareto Productividad</title>
    <link href="../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        
        <?php
            require_once '../db/ServerFunctions.php';
//            $line = $_REQUEST['line'];
//            $month = $_REQUEST['month'];
//            $year = $_REQUEST['year'];
            
            $line = 'L003';
            $month = 4;
            $year = 2019;
//            $varMesStr = listarMeses();
            
            //$mes = (int) $month();
            /******************** NUMERO DE SEMANAS *****************************/
            # Obtenemos el ultimo dia del mes
            $ultimoDiaMes=date("t",mktime(0,0,0,$month,1,$year)); 
            
            //DIAS DE MESES (PARA PRODUCTIVIDAD ANUAL)
            for($i = 1; $i < 13; $i++){
                $lastDayMonth[$i] = date("t",mktime(0,0,0,$i,1,$year));
                //echo 'm ',$i,': ',$lastDayMonth[$i],'<br>';
            }
            //MES
            $dateI = $year.'-'.$month.'-1';
            $dateF = $year.'-'.$month.'-'.$ultimoDiaMes;
            //echo $ultimoDiaMes,' , ', $dateF, '<br>'; 
            $firstDayYear = $year.'-01-01';
            $lastDayYear = $year.'-12-31';

            $ultimoDiaMes=date("t",mktime(0,0,0,$month,1,$year));
            # Obtenemos la semana del primer dia del mes
            $primeraSemana= (int) date("W",mktime(0,0,0,$month,1,$year));
                //echo $primeraSemana;
                # Obtenemos la semana del ultimo dia del mes
            $ultimaSemana= (int) date("W",mktime(0,0,0,$month,$ultimoDiaMes,$year));
                        //ECHO $ultimaSemana;
            if ($ultimaSemana < $primeraSemana) {
                $numSemanas = ((52 + $ultimaSemana) - $primeraSemana) + 1;
                    //echo $numSemanas;
            } else {
                $numSemanas = ($ultimaSemana - $primeraSemana) + 1;     
                    //echo $numSemanas;
            }

            $x = 0;
                //echo "i:",$primeraSemana," f:",$ultimaSemana," n:",$numSemanas,'<br>';
            for($i = 0; $i < $numSemanas; $i++ ){
                if($primeraSemana < $ultimaSemana){
                    $sem[$i] = $primeraSemana + $i;
                }else {
                    if($ultimaSemana < 52){
                        $sem[$i] = $primeraSemana + $i;
                        if($sem[$i] > 52){
                            $sem[$i] = $x + 1;
                            $x++;
                        }
                    }
                }
            }   
            /*******************************************************************/
            
            //ARRAYS
            $productivityPzasDay = productividadPzasDia($line, $year, $month, $ultimoDiaMes);
            $datPzasPSemana = productividadPzasSemana($line, $year, $month);
            $productivityPzasMonth = productividadPzasMensual($line, $year);
            
            //TARGETS
            $targetProductivityDay = targetProductitividadDia($line, $year, $month, $ultimoDiaMes);
            $targetProductivityWeek = targetProductividadSemana($line, $year, $month);
            $targetProductivityMonth = targetProductividadMes($line, $year);

            // VARIABLES            
            $day;
            $pzasDay;
            $minDay;
            $hrsDay; 
            $date;
            $hours;
            $mens;
            $turn;
            $hrsMens;
            $productivityDay;
            $targetDay;
            $productivityDayDown;
            $productivityDayUp;
            
            //SEMANA
            $pzasSemana;
            $minSemana;
            $mensSemana;
            $hoursSemana;
            $hrsMensSemana;
            $turnsSemana;
            $productividadSemana;
            $targetWeek;
            $productivityWeekDown;
            $productivityWeekUp;
            
            //MES
            $mt;
            $month;
            $pzasMonth;
            $minMonth;
            $hoursMonth;
            $mensMonth;            
            $hrsMensMonth;
            $turnsMonth;
            $productivityMonth;
            $targetMonth;
            $targetPromMonth;
            $productivityMonthDown;
            $productivityMonthUp;
            
            /******************************************************************/
            //APARTAMOS LOS ESPACIOS PARA EL TOTAL DE DIAS DEL MES            
            for($i = 1; $i <= $ultimoDiaMes; $i++){
                $productivityDay[$i] = 0;
                $day[$i] = $i;
                $pzasDay[$i] = 0;
                $minDay[$i] = 0;
                $date[$i] = (string) $year.'-'.$month.'-'.$i;
                $mens[$i] = 0;
                $hours[$i] = 0;
                $hrsMens[$i] = 0;
                $hrsDay[$i] = 0; 
                $turn[$i] = 0;  
                $targetDay[$i] = 0;
                $productivityDayDown[$i] = 0;
                $productivityDayUp[$i] = 0;
            }
            
            //APARTAMOS LOS ESPACIOS PARA LAS SEMANAS
            for($i = 0; $i <= $numSemanas; $i++ ){
                $semana[$i] = $i;
                $pzasSemana[$i] = 0;
                $minSemana[$i] = 0;
                $hoursSemana[$i] = 0;
                $mensSemana[$i] = 0;
                $hrsMensSemana[$i] = 0;
                $turnsSemana[$i] = 0;
                $productividadSemana[$i] = 0;
                $targetWeek[$i] = 0;
            }
            
            //APARTAMOS ESPACIO PARA MESES
            for($i = 1; $i <= 12; $i++){
                $mt = $i;
                $monthProd[$i] = $i;
                $pzasMonth[$i] = 0;
                $minMonth[$i] = 0;
                $hoursMonth[$i] = 0;
                $mensMonth[$i] = 0;
                $hrsMensMonth[$i] = 0;
                $turnsMonth[$i] = 0;
                $productivityMonth[$i] = 0;
                $targetMonth[$i] = 0;
                $targetPromMonth[$i] = 0;
            }
            /******************************************************************/
            
            /********************** PRODUCTIVIDAD DIARIA ******************/
            // PZAS PARA PRODUCTIVIDAD 
            for($i = 0; $i < count($productivityPzasDay); $i++){
                $d = $productivityPzasDay[$i][0];
                $day[$d] = $productivityPzasDay[$i][0];
                $pzasDay[$d] = $productivityPzasDay[$i][1];
                $minDay[$d] = $productivityPzasDay[$i][2];
                $hours[$d] = $minDay[$d]/60;
                $date[$d] = date_format($productivityPzasDay[$i][3], 'Y-m-d');   
                
                /*************** HOMBRE POR TURNO ********************/
                $datHombresPDia = productividadHombresDia($line, $date[$d]); 
                for($j = 0; $j < count($datHombresPDia); $j++){
                    $mens[$d] = $datHombresPDia[$j][0];
                    if(!$mens[$d] > 0 ){
                        $mens[$d] = 0;
                    }
                    $turn[$d] = $datHombresPDia[$j][1];
                }
                /*******************************************************************/
                $hrsDay[$d] =  @$minDay[$d]/60;
                if($mens[$d] && $hrsDay[$d] && $turn[$d] > 0){
                    $hrsMens[$d] = @($mens[$d]*$hrsDay[$d])/$turn[$d];
                    $productivityDay[$d] = round( $pzasDay[$d]/@($hrsMens[$d]), 3, PHP_ROUND_HALF_ODD);
                }                                 
            }  
           
            //for($i = 1; $i <= $ultimoDiaMes; $i++ ){
                //echo $day[$i],' | pzas:',$pzasDay[$i],' | min:',$minDay[$i],' | horas:',$hours[$i],' | hom:',$mens[$i],' | tur:',$turn[$i],' | prod: ',$productivityDay[$i],'<br>';
            //}       
            /*******************************************************************/            
            
            /**************** PRODUCTIVIDAD SEMANAL ****************************/
            $id = date("W",mktime(0,0,0,$month,01,$year));
            $datHombresPSemana = productividadHombresSemana($line, $dateI, $dateF);
            for($i = 0; $i < count($datPzasPSemana); $i++){
               $s = $datPzasPSemana[$i][0];
               //echo $s,' a ', $id,'<br>';
               if($s == $id){
                    $semana[$i] = $datPzasPSemana[$i][0];
                    $pzasSemana[$i] = $datPzasPSemana[$i][1];
                    $minSemana[$i] = $datPzasPSemana[$i][2];  
                    $hoursSemana[$i] = $minSemana[$i]/60;
                    
                    //echo $minSemana[$i],', ',$hoursSemana[$i],': ',$pzasSemana[$i];
                    /****************** HORAS HOMBRE *******************/
                    for($j = 0; $j < count($datHombresPDia); $j++){
                        $mensSemana[$i] = $datHombresPSemana [$j][1];
                        $turnsSemana[$i] = $datHombresPSemana[$j][2];
                    }
                    $hrsMensSemana[$i] = @($mensSemana[$i]*$hoursSemana[$i])/$turnsSemana[$i];
                    $productividadSemana[$i] = @round($pzasSemana[$i]/@($hrsMensSemana[$i]),2,PHP_ROUND_HALF_DOWN);
                    
                    //echo 'sem ',$semana[$i],' | pzas:',$pzasSemana[$i],' | min:',$minSemana[$i],' | hrs:',$hoursSemana[$i],' | hom:',$mensSemana[$i],' | tur:',$turnsSemana[$i],' | prod:',$productividadSemana[$i],'<br>';
                    
                }else{
                    $dif = (int) ($s - $id) + $i;
                    
                    $semana[$dif] = $datPzasPSemana[$i][0];
                    $pzasSemana[$dif] = $datPzasPSemana[$i][1];
                    $minSemana[$dif] = $datPzasPSemana[$i][2];  
                    $hoursSemana[$dif] = $minSemana[$i]/60;
                    /****************** HORAS HOMBRE *******************/
                    for($j = 0; $j < count($datHombresPDia); $j++){
                        $mensSemana[$dif] = $datHombresPSemana [$j][1];
                        $turnsSemana[$dif] = $datHombresPSemana[$j][2];
                    }
                    $hrsMensSemana[$dif] = @($mensSemana[$dif]*$hoursSemana[$dif])/$turnsSemana[$dif];
                    $productividadSemana[$dif] = @round($pzasSemana[$dif]/@($hrsMensSemana[$dif]),2,PHP_ROUND_HALF_DOWN);
                }
                $id++;              
            }            
//            for($i = $primeraSemana; $i <= $ultimaSemana; $i++){
//                echo 'sem ',$semana[$i],' | pzas:',$pzasSemana[$i],' | min:',$minSemana[$i],' | hrs:',$hoursSemana[$i],' | hom:',$mensSemana[$i],' | tur:',$turnsSemana[$i],' | prod:',$productividadSemana[$i],'<br>';
//            }
            
            /****************** PRODUCTIVIDAD POR MES **************************/
            $productivityTurnsMonth = productividadTurnoMensual($line, $firstDayYear, $lastDayYear);
            for($i = 0; $i < count($productivityPzasMonth); $i++){
                $m = $productivityPzasMonth[$i][0];
                $monthProd[$m] = $productivityPzasMonth[$i][0];
                $pzasMonth[$m] = $productivityPzasMonth[$i][1];
                $minMonth[$m] = $productivityPzasMonth[$i] [2];
                $hoursMonth[$m] = $minMonth[$m]/60;
            }
            
            /******************* CONSULTA DE TIEMPO TURNO ******************/ 
            for($i = 0; $i< count($productivityTurnsMonth); $i++){
                $m = $productivityTurnsMonth[$i][0];
                $mensMonth[$m] = $productivityTurnsMonth[$i][1];
                $turnsMonth[$m] = $productivityTurnsMonth[$i][2];
            }
            
            /***************************************************************/
            for ($i = 1; $i < 13; $i++){
                if($mensMonth[$i] && $hoursMonth[$i] && $turnsMonth[$i] >0){
                    $hrsMensMonth[$i] = @(@$mensMonth[$i]*@$hoursMonth[$i])/@$turnsMonth[$i];
                    $productivityMonth[$i] = @round($pzasMonth[$i]/@($hrsMensMonth[$i]),2,PHP_ROUND_HALF_DOWN);
                }
                //echo $mensMonth[$i],',',$hoursMonth[$i],',',$turnsMonth[$i],'<br>';
                
            }
            
            //for($i = 1; $i<=12; $i++){
                //echo 'month:',$monthProd[$i],' | pzas:',$pzasMonth[$i],' | hrs:',$hoursMonth[$i],' | hom:',$mensMonth[$i],' | tur:',$turnsMonth[$i],' | pro:',$productivityMonth[$i],'<br>';
            //}
            /*******************************************************************/
            
            /************************ TARGETS **********************************/
            //DIA
            for ($i = 0; $i < count($targetProductivityDay); $i++){
                $d = $targetProductivityDay[$i][0];
                $targetDay[$d] = $targetProductivityDay[$i][1];
            }
            
            //SEMANA
            $id = date("W",mktime(0,0,0,$month,01,$year));

            for($i = 0; $i < count($targetProductivityWeek); $i++){
                $w = $targetProductivityWeek[$i][0];
                if ($w = $id){
                    $targetWeek[$i] = $targetProductivityWeek[$i][1];
                }else{
                    $dif = (int) ($s - $id)+$i;
                    $targetWeek[$dif] = $targetProductivityWeek[$i][1];
                }
                //echo $targetWeek[$w],'<br>';
            }
            
            //MES
            for($i = 0; $i < count($targetProductivityMonth); $i++){
                $m = $targetProductivityMonth [$i][0];
                $targetMonth[$m] = $targetProductivityMonth[$i][1];
                //echo $targetMonth[$m],'<br>'; 
            }            
            /******************************************************************/                        
            
            //**********COMPARACION DE LAS PRODUCTIVIDAD CONTRA TARGET *********/            
            for($i = 1; $i < $ultimoDiaMes; $i++){   
                //echo $i,': ', $productivityDay[$i],'<br>';
                if( $targetDay[$i] > $productivityDay[$i] ){                 
                    $productivityDayDown[$i] = $productivityDay[$i];
                }else if($targetDay[$i] < $productivityDay[$i]){
                    $productivityDayUp[$i] = $productivityDay[$i];
                }                
            }            
            /*******************************************************************/
        ?>
    
<BODY>
    <head>
        <a align=center id="headerFixed" class="contenedor">   
            <div class='fila0'>                
            </div>             
            <h5 class="tituloPareto">
                <?php // echo 'Productividad<br>','Linea:&nbsp'.$line.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp Mes: '. $varMesStr[$month - 1]; ?>
            </h5>            
            <div class="fila1">
                <img src="../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
                <form action="../index.php" method="POST">
                    <?php
//                        echo "<input type="."\"hidden\" name="."\"contEntrada\""."value=".'1'.">";
//                        echo "<input type="."\"hidden\" name="."\"line\""."value=".$line.">";
//                        echo "<input type="."\"hidden\" name="."\"month\""."value=".$month.">";
//                        echo "<input type="."\"hidden\" name="."\"year\""."value=".$year.">";
                    ?>
                    <button class="btn btn-primary active btn-sm btnRegresarPareto"
                            onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" onmouseout="this.style.background='#02538B'">
                        Regresar
                    </button> 
                </form>                 
            </div>                
        </a> 
    </head>
    
    <div id ="graficas">
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>

        <div id = "graficasSuperiores">
            <div aling = "center" id="productividadMensual" class="grafMonths">
                <script>
                    chartCPU = Highcharts.chart('productividadMensual', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Productividad Mensual'
                    },
                    xAxis: {
                        type: 'category',
                        gridLineWidth: 1,
                        gridLineColor:'#000000',
                        categories:  ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
                        labels: {
                            style: {
                                fontSize: '10px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        },
                        title: {
                            text: 'Mes'
                        }
                    },
                    yAxis: {
                        gridLineWidth: 1,
                        gridLineColor:'#000000',
                        tickInterval: 2,
                        title: {
                            fontSize: '7px',
                            text: 'Productividad'
                        }
                    },
                    tooltip: {
                        pointFormat: 'Productividad: <b>{point.y:.1f} </b>'
                    },
                    series: [{
                        name: 'Productividad',
                        color: '#1A06AF',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i < 13; $i++){
                            ?>
                                data.push([<?php echo $productivityMonth[$i];?>]);
                            <?php } ?>
                            return data;
                        })()                
                    }, { //LINEA META
                        color: '#2ECC71',
                        type: 'spline',
                        name: 'Meta',
                         data: (function() {
                                var data = [];
                                <?php
                                    for($i = 1; $i < 13; $i++){
                                ?>
                                data.push([<?php echo $targetMonth[$i];?>]);
                                <?php } ?>
                                return data;
                            })()                  
                    }],
                    credits: {
                            enabled: false
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    } 
                });
                </script> 
            </div>

            <div aling = "center" id="productividadSemanal" class="grafWeek">
                <script>
                    chartCPU = Highcharts.chart('productividadSemanal', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Productividad Semanal'
                    },
                    xAxis: {
                        type: 'category',
                        gridLineWidth: 1,
                        gridLineColor:'#000000',
                        categories: (function() {
                                var data = [];
                                <?php
                                    for($i = 0; $i < $numSemanas; $i++ ){
                                ?>
                                data.push([<?php echo $sem[$i];?>]);
                                <?php } ?>
                            return data;
                        })(),
                        labels: {
                            style: {
                                fontSize: '10px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        },
                        title: {
                            text: 'Semana'
                        }
                    },
                    yAxis: {
                        tickInterval: 2,
                        gridLineWidth: 1,                        
                        gridLineColor:'#000000',
                        title: {
                            text: 'Productividad'
                        }
                    },
                    tooltip: {
                        pointFormat: 'Productividad: <b>{point.y:.1f} </b>'
                    },
                    series: [{
                        name: 'Productividad',
                        color: '#1A06AF',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 0; $i < $numSemanas; $i++){
                            ?>
                                data.push([<?php echo $productividadSemana[$i];?>]);
                            <?php } ?>
                            return data;
                        })()                
                    }, { //LINEA META
                        color: '#2ECC71',
                        type: 'spline',
                        name: 'Meta',
                         data: (function() {
                                var data = [];
                                <?php
                                    for($i = 0; $i < $numSemanas; $i++){
                                ?>
                                data.push([<?php echo $targetWeek[$i];?>]);
                                <?php } ?>
                                return data;
                            })()                  
                    }],
                    credits: {
                            enabled: false
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }                
                });
                </script> 
            </div>
        </div>

        <!--GRAFICA DIARIA DE PRODUCTIVIDAD-->
        <div id="productividadDiaria" class="grafDays">    
            <script>
                chartCPU = Highcharts.chart('productividadDiaria', {
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Productividad Diaria'
                    },
                    xAxis: {
                        //PARA NUMERACION EN LA PARTE X DE LA GRAFICA
                        type: 'category',
                        gridLineWidth: 1,
                        gridLineColor:'#000000',                        
                        categories: (function() {
                                var data = [];
                                <?php
                                    for($i = 1; $i <= $ultimoDiaMes; $i++){
                                ?>
                                data.push([<?php echo $i;?>]);
                                <?php } ?>
                            return data;
                        })(),
                        labels: {
                            style: {
                                fontSize: '10px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        },
                        title: {
                            text: 'Día'
                        }
                    },
                    yAxis: {
                        gridLineWidth: 1,
                        tickInterval: 2,
                        gridLineColor:'#000000',
                        title: {
                            text: 'Productividad'
                        }
                    },
                    tooltip: {
                        pointFormat: 'Productividad: <b>{point.y:.1f} </b>'
                    },
                    series: [{
                        //MANDAMOS LOS DATOS A LA GRAFICA
                        name: 'Productividad-Debajo',
                        color: '#FA5858',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i <= $ultimoDiaMes; $i++){
                            ?>
                                data.push([<?php echo $productivityDayDown[$i];?>]);
                            <?php } ?>
                            return data;
                        })()                
                    },{
                        //MANDAMOS LOS DATOS A LA GRAFICA
                        name: 'Productividad-Arriba',
                        color: '#1A06AF',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i <= $ultimoDiaMes; $i++){
                            ?>
                                data.push([<?php echo $productivityDayUp[$i];?>]);
                            <?php } ?>
                            return data;
                        })()                
                    }, { //LINEA META
                        color: '#2ECC71',
                        type: 'spline',
                        name: 'Meta',
                         data: (function() {
                                var data = [];
                                <?php
                                    for($i = 1; $i <= $ultimoDiaMes; $i++){
                                ?>
                                data.push([<?php echo $targetDay[$i];?>]);
                                <?php } ?>
                                return data;
                            })()                  
                    }],
                    credits: {
                            enabled: false
                    },
                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 500
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'horizontal',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }                
                });
            </script>
        </div>
        
        <div style="margin-top: 50%">
            <div style="margin-left: 140px; width: 82%">
                <table cellspacing="0" class="table-bordered table-hover table-responsive ">
                    <thead>
                        <tr style="background: #F2F2F2">
                            <th class="fixed" style="left: 30px; background: #F2F2F2; width: 109px; ">DÍA</th>
                            <?php
                                for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                            ?>
                                <th><?php echo $i ?></th>
                            <?php 
                                }
                            ?>   
                        </tr>
                    </thead>      
                    <tbody>
                        <tr>
                            <th class="fixed" style="background: #F2F2F2; left: 30px">Productividad</th>
                            <?php
                                for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                            ?>
                                <td> <?php echo $productivityDay[$i] ?> </td>
                           <?php } ?>    
                        </tr>                        
                    </tbody>
                </table>
            </div>
            <br>
        </div>
    </div>
</BODY>
    
</HTML>