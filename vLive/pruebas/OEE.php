<?php
    require_once '../db/ServerFunctions.php';
    
    $line = 'L002';
    $month = '03';
    $year = '2019';
    
//    $line = $_REQUEST['line'];
//    $month = $_REQUEST['month'];
//    $year = $_REQUEST['year'];
//    $varMesStr = listarMeses();
    
    $primerDiaMes = 01;
    $ultimoDiaMes=date("t",mktime(0,0,0,$month,1,$year));
    $primeraSemana= (int) date("W",mktime(0,0,0,$month,01,$year));
    //echo $primeraSemana;
    $ultimaSemana= (int) date("W",mktime(0,0,0,$month,$ultimoDiaMes,$year));

    if ($ultimaSemana < $primeraSemana) {
        $numSemanas = ((52 + $ultimaSemana) - $primeraSemana) + 1;
            //echo $numSemanas;
    } else {
        $numSemanas = ($ultimaSemana - $primeraSemana) + 1;     
            //echo $numSemanas;
    }

    $x = 0;
        //echo "i:",$primeraSemana," f:",$ultimaSemana," n:",$numSemanas,'<br>';
    for($i = 0; $i < $numSemanas; $i++ ){
        if($primeraSemana < $ultimaSemana){
            $sem[$i] = $primeraSemana + $i;
        } else {
            if($ultimaSemana < 52){
                $sem[$i] = $primeraSemana + $i;
                if($sem[$i] > 52){
                    $sem[$i] = $x + 1;
                    $x++;
                }
            }
        }
    }    
    $datOEEDay = percentOEEMonthDay($line, $year, $month);    
    
    //LINE TACK
    
    //APARTADO PARA INICIAR LAS VARIABLES EN CERO     
    for ($i = 0; $i <= $ultimoDiaMes; $i++){
        $pProdDay[$i] = 0;
        $pTecDay[$i] = 0;
        $pOrgDay[$i] = 0;
        $pPlanDay[$i] = 0;
        $pCaliDay[$i] = 0;
        $pCamDay[$i] = 0;
        $pTFalDay[$i] = 0;
        $targetDia[$i] = 0;
    }
    
    //APARTADO DE CONSULTAS DE BD
    //DAY    
    for ($i = 0; $i < count($datOEEDay); $i++  ){        
        $d = $datOEEDay[$i][0];        
        $pProdDay[$d] = @round($datOEEDay[$i][1],2);
        $pTecDay[$d] = @round($datOEEDay[$i][2],2);
        $pOrgDay[$d] = @round($datOEEDay[$i][3],2);
        $pCaliDay[$d] = @round($datOEEDay[$i][4],2);
        $pCamDay[$d] = @round($datOEEDay[$i][5],2);
        //$pPlanDay[$d] = $datOEEDay[$i][5];        
        $pTFalDay[$d] = @round($datOEEDay[$i][6],2);
    }    
    
    //TARGET
    $targetDiasOEE = targetDiaOEE($line, $year, $month);
    for($i = 0; $i < count($targetDiasOEE); $i++){
        $d = $targetDiasOEE[$i][0];
        $targetDia[$d] = $targetDiasOEE[$i][1];
    }
    
    //APARTADOS PARA MES
    //$minTotalesM = $datMinRepMes[0][0] - $datMinDescMes[0][0];
    for ($i = 1; $i < 13; $i++ ){
        //APARTADO PARA POCENTAJES
        $percentProdM[$i] = 0;
        $percentResM[$i] = 0;
        $percentTecM[$i] = 0;
        $percentOrgM[$i] = 0;
        $percentCaliM[$i] = 0;
        $percentCamM[$i] = 0;
        $percentTFalM[$i] = 0;
        //TARGET
        $targetMes[$i] = 0;
    }
        
    //ARREGLOS PARA TIEMPOS MES/AREA
    
    $cPercentTopicMonthly = cPercentTopicMonthly($line, $year);
    for ($i = 0; $i < count($cPercentTopicMonthly); $i++){
        $m = $cPercentTopicMonthly[$i][0];        
        $percentProdM[$m] = @round($cPercentTopicMonthly[$i][1],2);
        $percentTecM[$m] = @round($cPercentTopicMonthly[$i][2],2);
        $percentOrgM[$m] = @round($cPercentTopicMonthly[$i][3],2);
        $percentCaliM[$m] = @round($cPercentTopicMonthly[$i][4],2);
        $percentCamM[$m] = @round($cPercentTopicMonthly[$i][5],2);
        $percentTFalM[$m] = @round($cPercentTopicMonthly[$i][6],2);
    }
    
    if ($line == 'L001' && $year == '2019') {
        $percentProdM[1] = @round(77.8,2);
        $percentTecM[1] = @round(9.4,2);
        $percentOrgM[1] = @round(5.6,2);
        $percentCaliM[1] = @round(0,2);
        $percentCamM[1] = @round(5.2,2);
        $percentTFalM[1] = @round(0,2);
    } else if ($line == 'L002' && $year == '2019' ) {
        $percentProdM[1] = @round(84,2);
        $percentTecM[1] = @round(11,2);
        $percentOrgM[1] = @round(3,2);
        $percentCaliM[1] = @round(0,2);
        $percentCamM[1] = @round(3,2);
        $percentTFalM[1] = @round(0,2);
    } else if ($line == 'L003' && $year == '2019') {
        $percentProdM[1] = @round(83,2);
        $percentTecM[1] = @round(7,2);
        $percentOrgM[1] = @round(2,2);
        $percentCaliM[1] = @round(0,2);
        $percentCamM[1] = @round(8,2);
        $percentTFalM[1] = @round(0,2);
    }
     
    //TARGET MES
    $targetMesOEE = targetMesOEE($line, $year);
    for ($i = 0; $i < count($targetMesOEE); $i++){
        $m = $targetMesOEE[$i][0];
        $targetMes[$m] = $targetMesOEE[$i][1];
       // ECHO $targetMes[$m],'<br>';
    }
    
    //APARTADO PARA SEMANA 
    //APARTADO PARA INICIAR LOS ESPACIOS Y VALORES DE SEMANA
    for($i = 0; $i < $numSemanas; $i++){
        $pzasProdW[$i] = 0;
        $pzasEspW[$i] = 0; 
        
        $minRepW[$i] = 0;
        $minDescW[$i] = 0;        
        $minTPerdW[$i] = 0;  
        //MINUTOS POR AREA
        $minTecW[$i] = 0;
        $minOrgW[$i] = 0;
        $minCamW[$i] = 0;        
        $minCaliW[$i] = 0;        
        $minTFalW[$i] = 0; 
        //MINUTOS PAROS POR MES 
        $minTotalParosW[$i] = 0;
        //APARTADO PARA POCENTAJES
        $percentProdW[$i] = 0;
        $percentResW[$i] = 0;
        $percentTecW[$i] = 0;
        $percentOrgW[$i] = 0;
        $percentCaliW[$i] = 0;
        $percentCamW[$i] = 0;
        $percentTFalW[$i] = 0;
        //TARGET
        $targetWeek[$i] = 0;
    }
    
    //MINUTOS / AREA
    $datMinTecW = minTecWeek($line, $year, $month); 
    $id = date("W",mktime(0,0,0,$month,01,$year));
    for ($i = 0; $i < count($datMinTecW); $i++){
        $s = $datMinTecW[$i][0];
        if($s == $id){
            $minTecW[$i] = $datMinTecW[$i][1];                
        } else {
            $dif = (int) ($s - $id) + $i;
            $minTecW[$dif] = $datMinTecW[$i][1];
        }
        $id++;
    }
    
    $datMinOrgW = minOrgWeek($line, $year, $month); 
    $id = date("W",mktime(0,0,0,$month,01,$year));
    for ($i = 0; $i < count($datMinOrgW); $i++){
        $s = $datMinOrgW[$i][0];
        if($s == $id){
            $minOrgW[$i] = $datMinOrgW[$i][1];                
        } else {
            $dif = (int) ($s - $id) + $i;
            $minOrgW[$dif] = $datMinOrgW[$i][1];
        }
        $id++;
    }
    
    $datMinCaliW = minCalWeek($line, $year, $month); 
    $id = date("W",mktime(0,0,0,$month,01,$year));
    for ($i = 0; $i < count($datMinCaliW); $i++){
        $s = $datMinCaliW[$i][0];
        if($s == $id){
            $minCaliW[$i] = $datMinCaliW[$i][1];                
        } else {
            $dif = (int) ($s - $id) + $i;
            $minCaliW[$dif] = $datMinCaliW[$i][1];
        }
        $id++;
    }
    
    $datMinCamW = minCamWeek($line, $year, $month); 
    $id = date("W",mktime(0,0,0,$month,01,$year));
    for ($i = 0; $i < count($datMinCamW); $i++){
        $s = $datMinCamW[$i][0];
        if($s == $id){
            $minCamW[$i] = $datMinCamW[$i][1];                
        } else {
            $dif = (int) ($s - $id) + $i;
            $minCamW[$dif] = $datMinCamW[$i][1];
        }
        $id++;
    }
    
    $datMinTFalW = minTFalWeek($line, $year, $month); 
    $id = date("W",mktime(0,0,0,$month,01,$year));
    for ($i = 0; $i < count($datMinTFalW); $i++){
        $s = $datMinTFalW[$i][0];
        if($s == $id){
            $minTFalW[$i] = $datMinTFalW[$i][1];                
        } else {
            $dif = (int) ($s - $id) + $i;
            $minTFalW[$dif] = $datMinTFalW[$i][1];
        }
        $id++;
    }    
    
    //JUNTAMOS TODOS LOS MINUTOS DE TODAS LAS AREAS (TOTALES) POR SEMANA
    for($i = 0; $i < $numSemanas; $i++){
        $minTotalParosW[$i] = $minCaliW[$i]+$minOrgW[$i]+$minTecW[$i]+$minCamW[$i]+$minTFalW[$i];
    }
    
    //DATOS SEMANALES
    //PIEZAS PRODUCIDAS
    $datPzasProdW = pzasRealesWeek($line, $year, $month); 
    $id = date("W",mktime(0,0,0,$month,01,$year));
    for ($i = 0; $i < count($datPzasProdW); $i++){
        $s = $datPzasProdW[$i][0];
        if($s == $id){
            $pzasProdW[$i] = $datPzasProdW[$i][1];
        } else {
            $dif = (int) ($s - $id) + $i;            
            $pzasProdW[$dif] = $datPzasProdW[$i][1];
        }
        $id++;
    }
    
    //PIEZAS ESPERADAS POR SEMANA
    $datPzasEspProdW = pzasEsperadasWeek($line, $year, $month); 
    $id = date("W",mktime(0,0,0,$month,01,$year));
    for ($i = 0; $i < count($datPzasEspProdW); $i++){
        $s = $datPzasEspProdW[$i][0];
        if($s == $id){   
            $pzasEspW[$i] = $datPzasEspProdW[$i][1];   
            //OBTENEMOS EL % PRODUCCION DE CADA SEMANA 
            
            $percentProdW[$i] =  @round(($pzasProdW[$i]*100)/$pzasEspW[$i],2);
            if(! $percentProdW[$i] > 0 ){
                $percentProdW[$i] = 0;
                $percentOrgW[$i] = 0;
                $percentCaliW[$i] = 0;
                $percentCamW[$i] = 0;
                $percentTecW[$i] = 0;
            } else {
                $percentResW[$i] = 100 - $percentProdW[$i]; 
                
                //PORCENTAJES PARA AREAS
                $percentCaliW[$i] = @round(($minCaliW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
                $percentOrgW[$i] = @round(($minOrgW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
                $percentTecW[$i] = @round(($minTecW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
                $percentCamW[$i] = @round(($minCamW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
                $percentTFalW[$i] = @round(($minTFalW[$i]*$percentResW[$i])/$minTotalParosW[$i],2);
            }
            
            //PORCENTAJES PARA AREAS 
        } else {
            $dif = (int) ($s - $id) + $i;
            
            $pzasEspW[$dif] = $datPzasEspProdW[$i][1];
            //OBTENEMOS EL % PRODUCCION DE CADA SEMANA 
            
            $percentProdW[$dif] =  @(($pzasProdW[$dif]*100)/$pzasEspW[$dif]);
             if(! $percentProdW[$dif] > 0 ){
                $percentProdW[$dif] = 0;
                $percentOrgW[$dif] = 0;
                $percentCaliW[$dif] = 0;
                $percentCamW[$dif] = 0;
                $percentTecW[$dif] = 0;
            } else {
                $percentResW[$dif] = 100 - $percentProdW[$dif];
                
                //PORCENTAJES PARA AREAS
                $percentCaliW[$dif] = @(($minCaliW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
                $percentOrgW[$dif] = @(($minOrgW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
                $percentTecW[$dif] = @(($minTecW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
                $percentCamW[$dif] = @(($minCamW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
                $percentTFalW[$dif] = @(($minTFalW[$dif]*$percentResW[$dif])/$minTotalParosW[$dif]);
            } 
        }
        $id++;
    }   
    
    //for($i = 0; $i < $numSemanas; $i++){
        //echo $pzasEspW[$i],', ',$pzasProdW[$i],' :',$percentProdW[$i],'<br>';
        //echo '%Res',$percentResW[$i],'% ORG ',$percentOrgW[$i],', %CALI ',$percentCaliW[$i],' %CAM ',$percentCamW[$i],' %TEC ',$percentTecW[$i],'<br>';
    //}
    
    
    //TIEMPOS DE PERDIDAS
    
    //PORCENTAJES DE TIEMPOS    
        
    //APARTADO PARA  CALCULOS SEMANALES
    
    
    ?>

<head>
    <LINK REL=StyleSheet HREF="../css/estiloOEE.css" TYPE="text/css" MEDIA=screen>
    <title>Pareto OEE</title>
    <link href="../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    
    <!--DISEÑO CABECERA-->
    <a align=center id="headerFixedOEE" class="contenedor" >
        <div class='fila0'>  
        </div>             
        <h5 class="tituloPareto"> 
            <?php echo 'Seguimiento de OEE<br>','Linea:&nbsp'.$line.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp Mes: '.$varMesStr[$month - 1];?>
        </h5> 
        <div class="fila1">
            <img src="../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
            <form action="losses.php" method="POST">
                <?php
                    echo "<input type="."\"hidden\" name="."\"contEntrada\""."value=".'1'.">";
                    echo "<input type="."\"hidden\" name="."\"line\""."value=".$line.">";
                    echo "<input type="."\"hidden\" name="."\"month\""."value=".$month.">";
                    echo "<input type="."\"hidden\" name="."\"year\""."value=".$year.">";
                ?>                
                <button class="btn btn-success btn-sm btnLosses" onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer' "onmouseout="this.style.background='#008C5B'">
                    Losses
                </button> 
            </form>
            
            <form action="perdidasGeneral.php" method="POST">
                <?php
                    echo "<input type="."\"hidden\" name="."\"contEntrada\""."value=".'1'.">";
                    echo "<input type="."\"hidden\" name="."\"pLine\""."value=".$line.">";
                    echo "<input type="."\"hidden\" name="."\"pMonth\""."value=".$month.">";
                    echo "<input type="."\"hidden\" name="."\"pYear\""."value=".$year.">";
                ?>                
                <button class="btn btn-success btn-sm btnPerdias" onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer' "onmouseout="this.style.background='#008C5B'">
                    P&eacute;rdidas
                </button> 
            </form>

            <form action="../index.php" method="POST">
                <?php
                    echo "<input type="."\"hidden\" name="."\"contEntrada\""."value=".'1'.">";
                    echo "<input type="."\"hidden\" name="."\"line\""."value=".$line.">";
                    echo "<input type="."\"hidden\" name="."\"month\""."value=".$month.">";
                    echo "<input type="."\"hidden\" name="."\"year\""."value=".$year.">";
                ?>
                <button class="btn btn-primary active btn-sm" style="float: right; position: absolute; top: 2.9vh; left: 180px; width: 5.7%" onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" onmouseout="this.style.background='#02538B'">
                    Regresar
                </button> 
            </form>
        </div>            
    </a>    
    
</head>

<body>
    <div id="graficas"> 
        <div id="mes" class="grafMonths">
            <script>
                Highcharts.chart('mes', {
                    chart: {
                      type: 'column'
                    },
                    title: {
                      text: 'OEE con Factores de Pérdidas - Mensual'
                    },
                    xAxis: {
                        title: {
                            text: 'Mes'
                        },      
                        gridLineWidth: 1,
                        gridLineColor: 	'#000000' ,
                        categories:  ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
                    },
                    yAxis: {                
                        tickInterval: 50,
                        gridLineColor: 	'#000000',
                        title: {
                            text: 'Porcentaje'
                        }
                    },
                    tooltip: {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            stacking: 'percent'
                        }
                    },
                    series: [{
                        color: '#9E9E9E',
                        name: 'Desempeño',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i < 13; $i++){
                            ?>
                            data.push([<?php echo $percentTFalM[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#3498db',
                        name: 'Cambios',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i < 13; $i++){
                            ?>
                            data.push([<?php echo $percentCamM[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    },{
                        color: '#311B92',
                        name: 'Tecnicas',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i < 13; $i++){
                            ?>
                            data.push([<?php echo $percentTecM[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#F06292',
                        name: 'Organizacionales',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i < 13; $i++){
                            ?>
                            data.push([<?php echo $percentOrgM[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#B71C1C',
                        name: 'Calidad',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i < 13; $i++){
                            ?>
                            data.push([<?php echo $percentCaliM[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#2ecc71',
                        name: 'Produccion',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i < 13; $i++){
                            ?>
                            data.push([<?php echo $percentProdM[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    },{
                        color: '#2ECC71',
                        type: 'spline',
                        name: 'Meta',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i < 13; $i++){
                            ?>
                            data.push([<?php echo $targetMes[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }],
                    credits: {
                            enabled: false
                    }
                  });           
            </script>    
        </div>

        <div id="semana" class="grafWeek">
            <script>
                Highcharts.chart('semana', {
                    chart: {
                      type: 'column'
                    },
                    title: {
                      text: 'OEE con Factores de Pérdidas - Semanal'
                    },
                    xAxis: {
                        title: {
                            text: 'Semana',
                            visible: false
                        },      
                        gridLineWidth: 1,
                        gridLineColor: 	'#000000' ,
                        categories: (function() {
                            var data = [];
                            <?php
                                for($i = 0; $i < $numSemanas; $i++){
                            ?>
                            data.push([<?php echo $sem[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    },
                    yAxis: {
                        tickInterval: 50,
                        gridLineColor: 	'#000000' ,
                        title: {
                            text: 'Porcentaje'
                        }
                    },
                    tooltip: {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            stacking: 'percent'
                        }
                    },
                    series: [{
                        color: '#9E9E9E',
                        name: 'Desempeño',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 0; $i < $numSemanas; $i++){
                            ?>
//                                                DESEMPENIO
                            data.push([<?php echo $percentTFalW[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#3498db',
                        name: 'Cambios',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 0; $i < $numSemanas; $i++){
                            ?>
                                                //CAMBIOS
                            data.push([<?php echo $percentCamW[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#311B92',
                        name: 'Tecnicas',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 0; $i < $numSemanas; $i++){
                            ?>
                            data.push([<?php echo $percentTecW[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#F06292',
                        name: 'Organizacionales',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 0; $i < $numSemanas; $i++){
                            ?>
                                                //ORG
                            data.push([<?php echo $percentOrgW[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#B71C1C',
                        name: 'Calidad',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 0; $i < $numSemanas; $i++){
                            ?>
                                                //CALIDAD
                            data.push([<?php echo $percentCaliW[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#2ecc71',
                        name: 'Produccion',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 0; $i < $numSemanas; $i++){
                            ?>
                            data.push([<?php echo $percentProdW[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#2ECC71',
                        type: 'spline',
                        name: 'Meta',
                        showInLegend: false,
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 0; $i < $numSemanas ; $i++){
                            ?>
                            data.push([<?php echo $targetWeek[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }],
                    credits: {
                        enabled: false
                    }
                });        
            </script>    
        </div>

        <div id="dia" class="grafDays">
            <script>
                Highcharts.chart('dia', {
                    chart: {
                      type: 'column'
                    },
                    title: {
                      text: 'OEE con Factores de Pérdidas - Diaria'
                    },
                    xAxis: {
                        title: {
                            text: 'Día'
                        },
                        gridLineWidth: 1,
                        gridLineColor: 	'#000000' ,
                        categories: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i <= $ultimoDiaMes; $i++){
                            ?>//
                            data.push([<?php echo $i;?>]);
                            <?php } ?>
                            return data;
                        })()
                    },
                    yAxis: {
                        min: 0,
                        tickInterval: 10,
                        gridLineColor: 	'#000000' ,
                        gridLineWidth: 1,
                        title: {
                            text: 'Porcentaje'
                        }
                    },
                    tooltip: {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
                        shared: true
                    },
                    plotOptions: {
                        column: {
                            stacking: 'percent'
                        }
                    },
                    series: [{
                        color: '#9E9E9E',
                        name: 'Desempeño',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i <= $ultimoDiaMes; $i++){
                            ?>
                            data.push([<?php echo $pTFalDay[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#3498db',
                        name: 'Cambios',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i < $ultimoDiaMes; $i++){
                            ?>
                            data.push([<?php echo $pCamDay[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#311B92',
                        name: 'Tecnicas',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i <= $ultimoDiaMes; $i++){
                            ?>
                            data.push([<?php echo $pTecDay[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#F06292',
                        name: 'Organizacionales',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i <= $ultimoDiaMes; $i++){
                            ?>
                            data.push([<?php echo $pOrgDay[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#B71C1C',
                        name: 'Calidad',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i <= $ultimoDiaMes; $i++){
                            ?>
                            data.push([<?php echo $pCaliDay[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }, {
                        color: '#2ecc71',
                        name: 'Produccion',
                        data: (function() {
                            var data = [];                            
                            <?php
                                for($i = 1; $i <= $ultimoDiaMes; $i++){
                            ?>
                            data.push([<?php echo $pProdDay[$i];?>]);
                            <?php } ?>                                
                            return data;                            
                        })()
                    },{
                        color: '#2ECC71',
                        type: 'spline',
                        name: 'Meta',
                        data: (function() {
                            var data = [];
                            <?php
                                for($i = 1; $i <= $ultimoDiaMes; $i++){
                            ?>
                            data.push([<?php echo $targetDia[$i];?>]);
                            <?php } ?>
                            return data;
                        })()
                    }],
                    credits: {
                            enabled: false
                    }           
                });        
            </script>    
        </div>
        
        <!--TABLA-->
        <div>
            <br>
            <div class="table-wrapper tabla">
                <br>
                <table class="table table-hover">
                    <thead>
                        <tr style="background: #F2F2F2"> 
                            <th width='250' style="font-size:12px;" class="first-col" >DIARIO</th>
                            <?php
                            for ($i = 0; $i < cal_days_in_month(CAL_GREGORIAN, $month, $year); $i++) {
                                echo "<th width='35' style='font-size:13px;'> " . ($i + 1) . "</th>";
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>                    
                        <tr> 
                            <td width='250' style='font-size:12px;' class="first-col">OEE (%)</td>
                            <?php
                            for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                                echo "<td width='38' style='font-size:12px;'>" . $pProdDay[$i] . "</td>";
                            }
                            ?>
                        </tr>
                        <tr> 
                            <td width='250' style='font-size:12px;' class="first-col">NO Calidad (%)</td>
                            <?php
                            for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                                echo "<td width='38' style='font-size:12px;'>" . $pCaliDay[$i] . "</td>";
                            }
                            ?>
                        </tr>
                        <tr> 
                            <td width='250' style='font-size:12px;' class="first-col">Organizacionales (%)</td>
                            <?php
                            for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                                echo "<td width='38' style='font-size:12px;'>". $pOrgDay[$i]. "</td>";
                            }
                            ?>
                        </tr>
                        <tr>  
                            <td width='250' style="font-size:12px;" class="first-col">T&eacute;cnicas (%)</td>
                            <?php
                            for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                                echo "<td width='38' style='font-size:12px;'>" . $pTecDay[$i] . "</td>";
                            }
                            ?>
                        </tr>
                        <tr> 
                            <td width='250' style="font-size:12px;" class="first-col">Cambio de Modelo (%)</td>
                            <?php
                            for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                                echo "<td width='38' style='font-size:12px;'>" .$pCamDay[$i] . "</td>";
                            }
                            ?>
                        </tr>
                        <tr> 
                            <td width='250' style="font-size:12px;" class="first-col" >Desempe&ntilde;o (%)</td>
                            <?php
                            for ($i = 1; $i <= $ultimoDiaMes; $i++) {
                                echo "<td width='38' style='font-size:12px;'>". $pTFalDay[$i] . "</td>";
                            }
                            ?>
                        </tr>
                    </tbody>
                </table> 
            </div>
        </div>
        
    </div>
</body>