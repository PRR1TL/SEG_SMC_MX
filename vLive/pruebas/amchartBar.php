<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>

<style>
    #gTec {
        width : 100%;
        height : 300px;
    }
    
    #chartdiv {
        width : 100%;
        height : 300px;
    }
    
    
</style>

<div id="gTec" >
    <script>
        var dataTec = [{
            "category": 2009,
            "income": 23.5,
            "url":"#",
            "description":"click to drill-down",
            "months": [
                { "category": "1_1", "income": 1,
                    "url":"#",
                    "description":"click to drill-down",
                    "nivel3": [
                        { "datC": 1, "income": 1 }
                    ] 
                },
                { "category": 2, "income": 2 },
                { "category": 3, "income": 1 },
                { "category": 4, "income": 3 },
                { "category": 5, "income": 2.5 },
                { "category": 6, "income": 1.1 },
                { "category": 7, "income": 2.9 },
                { "category": 8, "income": 3.5 },
                { "category": 9, "income": 3.1 },
                { "category": 10, "income": 1.1 },
                { "category": 11, "income": 1 },
                { "category": 12, "income": 3 }
            ]
        }, {
            "category": 2010,
            "income": 26.2,
            "url":"#",
            "description":"click to drill-down",
            "months": [
                { "category": 1, "income": 4 },
                { "category": 2, "income": 3 },
                { "category": 3, "income": 1 },
                { "category": 4, "income": 4 },
                { "category": 5, "income": 2 },
                { "category": 6, "income": 1 },
                { "category": 7, "income": 2 },
                { "category": 8, "income": 2 },
                { "category": 9, "income": 3 },
                { "category": 10, "income": 1 },
                { "category": 11, "income": 1 },
                { "category": 12, "income": 3 }
            ]
        }, {
            "category": 2011,
            "income": 30.1,
            "url":"#",
            "description":"click to drill-down",
            "months": [
                { "category": 1, "income": 2 },
                { "category": 2, "income": 3 },
                { "category": 3, "income": 1 },
                { "category": 4, "income": 5 },
                { "category": 5, "income": 2 },
                { "category": 6, "income": 1 },
                { "category": 7, "income": 2 },
                { "category": 8, "income": 2.5 },
                { "category": 9, "income": 3.1 },
                { "category": 10, "income": 1.1 },
                { "category": 11, "income": 1 },
                { "category": 12, "income": 3 }
            ]
        }];

        var chartTec = AmCharts.makeChart("gTec", {
            "type": "serial",
            "theme": "none",
            "pathToImages": "/lib/3/images/",
            "autoMargins": false,
            "marginLeft": 30,
            "marginRight": 8,
            "marginTop": 10,
            "marginBottom": 26,
            "titles": [{
                "text": "LOSSES: Técnicas"
            }],
            "dataProvider": dataTec,
            "startDuration": 1,
            "graphs": [{
                "alphaField": "alpha",
                "balloonText": "<span style='font-size:13px;'>[[title]] in [[category]]:<b>[[value]]</b> [[additional]]</span> <br>[[description]]",
                "dashLengthField": "dashLengthColumn",
                "fillAlphas": 1,
                "title": "Income",
                "type": "column",
                "valueField": "income","urlField":"url"
            }],
            "categoryField": "category",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "tickLength": 0
            }, "chartScrollbar": {
                "graph": "g2",
                "oppositeAxis": false,
                "offset": 30,
                "scrollbarHeight": 20,
                "backgroundAlpha": 0,
                "selectedBackgroundAlpha": 0.1,
                "selectedBackgroundColor": "#888888",
                "graphFillAlpha": 0,
                "graphLineAlpha": 0.5,
                "selectedGraphFillAlpha": 0,
                "selectedGraphLineAlpha": 1,
                "autoGridCount": true,
                "color": "#AAAAAA"
            },
            "chartCursor": {
                "pan": true,
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 0,
                "valueLineAlpha": 0.2
            }
            
        });

        chartTec.addListener("clickGraphItem", function (event) {
            if ( 'object' === typeof event.item.dataContext.months ) {
                // set the monthly data for the clicked month
                event.chart.dataProvider = event.item.dataContext.months;
                // update the chart title
                event.chart.titles[0].text = event.item.dataContext.category + ' ';
                // let's add a label to go back to yearly data
                event.chart.addLabel(
                    "!10", 25, 
                    "Go back >",
                    "right", 
                    undefined, 
                    undefined, 
                    undefined, 
                    undefined, 
                    true, 
                    'javascript:resetGTec();');

                // validate the new data and make the chart animate again
                event.chart.validateData();
                event.chart.animateAgain();
            }
            
            if ( 'object' === typeof event.item.dataContext.nivel3 ) {

                // set the monthly data for the clicked month 
                event.chart.dataProvider = event.item.dataContext.nivel3; 
                // update the chart title
                event.chart.titles[0].text = event.item.dataContext.category + ' ';

                // let's add a label to go back to yearly data
                event.chart.addLabel(
                    "!10", 25, 
                    "Go back >",
                    "right", 
                    undefined, 
                    undefined, 
                    undefined, 
                    undefined, 
                    true, 
                    'javascript:resetGTec2();');

                // validate the new data and make the chart animate again
                event.chart.validateData();
                event.chart.animateAgain();
            }
            
        });

        // function which resets the chart back to yearly data
        function resetGTec() {
            chartTec.dataProvider = dataTec;
            chartTec.titles[0].text = 'LOSSES: TECNICAS';

            // remove the "Go back" label
            chartTec.allLabels = [];

            chartTec.validateData();
            chartTec.animateAgain();
        }
        
        function resetGTec2() {
            chartTec.dataProvider = dataTec; 
            chartTec.titles[0].text = 'LOSSES: TECNICAS'; 

            // remove the "Go back" label 
            chartTec.allLabels = []; 

            chartTec.validateData(); 
            chartTec.animateAgain(); 
        }
    </script>
</div>





