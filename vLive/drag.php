<!DOCTYPE html>
<head>
    <meta charset="utf-8" > 
    <meta http-equiv="X-UA-Compatible" content="IE=edge" >  
    <meta name="viewport" content="width=device-width, initial-scale=1" > 
    <link href="css/style.css" rel="stylesheet" >     
</head> 

<body class="fix-header card-no-border fix-sidebar" >    
    <div class="grid-stack" data-gs-width="12" data-gs-animate="yes" > 
        <div class="grid-stack-item" data-gs-x="0" data-gs-y="0" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >1</div> 
        </div>                                     
        <div class="grid-stack-item" data-gs-x="3" data-gs-y="0" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content"style="border: 1px solid #000000;" >2</div> 
        </div> 
        <div class="grid-stack-item" data-gs-x="6" data-gs-y="0" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >3</div> 
        </div>                                     
        <div class="grid-stack-item" data-gs-x="9" data-gs-y="0" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >4</div> 
        </div>                                     
        <div class="grid-stack-item" data-gs-x="0" data-gs-y="3" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >5</div> 
        </div>                                     
        <div class="grid-stack-item" data-gs-x="3" data-gs-y="3" data-gs-width="6" data-gs-height="5" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >6</div> 
        </div> 
        <div class="grid-stack-item" data-gs-x="9" data-gs-y="3" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >7</div> 
        </div> 
        <div class="grid-stack-item" data-gs-x="0" data-gs-y="6" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >8</div> 
        </div> 
        <div class="grid-stack-item" data-gs-x="9" data-gs-y="6" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >9</div> 
        </div> 
        <div class="grid-stack-item" data-gs-x="3" data-gs-y="8" data-gs-width="6" data-gs-height="4" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >10</div> 
        </div> 
        <div class="grid-stack-item" data-gs-x="0" data-gs-y="9" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >11</div> 
        </div>                                     
        <div class="grid-stack-item" data-gs-x="9" data-gs-y="9" data-gs-width="3" data-gs-height="3" > 
            <div class="grid-stack-item-content" style="border: 1px solid #000000;" >12<span class="fa fa-hand-o-up"></span> Drag me! </div> 
        </div> 
    </div> 
               
    <script src="js/drag/jquery.min.js"></script> 
    <script src="js/drag/jquery-ui.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="js/drag/lodash.js"></script> 
    <script src="js/drag/gridstack.js"></script> 
    <script src="js/drag/gridstack.jQueryUI.js"></script> 
    <script type="text/javascript"> 
    $(function() { 
        $('.grid-stack').gridstack({ 
            width: 12, 
            alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
            resizable: { 
                handles: 'e, se, s, sw, w' 
            } 
        }); 
    }); 
    </script> 
</body> 

</html>



