<?php
require_once 'ServerConfig.php';
/**
 * Description of ServerFunctions
 *
 * @author GJA5TL
 */

//CONFIGURAMOS PARA QUE LA HORA SEA LA INTERNACION MEXICANA Y NO LA DE ALEMANIA
date_default_timezone_set("America/Mexico_City");

//Devuelve un array multidimensional con el resultado de la consulta
function getArraySQL($sql) {
    $connectionObj = new ServerConfig();
    $connectionStr = $connectionObj -> serverConnection();
    
    if (!$result = sqlsrv_query($connectionStr, $sql)) die("Conexion: Server Functions (Line 48-49)");

    $rawdata = array();
    $i = 0;
    while ($row = sqlsrv_fetch_array($result)) {
        $rawdata[$i] = $row;
        $i++;
    }
    //$connectionObj ->serverDisconnect();
    return $rawdata;
}

//OBTENEMOS LA FECHA POR SEPARADO
function listarLineas() { 
    $sql = "SELECT linea, nombre FROM Lineas ORDER BY 1"; 
    return getArraySQL($sql); 
} 

function listarLineas_Producto($producto) { 
    $sql = "SELECT linea, nombre FROM Lineas WHERE producto = '$producto' ORDER BY 1"; 
    return getArraySQL($sql); 
} 

function listarLineas_CValor($cValor) { 
    $sql = "SELECT linea, nombre FROM Lineas WHERE cValor = '$cValor' ORDER BY 1"; 
    return getArraySQL($sql); 
}

function nombreLinea($linea) { 
    $lineaS = "SELECT l.nombre, c.costCenter, c.profitCenter, l.CValor, l.producto FROM Lineas as l, linea_costCenter as c WHERE l.linea = '$linea' AND c.linea = l.linea "; 
    return getArraySQL($lineaS); 
} 

function listar_SubEnsable_CadVal($cValor) { 
    $lineaS = "SELECT linea, nombre FROM Lineas WHERE cValor = '$cValor' AND linea <> 'L163-CP1' AND linea <> 'L107' AND tipo > 10 ORDER BY tipo "; 
    return getArraySQL($lineaS); 
} 




/*-- PLANTA --*/
function pzasEFPlanta($fecha) { 
    $lineaS = "SELECT b.fecha, SUM(b.cantPzas) FROM lineas as l, Bitacora as b 
                WHERE l.tipo BETWEEN 10 AND 19  AND l.linea = b.linea AND b.fecha >= '$fecha' 
                GROUP BY b.fecha; "; 
    return getArraySQL($lineaS); 
} 

function mtaPzasEFPlanta($fecha) { 
    $sql = "SELECT m.fecha, SUM(m.mtaPzas) FROM lineas as l, targetProduccion as m 
            WHERE l.tipo BETWEEN 10 AND 19 AND l.linea = m.linea AND m.fecha >= '$fecha' 
            GROUP BY m.fecha "; 
    return getArraySQL($sql); 
} 

/*-- PRODUCTO --*/
#SEGURIDAD 
function seguridadMes_Producto($producto, $fIni, $fFin){ 
    $sql = "SELECT DAY(b.fecha), b.clasificacion, b.reporta FROM lineas as l, seguridad as b 
            WHERE l.producto = '$producto' AND l.tipo BETWEEN 10 AND 19 AND b.tReporte = 'S' AND b.linea = l.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin'
            ORDER BY b.fecha, b.reporta; ";
    return getArraySQL($sql);
}

function ambienteMes_Producto($producto, $fIni, $fFin){ 
    $sql = "SELECT DAY(b.fecha), b.clasificacion, b.reporta FROM lineas as l, seguridad as b 
            WHERE l.producto = '$producto' AND l.tipo BETWEEN 10 AND 19 AND b.tReporte = 'A' AND b.linea = l.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin'
            ORDER BY b.fecha, b.reporta; ";
    return getArraySQL($sql);
}

#ENTREGAS
function pzasEFProducto($producto, $fIni, $fFin) { 
    $lineaS = "SELECT CAST(CONVERT(date, b.fecha, 103) as VARCHAR(15)), SUM(b.cantPzas) FROM lineas as l, Bitacora as b 
                WHERE l.producto = '$producto' AND l.tipo BETWEEN 10 AND 19  AND l.linea = b.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin'
                GROUP BY b.fecha; "; 
    return getArraySQL($lineaS); 
} 

function mtaPzasEFProducto($producto, $fecha) { 
    $sql = "SELECT CAST(CONVERT(date, m.fecha, 103) as VARCHAR(15)), SUM(m.mtaPzas) FROM lineas as l, targetProduccion as m 
            WHERE l.producto = '$producto' AND l.tipo BETWEEN 10 AND 19 AND l.linea = m.linea AND m.fecha >= '$fecha'
            GROUP BY m.fecha "; 
    return getArraySQL($sql);
}

#FALLAS INTERNAS
function calidadInternaMes_Producto ($producto, $fIni, $fFin ) { 
    $sql = "SELECT COUNT(c.id), DAY(c.fecha) 
            FROM calidadInterna as c, Lineas as l 
            WHERE l.producto = '$producto' AND c.fecha >= '$fIni' AND c.fecha <= '$fFin' AND c.linea = l.linea AND c.tipo = 1 
            GROUP BY c.id, c.fecha ORDER by c.fecha;"; 
    return getArraySQL($sql); 
} 

function calidadKmMes_Producto ($producto, $fIni, $fFin ) { 
    $sql = "SELECT COUNT(c.id), DAY(c.fecha) 
            FROM calidadInterna as c, Lineas as l 
            WHERE l.producto = '$producto' AND c.fecha >= '$fIni' AND c.fecha <= '$fFin' AND c.linea = l.linea  AND c.tipo = 2 
            GROUP BY c.id, c.fecha ORDER by c.fecha; "; 
    return getArraySQL($sql); 
} 

function targetFallasInternas_Producto ($producto, $fIni, $fFin ) { 
//    $sql = "SELECT COUNT(c.id), cast(c.fecha as varchar) 
//            FROM calidadInterna as c, Lineas as l 
//            WHERE l.cValor = '$producto' AND c.fecha >= '$fecha' AND c.linea = l.linea  AND c.tipo = 2 
//            GROUP BY c.id, c.fecha ORDER by c.fecha; "; 
//    return getArraySQL($sql); 
} 

#QQ-Sheet 
function calidadInternaAnio_Producto ($producto, $anio ) { 
    $sql = "SELECT MONTH(c.fecha) as mes 
            FROM calidadInterna as c, Lineas as l
            WHERE l.producto IN ('$producto') AND YEAR(c.fecha) = '$anio' AND c.linea = l.linea  AND c.tipo = 1 
            GROUP BY MONTH(c.fecha); "; 
    return getArraySQL($sql); 
} 

function calidadKmAnio_Producto ($producto, $anio) { 
    $sql = "SELECT MONTH(c.fecha) as mes 
            FROM calidadInterna as c, Lineas as l 
            WHERE l.producto IN ('$producto') AND  YEAR(c.fecha) = '$anio' AND c.linea = l.linea AND c.tipo = 2 
            GROUP BY MONTH(c.fecha); ";
    return getArraySQL($sql); 
} 

#OEE 
function  oeeDia_Producto($producto, $fIni, $fFin){     
    $sql = "SELECT CONVERT(varchar(11),o.fecha), AVG(o.prod), AVG(o.tec), AVG(o.org), AVG(o.cal), AVG(o.cam) , AVG(o.tDes) 
            FROM OEEPercentDay as o, Lineas as l 
            WHERE l.producto = '$producto' AND l.linea = o.linea AND l.linea <> 'L163-CP2' AND L.linea <> 'L107' AND o.fecha >= '$fIni' AND fecha <= '$fFin' AND (o.tec <> 0 OR o.prod <> 0 OR o.org <> 0)
            GROUP BY o.fecha 
            ORDER BY o.fecha ASC"; 
    return getArraySQL($sql);
}

function targetOEEDia_Producto($producto, $fIni, $fFin){     
    $sql = "SELECT CONVERT(varchar(11),o.fecha), AVG(o.oee)
            FROM targetLosses as o, Lineas as l 
            WHERE l.producto = '$producto' AND l.linea = o.linea AND l.linea <> 'L163-CP2' AND L.linea <> 'L107' AND o.fecha >= '$fIni' AND fecha <= '$fFin' AND (o.OEE > 0 )
            GROUP BY o.fecha 
            ORDER BY o.fecha ASC";
    return getArraySQL($sql);
}

#SCRAP 
function ifk_Day_Producto ($producto, $fIni, $fFin){
    $sql = "SELECT CONVERT(varchar(15), i.docDate), sum(i.valueCompanyCode) AS valIFK 
            FROM IfkDaily AS i, Lineas as l, linea_costCenter as c 
            WHERE l.producto = '$producto' AND L.linea = c.linea AND i.costCenter LIKE '%'+c.costCenter+'%' AND i.docDate >= '$fIni' AND i.docDate <= '$fFin' 
            GROUP BY i.docDate ORDER BY i.docDate"; 
    return getArraySQL($sql); 
} 

function miscellaneous_Day_Producto ($producto, $fIni, $fFin){
    $sql = "SELECT CONVERT(varchar(15), m.docDate), sum(m.valueCompanyCode) AS valMISC 
            FROM MiscellaneousDaily as m, Lineas as l, linea_costCenter as c 
            WHERE l.producto = '$producto' AND L.linea = c.linea AND m.costCenter LIKE '%'+c.costCenter+'%' AND m.docDate >= '$fIni' AND m.docDate <= '$fFin' 
            GROUP BY m.docDate ORDER BY m.docDate "; 
    return getArraySQL($sql); 
} 

#TARGET 
function targetScrap_Day_Producto ($producto, $fIni, $fFin){
    $sql = "SELECT CONVERT(varchar(15), m.fecha), sum(scrap) AS valMISC 
            FROM targetLosses as m, Lineas as l
            WHERE l.producto = '$producto' AND L.linea = m.linea AND m.fecha >= '$fIni' AND m.fecha <= '$fFin' 
            GROUP BY m.fecha ORDER BY m.fecha"; 
    return getArraySQL($sql); 
} 

#SUB-ENSAMBLE
#ROTOR / INDUCIDO 
function pzas_ROTOR_INDUCIDO_Producto($producto, $fIni, $fFin) { 
    $lineaS = "SELECT CAST(CONVERT(date, b.fecha, 103) as VARCHAR(15)), SUM(b.cantPzas) as reales FROM lineas as l, Bitacora as b
                WHERE l.producto = '$producto' AND l.tipo BETWEEN 20 AND 29 AND l.linea = b.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' AND l.linea <> 'L163-CP2' AND l.linea <> 'L107'
                GROUP BY b.fecha ORDER BY b.fecha;"; 
    return getArraySQL($lineaS); 
} 

function pzasMta_ROTOR_INDUCIDO_Producto($producto, $fIni, $fFin) { 
    $lineaS = "SELECT CAST(CONVERT(date, t.fecha, 103) as VARCHAR(15)), t.mtaPzas FROM lineas as l, targetProduccion as t
                WHERE l.producto = '$producto' AND l.tipo BETWEEN 20 AND 29 AND t.linea = l.linea AND t.fecha >= '$fIni' AND t.fecha <= '$fFin' AND l.linea <> 'L163-CP2' AND l.linea <> 'L107'
                GROUP BY t.fecha, t.mtaPzas ORDER BY t.fecha;"; 
    return getArraySQL($lineaS); 
} 

#ESTATOR / SOLENOIDE
function pzas_ESTATOR_SOLENOIDE_Producto($producto, $fIni, $fFin) { 
    $lineaS = "SELECT CAST(CONVERT(date, b.fecha, 103) as VARCHAR(15)),SUM(b.cantPzas) as reales FROM lineas as l, Bitacora as b
                WHERE l.producto = '$producto' AND l.tipo BETWEEN 30 AND 39 AND l.linea = b.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' AND l.linea <> 'L163-CP2' AND l.linea <> 'L107'
                GROUP BY b.fecha ORDER BY b.fecha;"; 
    return getArraySQL($lineaS); 
} 

function pzasMta_ESTATOR_SOLENOIDE_Producto($producto, $fIni, $fFin) { 
    $lineaS = "SELECT CAST(CONVERT(date, t.fecha, 103) as VARCHAR(15)), t.mtaPzas FROM lineas as l, targetProduccion as t
                WHERE l.producto = '$producto' AND l.tipo BETWEEN 30 AND 39 AND t.linea = l.linea AND t.fecha >= '$fIni' AND t.fecha <= '$fFin' AND l.linea <> 'L163-CP2' AND l.linea <> 'L107'
                GROUP BY t.fecha, t.mtaPzas ORDER BY t.fecha;"; 
    return getArraySQL($lineaS); 
} 

#RECTIFICADOR 
function pzas_RECTIFICADOR_Producto($producto, $fIni, $fFin) { 
    $lineaS = "SELECT CAST(CONVERT(date, b.fecha, 103) as VARCHAR(15)), SUM(b.cantPzas) as reales FROM lineas as l, Bitacora as b
                WHERE l.producto = '$producto' AND l.tipo BETWEEN 40 AND 49 AND l.linea = b.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' AND l.linea <> 'L163-CP2' AND l.linea <> 'L107'
                GROUP BY b.fecha ORDER BY b.fecha;"; 
    return getArraySQL($lineaS); 
}

function pzasMta_RECTIFICADOR_Producto($producto, $fIni, $fFin) { 
    $lineaS = "SELECT CAST(CONVERT(date, t.fecha, 103) as VARCHAR(15)), t.mtaPzas FROM lineas as l, targetProduccion as t
                WHERE l.producto = '$producto' AND l.tipo BETWEEN 40 AND 49 AND t.linea = l.linea AND t.fecha >= '$fIni' AND t.fecha <= '$fFin' AND l.linea <> 'L163-CP2' AND l.linea <> 'L107'
                GROUP BY t.fecha, t.mtaPzas ORDER BY t.fecha;"; 
    return getArraySQL($lineaS); 
} 

#PRODUCTIVIDAD 
function prodD_PzasMin_Producto($producto, $fIni, $fFin){ 
    $query = "SELECT CAST(CONVERT(date, b.fecha, 103) as VARCHAR(15)) AS fech , SUM(b.cantPzas) as Pzas, SUM(b.duracion) as Duracion FROM Bitacora as b, lineas as l
            WHERE l.producto = '$producto' AND b.linea = l.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' GROUP BY b.fecha ORDER BY b.fecha ASC ";
    return getArraySQL($query);     
}

function prodD_Hombres_Turnos_Producto ($producto, $fIni, $fFin){
    $query = "SELECT CAST(CONVERT(date, t.fecha, 103) as VARCHAR(15)) AS fech, sum(t.NoPersonal) as hombres, count(t.Fecha) as turnos 
            FROM TiempoTurno as t, Lineas as l 
            WHERE l.producto = '$producto' AND t.linea = l.linea AND t.fecha >= '$fIni' AND t.fecha <= '$fFin'  GROUP by t.fecha ORDER by t.fecha ";
    return getArraySQL($query); 
}

function prodD_Target_Producto ($producto, $fIni, $fFin) { 
    $sql = "SELECT CONVERT(varchar(15), t.fecha), AVG(productividad) 
                FROM targetLosses as t, Lineas as l 
                WHERE l.producto = '$producto' AND t.linea = l.linea AND t.fecha >= '$fIni' AND t.fecha <= '$fFin' 
                GROUP BY t.fecha;";
    return getArraySQL($sql); 
} 


#INVENTARIO 
function inventarioD_Producto ($producto, $fIni, $fFin) { 
    $sql = "SELECT i.fecha, AVG(i.vmax), AVG(i.vmin), AVG(i.vreal) 
            FROM inventario AS i, Lineas AS l 
            WHERE l.producto = '$producto' AND L.LINEA = I.linea AND I.fecha >= '$fIni' AND FECHA <= '$fFin' 
            GROUP BY i.fecha ORDER BY i.fecha ";
    return getArraySQL($sql); 
} 

# PARETO 
function pareto_TOP5_Producto ($producto, $fIni, $fFin) { 
    $sql = "SELECT TOP 5 b.linea, b.operacion, b.problema, SUM(b.duracion) as tm, Count(b.problema) as fre 
            FROM Bitacora as b, Lineas as l 
            WHERE L.producto = '$producto' AND l.linea = b.linea AND b.tema IN('Tecnicas','Organizacionales') AND b.problema <> '' AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' AND l.linea <> 'L163-CP1'
            GROUP BY b.linea, b.operacion, b.problema 
            ORDER BY tm DESC; ";
    return getArraySQL($sql); 
} 

function pareto_TOP3_Producto ($producto, $fIni, $fFin) { 
    $sql = "SELECT TOP 3 b.linea, b.operacion, b.problema, sum(cast(b.scrap as int)) as scrap , count(b.problema) as frec, sum(cast(b.scrap as int)) as scrap
            FROM Bitacora as b, Lineas as l 
            WHERE L.producto = '$producto' AND l.linea = b.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' and b.tema = 'Calidad' AND l.linea <> 'L163-CP1' 
            GROUP BY b.linea, b.operacion, b.problema 
            ORDER BY sum(cast(b.scrap as int)) DESC; ";
    return getArraySQL($sql); 
} 

function pareto_TOP1_Producto ($producto, $fIni, $fFin) { 
    $sql = "SELECT TOP 1 b.linea, CONCAT(CONCAT(b.noParte,' a '), b.noParteCambio) as prob, SUM(b.DURACION) as tm, Count(b.problema) as fre 
            FROM Bitacora as b, Lineas as l 
            WHERE L.producto = '$producto' AND l.linea = b.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' and b.tema IN('Cambio de Modelo') 
            GROUP BY b.linea, CONCAT(CONCAT(b.noParte,' a '), b.noParteCambio); ";
    return getArraySQL($sql); 
} 




/*-- CADENA DE VALOR --*/
#SEGURIDAD
function seguridad_CValor($cadValor, $fIni, $fFin) { 
    $lineaS = "SELECT DAY(c.fecha), c.clasificacion, c.reporta 
                FROM seguridad as c, Lineas as l 
                WHERE c.tReporte = 'S' AND l.cValor IN ('$cadValor') AND c.linea = l.linea AND c.fecha >= '$fIni' AND c.fecha <= '$fFin' 
                ORDER BY c.fecha, c.reporta"; 
    return getArraySQL($lineaS); 
} 

function ambiente_CValor($cadValor, $fIni, $fFin) { 
    $sql = "SELECT DAY(c.fecha), c.clasificacion, c.reporta 
                FROM seguridad as c, Lineas as l 
                WHERE c.tReporte = 'A' AND l.cValor IN ('$cadValor') AND c.linea = l.linea AND c.fecha >= '$fIni' AND c.fecha <= '$fFin' 
                ORDER BY c.fecha, c.reporta"; 
    return getArraySQL($sql);
} 

#QQ-Sheet 
function calidadInternaAnio_CValor ($cValor, $anio ) {  
    $sql = "SELECT MONTH(c.fecha) as mes 
            FROM calidadInterna as c, Lineas as l
            WHERE l.cValor IN ('$cValor') AND YEAR(c.fecha) = '$anio' AND c.linea = l.linea  AND c.tipo = 1 
            GROUP BY MONTH(c.fecha); "; 
    return getArraySQL($sql); 
} 

function calidadKmAnio_CValor ($cValor, $anio) { 
    $sql = "SELECT MONTH(c.fecha) as mes 
            FROM calidadInterna as c, Lineas as l 
            WHERE l.cValor IN ('$cValor') AND  YEAR(c.fecha) = '$anio' AND c.linea = l.linea AND c.tipo = 2 
            GROUP BY MONTH(c.fecha); ";
    return getArraySQL($sql); 
} 

#FALLAS INTERNAS
function calidadInternaMes_CValor ($cValor, $fIni, $fFin ) { 
    $sql = "SELECT COUNT(c.id), DAY(c.fecha) 
            FROM calidadInterna as c, Lineas as l 
            WHERE l.cValor = '$cValor' AND c.fecha >= '$fIni' AND c.fecha <= '$fFin' AND c.linea = l.linea AND c.tipo = 1 
            GROUP BY c.id, c.fecha ORDER by c.fecha; ";
    return getArraySQL($sql); 
} 

function calidadKmMes_CValor ($cValor, $fIni, $fFin ) { 
    $sql = "SELECT COUNT(c.id), DAY(c.fecha) 
            FROM calidadInterna as c, Lineas as l 
            WHERE l.cValor = '$cValor' AND c.fecha >= '$fIni' AND c.fecha <= '$fFin' AND c.linea = l.linea  AND c.tipo = 2 
            GROUP BY c.id, c.fecha ORDER by c.fecha; "; 
    return getArraySQL($sql); 
} 

#ENTREGAS
function pzasEFCValor($cadValor, $fIni, $fFin) { 
    $lineaS = "SELECT CAST(CONVERT(date, b.fecha, 103) as VARCHAR(15)), SUM(b.cantPzas) FROM lineas as l, Bitacora as b 
                WHERE l.cValor IN ('$cadValor') AND l.tipo BETWEEN 10 AND 19  AND l.linea = b.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin'
                GROUP BY b.fecha; "; 
    return getArraySQL($lineaS); 
} 

function mtaPzasEFCValor($cadValor, $fIni, $fFin) { 
    $sql = "SELECT CAST(CONVERT(date, m.fecha, 103) as VARCHAR(15)), SUM(m.mtaPzas) FROM lineas as l, targetProduccion as m 
            WHERE l.cValor IN ('$cadValor') AND l.tipo BETWEEN 10 AND 19 AND l.linea = m.linea AND m.fecha >= '$fIni' AND m.fecha <= '$fFin'
            GROUP BY m.fecha "; 
    return getArraySQL($sql);
}

#OEE 
function  oeeDia_CValor($cValor, $fIni, $fFin){     
    $sql = "SELECT CONVERT(varchar(11),o.fecha), AVG(o.prod), AVG(o.tec), AVG(o.org), AVG(o.cal), AVG(o.cam) , AVG(o.tDes) 
            FROM OEEPercentDay as o, Lineas as l 
            WHERE l.cValor = '$cValor' AND l.linea = o.linea AND l.linea <> 'L163-CP2' AND L.linea <> 'L107' AND o.fecha >= '$fIni' AND fecha <= '$fFin' AND (o.tec <> 0 OR o.prod <> 0 OR o.org <> 0)
            GROUP BY o.fecha 
            ORDER BY o.fecha ASC"; 
    return getArraySQL($sql);
}

function targetOEEDia_CValor($cValor, $fIni, $fFin){     
    $sql = "SELECT CONVERT(varchar(11),o.fecha), AVG(o.oee)
            FROM targetLosses as o, Lineas as l 
            WHERE l.cvalor = '$cValor' AND l.linea = o.linea AND l.linea <> 'L163-CP2' AND L.linea <> 'L107' AND o.fecha >= '$fIni' AND fecha <= '$fFin' AND (o.OEE > 0 ) 
            GROUP BY o.fecha 
            ORDER BY o.fecha ASC";
    return getArraySQL($sql);
}

function entregas_Sub_CadValor($cValor, $fIni, $fFin){ 
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), sum(cantPzas) FROM bitacora WHERE linea = '$cValor' AND tema LIKE 'Piezas Producidas' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY fecha ORDER BY fecha ";
    return getArraySQL($sql);
}

function metaPzas_Sub_CadValor($cValor, $fIni, $fFin ) { 
    $sql = "SELECT CONVERT(varchar(15), fecha), mtaPzas FROM targetProduccion WHERE linea = '$cValor' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY CONVERT(varchar(15), fecha), mtaPzas; "; 
    return getArraySQL($sql); 
} 

#PRODUCTIVIDAD
function prodD_PzasMin_CValor($producto, $fIni, $fFin){ 
    $query = "SELECT CAST(CONVERT(date, b.fecha, 103) as VARCHAR(15)) AS fech , SUM(b.cantPzas) as Pzas, SUM(b.duracion) as Duracion FROM Bitacora as b, lineas as l
            WHERE l.cValor = '$producto' AND b.linea = l.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' GROUP BY b.fecha ORDER BY b.fecha ASC ";
    return getArraySQL($query);     
}

function prodD_Hombres_Turnos_CValor($producto, $fIni, $fFin){
    $query = "SELECT CAST(CONVERT(date, t.fecha, 103) as VARCHAR(15)) AS fech, sum(t.NoPersonal) as hombres, count(t.Fecha) as turnos 
            FROM TiempoTurno as t, Lineas as l 
            WHERE l.cValor = '$producto' AND t.linea = l.linea AND t.fecha >= '$fIni' AND t.fecha <= '$fFin'  GROUP by t.fecha ORDER by t.fecha ";
    return getArraySQL($query); 
}

function prodD_Target_CValor($producto, $fIni, $fFin) { 
    $sql = "SELECT CONVERT(varchar(15), t.fecha), AVG(productividad) 
                FROM targetLosses as t, Lineas as l 
                WHERE l.cValor = '$producto' AND t.linea = l.linea AND t.fecha >= '$fIni' AND t.fecha <= '$fFin' 
                GROUP BY t.fecha;";
    return getArraySQL($sql); 
} 

#SCRAP
function ifk_Day_CValor ($cValor, $fIni, $fFin){
    $sql = "SELECT CONVERT(varchar(15), i.docDate), sum(i.valueCompanyCode) AS valIFK 
            FROM IfkDaily AS i, Lineas as l, linea_costCenter as c 
            WHERE l.cValor = '$cValor' AND L.linea = c.linea AND i.costCenter LIKE '%'+c.costCenter+'%' AND i.docDate >= '$fIni' AND i.docDate <= '$fFin' 
            GROUP BY i.docDate ORDER BY i.docDate"; 
    return getArraySQL($sql);
} 

function miscellaneous_Day_CValor ($cValor, $fIni, $fFin){
    $sql = "SELECT CONVERT(varchar(15), m.docDate), sum(m.valueCompanyCode) AS valMISC 
            FROM MiscellaneousDaily as m, Lineas as l, linea_costCenter as c 
            WHERE l.cValor = '$cValor' AND L.linea = c.linea AND m.costCenter LIKE '%'+c.costCenter+'%' AND m.docDate >= '$fIni' AND m.docDate <= '$fFin' 
            GROUP BY m.docDate ORDER BY m.docDate "; 
    return getArraySQL($sql);
} 

# TARGET 

function targetScrap_Day_CValor($cValor, $fIni, $fFin){ 
    $sql = "SELECT CONVERT(varchar(15), m.fecha), sum(scrap) AS valMISC 
            FROM targetLosses as m, Lineas as l
            WHERE l.cvalor = '$cValor' AND L.linea = m.linea AND m.fecha >= '$fIni' AND m.fecha <= '$fFin' 
            GROUP BY m.fecha ORDER BY m.fecha";
    return getArraySQL($sql);
}


#PARETOS
function pareto_TOP5_CValor ($cValor, $fIni, $fFin) { 
    $sql = "SELECT TOP 5 b.linea, b.operacion, b.problema, SUM(b.duracion) as tm, Count(b.problema) as fre 
            FROM Bitacora as b, Lineas as l 
            WHERE l.cvalor = '$cValor' AND l.linea = b.linea AND b.tema IN('Tecnicas','Organizacionales') AND b.problema <> '' AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' AND l.linea <> 'L163-CP1'
            GROUP BY b.linea, b.operacion, b.problema 
            ORDER BY tm DESC; ";
    return getArraySQL($sql); 
} 

function pareto_TOP3_CValor ($cValor, $fIni, $fFin) { 
    $sql = "SELECT TOP 3 b.linea, b.operacion, b.problema, sum(cast(b.scrap as int)) as scrap , count(b.problema) as frec, sum(cast(b.scrap as int)) as scrap
            FROM Bitacora as b, Lineas as l 
            WHERE l.cvalor = '$cValor' AND l.linea = b.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' and b.tema = 'Calidad' AND l.linea <> 'L163-CP1' 
            GROUP BY b.linea, b.operacion, b.problema 
            ORDER BY sum(cast(b.scrap as int)) DESC; ";
    return getArraySQL($sql); 
} 

function pareto_TOP1_CValor ($cValor, $fIni, $fFin) { 
    $sql = "SELECT TOP 1 b.linea, CONCAT(CONCAT(b.noParte,' a '), b.noParteCambio) as prob, SUM(b.DURACION) as tm, Count(b.problema) as fre 
            FROM Bitacora as b, Lineas as l 
            WHERE L.producto = '$cValor' AND l.linea = b.linea AND b.fecha >= '$fIni' AND b.fecha <= '$fFin' and b.tema IN('Cambio de Modelo') 
            GROUP BY b.linea, CONCAT(CONCAT(b.noParte,' a '), b.noParteCambio); ";
    return getArraySQL($sql); 
} 




