<?php
require_once 'ServerConfig.php'; 
/**
 * Description of ServerFunctions
 *
 * @author GJA5TL
 */

//CONFIGURAMOS PARA QUE LA HORA SEA LA INTERNACION MEXICANA Y NO LA DE ALEMANIA
date_default_timezone_set("America/Mexico_City"); 

//Devuelve un array multidimensional con el resultado de la consulta
function getArraySQL($sql) { 
    $connectionObj = new ServerConfig(); 
    $connectionStr = $connectionObj -> serverConnection(); 
    
    if (!$result = sqlsrv_query($connectionStr, $sql)) 
        die("Conexion: Server Functions (Line 48-49)"); 
    
    $rawdata = array(); 
    $i = 0; 
    while ($row = sqlsrv_fetch_array($result)) { 
        $rawdata[$i] = $row; 
        $i++; 
    } 
    //$connectionObj ->serverDisconnect();
    return $rawdata; 
} 

//OBTENEMOS LA FECHA POR SEPARADO
function listarLineas() { 
    $sql = "SELECT linea, nombre FROM Lineas ORDER BY 1"; 
    return getArraySQL($sql); 
}

function cProducto_CValor($cValor) { 
    $sql = "SELECT producto FROM Lineas WHERE cValor = '$cValor' "; 
    return getArraySQL($sql); 
} 

function listar_Lineas_Producto($producto) { 
    $sql = "SELECT linea, nombre FROM Lineas WHERE producto = '$producto' GROUP BY linea, nombre ORDER BY 1 "; 
    return getArraySQL($sql); 
} 

function listarCValor_Producto($producto) { 
    $sql = "SELECT cValor FROM Lineas WHERE producto = '$producto' GROUP BY cvalor ORDER BY 1 "; 
    return getArraySQL($sql); 
} 

function listarLineas_Producto($producto) { 
    $sql = "SELECT linea, nombre FROM Lineas WHERE producto = '$producto' ORDER BY 1 "; 
    return getArraySQL($sql); 
} 

function listarLineas_CValor($cValor) { 
    $sql = "SELECT linea, nombre FROM Lineas WHERE cValor = '$cValor' ORDER BY 1"; 
    return getArraySQL($sql); 
}

function nombreLinea($linea) { 
    $lineaS = "SELECT l.nombre, c.costCenter, c.profitCenter, l.CValor, l.producto FROM Lineas as l, linea_costCenter as c WHERE l.linea = '$linea' AND c.linea = l.linea ";
    return getArraySQL($lineaS);
}

/* OEE MENSUAL */
function oeeMensualGrafica($linea) {
    $sql = "SELECT MonthOEE, OEE, Quality, Organizational, Technical, Changeover, Performance FROM HourlyCountPercent WHERE Line LIKE '$linea' ORDER BY MonthOEE ASC";
    return getArraySQL($sql);
}

//Listar los dias de produccion por Mes 
function pzasProdAnual($linea, $mes, $anio) {
    $sql = "SELECT anio, SUM(cantPzas) FROM bitacora WHERE linea LIKE '$linea' AND mes = '$mes' AND anio <= '$anio' GROUP BY anio";
    return getArraySQL($sql);
}

function pzasProdMes($linea, $anio) {
    $sql = "SELECT mes, SUM(cantPzas) FROM bitacora WHERE linea LIKE '$linea' AND anio = '$anio' GROUP BY mes ORDER BY mes ASC";
    return getArraySQL($sql);
}

function pzasProdNoParteAnio($linea, $anio) {
    $sql = "SELECT noParte FROM Bitacora WHERE linea LIKE '$linea' AND Anio = '$anio' AND tema = 'Piezas Producidas' AND noParte <> '$linea' GROUP BY noParte ORDER BY noParte ASC";
    return getArraySQL($sql);
}

function pzasProdNoParteAnioDia($linea, $fIni, $fFin) {
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), noParte, SUM(cantPzas) FROM Bitacora WHERE linea LIKE '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' AND  tema = 'Piezas Producidas' GROUP BY fecha, noParte ORDER BY fecha, noParte ASC";
    return getArraySQL($sql);
}

function pzasProdNoPartePeriod($linea, $fIni, $fFin) {
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), noParte, SUM(cantPzas) FROM Bitacora WHERE linea LIKE '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' AND tema = 'Piezas Producidas' GROUP BY fecha, noParte ORDER BY fecha, noParte ASC";
    return getArraySQL($sql);
} 

function pzasProdNoParte($linea, $mes, $anio) {
    $sql = "SELECT noParte FROM Bitacora WHERE linea LIKE '$linea' AND Mes = '$mes' and Anio = '$anio' AND tema = 'Piezas Producidas' GROUP BY noParte ORDER BY noParte ASC";
    return getArraySQL($sql); 
} 
function pzasNoParteMes($linea, $anio, $mes) { //linea LIKE '$linea' AND Mes = '$mes' and Anio = '$anio' AND tema = 'Piezas Producidas'
    $sql = "SELECT noParte, SUM(cantPzas) AS sum FROM Bitacora WHERE linea='$linea' AND Mes ='$mes' and Anio = '$anio' AND tema = 'Piezas Producidas' GROUP BY noParte ORDER BY noParte ASC";
    return getArraySQL($sql);
}
//GENERAL 
//OBTENEMOS TURNO Y HORAS DE TURNOS REGISTRADOS 
function turnosHoras ($linea, $anio, $fechaI, $fechaF){
    $sql = "SELECT fecha, Turno,HoraInicio,HoraFin FROM TiempoTurno WHERE Linea = '$linea' AND Fecha >= '$fechaI' and Fecha <= '$fechaF' ORDER BY Fecha, Turno,HoraInicio ASC";
    return getArraySQL($sql);
} 
/* SCRAP */
function scrapAnual($linea, $anio) {
    $sql = "SELECT anio, SUM(cast(scrap AS INTEGER)) FROM bitacora WHERE  linea LIKE '$linea' AND anio = '$anio' GROUP BY anio";
    return getArraySQL($sql);
}

function scrapMes($linea, $anio) { 
    $sql = "SELECT mes, SUM(cast(scrap AS INTEGER)) FROM bitacora WHERE linea LIKE '$linea' AND anio = '$anio' GROUP BY mes ORDER BY mes ASC";
    return getArraySQL($sql);
} 

/* PERDIDAS */
function pTecnicasMes($linea, $anio) {
    $sql = "SELECT mes,SUM(duracion) FROM Bitacora WHERE tema LIKE 'Tecnicas' AND linea LIKE '$linea' AND anio = '$anio' GROUP BY mes ORDER BY mes ASC";
    return getArraySQL($sql);
}

function pTecnicasNoParte($linea,$anio, $mes, $dia, $operacion, $problema){
    $sql = "SELECT b.noParte,SUM(b.duracion) FROM Bitacora as b WHERE b.linea = '$linea' AND b.anio = '$anio' AND b.mes = '$mes' and b.dia = '$dia' AND b.tema = 'Tecnicas' AND b.problema = '$problema' AND b.operacion = '$operacion' GROUP BY b.noParte";
    return getArraySQL($sql);
}

/* ORGANIZACIONALES */
function pOrganizacionalesMes($linea, $anio) {
    $sql = "SELECT mes,SUM(duracion) FROM Bitacora WHERE tema LIKE 'Organizacionales' AND linea LIKE '$linea' AND anio = '$anio' GROUP BY mes ORDER BY mes ASC";
    return getArraySQL($sql);
}

function pOrganizacionalesNoParte($linea,$anio, $mes, $dia, $problema, $detalleMaterial){
    $sql = "SELECT b.noParte,SUM(b.duracion) FROM Bitacora as b WHERE b.linea = '$linea' AND b.anio = '$anio' AND b.mes = '$mes' AND b.dia = '$dia' AND b.tema = 'Organizacionales' AND b.problema = '$problema' AND b.detalleMaterial = '$detalleMaterial' GROUP BY b.noParte";
    return getArraySQL($sql);
}

/* CAMBIOS */
function pCambioModMes($linea, $anio) {
    $sql = "SELECT mes, SUM(duracion) as tmp FROM Bitacora WHERE tema LIKE 'Cambio de Modelo' AND linea LIKE '$linea' AND anio = '$anio' GROUP BY mes ORDER BY mes ASC";
    return getArraySQL($sql);
}

function pCambioNoParte($linea,$anio, $mes, $dia, $noParte, $nuevoNoParte){
    $sql = "SELECT SUM(b.duracion) FROM Bitacora as b "
         . "WHERE b.linea = '$linea' AND b.anio = '$anio' AND b.mes = '$mes' AND b.dia = '$dia' AND b.tema = 'Cambio de Modelo' AND b.noParte = '$noParte' AND b.noParteCambio = '$nuevoNoParte' GROUP BY b.noParte";
    return getArraySQL($sql);
}

/* PLANEADOS */
function pPlaneadoMes($linea, $anio) {
    $sql = "SELECT mes, SUM(duracion) as tmp FROM Bitacora WHERE tema LIKE 'Paros Planeados' AND linea LIKE '$linea' AND anio = '$anio' "
    . " AND area <> 'Arranque de linea' AND area <> 'Juntas de trabajo planeado' AND area <> 'Junta Informativa' AND area <> 'Comedor' "  
    . "GROUP BY mes ORDER BY mes ASC";
    return getArraySQL($sql);
}

function pPlaneadosNoParte($linea,$anio, $mes, $dia, $area){
    $sql = "SELECT b.noParte, SUM(b.duracion) FROM Bitacora as b WHERE b.linea = '$linea' AND b.anio = '$anio' AND b.mes = '$mes' AND b.dia = '$dia' AND b.tema = 'Paros Planeados' AND b.area = '$area' GROUP BY b.noParte";
    return getArraySQL($sql);
}

/* CALIDAD */
function pCalidadMes($linea, $anio) {
    $sql = "SELECT mes, sum(cast(scrap as int)) as tmp FROM Bitacora WHERE tema LIKE 'Calidad' AND linea LIKE '$linea' AND anio = '$anio' GROUP BY mes ORDER BY mes ASC";
    return getArraySQL($sql);
}

/* PARETOS TOPS */
function t5TecnicasYOrganizacionales($linea,$varDiaI, $varDiaF) {
    $sql = "SELECT TOP 5 operacion,problema, SUM(duracion) as tm, Count(problema) as fre FROM Bitacora WHERE LINEA LIKE '$linea' AND tema IN('Tecnicas','Organizacionales') AND problema <> '' AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' GROUP BY operacion,problema order by tm desc";
    return getArraySQL($sql);
}

function t5TecnicasYOrganizacionalesFrec($linea, $varDiaI, $varDiaF) {
    $sql = "SELECT TOP 5 operacion,problema, COUNT(problema) as fre,SUM(duracion) as tm FROM Bitacora "
         . "WHERE linea LIKE '$linea' AND tema IN('Tecnicas','Organizacionales') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' "
            . "GROUP BY operacion,problema order by fre desc";
    return getArraySQL($sql);
}

//NP
function t5TecnicasYOrganizacionalesNoParte($linea, $fIni, $fFin, $problema, $operacion){
    $sql = "SELECT noParte,SUM(duracion) FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' AND tema IN('Tecnicas','Organizacionales') AND problema = '$problema' AND operacion = '$operacion' GROUP BY noParte";
    return getArraySQL($sql);
}

function t1pareto($linea, $varDiaI, $varDiaF) {    
    $sql = "SELECT TOP 1 CONCAT(CONCAT(noParte,' a '), noParteCambio) as prob, SUM(DURACION) as tm, Count(problema) as fre FROM Bitacora 
WHERE LINEA LIKE '$linea' AND tema IN('Cambio de Modelo') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' GROUP BY CONCAT(CONCAT(noParte,' a '), noParteCambio)";
    return getArraySQL($sql);
}

//NP
function t1CambioNoParte($linea,$varDiaI, $varDiaF, $problema){
    $sql = "SELECT noParte,SUM(duracion) FROM Bitacora WHERE linea = '$linea' AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' AND tema IN('Cambio de Modelo') AND problema = '$problema' GROUP BY noParte";
    return getArraySQL($sql);
}

function t1paretoFrec($linea, $varDiaI, $varDiaF) {
    $sql = "SELECT TOP 1 CONCAT(CONCAT(noParte,' a '), noParteCambio) as prob, COUNT(problema) as fre ,SUM(duracion) as tm FROM Bitacora "
         . "WHERE LINEA LIKE '$linea' AND tema IN('Cambio de Modelo') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' GROUP BY noParte,noParteCambio order by fre DESC";
    return getArraySQL($sql);
}

//ACTUALES
function t3Calidad($linea, $varDiaI, $varDiaF) { 
    $sql = "SELECT TOP 5 operacion, problema, sum(cast(scrap as int)) as scrap , count(problema) as frec, sum(cast(scrap as int)) as scrap
            FROM Bitacora WHERE linea = '$linea' AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' and tema = 'Calidad' GROUP BY operacion,problema ORDER BY sum(cast(scrap as int)) DESC" ;
    return getArraySQL($sql);
}

function t3CalidadYOP($linea, $varDiaI, $varDiaF, $operacion) {
    $sql = "SELECT TOP 5 operacion, problema, sum(cast(scrap as int)) as scrap, count(problema) as frec FROM Bitacora WHERE linea = '$linea' AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' and tema = 'Calidad' AND operacion = '$operacion' GROUP BY operacion,problema ORDER BY scrap DESC" ;
    return getArraySQL($sql);
}

//FRECUENCIA
function t3CalidadFrec($linea, $varDiaI, $varDiaF) {
    $sql = "SELECT TOP 5 operacion, problema, count(problema) as frec, sum(cast(scrap as int)) as scrap FROM Bitacora "
         . "WHERE linea = '$linea' AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' and tema = 'Calidad'  GROUP BY operacion,problema ORDER BY frec DESC" ;
    return getArraySQL($sql);
}

function t3CalidadFrecYOP($linea, $varDiaI, $varDiaF, $operacion) {
    $sql = "SELECT TOP 5 operacion, problema,count(problema) as frec, sum(cast(scrap as int)) as scrap FROM Bitacora "
         . "WHERE linea = '$linea' AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' and tema = 'Calidad' AND operacion = '$operacion' GROUP BY operacion,problema ORDER BY frec DESC" ;
    return getArraySQL($sql);
}

//PZAS 
function t3CalidadPzas($linea, $anio, $mes, $varDiaI, $varDiaF) {
    $sql = "SELECT TOP 5 operacion, problema,sum(cast(scrap as int)) as scrap, sum(duracion) as dur, count(problema) as frec FROM Bitacora "
         . "WHERE linea = '$linea' AND anio = '$anio' AND mes = '$mes' AND dia >= '$varDiaI' AND dia <= '$varDiaF' and tema = 'Calidad'  GROUP BY operacion,problema ORDER BY scrap DESC" ;
    return getArraySQL($sql);
}

function t3CalidadPzasYOP($linea, $anio, $mes, $varDiaI, $varDiaF) {
    $sql = "SELECT TOP 5 operacion, problema,sum(cast(scrap as int)) as scrap, sum(duracion) as dur, count(problema) as frec FROM Bitacora "
         . "WHERE linea = '$linea' AND anio = '$anio' AND mes = '$mes' AND dia >= '$varDiaI' AND dia <= '$varDiaF' and tema = 'Calidad' AND operacion = '$operacion' GROUP BY operacion,problema ORDER BY scrap DESC" ;
    return getArraySQL($sql);
}

/* Top 5 */
//TECNICAS DURACION
function t3Tecnicas($linea, $varDiaI, $varDiaF) {
    $sql = "SELECT TOP 5 operacion, problema, SUM(duracion) AS tm, COUNT (problema)as fre FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Tecnicas') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' "
    . "GROUP BY operacion,problema ORDER BY SUM(duracion) desc;";    
    return getArraySQL($sql);
}

function t3TecnicasYOp($linea, $varDiaI, $varDiaF, $operacion) {
    $sql = "SELECT TOP 5 operacion, problema, SUM(duracion) AS tm, COUNT (problema)as fre FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Tecnicas') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' AND operacion = '$operacion'"
    . "GROUP BY operacion,problema ORDER BY SUM(duracion) desc;";    
    return getArraySQL($sql);
}

//TECNICAS FRECUENCIA
function t3TecnicasFrec($linea, $varDiaI, $varDiaF) {    
    $sql  = "SELECT TOP 5 operacion, problema, COUNT (problema)as fre, SUM(duracion) AS tm FROM bitacora "
            . "WHERE linea LIKE '$linea' AND tema IN('Tecnicas') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' "
            . "GROUP BY operacion,problema ORDER BY COUNT(problema) DESC, SUM(duracion) DESC;";
    return getArraySQL($sql);
}

function t3TecnicasFrecYOp($linea, $varDiaI, $varDiaF, $operacion) {    
    $sql  = "SELECT TOP 5 operacion, problema, COUNT (problema)as fre, SUM(duracion) AS tm FROM bitacora "
            . "WHERE linea LIKE '$linea' AND tema IN('Tecnicas') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' AND operacion = '$operacion'"
            . "GROUP BY operacion,problema ORDER BY COUNT (problema) DESC, SUM(duracion) DESC;";
    return getArraySQL($sql);
}

function t3TecnicasNoParte($linea, $varDiaI, $varDiaF, $operacion, $problema){
    $sql = "SELECT b.noParte,SUM(b.duracion) FROM Bitacora as b WHERE b.linea = '$linea' AND b.fecha >= '$varDiaI' and b.fecha <= '$varDiaF' AND b.tema = 'Tecnicas' AND b.problema = '$problema' AND b.operacion = '$operacion' GROUP BY b.noParte";
    return getArraySQL($sql);
}

function t3Tc($linea, $noParte) { 
    $sql = "SELECT tcPromedio FROM tcNumParte WHERE linea = '$linea' AND numParte = '$noParte'";
    return getArraySQL($sql);
}

//ORGANIZACIONALES DURACION
function t3Organizacionales($linea, $varDiaI, $varDiaF) {
    $sql = "SELECT TOP 5 operacion, problema, SUM(duracion) as tm, COUNT(problema) AS frec, detalleMaterial FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' GROUP BY operacion, problema, detalleMaterial ORDER BY tm DESC;";
    return getArraySQL($sql);
}

function t3OrganizacionalesYOP($linea, $varDiaI, $varDiaF, $operacion) {
    $sql = "SELECT TOP 5 operacion, CONCAT(CONCAT(problema,' '), detalleMaterial) as problema,SUM(duracion) as tm,COUNT(problema) AS frec FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' AND operacion = '$operacion' GROUP BY operacion, problema, detalleMaterial ORDER BY tm DESC;";
    return getArraySQL($sql);
}

//FECUENCIA
function t3OrganizacionalesFrec($linea, $varDiaI, $varDiaF) { 
    $sql = "SELECT TOP 5 operacion, problema, COUNT(problema) AS frec, SUM(duracion) as tm,detalleMaterial FROM bitacora "
        . "WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' "
        . "GROUP BY operacion, problema, detalleMaterial ORDER BY COUNT(problema) DESC, SUM(duracion) DESC;";
    return getArraySQL($sql);
}
//NP
function t3OrganizacionalesNoParte($linea, $varDiaI, $varDiaF, $problema, $detalleMaterial){
    $sql = "SELECT noParte,SUM(duracion) FROM Bitacora WHERE linea = '$linea' AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' AND tema = 'Organizacionales' AND problema = '$problema' AND detalleMaterial = '$detalleMaterial' GROUP BY noParte";
    return getArraySQL($sql);
}

//CAMBIO DE MODELO DURACION
function t3CambioModelo($linea,$varDiaI, $varDiaF) { 
    $sql = "SELECT TOP 5 CONCAT(CONCAT(noParte,' a '), noParteCambio) as problema, SUM(duracion) as dur, COUNT(problema) as tm FROM bitacora
            WHERE linea LIKE '$linea' AND tema IN('Cambio de Modelo') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' GROUP BY CONCAT(CONCAT(noParte,' a '), noParteCambio) ORDER BY dur desc;";
    return getArraySQL($sql);
}
//CAMBIO DE MODELO FRECUENCIA
function t3CambioModeloFrec($linea, $varDiaI, $varDiaF) { 
   $sql = "SELECT TOP 5 CONCAT(CONCAT(noParte,' a '), noParteCambio) as problema, COUNT(problema) as tm, SUM(duracion) as dur FROM bitacora "
        . "WHERE linea LIKE '$linea' AND tema IN('Cambio de Modelo') AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' "
        . "GROUP BY CONCAT(CONCAT(noParte,' a '), noParteCambio) ORDER BY COUNT(problema) DESC, SUM(duracion) DESC ;";    
    return getArraySQL($sql);
}
//NP
function t3CambioNoParte($linea, $fIni, $fFin, $problema){
    $sql = "SELECT noParte, SUM(duracion) FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' AND tema = 'Cambio de Modelo' AND CONCAT(CONCAT(noParte,' a '), noParteCambio) = '$problema' GROUP BY noParte, noParteCambio";
    return getArraySQL($sql);
}

//PAROS PLANEADOS DURACION
function t3Planeados($linea, $anio, $mes,$varDiaI, $varDiaF) {
    $sql = "SELECT TOP 5 b.area, SUM(b.duracion) as tmp, COUNT(b.problema) AS fre FROM bitacora as b "
         . "WHERE b.linea LIKE '$linea' AND b.tema = 'Paros Planeados' AND b.anio = '$anio' AND b.mes = '$mes' AND b.dia >= '$varDiaI' AND b.dia <= '$varDiaF' "
         . "AND b.area <> 'Arranque de linea' AND b.area <> 'Juntas de trabajo planeado' AND b.area <> 'Junta Informativa' AND b.area <> 'Comedor' GROUP BY b.area ORDER BY tmp DESC";
     return getArraySQL($sql);
}
//FRECUENCIA
function t3PlaneadosFrec($linea, $anio, $mes, $varDiaI, $varDiaF) {
    $sql = "SELECT TOP 5 b.area, COUNT(b.problema) AS fre, SUM(b.duracion) as tmp FROM bitacora as b "
         . "WHERE b.linea LIKE '$linea' AND b.tema = 'Paros Planeados' AND b.anio = '$anio' AND b.mes = '$mes' AND b.dia >= '$varDiaI' AND b.dia <= '$varDiaF' "
         . "AND b.area <> 'Arranque de linea' AND b.area <> 'Juntas de trabajo planeado' AND b.area <> 'Junta Informativa' AND b.area <> 'Comedor' GROUP BY b.area ORDER BY fre DESC";  
    return getArraySQL($sql);
}

//NP
function t3PlanNoParte ($linea,$anio, $mes, $varDiaI, $varDiaF, $area){
     $sql = "SELECT b.noParte,SUM(b.duracion) FROM Bitacora as b WHERE b.linea = '$linea' AND b.anio = '$anio' AND b.mes = '$mes' AND b.dia >= '$varDiaI' AND b.dia <= '$varDiaF' AND b.tema = 'Paros Planeados' AND b.area = '$area' GROUP BY b.noParte";
    return getArraySQL($sql);
}

/***************** TOP5 **********************/
function t5General($linea, $varDiaI, $varDiaF){
    $sql = "SELECT Top 5 tema,operacion, problema, detalleMaterial, SUM(duracion) AS dur, count(problema) AS frecuencia FROM Bitacora 
WHERE TEMA <> 'Piezas Producidas' AND TEMA <> 'Cambio de Modelo' AND TEMA <> 'Paros Planeados' AND LINEA LIKE '$linea' AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' GROUP BY tema,operacion,problema,area,detalleMaterial ORDER BY dur DESC;";
    return getArraySQL($sql);
}

//NP 
function t5GeneralNoParte($linea, $varDiaI, $varDiaF, $tema, $operacion, $problema, $detalleMaterial){
    $sql = "SELECT noParte,SUM(duracion) FROM Bitacora WHERE linea = '$linea' AND fecha >= '$varDiaI' AND fecha <= '$varDiaF' "
         . "AND tema = '$tema' AND operacion = '$operacion' AND problema = '$problema' AND detalleMaterial = '$detalleMaterial' GROUP BY noParte";
    return getArraySQL($sql);
}

//TIEMPO CICLO
function t5GeneralFrec($linea, $varDiaI, $varDiaF){
    $sql = "SELECT Top 5 tema, operacion, problema, detalleMaterial, count(problema) AS frecuencia,SUM(duracion) AS dur FROM Bitacora WHERE TEMA <> 'Piezas Producidas' AND TEMA <> 'Cambio de Modelo' AND problema <> '' AND LINEA LIKE 'L001' AND fecha >= '2019-01-01' AND fecha <= '2019-01-25' GROUP BY tema,operacion,problema,area,detalleMaterial ORDER BY frecuencia DESC;";
    return getArraySQL($sql);
}

/*************************** LOSSES **************************/
function lossesPzasProducidas($linea, $fechaI, $fechaF){
    $sql = "SELECT SUM(cantPzas) FROM Bitacora WHERE linea LIKE '$linea' AND fecha>= '$fechaI' AND fecha <= '$fechaF'";
    return getArraySQL($sql);
}

function lossesPzasPlaneadas($linea, $fechaI, $fechaF){
    $sql = "SELECT SUM(pzasEsp) FROM OEEPercentHour WHERE linea LIKE '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' ";
    return getArraySQL($sql);
}

function lossesTiempoTotalPerdias ($linea, $fechaI, $fechaF ){
    $sql = "SELECT sum(duracion) as Tiempo_Total FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' AND tema <> 'Piezas Producidas'";
    return getArraySQL($sql);
}

function lossesTimeTechnicals($linea, $fechaI, $fechaF){
    $sql = "SELECT SUM(duracion) FROM Bitacora WHERE linea LIKE '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' AND tema = 'Tecnicas' ";
    return getArraySQL($sql);
}

function lossesTimeQuality($linea, $fechaI, $fechaF){
    //$sql = "SELECT SUM(duracion) FROM Bitacora WHERE linea LIKE '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' AND tema = 'Calidad' ";
    $sql = "SELECT SUM(CAST(scrap AS int)) FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' ";
    return getArraySQL($sql);
}

function lossesTimeOrganizational($linea, $fechaI, $fechaF){
    $sql = "SELECT SUM(duracion) FROM Bitacora WHERE linea LIKE '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' AND tema = 'Organizacionales' ";
    return getArraySQL($sql);
}

function lossesDayOrgPercent($linea, $anio, $mes,$varDiaI, $varDiaF){
    $sql = "SELECT sum(cast(replace(Organizacionales,'%','') AS DOUBLE PRECISION))  FROM HourlyCountOEE WHERE  linea LIKE '$linea' AND anio = '$anio' AND mes = '$mes' AND dia >= '$varDiaI' AND dia <= '$varDiaF' ";
    return getArraySQL($sql);
}

function lossesDayTecPercent($linea, $anio, $mes,$varDiaI, $varDiaF){
    $sql = "SELECT sum(cast(replace(Tecnicas,'%','') AS DOUBLE PRECISION))  FROM HourlyCountOEE WHERE  linea LIKE '$linea' AND anio = '$anio' AND mes = '$mes' AND dia >= '$varDiaI' AND dia <= '$varDiaF' ";
    return getArraySQL($sql);
}

function lossesDayCalPercent($linea, $anio, $mes,$varDiaI, $varDiaF){
    $sql = "SELECT sum(cast(replace(Calidad,'%','') AS DOUBLE PRECISION))  FROM HourlyCountOEE WHERE  linea LIKE '$linea' AND anio = '$anio' AND mes = '$mes' AND dia >= '$varDiaI' AND dia <= '$varDiaF' ";
    return getArraySQL($sql);
}

function lossesDayOrg($linea, $mes, $anio,$varDiaI, $varDiaF){
    $sql = "SELECT TOP 5 problema,detalleMaterial, sum(duracion) AS tmp FROM Bitacora WHERE tema in ('Organizacionales') AND linea LIKE '$linea' AND mes = '$mes' AND anio = '$anio' AND dia >= '$varDiaI' AND dia <= '$varDiaF' GROUP BY problema, detalleMaterial ORDER BY tmp DESC";
    return getArraySQL($sql);
}

function lossesDayOrgTotal($linea, $mes, $anio,$varDiaI, $varDiaF){
    $sql = "SELECT sum(duracion) FROM Bitacora WHERE tema = 'Organizacionales' AND linea LIKE '$linea' AND mes = '$mes' AND anio = '$anio' AND dia >= '$varDiaI' AND dia <= '$varDiaF'";
    return getArraySQL($sql);
}

//MES
function tiempoTotalMes ($linea, $fIni, $fFin){
    $sql = "SELECT sum(duracion) as Tiempo_Total FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin';";
    return getArraySQL($sql);
}

function tiempoTotalPerdias ($linea, $fechaI, $fechaF ){
    $sql = "SELECT sum(duracion) as Tiempo_Total FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' AND tema <> 'Piezas Producidas' ";
    return getArraySQL($sql);
}

//ORGANIZACIONALES DURACION
function t3OrgMesAreaLosses($linea, $anio, $mes) {
    $sql = "SELECT TOP 5 area, SUM(duracion) as tm, COUNT(problema) AS frec FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND anio = '$anio' AND mes = '$mes' GROUP BY area ORDER BY tm DESC;";
    return getArraySQL($sql);
}

function t3OrgMesAreaLossesFrec($linea, $anio, $mes) {
    $sql = "SELECT TOP 5 area, COUNT(problema) AS frec, SUM(duracion) as tm FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND anio = '$anio' AND mes = '$mes' GROUP BY area ORDER BY frec DESC;";
    return getArraySQL($sql);
}

function t3ProblemOrgMesLosses($linea, $anio, $mes, $area) {
    $sql = "SELECT TOP 5 problema, SUM(duracion) as tm, COUNT(problema) AS frec FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND area = '$area' AND anio = '$anio' AND mes = '$mes' GROUP BY problema ORDER BY tm DESC;";
    return getArraySQL($sql);
}

function t3ProblemOrgMesLossesFrec($linea, $anio, $mes) {
    $sql = "SELECT TOP 5 problema, COUNT(problema) AS frec, SUM(duracion) AS tm FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND area = '$area' AND anio = '$anio' AND mes = '$mes' GROUP BY problema ORDER BY frec DESC;";
    return getArraySQL($sql);
}

//TECNICAS
function t3TecMesOpLosses($linea, $anio, $mes) {
    $sql = "SELECT TOP 5 b.operacion,op.MA,op.descripcion, SUM(b.duracion) as tm, COUNT(b.problema) AS frec FROM bitacora as b, op as op "
         . "WHERE b.linea LIKE '$linea' AND b.tema IN('Tecnicas') AND b.anio = '$anio' AND b.mes = '$mes' AND op.operacion = b.operacion AND b.linea = op.linea GROUP BY b.operacion,op.MA,op.descripcion ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3TecMesopLossesFrec($linea, $anio, $mes) {
    $sql = "SELECT TOP 5 b.operacion,op.MA,op.descripcion, COUNT(b.problema) AS frec, SUM(b.duracion) as tm FROM bitacora as b, op as op "
         . "WHERE b.linea LIKE '$linea' AND b.tema IN('Tecnicas') AND b.anio = '$anio' AND b.mes = '$mes' AND op.operacion = b.operacion AND b.linea = op.linea GROUP BY b.operacion,op.MA,op.descripcion ORDER BY frec DESC";
    return getArraySQL($sql);
}

function t3ProblemTecMesLosses ($linea, $anio, $mes, $op ){
    $sql = "SELECT TOP 5 problema, sum(duracion) as dur, count(problema) as frec FROM Bitacora WHERE linea = '$linea' AND anio = '$anio' AND mes = '$mes' AND tema = 'Tecnicas' AND operacion = '$op' GROUP BY problema ORDER BY dur DESC";
    return getArraySQL($sql);
}

function t3ProblemTecMesLossesFrec ($line, $anio, $mes, $op ){
    $sql = "SELECT TOP 5 problema, count(problema) as frec, sum(duracion) as dur FROM Bitacora WHERE linea = '$linea' AND anio = '$anio' AND mes = '$mes' AND tema = 'Tecnicas' AND operacion = '$op' GROUP BY problema ORDER BY frec DESC";
    return getArraySQL($sql);
}

//CALIDAD
function t3CaliMesOpLosses($linea, $anio, $mes) {
    $sql = "SELECT TOP 5 b.operacion,op.MA,op.descripcion, SUM(b.duracion) as tm, COUNT(b.problema) AS frec FROM bitacora as b, op as op "
         . "WHERE b.linea LIKE '$linea' AND b.tema IN('Calidad') AND b.anio = '$anio' AND b.mes = '$mes' AND op.operacion = b.operacion AND b.linea = op.linea GROUP BY b.operacion,op.MA,op.descripcion ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3CaliMesOpLossesFrec($linea, $anio, $mes) {
    $sql = "SELECT TOP 5 b.operacion,op.MA,op.descripcion, COUNT(b.problema) AS frec, SUM(b.duracion) as tm FROM bitacora as b, op as op "
         . "WHERE b.linea LIKE '$linea' AND b.tema IN('Calidad') AND b.anio = '$anio' AND b.mes = '$mes' AND op.operacion = b.operacion AND b.linea = op.linea GROUP BY b.operacion,op.MA,op.descripcion ORDER BY frec DESC";
    return getArraySQL($sql);
}

function t3ProblemCaliMesLosses($linea, $anio, $mes, $op) {
    $sql = "SELECT TOP 5 problema, SUM(duracion) as tm, COUNT(problema) AS frec FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Calidad') AND operacion = '$op' AND anio = '$anio' AND mes = '$mes' GROUP BY problema ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3ProblemCaliMesLossesFrec($linea, $anio, $mes) {
    $sql = "SELECT TOP 5 problema, COUNT(problema) AS frec, SUM(duracion) as tm FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Calidad') AND operacion = '$op' AND anio = '$anio' AND mes = '$mes' GROUP BY problema ORDER BY frec DESC";
    return getArraySQL($sql);
}

//PERIOD
function tiempoTotalPeriodo ($linea, $fIni, $fFin){
    $sql = "SELECT sum(duracion) as Tiempo_Total FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' AND tema <> 'Piezas Producidas' ";
    return getArraySQL($sql);
}

//DIA
function tiempoTotalDia ($linea, $anio, $mes, $varDiaI, $varDiaF){
    $sql = "SELECT sum(duracion) as Tiempo_Total FROM Bitacora WHERE linea = '$linea' AND anio = '$anio' AND mes = '$mes' AND dia >= '$varDiaI' AND dia <= '$varDiaF'";
    return getArraySQL($sql);
}
//ORGANIZACIONALES DURACION
function t3OrgDiaAreaLosses($linea, $fechaI, $fechaF) {
    $sql = "SELECT TOP 10 area, SUM(duracion) as tm, COUNT (area) as frec FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY area ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3ProblemOrgDiaAreaLosses($linea, $fechaI, $fechaF, $area) {
    $sql = "SELECT TOP 10 problema, detallematerial, SUM(duracion) as tm, COUNT(problema) as frec FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND area = '$area' AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY problema, detallematerial ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3NoParteOrgDiaAreaLosses($linea, $fechaI, $fechaF, $area, $problema) {
    $sql = "SELECT TOP 5 noParte, SUM(duracion) as tm, COUNT(problema) as frec FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND area = '$area' AND fecha >= '$fechaI' AND fecha <= '$fechaF' AND problema LIKE '%$problema%' GROUP BY noParte ORDER BY tm DESC ";
    return getArraySQL($sql);
}

function t3OrgDiaAreaLossesFrec($linea, $fechaI, $fechaF) {
    $sql = "SELECT TOP 5 area, COUNT(problema) AS frec, SUM(duracion) as tm  FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY area ORDER BY frec DESC";
    return getArraySQL($sql);
}

function t3ProblemOrgDiaAreaLossesFrec($linea, $fechaI, $fechaF, $area) {
    $sql = "SELECT TOP 5 problema, detallematerial, COUNT(problema) AS frec, SUM(duracion) as tm  FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Organizacionales') AND area = '$area' AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY problema, detalleMaterial ORDER BY frec DESC";
    return getArraySQL($sql);
}

//TECNICAS
function t3TecDiaOpLosses($linea, $fechaI, $fechaF) {
    $sql = "SELECT TOP 10 b.operacion, op.MA, op.descripcion, SUM(b.duracion) as tm, COUNT(b.problema) AS frec FROM bitacora as b, op as op WHERE b.linea LIKE '$linea' AND b.tema IN('Tecnicas') AND b.fecha >= '$fechaI' AND b.fecha <= '$fechaF' AND op.operacion = b.operacion AND b.linea = op.linea GROUP BY b.operacion,op.MA,op.descripcion ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3ProblemTecDiaOpLosses($linea, $fechaI, $fechaF, $op) {
    $sql = "SELECT TOP 5 problema,sum(duracion) as dur,count(problema) as frec FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' and tema = 'Tecnicas' AND operacion = '$op' GROUP BY problema ORDER BY dur DESC";
    return getArraySQL($sql);
}

function tiempoTecOtrosLosses($linea, $fechaI, $fechaF, $op1, $op2, $op3, $op4, $op5) {
    $sql = "SELECT SUM(duracion) as tm FROM bitacora WHERE linea LIKE '%$linea%' AND tema IN('Tecnicas') AND operacion <> '$op1' AND operacion <> '$op2' AND operacion <> '$op3' AND operacion <> '$op4' AND operacion <> '$op5' AND fecha >= '$fechaI' AND fecha <= '$fechaF' ";
    return getArraySQL($sql);
}

function topProblemOtrosTecDiaOpLosses($linea, $fechaI, $fechaF, $op1, $op2, $op3, $op4, $op5) {
    $sql = "SELECT TOP 10 operacion, CONCAT(operacion,', ',problema), SUM(duracion) as tm, COUNT(problema) AS frec FROM bitacora WHERE linea LIKE '%$linea%' AND tema IN('Tecnicas') AND operacion <> '$op1' AND operacion <> '$op2' AND operacion <> '$op3' AND operacion <> '$op4' AND operacion <> '$op5' AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY operacion, problema ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3NoParteTecDiaOpLosses($linea, $fechaI, $fechaF, $op, $problema) {
    $sql = "SELECT TOP 5 noParte, sum(duracion) as dur, count(noParte) as frec FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' and tema = 'Tecnicas' AND operacion = '$op' AND problema LIKE '$problema' GROUP BY noParte ORDER BY dur DESC";
    return getArraySQL($sql);
}

function t3TecDiaOpLossesFrec($linea, $fechaI, $fechaF) {
    $sql = "SELECT TOP 5 b.operacion,op.MA,op.descripcion, COUNT(b.problema) AS frec, SUM(b.duracion) as tm FROM bitacora as b, op as op "
         . "WHERE b.linea LIKE '$linea' AND b.tema IN('Tecnicas') AND b.fecha >= '$fechaI' AND b.fecha <= '$fechaF' AND op.operacion = b.operacion AND b.linea = op.linea GROUP BY b.operacion,op.MA,op.descripcion ORDER BY frec DESC";
    return getArraySQL($sql);
}

function t3ProblemTecDiaOpLossesFrec($linea, $fechaI, $fechaF, $op) {
    $sql = "SELECT TOP 5 problema,count(problema) as frec, sum(duracion) as dur FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' and tema = 'Tecnicas' AND operacion = '$op' GROUP BY problema ORDER BY frec DESC";
    return getArraySQL($sql);
}

//CALIDAD
function t3CaliDiaOpLosses($linea, $fechaI, $fechaF) { 
    //$sql = "SELECT TOP 5 b.operacion, op.MA, op.descripcion, SUM(CAST(scrap AS int)) as tm, COUNT(b.problema) AS frec FROM bitacora as b, op as op WHERE b.linea LIKE '$linea' AND b.tema IN('Calidad') AND b.fecha >= '$fechaI' AND b.fecha <= '$fechaF' AND op.operacion = b.operacion AND b.linea = op.linea GROUP BY b.operacion,op.MA,op.descripcion ORDER BY tm DESC";
    $sql = "SELECT TOP 5 b.operacion, op.MA, op.descripcion, SUM(CAST(scrap AS int)) as tm, COUNT(b.problema) AS frec 
            FROM bitacora as b, op as op WHERE b.linea LIKE '$linea' AND b.fecha >= '$fechaI' AND b.fecha <= '$fechaF' AND tema NOT IN ('Piezas Producidas') AND scrap > 0 AND op.operacion = b.operacion AND b.linea = op.linea 
            GROUP BY b.operacion,op.MA,op.descripcion ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3CaliDiaOpLossesFrec($linea, $fechaI, $fechaF) {
    $sql = "SELECT TOP 5 b.operacion,op.MA,op.descripcion, COUNT(b.problema) AS frec, SUM(b.duracion) as tm FROM bitacora as b, op as op "
         . "WHERE b.linea LIKE '$linea' AND b.tema IN('Calidad') AND b.fecha >= '$fechaI' AND b.fecha <= '$fechaF' AND op.operacion = b.operacion AND b.linea = op.linea GROUP BY b.operacion,op.MA,op.descripcion ORDER BY frec DESC";
    return getArraySQL($sql);
}

function t3ProblemCaliDiaOpLosses($linea, $fechaI, $fechaF, $op) {
    $sql = "SELECT TOP 5 problema, SUM(CAST(scrap AS int)) as tm, COUNT(problema) AS frec FROM bitacora WHERE linea LIKE '%$linea%' AND tema IN('Calidad') AND operacion = '$op' AND fecha >= '$fechaI' AND fecha <= '$fechaF' 
            GROUP BY problema ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3NoParteCaliDiaOpLosses($linea, $fechaI, $fechaF, $op, $problema) {
    $sql = "SELECT TOP 5 noParte, SUM(CAST(scrap AS int)) as tm, COUNT(problema) AS frec FROM bitacora WHERE linea LIKE '%$linea%' AND tema IN('Calidad') AND operacion = '$op' AND problema LIKE '%$problema%' AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY noParte ORDER BY tm DESC";
    return getArraySQL($sql);
}

function t3ProblemCaliDiaOpLossesFrec($linea, $fechaI, $fechaF, $op) {
    $sql = "SELECT TOP 5 problema, COUNT(problema) AS frec, SUM(duracion) as tm FROM bitacora WHERE linea LIKE '$linea' AND tema IN('Calidad') AND operacion = '$op' AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY problema ORDER BY frec DESC";
    return getArraySQL($sql);
}

/* semanales */
function semanaTecnicas($linea, $anio,$mes) {
    $sql = "SELECT DATEPART(week,fecha) AS SEM, sum(duracion) as sum FROM Bitacora WHERE tema = 'Tecnicas' AND linea LIKE '$linea' AND anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql);
}

function semanaOrganizacionales($linea, $anio,$mes) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) AS SEM, sum(duracion) as sum FROM Bitacora WHERE tema = 'Organizacionales' AND linea LIKE '$linea' AND anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql);
}

function semanaPlaneados($linea, $anio,$mes) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) AS SEM, sum(duracion) as sum FROM Bitacora WHERE tema = 'Paros Planeados' AND linea LIKE '$linea' AND anio = '$anio' AND mes = '$mes' AND area <> 'Arranque de linea' AND area <> 'Juntas de trabajo planeado' AND area <> 'Junta Informativa' AND area <> 'Workshop planeado' AND area <> 'Comedor'  GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql);
}

function semanaCalidad($linea, $anio,$mes) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) AS SEM, sum(cast(scrap as int)) FROM Bitacora WHERE tema = 'Calidad' AND linea LIKE '$linea' AND anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql);
}

function semanaCambios($linea, $anio,$mes) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) AS SEM, sum(duracion) as sum FROM Bitacora WHERE tema = 'Cambio de Modelo' AND linea LIKE '$linea' AND anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql);
}
//Producidas
function semanaProducidas($linea, $anio,$mes) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) AS SEM, sum(cantPzas) as sum FROM Bitacora WHERE tema = 'Piezas Producidas' AND linea LIKE '$linea' AND anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql);
}
//entrega
function semanaEntrega($linea, $fIni, $fFin) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) AS SEM, sum(cantPzas) as sum FROM Bitacora WHERE tema = 'Piezas Producidas' AND linea LIKE '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql);
} 

function mesEntrega($linea, $anio) {
    $sql = "SELECT DATEPART(MONTH,fecha) AS mes, sum(cantPzas) as sum FROM Bitacora WHERE tema = 'Piezas Producidas' AND linea LIKE '$linea' AND anio = '$anio' GROUP BY DATEPART(MONTH,fecha) ORDER BY mes ASC";
    return getArraySQL($sql);
} 

//******************************************PRODUCTIVIDAD**********************/
//PIEZAS Y DURACION(MIN) DIARIA -> FECHA 
function productividadPzasDia ($linea, $fIni, $fFin){
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)) AS fech , SUM(cantPzas) as Pzas, SUM(duracion) as Duracion FROM Bitacora 
            WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY fecha ORDER BY fecha ASC";
    return getArraySQL($sql);
}
//HOMBRES Y TURNOS
function productividadHombresDia ($linea, $fIni, $fFin){ 
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)) AS fech, sum(NoPersonal) as hombres, count(Fecha) as turnos FROM TiempoTurno WHERE Linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin'  GROUP by fecha ORDER by fecha";
    return getArraySQL($sql);
}
//PRIEZAS Y DURACION (MIN) SEMANAL -> 
function productividadPzasSemana ($linea, $anio, $mes){
    $sql = " SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha) as sem,SUM(cantPzas) as Pzas, SUM(duracion) as Duracion FROM Bitacora "
            . "WHERE linea like '$linea' AND mes = '$mes' AND anio = '$anio' GROUP BY DATEPART(WEEK,fecha) order by sem asc;";
    return getArraySQL($sql);
}
function productividadHombresSemana ($linea,$fechaI,$fechaF){
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha) as semana,sum(NoPersonal) as hombres, count(Fecha) as turno FROM TiempoTurno 
    WHERE linea like '$linea' and fecha >= '$fechaI' and Fecha <= '$fechaF' AND turno <> 'Segundo Complementario' GROUP BY DATEPART(WEEK,fecha) ORDER BY semana asc";
    return getArraySQL($sql);
}
function productividadPzasMensual ($linea,$anio){
    $sql = "SELECT mes, SUM(cantPzas) as Pzas, SUM(duracion) as Duracion FROM Bitacora WHERE linea = '$linea' AND anio = '$anio' GROUP BY mes ORDER BY mes;";
    return getArraySQL($sql);
}
function productividadTurnoMensual($linea, $anio){
    $sql = "SELECT DATEPART(MONTH,FECHA), SUM(NoPersonal), COUNT(Linea) FROM TiempoTurno WHERE linea = '$linea' AND DATEPART(YEAR,FECHA) = '$anio' GROUP BY DATEPART(MONTH,FECHA) ORDER BY DATEPART(MONTH,FECHA);";
    return getArraySQL($sql);
}

//FUNCIONES PARA EOO AUTOMATICO
//DIA
function percentOEEMonthDay ($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SELECT dia, prod, tec, org, cal, cam, tDes FROM OEEPercentDay WHERE linea = '$linea' AND Anio = '$anio' AND Mes = '$mes' ORDER BY dia ASC";
    return getArraySQL($sql);
}

function percentOEERangeDays($linea, $fI, $fF){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), prod, tec, org, cal, cam, tDes FROM OEEPercentDay WHERE linea = '$linea' AND fecha >= '$fI' AND fecha <= '$fF' ORDER BY dia ASC";
    return getArraySQL($sql);
}

function piezasTCHourly ($linea, $fecha){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SELECT SUM(cantPzas) as pzas, tiempoCiclo as tc FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema = 'Piezas Producidas' GROUP BY tiempoCiclo";
    return getArraySQL($sql);
}

function piezasEsperadasyPercentHora($linea, $fecha){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SELECT hora, pzasEsp, prod FROM OEEPercentHour WHERE linea = '$linea' AND fecha = '$fecha' ORDER BY hora";
    return getArraySQL($sql);
}

function piezasTotalesDiaHourly ($linea, $fecha){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SELECT SUM(cantPzas) as pzas FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' ";
    return getArraySQL($sql);
}

function percentHourlyDay ($linea, $fecha){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SELECT prod, tec, org, cal, cam, tDes FROM OEEPercentDay WHERE linea = '$linea' AND fecha = '$fecha' ORDER BY dia ASC";
    return getArraySQL($sql);
}

function minDescHoraHourly ($linea, $fecha){
    $sql = "SELECT hora, SUM(duracion) as tPlan FROM Bitacora WHERE fecha = '$fecha' AND linea = '$linea' AND tema = 'Paros Planeados' GROUP BY hora ORDER BY hora ASC";
    return getArraySQL($sql);
} 

function pzasMes ($linea, $anio){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SELECT mes, sum(cantPzas) FROM Bitacora WHERE linea = '$linea' AND Anio = '$anio' GROUP BY Mes ORDER BY Mes ASC ";
    return getArraySQL($sql);
} 

//FRECUENCIA
function tablaJidokaNOPlaneadosF ($tema, $linea, $fI, $fF) { 
    //$sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), upper(problema), COUNT(problema) FROM bitacora WHERE linea = '$linea' AND tema LIKE '%$tema%' AND fecha >= '$fI' AND fecha <= '$fF' GROUP BY upper(problema),fecha ORDER BY upper(problema)";
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), CONCAT(b.operacion,' - MA ',o.MA), f.codigoCategoria, upper(b.problema), COUNT(problema), b.noParte 
FROM bitacora as b, factoresPerdidaOEE as f, op as o
WHERE b.linea = '$linea' AND o.linea = b.linea AND b.tema LIKE '%$tema%' AND f.categoria LIKE '%'+b.area+'%' AND b.fecha >= '$fI' AND b.fecha <= '$fF' AND o.operacion = b.operacion
GROUP BY upper(b.problema), CONCAT(b.operacion,' - MA ',o.MA), f.codigoCategoria, b.fecha, b.noParte ORDER BY upper(problema);";
    return getArraySQL($sql);
}

function tablaJidokaCalidadF ($linea, $fI, $fF) { 
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), upper(problema), COUNT(problema) FROM bitacora WHERE linea = '$linea' AND tema LIKE '%calidad%' AND fecha >= '$fI' AND fecha <= '$fF' GROUP BY upper(problema),fecha ORDER BY upper(problema)";
    return getArraySQL($sql);
}

function tablaJidokaPlaneadosF ($linea, $fI, $fF) {
    $sql = "SELECT upper(area), CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), COUNT(area) FROM bitacora WHERE linea = '$linea' AND tema = 'Paros Planeados' AND fecha >= '$fI' AND fecha <= '$fF' GROUP BY upper(area),fecha ORDER BY upper(area)";
    return getArraySQL($sql);
}

//SEMANA 
function minDescWeek($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SET DATEFIRST 1  SELECT DATEPART(WEEK,fecha), SUM(duracion) as tDesc FROM Bitacora WHERE linea='$linea' AND anio='$anio' AND tema = 'Paros Planeados' AND area = 'Comedor' OR area = 'Arranque de linea' AND linea='$linea' AND anio='$anio' OR area = 'Junta' AND linea='$linea' AND anio='$anio' OR area = 'Junta Informativa' AND linea='$linea' AND anio='$anio' OR area = 'Descanso' AND linea='$linea' AND anio='$anio' GROUP BY DATEPART(WEEK,fecha) ORDER BY DATEPART(WEEK,fecha) ASC";
    return getArraySQL($sql);
}

function minTotalesWeek($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), sum(duracion) as tTotal FROM Bitacora WHERE linea = '$linea' AND Anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(WEEK,fecha) ORDER BY DATEPART(WEEK,fecha) ASC";
    return getArraySQL($sql);
}

function minTecWeek($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%    
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), sum(duracion) as tTecM FROM Bitacora WHERE linea = '$linea' AND tema = 'Tecnicas' AND Anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(WEEK,fecha) ORDER BY DATEPART(WEEK,fecha) ASC ";
    return getArraySQL($sql);
}

function minOrgWeek($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), sum(duracion) as tOrgM FROM Bitacora WHERE linea = '$linea' AND tema = 'Organizacionales' AND Anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(WEEK,fecha) ORDER BY DATEPART(WEEK,fecha) ASC";
    return getArraySQL($sql);
}

function minCamWeek($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), sum(duracion) as tCamM FROM Bitacora WHERE linea = '$linea' AND tema = 'Cambio de Modelo' AND Anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(WEEK,fecha) ORDER BY DATEPART(WEEK,fecha) ASC";
    return getArraySQL($sql);
}

function minCalWeek($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), sum(duracion) as tCalM FROM Bitacora WHERE linea = '$linea' AND tema = 'Calidad' AND Anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(WEEK,fecha) ORDER BY DATEPART(WEEK,fecha) ASC";
    return getArraySQL($sql);
}

function minTFalWeek($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), sum(duracion) as tCalM FROM Bitacora WHERE linea = '$linea' AND tema = 'Tiempo Faltante' AND Anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(WEEK,fecha) ORDER BY DATEPART(WEEK,fecha) ASC";
    return getArraySQL($sql);
}

function pzasRealesWeek($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), sum(cantPzas) FROM Bitacora WHERE linea = '$linea' AND Anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(WEEK,fecha) ORDER BY DATEPART(WEEK,fecha) ASC ";
    return getArraySQL($sql);
}

function pzasEsperadasWeek($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), sum(pzasEsp) FROM OEEPercentHour  WHERE linea = '$linea' AND Anio = '$anio' AND mes = '$mes' GROUP BY DATEPART(WEEK,fecha) ORDER BY DATEPART(WEEK,fecha) ASC ";
    return getArraySQL($sql);
}

function cPercentTopicMonthly($linea, $anio){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    //echo "<br> * ", $linea,', ',  $anio;
    $sql = "SELECT mes, AVG(prod) prod, AVG(tec) tec, AVG(org) org, AVG(cal) cal, AVG(cam) camb, AVG(tDes) des FROM OEEPercentDay WHERE linea = '$linea' AND anio = '$anio' AND (tec <> 0 OR prod <> 0 OR org <> 0) GROUP BY mes";
    return getArraySQL($sql);
} 

function cPercentTopicMonth($linea, $anio, $mes){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SELECT mes, AVG(prod) prod, AVG(tec) tec, AVG(org) org, AVG(cal) cal, AVG(cam) camb, AVG(tDes) des FROM OEEPercentDay WHERE linea = '$linea' AND anio = '$anio' AND mes = '$mes' AND (tec <> 0 OR prod <> 0 OR org <> 0) GROUP BY mes";
    return getArraySQL($sql);
}

function cPercentTopicWekly($linea, $fIni, $fFin){ //PROBABLEMENTE ESTO NO SIRVA POR EL TIPO DE GRAFICA QUE TENEMOS :) AJUSTA EL 100%
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), AVG(prod) prod, AVG(tec) tec, AVG(org) org, AVG(cal) cal, AVG(cam) camb, AVG(tDes) des  FROM OEEPercentDay  
        WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' AND (tec <> 0 OR prod <> 0 OR org <> 0) GROUP BY DATEPART(WEEK,fecha)";
    return getArraySQL($sql);
}

//OEE
function tcPonderadoProd ($linea, $fecha){
   $sql = "SELECT SUM(cantPzas * tiempoCiclo)/SUM(cantPzas) FROM Bitacora WHERE fecha = '$fecha' AND linea = '$linea'"; 
   return getArraySQL($sql);
}

function tcPonderadoNOProd ($linea, $fecha){
   $sql = "SELECT SUM(1 * tiempoCiclo)/COUNT(tiempoCiclo) FROM Bitacora WHERE fecha = '$fecha' AND linea = '$linea'"; 
   return getArraySQL($sql);
}

//TURNOS EN EL DIA
function hourlyDiaTurnos($linea, $fecha ){
    $sql = "SELECT linea, HoraInicio, HoraFin, Turno FROM TiempoTurno WHERE linea = '$linea' AND Fecha = '$fecha' ORDER BY horaInicio ASC";
    return getArraySQL($sql);
}

function hourlyTurnos($linea, $fecha ){
    $sql = "SELECT linea, HoraInicio, HoraFin, Turno FROM TiempoTurno WHERE linea = '$linea' AND Fecha = '$fecha' ORDER BY Turno ASC";
    return getArraySQL($sql);
}

//HOURLY
function hourlyDiaPzasProd ($linea, $fecha ){
    $sql = "SELECT hora, sum(cantPzas), noParte,tiempoCiclo FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema = 'Piezas Producidas' GROUP BY hora, cantPzas, noParte,tiempoCiclo ORDER BY hora";
    return getArraySQL($sql);
}
function hourlyTCPonderadoHGeneral ($linea, $fecha, $hora){
    $sql = "SELECT hora, SUM(cantPzas * tiempoCiclo)/SUM(cantPzas) as tcPonderado FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND hora = '$hora' GROUP BY hora ORDER BY hora ASC";
    return getArraySQL($sql);
}

function hourlyTCPonderadoFallaHGeneral ($linea, $fecha, $hora ){
    $sql = "SELECT hora, SUM(1 * tiempoCiclo)/COUNT(tiempoCiclo) as tcPonderadoFalla FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND hora = '$hora' GROUP BY hora ORDER BY hora ASC";
    return getArraySQL($sql);
}

function hourlyNoParteTC($linea, $fecha){
    $sql = "SELECT hora, noParte, tiempoCiclo FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' GROUP BY hora, noParte, tiempoCiclo ORDER BY hora ASC ";
    return getArraySQL($sql);
}

function hourlyDiaPzasCalidad ($linea, $fecha ){
    $sql = "SELECT hora, SUM(cast(scrap AS INT)) as scrap FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' GROUP BY hora ORDER BY hora";
    return getArraySQL($sql);
}

/** FUNCIONES PARA LOS PROBLEMAS PRIMARIOS **/
function hourlyDiaDurCalidad ($linea, $fecha ){
    $sql = "SELECT hora, problema, operacion, sum(duracion) as duracion FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema LIKE 'Calidad' AND iReg <> 'X' OR iReg = null GROUP BY hora,problema,operacion ORDER BY hora";
    return getArraySQL($sql);
}

function hourlyDiaDurTecnicas ($linea, $fecha ){
    $sql = "SELECT  hora,problema,operacion, sum(duracion) as duracion FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema LIKE 'Tecnicas' AND iReg <> 'X' GROUP BY hora,problema,operacion ORDER BY hora";
    return getArraySQL($sql);
}

function hourlyDiaDurCambios ($linea, $fecha ){
    $sql = "SELECT hora, problema, operacion, noParte, noParteCambio, detalleMaterial, sum(duracion) as duracion FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema LIKE 'Cambio de Modelo' AND ireg <> 'X' GROUP BY hora, problema, operacion, noParte, noParteCambio, detalleMaterial ORDER BY hora";
    return getArraySQL($sql);
}

function hourlyDiaOrg ($linea, $fecha ){
    $sql = "SELECT hora, problema,operacion, sum(duracion) as duracion FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema = 'Organizacionales' AND iReg <> 'X' GROUP BY hora, problema,operacion ORDER BY hora";
    return getArraySQL($sql);
}

function hourlyDiaDesempenio($linea, $fecha){
    $sql = "SELECT hora, sum(duracion) as duracion FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema = 'Tiempo Faltante' GROUP BY hora ORDER BY hora";
    return getArraySQL($sql);
}

/** FUNCIONES PARA LOS PROBLEMAS SECUANDARIOS **/
function cHourlysPercent ($linea, $fecha) {
    $sql = "SELECT prod,tec,org,cal,cam,tDes FROM OEEPercentDay WHERE linea = '$linea' AND fecha = '$fecha' ";
    return getArraySQL($sql); 
}
function hourlyDiaDurCalidadSec ($linea, $fecha ){
    $sql = "SELECT hora, problema, operacion, sum(duracion) as duracion FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema LIKE 'Calidad' AND iReg = 'X' GROUP BY hora,problema,operacion ORDER BY hora";
    return getArraySQL($sql);
}

function hourlyDiaDurTecnicasSec ($linea, $fecha ){
    $sql = "SELECT  hora,problema,operacion, sum(duracion) as duracion FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema LIKE 'Tecnicas' AND iReg = 'X' GROUP BY hora,problema,operacion ORDER BY hora";
    return getArraySQL($sql);
}

function hourlyDiaDurCambiosSec ($linea, $fecha ){
    $sql = "SELECT hora, problema, operacion, noParte, noParteCambio, detalleMaterial, sum(duracion) as duracion FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema LIKE 'Cambio de Modelo' AND ireg = 'X' GROUP BY hora, problema, operacion, noParte, noParteCambio, detalleMaterial ORDER BY hora";
    return getArraySQL($sql);
}

function hourlyDiaOrgSec ($linea, $fecha ){
    $sql = "SELECT hora, problema,operacion, sum(duracion) as duracion FROM Bitacora WHERE linea = '$linea' AND fecha = '$fecha' AND tema = 'Organizacionales' AND iReg = 'X' GROUP BY hora, problema,operacion ORDER BY hora";
    return getArraySQL($sql);
}

//CONSULTAS PARA REPORTES 
//BTS 
//function consultaBTS ($linea, $anio, $mes, $dia){
//    $sql = "SELECT noParte,volumen,secuencia,noParteCambio,volCambio,secCambio,descCambio,secProdReal,volProducido,volCalificacion,secCalificacion,evalLote,noParteCambioNP,volCambioNP,secCambioNP,descCambioNP "
//            . "FROM bts WHERE linea = '$linea' AND anio = '$anio' AND Mes = '$mes' AND Dia = '$dia' ORDER BY secuencia ASC";
//    return getArraySQL($sql);
//}

//OPL
//function consultaOPL($linea){
//    $sql = "SELECT * FROM opl WHERE linea ='$linea' AND estado <> 4 ";
//    return getArraySQL($sql);
//}

function pzasProducidasDia($linea, $anio, $mes, $varDiaI, $varDiaF){
    $sql = "SELECT l.cliente,SUM(b.cantPzas) FROM Bitacora AS b, ListaNumPartes as l WHERE b.linea = '$linea' And l.LineaProduce = '$linea' AND b.noParte = l.noParte AND b.anio = '$anio' AND mes = '$mes' AND dia >= '$varDiaI' AND dia <= '$varDiaF' AND b.tema = 'Piezas Producidas' GROUP BY l.cliente ORDER BY l.cliente" ; 
    return getArraySQL($sql);
}

function clientePzasProdDia ($linea, $noParte){
    $sql = "SELECT cliente FROM ListaNumPartes WHERE lineaProduce LIKE '$linea' AND noParte = '$noParte'" ; 
    return getArraySQL($sql);
}

//QUERYES PARA SCRAP
function centroCostoLinea($linea){
    $sql = "SELECT costCenter FROM linea_CostCenter WHERE linea = '$linea'"; 
    return getArraySQL($sql);
}

function scrapMiscellaneousDailyProd($costCenter){
    $sql = "SELECT CONVERT(varchar(15), CONCAT(year(docDate),'-',month(docDate),'-',day(docDate))), sum(valueCompanyCode) FROM MiscellaneousDaily WHERE costCenter LIKE '%$costCenter%' GROUP BY docDate ORDER BY docDate";
    return getArraySQL($sql);
}

/**** IFK MENSUAL (LINEA) ***/ 
function cIFKMonthlyProd($costCenter, $anio ) { 
    $sql = "SELECT MONTH(docDate), sum(valueCompanyCode) FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' GROUP BY MONTH(docDate) ORDER BY MONTH(docDate)";
    return getArraySQL($sql); 
} 

function cIFKOperationMonthlyProd($costCenter, $anio, $mes ) { 
    $sql = "SELECT operation, sum(valueCompanyCode) FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes' GROUP BY operation ORDER BY operation";
    return getArraySQL($sql); 
} 

function cIFKCodeOperationMonthlyProd($costCenter, $anio, $mes, $operation ) { 
    $sql = "SELECT docHeader, sum(valueCompanyCode) FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes' AND operation = '$operation' GROUP BY docHeader ORDER BY docHeader";
    return getArraySQL($sql); 
} 

/***** IFK SEMANAL (LINEA) ****/
function cIFKWeeklyProd($costCenter, $fIni, $fFin ) { 
    $sql = "SELECT DATEPART(week,docdate) AS SEM, sum(valueCompanyCode) FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY DATEPART(week,docdate) ORDER BY DATEPART(week,docdate)";
    return getArraySQL($sql); 
} 

function cIFKOperationWeeklyProd($costCenter, $anio, $semana ) { 
    $sql = "SELECT operation, sum(valueCompanyCode) FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' AND DATEPART(week,docdate) = '$semana' GROUP BY operation ORDER BY operation";
    return getArraySQL($sql); 
} 

function cIFKCodeOperationWeeklyProd($costCenter, $anio, $semana, $operation ) { 
    $sql = "SELECT docHeader, sum(valueCompanyCode) FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio'  AND DATEPART(week,docdate) = '$semana' AND operation = '$operation' GROUP BY docHeader ORDER BY docHeader";
    return getArraySQL($sql); 
} 

/**** IFK DIARIO (LINEA) ***/ 
function cIFKDailyProd($costCenter, $fIni, $fFin ) { 
    $sql = "SELECT CONVERT(varchar(15), docDate), sum(valueCompanyCode) FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY docDate ORDER BY docDate";
    return getArraySQL($sql); 
} 

function cIFKOperationDailyProd($costCenter, $day ) { 
    $sql = "SELECT operation, sum(valueCompanyCode) FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND docDate = '$day' GROUP BY operation ORDER BY operation";
    return getArraySQL($sql); 
} 

function cIFKCodeOperationDailyProd($costCenter, $day, $operation ) {  
    $sql = "SELECT docHeader, sum(valueCompanyCode) FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND docDate = '$day' AND operation = '$operation' GROUP BY docHeader ORDER BY docHeader";
    return getArraySQL($sql); 
}

/***** IFK MENSUAL (CFA) *************/
function cIFKMonthlyCFA($costCenter, $anio ) { 
    $sql = "SELECT MONTH(docDate), sum(valueCompanyCode) FROM IfkDaily WHERE profitCenter IN (
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' GROUP BY profitCenter
            ) AND YEAR(docDate) = '$anio' GROUP BY MONTH(docDate) ORDER BY MONTH(docDate)";
    return getArraySQL($sql); 
} 

function cIFKCostMonthlyCFA($costCenter, $anio, $mes ) { 
    $sql = "SELECT costCenter, sum(valueCompanyCode) FROM IfkDaily WHERE profitCenter IN ( 
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes'  GROUP BY profitCenter
            ) AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes' GROUP BY costCenter ORDER BY costCenter"; 
    return getArraySQL($sql); 
} 

function cIFKOperationMonthlyCFA($costCenter, $cost, $anio, $mes ) { 
    $sql = "SELECT operation, sum(valueCompanyCode) FROM IfkDaily WHERE profitCenter IN ( 
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes' GROUP BY profitCenter 
            ) AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes' AND costCenter = '$cost' GROUP BY operation ORDER BY operation"; 
    return getArraySQL($sql); 
} 

/***** IFK SEMANAL (CFA) ****/
function cIFKWeeklyCFA($costCenter, $fIni, $fFin ) {  
    $sql = "SELECT DATEPART(week,docdate), sum(valueCompanyCode) FROM IfkDaily WHERE profitCenter IN (
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' GROUP BY profitCenter
            ) AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY DATEPART(week,docdate) ORDER BY DATEPART(week,docdate)";
    return getArraySQL($sql); 
} 

function cIFKCostWeeklyCFA($costCenter, $anio, $semana ) { 
    $sql = "SELECT costCenter, sum(valueCompanyCode) FROM IfkDaily WHERE profitCenter IN ( 
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' GROUP BY profitCenter 
            ) AND YEAR(docDate) = '$anio' AND DATEPART(week,docdate) = '$semana' GROUP BY costCenter ORDER BY costCenter";
    return getArraySQL($sql); 
} 

function cIFKOperationWeeklyCFA($costCenter, $cost, $anio, $semana ) { 
    $sql = "SELECT operation, sum(valueCompanyCode) FROM IfkDaily WHERE profitCenter IN ( 
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' GROUP BY profitCenter 
            ) AND costCenter = '$cost' AND YEAR(docDate) = '$anio' AND DATEPART(week,docdate) = '$semana' GROUP BY operation ORDER BY operation ";
    return getArraySQL($sql); 
} 

/**** IFK DIARIO (CFA) ***/ 
function cIFKDailyCFA($costCenter, $fIni, $fFin ){ 
    $sql = "SELECT CONVERT(varchar(15), docDate), sum(valueCompanyCode) FROM IfkDaily WHERE profitCenter IN ( 
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' GROUP BY profitCenter 
            ) AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY docDate ORDER BY docDate";
    return getArraySQL($sql); 
} 

function cIFKCostDailyCFA($costCenter, $fIni, $fFin, $day ){ 
    $sql = "SELECT costCenter, sum(valueCompanyCode) FROM IfkDaily WHERE profitCenter IN ( 
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' GROUP BY profitCenter 
            ) AND docDate = '$day' GROUP BY costCenter ORDER BY costCenter";
    return getArraySQL($sql); 
} 

function cIFKOperationDailyCFA($costCenter, $fIni, $fFin, $cost, $day ){ 
    $sql = "SELECT operation, sum(valueCompanyCode) FROM IfkDaily WHERE profitCenter IN ( 
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' GROUP BY profitCenter 
            ) AND costCenter = '$cost' AND docDate = '$day' GROUP BY operation ORDER BY operation"; 
    return getArraySQL($sql); 
}

/********************/
# MISCELLANEOUS MES
function cMiscellaneousMonthly($profitCenter, $anio ) {     
    //$sql = "SELECT MONTH(docDate), sum(valueCompanyCode) FROM MiscellaneousDaily WHERE profitCenter LIKE '%$profitCenter%' AND YEAR(docDate) = '$anio' GROUP BY MONTH(docDate) ORDER BY MONTH(docDate)";
    $sql = "SELECT MONTH(docDate), sum(valueCompanyCode) FROM MiscellaneousDaily WHERE costCenter LIKE '%$profitCenter%' AND YEAR(docDate) = '$anio' GROUP BY MONTH(docDate) ORDER BY MONTH(docDate)";
    return getArraySQL($sql); 
} 

function cCostCenterMiscMonthly($profitCenter, $anio, $mes ) { 
    //$sql = "SELECT CONVERT(varchar(18),costCenter), sum(valueCompanyCode) FROM MiscellaneousDaily WHERE profitCenter LIKE '%$profitCenter%' AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes' GROUP BY costCenter ORDER BY costCenter";
    $sql = "SELECT CONVERT(varchar(18),costCenter), sum(valueCompanyCode) FROM MiscellaneousDaily WHERE costCenter LIKE '%$profitCenter%' AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes' GROUP BY costCenter ORDER BY costCenter";
    return getArraySQL($sql); 
} 

function cReasonMiscMonthly($profitCenter, $costCenter, $anio, $mes ) { 
    //$sql = "SELECT reasonAdd, sum(valueCompanyCode) FROM MiscellaneousDaily WHERE profitCenter LIKE '%$profitCenter%' AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes' AND costCenter = '$costCenter' GROUP BY reasonAdd ORDER BY reasonAdd";
    $sql = "SELECT reasonAdd, sum(valueCompanyCode) FROM MiscellaneousDaily WHERE costCenter LIKE '%$profitCenter%' AND YEAR(docDate) = '$anio' AND MONTH(docDate) = '$mes' AND costCenter = '$costCenter' GROUP BY reasonAdd ORDER BY reasonAdd";
    return getArraySQL($sql); 
}

# MISCELLANEOUS SEMANA
function cMiscellaneousWeekly ($profitCenter, $fIni, $fFin ){ 
    //$sql = "SELECT DATEPART(week,docdate), sum(valueCompanyCode) FROM MiscellaneousDaily WHERE profitCenter LIKE '%$profitCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY DATEPART(week,docdate) ORDER BY DATEPART(week,docdate)";
    $sql = "SELECT DATEPART(week,docdate), sum(valueCompanyCode) FROM MiscellaneousDaily WHERE costCenter LIKE '%$profitCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY DATEPART(week,docdate) ORDER BY DATEPART(week,docdate)";
    return getArraySQL($sql); 
} 

function  cCostCenterMiscWeekly ($profitCenter, $anio, $semana ){ 
    //$sql = "SELECT costCenter, sum(valueCompanyCode) FROM MiscellaneousDaily WHERE profitCenter LIKE '%$profitCenter%' AND YEAR(docDate) = '$anio' AND DATEPART(week,docdate) = '$semana' GROUP BY costCenter ORDER BY costCenter";
    $sql = "SELECT costCenter, sum(valueCompanyCode) FROM MiscellaneousDaily WHERE costCenter LIKE '%$profitCenter%' AND YEAR(docDate) = '$anio' AND DATEPART(week,docdate) = '$semana' GROUP BY costCenter ORDER BY costCenter";
    return getArraySQL($sql); 
} 

function cReasonMiscWeekly ($profitCenter, $costCenter, $anio, $semana ){ 
    $sql = "SELECT reasonAdd, sum(valueCompanyCode) FROM MiscellaneousDaily WHERE profitCenter LIKE '%$profitCenter%' AND costCenter = '$costCenter' AND YEAR(docDate) = '$anio'  AND DATEPART(week,docdate) = '$semana' GROUP BY reasonAdd ORDER BY reasonAdd";
    return getArraySQL($sql); 
}

# MISCELLANEOUS DIA
function cMiscellaneousDaily ($profitCenter, $fIni, $fFin ){ 
    //$sql = "SELECT CONVERT(varchar(15), docDate), sum(valueCompanyCode) FROM MiscellaneousDaily WHERE profitCenter LIKE '%$profitCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY docDate ORDER BY docDate";
    $sql = "SELECT CONVERT(varchar(15), docDate), sum(valueCompanyCode) FROM MiscellaneousDaily WHERE costCenter LIKE '%$profitCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY docDate ORDER BY docDate";
    return getArraySQL($sql); 
} 

function  cCostCenterMiscDaily($profitCenter, $day ){ 
    //$sql = "SELECT costCenter, sum(valueCompanyCode) FROM MiscellaneousDaily WHERE profitCenter LIKE '%$profitCenter%' AND docDate = '$day' GROUP BY costCenter ORDER BY costCenter";
    $sql = "SELECT costCenter, sum(valueCompanyCode) FROM MiscellaneousDaily WHERE costCenter LIKE '%$profitCenter%' AND docDate = '$day' GROUP BY costCenter ORDER BY costCenter";
    return getArraySQL($sql); 
} 

function cReasonMiscDaily($costCenter, $day ){ 
    //$sql = "SELECT reasonAdd, sum(valueCompanyCode) FROM MiscellaneousDaily WHERE profitCenter LIKE '%$profitCenter%' AND costCenter = '$costCenter' AND docDate = '$day' GROUP BY reasonAdd ORDER BY reasonAdd";
    $sql = "SELECT reasonAdd, sum(valueCompanyCode) FROM MiscellaneousDaily WHERE costCenter = '$costCenter' AND docDate = '$day' GROUP BY reasonAdd ORDER BY reasonAdd";
    return getArraySQL($sql); 
}

#CONSULTAS PARA GRAFICA GENERAL
#MENSUAL
function  cIFKProfMonthG($costCenter, $anio ){ 
    $sql = "SELECT MONTH(docDate), sum(valueCompanyCode) AS valIFK FROM IfkDaily WHERE profitCenter IN (
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' GROUP BY profitCenter
            ) AND YEAR(docDate) = '$anio' GROUP BY MONTH(docDate) ORDER BY MONTH(docDate)";
    return getArraySQL($sql); 
} 

function cMiscProfMonthG($costCenter, $anio){ 
    $sql = "SELECT MONTH(docDate), sum(valueCompanyCode) AS valMISC FROM MiscellaneousDaily WHERE profitCenter IN (
                SELECT profitCenter FROM MiscellaneousDaily 
                WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' GROUP BY profitCenter 
            ) AND YEAR(docDate) = '$anio' GROUP BY MONTH(docDate) ORDER BY MONTH(docDate)";
    return getArraySQL($sql); 
}

function  cIFKCostMonthG($costCenter, $anio ){ 
    $sql = "SELECT MONTH(docDate), sum(valueCompanyCode) AS valIFK FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' GROUP BY MONTH(docDate) ORDER BY MONTH(docDate)";
    return getArraySQL($sql); 
} 

function cMiscCostMonthG($costCenter, $anio){ 
    $sql = "SELECT MONTH(docDate), sum(valueCompanyCode) AS valMISC FROM MiscellaneousDaily WHERE costCenter LIKE '%$costCenter%' AND YEAR(docDate) = '$anio' GROUP BY MONTH(docDate) ORDER BY MONTH(docDate)";
    return getArraySQL($sql); 
}

#SEMANAL
function cMiscProfWeekG($costCenter, $fIni, $fFin ){  
    $sql = "SELECT DATEPART(week,docdate), sum(valueCompanyCode) AS valMISC FROM MiscellaneousDaily WHERE profitCenter IN ( 
                SELECT profitCenter FROM MiscellaneousDaily 
                WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' 
                GROUP BY profitCenter 
            ) AND docDate >= '$fIni' AND docDate <= '$fFin' 
            GROUP BY DATEPART(week,docdate) ORDER BY DATEPART(week,docdate)";
    return getArraySQL($sql); 
} 

function  cIFKProfWeekG($costCenter, $fIni, $fFin ){
    $sql = "SELECT DATEPART(week,docdate), sum(valueCompanyCode) AS valIFK FROM IfkDaily WHERE profitCenter IN ( 
                    SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY profitCenter
            ) AND docDate >= '$fIni' AND docDate <= '$fFin' 
            GROUP BY DATEPART(week,docdate) ORDER BY DATEPART(week,docdate)"; 
    return getArraySQL($sql); 
} 

function cMiscCostWeekG($costCenter, $fIni, $fFin){
    $sql = "SELECT DATEPART(week,docdate), sum(valueCompanyCode) AS valMISC FROM MiscellaneousDaily WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY DATEPART(week,docdate) ORDER BY DATEPART(week,docdate)";
    return getArraySQL($sql); 
} 

function  cIFKCostWeekG($costCenter, $fIni, $fFin ){ 
    $sql = "SELECT DATEPART(week,docdate), sum(valueCompanyCode) AS valIFK FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY DATEPART(week,docdate) ORDER BY DATEPART(week,docdate)";
    return getArraySQL($sql); 
} 

#DIARIA
function  cIFKProfDayG($costCenter, $fIni, $fFin ){ 
    $sql = "SELECT CONVERT(varchar(15), docDate), sum(valueCompanyCode) AS valIFK FROM IfkDaily WHERE profitCenter IN (
                SELECT profitCenter FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin'  GROUP BY profitCenter
            ) AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY docDate ORDER BY docDate";
    return getArraySQL($sql); 
} 

function cMiscProfDayG($costCenter, $fIni, $fFin ){ 
    $sql = "SELECT CONVERT(varchar(15), docDate), sum(valueCompanyCode) AS valMISC FROM MiscellaneousDaily WHERE profitCenter IN ( 
                SELECT profitCenter FROM MiscellaneousDaily 
                WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY profitCenter 
            ) AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY docDate ORDER BY docDate";
    return getArraySQL($sql); 
} 

function  cIFKCostDayG($costCenter, $fIni, $fFin ){ 
    $sql = "SELECT CONVERT(varchar(15), docDate), sum(valueCompanyCode) AS valIFK FROM IfkDaily WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY docDate ORDER BY docDate";
    return getArraySQL($sql); 
} 

function cMiscCostDayG($costCenter, $fIni, $fFin){ 
    $sql = "SELECT CONVERT(varchar(15), docDate), sum(valueCompanyCode) AS valMISC FROM MiscellaneousDaily WHERE costCenter LIKE '%$costCenter%' AND docDate >= '$fIni' AND docDate <= '$fFin' GROUP BY docDate ORDER BY docDate";
    return getArraySQL($sql); 
} 

// QUERRYS PARA LAS CONSULTAS NUEVAS DE LOS JIDOKA
function jidokaDia ($tema, $linea, $anio) {
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), sum(duracion) FROM bitacora WHERE linea = '$linea' AND tema LIKE '%$tema%' AND anio = '$anio' GROUP BY fecha ORDER BY fecha ";
    return getArraySQL($sql);
}

function jidokaDiaCambios ( $linea, $anio) {
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), SUM(duracion) FROM Bitacora WHERE anio = '$anio' AND linea = '$linea' AND tema LIKE '%cambio de modelo%' GROUP BY fecha, duracion ORDER BY fecha ASC";
    return getArraySQL($sql);
}

function jidokaDiaCalidad ($linea, $anio) {
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), SUM(cast(scrap AS INTEGER)) FROM bitacora WHERE linea = '$linea' AND tema LIKE '%calidad%' AND anio = '$anio' GROUP BY fecha ORDER BY fecha ";
    return getArraySQL($sql);
}

function jidokaSemana ($tema, $linea, $fechaI, $fechaF) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) AS SEM, sum(duracion) as sum FROM Bitacora WHERE tema LIKE '%$tema%' AND linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql);
}

function jidokaSemanaCalidad ($linea, $fechaI, $fechaF) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) AS SEM, SUM(cast(scrap AS INTEGER)) FROM Bitacora WHERE tema LIKE '%calidad%' AND linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql);
}

function jidokaMonth ($tema, $linea, $anio) {
    $sql = "SELECT mes, sum(duracion) as tTecM FROM Bitacora WHERE linea = '$linea' AND tema LIKE '%$tema%' and Anio = '$anio' GROUP BY Mes ORDER BY Mes ASC";
    return getArraySQL($sql);
}

function jidokaMonthCalidad ($linea, $anio) {
    $sql = "SELECT mes, SUM(cast(scrap AS INTEGER)) FROM Bitacora WHERE linea = '$linea' AND tema LIKE '%calidad%' and Anio = '$anio' GROUP BY Mes ORDER BY Mes ASC";
    return getArraySQL($sql);
}

//JIDOKAS PARA FRECUENCIA
function jidokaDiaF ($tema, $linea, $anio) {
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), count(problema) FROM bitacora WHERE linea = '$linea' AND tema LIKE '%$tema%' AND anio = '$anio' GROUP BY fecha ORDER BY fecha ";
    return getArraySQL($sql);
}

function jidokaSemanaF ($tema, $linea, $fechaI, $fechaF) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) AS SEM, COUNT(DISTINCT upper(problema)) FROM Bitacora WHERE tema LIKE '%$tema%' AND linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' GROUP BY DATEPART(week,fecha) ORDER BY SEM ASC";
    return getArraySQL($sql); 
}

function jidokaMonthF ($tema, $linea, $anio) {
    $sql = "SELECT mes, COUNT(upper(problema)) FROM Bitacora WHERE linea = '$linea' AND tema LIKE '%$tema%' and Anio = '$anio' GROUP BY Mes ORDER BY Mes ASC";
    return getArraySQL($sql);
}

// TABLAS DE LOS JIDOKAS 
function tablaJidokaNOPlaneados ($tema, $linea, $fI, $fF) {
    //$sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), upper(problema), sum(duracion), noParte FROM bitacora WHERE linea = '$linea' AND tema LIKE '%$tema%' AND fecha >= '$fI' AND fecha <= '$fF' GROUP BY upper(problema), fecha, noParte ORDER BY upper(problema)";
    $sql = "SELECT CAST(CONVERT(date, b.fecha, 103) as VARCHAR(15)),CONCAT(b.operacion,' - MA ',o.MA), f.codigoCategoria, upper(b.problema), sum(b.duracion), b.noParte 
            FROM bitacora as b, factoresPerdidaOEE as f, op as o
            WHERE b.linea = '$linea' AND o.linea = b.linea AND b.tema LIKE '%$tema%' AND f.categoria IN(''+b.area+'') AND b.fecha >= '$fI' AND b.fecha <= '$fF' AND o.operacion = b.operacion
            GROUP BY upper(b.problema), CONCAT(b.operacion,' - MA ',o.MA), f.codigoCategoria, b.fecha, b.noParte ORDER BY upper(b.problema) ";
    return getArraySQL($sql);
}

function tablaJidokaCalidad ( $linea, $fI, $fF) {
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), upper(problema), SUM(cast(scrap AS INTEGER)), noParte FROM bitacora WHERE linea = '$linea' AND tema LIKE '%calidad%' AND fecha >= '$fI' AND fecha <= '$fF' GROUP BY upper(problema), fecha, noParte ORDER BY upper(problema)";
    return getArraySQL($sql);
}

function tablaProblemasJidokaCambios ($linea, $fI, $fF) {
    $sql = "SELECT CONCAT(noParte,'-',noParteCambio) FROM Bitacora WHERE linea = '$linea' AND fecha >= '$fI' AND  fecha <= '$fF' AND tema LIKE '%cambio de modelo%' GROUP BY noParte, noParteCambio";
    return getArraySQL($sql);
}

function tablaJidokaCambios ( $linea, $fI, $fF) {
    $sql = "SELECT CONCAT(noParte,'-',noParteCambio), CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), SUM(duracion), noParte FROM Bitacora WHERE fecha >= '$fI' AND linea = '$linea' AND fecha <= '$fF' AND tema LIKE '%cambio de modelo%' GROUP BY CONCAT(noParte,'-',noParteCambio), fecha, duracion, noParte";
    return getArraySQL($sql);
}

function tablaJidokaCambiosF ( $linea, $fI, $fF) { 
    $sql = "SELECT CONCAT(noParte,'-',noParteCambio), CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), COUNT(problema), noParte FROM Bitacora 
WHERE fecha >= '$fI' AND linea = '$linea' AND fecha <= '$fF' AND tema LIKE '%cambio de modelo%' GROUP BY CONCAT(noParte,'-',noParteCambio), fecha, problema, noParte";
    return getArraySQL($sql);
}

function tablaJidokaPlaneados ($linea, $fI, $fF) {
    $sql = "SELECT upper(area), CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), sum(duracion), noParte FROM bitacora WHERE linea = '$linea' AND tema = 'Paros Planeados' AND fecha >= '$fI' AND fecha <= '$fF' GROUP BY upper(area), fecha, noParte ORDER BY upper(area)";
    return getArraySQL($sql);
}

//CONSULTA DE TIEMPO CICLO POR LINEA Y NUMERO DE PARTE
function tcLineaNoParte ($linea, $noParte ) {
    $sql = "SELECT tcPromedio FROM tcNumParte WHERE linea = '$linea' AND numParte = '$noParte' ";
    return getArraySQL($sql);
}

//CONSUTLAS PARA ENTREGAS
function jidokaEntregasDia($linea, $anio){
    $sql = "SELECT CAST(CONVERT(date, fecha, 103) as VARCHAR(15)), sum(cantPzas) FROM bitacora WHERE linea = '$linea' AND tema LIKE 'Piezas Producidas' AND anio = '$anio' GROUP BY fecha ORDER BY fecha ";
    return getArraySQL($sql);
}

//function oeeProduction($linea, $fIni){
function oeeProductionDay($linea, $fIni, $fFin){ 
    $sql = "SELECT CONVERT(varchar(11),fecha), prod, tec, org, cal, cam, tDes FROM OEEPercentDay WHERE linea LIKE '%$linea%' AND fecha >= '$fIni' AND fecha <= '$fFin' ORDER BY fecha ASC";
    return getArraySQL($sql);
}

// CONSULTAS PARA ANALISIS DE OPERACIONES (ANALISIS TOP 5)
function operationsLine($linea){
    $sql = "SELECT operacion FROM op WHERE linea LIKE '%$linea%' AND operacion <> '$linea' AND tipo > 0 ";
    return getArraySQL($sql);
}

function analysisOperatrionsLine($linea, $tema){ 
    $sql = "SELECT CONVERT(varchar(15), CONCAT(year(fecha),'-',month(fecha),'-',day(fecha))), operacion, sum(duracion) FROM Bitacora WHERE linea like '%$linea%' AND tema LIKE '%$tema%' AND operacion <> '' AND operacion <> linea GROUP BY fecha, operacion";
    return getArraySQL($sql); 
} 

#INSERTAR EN OPL
function iOplLinea ($fecha, $detecto, $estacion, $desviacion, $causa, $accion, $responsable, $fCompromiso, $linea, $tipo) { 
    $sql = "INSERT INTO oplLinea (fecha, detecto, estacion, desviacion, causa, accion, responsable, fCompromiso, estatus, linea, tipo ) VALUES('$fecha', '$detecto', '$estacion', '$desviacion', '$causa', '$accion', '$responsable', '$fCompromiso', 1,'$linea','$tipo');";
    return getArraySQL($sql);
}

function iOplProyecto ($idPr, $accion, $responsable, $area, $fecha, $fCompromiso, $detecta ) { 
    $sql = "INSERT INTO oplProyectos (idPr, accion, responsable, area, fecha, estatus, evaluacion, fCompromiso, soporte) VALUES('$idPr', '$accion', '$responsable', '$area', '$fecha', 1, 0,'$fCompromiso', '$detecta');";
    return getArraySQL($sql);
} 

#CONSULTAS PARA FILTROS DE LISTAS DE OPL
function cOPLCerradosATiempoL ( $area, $fIni, $fFin ) { 
    $sql = "SELECT * FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso >= fActualiza AND tipo = 0 ";
    return getArraySQL($sql); 
} 

function cOPLCerradosATiempo_5S ( $area, $fIni, $fFin ) { 
    $sql = "SELECT * FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso >= fActualiza AND tipo = 2 ";
    return getArraySQL($sql); 
} 

function cOPLCerradosFTiempoL ( $area, $fIni, $fFin ) { 
    $sql = "SELECT * FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso < fActualiza AND tipo = 0 ";
    return getArraySQL($sql); 
} 

function cOPLCerradosFTiempo_5S ( $area, $fIni, $fFin ) { 
    $sql = "SELECT * FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso < fActualiza AND tipo = 2 ";
    return getArraySQL($sql); 
} 

function cOPLSinCerrarL ( $area, $fIni, $fFin, $fActual ) { 
    $sql = "SELECT * FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso < '$fActual' AND estatus <> 4 AND tipo = 0 ";
    return getArraySQL($sql);
} 

function cOPLSinCerrar_5S ( $area, $fIni, $fFin, $fActual ) { 
    $sql = "SELECT * FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso < '$fActual' AND estatus <> 4 AND tipo = 2 ";
    return getArraySQL($sql);
} 

function cOPLPFuturosL ( $area, $fIni, $fFin, $fActual ) { 
    $sql = "SELECT * FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso >= '$fActual' AND estatus <> 4 AND tipo = 0 ";
    return getArraySQL($sql); 
} 

function cOPLPFuturos_5Ss ( $area, $fIni, $fFin, $fActual ) { 
    $sql = "SELECT * FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso >= '$fActual' AND estatus <> 4 AND tipo = 2 ";
    return getArraySQL($sql); 
} 

function cOplLinea ($fIni, $fFin, $linea ) { 
    $sql = "SELECT * FROM oplLinea WHERE fecha >= '$fIni' AND fecha <= '$fFin' AND linea = '$linea' AND tipo = 0 ORDER BY fCompromiso, estatus, fActualiza; ";
    return getArraySQL($sql); 
} 

function cOplLineaAll ( $fIni, $fFin, $linea ) { 
    $sql = "SELECT * FROM oplLinea WHERE fCompromiso >= '$fIni' AND fCompromiso <= '$fFin' AND linea = '$linea' AND tipo = 0
            UNION 
            SELECT * FROM oplLinea WHERE linea = '$linea' AND estatus <> 4 AND tipo = 0 ORDER BY fCompromiso, estatus, fActualiza";
    return getArraySQL($sql); 
} 

function cOplLineaAll_5Ss ( $fIni, $fFin, $linea ) { 
    $sql = "SELECT * FROM oplLinea WHERE fCompromiso >= '$fIni' AND fCompromiso <= '$fFin' AND linea = '$linea' AND tipo = 2
            UNION 
            SELECT * FROM oplLinea WHERE linea = '$linea' AND estatus <> 4 AND tipo = 2 ORDER BY fCompromiso, estatus, fActualiza";
    return getArraySQL($sql); 
} 

#CONSULTAS PARA FILTROS DE LISTAS DE OPL
function cOPLCerradosATiempoP ( $area, $fIni, $fFin ) { 
    $sql = "SELECT * FROM oplProyectos WHERE area = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso >= fActualiza ORDER BY fCompromiso,estatus ";
    return getArraySQL($sql); 
} 

function cOPLCerradosFTiempoP ( $area, $fIni, $fFin ) { 
    $sql = "SELECT * FROM oplProyectos WHERE area = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso < fActualiza ORDER BY fCompromiso,estatus ";
    return getArraySQL($sql);
} 

function cOPLSinCerrarP ( $area, $fIni, $fFin, $fActual ) { 
    $sql = "SELECT * FROM oplProyectos WHERE area = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso < '$fActual' AND estatus <> 4 ORDER BY fCompromiso,estatus";
    return getArraySQL($sql);
} 

function cOPLPFuturosP ( $area, $fIni, $fFin, $fActual ) { 
    $sql = "SELECT * FROM oplProyectos WHERE area = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso >= '$fActual' AND estatus <> 4 ORDER BY fCompromiso,estatus";
    return getArraySQL($sql); 
} 

function cOplProyecto ($fIni, $fFin, $area ) { 
    $sql = "SELECT * FROM oplProyectos WHERE fecha >= '$fIni' AND fecha <= '$fFin' AND area = '$area' ORDER BY fCompromiso,estatus ; "; 
    return getArraySQL($sql); 
} 

#CONSULTAS PARA REPORTE DE PUNTOS POR AREA
function cTotalPuntosL ( $area, $fIni, $fFin ) {  
    $sql = "SELECT COUNT(id) as pLinea FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND tipo = 0 "; 
    return getArraySQL($sql); 
} 

function cTotalPuntosP ( $area, $fIni, $fFin ) { 
    $sql = "SELECT COUNT(id) as pProyectos FROM oplProyectos WHERE area = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' ";
    return getArraySQL($sql);
}

# CERRADOS A TIEMPO
function cPCerradosATiempoL ( $area, $fIni, $fFin ) {
    $sql = "SELECT COUNT(id) as pLinea FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso >= fActualiza AND tipo = 0 ";
    return getArraySQL($sql);
}

function cPCerradosATiempoP ($area, $fIni, $fFin ) {
    $sql = "SELECT COUNT(id) as pProyectos FROM oplProyectos WHERE area = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso >= fActualiza ";
    return getArraySQL($sql);
}

# CERRADOS FUERA DE TIEMPO
function cPCerradosFTiempoL ( $area, $fIni, $fFin ) {
    $sql = "SELECT COUNT(id) as pLinea FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso < fActualiza AND tipo = 0 ";
    return getArraySQL($sql);
}

function cPCerradosFTiempoP ($area, $fIni, $fFin ) {
    $sql = "SELECT COUNT(id) as pProyectos FROM oplProyectos WHERE area = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND estatus = 4 AND fCompromiso < fActualiza ";
    return getArraySQL($sql);
}

# SIN CERRAR
function cPSinCerradosL ( $area, $fIni, $fFin, $fActual ) {
    $sql = "SELECT COUNT(id) as pLinea FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso < '$fActual' AND estatus <> 4 AND tipo = 0 ";
    return getArraySQL($sql);
}

function cPSinCerradosP ($area, $fIni, $fFin, $fActual ) { 
    $sql = "SELECT COUNT(id) as pProyectos FROM oplProyectos WHERE area = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso < '$fActual' AND estatus <> 4 ";
    return getArraySQL($sql);
}

# ESPERA / ABIERTOS
function cPFuturosL ( $area, $fIni, $fFin, $fActual ) {
    $sql = "SELECT COUNT(id) as pLinea FROM oplLinea WHERE linea = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso >= '$fActual' AND estatus <> 4 AND tipo = 0 ";
    return getArraySQL($sql);
}

function cPFuturosP ($area, $fIni, $fFin, $fActual ) {
    $sql = "SELECT COUNT(id) as pProyectos FROM oplProyectos WHERE area = '$area' AND fecha >= '$fIni' AND fecha <= '$fFin' AND fCompromiso >= '$fActual' AND estatus <> 4 ";
    return getArraySQL($sql);
}

function uOplLinea ($estado, $fecha, $id ) {
    $sql = "UPDATE oplLinea SET estatus = '$estado', fActualiza='$fecha' WHERE id = '$id' ; ";
    return getArraySQL($sql);
}

function uOplProyecto ($fecha, $estado, $evaluacion, $id ) { 
    $sql = "UPDATE oplProyectos SET estatus = '$estado', fActualiza = '$fecha', evaluacion = '$evaluacion' WHERE id = '$id'; ";
    return getArraySQL($sql);
}

function dOplLinea ( $id ) {
    $sql = "DELETE FROM oplLinea WHERE id = '$id' ; ";
    return getArraySQL($sql);
}

function dOplProyecto ($idPr, $idOpl ) {
    $sql = "DELETE FROM oplProyectos WHERE idPr = '$idPr' AND id = '$idOpl'  ;";
    return getArraySQL($sql);
}

#APARTADO PARA USUARIOS 
function cUsuario ($usuario) {
    $sql = "SELECT * FROM usuarios WHERE usuario = '$usuario' AND estado = 1; ";
    return getArraySQL($sql); 
}

function cUsuarioContrasenia ($usuario, $contrasenia ) {
    $sql = "SELECT * FROM usuarios WHERE usuario = '$usuario' AND contrasenia like '%$contrasenia%'; ";
    return getArraySQL($sql); 
}

function cUsuarioTLinea ( $linea ) { 
    $sql = "SELECT correo FROM usuarios WHERE privilegio = 3 and estado = 1 and linea = '$linea' ORDER BY correo DESC; "; 
    return getArraySQL($sql); 
} 

function cLAsistencia_FMatriz ( $linea, $fecha, $turno ) { 
    $sql = "SELECT CONCAT(u.apellido, ' ', u.nombre), o.operacion FROM rAsistenciaPersonal AS rl, UsuariosLAsistencia as u, op as o 
            WHERE rl.bandera = 1 AND rl.linea = '$linea' AND rl.fecha = '$fecha' AND rl.turno = '$turno' AND rl.tAsistencia IN ('A','R') AND u.numEmpleado = rl.numEmpleado AND o.id = rl.operacion 
            GROUP BY CONCAT(u.apellido, ' ', u.nombre), o.operacion ; "; 
    return getArraySQL($sql); 
} 

function  cHRLAdmin (){
    $sql = "SELECT correo FROM usuarios WHERE linea = 'HRL' AND estado = 1 AND privilegio = 1 "; 
    return getArraySQL($sql);
}

#CALIDAD INTERNA 
function iCalidadInterna ($fecha, $linea, $cliente, $noParte, $desviacion,$nota, $usuario, $tipo ) { 
    $sql = "INSERT INTO calidadInterna (fecha, linea, cliente, noParte, desviacion, nota, uRegistra, tipo) VALUES('$fecha', '$linea', '$cliente', '$noParte', '$desviacion', '$nota', '$usuario', '$tipo');";
    return getArraySQL($sql);     
} 

function cCalidadInternaMes ( $linea, $fIni, $fFin ) { 
    $sql = "SELECT COUNT(id), DAY(fecha) FROM calidadInterna WHERE fecha >= '$fIni' AND fecha <= '$fFin' AND linea = '$linea' AND tipo = 1 GROUP BY id, fecha ORDER by fecha ";
    return getArraySQL($sql); 
} 

function cCalidadKmMes ( $linea, $fIni, $fFin ) { 
    $sql = "SELECT COUNT(id), DAY(fecha) FROM calidadInterna WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' AND tipo = 2 GROUP BY id, fecha, tipo ORDER by fecha, tipo ;";
    return getArraySQL($sql); 
}

function cCalidadInternaAnio ( $linea, $anio ) { 
    $sql = "SELECT MONTH(fecha) as mes, COUNT(id) FROM calidadInterna WHERE YEAR(fecha) = '$anio' AND linea = '$linea' AND tipo = 1 GROUP BY MONTH(fecha) ";
    return getArraySQL($sql); 
} 

function cCalidadKmAnio ( $linea, $anio) { 
    $sql = "SELECT MONTH(fecha) as mes,COUNT(id) FROM calidadInterna WHERE YEAR(fecha) = '$anio' AND linea = '$linea' AND tipo = 2 GROUP BY MONTH(fecha) ;";
    return getArraySQL($sql); 
}

function cCalidadInternaSemana ( $linea, $fIni, $fFin ) { 
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) as semana, COUNT(id) FROM calidadInterna WHERE fecha >= '$fIni' AND fecha <= '$fFin' AND linea = '$linea' AND tipo = 1 GROUP BY DATEPART(week,fecha) ";
    return getArraySQL($sql); 
} 

function cCalidadKmSemana ( $linea, $fIni, $fFin ) { 
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) as mes, COUNT(id) FROM calidadInterna WHERE fecha >= '$fIni' AND fecha <= '$fFin' AND linea = '$linea' AND tipo = 2 GROUP BY DATEPART(week,fecha); ";
    return getArraySQL($sql); 
}

# INVENTARIOS
function iInventario ($fecha, $linea, $max, $min, $real ) { 
    $sql = "INSERT INTO inventario (linea, fecha, vMax, vMin, vreal) VALUES('$linea', '$fecha', '$max', '$min', '$real');";
    return getArraySQL($sql); 
} 

function cInventario ($fIni, $fFin, $linea ) { 
    $sql = "SELECT fecha, vmax, vmin, vreal FROM inventario WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' ";
    return getArraySQL($sql); 
} 

#CONSULTA DE USUARIO TIPEADO
function cUsuarioType ($usuario) { 
    $sql = "SELECT nombre FROM lAsistencia WHERE nombre LIKE '%$usuario%' ";
    return getArraySQL($sql); 
}

#LISTAS DE ASISTENCIA
function iListaUsuarios ($linea, $fecha, $turno, $nombre, $asistencia, $horas, $puesto) { 
    $sql = "INSERT INTO lAsistencia (linea, fecha, turno, nombre, asistencia, horas, puesto) VALUES('$linea', '$fecha', '$turno', '$nombre', '$asistencia', '$horas', '$puesto');";
    return getArraySQL($sql); 
}

function cListaAsistencia ( $linea, $fIni, $fFin ) { 
    $sql = "SELECT linea, fecha, nombre, asistencia, horas, puesto FROM lAsistencia WHERE linea LIKE '%$linea%' AND fecha >= '$fIni' AND fecha <= '$fFin' ORDER BY fecha, turno, puesto, nombre ASC;";
    return getArraySQL($sql); 
}

function cLAsistencia_Linea ( $linea, $fIni, $fFin ) { 
    $sql = "SELECT r.fecha, r.linea, r.turno, o.operacion, r.numEmpleado, CONCAT(u.apellido,' ',u.nombre),  r.horas, r.tAsistencia, r.bandera 
            FROM rAsistenciaPersonal AS R, op as o, UsuariosLAsistencia as u 
            WHERE  r.linea= '$linea' AND r.linea = o.linea AND r.operacion = o.id AND r.numEmpleado = u.numEmpleado AND u.numEmpleado = r.numEmpleado AND fecha >= '$fIni' AND fecha <= '$fFin'
            GROUP BY r.fecha, r.linea, r.turno, o.operacion, r.numEmpleado, CONCAT(u.apellido,' ',u.nombre),  r.horas, r.tAsistencia, r.bandera";
    return getArraySQL($sql); 
} 

function cListaAsistenciaTurno ( $linea, $fIni, $fFin, $turno ) { 
    $sql = "SELECT linea, fecha, nombre, asistencia, horas, puesto FROM lAsistencia WHERE linea LIKE '%$linea%' AND fecha >= '$fIni' AND fecha <= '$fFin' AND turno = '$turno' ORDER BY fecha, turno, puesto, nombre ASC;";
    return getArraySQL($sql); 
} 

function cUsuariosListaAsistencia ( $linea, $fIni, $fFin ) { 
    $sql = "SELECT nombre FROM lAsistencia WHERE linea LIKE '%$linea%' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY nombre ORDER BY nombre ASC; ";
    return getArraySQL($sql); 
}

function cUsuariosListaAsistenciaTurno ( $linea, $fIni, $fFin, $turno ) { 
    $sql = "SELECT nombre FROM lAsistencia WHERE linea LIKE '%$linea%' AND fecha >= '$fIni' AND fecha <= '$fFin' AND turno = '$turno' GROUP BY nombre ORDER BY nombre ASC; ";
    return getArraySQL($sql); 
}

function cListaAsistenciaDiaTurno ( $linea, $dia, $turno ) { 
    $sql = "SELECT r.turno, CONCAT(u.apellido,' ', u.nombre), o.operacion, r.puesto, r.tAsistencia, r.horas, r.numEmpleado, r.operacion
            FROM rAsistenciaPersonal as r, op as o, UsuariosLAsistencia as u 
            WHERE r.fecha = '$dia' AND r.linea = '$linea' AND r.turno = '$turno' AND r.numEmpleado = u.numEmpleado AND r.operacion = o.id
            GROUP BY r.turno, CONCAT(u.apellido,' ', u.nombre), o.operacion, r.puesto, r.tAsistencia, r.horas, r.numEmpleado, r.operacion
            ORDER BY r.turno, o.operacion, CONCAT(u.apellido,' ', u.nombre), r.puesto, r.tAsistencia, r.horas, r.numEmpleado, r.operacion";
    return getArraySQL($sql); 
}

/********************/ 
function cAsistenciaIndexLinea($linea, $fecha){
    $sql = "SELECT r.puesto, CONCAT(u.apellido,' ',u.nombre), r.tAsistencia
            FROM rAsistenciaPersonal AS R, UsuariosLAsistencia as u 
            WHERE  r.linea= '$linea' AND u.numEmpleado = r.numEmpleado AND fecha = '$fecha' 
            GROUP BY r.puesto, CONCAT(u.apellido,' ',u.nombre), r.tAsistencia
            ORDER BY r.puesto, CONCAT(u.apellido,' ',u.nombre), r.tAsistencia";
    return getArraySQL($sql); 
}

function cAllUsuariosLAsistecia_Linea($linea){ 
    $sql = "SELECT puesto, CONCAT(apellido,' ',nombre), numEmpleado FROM UsuariosLAsistencia WHERE linea = '$linea' GROUP BY puesto, CONCAT(apellido,' ',nombre), numEmpleado ";
    return getArraySQL($sql); 
} 

function cUsuariosLAsistecia_Linea_Fecha($linea, $fIni, $fFin){ 
    $sql = "SELECT r.puesto, CONCAT(u.apellido,' ',u.nombre), u.numEmpleado
            FROM rAsistenciaPersonal AS R, UsuariosLAsistencia as u 
            WHERE  r.linea= '$linea' AND u.numEmpleado = r.numEmpleado AND fecha >= '$fIni' AND fecha <= '$fFin'
            GROUP BY r.puesto, CONCAT(u.apellido,' ',u.nombre), u.numEmpleado
            ORDER BY r.puesto, CONCAT(u.apellido,' ',u.nombre), u.numEmpleado";
    return getArraySQL($sql); 
} 

function cAllOperaciones_Linea($linea){ 
    $sql = "SELECT id, operacion FROM op WHERE linea = '$linea' ";
    return getArraySQL($sql); 
} 

function cUsuariosLAsistencia_Linea($linea, $fecha){
    $sql = "SELECT u.puesto, CONCAT(u.apellido,' ',u.nombre), u.numEmpleado,m.grupo 
            FROM matrizCapacitacion as m, UsuariosLAsistencia as u, op as o 
            WHERE NOT EXISTS (
                                SELECT r.numEmpleado
                                FROM rAsistenciaPersonal as r
                                WHERE r.numEmpleado = m.idUsuario AND r.fecha = '$fecha'
                        ) AND o.linea = '$linea' AND o.id = m.idOperacion AND m.idUsuario = u.numEmpleado
            GROUP BY u.puesto, CONCAT(u.apellido,' ',u.nombre), u.numEmpleado, m.grupo ORDER by m.grupo, u.puesto";  
    return getArraySQL($sql);
} 

function cOpLAsistencia_Linea_Empleado($linea, $empleado){
    $sql = "SELECT mc.idOperacion, o.operacion, o.descripcion
            FROM matrizCapacitacion as mc, op as o
            WHERE o.linea = '$linea' AND mc.idUsuario = '$empleado' AND o.id = mc.idOperacion
            GROUP BY MC.idOperacion, o.operacion, o.descripcion
            ORDER BY o.operacion";
    return getArraySQL($sql);
}

function iLAsistencia_Linea($fecha, $linea, $turno, $puesto, $operacion, $numEmpleado, $horas, $bandera, $tAsistencia ){ 
    $sql = "INSERT INTO rAsistenciaPersonal (fecha, linea, turno, puesto, operacion, numEmpleado, horas, bandera, tAsistencia) VALUES('$fecha', '$linea', '$turno', '$puesto', '$operacion', '$numEmpleado', '$horas', '$bandera', '$tAsistencia');";
    return getArraySQL($sql);
} 

/********************/

function cListaAsistenciaDia ( $linea, $dia ) { 
    $sql = "SELECT r.turno, CONCAT(u.apellido,' ', u.nombre), o.operacion, r.puesto, r.tAsistencia, r.horas, r.numEmpleado, r.operacion
            FROM rAsistenciaPersonal as r, op as o, UsuariosLAsistencia as u 
            WHERE r.fecha = '$dia' AND r.linea = '$linea' AND r.numEmpleado = u.numEmpleado AND r.operacion = o.id
            GROUP BY r.turno, CONCAT(u.apellido,' ', u.nombre), o.operacion, r.puesto, r.tAsistencia, r.horas, r.numEmpleado, r.operacion
            ORDER BY r.turno, o.operacion, CONCAT(u.apellido,' ', u.nombre), r.puesto, r.tAsistencia, r.horas, r.numEmpleado, r.operacion";
    return getArraySQL($sql); 
}

function uListaAsistencia ( $id, $fecha, $linea, $tipo, $horas) { 
    $sql = "UPDATE rAsistenciaPersonal SET tAsistencia='$tipo', horas='$horas' WHERE fecha='$fecha' AND linea='$linea' AND numEmpleado='$id' ";
    return getArraySQL($sql);
}

function dListaAsistencia ( $id, $fecha, $linea ) {     
    $sql = "DELETE FROM rAsistenciaPersonal WHERE fecha='$fecha' AND linea='$linea' AND numEmpleado='$id'; ";
    return getArraySQL($sql);
}

/**************************** TARGETS *************************************/ 
function  cPercentsTopicPeriodLine ($linea, $fIni, $fFin){ 
    $sql = "SELECT AVG(tec) tec, AVG(org) org, AVG(cal) cal FROM OEEPercentDay 
            WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' AND (tec <> 0 OR prod <> 0 OR org <> 0 OR cam <> 0)"; 
    return getArraySQL($sql); 
} 


function cTargetsMesLosses ($linea, $anio, $mes){ 
    $sql = "SELECT CONVERT(varchar(11),fecha), OEE, tecnicas, organizacionales, cambio, calidad, scrap, productividad FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR,fecha) = '$anio' and DATEPART(MONTH,fecha) = '$mes';";
    return getArraySQL($sql);     
} 

function cTargetsMesProduccion ($linea, $anio , $mes){
    $sql = "SELECT CONVERT(varchar(11),fecha), mtaPzas FROM targetProduccion WHERE linea = '$linea' AND DATEPART(YEAR,fecha) = '$anio' and DATEPART(MONTH,fecha) = '$mes'; ";
    return getArraySQL($sql);     
} 

function cExisteTargetsMesLineaLosses ( $anio, $mes, $linea ) { 
    $sql = "SELECT COUNT(id) FROM targetLosses WHERE DATEPART(YEAR,fecha) = '$anio' and DATEPART(MONTH,fecha) = '$mes' and linea = '$linea' AND OEE > 0 AND tecnicas > 0 AND organizacionales > 0 ";
    return getArraySQL($sql); 
} 

function cExisteTargetsMesLineaProduccion ( $anio, $mes, $linea ) { 
    $sql = "SELECT COUNT(id) FROM targetProduccion WHERE DATEPART(YEAR,fecha) = '$anio' and DATEPART(MONTH,fecha) = '$mes' and linea = '$linea' AND mtaPzas > 0 ";
    return getArraySQL($sql); 
} 

function iTargetLosses ( $linea, $fecha, $oee, $tec, $org, $cambio, $cali, $scrap, $produ) { 
    $sql = "INSERT INTO targetLosses (linea, fecha, OEE, tecnicas, organizacionales, cambio, calidad, scrap, productividad) VALUES ('$linea', '$fecha', '$oee', '$tec', '$org', '$cambio', '$cali', '$scrap', '$produ');";
    return getArraySQL($sql); 
}

function iTargetProduccion ( $linea, $fecha, $meta ) { 
    $sql = "INSERT INTO targetProduccion (linea, fecha, mtaPzas) VALUES('$linea', '$fecha', '$meta'); "; 
    return getArraySQL($sql); 
} 

function uTargetLosses ( $linea, $fecha, $oee, $tec, $org, $cambio, $cali, $scrap, $produ) { 
    $sql = "UPDATE targetLosses SET OEE = '$oee', tecnicas = '$tec', organizacionales = '$org', cambio = '$cambio', calidad = '$cali', scrap = '$scrap', productividad = '$produ' WHERE linea = '$linea' AND fecha = '$fecha' ; ";
    return getArraySQL($sql); 
}

function uTargetProd ( $linea, $fecha, $mtaPzas ) { 
    $sql = "UPDATE targetProduccion SET mtaPzas = '$mtaPzas' WHERE linea = '$linea' AND fecha = '$fecha' ; ";
    return getArraySQL($sql); 
}

function iHistoricoTarget ( $fReg, $fMod, $tema, $cO, $cA, $usuario, $comentario ) { 
    $sql = "INSERT INTO historicoTarget (fModifica, fModificada, tema, tOriginal, tActual, usuario, comentario) VALUES('$fReg', '$fMod', '$tema', '$cO', '$cA', '$usuario', '$comentario'); "; 
    return getArraySQL($sql); 
} 

#CONSULTAS DE TARGET PARA CADA TEMA 
#OEE
function cTargetOEEMonthly ($linea, $anio ) { 
    $sql = "SELECT DATEPART(MONTH,fecha), AVG(OEE) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND OEE <> 0 GROUP BY DATEPART(MONTH,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetOEEWeekly ($linea, $anio, $mes ) { 
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), AVG(OEE) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND DATEPART(MONTH, fecha) = '$mes' AND OEE <> 0 GROUP BY DATEPART(WEEK,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetOEEWeekly_IP ($linea, $fechaI, $fechaF ) {
    $sql = "SET DATEFIRST 1  SELECT DATEPART(WEEK,fecha), AVG(OEE) FROM targetLosses WHERE linea = '$linea' AND fecha >= '$fechaI' AND fecha <= '$fechaF' AND OEE <> 0 GROUP BY DATEPART(WEEK,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetOEEDaily ($linea, $fIni, $fFin ) { 
    $sql = "SELECT CONVERT(varchar(15), fecha), OEE FROM targetLosses WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY CONVERT(varchar(15), fecha), OEE "; 
    return getArraySQL($sql); 
} 

#TECNICAS
function cTargetTecMonthy ($linea, $anio ) {
    $sql = "SELECT DATEPART(MONTH,fecha), SUM(tecnicas) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND tecnicas <> 0 GROUP BY DATEPART(MONTH,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetTecWeekly ($linea, $anio, $mes ) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), SUM(tecnicas) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND DATEPART(MONTH, fecha) = '$mes' AND tecnicas <> 0 GROUP BY DATEPART(WEEK,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetTecDaily ($linea, $fIni, $fFin ) { 
    $sql = "SELECT CONVERT(varchar(15), fecha), tecnicas FROM targetLosses WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY CONVERT(varchar(15), fecha), tecnicas; "; 
    return getArraySQL($sql); 
} 

#ORGANIZACIONALES
function cTargetOrgMonthy ($linea, $anio ) {
    $sql = "SELECT DATEPART(MONTH,fecha), SUM(organizacionales) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND organizacionales <> 0 GROUP BY DATEPART(MONTH,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetOrgWeekly ($linea, $anio, $mes ) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), SUM(organizacionales) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND DATEPART(MONTH, fecha) = '$mes' AND organizacionales <> 0 GROUP BY DATEPART(WEEK,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetOrgDaily ($linea, $fIni, $fFin ) { 
    $sql = "SELECT CONVERT(varchar(15), fecha), organizacionales FROM targetLosses WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY CONVERT(varchar(15), fecha), organizacionales; "; 
    return getArraySQL($sql); 
} 

#CALIDAD
function cTargetCaliMonthy ($linea, $anio ) {
    $sql = "SELECT DATEPART(MONTH,fecha), SUM(calidad) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND calidad <> 0 GROUP BY DATEPART(MONTH,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetCaliWeekly ($linea, $anio, $mes ) {
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), SUM(calidad) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND DATEPART(MONTH, fecha) = '$mes' AND calidad <> 0 GROUP BY DATEPART(WEEK,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetCaliDaily ($linea, $fIni, $fFin ) { 
    $sql = "SELECT CONVERT(varchar(15), fecha), calidad FROM targetLosses WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY CONVERT(varchar(15), fecha), calidad; "; 
    return getArraySQL($sql); 
} 

#CAMBIO DE MODELO
function cTargetCambioMonthy ($linea, $anio ) {
    $sql = "SELECT DATEPART(MONTH,fecha), SUM(cambio) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND cambio <> 0 GROUP BY DATEPART(MONTH,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetCambioWeekly ($linea, $anio, $mes ) { 
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), SUM(cambio) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND DATEPART(MONTH, fecha) = '$mes' AND cambio <> 0 GROUP BY DATEPART(WEEK,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetCambioDaily ($linea, $fIni, $fFin ) { 
    $sql = "SELECT CONVERT(varchar(15), fecha), cambio FROM targetLosses WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY CONVERT(varchar(15), fecha), cambio; "; 
    return getArraySQL($sql); 
} 

#PIEZAS A PRODUCIR 
function cTargetPzasMonthy ($linea, $anio ) { 
    $sql = "SELECT DATEPART(MONTH,fecha), SUM(mtaPzas) FROM targetProduccion WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND mtaPzas <> 0 GROUP BY DATEPART(MONTH,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetPzasWeekly ($linea, $fIni, $fFin ) { 
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), SUM(mtaPzas) FROM targetProduccion WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' AND mtaPzas <> 0 GROUP BY DATEPART(WEEK,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetPzasDaily ($linea, $fIni, $fFin ) { 
    $sql = "SELECT CONVERT(varchar(15), fecha), mtaPzas FROM targetProduccion WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY CONVERT(varchar(15), fecha), mtaPzas; "; 
    return getArraySQL($sql); 
} 

#PRODUCTIVIDAD
function cTargetProductividadMonthy ($linea, $anio ) { 
    $sql = "SELECT DATEPART(MONTH,fecha), AVG(productividad) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND  productividad <> 0 GROUP BY DATEPART(MONTH,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetProductividadWeekly ($linea, $anio, $mes ) { 
    $sql = "SET DATEFIRST 1 SELECT DATEPART(WEEK,fecha), AVG(productividad) FROM targetLosses WHERE linea = '$linea' AND DATEPART(YEAR, fecha) = '$anio' AND DATEPART(MONTH, fecha) = '$mes' AND productividad <> 0 GROUP BY DATEPART(WEEK,fecha); "; 
    return getArraySQL($sql); 
} 

function cTargetProductividadDaily ($linea, $fIni, $fFin ) { 
    $sql = "SELECT CONVERT(varchar(15), fecha), productividad FROM targetLosses WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY CONVERT(varchar(15), fecha), productividad; "; 
    return getArraySQL($sql); 
} 

#CONSULTA DE OPL MODIFICADA
function cOPLIdL ($id) { 
    $sql = "SELECT desviacion, accion, fCompromiso, responsable, estatus, linea FROM oplLinea WHERE id = '$id' ;"; 
    return getArraySQL($sql); 
} 

function cOPLIdP ($id){ 
    $sql = "SELECT * FROM oplProyectos WHERE id = '$id'; "; 
    return getArraySQL($sql); 
} 

# LISTA DE JUNTA DE ASISTENCIA PARA JUNTAS 
function cULJunta ($linea){ 
    $sql = "SELECT CONCAT(Apellido,' ',Nombre), Departamento FROM ListaJuntas WHERE Linea = '$linea'; "; 
    return getArraySQL($sql); 
} 

function cUCVJunta ($cValor){ 
    $sql = "SELECT CONCAT(Apellido,' ',Nombre), Departamento FROM ListaJuntas WHERE Linea = '$linea'; "; 
    return getArraySQL($sql); 
} 

function cLJuntaLinea ($area, $fechaI){ 
    $sql = "SELECT * FROM LJunta WHERE area = '$area' AND fecha = '$fechaI' ; "; 
    return getArraySQL($sql); 
} 

function cExistLJuntaLinea ($fechaI, $area, $tJunta){ 
    $sql = "SELECT * FROM LJunta WHERE area = '$area' AND fecha = '$fechaI' AND tipoJunta = '$tJunta' ; "; 
    return getArraySQL($sql); 
} 

function iULJunta ($fecha, $area, $nombre, $departamento, $tAsistencia, $tJunta ){ 
    $sql = "INSERT INTO LJunta (fecha, area, nombre, departamento, tAsistencia, tipoJunta) VALUES('$fecha', '$area', '$nombre', '$departamento', '$tAsistencia', '$tJunta');"; 
    return getArraySQL($sql); 
} 

function cListaUsuariosJunta ( $linea, $fIni, $fFin ) { 
    $sql = "SELECT departamento, nombre FROM LJunta WHERE area LIKE '%$linea%' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY departamento, nombre ORDER BY departamento,nombre ASC; ";
    return getArraySQL($sql); 
}

function cListaAsistenciaJunta ( $linea, $fIni, $fFin ) { 
    $sql = "SELECT fecha, nombre, departamento, tAsistencia FROM LJunta WHERE area LIKE '%$linea%' AND fecha >= '$fIni' AND fecha <= '$fFin' ORDER BY fecha, departamento, nombre ASC;";
    return getArraySQL($sql); 
}

function cSeguridad( $linea, $fIni, $fFin ) { 
    $sql = "SELECT DAY(fecha), clasificacion, reporta FROM seguridad WHERE tReporte = 'S' AND linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' ORDER BY fecha, reporta";
    return getArraySQL($sql); 
}

function cMAmbiente( $linea, $fIni, $fFin ) { 
    $sql = "SELECT DAY(fecha), clasificacion, reporta FROM seguridad WHERE tReporte = 'A' AND linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' ORDER BY fecha, reporta";
    return getArraySQL($sql); 
}

// SEGURIDAD
function iSeguridad_Ambiente( $fecha, $linea, $tipo, $category, $descripcion, $reporta ) { 
    $sql = "INSERT INTO seguridad (fecha, linea, tReporte, clasificacion, descripcion, reporta) VALUES('$fecha', '$linea', '$tipo', '$category', '$descripcion', '$reporta');";
    return getArraySQL($sql); 
} 

/********** INSERT DE 5Ss ******/ 
function i5Ss ($linea, $auditado, $auditor, $puntaje, $fecha ){ 
    $sql = "INSERT INTO audit5Ss (linea, auditado, auditor, puntaje, fecha) VALUES('$linea', '$auditado', '$auditor', '$puntaje', '$fecha'); ";
    return getArraySQL($sql); 
} 

#CONSULTAS 
function c5SDay($linea, $fIni, $fFin ){
    $sql = "SELECT CONVERT(varchar(15), fecha), puntaje FROM audit5Ss WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY fecha, puntaje ORDER BY fecha";
    return getArraySQL($sql);
}

function c5SWeek($linea, $fIni, $fFin ){
    $sql = "SET DATEFIRST 1 SELECT DATEPART(week,fecha) as sem, puntaje FROM audit5Ss WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' GROUP BY fecha, puntaje ORDER BY fecha";
    return getArraySQL($sql);
}

function c5SMonth($linea, $anio ){
    $sql = "SELECT DATEPART(MONTH,fecha) as sem, puntaje FROM audit5Ss WHERE linea = '$linea' AND DATEPART(YEAR,fecha) = '$anio' GROUP BY fecha, puntaje ORDER BY fecha";
    return getArraySQL($sql);
}

/****************** TARGET PARA SCRAP **************/
function cTargetScrapDay($linea, $fIni, $fFin ){
    $sql = "SELECT CONVERT(varchar(11),fecha), scrap FROM targetLosses WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin' ORDER BY fecha";
    return getArraySQL($sql);
}

function cTargetScrapWeek($linea, $fIni, $fFin ){
    $sql = "SET DATEFIRST 1 
            SELECT DATEPART(week,fecha) as sem, SUM(scrap) 
            FROM targetLosses 
            WHERE linea = '$linea' AND fecha >= '$fIni' AND fecha <= '$fFin'
            GROUP BY DATEPART(week,fecha) ORDER BY DATEPART(week,fecha)";
    return getArraySQL($sql);
}

function cTargetScrapMonth($linea, $anio ){
    $sql = "SELECT MONTH(fecha), SUM(scrap) FROM targetLosses WHERE linea = '$linea' AND YEAR(fecha) = '$anio' GROUP BY MONTH(fecha) ORDER BY MONTH(fecha)";
    return getArraySQL($sql);
}
