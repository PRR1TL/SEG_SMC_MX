<?php //

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
    include './ServerFunctions.php'; 
    session_start(); 
    
    $_SESSION['conexion'] = 0;
    //ENTRADA DESDE BITACORA   
    if (isset($_REQUEST['linea'])){ 
        $_SESSION['conexion'] = 1; 
        $_SESSION['tipoDato'] = 1; 
        $_SESSION['entrada'] = 2; # DESDE BITACORA 
        $_SESSION['vista'] = 1; # VISTA POR RANGO DE FECHAS 
        $_SESSION['nivelReporte'] = 3; # LINEA 
        
        $dR = DateTime::createFromFormat('d/m/Y', $_REQUEST['f']);
        $f = $dR->format('Y-m-d');
        $_SESSION['anio'] = date("Y", strtotime($f)); 
        $_SESSION['mes'] = date("m", strtotime($f)); 
        $_SESSION['d'] = date("d", strtotime($f)); 
        
        $ultimoDiaMes = date("t",mktime(0,0,0, date("m"),1, date("Y"))); 
        
        $_SESSION['fIni'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-01'; 
        $_SESSION['fFin'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-'.$ultimoDiaMes; 
        
        $_SESSION['FIni'] = $_SESSION['fIni'];
        $_SESSION['FFin'] = $_SESSION['fFin'];
        
        //echo $f, ', ', $_SESSION['FIni'],', ', $_SESSION['FFin'];
        
        /* DATOS DEL USUARIO */ 
        if (!empty($_REQUEST['u'] && $_REQUEST['u'] != "" && strlen($_REQUEST['u']) > 4 )) { 
            $_SESSION['usuario'] = $_REQUEST['u']; 
            #CONSULTA DE INFORMACION DEL USUARIO 
            $cUsuario = cUsuario($_REQUEST['u']); 
            for ($i = 0; $i < count($cUsuario); $i++){ 
                $_SESSION['productoU'] = $cUsuario[$i][0]; //PRODUCTO 
                $_SESSION['lineaU'] = $cUsuario[$i][1]; //LINEA 
                $_SESSION['correo'] = $cUsuario[$i][7]; //CORREO 
                $_SESSION['nameUsuario'] = $cUsuario[$i][3].' '.$cUsuario[$i][4]; //NOMBRE 
                $lastName = explode(" ", $cUsuario[0][4]); 
                $_SESSION['nickName'] = substr($cUsuario[0][3],0,1).'. '.$lastName[0]; //NICKNAME 
                $_SESSION['privilegio'] = $_REQUEST['p']; //PRIVILEGIO 
            } 
        } 
        
        /* DATOS DE LA LINEA */        
        $_SESSION['linea'] = $_REQUEST['linea']; 
        //MANDA NOMBRE DE LA LINEA SELECCIONADA 
        $nombreLinea = nombreLinea($_REQUEST['linea']); 
        for ($i = 0; $i < count($nombreLinea); $i++) { 
            $_SESSION['nameLinea'] = $_REQUEST['linea'].' - '.$nombreLinea[$i][0]; 
            $_SESSION['costCenterL'] = $nombreLinea[$i][1]; 
            $_SESSION['profitCenterL'] = $nombreLinea[$i][2]; 
            $_SESSION['cValorL'] = $nombreLinea[$i][3]; 
            $_SESSION['productoL'] = $nombreLinea[$i][4]; 
        } 
    ?> 
        <script>
            window.location = "../iLinea.php";
        </script>       
    <?php 
    } else { 
        $_SESSION['conexion'] = 1;
        $_SESSION['tipoDato'] = 1; 
        $_SESSION['vista'] = 1; 
        $_SESSION['entrada'] = 1; #DESDE PARTE WEB 
        
        if (isset($_SESSION['tema'])){ 
            $_SESSION['tema'] = $_POST['tema']; 
        } else { 
            $_SESSION['tema'] = ''; 
        } 
        
        if (isset($_POST['turno'])){ 
            $_SESSION['turno'] = $_POST['turno']; 
        } 
        
        if (isset($_POST['dia'])){ 
            $_SESSION['dia'] = $_POST['dia']; 
        } else { 
            $_SESSION['dia'] = date("Y-m-d"); 
        }
        
        # INICIALIZAMOS ANIO Y MES CON LOS VALORES ACTUALES 
        $_SESSION['anio'] = date('Y'); 
        $_SESSION['mes'] = date('m'); 
        $ultimoDiaMes = date("t",mktime(0,0,0, date("m"),1, date("Y"))); 
        
        $_SESSION['fIni'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-01'; 
        $_SESSION['fFin'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-'.$ultimoDiaMes; 
        
        if (isset($_POST['fecha'])){ 
            $_SESSION['mes'] = date('m', strtotime($_POST['fecha'])); 
            $_SESSION['anio'] = date('Y', strtotime($_POST['fecha'])); 
            $ultimoDiaMes = date("t",mktime(0,0,0, $_SESSION['mes'],1, $_SESSION['anio'])); 
            $_SESSION['fIni'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-01'; 
            $_SESSION['fFin'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-'.$ultimoDiaMes; 
        } 
        
        if (isset($_POST['fechaP'])){ 
            $_SESSION['mes'] = date('m', strtotime($_POST['fechaP'])); 
            $_SESSION['anio'] = date('Y', strtotime($_POST['fechaP'])); 
            $ultimoDiaMes = date("t",mktime(0,0,0, $_SESSION['mes'],1, $_SESSION['anio'])); 
            $_SESSION['fIni'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-01'; 
            $_SESSION['fFin'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-'.$ultimoDiaMes; 
        } 
        
        $_SESSION['FIni'] = $_SESSION['fIni']; 
        $_SESSION['FFin'] = $_SESSION['fFin']; 
        
        if(isset($_POST['tipoVista'])){ 
            $_SESSION['vista'] = $_POST['tipoVista']; 
            echo $_POST['tipoVista'];
            switch ($_POST['tipoVista']){ 
                case 1: //PERIODO 
                    if (isset($_POST['fIni'])) { 
                        $_SESSION['FIni'] = date("Y-m-d", strtotime($_POST['fIni'])); 
                        $_SESSION['FFin'] = date("Y-m-d", strtotime($_POST['fFin'])); 
                    } 
                    break; 
                case 2: //AÑO 
                    $_SESSION['anio'] = date('Y', strtotime($_POST['fIni'])); 
                    $_SESSION['mes'] = date('m', strtotime($_POST['fIni'])); 
                    $_SESSION['FIni'] = $_SESSION['anio'].'-01-01'; 
                    $_SESSION['FFin'] = $_SESSION['anio'].'-12-31'; 
                    break; 
            } 
        }
        
        if(isset($_POST['tipoDato'])){
            $_SESSION['tipoDato'] = $_POST['tipoDato'];
        } 
        
        if (!isset($_SESSION['privilegio'])){ 
            $_SESSION['privilegio'] = 0; 
        } 
        
        /* CUANDO VA DE PRODUCTO A CADENA DE VALOR */ 
        if (isset($_POST['cmbProd'])){ 
            $_SESSION['nivelReporte'] = 2;
            $_SESSION['cValorL'] = $_POST['cmbProd'];
            $_SESSION['nameCValorL'] = 'VALUE STREAM '.$_POST['cmbProd'];             
        } 
        
        /* CUANDO VA DE CADENA DE VALOR A LINEA */ 
        if (isset($_POST['cmbCValor'])){ 
            if (substr($_POST['cmbCValor'], 0,1) == 'L'){ 
                $_SESSION['nivelReporte'] = 3; 
                $_SESSION['linea'] = $_POST['cmbCValor']; 
                //MANDA NOMBRE DE LA LINEA SELECCIONADA 
                $nombreLinea = nombreLinea($_POST['cmbCValor']); 
                for ($i = 0; $i < count($nombreLinea); $i++) { 
                    $_SESSION['nameLinea'] = $_POST['cmbLinea'].' - '.$nombreLinea[$i][0]; 
                    $_SESSION['costCenterL'] = $nombreLinea[$i][1]; 
                    $_SESSION['profitCenterL'] = $nombreLinea[$i][2]; 
                    $_SESSION['cValorL'] = $nombreLinea[$i][3]; 
                    $_SESSION['productoL'] = $nombreLinea[$i][4]; 
                } 
            } 
        } 
        
/* CREACION DE SESIONES DE ACUERO A VALORES DE COMPONENTES ENE LAS VENTANAS */ 
        /* ENTRANDO DESDE ILINEA */ 
        if(isset($_POST['cmbLinea'])){ 
            $_SESSION['linea'] = $_POST['cmbLinea']; 
            $nombreLinea = nombreLinea($_POST['cmbLinea']); 
            for ($i = 0; $i < count($nombreLinea); $i++) { 
                $_SESSION['nameLinea'] = $_POST['cmbLinea'].' - '.$nombreLinea[$i][0]; 
                $_SESSION['costCenterL'] = $nombreLinea[$i][1]; 
                $_SESSION['profitCenterL'] = $nombreLinea[$i][2]; 
                $_SESSION['cValorL'] = $nombreLinea[$i][3]; 
                $_SESSION['productoL'] = $nombreLinea[$i][4]; 
            }             
        } 
        
        if (isset($_POST['nivReporte'])){ 
            $_SESSION['nivelReporte'] = $_POST['nivReporte']; 
        } 
        
        if (isset($_SESSION['linea'])){
            $nombreLinea = nombreLinea($_SESSION['linea']); 
            for ($i = 0; $i < count($nombreLinea); $i++) { 
                $_SESSION['nameLinea'] = $_SESSION['linea'].' - '.$nombreLinea[$i][0]; 
                $_SESSION['costCenterL'] = $nombreLinea[$i][1]; 
                $_SESSION['profitCenterL'] = $nombreLinea[$i][2]; 
                $_SESSION['cValorL'] = $nombreLinea[$i][3]; 
                $_SESSION['productoL'] = $nombreLinea[$i][4]; 
            } 
        }
        
        if (isset($_POST['filtro'])){
            $_SESSION['filtro'] = $_POST['filtro'];
        }  
        
/*--------------------------------------------------------------------------------*/         
        /* T. REPORTE / APARTADO APRA EL INDEX Y LO QUE VAYAN SELECCIONANDO */ 
        # 1. PLANTA / SEG-MX
        # 2. PRODUCTO / MOE-1, MOE-2 
        # 3. C.VALOR / EL-PL, CV, NBL, ST 
        # 4. LINEA / L001, L002, L003, L022, L026, L024, L163-CP2, ETC.         
        if (isset($_POST['nivlReporte']) && !empty($_POST['nivlReporte'])){ 
            $_SESSION['nivelReporte'] = $_POST['nivlReporte']; 
            switch ($_POST['nivlReporte']){ 
                case 0: # PLANTA 
                    echo './iSEG.php'; 
                    break; 
                case 1: # PRODUCTO 
                    $_SESSION['productoL'] = $_POST['valor']; 
                    if($_POST['valor'] == 'GE'){ //ALTERNADORES 
                        $_SESSION['nameProductoL'] = 'GENERATORS'; 
                    } else { 
                        $_SESSION['nameProductoL'] = 'STARTER MOTORS'; 
                    } 
                    echo './iProd.php';
                    break; 
                case 2: # CAD VALOR
                    $_SESSION['cValorL'] = $_POST['valor']; 
                    $_SESSION['nameCValorL'] = 'VALUE STREAM '.$_POST['valor']; 
                    
                    # OBTENCION DE INFORMACION DE LA CADENA DE VALOR 
                    $cProducto = cProducto_CValor($_POST['valor']); 
                    for ($i = 0; $i < count($cProducto); $i++){ 
                        $_SESSION['productoL'] = $cProducto[$i][0];
                    } 
                    echo './iCad.php'; 
                    break; 
                case 3: 
                    echo './iLinea.php'; 
                    break;                 
            } 
        } 
    
    } 
    
    