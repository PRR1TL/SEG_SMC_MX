<HTML>
    <head>
        <LINK REL=StyleSheet HREF="../css/hourly.css" TYPE="text/css" MEDIA=screen>
        <title>Hourly Count</title>
        <link href="../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" >
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
                
        <script src="https://code.highcharts.com/highcharts.js"></script> 
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
        <script src="https://code.jquery.com/jquery-1.12.1.js"></script>         
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>              
        
        <script src="//www.amcharts.com/lib/3/amcharts.js"></script>
        <script src="//www.amcharts.com/lib/3/serial.js"></script>
        <script src="//www.amcharts.com/lib/3/themes/light.js"></script>
        
        
        
        <!-------------------------------CONSULTAS----------------------------->            
        <script>
            var entrada = 0;
            document.cookie ='entrada='+entrada+'; path=/';
        </script>
        <?php            
            require_once '../db/ServerFunctions.php';            
            
            if ( isset($_COOKIE['entrada']) ){
                $bnEntrada = isset($_COOKIE['entrada']) ? $_COOKIE['entrada']: 0;                
            } else {
                $bnEntrada = isset($_REQUEST['contEntrada']) ? $_REQUEST['contEntrada']: $_COOKIE['entrada'];
            }
                          
            $yearCurrent = date("Y");
            $monthCurrent =  date("m");
            
            //echo 'e:',$bnEntrada,"<br>";
            if ($bnEntrada == 0){ //PARA CUANDO ENTRA POR PRIMERA VEZ DESDE BITACORA                
                //echo '<br>entra 1<br><br><br>';
                if ( isset( $_REQUEST['f'] ) ){ //PARTIMOS DE LA FECHA
                    //echo '<br>entra 1.1 <br><br><br>';
                    $f = explode("/", $_REQUEST['f']);
                    
                    $dia = $f[0];      
                    $month = $f[1];
                    $year = $f[2];                    
                    
                    $line = isset($_REQUEST['line']) ? $_REQUEST['line']: $_COOKIE["lineaI"];
                } else {
                    //echo '<br>entra 1.2 <br><br><br>';
                //PARA CUANDO ENTRA DESDE LA PAGINA WEB (SOLO QUE SE SEPAN LA RUTA)
                    $line = isset($_REQUEST['line']) ? $_REQUEST['line']: '';
                    
                    $month = isset($_REQUEST['month']) ?  $_REQUEST['month']: $monthCurrent ;
                    $year = isset($_REQUEST['year']) ? $_REQUEST['year']: $yearCurrent;
                    //PARA CUANDO ENTRA POR PRIMERA VEZ DESDE NAVEGADOR (QUE SE SEPAN LA RUTA ) 
                    if ($yearCurrent == $year && $monthCurrent == $month){
                        $dia = date("d");
                    } else {
                        $dia = '01';
                    }
                }          
                
                $daySelect = $year.'-'.$month.'-'.$dia;
                
                $datTargetBD = targetHourly($line, $year, $month, $dia); //CONSULTA A BASE DE DATOS DE TARGETS DE PRODUCTIVIDAD
                $targetBD = isset($datTargetBD[0][0]) ? $datTargetBD[0][0]: 80;
                
                //Line Takt 
                $datLineTakt = tcPonderadoProd($line,$daySelect);
                $lineTakt = isset($datLineTakt[0][0]) ? $datLineTakt[0][0]:0;
                
                $targetOEE = isset($_COOKIE['Meta']) ? $_COOKIE['Meta']: $targetBD; //CONSULTA A BASE DE DATOS DE TARGETS DE PRODUCTIVIDAD
            } else if ($bnEntrada == 1) { //PARA CUANDO VIENE DEL INDEX
                //echo '<br>entra 2 <br><br><br>';
                $line = isset($_REQUEST['line']) ? $_REQUEST['line']: $_COOKIE["lineHourly"];
                $month = isset($_REQUEST['month']) ?  $_REQUEST['month']: $monthCurrent ;
                $year = isset($_REQUEST['year']) ? $_REQUEST['year']: $yearCurrent;
                
                if ($yearCurrent == $year && $monthCurrent == $month){
                    $dia = date("d");
                } else {
                    $dia = '01';
                }
                
                $dateSelect = $year.'-'.$month.'-'.$dia;
                
                $datLineTakt = tcPonderadoProd($line,$dateSelect);
                $lineTakt = $datLineTakt[0][0];
                
                $datTargetBD = targetHourly($line, $year, $month, $dia); //CONSULTA A BASE DE DATOS DE TARGETS DE PRODUCTIVIDAD
                $targetBD = $datTargetBD[0][0];
                
                $targetOEE = isset($_COOKIE['Meta']) ? $_COOKIE['Meta']: $targetBD; //CONSULTA A BASE DE DATOS DE TARGETS DE PRODUCTIVIDAD
            } else if ($bnEntrada == 2){ //PARA CUANDO CAMBIAN LOS PARAMETROS
                //echo '<br>entra 3 <br>';
                $line = $_COOKIE ["lineHourly"];
                $month = $_COOKIE['mesHourly'];
                $year = isset($_COOKIE['anioHourly']) ? $_COOKIE['anioHourly']: $_COOKIE["anioI"];
                $dia =  isset($_COOKIE["diaHourly"]) ? $_COOKIE["diaHourly"]: 1; 
                
                $daySelect = $year.'-'.$month.'-'.$dia;
                //echo 'f: ', $dia, '', $month;
                $datTargetBD = targetHourly($line, $year, $month, $dia); //CONSULTA A BASE DE DATOS DE TARGETS DE PRODUCTIVIDAD
                $targetBD = isset($datTargetBD[0][0]) ? $datTargetBD[0][0] : 80;
                
                //REFRESCAMOS EL TARGET, YA SE POR CAMBIO DE LINEA, O CAMBIO DEL MISMO VALOR DE TARGET
                if (isset($_COOKIE ['Meta']) && $_COOKIE ['Meta'] != 0 ){ //SI ESTA DEFINIDA LA COOKIE
                    // MIENTRAS EL TARGET SEA DISTINTO DE 0 (YA QUE CUANDO SE CAMBIA LA LINEA, EL TARGET SE CONVIERTE EN 0)
                    $targetOEE = $_COOKIE["Meta"];
                } else {
                    //SI EL TARGET ES 0 (CAMBIO DE LINEA) SE ACTUALIZA EL TARGET
                    $targetOEE = isset($targetBD) ? $targetBD: 80;
                }                 
            }            
            $daySelect = $year.'-'.$month.'-'.$dia;            
            
            //echo 'd: ',$dia,' m: ',$month,' y: ',$year;
            $line = 'L003';
            $month = '5';
            $year = '2019';
            $dia = '17';
            
            //echo '<br>tg: ',$targetOEE;
                  
            //$targetOEE = 80; //CONSULTA A BASE DE DATOS DE TARGETS DE PRODUCTIVIDAD
            
            //$varMesStr = listarMeses();
            //$dia = 20;
            $ultimoDiaMes=date("t",mktime(0,0,0,$month,1,$year));
            $diaIni = $month.'/01/'.$year;
            
            $fecha = $dia.'/'.$month.'/'.$year;
            $fConsult = $year.'-'.$month.'-'.$dia;
            //echo $diaIni,'<br>';
            $diaFn = $month.'/'.$ultimoDiaMes.'/'.$year;            

            $pDiaF = $ultimoDiaMes;
            
            $xI = $fecha;
            $xF = $fecha;
                        
            $lineasArrObj = listarLineas();
            for ($i = 0; $i < count($lineasArrObj); $i++) {
                $lineaArr[$i] = $lineasArrObj[$i][0];
            }
        
            //INCIAMOS EL CONTEO PARA LAS HORAS
            for( $i = 0; $i < 25; $i++){
                $pzasAcumTarget[$i] = 0;
                $pzasProdH[$i] = 0;
                $cumPzasProdH[$i] = 0;
                $noParteTcH[$i] = '';
                $typeH[$i] = '';
                $tcH[$i] = '';
                $pzasScrapH[$i] = 0;

                $reworkH[$i] = 0;
                $durCambioH[$i] = 0;
                $durTecnicasH[$i] = 0;
                $durCalidadH[$i] = 0;
                $durOrgH[$i] = 0;
                $durDesempenioH[$i] = 0;
                
                //ETIQUETA DE PRODUCCION
                $typeH[$i] = '';

                //LOS CODIGOS X & DESCRIPCION X SON DE CAMBIOS & TECNICAS
                $codeHX[$i] = '';
                $descriptionHX[$i]= '';

                //LOS CODIGOS Y DESCRIPCION Y SON ORGANIZACIONALES & DESEMPENIO
                $codeHY[$i] = '';
                $descriptionHY[$i]= '';

                $periodOEEH[$i] = 0;
                $OEEAcumH[$i] = 0;
                
                $pzasTargetH[$i] = 0;
            }

            //TURNOS DIA
            $datHourlyTurno = hourlyTurnos($line, $fConsult);
            $nTurnos = count($datHourlyTurno);
            //echo $nTurnos,'<br>';
            
            //INICIAMOS LAS VARIABLES DE LOS TURNOS            
            $iniT1 = 26;
            $iniT2 = 26;
            $iniT3 = 26;
            $iniT4 = 26;
            
            for ($i = 0; $i < count($datHourlyTurno); $i++){       
                if ($i == 0){ //PARA PRIMER TURNO TURNO 
                    $iniT1 = $datHourlyTurno[0][1];
                    $finT1 = $datHourlyTurno[0][2];                   
                    $durT1 = $finT1 - $iniT1;
                }             
                if ($i == 1){ //PARA SEGUNDO TURNO             
                    $iniT2 = $datHourlyTurno[1][1];
                    $finT2 = $datHourlyTurno[1][2];
                    $durT2 = $finT2 - $iniT2;
                }   
                if ($i == 2 ){ //PARA TERCER TURNO Y/O SEGUNDO COMPLEMENTARIO
                    $iniT3 = $datHourlyTurno[2][1];
                    $finT3 = $datHourlyTurno[2][2];
                    $durT3 = $finT3 - $iniT3;     
                } 
                if ($i == 3 ){ //PARA TERCER TURNO COMPLEMENTARIO
                    $iniT4 = $datHourlyTurno[2][1];
                    $finT4 = $datHourlyTurno[2][2];
                    $durT4 = $finT4 - $iniT4;     
                } 
            }

            if ($nTurnos!= 0 ){                
                $cont = 0;
                $datHourlyPzas = hourlyDiaPzasProd($line, $year, $month, $dia);
                for($i = 0; $i < count($datHourlyPzas); $i++ ){
                    $h = $datHourlyPzas[$i][0];
                    $hour[$i] = $h;

                    if ($i != 0 && $hour[$i-1] == $h ){
                        //echo $i,': ', $hour[$i-1],', ', $hour[$i],' - ',$pzasProdH[$h-1],'<br>';
                        $pzasProdH[$h] = $pzasProdH[$h] + $datHourlyPzas[$i][1];
                    } else {
                        //echo $i,': ', $hour[$i],', ','<br>';
                        $pzasProdH[$h] = $datHourlyPzas[$i][1];
                    }
                    
                    if ($i != 0){
                        if ($h == $iniT1 || $h == $iniT2 || $h == $iniT3){
                            $cumPzasProdH[$h] = $pzasProdH[$h];
                        } else {
                            $cumPzasProdH[$h] = $cumPzasProdH[$h-1] + $pzasProdH[$h];
                        }
                    } else {                
                        $cumPzasProdH[$h] =  $pzasProdH[$h];
                    }
                    //echo $h,': ',$pzasProdH[$h],', ',$noParteTcH[$h],', ',$cumPzasProdH[$h],'<br>';
                }                
                
                $datNoPartTc = hourlyNoParteTC($line, $daySelect);
                for ($i = 0; $i < count($datNoPartTc); $i++){
                    $h = $datNoPartTc[$i][0];                        
                    if (isset($typeH[$h])){
                        $typeH[$h] = $typeH[$h].'<br>'.$datNoPartTc[$i][1].' / '.$datNoPartTc[$i][2];
                    } else {
                        $typeH[$h] = $datNoPartTc[$i][1].' / '.$datNoPartTc[$i][2];
                    }
                }
                        
                //MINUTOS A TRABAJAR CONSIDERANDO LOS MINUTOS DE COMEDOR E INICIO DE TURNO
                for($i = 0; $i < 25; $i++){
                    $minT[$i] = 60;
                    
                    if ($pzasProdH[$i] != 0 && $pzasProdH[$h] > 0){
                        //TIEMPO CICLO GENERAL PARA PRODUCCION                       
                        $datTCPonderadoGeneralH = hourlyTCPonderadoHGeneral($line, $daySelect, $i);
                        for ($j = 0; $j < count($datTCPonderadoGeneralH); $j++){
                            $h = $datTCPonderadoGeneralH[$j][0];
                            $tcH[$h] = $datTCPonderadoGeneralH[$j][1];
                            $typeH[$h] = $typeH[$h].'<br> ('.$tcH[$h].')';
                        }
                    } else {
                        //TIEMPO CICLO GENERAL PARA FALLAS                       
                        $datTCPonderadoFallaH= hourlyTCPonderadoFallaHGeneral($line, $daySelect, $i);
                        for ($j = 0; $j < count($datTCPonderadoFallaH); $j++){
                            $h = $datTCPonderadoFallaH[$j][0];
                            $tcH[$h] = $datTCPonderadoFallaH[$j][1];
                            $typeH[$h] = $typeH[$h].'<br> ('.$tcH[$h].')';
                        }
                    }
                }
//
                $datMinDescDia = minDescHoraHourly($line, $fConsult);
                for ( $i = 0; $i < count($datMinDescDia); $i++){
                    $h = $datMinDescDia[$i][0];
                    $minT[$h] = 60 - $datMinDescDia[$i][1];
                }

                //PIEZAS PLANEADAS POR HORA
                $datHPzasEspPercent = piezasEsperadasyPercentHora($line, $year, $month, $dia);
                for ($i = 0; $i < count($datHPzasEspPercent); $i++){
                    $h = $datHPzasEspPercent[$i][0];
                    $pzasTargetH[$h] = $datHPzasEspPercent[$i][1];
                    $periodOEEH[$h] = @round($datHPzasEspPercent[$i][2],2);     
                }
                    
                $pzasAcum = 0;
                for( $i = 0; $i < 25 ; $i++){
                    if($i == $iniT1 || $i == $iniT2 || $i == $iniT3){
                            $pzasAcumTarget[$i] = $pzasTargetH[$i];  
                    }else {
                        if ($i == 0){
                            $pzasAcumTarget[$i] = $pzasTargetH[$i]; 
                        }else {
                            if ($typeH[$i] != ''){
                                $pzasAcumTarget[$i] = $pzasAcumTarget[$i-1] + $pzasTargetH[$i]; 
                            } else {
                                $pzasAcumTarget[$i] = $pzasAcumTarget[$i-1] + 0; 
                            }                            
                        }
                    }
                    $pzasAcum = $pzasAcumTarget[$i];
                }

                //CONSULTAS PARA LE HOURLY                
                for ($i = 0; $i < 24; $i++){
                    if ($i != 0 && $i != $iniT1 && $i != $iniT2 && $i != $iniT3 ){
                        if ($cumPzasProdH[$i] != 0){
                            $acumuladoPzasProdH[$i] = $acumuladoPzasProdH[$i-1] + $pzasProdH[$i];
                        }else {                        
                            $acumuladoPzasProdH[$i] =  $acumuladoPzasProdH[$i-1];
                        }        
                    }else {
                        $acumuladoPzasProdH[$i] =  $cumPzasProdH[$i];
                    }                    
                }

                //CONSULTAS POR TEMAS (PRIMARIOS)
                //SCRAP POR HORAS
                $datHourlyScrap = hourlyDiaPzasCalidad($line, $year, $month, $dia);
                for($i = 0; $i < count($datHourlyScrap); $i++){
                    $h = $datHourlyScrap[$i][0];
                    $pzasScrapH[$h] = $datHourlyScrap[$i][1];
                }
                
                //CALIDAD
                $datCalidad = hourlyDiaDurCalidad($line, $year, $month, $dia);
                for($i = 0; $i < count($datCalidad); $i++){
                    $h = $datCalidad[$i][0];
                    $problemaCalidad[$h] = $datCalidad[$i][1];
                    $opCalidad[$h] = $datCalidad[$i][2];
                    $durCalidadH[$h] = $datCalidad[$i][3];
                }
                
                //REWORK
                //CAMBIOS
                $datCambios = hourlyDiaDurCambios($line, $year, $month, $dia);
                for($i = 0; $i < count($datCambios); $i++ ){
                    $h = $datCambios[$i][0];
                    $problemaCambioH[$h] = 'Cambio de Modelo '.$datCambios[$i][1].' '. $datCambios[$i][5] .': '.$datCambios[$i][3]. ' a '.$datCambios[$i][4];
                    $opCambioH[$h] = $datCambios[$i][2];
                    $durCambioH[$h] = $datCambios[$i][6];
                }

                //TECNICAS
                $datTecnicas = hourlyDiaDurTecnicas($line, $year, $month, $dia);
                for($i = 0; $i < count($datTecnicas); $i++ ){
                    $h = $datTecnicas[$i][0];                   
                    $problemaTecnicas[$h] = $datTecnicas[$i][1];
                    $opTecnicas[$h] = $datTecnicas[$i][2];
                    $durTecnicasH[$h] = $datTecnicas[$i][3];
                }
//
                //ORGANIZACIONALES        
                $datOrganizacionales = hourlyDiaOrg($line, $year, $month, $dia);
                for($i = 0; $i < count($datOrganizacionales); $i++){
                    $h = $datOrganizacionales[$i][0];
                    $problemOrgH[$h] = $datOrganizacionales[$i][1];
                    $opOrgH[$h] = $datOrganizacionales[$i][2];
                    $durOrgH[$h] = $datOrganizacionales[$i][3];
                }        
                
                /*********** APARTADO PARA PROBLEMAS SECUENDARIOS *************/
                //CALIDAD
                $datCalidadSec = hourlyDiaDurCalidadSec($line, $year, $month, $dia);
                for($i = 0; $i < count($datCalidadSec); $i++){
                    $h = $datCalidadSec[$i][0];
                    $problemaCalidadSec[$h] = $datCalidadSec[$i][1].' ('.$datCalidadSec[$i][3].')';
                    $opCalidadSec[$h] = $datCalidadSec[$i][2];
                    
                }
                
                //REWORK
                //CAMBIOS
                $datCambiosSec = hourlyDiaDurCambiosSec($line, $year, $month, $dia);
                for($i = 0; $i < count($datCambiosSec); $i++ ){
                    $h = $datCambiosSec[$i][0];
                    $problemaCambioSecH[$h] = 'Cambio de Modelo '.$datCambiosSec[$i][1].' '. $datCambiosSec[$i][5] .': '.$datCambiosSec[$i][3]. ' a '.$datCambiosSec[$i][4].' ('.$datCambiosSec[$i][6].')';
                    $opCambioSecH[$h] = $datCambiosSec[$i][2];
                }

                //TECNICAS
                $datTecnicasSec = hourlyDiaDurTecnicasSec($line, $year, $month, $dia);
                for($i = 0; $i < count($datTecnicasSec); $i++ ){
                    $h = $datTecnicasSec[$i][0];                   
                    $problemaTecnicasSec[$h] = $datTecnicasSec[$i][1].' ('. $datTecnicasSec[$i][3].')';
                    $opTecnicasSec[$h] = $datTecnicasSec[$i][2];
                }

                //ORGANIZACIONALES        
                $datOrganizacionalesSec = hourlyDiaOrgSec($line, $year, $month, $dia);
                for($i = 0; $i < count($datOrganizacionalesSec); $i++){
                    $h = $datOrganizacionalesSec[$i][0];
                    $problemOrgSecH[$h] = $datOrganizacionalesSec[$i][1].' ('.$datOrganizacionalesSec[$i][3].')';
                    $opOrgSecH[$h] = $datOrganizacionalesSec[$i][2];
                    
                }    

                /**************************************************************/
                
                //DESEMPENIO
                $datDesempenio = hourlyDiaDesempenio($line, $year, $month, $dia);
                for($i = 0; $i < count($datDesempenio); $i++){
                    $h = $datDesempenio[$i][0];
                    $opDesempenio[$h] = $line;
                    $problemaDesempenioH[$h] = 'Perdida Por Desempenio';
                    $durDesempenioH[$h] = $datDesempenio[$i][1];
                }
                
                // ARREGLO PARA JUNTAS LOS CODIGOS Y LOS PROBLEMAS POR X & Y
                for($i = 0; $i < 25; $i++ ){
                    
                    //X: CALIDAD Y CAMBIO DE MODELO 
                    if (isset($opCambioH[$i]) && isset($opScrap[$i]) ){
                        $codeHX[$i] = $opCalidad[$i].', '.$opCambioH[$i].'<br>';
                        $descriptionHX[$i]= $problemaCalidad[$i].', '.$problemaCambioH[$i].' <br> ';
                    } else if(isset($opCambioH[$i]) && !isset($opScrap[$i])){
                        $codeHX[$i] = $opCambioH[$i].'<br>';
                        $descriptionHX[$i]= $problemaCambioH[$i].' <br> ';
                    } else if(!isset($opCambioH[$i]) && isset($opScrap[$i])){
                        $codeHX[$i] = $opScrap[$i].'<br>';
                        $descriptionHX[$i]= $problemaScrap[$i].' <br> ';
                    }
                    
                    //CODIGOS SECUNDARIOS
                    if (isset($opCalidadSec[$i]) && isset($opCambioSecH[$i])){
                        $codeHX[$i] = $codeHX[$i].$opCalidadSec[$i].'<br>'.$opCambioSecH[$i]; 
                    } else if (!isset($opCalidadSec[$i]) && isset($opCambioSecH[$i])){
                        $codeHX[$i] = $codeHX[$i].'<br>'.$opCambioSecH[$i]; 
                    } else if (isset($opCalidadSec[$i]) && isset($opCambioSecH[$i])){
                        $codeHX[$i] = $codeHX[$i].'<br>'.$opCalidadSec[$i]; 
                    }
                    
                    //PROBLEMAS SECUNDARIOS
                    if (isset($problemaCalidadSec[$i]) && isset($problemaCambioSecH)){
                        $descriptionHX[$i] = $descriptionHX[$i].$problemaCalidadSec[$i].'<br>'.$problemaCambioSecH[$i]; 
                    } else if (!isset($problemaCalidadSec[$i]) && isset($problemaCambioSecH[$i])){
                        $descriptionHX[$i] = $descriptionHX[$i].$problemaCambioSecH[$i]; 
                    } else if (isset($problemaCalidadSec[$i]) && !isset($problemaCambioSec[$i])){
                        $descriptionHX[$i] = $descriptionHX[$i].$problemaCalidadSec[$i]; 
                    }
                    
                    //SEPARACION DE PROBLEMAS
                    if (isset($opOrgH[$i]) && isset($opTecnicas[$i]) && isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opOrgH[$i].', '.$opTecnicas[$i].', '.$opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].', '.$problemaTecnicas[$i].', '.$problemaDesempenioH[$i].' <br> ';
                    } else if (isset($opOrgH[$i]) && isset($problemaTecnicas[$i]) && !isset($opDesempenio[$i])){
                        $codeHY[$i] = $opOrgH[$i].', '.$opTecnicas[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].', '.$problemaTecnicas[$i].' <br> ';
                    } else if (isset($opOrgH[$i]) && !isset($problemaTecnicas[$i]) && isset($opDesempenio[$i])){
                        $codeHY[$i] = $opOrgH[$i].','.$opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].','.$problemaDesempenioH[$i].' <br> ';
                    } else if (!isset($opOrgH[$i]) && isset($problemaTecnicas[$i]) && isset($opDesempenio[$i])){
                        $codeHY[$i] = $opTecnicas[$i].', '.$opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemaTecnicas[$i].', '.$problemaDesempenioH[$i].' <br> ';
                    } else if (isset($opOrgH[$i]) && !isset($opTecnicas[$i]) && !isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opOrgH[$i].'<br>';
                        $descriptionHY[$i] = $problemOrgH[$i].' <br> ';
                    } else if (!isset($opOrgH[$i]) && isset($opTecnicas[$i]) && !isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opTecnicas[$i].'<br>';
                        $descriptionHY[$i] = $problemaTecnicas[$i].' <br> ';
                    } else if (!isset($opOrgH[$i]) && !isset($opTecnicas[$i]) && isset($opDesempenio[$i]) ){
                        $codeHY[$i] = $opDesempenio[$i].'<br>';
                        $descriptionHY[$i] = $problemaDesempenioH[$i].' <br> ';
                    }  
                    
                    //echo $opTecnicasSec[$i],' , ',$opOrgSecH[$i], '<br>';
                    //CODIGOS SECUNDARIOS
                    if (isset($opTecnicasSec[$i]) && isset($opOrgSecH[$i])){
                        //echo '<br>+<br><br><br><br><br><br>';
                        $codeHY[$i] = $codeHY[$i].$opTecnicasSec[$i].'<br>'.$opOrgSecH[$i]; 
                    } else if (!isset($opTecnicasSec[$i]) && isset($opOrgSecH[$i])){
                        //echo '<br>-<br><br><br><br><br><br>';
                        $codeHY[$i] = $codeHY[$i].$opOrgSecH[$i];
                    } else if (isset($opTecnicasSec[$i]) && !isset($opOrgSecH[$i])){
                        $codeHY[$i] = $codeHY[$i].$opTecnicasSec[$i]; 
                        //echo '<br>.<br><br><br><br><br><br>';
                    }
                    
                    //PROBLEMAS SECUNDARIOS
                    if (isset($problemOrgSecH[$i]) && isset($problemaTecnicasSec[$i])){                       
                        $descriptionHY[$i] = $descriptionHY[$i].$problemOrgSecH[$i].'<br>'.$problemaTecnicasSec[$i]; 
                    } else if (!isset($problemOrgSecH[$i]) && isset($problemaTecnicasSec[$i])){
                        $descriptionHY[$i] = $descriptionHY[$i].$problemaTecnicasSec[$i]; 
                    } else if (isset($problemOrgSecH[$i]) && !isset($problemaTecnicasSec[$i])){
                        $descriptionHY[$i] = $descriptionHY[$i].$problemOrgSecH[$i]; 
                    } 
                    
                }        

                //PARA EL TARGET DE ACUERDO REAL (QUITA LOS MINUTOS EN AUTOMATICO LAS PIEZAS DE ACUERDO A LOS MINUTOS DE LOS PAROS)
                // Y TOMANDO EL TIEMPO PLANEADO QUITANDO COMEDOR, INICIO DE COMEDOR Y ASI            
                for($i = 0; $i < 24; $i++){
                    $tiempoReal[$i] = $minT[$i] - ($durCambioH[$i] + $durTecnicasH[$i] + $durOrgH[$i] + $durDesempenioH[$i]);
                    
                    // COMPARAMOS SI EL TIEMPO PERDIDO ES MAYOR AL TIEMPLO PLANEADO (TIEMPO QUE SE LE QUITA COMEDOR Y JUNTAS)
                    if($tiempoReal[$i] >= 0 ){
                        if ($tcH[$i] != '') {
                            //$pzasTargetRealH[$i] = @round((60 * $tiempoReal[$i]) / $tcH[$i]) - $pzasScrapH[$i];
                            $pzasTargetRealH[$i] = @round((60 * $tiempoReal[$i]) / $tcH[$i]) - $pzasScrapH[$i];
                        } else {                   
                            $pzasTargetRealH[$i] = 0;
                        }                       
                    } else {
                        $pzasTargetRealH[$i] = 0;
                    }
                }

                $pzasAcum2 = 0;
                for( $i = 0; $i < 24 ; $i++){
                    if($i == $iniT1 || $i == $iniT2 || $i == $iniT3 || $i == $iniT4){
                        $pzasAcumTargetRealH[$i] = $pzasTargetRealH[$i]; 
                    }else {
                        if ($i == 0){
                            $pzasAcumTargetRealH[$i] = $pzasTargetRealH[$i]; 
                        }else {
                            $pzasAcumTargetRealH[$i] = $pzasAcumTargetRealH[$i-1] + $pzasTargetRealH[$i]; 
                        }
                    }
                    $pzasAcum2 = $pzasAcumTargetRealH[$i];
                }

                //TOTALES
                if (isset($finT1) && isset($finT2) &&  isset($finT3) ){
                    $totalPzasTarget = $pzasAcumTarget[$finT1-1] + $pzasAcumTarget[$finT2-1] + $pzasAcumTarget[$finT3-1];
                    $totalPzasTargetReal = $pzasAcumTargetRealH[$finT1-1] + $pzasAcumTargetRealH[$finT2-1] + $pzasAcumTargetRealH[$finT3-1];
                    $totalPzasProducidas = $acumuladoPzasProdH[$finT1-1] + $acumuladoPzasProdH[$finT2-1] + $acumuladoPzasProdH[$finT3-1];
                }else if(isset($finT1) && isset($finT2) &&  !isset($finT3)) {
                    $totalPzasTarget = $pzasAcumTarget[$finT1-1] + $pzasAcumTarget[$finT2-1];
                    $totalPzasTargetReal = $pzasAcumTargetRealH[$finT1-1] + $pzasAcumTargetRealH[$finT2-1];
                    $totalPzasProducidas = $acumuladoPzasProdH[$finT1-1] + $acumuladoPzasProdH[$finT2-1];
                }else if(isset($finT1) && !isset($finT2) &&  isset($finT3)){
                    $totalPzasTarget = $pzasAcumTarget[$finT1-1] + $pzasAcumTarget[$finT3-1];
                    $totalPzasTargetReal = $pzasAcumTargetRealH[$finT1-1] + $pzasAcumTargetRealH[$finT3-1];
                    $totalPzasProducidas = $acumuladoPzasProdH[$finT1-1] + $acumuladoPzasProdH[$finT3-1];
                } else if (isset($finT1) && !isset($finT2) &&  !isset($finT3) ){
                    $totalPzasTarget = $pzasAcumTarget[$finT1-1];
                    $totalPzasTargetReal = $pzasAcumTargetRealH[$finT1-1];
                    $totalPzasProducidas = $acumuladoPzasProdH[$finT1-1];
                }

                $percentOEE = @round(($totalPzasProducidas*100)/$totalPzasTarget,2,PHP_ROUND_HALF_EVEN);
                
                //TOTAL DE FALLAS 
                for ($i = 0; $i < 24; $i++){
                    if ($i == 0){                 
                        $pzasTotalScrap = $pzasScrapH[$i];
                        $durTotalCambios = $durCambioH[$i];
                        $durTotalTecnicas = $durTecnicasH[$i];
                        $durTotalOrg = $durOrgH[$i];
                        $durTotalDesempenio = $durDesempenioH[$i];
                    }else {
                        $pzasTotalScrap = $pzasTotalScrap + $pzasScrapH[$i];
                        $durTotalCambios = $durTotalCambios + $durCambioH[$i];
                        $durTotalTecnicas = $durTotalTecnicas + $durTecnicasH[$i];
                        $durTotalOrg = $durTotalOrg + $durOrgH[$i];
                        $durTotalDesempenio = $durTotalDesempenio + $durDesempenioH[$i];
                    }
                }

                $percentLosses = 100 - $percentOEE;
                $timeLosses = $pzasTotalScrap + $durTotalCambios + $durTotalTecnicas + $durTotalOrg + $durTotalDesempenio;
                //$percentCalidad = ;
                $percentCambios = @round(($durTotalCambios * $percentLosses)/$timeLosses,2,PHP_ROUND_HALF_DOWN);
                $percentTecnicas = @round(($durTotalTecnicas * $percentLosses)/$timeLosses,2,PHP_ROUND_HALF_DOWN);
                $percentOrg = @round(($durTotalOrg * $percentLosses)/$timeLosses,2,PHP_ROUND_HALF_DOWN);
                $percentDesempenio = @round(($durTotalDesempenio * $percentLosses)/$timeLosses,2,PHP_ROUND_HALF_DOWN);

                //PORCENTAJES OEE 
                $div = 0;
                $acum[0] = 0;
                for ($i = 0; $i < 24; $i++){
                    if ($i == 0){
                        if ($pzasProdH[$i] != 0){
                            $acum[$i] = ($acumuladoPzasProdH[$i] * 100 ) / $pzasAcumTarget[$i];
                        } else {
                            $acum[$i] = 0;
                        }
                            $div = 1;
                    }else {
                        if ($i == $iniT1 || $i == $iniT2 || $i == $iniT3 || $i == $iniT4 ){
                            if ($pzasProdH[$i] > 0){
                                $acum[$i] = ($acumuladoPzasProdH[$i] * 100 ) / $pzasAcumTarget[$i];
                            } else {
                                $acum[$i] = 0;
                            }
                            $div = 1;
                        } else {
                            if ($pzasProdH[$i] != 0){
                                $acum[$i] = ($acumuladoPzasProdH[$i] * 100 ) / $pzasAcumTarget[$i];
                            } else {
                                $acum[$i] = $acum[$i-1];
                            }
                            $div++;
                        }
                    }
                    $OEEAcumH[$i] = @round($acum[$i],2,PHP_ROUND_HALF_UP) ;
                }
            } else {
                $percentCambios = 0;
                $percentTecnicas = 0;
                $percentOrg = 0;
                $percentDesempenio = 0;
                $percentOEE = 0;

                //INDICADORES DE PIEZAS (DAOLY TOTAL)
                $totalPzasTarget = 0;
                $totalPzasTargetReal = 0;
                $totalPzasProducidas = 0;
                $pzasTotalScrap = 0;
                $durTotalCambios = 0;
                $durTotalTecnicas = 0;
                $durTotalOrg = 0;
                $durTotalDesempenio = 0;
            }        
            
            //PIE DE HOURLY 
            $datOEEDay = percentHourlyDay ($line, $year, $month, $dia);             
            
            $pFnProd = 0;
            $pFnTec = 0;
            $pFnOrg = 0;
            $pFnCal = 0;
            $pFnCam = 0;
            $pFnTFal = 0;
            
            for ($i =0; $i < count($datOEEDay); $i++){
                $pFnProd = @round($datOEEDay [$i][0],2);
                $pFnTec = @round($datOEEDay [$i][1],2);
                $pFnOrg = @round($datOEEDay [$i][2],2);
                $pFnCal = @round($datOEEDay [$i][3],2);
                $pFnCam = @round($datOEEDay [$i][4],2);
                $pFnTFal = @round($datOEEDay [$i][5],2);
            } 
            
            //APARTADO PARA LINETACK
            //CONSULTA PARA EL TOTAL DE PIEZAS PRODUCIDAS ESE DIA
            $pzasTotalDia = 0;
            $datPzasTotalDia = piezasTotalesDiaHourly($line, $year, $month, $dia);
            for ($i = 0; $i < count($datPzasTotalDia); $i++ ){
                $pzasTotalDia = $datPzasTotalDia[$i][0];
            }
            
            $lineTakt = 0;            
            //CONSULTA DE CANTIDAD DE PIEZAS POR TIEMPO CICLO           
            $datPzasTC = piezasTCHourly($line, $year, $month, $dia);
            for ($i = 0; $i < count($datPzasTC); $i++){
                $pzasTc[$i] = $datPzasTC[$i][0]; 
                $tcPzas[$i] = $datPzasTC[$i][1];                
                //HACEMOS EL PORCENTAJE POR TIEMPO CICLO                
                $percentTc[$i] = @round(($pzasTc[$i]*100)/$pzasTotalDia, 0, PHP_ROUND_HALF_UP )*0.010; 
                
                //OBTENEMOS EL PRODUCTO DE LOS PORCENTAJES POR EL TIEMPO CICLO
                $prodTc[$i] = $tcPzas[$i] * $percentTc[$i];
                
                //SE HACE LA SUMATORIA DE LOS PRODUCTOS (SUMPROD)
                $lineTakt += $prodTc[$i];
            }             
        ?>
            
        <!--FUNCIONALIDAD DE LOS DATEPICKET-->
        <script>            
            $( function() {                
                $( "#dateEnd").datepicker({ 
                    dateFormat: 'dd/mm/yy',
                    onSelect: function(date) {                         
                        var dfech = date;
                        var dx = dfech.split("/");
                        
                        var m = dx[1];
                        var di = dx[0];
                        var a = dx[2];
                        //console.log(di);
                        var lt = document.getElementById("Meta").value;
                        //var target = document.getElementById("Meta").value;
                        
                        //VAMOS A MANDAR EL TEXTO DEL COMBO DE LINEA EN LUGAR DEL VALOR DEL ITEM
                        var combo = document.getElementById("lineaCombo");
                        //CON LA VARIABLE x HACEMOS QUE SE MANDE EL TEXTO DEL COMBO (LINEA) 
                        //QUE SE SELECCIONA Y PERMITIR PASAR LOS VALORES CORRECTOS
                        var x = combo.options[combo.selectedIndex].text;                                        
                        
                        document.cookie ='lineHourly='+x+'; path=/';
                        document.cookie ='entrada='+2+'; path=/';
                        document.cookie ='diaHourly='+di+'; path=/';
                        document.cookie ='mesHourly='+m+'; path=/';
                        document.cookie ='anioHourly='+a+'; path=/';
                        document.cookie ='target='+lt+'; path=/';
                        window.confirm = function() { return false; };
                        location.reload(true);
                        window.confirm = function() { return false; };
                        $(".alert").hide();
                    }                    
                });
                                
                $( "#target" ).keypress(function( event ) {
                    document.cookie ='entrada='+2+'; path=/';                    
                    if ( event.which == 13 || event.keyCode == 10 || event.keyCode == 13 ) {
                        var target = document.getElementById("Meta").value;
                        document.cookie ='target='+target+'; path=/';
                        
                        //DATOS DE FECHA Y LINEA
                        //VAMOS A MANDAR EL TEXTO DEL COMBO DE LINEA EN LUGAR DEL VALOR DEL ITEM
                        var combo = document.getElementById("lineaCombo");
                        var dfech = document.getElementById("dateEnd").value;
                        
                        var dx = dfech.split("/");
                        
                        var m = dx[1];
                        var di = dx[0];
                        var a = dx[2];
                        //CON LA VARIABLE x HACEMOS QUE SE MANDE EL TEXTO DEL COMBO (LINEA) 
                        //QUE SE SELECCIONA Y PERMITIR PASAR LOS VALORES CORRECTOS
                        var x = combo.options[combo.selectedIndex].text;
                     
                        document.cookie ='lineHourly='+x+'; path=/';    
                        document.cookie ='diaHourly='+di+'; path=/';
                        document.cookie ='mesHourly='+m+'; path=/';
                        document.cookie ='anioHourly='+a+'; path=/';
                        
                        window.confirm = function() { return false; };
                        location.reload(true);
                        window.confirm = function() { return false; };
                        $("alert").hide();
                       //alert('.');
                    }  
                });                
            } ); 
            
            function consultaLinea() {
                var fecha = document.getElementById("dateEnd").value;
                
                var dx = fecha.split("/");                        
                var m = dx[1];
                var di = dx[0];
                var a = dx[2];
                        
                var x = document.getElementById("lineaCombo").value;
                var target = document.getElementById("Meta").value;
               // console.log(x);
                document.cookie ='diaHourly='+di+'; path=/';
                document.cookie ='mesHourly='+m+'; path=/';
                document.cookie ='anioHourly='+a+'; path=/';
                document.cookie ='lineHourly='+x+'; path=/';
                document.cookie ='target='+0+'; path=/';
                document.cookie ='entrada='+2+'; path=/';
                $(".alert").hide();
                window.confirm = function() { return false; };
                location.reload(true);
                window.confirm = function() { return false; };
                $(".alert").hide();
            }
                
        </script>     
            
        <a align=center id="headerTop3" class="contenedor">               
            <div class='fila0'>                
            </div>           
            <h5 class="tituloPareto">
                <?php echo 'Hourly Count <br>'?>
                <!--,'Linea:&nbsp'.$line.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp Mes: '. $varMesStr[$month - 1] ?>-->
            </h5>            
            <div class="fila1">      
                <img src="../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
                <form action="../index.php" method="POST">
                    <?php
                        echo "<input type="."\"hidden\" name="."\"line\""."value=".$line.">";
                        echo "<input type="."\"hidden\" name="."\"month\""."value=".$month.">";
                        echo "<input type="."\"hidden\" name="."\"year\""."value=".$year.">";
                    ?>                
                    <button class="btn btn-success btn-sm btnRegresarTop" type="submit" onclik='window.location.href="../index.php"';
                        onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer'" onmouseout="this.style.background='#008C5B'">
                        <img src="../imagenes/home.png" />
                    </button> 
                </form>

                <div class="pickers">
                    <form aling = "center"  method="POST" >  
                        <!--action="top3Planeados.php"-->
                        <ul class="nav justify-content-center">
                            <li class="nav-item">
                                <label>Linea: </label>
                                <select type="text" id="lineaCombo" name="cmbLinea" class=" btn btn-secondary btn-sm datesPickets" onchange="consultaLinea()" >
                                    <?php
                                        for ($i = 0; $i < count($lineasArrObj); $i++) {
                                            if($line == $lineaArr[$i]){
                                                echo "<option value='".($i+1)."' selected>" . $lineaArr[$i] . "</option>";
                                            } else {
                                                echo "<option>" . $lineaArr[$i] . "</option>";     
                                            }
                                        }
                                    ?>
                                </select>
                            </li>
                            <li class="nav-item">                      
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Fecha: <input class="btn btn-secondary dropdown-toggle btn-sm datesPickets" type="text" name="dateEnd" id="dateEnd" placeholder="Dia Final" value="<?php echo $xF;?>">
                            </li>
                            <li class="nav-item">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Line Takt: <input class="btn-sm cmbTxtArriba" id="tc" name="tc" placeholder="Line Takt" value="<?php echo $lineTakt?>" readonly >                                    
                            </li>                            
                            <li class="nav-item">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                Target: <input class="btn-sm cmbTxtArriba" id="Meta" name="Meta" placeholder="Meta" value="<?php echo $targetOEE?>" />
                            </li>
                        </ul>
                        <?php
                            echo "<input type="."\"hidden\" name="."\"pLine\""."value=".$line.">";
                            echo "<input type="."\"hidden\" name="."\"pMonth\""."value=".$month.">";
                            echo "<input type="."\"hidden\" name="."\"pYear\""."value=".$year.">";                                    
                        ?>
                    </form>
                </div>          
            </div>                
        </a> 
    </head>
    
    <body>        
       <div id="graficas">
           <br> <br>   
           <table class="rwd_auto fontsize" id="ptc" >
                <thead style="font-weight:bold; width: 100%">
                    <tr style="background: #F2F2F2;"> 
                        <th></th> 
                        <th>Time</th> 
                        <th COLSPAN=2>Target</th> 
                        <th COLSPAN=2>Real Est</th> 
                        <th COLSPAN=2>Real Prod</th> 
                        <th>Pces / Hour</th> 
                        <th>Type</th> 
                        <th COLSPAN=2>Quality[Pces]</th> 
                        <th COLSPAN=4>Availability Losses</th> 
                        <th COLSPAN=4>Ocurrences</th>
                        <th COLSPAN=2>OEE%</th>
                    </tr>
                    <tr style="background: #F2F2F2" height="25vh"> 
                        <th width='2.8%'>Period</th> 
                        <th width='2.8%'>Mins</th> 
                        <th width='2.8%'>Units</th><th width='2.8%'>Cum</th> 
                        <th width='2.8%'>Units</th><th width='2.8%'>Cum</th> 
                        <th width='2.8%'>Units</th><th width='2.8%'>Cum</th> 
                        <th width='11%'></th> 
                        <th width='7%'>Type / TC</th> 
                        <th width='2.8%'>Scrap</th><th width='2.8%'>Rework</th> 
                        <th width='4.5%'>Changeover</th><th width='4.5%'>Technical</th><th width='4.5%'>Organizat</th><th width='4.5%'>Perform</th> 
                        <th width='2.8%'>Code</th><th width='17%' >Deviation</th><th width='2.8%'>Code</th><th width='17%' >Deviation</th> 
                        <th width='3%'>PERIODO OEE</th><th width='3%'>OEE Cumm</th> 
                        <th width="10px"/> 
                    </tr> 
                </thead> 
                <tbody> 
                    <?php if ($nTurnos != 0) { ?>
                        <!--PRIMER TURNO-->
                        <?php for ($i = $iniT1; $i < $finT1; $i++){?> 
                            <tr style= "background: <?php  if($periodOEEH[$i] == 0 ){ echo '#F2F3F4';} ?> ;" >
                                <?php for ($j = 0; $j < 22; $j++){ ?>
<!--                                    <td>-->
                                        <?php
                                            switch ($j){
                                                case 0:                                                    
                                                    echo "<td width='2.8%'>";
                                                        echo $i,'-',$i+1;
                                                    break;
                                                case 1:
                                                    echo "<td align='center' width = '2.5%'>";
                                                        echo $minT[$i];
                                                    break;
                                                case 2:
                                                    ?>
                                                        <td align='center' width='2.4%' style= " background:  #cacfd2 " >
                                                    <?php
                                                        echo $pzasTargetH[$i];
                                                    break;
                                                case 3:
                                                    echo "<td align='center' width='2.8%'>";
                                                        echo $pzasAcumTarget[$i];
                                                    break;
                                                case 4:
                                                    ?>
                                                        <td align="center" width='2.8%' style= " background: <?php  if( ($pzasTargetRealH[$i]*100)/$pzasTargetH[$i] < $targetOEE ){ echo '#cd6155'; /*color rojo*/;} else {echo '#58d68d' /*color verde*/;} ?>; color: black;">
                                                    <?php
                                                       echo $pzasTargetRealH[$i];
                                                    break;
                                                case 5:
                                                    echo "<td align='center' width='2.8%'>";
                                                        echo $pzasAcumTargetRealH[$i];
                                                    break;
                                                case 6:
                                                    ?>
                                                        <td align="center" width='2.8%' style= " background: <?php 
                                                        if( ($pzasProdH[$i]*100)/$pzasTargetH[$i] > $targetOEE ){ 
                                                            echo '#58d68d';                                                            
                                                        } else { 
                                                            if ($pzasProdH[$i] == 0){
                                                                echo '#B3B6B7';
                                                            } else {
                                                                if ( $pzasTargetRealH[$i] > $pzasProdH[$i] ){
                                                                    echo '#cd6155';
                                                                } else {
                                                                    echo '#f7dc6f'; 
                                                                }                                                          
                                                            }                                                          
                                                        } ?>; 
                                                        color: black;">
                                                    <?php 
                                                    //echo "<td width='25vh'>";
                                                        echo $pzasProdH[$i];
                                                    break;
                                                case 7:
                                                    echo "<td align='center' width='2.4%'>";
                                                        echo $acumuladoPzasProdH[$i];
                                                    break;
                                                case 8:
                                                    echo "<td width = '3.5%'>";
                                                    ?>
                                                    <div id="<?php echo "container".$i ?>" > 
                                                        <script>
                                                            var chart = AmCharts.makeChart("<?php echo "container".$i ?>", {
                                                            "theme": "light", 
                                                            "type": "serial", 
                                                            "dataProvider": [{ 
                                                              "hora": "USA", 
                                                              "meta": 3.5, 
                                                              "real": 4.2 
                                                            }],
                                                            "valueAxes": [{
                                                              "labelsEnabled": false,
                                                              "integersOnly": true
                                                            }],
                                                            "rotate": true,
                                                            "startDuration": 1,
                                                            "graphs": [{
                                                              "balloonText": "Meta: <b>[[value]]</b>",
                                                              "fillAlphas": 0.9,
                                                              "lineAlpha": 0.2,
                                                              "title": "2004",
                                                              "type": "column",
                                                              "valueField": "meta"
                                                            }, {
                                                              "balloonText": "Real: <b>[[value]]</b>",
                                                              "fillAlphas": 0.9,
                                                              "lineAlpha": 0.2,
                                                              "title": "2005",
                                                              "type": "column",
                                                              "clustered": false,
                                                              "columnWidth": 0.5,
                                                              "valueField": "real"
                                                            }],
                                                            "plotAreaFillAlphas": 0.1,
                                                            "categoryField": "hora",
                                                            "categoryAxis": {   
                                                                "labelsEnabled": false
                                                            }
                                                          });
                                                        </script>
                                                    </div>
                                                    <?php
                                                    break;
                                                case 9:
                                                    echo "<td align='center' width = '7.0%'>";
                                                        echo $typeH[$i];
                                                    break;
                                                case 10:
                                                    echo "<td align='center' width='2.8%'>";
                                                        echo $pzasScrapH[$i];
                                                    break;
                                                case 11:
                                                    echo "<td align='center' width='2.8%'>";
                                                        echo $reworkH[$i];
                                                    break;
                                                case 12:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durCambioH[$i];
                                                    break;
                                                case 13:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durTecnicasH[$i];
                                                    break;
                                                case 14:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durOrgH[$i];
                                                    break;
                                                case 15:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durDesempenioH[$i];
                                                    break;
                                                case 16:
                                                    echo "<td width='2.4%'>";
                                                        echo $codeHX[$i];
                                                    break;
                                                case 17:
                                                    echo "<td width='17%'>";
                                                        echo $descriptionHX[$i];
                                                    break;
                                                case 18:
                                                    echo "<td width='2.4%'>";
                                                        echo $codeHY[$i];
                                                    break;
                                                case 19:
                                                    echo "<td width='17%'>";
                                                        echo $descriptionHY[$i];
                                                    break;
                                                case 20:
                                                    echo "<td align='center' width='2.4%'>";
                                                        echo $periodOEEH[$i];
                                                    break;
                                                case 21:
                                                    echo "<td align='center' width='2.4%'>";
                                                        echo $OEEAcumH[$i];
                                                    break;
                                            }
                                        ?>                                
                                    </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>

                        <!--DIVICION DE FIN DE TURNO 1-->
                        <tr style= "background: <?php  if($OEEAcumH[$finT1-1] < $targetOEE ){ echo '#cd6155';} else {echo '#52be80';} ?> ;" height="10px" >

                            <td colspan="2"><?php echo '1. Shift' ?> </td>
                            <td>Total</td>
                            <td> <?PHP echo $pzasAcumTarget[$finT1-1] ?> </td>
                            <td colspan="2"> <?php  echo $pzasAcumTargetRealH[$finT1-1] ?> </td>
                            <td colspan="2">  <?php  echo $acumuladoPzasProdH[$finT1-1]?> </td>
                            <td></td>
                            <td></td>
                            <td> <?php 
                                $sumPzasScrap1 = 0;
                                for ($i = $iniT1; $i< ($iniT1+$durT1); $i++){
                                    $sumPzasScrap1 = $sumPzasScrap1 + $pzasScrapH[$i];
                                }
                                echo $sumPzasScrap1;
                            ?></td>
                            <td></td>
                            <td> <?php 
                                $sumDurCambios1 = 0;
                                for ($i = $iniT1; $i< ($iniT1+$durT1); $i++){
                                    $sumDurCambios1 = $sumDurCambios1 + $durCambioH[$i];
                                }
                                echo $sumDurCambios1;
                            ?> </td>
                            <td> <?php 
                                $sumDurTecnicas1 = 0;
                                for ($i = $iniT1; $i< ($iniT1+$durT1); $i++){
                                    $sumDurTecnicas1 = $sumDurTecnicas1 + $durTecnicasH[$i];
                                }
                                echo $sumDurTecnicas1;
                            ?> </td>
                            <td>
                                <?php 
                                    $sumDurOrganizacionales1 = 0;
                                    for ($i = $iniT1; $i< ($iniT1+$durT1); $i++){
                                        $sumDurOrganizacionales1 = $sumDurOrganizacionales1 + $durOrgH[$i];
                                    }
                                    echo $sumDurOrganizacionales1;
                                ?>
                            </td>
                            <td>
                                <?php 
                                    $sumDurDesempenio1 = 0;
                                    for ($i = $iniT1; $i< ($iniT1+$durT1); $i++){
                                        $sumDurDesempenio1 = $sumDurDesempenio1 + $durDesempenioH[$i];
                                    }
                                    echo $sumDurDesempenio1;
                                ?>
                            </td>                            
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <!--PARA PROCENTAJES DE OEE-->
                            <td width='2vh'> 1. Shift </td>
                            <td width='2vh'> <?php echo $OEEAcumH[$finT1-1],'%'  ?></td>                        
                        </tr>

                    <?php } if ($nTurnos >= 2) { $aux = 1; $aux2 = 2; ?>
                        <!--SEGUNDO TURNO--> 
                        
                        <?php for ($i = $iniT2; $i < $finT2; $i++){?> 
                            <tr style= "background: <?php  if($periodOEEH[$i] == 0 ){ echo '#F2F3F4';} ?> ;" >
                                <?php for ($j = 0; $j < 22; $j++){ ?>
                                    <!--<td>-->
                                        <?php
                                            switch ($j){
                                                case 0:
                                                    echo "<td width='2.8%'>";
                                                        echo $i,'-',$i+1;
                                                    break;
                                                case 1:
                                                    echo "<td align='center'  width = '2.5%'>";
                                                    //echo "<td width='120vh'>";
                                                        echo $minT[$i];
                                                    break;
                                                case 2:
                                                    ?>
                                                        <td align='center' width='2.4%' style= " background: #cacfd2 " >
                                                    <?php
                                                    //echo "<td align='center'  width='2.4%'>";
                                                        echo $pzasTargetH[$i];
                                                    break;
                                                case 3:
                                                    echo "<td align='center' width='2.4%'>";
                                                        echo $pzasAcumTarget[$i];
                                                    break;
                                                case 4:
                                                    ?>
                                                        <td align="center" width='2.8%' style= " background: <?php  if( ($pzasTargetRealH[$i]*100)/$pzasTargetH[$i] < $targetOEE ){ echo '#cd6155'; /*color rojo*/;} else {echo '#58d68d' /*color verde*/;} ?>; color: black;">
                                                    <?php
                                                    //echo "<td width='25vh'>";
                                                       echo $pzasTargetRealH[$i];
                                                    break;
                                                case 5:
                                                    echo "<td align='center' width='2.4%'>";
                                                        echo $pzasAcumTargetRealH[$i];
                                                    break;
                                                case 6:
                                                    ?>
                                                        <td align="center" width='2.8%' style= " background: <?php 
                                                        if( ($pzasProdH[$i]*100)/$pzasTargetH[$i] > $targetOEE ){ 
                                                            echo '#58d68d';                                                            
                                                        } else { 
                                                            if ($pzasProdH[$i] == 0){
                                                                echo '#B3B6B7';
                                                            } else {
                                                                if ( $pzasTargetRealH[$i] > $pzasProdH[$i] ){
                                                                    echo '#cd6155';
                                                                } else {
                                                                    echo '#f7dc6f'; 
                                                                }                                                          
                                                            }                                                          
                                                        } ?>; 
                                                        color: black;">
                                                    <?php 
                                                        echo $pzasProdH[$i];
                                                    break;
                                                case 7:
                                                    echo "<td align='center' width='2.4%'>";
                                                        echo $acumuladoPzasProdH[$i];
                                                    break;
                                                case 8:
                                                    echo "<td align='center' width = '3.5%'>";
                                                    ?>
                                                    <div id="<?php echo 'container'.$i ?>" style="min-width: 5px;">
                                                       <script>
                                                            Highcharts.chart('<?php echo 'container'.$i ?>', {
                                                                    chart: {
                                                                        type: 'column',
                                                                        inverted: true,
                                                                        height: 50
                                                                        
                                                                    },
                                                                    title: {
                                                                        text: ''
                                                                    },
                                                                    xAxis: {
                                                                        categories: (function() {
                                                                                var data = [];
                                                                                data.push([<?php echo $i;?>]);
                                                                                return data;
                                                                            })(),
                                                                        visible: false,
                                                                        gridLineWidth: 10
                                                                    },
                                                                    yAxis: [{
                                                                        min: 0,
                                                                        title: {
                                                                            text: ''
                                                                        },
                                                                        visible: false,
                                                                        gridLineWidth: 10,
                                                                        tickInterval: 100
                                                                    }],
                                                                    legend: {
                                                                        shadow: false
                                                                    },
                                                                    tooltip: {
                                                                         style: {
                                                                            hideDelay: .0001,
                                                                            padding: 10 
                                                                        }
                                                                    },
                                                                    plotOptions: {
                                                                        column: {
                                                                            grouping: false,
                                                                            shadow: false,
                                                                            borderWidth: 0
                                                                        },
                                                                        style: {
                                                                            hideDelay: false,
                                                                            padding: 8
                                                                        }
                                                                    },
                                                                    series: [{
                                                                        name: 'Meta',
                                                                        showInLegend: false,  
                                                                        color: '#839192',
                                                                        data: (function() {
                                                                            var data = [];
                                                                            data.push([<?php echo $pzasTargetH[$i];?>]);
                                                                            return data;
                                                                        })(), 
                                                                        pointPadding: -0.7,
                                                                    }, {
                                                                        name: 'Real',
                                                                        showInLegend: false,  
                                                                        color: '#5dade2',
                                                                        data: (function() {
                                                                            var data = [];
                                                                            data.push([<?php echo $pzasProdH[$i];?>]);
                                                                            return data;
                                                                        })(), 
                                                                        pointPadding: -0.7,
                                                                    }], credits: {
                                                                        enabled: false
                                                                     }
                                                                });
                                                        </script>      
                                                    </div>
                                                    <?php
                                                    break;
                                                case 9:
                                                    echo "<td align='center' width = '7.0%'>";
                                                        echo $typeH[$i];
                                                    break;
                                                case 10:
                                                    echo "<td align='center' width='2.8%'>";
                                                        echo $pzasScrapH[$i];
                                                    break;
                                                case 11:
                                                    echo "<td align='center' width='2.8%'>";
                                                        echo $reworkH[$i];
                                                    break;
                                                case 12:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durCambioH[$i];
                                                    break;
                                                case 13:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durTecnicasH[$i];
                                                    break;
                                                case 14:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durOrgH[$i];
                                                    break;
                                                case 15:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durDesempenioH[$i];
                                                    break;
                                                case 16:
                                                    echo "<td width='2.4%'>";
                                                        echo $codeHX[$i];
                                                    break;
                                                case 17:
                                                    echo "<td width='17%'>";
                                                        echo $descriptionHX[$i];
                                                    break;
                                                case 18:
                                                    echo "<td width='2.4%'>";
                                                        echo $codeHY[$i];
                                                    break;
                                                case 19:
                                                    echo "<td width='17%'>";
                                                        echo $descriptionHY[$i];
                                                    break;
                                                case 20:
                                                    echo "<td align='center' width='3.0%'>";
                                                        echo $periodOEEH[$i];
                                                    break;                                                
                                                case 21:
                                                    echo "<td align='center' width='3.0%'>";
                                                        echo $OEEAcumH[$i];
                                                    break;
                                            }
                                        ?>                                
                                    </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                            

                        <!--DIVICION ENTRE 2 y 3 TURNO-->
                        <tr style= " background: <?php  if($OEEAcumH[$finT2-1] < $targetOEE ){ echo '#cd6155';} else {echo '#52be80';} ?> ;" height="10px" >                       
                            <td colspan="2">2. Shift</td>
                            <td>Total</td>
                            <td> <?PHP echo $pzasAcumTarget[$finT2-1] ?> </td>
                            <td colspan="2">  <?php  echo $pzasAcumTargetRealH[$finT2-1] ?></td>
                            <td colspan="2">  <?php  echo $acumuladoPzasProdH[$finT2-1]?></td>
                            <td></td>
                            <td></td>
                            <td> <?php 
                                $sumPzasScrap3 = 0;
                                for ($i = $iniT2; $i< ($iniT2+$durT2); $i++){
                                    $sumPzasScrap3 = $sumPzasScrap3 + $pzasScrapH[$i];
                                }
                                echo $sumPzasScrap3;
                            ?></td>
                            <td></td>
                            <td> <?php 
                                $sumDurCambios3 = 0;
                                for ($i = $iniT2; $i< ($iniT2+$durT2); $i++){
                                    $sumDurCambios3 = $sumDurCambios3 + $durCambioH[$i];
                                }
                                echo $sumDurCambios3;
                            ?> </td>
                            <td> <?php 
                                $sumDurTecnicas3 = 0;
                                for ($i = $iniT2; $i< ($iniT2+$durT2); $i++){
                                    $sumDurTecnicas3 = $sumDurTecnicas3 + $durTecnicasH[$i];
                                }
                                echo $sumDurTecnicas3;
                            ?> </td>
                            <td>
                                <?php 
                                    $sumDurOrganizacionales3 = 0;
                                    for ($i = $iniT2; $i< ($iniT2+$durT2); $i++){
                                        $sumDurOrganizacionales3 = $sumDurOrganizacionales3 + $durOrgH[$i];
                                    }
                                    echo $sumDurOrganizacionales3;
                                ?>
                            </td>
                            <td>
                                <?php 
                                    $sumDurDesempenio3 = 0;
                                    for ($i = $iniT2; $i< ($iniT2+$durT2); $i++){
                                        $sumDurDesempenio3 = $sumDurDesempenio3 + $durDesempenioH[$i];
                                    }
                                    echo $sumDurDesempenio3;
                                ?>
                            </td>                            
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td> 2. Shift </td>
                            <td> <?php echo $OEEAcumH[$finT2-1],'%'  ?> </td>                        
                        </tr>

                    <?php } if ($nTurnos >= 3 ) {?>
                        <!--TERCER TURNO--> 
                        <?php for ($i = $iniT3; $i < $finT3; $i++){?> 
                            <tr style= "background: <?php  if($periodOEEH[$i] == 0 ){ echo '#F2F3F4';} ?> ;" >
                                <?php for ($j = 0; $j < 22; $j++){ ?>
                                    <!--<td>-->
                                        <?php
                                            switch ($j){
                                                case 0:
                                                    echo "<td width='2.8%'>";
                                                        echo $i,'-',$i+1;
                                                    break;
                                                case 1:
                                                    echo "<td align='center' width = '2.5%'>";
                                                    //echo "<td width='120vh'>";
                                                        echo $minT[$i];
                                                    break;
                                                case 2:
                                                    ?>
                                                    <td align='center' width='2.4%' style= " background: #cacfd2 " >
                                                    <?php
                                                    //echo "<td align='center' width='2.4%' style = 'background: #b3b6b7'>";
                                                        echo $pzasTargetH[$i];
                                                    break;
                                                case 3:
                                                    echo "<td align='center' width='2.4%'>";
                                                        echo $pzasAcumTarget[$i];
                                                    break;
                                                case 4:
                                                    ?>
                                                        <td align="center" width='2.8%' style= " background: <?php  if( ($pzasTargetRealH[$i]*100)/$pzasTargetH[$i] < $targetOEE ){ echo '#cd6155'; /*color rojo*/;} else {echo '#58d68d' /*color verde*/;} ?>; color: black;">
                                                    <?php
                                                    //echo "<td width='25vh'>";
                                                       echo $pzasTargetRealH[$i];
                                                    break;
                                                case 5:
                                                    echo "<td align='center' width='2.4%'>";
                                                        echo $pzasAcumTargetRealH[$i];
                                                    break;
                                                case 6:
                                                    ?>
                                                        <td align="center" width='2.8%' style= " background: <?php 
                                                        if( ($pzasProdH[$i]*100)/$pzasTargetH[$i] > $targetOEE ){ 
                                                            echo '#58d68d';                                                            
                                                        } else { 
                                                            if ($pzasProdH[$i] == 0){
                                                                echo '#B3B6B7';
                                                            } else {
                                                                if ( $pzasTargetRealH[$i] > $pzasProdH[$i] ){
                                                                    echo '#cd6155';
                                                                } else {
                                                                    echo '#f7dc6f'; 
                                                                }                                                          
                                                            }                                                          
                                                        } ?>; 
                                                        color: black;">
                                                    <?php 
                                                        echo $pzasProdH[$i];
                                                    break;
                                                case 7:
                                                    echo "<td align='center' width='2.4%'>";
                                                        echo $acumuladoPzasProdH[$i];
                                                    break;
                                                case 8:
                                                    echo "<td width = '3.5%'>";
                                                    ?>
                                                    <div id="<?php echo 'container'.$i ?>" >
                                                        <script>
                                                            Highcharts.chart('<?php echo 'container'.$i ?>', {
                                                                chart: {
                                                                    type: 'column',
                                                                    inverted: true,
                                                                    height: 50,

                                                                },
                                                                title: {
                                                                    text: ''
                                                                },
                                                                xAxis: {
                                                                    categories: (function() {
                                                                            var data = [];
                                                                            data.push([<?php echo $i;?>]);
                                                                            return data;
                                                                        })(),
                                                                    visible: false,
                                                                    gridLineWidth: 10
                                                                },
                                                                yAxis: [{
                                                                    min: 0,
                                                                    title: {
                                                                        text: ''
                                                                    },
                                                                    visible: false,
                                                                    gridLineWidth: 10,
                                                                    tickInterval: 100
                                                                }],
                                                                legend: {
                                                                    shadow: false
                                                                },
                                                                tooltip: {
                                                                     style: {
                                                                        hideDelay: .0001,
                                                                        padding: 10 
                                                                    }
                                                                },
                                                                plotOptions: {
                                                                    column: {
                                                                        grouping: false,
                                                                        shadow: false,
                                                                        borderWidth: 0
                                                                    },
                                                                    style: {
                                                                        hideDelay: false,
                                                                        padding: 8
                                                                    }
                                                                },
                                                                series: [{
                                                                    name: 'Meta',
                                                                    showInLegend: false,  
                                                                    color: '#839192',
                                                                    data: (function() {
                                                                        var data = [];
                                                                        data.push([<?php echo $pzasTargetH[$i];?>]);
                                                                        return data;
                                                                    })(), 
                                                                    pointPadding: -0.7,
                                                                }, {
                                                                    name: 'Real',
                                                                    showInLegend: false,  
                                                                    color: '#5dade2',
                                                                    data: (function() {
                                                                        var data = [];
                                                                        data.push([<?php echo $pzasProdH[$i];?>]);
                                                                        return data;
                                                                    })(), 
                                                                    pointPadding: -0.7,
                                                                }], credits: {
                                                                    enabled: false
                                                                 }
                                                            });
                                                        </script>    
                                                    </div>
                                                    <?php
                                                    break;
                                                case 9:
                                                    echo "<td align='center' width = '7.0%'>";
                                                        echo $typeH[$i];
                                                    break;
                                                case 10:
                                                    echo "<td align='center' width='2.8%'>";
                                                        echo $pzasScrapH[$i];
                                                    break;
                                                case 11:
                                                    echo "<td align='center' width='2.8%'>";
                                                        echo $reworkH[$i];
                                                    break;
                                                case 12:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durCambioH[$i];
                                                    break;
                                                case 13:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durTecnicasH[$i];
                                                    break;
                                                case 14:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durOrgH[$i];
                                                    break;
                                                case 15:
                                                    echo "<td align='center' width='4.0%'>";
                                                        echo $durDesempenioH[$i];
                                                    break;
                                                case 16:
                                                    echo "<td width='2.4%'>";
                                                        echo $codeHX[$i];
                                                    break;
                                                case 17:
                                                    echo "<td width='17%'>";
                                                        echo $descriptionHX[$i];
                                                    break;
                                                case 18:
                                                    echo "<td width='2.4%'>";
                                                        echo $codeHY[$i];
                                                    break;
                                                case 19:
                                                    echo "<td width='17%' >";
                                                        echo $descriptionHY[$i];
                                                    break;
                                                case 20:
                                                    echo "<td align='center' width='3.0%'>";
                                                        echo $periodOEEH[$i];
                                                    break;
                                                case 21:
                                                    echo "<td align='center' width='3.0%'>";
                                                        echo $OEEAcumH[$i];
                                                    break;
                                            }
                                        ?>                                
                                    </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>

                        <!--DIVICION ENTRE 3 y 4 TURNO-->
                        <tr style= " background: <?php  if($OEEAcumH[$finT3-1] < $targetOEE ){ echo '#cd6155';} else {echo '#52be80';} ?> ;">                       
                            <td colspan="2">3. Shift</td>
                            <td>Total</td>
                            <td> <?PHP echo $pzasAcumTarget[$finT3-1] ?> </td>
                            <td colspan="2"> <?php  echo $pzasAcumTargetRealH[$finT3-1] ?> </td>
                            <td colspan="2"> <?php  echo $acumuladoPzasProdH[$finT3-1] ?> </td>
                            <td></td>
                            <td></td>
                            <td> <?php 
                                $sumPzasScrap3 = 0;
                                for ($i = $iniT3; $i < 24; $i++){
                                    $sumPzasScrap3 = $sumPzasScrap3 + $pzasScrapH[$i];
                                }
                                echo $sumPzasScrap3;
                            ?></td>
                            <td></td>
                            <td> <?php 
                                $sumDurCambios3 = 0;
                                for ($i = $iniT3; $i < 24; $i++){
                                    $sumDurCambios3 = $sumDurCambios3 + $durCambioH[$i];
                                }
                                echo $sumDurCambios3;
                            ?> </td>
                            <td> <?php 
                                $sumDurTecnicas3 = 0;
                                for ($i = $iniT3; $i < 24; $i++){
                                    $sumDurTecnicas3 = $sumDurTecnicas3 + $durTecnicasH[$i];
                                }
                                echo $sumDurTecnicas3;
                            ?> </td>
                            <td>
                                <?php 
                                    $sumDurOrganizacionales3 = 0;
                                    for ($i = $iniT3; $i < 24; $i++){
                                        $sumDurOrganizacionales3 = $sumDurOrganizacionales3 + $durOrgH[$i];
                                    }
                                    echo $sumDurOrganizacionales3;
                                ?>
                            </td>
                            <td>
                                <?php 
                                    $sumDurDesempenio3 = 0;
                                    for ($i = $iniT3; $i < 24; $i++){
                                        $sumDurDesempenio3 = $sumDurDesempenio3 + $durDesempenioH[$i];
                                    }
                                    echo $sumDurDesempenio3;
                                ?>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td> 3. Shift </td>
                            <td> <?php echo $OEEAcumH[$finT3-1],'%' ?> </td>                     
                        </tr>
                    <?php } ?>
                </tbody>
                <tfoot width = '100%'>
                    <!--PROCENTAJES E INDICADORES TOTAlES DE HOURLY--> 
                    <tr  style= " background: <?php echo '#d5dbdb' ?> ;" height="25vh" width = '100%' >                       
                        <td width='8.8%'>Daily loss percent</td>
                        <td width='4.8%'></td>
                        <td width='4.8%'>  <?php  echo $pFnProd,'%'; ?> </td>
                        <td width='12.8%'></td>
                        <td width='7.5%'></td>
                        <td width='2.8%'></td>
                        <td width='2.8%'></td>
                        <td width='4.8%'> <?php echo $pFnCam,'%' ?></td>
                        <td width='4.8%'> <?php echo $pFnTec,'%'  ?></td> 
                        <td width='4.8%'> <?php echo $pFnOrg,'%'  ?></td>
                        <td width='4.8%'> <?php echo $pFnTFal,'%'  ?></td>                        
                        <td width='2.8%'></td>
                        <td width='17.8%'></td>                        
                        <td width='2.8%'></td>
                        <td width='17.8%'></td>
                        <td width='3.0%'></td>
                        <td width='3.0%'></td>
                        <td width='10px'></td>
                    </tr>

                </tfoot>
                <tfoot>
                    <tr style="background:  #d5dbdb; "  height="25vh" width = '100%'>                       
                        <td width='4.8%'>Dayli Total</td>
                        <td width='2.8%'>Total</td>
                        <td width='2.8%'> <?php  echo $totalPzasTarget ?>  </td>
                        <td width='4.8%'> <?php  echo $totalPzasTargetReal ?>  </td>
                        <td width='4.8%'> <?php  echo $totalPzasProducidas ?> </td>
                        <td width='12.8%'></td>
                        <td width='7.5%'>Dayli Total</td>
                        <td width='2.8%'> <?php echo $pzasTotalScrap ?></td>
                        <td width='2.8%'></td>
                        <td width='4.8%'> <?php echo $durTotalCambios ?></td>
                        <td width='4.8%'> <?php echo $durTotalTecnicas ?> </td>
                        <td width='4.8%'> <?php echo $durTotalOrg ?> </td>
                        <td width='4.8%'> <?php echo $durTotalDesempenio ?></td>
                        <td width='2.8%'></td>
                        <td width='17.8%'></td>                        
                        <td width='2.8%'></td>
                        <td width='17.8%'></td>
                        <td width='3.0%'></td>
                        <td width='3.0%'></td>
                        <td width='85px'></td>
                    </tr>
                </tfoot>
           </table>
        </div>
    </body>
</html>