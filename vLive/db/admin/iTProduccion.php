<?php
    
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    include '../ServerFunctions.php'; 
    $date = new DateTime; 
    
    $mes = date("m", strtotime($_POST["mPickerMP"])); 
    $anio = date("Y", strtotime($_POST["mPickerMP"])); 
    $linea = $_POST["lineaTP"]; 
    
    #RECIBE PARAMENTROS DE LA VENTANA 
    $eTMes = 0; 
    $cTargets = cExisteTargetsMesLineaProduccion($anio, $mes, $linea); 
    $eTMes = $cTargets[0][0]; 
    if ($eTMes != 0 ) { 
        $errors [] = "EL MES SELECCIONADO YA CUENTA CON TARGETS ASIGNADOS"; 
    } else { 
    ?>
        <div class="alert alert-warning" role="alert"> 
            <button type="button" class="close" data-dismiss="alert">&times;</button> 
            <strong> CARGANDO... </strong> 
        </div>
    <?php 
        $day = 1;     

        $date->setISODate("$anio", 53); 
        # Si estamos en la semana 53 devolvemos 53, sino, es que estamos en la 52 
        if($date->format("W") == 53) { 
            $nWY = 53; 
        } else { 
            $nWY = 52; 
        } 

        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio)); 
        # Obtenemos la semana del primer dia del mes 
        $primeraSemana = date("W",mktime(0,0,0,$mes,1,$anio)); 
        # Obtenemos la semana del ultimo dia del mes 
        $ultimaSemana = date("W",mktime(0,0,0,$mes,$ultimoDiaMes+1,$anio)); 
        $cW = $ultimaSemana - $primeraSemana;     

        if ($ultimaSemana > $primeraSemana ){
            for ($i = 0; $i <= $cW; $i++) { 
                $indWeek = $primeraSemana + $i; 

                $meta = 0;
                if (isset($_POST["meta".$indWeek]) && !empty($_POST["meta".$indWeek])){ 
                    $meta = $_POST["meta".$indWeek]; 
                } 

                $day = ($i * 7) +1 ;  
                $diaSemana = date("w",mktime(0,0,0,$mes,$day,$anio)); 

                # el 0 equivale al domingo... 
                if($diaSemana == 0) 
                    $diaSemana = 7; 

                $tipo = (int) $_POST["cW".$indWeek]; 
                if ($tipo == 1) { //L-V  
                    # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
                    $pDia = date("d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                    if ($pDia > 1 && $i == 0 ) { 
                        $primerDia = "$anio-$mes-01"; 
                    } else { 
                        $primerDia = date("Y-m-d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                        $primerDia = date("Y-m-d",strtotime($primerDia ."+ 1 days")); 
                    } 

                    # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
                    $uDia = date("d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    if ($uDia < 8 && $i == $cW){ 
                        $ultimoDia = "$anio-$mes-$ultimoDiaMes"; 
                    } else { 
                        $ultimoDia = date("Y-m-d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    }

                    for ($j = $primerDia; $j < $ultimoDia; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        iTargetProduccion($linea, $j, $meta); 
                    } 
                } else { //TODOS LOS DIAS  
                    # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
                    $pDia = date("d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                    if ($pDia > 1 && $i == 0 ) { 
                        $primerDia = "$anio-$mes-01"; 
                    } else { 
                        $primerDia = date("Y-m-d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                    } 

                    # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
                    $uDia = date("d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    if ($uDia < 8 && $i == $cW) { 
                        $ultimoDia = "$anio-$mes-$ultimoDiaMes"; 
                    } else { 
                        $ultimoDia = date("Y-m-d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    } 
                    
                    for ($j = $primerDia; $j <= $ultimoDia; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        iTargetProduccion($linea, $j, $meta); 
                    } 
                } 
            } 
        } else { //ULTIMAS SEMANAS DEL AÑO 
            $cont = 0; 
            for ($i = $primeraSemana; $i <= $nWY; $i++){ 
                $indWeek = $i; 

                $meta = 0;
                if (isset($_POST["meta".$indWeek]) && !empty($_POST["meta".$indWeek])){ 
                    $meta = $_POST["meta".$indWeek]; 
                } 

                $day = ($cont * 7) +1 ;  
                $diaSemana = date("w",mktime(0,0,0,$mes,$day,$anio)); 

                # el 0 equivale al domingo... 
                if($diaSemana == 0) 
                    $diaSemana = 7; 

                $tipo = (int) $_POST["cW".$indWeek]; 
                if ($tipo == 1) { //L-V  
                    # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
                    $pDia = date("d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                    if ($pDia > 1 && $i == 0 ) { 
                        $primerDia = "$anio-$mes-01"; 
                    } else { 
                        $primerDia = date("Y-m-d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                        $primerDia = date("Y-m-d",strtotime($primerDia ."+ 1 days")); 
                    } 

                    # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
                    $uDia = date("d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    if ($uDia < 8 && $i == $cW){ 
                        $ultimoDia = "$anio-$mes-$ultimoDiaMes"; 
                    } else { 
                        $ultimoDia = date("Y-m-d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    }

                    for ($j = $primerDia; $j < $ultimoDia; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        iTargetProduccion($linea, $j, $meta); 
                    } 
                } else { //TODOS LOS DIAS  
                    # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
                    $pDia = date("d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                    if ($pDia > 1 && $i == 0 ) { 
                        $primerDia = "$anio-$mes-01"; 
                    } else { 
                        $primerDia = date("Y-m-d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                    } 

                    # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
                    $uDia = date("d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    if ($uDia < 8 && $i == $cW) { 
                        $ultimoDia = "$anio-$mes-$ultimoDiaMes"; 
                    } else { 
                        $ultimoDia = date("Y-m-d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    } 

                    for ($j = $primerDia; $j <= $ultimoDia; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        iTargetProduccion($linea, $j, $meta); 
                    } 
                } 
            } 

            for ($i = 1; $i <= $ultimaSemana; $i++) { 
                $indWeek = $i; 

                $meta = 0; 
                if (isset($_POST["meta".$indWeek]) && !empty($_POST["meta".$indWeek])){ 
                    $meta = $_POST["meta".$indWeek]; 
                } 

                $day = ($cont * 7) +1 ;  
                $diaSemana = date("w",mktime(0,0,0,$mes,$day,$anio)); 

                # el 0 equivale al domingo... 
                if($diaSemana == 0) 
                    $diaSemana = 7; 

                $tipo = (int) $_POST["cW".$indWeek]; 
                if ($tipo == 1) { //L-V  
                    # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
                    $pDia = date("d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                    if ($pDia > 1 && $i == 0 ) { 
                        $primerDia = "$anio-$mes-01"; 
                    } else { 
                        $primerDia = date("Y-m-d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                        $primerDia = date("Y-m-d",strtotime($primerDia ."+ 1 days")); 
                    } 

                    # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
                    $uDia = date("d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    if ($uDia < 8 && $i == $cW){ 
                        $ultimoDia = "$anio-$mes-$ultimoDiaMes"; 
                    } else { 
                        $ultimoDia = date("Y-m-d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    }

                    for ($j = $primerDia; $j < $ultimoDia; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        iTargetProduccion($linea, $j, $meta); 
                    } 
                } else { //TODOS LOS DIAS  
                    # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes 
                    $pDia = date("d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                    if ($pDia > 1 && $i == 0 ) { 
                        $primerDia = "$anio-$mes-01"; 
                    } else { 
                        $primerDia = date("Y-m-d",mktime(0,0,0,$mes,$day-$diaSemana,$anio)); 
                    } 

                    # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo 
                    $uDia = date("d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    if ($uDia < 8 && $i == $cW) { 
                        $ultimoDia = "$anio-$mes-$ultimoDiaMes"; 
                    } else { 
                        $ultimoDia = date("Y-m-d",mktime(0,0,0,$mes,$day+(8-$diaSemana-2),$anio)); 
                    } 

                    for ($j = $primerDia; $j <= $ultimoDia; $j = date("Y-m-d", strtotime($j ."+ 1 days"))) { 
                        iTargetProduccion($linea, $j, $meta); 
                    } 
                } 
            }         
        } 
    }
    
    
    if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert"> 
        <button type="button" class="close" data-dismiss="alert">&times;</button> 
        <strong><?php foreach ($errors as $error) { 
            echo $error; 
        } ?> </strong> 
        </div> 
    <?php } else { ?>
        <div class="alert alert-success" role="alert" > 
            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
            <strong> ¡DATOS GUARDADOS CORRECTAMENTE! </strong> 
        </div> 
        <script>
            location.href = "<?php echo "./targetsp.php" ;?>"; 
        </script>
    <?php 

    }
    
    