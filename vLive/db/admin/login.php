<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    require_once '../ServerFunctions.php';    
    session_start();
    $usuario = strtoupper($_POST["username"]);
    $password = strtoupper($_POST["password"]); 
    $path = $_SESSION["path"]; 
     
    //MANDAMOS TRAER LOS DATOS DE LAS AUDITORIAS POR USUARIO
    $datConsultaLogin = cUsuarioContrasenia($usuario, $password); 
    if (count($datConsultaLogin) > 0) { 
        $producto = $datConsultaLogin[0][0]; 
        $linea = $datConsultaLogin[0][1]; 
        $usuario = $datConsultaLogin[0][2]; 
        $nombre = $datConsultaLogin[0][3].' '.$datConsultaLogin[0][4];         
        $lastName = explode(" ", $datConsultaLogin[0][4]);        
        $nickName = substr($datConsultaLogin[0][3],0,1).'. '.$lastName[0]; 
        $puesto = $datConsultaLogin[0][5]; 
        $privilegio = $datConsultaLogin[0][6]; 
        $correo = $datConsultaLogin[0][7]; 
        $estado = $datConsultaLogin[0][9]; 
        
        if ($estado != 0 ){
            $_SESSION['usuario'] = $usuario;
            $_SESSION['nameUsuario'] = $nombre;
            $_SESSION["nickName"] = $nickName;
            $_SESSION['privilegio'] = $privilegio; 
            $_SESSION["lineaU"] = $linea;
            $_SESSION['productoU'] = $producto;
            $_SESSION["puestoU"] = $puesto; 
            $_SESSION["correo"] = $correo;
        } else {
            $errors []= "El usuario esta deshabilitado :( ";
        }                      
    } else {
        $errors []= "Verifica Usuario y Contraseña";
    }
    
    //OBTENEMOS EL PATH PARA PODER REDIRECCIONAR LA PAGINA EN LA QUE ME ENCUENTRO
    $ruta = split ("/", $path); 
    $c = count($ruta)-1;
    $archivo = $ruta[$c];
    
//    MODULO PARA IMPRIMIR ERRORES
    if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert"> 
        <button type="button" class="close" data-dismiss="alert">&times;</button> 
        <strong><?php foreach ($errors as $error) { 
            echo $error; 
        } ?> </strong> 
        </div> 
    <?php } else { 
        ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>BIENVENIDO</strong>     
        </div>
        <script>
            location.href = "<?php echo "./".$archivo;?>"; 
        </script>        
        <?php
    } 
?>
