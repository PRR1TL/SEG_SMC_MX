<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include '../ServerFunctions.php';


if ( (isset($_POST["linea"]) && isset($_POST["fRegistro"]) && isset($_POST["vmax"]) && 
      isset($_POST["vmin"]) && isset($_POST["vreal"])) && (!empty($_POST["linea"]) && 
      !empty($_POST["fRegistro"])  && !empty($_POST["vmax"])  ) ){
    
    $linea = $_POST["linea"];
    $fecha = date("Y-m-d", strtotime($_POST["fRegistro"]));
    $max = $_POST["vmax"];
    $min = $_POST["vmin"];
    $real = $_POST["vreal"];   
} else {
    $errors[] = "VERIFICA QUE LOS DATOS SEAN CORRECTOS";
}

 if (isset($errors)){ 
?> 
    <div class="alert alert-danger" role="alert"> 
    <button type="button" class="close" data-dismiss="alert">&times;</button> 
    <strong><?php foreach ($errors as $error) { 
        echo $error; 
    } ?> </strong> 
    </div> 
<?php } else { 
    #INSERCION DE LOS DATOS DE ACUERDO AL TIPO Y MANDAMOS NOTIFICACION 
    echo $linea,', ', $fecha,', ', $max,', ', $min,', ', $real;
    iInventario($fecha, $linea, $max, $min, $real);
          
    ?>
    <div class="alert alert-success" role="alert"> 
        <button type="button" class="close" data-dismiss="alert">&times;</button> 
        <strong>DATOS GUARADOS CORRECTAMENTE </strong> 
    </div>     
    <?php
} 