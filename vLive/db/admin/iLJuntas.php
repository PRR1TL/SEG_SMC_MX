<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
    include '../ServerFunctions.php'; 
    session_start(); 
    
    $path = $_SESSION["path"]; 
    $ruta = split ("/", $path); 
    $c = count($ruta)-1;
    $archivo = $ruta[$c];
    
    # TIPO DE JUNTA
    # 1: LINEA
    # 2: CADENA DE VALOR
    # 3: MOES
    
    $area = $_POST['cmbLineaJ']; 
    if (isset($_POST['fRegistro'])){
        $fecha = date('Y-m-d', strtotime($_POST['fRegistro']));
    } else {
        $fecha = date('Y-m-d', strtotime($_POST['fecha'])); 
        $tJunta = 1; 
    } 
    //CONSULTA PARA LOS REGISTROS LISTA DE ASISTENCIA A JUNTA ESE DIA 
    $cExisteJunta = cExistLJuntaLinea($fecha, $area, $tJunta); 
    if (count($cExisteJunta) == 0){ 
        if ($_POST['contU'] > 0 && !empty($_POST['contU'])){ 
            $cont = $_POST['contU'];             
            for($i = 0; $i <= $cont; $i++ ){ 
                if(!empty($_POST['nombre'.$i]) && !empty($_POST['puesto'.$i])){ 
                    $nombre = $_POST['nombre'.$i]; 
                    $departamento = $_POST['puesto'.$i]; 
                    $tAsistencia = $_POST['tAsistencia'.$i]; 
                    //INSERTAR 
                    iULJunta($fecha, $area, $nombre, $departamento, $tAsistencia, $tJunta); 
                } 
            } 
        } else { 
            $errors [] = "OPS! NO SE ENCUENTRA INFORMACION EL LA LISTA";
        } 
    } else { 
        $errors [] = "OPS! EXISTEN REGISTROS DE LA JUNTA";
    } 
    
    if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert" > 
            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
            <strong><?php foreach ($errors as $error) { 
                echo $error; 
            } ?> </strong> 
        </div> 
    <?php } else { 
        ?>
        <div class="alert alert-success" role="alert" > 
            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
            <strong> REGISTROS GUARDADOS CORRECTAMENTE </strong> 
        </div>
        <?php switch ($tJunta){ 
           case 0: 
           ?>
                <script>
                    //location.href = "<?php //echo "./".$archivo ;?>"; 
                </script>        
            <?php
                break; 
            case 1: //LINEAS
            ?>
                <script>
                    $.ajax({ 
                        url: 'db/sesionReportes.php', 
                        success: function(respuesta) { 
                            $('#pnlX').load('./contenedores/index/linea/4_ILJunta.php'); 
                        }, error: function() { 
                            console.log("No se ha podido obtener la información"); 
                        } 
                    }); 
                </script> 
            <?php
                break; 
            case 2: // CADENA DE VALOR 
            ?>
                 <script>
                    $.ajax({ 
                        url: 'db/sesionReportes.php', 
                        success: function(respuesta) { 
                            $('#pnlX').load('./contenedores/index/linea/4_ILJunta.php'); 
                        }, error: function() { 
                            console.log("No se ha podido obtener la información"); 
                        } 
                    }); 
                </script> 
            <?php
                break; 
            case 3: 
            ?>
                <script>
                    //location.href = "<?php //echo "./".$archivo ;?>"; 
                </script> 
            <?php
                break; 
        }
?>
        <script>
            //location.href = "<?php //echo "./".$archivo ;?>"; 
        </script>        
        <?php
    } 
    
    
    

