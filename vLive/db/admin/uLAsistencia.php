<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../ServerFunctions.php';
    
    #DATOS INFORMATIVOS
    $id = $_POST['uLAId'];
    $fecha = date('Y-m-d', strtotime($_POST['uLAFecha']));   
    
    if (isset($_POST['uLAId']) && isset($_POST['uLAFecha']) && 
        isset($_POST['uLALinea']) && isset($_POST['uLANombre']) && 
        isset($_POST['tAsistencia']) && isset($_POST['uLAHoras']) && isset($_POST['uLOperacion']) ){ 
        
        #DATOS POSIBLES A CAMBIAR 
        $asistencia = $_POST['tAsistencia']; 
        $horas = $_POST['uLAHoras']; 
        
        uListaAsistencia($_POST['uLAId'], $fecha, $_POST['uLALinea'], $asistencia, $horas, $_POST['uLOperacion'] ); 
    } else { 
        $errors [] = 'VERIFICA QUE LOS DATOS ESTEN LLENOS CORRECTAMENTE'; 
    } 
    
    if (isset($errors)){ 
    ?> 
        <div class='alert alert-danger' role='alert' > 
        <button type='button' class='close' data-dismiss='alert' >&times;</button> 
        <strong><?php foreach ($errors as $error) { 
            echo $error; 
        } ?> </strong> 
        </div> 
    <?php } else { ?>
        <div class='alert alert-success' role='alert' > 
            <button type='button' class='close' data-dismiss='alert' >&times;</button> 
            <strong> ¡ACTUALIZACION CORRECTA! </strong> 
        </div>
    <?php 
    }
    
    