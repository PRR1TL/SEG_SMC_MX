<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//CONSULTA PARA NO REPETIR LOS DATOS EN LA BASE DE DATOS 
include '../ServerFunctions.php';
session_start();

$mes = date("m",strtotime($_POST["mPickerM"])); 
$anio = date("Y",strtotime($_POST["mPickerM"])); 

//ULTIMO DIA DEL MES
$ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));  

$fIni = strtotime($anio.'-'.$mes.'-01');
$fFin = strtotime($anio.'-'.$mes.'-'.$ultimoDiaMes);

$linea = $_POST["linea"];
$eTMes = 0;

$cTargets = cExisteTargetsMesLineaLosses($anio, $mes, $linea);             
$eTMes = $cTargets[0][0]; 

if (isset($_POST["dH"])){ 
    $tRegistro = 1; 
} else { 
    $tRegistro = 0; 
} 

if ($eTMes < 1) {
    //VALIDACIONES
    if (empty($_POST["tOEE"]) || empty($_POST["tTec"]) || empty($_POST["tOrg"]) 
     || empty($_POST["tCam"]) || empty($_POST["tProd"])){ 
       $errors [] = "VERIFICA QUE LOS DATOS ESTEN LLENOS CORRECTAMENTE"; 
    } 

    if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert"> 
        <button type="button" class="close" data-dismiss="alert">&times;</button> 
        <strong><?php foreach ($errors as $error) { 
            echo $error; 
        } ?> </strong> 
        </div> 
    <?php } else { ?>
        <div class="alert alert-warning" role="alert"> 
            <button type="button" class="close" data-dismiss="alert">&times;</button> 
            <strong> CARGANDO... </strong> 
        </div>
    <?php 
        
        $oee = $_POST["tOEE"]; 
        $tec = $_POST["tTec"]; 
        $org = $_POST["tOrg"]; 
        $cam = $_POST["tCam"]; 
        $cali = $_POST["tCali"]; 
        $prod = $_POST["tProd"]; 
        $scrap = $_POST["tScrap"]; 
        
        if($tRegistro == 1){
            #RECORRIDO DE FECHA PARA IDENTIFICAR SOLO DIAS HABILES 
            for($fIni; $fIni<=$fFin; $fIni=strtotime('+1 day ' . date('Y-m-d',$fIni))){  
                if((strcmp(date('D',$fIni),'Sun') != 0) && (strcmp(date('D',$fIni),'Sat') != 0)){ 
                    //INSERCION A LA BASE DE DATOS 
                    iTargetLosses($linea, date('Y-m-d',$fIni), $oee, $tec, $org, $cam, $cali, $scrap, $prod); 
                } else {
                    iTargetLosses($linea, date('Y-m-d',$fIni), 0, 0, 0, 0, 0, 0, 0); 
                }
            } 
        } else { 
            #RECORRRIDO DE DIAS 
            //for($i = $fIni; $i <= $fFin; $fIni = strtotime('+1 day ' . date('Y-m-d',$i))){ 
            for($fIni; $fIni<=$fFin; $fIni=strtotime('+1 day ' . date('Y-m-d',$fIni))){   
                //INSERCION A LA BASE DE DATOS 
                iTargetLosses($linea, date('Y-m-d',$fIni), $oee, $tec, $org, $cam, $cali, $scrap, $prod); 
            } 
        } ?>
        <div class="alert alert-success" role="alert" > 
            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
            <strong> ¡DATOS GUARDADOS CORRECTAMENTE! </strong> 
        </div> 
        <script>
            location.href = "<?php echo "./targetsL.php" ;?>"; 
        </script>
    <?php 
        } 
    } else { 
    ?> 
        <div class="alert alert-danger" role="alert" > 
            <button type="button" class="close" data-dismiss="alert">&times;</button> 
            <strong> EL MES SELECCIONADO YA CUENTA CON TARGETS ASIGNADOS </strong> 
        </div> 
<?php 
}
    
    