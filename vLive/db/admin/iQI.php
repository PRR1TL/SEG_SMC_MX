<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../ServerFunctions.php';
    session_start();
    
    $path = $_SESSION["path"]; 
    $ruta = split ("/", $path); 
    $archivo = $ruta[5];
    
    #RECIBIMOS LOS DATOS     
    if ((isset($_POST["linea"]) && isset($_POST["detecta"]) && isset($_POST["fRegistro"]) && 
        isset($_POST["cliente"]) && isset($_POST["noParte"]) &&isset($_POST["descripcion"]) && 
        isset($_POST["nota"]) ) && (!empty($_POST["linea"]) && !empty($_POST["detecta"]) && 
        !empty($_POST["fRegistro"]) && !empty($_POST["cliente"]) && !empty($_POST["noParte"])
        && !empty($_POST["descripcion"]) && !empty($_POST["nota"])) ){ 
        $tipo = $_POST["tipo"]; 
        $linea = $_POST["linea"]; 
        $reporta = $_POST["detecta"]; 
        $fecha = date("Y-m-d", strtotime($_POST["fRegistro"])); 
        $cliente = $_POST["cliente"]; 
        $parte = $_POST["noParte"]; 
        $desc = $_POST["descripcion"]; 
        $nota = $_POST["nota"]; 
    } else { 
        $errors [] = "VERIFICA QUE LOS DATOS ESTEN LLENOS CORRECTAMENTE"; 
    } 
    
    if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert"> 
        <button type="button" class="close" data-dismiss="alert">&times;</button> 
        <strong><?php foreach ($errors as $error) { 
            echo $error; 
        } ?> </strong> 
        </div> 
    <?php } else { 
        #INSERCION DE LOS DATOS DE ACUERDO AL TIPO Y M&&AMOS NOTIFICACION 
        iCalidadInterna($fecha, $linea, $cliente, $parte, $desc, $nota, $reporta, $tipo);
        
        ?>
        <div class="alert alert-success" role="alert"> 
            <button type="button" class="close" data-dismiss="alert">&times;</button> 
            <strong>OPL GUARDADA CORRECTAMENTE </strong> 
        </div>
        <script>
            location.href = "<?php echo "./".$archivo ;?>"; 
        </script>        
        <?php
    } 
    
    
    
