<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../ServerFunctions.php';
    
    #DATOS INFORMATIVOS
    $id = $_POST["idLAD"];
    $fecha = date("Y-m-d", strtotime($_POST["fechaLAD"]));   
    
    if ( isset($_POST["idLAD"]) && !empty($_POST["idLAD"]) ){         
        #DATOS POSIBLES A CAMBIAR 
        $linea = $_POST["lineaLAD"]; 
        
        dListaAsistencia($id, $fecha, $linea); 
    } else { 
        $errors [] = "VERIFICA QUE LOS DATOS ESTEN LLENOS CORRECTAMENTE"; 
    } 
    
    if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert" > 
        <button type="button" class="close" data-dismiss="alert" >&times;</button> 
        <strong><?php foreach ($errors as $error) { 
            echo $error; 
        } ?> </strong> 
        </div> 
    <?php } else { ?>
        <div class="alert alert-success" role="alert" > 
            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
            <strong> ¡ELIMINACION CORRECTA! </strong> 
        </div>
        <script>
            location.href = "<?php echo "./MLAsistencia.php" ;?>"; 
        </script>
    <?php 
    }
    
    