<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    
    <style> 
        /* PROPIEDADES PRA LOS PANEL DE OPL */
        .no { 
            display:none; 
        } 
        .si { 
            display:block; 
        } 
    </style> 
    
</head>

<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    
session_start();
    include '../ServerFunctions.php'; 
    
    if (isset($_POST["linea"]) && !empty($_POST["linea"])){
        $linea = $_POST["linea"];
    } else {
        $linea = $_SESSION["linea"];
    }
    
    $fR = date("Y-m-d", strtotime($_POST['fechaR']));    

    $cUsuarios = cUsuariosLAsistencia_Linea($linea, $fR); 
    $cOperaciones = cAllOperaciones_Linea($linea); 
    for ($i = 0; $i < count($cOperaciones); $i++ ) { 
        if ($linea == $cOperaciones[$i][1] ) { 
            $idOpLinea = $cOperaciones[$i][0]; 
        } 
    } 
        
?>

<table style="width: 100%; ">
    <thead> 
        <tr> 
            <th >PUESTO</th> 
            <th >PERSONAL</th> 
            <th >OPERACION</th> 
            <th >ASISTENCIA</th> 
        </tr> 
    </thead>  
    <tbody> 
<?php 
for ($i = 0; $i < count($cUsuarios); $i++){ 
?> 
    <tr > 
        <td style="width: 2%; " > 
            <input class="no" id="num" name="num" value="<?php echo count($cUsuarios); ?>" > 
            <input id="<?php echo "puesto$i" ?>" name="<?php echo "puesto$i" ?>" value="<?php echo $cUsuarios[$i][0]; ?>" style="border: 0;" readonly >
        </td> 
        <td style="width: 65%; " > 
            <input class="no" id="<?php echo "numEmpleado$i" ?>" name="<?php echo "numEmpleado$i" ?>" value="<?php echo utf8_decode($cUsuarios[$i][2]); ?>" readonly >
            <input style="width: 100%; border: 0; " id="<?php echo "nombre$i" ?>" name="<?php echo "nombre$i" ?>" value="<?php echo utf8_decode($cUsuarios[$i][1]); ?>" readonly >
        </td> 
        <td style="width: 25%; " > 
            <div class="row" > 
                <div class="col-lg-6 col-md-6 " > 
                    <select id="<?php echo "operacion$i" ?>" name="<?php echo "operacion$i" ?>" style="height: 33px; " onchange="operacion(this.name);" > 
                     <?php if ($cUsuarios[$i][0] != 'OP' && $cUsuarios[$i][0] != 'AUD'){ ?> 
                        <option value="<?php echo $idOpLinea; ?>" > <?php echo $linea; ?> </option> 
                     <?php }                          
                        $cOpEmpleado = cOpLAsistencia_Linea_Empleado($linea, $cUsuarios[$i][2]); 
                        for ($j = 0; $j < count($cOpEmpleado); $j++ ){ ?> 
                        <option value="<?php echo $cOpEmpleado[$j][0]; ?>" > <?php echo $cOpEmpleado[$j][1]; ?> </option> 
                    <?php } ?>
                        <option value="X" > OTRA </option> 
                    </select> 
                </div>
                <div class="col-lg-7 col-md-7 no" id="<?php echo "pnloperacion$i" ?>" >
                    <select id="<?php echo "estacion$i" ?>" name="<?php echo "estacion$i" ?>" style="height: 33px; " > 
                    <?php 
                    for ($j = 0; $j < count($cOperaciones); $j++ ){ ?> 
                        <option value="<?php echo $cOperaciones[$j][0]; ?>" > <?php echo $cOperaciones[$j][1]; ?> </option> 
                    <?php } ?>
                    </select> 
                </div> 
            </div> 
        </td>
        <td  >
            <select id="<?php echo "asistencia$i" ?>" name="<?php echo "asistencia$i" ?>" style="height: 33px;" > 
                <option value="0" > ---------- </option> 
                <option value="A" > Asistencia </option> 
                <option value="B" > Baja </option> 
                <option value="C" > Capacitacion </option> 
                <option value="F" > Falta </option> 
                <option value="I" > Incapacidad </option> 
                <option value="P" > Permiso </option> 
                <option value="S" > Suspencion </option> 
                <option value="V" > Vacaciones </option> 
                <option value="R" > Retardo </option> 
            </select>
        </td>
    </tr>
<?php  } ?>
    </tbody>
</table> 


        