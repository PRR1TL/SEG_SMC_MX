<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    //RECIBIMOS LOS VALORES DE LA LISTA DE ASISTENCIA 
    include '../ServerFunctions.php';
    require("../../smtp/class.phpmailer.php");

    $linea = $_POST["cmbLineaLA"]; 
    //INFORMACION GENERAL 
    $horas = $_POST['horas']; 
    if(isset($_POST["fRegistroC"])){
        $fecha = date("Y-m-d", strtotime($_POST["fRegistroC"]));
    } else {
        $fecha = date("Y-m-d", strtotime($_POST["fecha"]));
    }      
    $turno = $_POST['turno']; 
    $num = $_POST["num"]; 
        
    if (isset($fecha) && isset($turno) && isset($horas) && $horas > 1 ){ 
        for($i = 0; $i < $num; $i++){             
            if (isset($_POST["asistencia".$i]) && $_POST["asistencia".$i] != '0' ){ 
                $puesto = $_POST["puesto".$i]; 
                $numEmpleado = $_POST["numEmpleado".$i]; 
                $bnd = 0; //asistencia 
                $tAsistencia = $_POST["asistencia".$i]; 
                
                if ($_POST["operacion".$i] == "X"){ #VALIDACION PARA CUANDO SE SELECCIONA EN OTRA 
                    $operacion = $_POST['estacion'.$i]; 
                    $bnd = 1; 
                } else { #DIRECTO AL INSERT 
                    $operacion = $_POST["operacion".$i]; 
                } 
                iLAsistencia_Linea($fecha, $linea, $turno, $puesto, $operacion, $numEmpleado, $horas, $bnd, $tAsistencia); 
            } 
        } 
        
        if ($num > 0 ){ 
            notificacionLinea($linea, $fecha, $turno);
        }         
    } else { 
        $errors [] = "LOS CAMPOS DE REGISTRO DEBEN ESTAR LLENOS DE FORMA CORRECTA"; 
    } 
    
    if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert"> 
        <button type="button" class="close" data-dismiss="alert">&times;</button> 
        <strong><?php foreach ($errors as $error) { 
            echo $error,'<BR>'; 
        } ?> </strong> 
        </div> 
    <?php } else { ?>
        <div class="alert alert-success" role="alert"> 
            <button type="button" class="close" data-dismiss="alert">&times;</button> 
            <strong>LISTA DE ASISTENCIA GUARDADA CORRECTAMENTE </strong> 
        </div> 
        
        <?php
    } 
    
    function notificacionLinea($linea, $fecha, $turno) {         
        switch ($turno) {
            case 1:
                $tTurno = "Primer Turno";
                break;
            case 2:
                $tTurno = "Segundo Turno";
                break;
            case 3:
                $tTurno = "Tercer Turno";
                break; 
        }
        
        $c = 0;
        $mail = new PHPMailer();
        $mail->isSMTP();
        $mail->SMTPDebug = 2;
        $mail->Debugoutput = 'html';
        //CONFIGURACION DE HOST DE SEG
        $mail->Host = "smtp.sg.lan";
        $mail->Port = 25;
        $mail->SMTPAuth = false;
        $mail->SMTPSecure = false;
        $mail->setFrom("no-reply@seg-automotive.com", "Portal SMC");
        
        $cHRL = cHRLAdmin();

        $mensaje = "Personal en operacion distinta a <b>MATRIZ DE CAPACITACION</b><br><br><b>Linea:</b> $linea / <b>Fecha</b>: $fecha <b> / Turno: </b> $tTurno <br> <br>";
        $correo = "";
        $correo = NULL;
        $correo = $cHRL[0][0];
        $cListFMatriz = cLAsistencia_FMatriz($linea, $fecha, $turno); 
        if (count($cListFMatriz)){ 
            for ($i = 0; $i < count($cListFMatriz); $i++) { 
                $c = $i+1;
                $nombre = $cListFMatriz[$i][0];
                $operacion = $cListFMatriz[$i][1];
                if ($i > 0 ){ 
                    $mensaje2 = $mensaje2. "<tr> 
                            <td>$c</td> 
                            <td>$operacion</td> 
                            <td>$nombre</td> 
                        </tr> "; 
                } else { 
                    $mensaje2 = "<tr> 
                            <td>$c</td> 
                            <td>$operacion</td> 
                            <td>$nombre</td> 
                        </tr>"; 
                }
            } 
            
            if (count($cListFMatriz) > 0){               
                $mensaje = $mensaje."<table border='1' style='border-collapse: collapse;'>
                    <tr> 
                        <th>No.</th> 
                        <th>Operacion</th> 
                        <th>Nombre</th>                       
                    </tr>".$mensaje2."</table>"; 
            } 
            
            //PROXIMOS PUNTOS 
            //ENVIAMOS LA NOTIFICACION DE LA AUDITORIA 
            $mail->addAddress("$correo", "Recepient Name");
            $mail->isHTML(true);
            
            $mail->Subject = "LISTA DE ASISTENCIA";
            $mail->Body = "$mensaje";
            $mail->AltBody = "This is the plain text version of the email content";

            if(!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                unset($correo);
                $correo = "";                
            } 
            $correo = NULL;
            unset($correo); 
        }
    
        ?>
        <script> 
            location.reload(); 
        </script> 
        <?php 
        
    }   
    
    
    
    
    
    

