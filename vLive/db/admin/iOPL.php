<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../ServerFunctions.php';
    session_start();
    $date = new DateTime;
    
    $path = $_SESSION["path"];    
    $ruta = split ("/", $path); 
    $c = count($ruta)-1;
    $archivo = $ruta[$c]; 
  
    $tipo = $_POST["tipOpl"];     
    if (isset($_POST['detecta'])){ 
        $detecta = $_POST["detecta"]; 
    } 
    
    if (isset($_POST["fRegistro"])){ 
        $fRegistra =  date('Y-m-d', strtotime($_POST["fRegistro"]));  
    } else {
        $fRegistra =  date('Y-m-d');
    }
    
    if ($tipo == 1) {  //PROYECTO 
        if ( (isset($_POST["proyecto"]) && isset($_POST["accionP"]) && 
            isset($_POST["areaP"]) && isset($_POST["responsableP"]) && 
            isset($_POST["fCompromisoP"])) && 
            (!empty($_POST["proyecto"]) && !empty($_POST["accionP"]) && 
            !empty($_POST["areaP"]) && !empty($_POST["responsableP"]) && 
            !empty($_POST["fCompromisoP"])) ) { 
            $idProyecto = $_POST["proyecto"]; 
            $accion = $_POST["accionP"]; 
            $area = $_POST["areaP"]; 
            $responsable = $_POST["responsableP"]; 
            $fCompromiso =  date('Y-m-d', strtotime($_POST["fCompromisoP"])); 
        } else { 
            if (!isset($_POST["proyecto"])){ 
                echo " 1 "; 
            } else if (!isset($_POST["accionP"])){ 
                echo " 2 "; 
            } else if (!isset($_POST["areaP"])){ 
                echo " 3 "; 
            } else if (!isset($_POST["responsableP"])){ 
                echo " 4 "; 
            } else if (!isset($_POST["fCompromisoP"])) { 
                echo " 5 "; 
            }            
            $errors [] = "VERIFICA QUE LOS DATOS ESTEN LLENOS CORRECTAMENTE"; 
        }         
    } else if ($tipo == 2) { //TEMA 
        
    } else if ($tipo == 3) { //LINEA 
        $area = $_POST["linea"]; 
        if ( (isset($_POST["operacion"]) && isset($_POST["hallazgoL"]) && 
            isset($_POST["causaL"]) && isset($_POST["accionL"]) && 
            isset($_POST["responsableL"]) && isset($_POST["fCompromisoL"])) && 
            (!empty($_POST["operacion"]) && !empty($_POST["hallazgoL"]) && 
            !empty($_POST["causaL"]) && !empty($_POST["accionL"]) && 
            !empty($_POST["responsableL"]) && !empty($_POST["fCompromisoL"])) ) { 
                $operacion = $_POST["operacion"]; 
                $hallazgo = $_POST["hallazgoL"]; 
                $causa = $_POST["causaL"]; 
                $accion = $_POST["accionL"]; 
                $responsable = $_POST["responsableL"]; 
                $fCompromiso =  date('Y-m-d', strtotime($_POST["fCompromisoL"])); 
        } else { 
            $errors [] = "VERIFICA QUE LOS DATOS ESTEN LLENOS CORRECTAMENTE"; 
        } 
    } else if ($tipo == 5) { //LINEA         
        $area = $_POST["linea"]; 
        if ((isset($_POST["h"]) && isset($_POST["a"]) && isset($_POST["r"]) && isset($_POST["s"]) && isset($_POST["fF"])) && 
            (!empty($_POST["h"]) && !empty($_POST["a"]) && !empty($_POST["r"]) && !empty($_POST["s"]) && !empty($_POST["fF"])) ) { 
                $operacion = "-"; 
                if (isset($_SESSION['usuario'])){ 
                    $detecta = $_SESSION['usuario']; 
                } else {
                    $detecta = $area; 
                }                 
                $hallazgo = $_POST["h"]; 
                $causa = "-"; 
                $accion = $_POST["a"]; 
                $responsable = $_POST["r"]; 
                $soporte = $_POST["s"]; 
                $fCompromiso =  date('Y-m-d', strtotime($_POST["fF"])); 
        } else { 
            $errors [] = "VERIFICA QUE LOS DATOS ESTEN LLENOS CORRECTAMENTE"; 
        } 
    } 
    
    if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert"> 
        <button type="button" class="close" data-dismiss="alert">&times;</button> 
        <strong><?php foreach ($errors as $error) { 
            echo $error; 
        } ?> </strong> 
        </div> 
    <?php } else { 
        #INSERCION DE LOS DATOS DE ACUERDO AL TIPO Y MANDAMOS NOTIFICACION 
        switch ($tipo) { 
            case 1: //PROYECTO 
                iOplProyecto($idProyecto, $accion, $responsable, $area, $fRegistra, $fCompromiso, $detecta ); 
            ?>
                <div class="alert alert-success" role="alert"> 
                    <button type="button" class="close" data-dismiss="alert">&times;</button> 
                    <strong>OPL GUARDADA CORRECTAMENTE </strong> 
                </div> 
                <script> 
                    location.href = "<?php echo "./".$archivo ;?>"; 
                </script>        
            <?php  
                break;
            case 2: //TEMA 
                
                break; 
            case 3: //LINEA 
                iOplLinea($fRegistra, $detecta, $operacion, $hallazgo, $causa, $accion, $responsable, $fCompromiso, $_SESSION["linea"], 0); 
            ?>
                <div class="alert alert-success" role="alert"> 
                    <button type="button" class="close" data-dismiss="alert">&times;</button> 
                    <strong>OPL GUARDADA CORRECTAMENTE </strong> 
                </div> 
                <script> 
                    location.href = "<?php echo "./".$archivo ;?>"; 
                </script>        
            <?php 
                break;
            case 4: //LINEA DE FALLAS EN MATERIAL
                iOplLinea($fRegistra, $detecta, $operacion, $hallazgo, $causa, $accion, $responsable, $fCompromiso, $_SESSION["linea"], 1); 
                break;
            case 5: //LINEA 5Ss
                iOplLinea($fRegistra, $detecta, $operacion, $hallazgo, $causa, $accion, $responsable, $fCompromiso, $_SESSION["linea"], 2); 
                break;
        }      
    } 
    
        
    