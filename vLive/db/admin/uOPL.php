<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    include '../ServerFunctions.php';
    require("../../smtp/class.phpmailer.php");
    
    session_start();
    $date = new DateTime;
    
    $now = date("Y-m-d"); 
    $path = $_SESSION["path"]; 
    $ruta = split ("/", $path); 
    $c = count($ruta)-1; 
    $archivo = $ruta[$c]; 
    $tipo = $_POST["tipOplLU"];       
    
    if ($tipo == 1) { //PROYECTO         
        if ( (isset($_POST["idOplPU"])  && !empty($_POST["idOplPU"])) ) {             
            $idOpl = $_POST["idOplPU"]; 
            $estado = $_POST["estadoPU"];
            $evaluacion = $_POST["evaluacionPU"];
        } else {             
            $errors [] = "OPS!: UN ERROR A OCURRIDO EN uOPL.php (Linea 31 )"; 
        }         
    } else if ($tipo == 2) { //TEMA
        
    } else if ($tipo == 3) { //LINEA
        if ( (isset($_POST["idOplLU"]) && !empty($_POST["idOplLU"])) ) {             
            $idOpl = $_POST["idOplLU"]; 
            $estado = $_POST["estadoLU"];
        } else {            
            $errors [] = "OPS!: UN ERROR A OCURRIDO EN uOPL.php (Linea 40 )"; 
        }    
    } 
    
    //echo $_POST["fRegistro"],' -> ',$fRegistra,', ', $_POST["fCompromisoP"],' -> ', $fCompromiso,'<BR>';
    
    if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert"> 
        <button type="button" class="close" data-dismiss="alert">&times;</button> 
        <strong><?php foreach ($errors as $error) { 
            echo $error; 
        } ?> </strong> 
        </div> 
    <?php } else { 
        #INSERCION DE LOS DATOS DE ACUERDO AL TIPO Y MANDAMOS NOTIFICACION 
        switch ($tipo) { 
            case 1: //PROYECTO 
                uOplProyecto($now, $estado, $evaluacion, $idOpl);
                break;
            case 2: //TEMA 
                
                break; 
            case 3: //LINEA 
                uOplLinea($estado, $now, $idOpl); 
                notificacionLinea($idOpl);
                break;
        } 
        
        ?>
        <div class="alert alert-success" role="alert"> 
            <button type="button" class="close" data-dismiss="alert">&times;</button> 
            <strong>OPL GUARDADA CORRECTAMENTE </strong> 
        </div>
        <script>
           location.href = "<?php echo "./".$archivo ;?>"; 
        </script>        
        <?php
    } 
    
    //NOTIFICACION DE OPL
    function notificacionLinea($id) { 
        $now = date("Y-m-d");
        $cDatosOpl = cOPLIdL($id); 
        
        for ($i = 0; $i < count($cDatosOpl); $i++ ){
            $actulizaProblema = $cDatosOpl[$i][0]; 
            $actualizaFCompromiso = $cDatosOpl[$i][2]->format('d, M');              
            switch ($cDatosOpl[$i][4]) { 
                case 0:
                    $actualizaEstatus = "Acción no definida";
                    break; 
                case 1: 
                    $actualizaEstatus = "Acción definida";
                    break; 
                case 2: 
                    $actualizaEstatus = "Accion en proceso";
                    break; 
                case 3: 
                    $actualizaEstatus = "Acción ejecutada";
                    break; 
                case 4: 
                    $actualizaEstatus = "Acción cerrada/aprobada";
                    break; 
            }             
            $linea = $cDatosOpl[$i][5]; 
        } 
        
        $anio = date("Y", strtotime($now));
        $mes = date("m", strtotime($now));
        $ultimoDiaMes = date("t",mktime(0,0,0,$mes,1,$anio));   
        
        $dIni = $anio.'-'.$mes.'-01';
        $dFin = $anio.'-'.$mes.'-'.$ultimoDiaMes;
        
        $cULinea = cUsuarioTLinea($linea); 
        for ($i = 0; $i < count($cULinea); $i++) {             
            $mail = new PHPMailer();

            $mail->isSMTP();
            $mail->SMTPDebug = 2;
            $mail->Debugoutput = 'html';
            //CONFIGURACION DE HOST DE SEG
            $mail->Host = "smtp.sg.lan";
            $mail->Port = 25;
            $mail->SMTPAuth = false;
            $mail->SMTPSecure = false;

            $mail->setFrom("no-reply@seg-automotive.com", "Portal SMC");
            
            $mensaje = "<br> El punto <b>$actulizaProblema</b> con fecha comprimiso <b>$actualizaFCompromiso</b> ha sido actualizado, su estatus actual es <b>".utf8_decode($actualizaEstatus)."</b> ";
            $correo = "";
            $correo = NULL;
            $correo = $cULinea[$i][0];
            
            //PUNTOS PENDIENTES 
            $cPPenedientes = cOPLSinCerrarL($linea, $dIni, $dFin, $now); 
            for ($j = 0; $j < count($cPPenedientes); $j++) { 
                $problema = $cPPenedientes[$j][4]; 
                $accion = $cPPenedientes[$j][6]; 
                $estado = $cPPenedientes[$j][9]; 
                $fComp = $cPPenedientes[$j][8]->format('d, M'); 
                $responsable = $cPPenedientes[$j][7]; 
                
                if ($j > 0 ){
                    $mensaje2 = $mensaje2. "<tr> 
                            <td>$j</td> 
                            <td>$problema</td> 
                            <td>$accion</td> 
                            <td>$responsable</td> 
                            <td>$fComp</td> 
                            <td>$estado</td> 
                        </tr> ";
                } else {
                    $mensaje2 = "<tr> 
                            <td>$j</td> 
                            <td>$problema</td> 
                            <td>$accion</td> 
                            <td>$responsable</td> 
                            <td>$fComp</td> 
                            <td>$estado</td> 
                        </tr>"; 
                }
            } 
            
            if (count($cPPenedientes) > 0){ 
                $mensaje = $mensaje." <br><br><b> Puntos pendientes: </b> <br>";                
                $mensaje = $mensaje."<table border='1' style='border-collapse: collapse;'>
                    <tr> 
                        <th>N.P</th> 
                        <th>Hallazgo</th> 
                        <th>Accion</th> 
                        <th>Responsable</th> 
                        <th>F.Compromiso</th> 
                        <th>Estado</th> 
                    </tr>".$mensaje2."</table>"; 
            } 
            
            $cPProximos = cOPLPFuturosP($linea, $dIni, $dFin, $now); 
            for ($j = 0; $j < count($cPProximos); $j++) { 
                $problema = $cPPenedientes[$j][4];
                $accion = $cPPenedientes[$j][6];
                $estado = $cPPenedientes[$j][9];
                $fComp = $cPPenedientes[$j][8]->format('d, M');
                $responsable = $cPPenedientes[$j][7];
                
                if ($j > 0 ){
                    $mensaje3 = $mensaje3. "<tr> 
                            <td>$j</td> 
                            <td>$problema</td> 
                            <td>$accion</td> 
                            <td>$responsable</td> 
                            <td>$fComp</td> 
                            <td>$estado</td> 
                        </tr> ";
                } else {
                    $mensaje3 = "<tr> 
                            <td>$j</td> 
                            <td>$problema</td> 
                            <td>$accion</td> 
                            <td>$responsable</td> 
                            <td>$fComp</td> 
                            <td>$estado</td> 
                        </tr> "; 
                }
            } 
            
            if (count($cPProximos) > 0){ 
                $mensaje = $mensaje." <br><br><b> Puntos proximos: </b>";                
                $mensaje = $mensaje."<br> <table border='1' style='border-collapse: collapse;'>
                    <tr> 
                        <th>N.P</th> 
                        <th>Hallazgo</th> 
                        <th>Accion</th> 
                        <th>Responsable</th> 
                        <th>F.Compromiso</th> 
                        <th>Estado</th> 
                    </tr>".$mensaje3."</table> <br><br>";
            }
            
            //PROXIMOS PUNTOS 
            //ENVIAMOS LA NOTIFICACION DE LA AUDITORIA 
            $mail->addAddress("$correo", "Recepient Name");
            $mail->isHTML(true);

            $mail->Subject = "OPL SMC";
            $mail->Body = "$mensaje";
            $mail->AltBody = "This is the plain text version of the email content";

            if(!$mail->send()) {
                echo "Mailer Error: " . $mail->ErrorInfo;
            } else {
                unset($correo);
                $correo = "";                
            } 
            $correo = NULL;
            unset($correo);            
        } 
    }   
    
    function notificacionProyecto($id) { 
        $cDatosOpl = cOPLIdP($id);
    } 

    
    