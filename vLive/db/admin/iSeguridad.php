<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
include '../ServerFunctions.php';
session_start();
$linea = $_POST["cmbLineaLA"];     
$tIncidencia = $_POST["rIncidencia"]; 
$fecha = $_POST["fRegistro"]; 
if ($_SESSION["aLinea"] == 'HSE'){
    $reporta = 1;
} else {
    $reporta = 2;
}

$depReporta = $_POST["rNivel"];

if ($tIncidencia == "S"){ //SEGURIDAD
    $incidencia = $_POST["iSeguridad"];
} else { //AMBIENTE
    $incidencia = $_POST["iAmbiente"];
}

$descripcion = $_POST["iDescripcion"];


if (isset($linea) && isset($tIncidencia) && isset($fecha) && isset($depReporta) && isset($incidencia)){
    iSeguridad_Ambiente($fecha, $linea, $tIncidencia, $incidencia, $descripcion, $reporta);
} else {
    $errors [] = "OPS! VERIFICAR LA INFORMACION";
}

if (isset($errors)){ 
    ?> 
        <div class="alert alert-danger" role="alert" > 
            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
            <strong><?php foreach ($errors as $error) { 
                echo $error; 
            } ?> </strong> 
        </div> 
    <?php } else { 
        ?>
        <div class="alert alert-success" role="alert" > 
            <button type="button" class="close" data-dismiss="alert" >&times;</button> 
            <strong> REGISTROS GUARDADOS CORRECTAMENTE </strong> 
        </div>

    <?php }
    
