<?php

/* 
 * To change this license header, choose License Headers in Project Properties. 
 * To change this template file, choose Tools | Templates 
 * and open the template in the editor. 
 */ 

    session_start();
    date_default_timezone_set("America/Mexico_City"); 
    include './ServerFunctions.php';
    
    //IDENTIFICA DATOS PROVENIENTES PARA PODER MANDAR LAS SESIONES 
    if (isset($_REQUEST['line']) && isset($_REQUEST['f'])){ 
        $_SESSION["lineaH"] = $_REQUEST['line']; 
        $_SESSION["linea"] = $_REQUEST['line'];
        
        $dR = DateTime::createFromFormat('d/m/Y', $_REQUEST['f']);
        $f = $dR->format('Y-m-d');
        $_SESSION["fechaH"] = $f;    
        $_SESSION['anio'] = date("Y", strtotime($f)); 
        $_SESSION['mes'] = date("m", strtotime($f)); 
        $_SESSION['d'] = date("d", strtotime($f)); 
        
        $ultimoDiaMes = date("t",mktime(0,0,0, date("m"),1, date("Y"))); 
        
        $_SESSION['fIni'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-01'; 
        $_SESSION['fFin'] = $_SESSION['anio'].'-'.$_SESSION['mes'].'-'.$ultimoDiaMes; 
        
        $_SESSION['FIni'] = $_SESSION['fIni'];
        $_SESSION['FFin'] = $_SESSION['fFin'];
        
         /* DATOS DEL USUARIO */ 
        if (!empty($_REQUEST['u'] && $_REQUEST['u'] != "" && strlen($_REQUEST['u']) > 4 )) { 
            $_SESSION['usuario'] = $_REQUEST['u']; 
            #CONSULTA DE INFORMACION DEL USUARIO 
            $cUsuario = cUsuario($_REQUEST['u']); 
            for ($i = 0; $i < count($cUsuario); $i++){ 
                $_SESSION['productoU'] = $cUsuario[$i][0]; //PRODUCTO 
                $_SESSION['lineaU'] = $cUsuario[$i][1]; //LINEA 
                $_SESSION['correo'] = $cUsuario[$i][7]; //CORREO 
                $_SESSION['nameUsuario'] = $cUsuario[$i][3].' '.$cUsuario[$i][4]; //NOMBRE 
                $lastName = explode(" ", $cUsuario[0][4]); 
                $_SESSION['nickName'] = substr($cUsuario[0][3],0,1).'. '.$lastName[0]; //NICKNAME 
                $_SESSION['privilegio'] = $_REQUEST['p']; //PRIVILEGIO 
            } 
        } 
        
        /* DATOS DE LA LINEA */        
        $_SESSION['linea'] = $_REQUEST['line']; 
        //MANDA NOMBRE DE LA LINEA SELECCIONADA 
        $nombreLinea = nombreLinea($_REQUEST['line']); 
        for ($i = 0; $i < count($nombreLinea); $i++) { 
            $_SESSION['nameLinea'] = $_REQUEST['line'].' - '.$nombreLinea[$i][0]; 
            $_SESSION['costCenterL'] = $nombreLinea[$i][1]; 
            $_SESSION['profitCenterL'] = $nombreLinea[$i][2]; 
            $_SESSION['cValorL'] = $nombreLinea[$i][3]; 
            $_SESSION['productoL'] = $nombreLinea[$i][4]; 
        } 
        
        $_SESSION["targetH"] = 80; 
    ?> 
        <script>
            window.location = "../master/analisis/hourly.php";
        </script>       
    <?php 
            
            //include '../master/analisis/hourly.php';
        //REDIRECCION A PAGINA DE HOURLY
        //header('Location: http://sglersm01:8080/SMC/master/analisis/hourly.php');
    } else {     
        if (isset($_POST["linea"])){ 
            $linea = $_POST["linea"]; 
        } else if (isset($_SESSION["linea"])) { 
            $linea = $_SESSION["linea"];
        } else { 
            $linea = 'L001'; 
        } 

        if (isset($_POST["fecha"])){ 
            $fecha = date("Y-m-d", strtotime($_POST["fecha"])); 
        } else { 
            $fecha = date('Y-m-d');
        } 
        
        if (isset($_POST["Meta"]) && !empty($_POST["Meta"]) ){
            $target = $_POST["Meta"];
        } else {
            $target = 80;
        }

        $_SESSION["lineaH"] = $linea;
        $_SESSION["fechaH"] = $fecha;
        $_SESSION["targetH"] = $target;         
    }

    
    
    
    