<?php
require_once 'ServerConfig.php'; 
/**
 * Description of ServerFunctions
 *
 * @author GJA5TL
 */

//CONFIGURAMOS PARA QUE LA HORA SEA LA INTERNACION MEXICANA Y NO LA DE ALEMANIA
date_default_timezone_set("America/Mexico_City"); 

//Devuelve un array multidimensional con el resultado de la consulta
function getArraySQL($sql) { 
    $connectionObj = new ServerConfig(); 
    $connectionStr = $connectionObj -> serverConnection(); 
    
    if (!$result = sqlsrv_query($connectionStr, $sql)) 
        die("Conexion: administracion (Line 48-49)"); 
    
    $rawdata = array(); 
    $i = 0; 
    while ($row = sqlsrv_fetch_array($result)) { 
        $rawdata[$i] = $row; 
        $i++; 
    } 
    //$connectionObj ->serverDisconnect();
    return $rawdata; 
} 

//OBTENEMOS LA FECHA POR SEPARADO
function listarPersonal_linea() { 
    $sql = "SELECT linea, nombre FROM Lineas ORDER BY 1"; 
    return getArraySQL($sql); 
} 



