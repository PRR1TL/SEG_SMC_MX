<!DOCTYPE html>
<html> 
<head> 
    <!-- LIBRERIAS PARA EL LAYOUT --> 
    <link rel = stylesheet href = "css/estiloHourly.css" type = "text/css" media = screen /> 
    <LINK REL=StyleSheet HREF="css/index3.css" TYPE="text/css" MEDIA = screen > 
    <link href="css/style.css" rel="stylesheet" > 
    <meta charset="utf-8" > 
    <meta http-equiv="X-UA-Compatible" content="IE=edge" > 
    <meta name="viewport" content="width=device-width, initial-scale=1" > 
    <title>Principal Indicadores</title> 
    <link href="./imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" > 
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous" >
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" ></script> 
    <script src="js/index.js" ></script> 
    
    <!-- LIBRERIAS PARA GRAFICAS -->     
    <script src="//www.amcharts.com/lib/3/amcharts.js" ></script> 
    <script src="//www.amcharts.com/lib/3/serial.js" ></script> 
    <script src="https://www.amcharts.com/lib/4/core.js" ></script> 
    <script src="https://www.amcharts.com/lib/4/charts.js"></script> 
    <script src="https://www.amcharts.com/lib/4/themes/animated.js" ></script> 
    
    <!-- LIBRERIAS PARA EL PICKER --> 
    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" /> 
    <link href="css/MonthPicker.min.css" rel="stylesheet" type="text/css" /> 
    <script src="https://code.jquery.com/jquery-1.12.1.min.js" ></script> 
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" ></script> 
    <script src="https://cdn.rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js" ></script> 
    <script src="js/MonthPicker.min.js" ></script> 
        
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <link href="css/dropDown.css" rel="stylesheet" type="text/css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script>     
    <script src="js/drag/jquery.min.js"></script> 
    <script src="js/drag/jquery-ui.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="js/drag/lodash.js"></script> 
    <script src="js/drag/gridstack.js"></script> 
    <script src="js/drag/gridstack.jQueryUI.js"></script> 
    <script src="js/MonthPicker.min.js" ></script> 
   
    <script type="text/javascript"> 
    $(function() { 
        $('.grid-stack').gridstack({ 
            width: 12, 
            alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
            resizable: { 
                handles: 'e, se, s, sw, w' 
            } 
        }); 
    }); 
    </script> 
    
    <?php 
        include './db/ServerFunctions.php'; 
        session_start(); 
        $_SESSION['nivelReporte'] = 3; 
        $_SESSION['tipoDato'] = 1; 
        $_SESSION['path'] = $_SERVER['PHP_SELF']; 
        
        $fecha = date('M, Y', strtotime($_SESSION['fIni'])); 
        
    ?>    
</head> 
<script> 
    $(document).ready(function() { 
        //MANDAMOS LA LISTA DE LINEAS EN EL COMBO DE AJAX 
        $.ajax({ 
            success: function(respuesta) { 
                $('#cmbLinea').load('db/lineas.php'); 
            }, 
            error: function() { 
                console.log("No se ha podido obtener la información"); 
            }             
        }); 
        
        $.ajax({ 
            success: function(respuesta) { 
                $('#pnl1').load('./contenedores/index/linea/1_Iseguridad.php'); 
                $('#pnl2').load('./contenedores/index/linea/2_IQQSheet.php'); 
                $('#pnl3').load('./contenedores/index/linea/3_IFInternas.php'); 
                $('#pnlX').load('./contenedores/index/linea/4_ILJunta.php'); 
                $('#pnl4').load('./contenedores/index/linea/5_IEntregas.php'); 
                $('#pnl5').load('./contenedores/index/linea/6_IOEE.php'); 
                $('#pnl6').load('./contenedores/index/linea/7_IJidoka.php'); 
                $('#pnl7').load('./contenedores/index/linea/8_IProductividad.php'); 
                $('#pnl8').load('./contenedores/index/linea/9_IScrap.php'); 
                $('#pnl9').load('./contenedores/index/linea/10_IInventario.php'); 
                $('#pnl10').load('./contenedores/index/linea/11_ILAsistencia.php'); 
                $('#pnl11').load('./contenedores/index/linea/12_I5Ss.php'); 
                $('#pnl12').load('./contenedores/index/linea/13_Hourly.php'); 
                $('#pnl13').load('./contenedores/index/linea/14_IOpl.php'); 
                $('#pnl15').load('./contenedores/index/linea/15_Pareto.php'); 
            }, error: function() { 
                console.log("No se ha podido obtener la información"); 
            } 
        }); 
    });
    
    $(function () { 
        $('#mPicker').MonthPicker({ 
            ShowIcon: false, 
            SelectedMonth: '<?php echo $fecha; ?>', 
            MonthFormat: 'M, yy' 
        }); 
        
        //FUNCIONES PARA LOS BOTONES DE LA CABECERA
        $('#fCalcular').submit(function( event ) { 
            var linea = document.getElementById('cmbLinea').value; 
            var mes = document.getElementById('mPicker').value; 
            
            if (linea.substr(0,1) == 'L' ){ 
                $.ajax({                 
                    type: "POST", 
                    url: "./db/sesionReportes.php", 
                    data: {nivReporte: 3, cmbLinea: linea, fecha: mes }, 
                    success: function(datos){ 
                        //LIMPIAMOS EL SELECT 
                        $('#pnl1').load('./contenedores/index/linea/1_Iseguridad.php'); 
                        $('#pnl2').load('./contenedores/index/linea/2_IQQSheet.php'); 
                        $('#pnl3').load('./contenedores/index/linea/3_IFInternas.php'); 
                        $('#pnlX').load('./contenedores/index/linea/4_ILJunta.php'); 
                        $('#pnl4').load('./contenedores/index/linea/5_IEntregas.php'); 
                        $('#pnl5').load('./contenedores/index/linea/6_IOEE.php'); 
                        $('#pnl6').load('./contenedores/index/linea/7_IJidoka.php'); 
                        $('#pnl7').load('./contenedores/index/linea/8_IProductividad.php'); 
                        $('#pnl8').load('./contenedores/index/linea/9_IScrap.php'); 
                        $('#pnl9').load('./contenedores/index/linea/10_IInventario.php'); 
                        $('#pnl10').load('./contenedores/index/linea/11_ILAsistencia.php'); 
                        $('#pnl11').load('./contenedores/index/linea/12_I5Ss.php'); 
                        $('#pnl12').load('./contenedores/index/linea/13_Hourly.php'); 
                        $('#pnl13').load('./contenedores/index/linea/14_IOpl.php'); 
                        $('#pnl15').load('./contenedores/index/linea/15_Pareto.php'); 
                    } 
                }).fail( function( jqXHR, textStatus, errorThrown ) { 
                    if (jqXHR.status === 0) { 
                        alert('Not connect: Verify Network.'); 
                    } else if (jqXHR.status == 404) { 
                        alert('Requested page not found [404]'); 
                    } else if (jqXHR.status == 500) { 
                        alert('Internal Server Error [500].'); 
                    } else if (textStatus === 'parsererror') { 
                        alert('Requested JSON parse failed.'); 
                    } else if (textStatus === 'timeout') { 
                        alert('Time out error.'); 
                    } else if (textStatus === 'abort') { 
                        alert('Ajax request aborted.'); 
                    } else { 
                        alert('Uncaught Error: ' + jqXHR.responseText); 
                    } 
                }); 
            } else { 
                $.ajax({ 
                    type: "POST", 
                    url: "db/sesionReportes.php", 
                    data: {nivReporte: 2, mPicker: mes, cmbProducto: linea },                 
                    success: function(datos){ 
                        window.location = './iCad.php'; 
                    } 
                }).fail( function( jqXHR, textStatus, errorThrown ) { 
                    if (jqXHR.status === 0) { 
                        alert('Not connect: Verify Network.'); 
                    } else if (jqXHR.status == 404) { 
                        alert('Requested page not found [404]'); 
                    } else if (jqXHR.status == 500) { 
                        alert('Internal Server Error [500].'); 
                    } else if (textStatus === 'parsererror') { 
                        alert('Requested JSON parse failed.'); 
                    } else if (textStatus === 'timeout') { 
                        alert('Time out error.'); 
                    } else if (textStatus === 'abort') { 
                        alert('Ajax request aborted.'); 
                    } else { 
                        alert('Uncaught Error: ' + jqXHR.responseText); 
                    } 
                }); 
            }    
            event.preventDefault(); 
        }); 
    }); 
</script> 

<?php

    include './master/index/modals/mIJunta.php'; 
    include './master/index/modals/mILAsistencia.php'; 
    include './master/index/modals/mI5Ss.php'; 

?>

<body class="respo" > 
    <a align=center id="headerFixedPrincipal" class="contenedor" > 
        <div class='fila0' > 
        </div> 
        <div class="fila1" > 
            <div class="pickersPrincipal" style="margin-top: -10px;" > 
                <form  id = "fCalcular" method = "POST" aling = "center" > 
                    <ul class="nav justify-content-center" style="margin-left: -15%;" > 
                        <li class="nav-item" > 
                            <label>Linea: </label> 
                            <select id="cmbLinea" name="cmbLinea" class="btn btn-secondary btn-sm" style="text-align: left" > 
                            </select> 
                        </li> 
                        <li class="nav-item" > 
                            &nbsp;&nbsp;&nbsp;
                            <label>Mes: </label>
                            <input id="mPicker" name="mPicker" class="btn btn-secondary btn-sm" style="width: 130px;" >
                        </li> 
                        &nbsp;&nbsp;&nbsp;
                        <li class="nav-item" > 
                            <button type="submit" id="calcular" name="calular" class="btn btn-default btn-sm" >Calcular Gr&aacute;ficas</button> 
                        </li> 
                        <br><br> 
                    </ul> 
                </form>                 
                <div class="btn-group btn-sm " style="margin-left: 82%; margin-top: -70px;" > 
                    <button type="button" class="btn btn-sm" data-toggle="dropdown" style="background-color: transparent; border: 0;" > 
                        <img src="imagenes/buttons/main.png" >
                    </button> 
                    <ul class="dropdown-menu btn-sm" > 
                        <li> 
                            <form action="" method="POST" > 
                                <button class="dropdown-item" > Over Views </button> 
                            </form> 
                        </li> 
                        <li> 
                            <form action="master/otras/targets.php" method="POST" > 
                                <button class="dropdown-item" > Targets </button>  
                            </form> 
                        </li> 
                    </ul> 
                </div> 
            </div> 
        </div> 
    </a> 
    
    <br><br>    
    <div aling = "center" id="graficasPrincipal" class="container" > 
        <br> 
        <div class="fix-header card-no-border fix-sidebar" > 
            <div class="grid-stack" data-gs-width="12" data-gs-animate="yes" > 
                <!-- 1. SEGURIDAD --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="0" data-gs-width="3" data-gs-height="3" onclick="window.location = './master/otras/seguridad.php'; " > 
                    <div class="grid-stack-item-content contenidoCentrado" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/lMenu/1.png" style="width: 155px; height: 35px; margin-left: -1.5%; " > 
                        <div id="pnl1" name="pnl1" > 
                        </div> 
                    </div> 
                </div> 
                <!-- 2. QQ SHEET --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                     data-gs-x="3" data-gs-y="0" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/otras/calidadInterna.php'; " > 
                    <div class="grid-stack-item-content contenidoCentrado" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" >
                        <img src="imagenes/lMenu/2.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <img src="imagenes/nomenclaturas/i_FInternas.png" style="width: 95px; height: 35px; margin-left: -0.5% " > 
                        <div id="pnl2" name="pnl2" > 
                        </div> 
                    </div> 
                </div> 
                <!-- X. LISTA DE ASISTENCIA --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="0" data-gs-width="3" data-gs-height="3" data-toggle="modal" data-target="#mIJunta" > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" > 
                        <img src="imagenes/lMenu/x.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <div id="pnlX" name="pnlX" style="margin-top: 20px; " >                         
                        </div> 
                    </div> 
                </div> 
                <!-- 4. ENTREGAS -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="3" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/jidokas/producidas.php'; " > 
                    <div class="grid-stack-item-content"  style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;"  >
                        <img src="imagenes/lMenu/4.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <img src="imagenes/nomenclaturas/Entregas.png" style="width: 165px; height: 25px; margin-left: -1.5% " > 
                        <div id="pnl4" name="pnl4" >                             
                        </div> 
                    </div> 
                </div> 
                <!-- 5. OEE -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="6" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/jidokas/oee.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/lMenu/5.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <img src="imagenes/nomenclaturas/lblOEE.png" style="width: 165px; height: 25px; margin-left: -1.5% " > 
                        <div id="pnl5" name="pnl5" style="overflow-x: hidden; overflow-y: hidden;" ></div> 
                    </div> 
                </div> 
                <!-- 6. JIDOKA -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="9" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/jidokas/tecnicas.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/lMenu/6.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <img src="imagenes/nomenclaturas/i_Jidoka.png" style="width: 165px; height: 25px; margin-left: -1.5% " > 
                        <div id="pnl6" name="pnl6" style="overflow-x: hidden; overflow-y: hidden;" > 
                        </div> 
                    </div> 
                </div> 
                <!-- 7. PRODUCTIVIDAD -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="12" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/jidokas/productividad.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/lMenu/7.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <img src="imagenes/nomenclaturas/i_Productividad.png" style="width: 155px; height: 18px; margin-left: -1.5% " > 
                        <div id="pnl7" name="pnl7" ></div> 
                    </div> 
                </div> 
                <!-- 8. SCRAP -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="3" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/analisis/scrapG.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden; "  > 
                        <img src="imagenes/lMenu/8.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <img src="imagenes/nomenclaturas/i_scrap.png" style="width: 155px; height: 25px; margin-left: -1.5% " > 
                        <div id="pnl8" name="pnl8" ></div> 
                    </div> 
                </div> 
                <!-- 9. INVENTARIO -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="6" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/otras/inventario.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/lMenu/9.png" style="width: 155px; height: 35px; margin-left: -1.5% " >
                        <img src="imagenes/nomenclaturas/i_inventario.png" style="width: 155px; height: 20px; margin-left: -1.5% " >
                        <div id="pnl9" name="pnl9" > </div>
                    </div> 
                </div> 
                <!-- 3. FALLAS INTERNAS -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="6" data-gs-y="0" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/otras/fInternas.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/lMenu/3.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <img src="imagenes/nomenclaturas/i_FBInternas.png" style="width: 155px; height: 20px; margin-left: -1.5% " > 
                        <div id="pnl3" name="pnl3" style="overflow-x: hidden; overflow-y: hidden;" ></div>
                    </div> 
                </div> 
                <!-- 10. PERSONAL -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="9" data-gs-width="3" data-gs-height="3" data-toggle="modal" data-target="#mILAsistencia" > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" > 
                        <img src="imagenes/lMenu/10.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <div id="pnl10" name="pnl10" style="margin-top: 20px; overflow-x: hidden; overflow-y: hidden;" ></div> 
                    </div> 
                </div> 
                <!-- 11. 5Ss -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="12" data-gs-width="3" data-gs-height="3" data-toggle="modal" data-target="#mLDaily" > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" style="overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/lMenu/11.png" style="width: 155px; height: 35px; margin-left: -1.5% " >
                        <img src="imagenes/nomenclaturas/i_Productividad.png" style="width: 155px; height: 18px; margin-left: -1.5% " >
                        <div id="pnl11" name="pnl11" style="margin-top: 20px; overflow-x: hidden; overflow-y: hidden;" ></div>
                    </div> 
                </div> 
                <!-- 12. HOURLY  -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="3" data-gs-y="3" data-gs-width="6" data-gs-height="5" target="_blank" onclick="window.location = 'master/analisis/hourly.php'; " >                     
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/lMenu/12.png" style="width: 195px; height: 35px; margin-left: -1.0% " > 
                        <div id="pnl12" name="pnl12" style="height: 90%; " ></div>
                    </div> 
                </div> 
                <!-- 13. OPL --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="3" data-gs-y="8" data-gs-width="6" data-gs-height="4" onclick="window.location = 'master/otras/opl.php'; "  >                     
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden; " > 
                        <img src="imagenes/lMenu/13.png" style="width: 195px; height: 35px; margin-left: -1.0% " > 
                        <div id="pnl13" name="pnl13" style="overflow-x: hidden; overflow-y: hidden;" ></div>
                    </div> 
                </div> 
                <!-- 14. CONFIRMACION --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="6" data-gs-y="12" data-gs-width="3" data-gs-height="3" > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" > 
                        <img src="imagenes/lMenu/14.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <div id="pnl14" name="pnl14" style="overflow-x: hidden; overflow-y: hidden;" ></div>
                    </div> 
                </div> 
                <!--5Ss-->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="3" data-gs-y="12" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/paretos/pT5.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/lMenu/15.png" style="width: 155px; height: 35px; margin-left: -1.5% " > 
                        <div id="pnl15" name="pnl15" style="overflow-x: hidden; overflow-y: hidden;" ></div>
                    </div> 
                </div> 
            </div> 
        </div> 
        <br> 
    </div> 
</body> 
</html> 
