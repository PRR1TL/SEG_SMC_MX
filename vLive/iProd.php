<!DOCTYPE html>
<html> 
<head> 
    <!-- LIBRERIAS PARA EL LAYOUT --> 
    <link rel = stylesheet href = "css/estiloHourly.css" type = "text/css" media = screen /> 
    <LINK REL=StyleSheet HREF="css/index3.css" TYPE="text/css" MEDIA = screen > 
    <link href="css/style.css" rel="stylesheet" > 
    <meta charset="utf-8" > 
    <meta http-equiv="X-UA-Compatible" content="IE=edge" > 
    <meta name="viewport" content="width=device-width, initial-scale=1" > 
    <title>Principal Indicadores</title> 
    <link href="./imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" > 
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous" >
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" ></script> 
    <script src="js/index.js" ></script> 
    
    <!-- LIBRERIAS PARA GRAFICAS -->     
    <script src="//www.amcharts.com/lib/3/amcharts.js" ></script> 
    <script src="//www.amcharts.com/lib/3/serial.js" ></script> 
    <script src="https://www.amcharts.com/lib/4/core.js" ></script> 
    <script src="https://www.amcharts.com/lib/4/charts.js"></script> 
    <script src="https://www.amcharts.com/lib/4/themes/animated.js" ></script> 
    
    <!-- LIBRERIAS PARA EL PICKER --> 
    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" /> 
    <link href="css/MonthPicker.min.css" rel="stylesheet" type="text/css" /> 
    <script src="https://code.jquery.com/jquery-1.12.1.min.js" ></script> 
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" ></script> 
    <script src="https://cdn.rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js" ></script> 
    <script src="js/MonthPicker.min.js" ></script> 
        
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <link href="css/dropDown.css" rel="stylesheet" type="text/css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script>     
    <script src="js/drag/jquery.min.js"></script> 
    <script src="js/drag/jquery-ui.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script src="js/drag/lodash.js"></script> 
    <script src="js/drag/gridstack.js"></script> 
    <script src="js/drag/gridstack.jQueryUI.js"></script> 
    <script src="js/MonthPicker.min.js" ></script> 
   
    <script type="text/javascript"> 
    $(function() { 
        $('.grid-stack').gridstack({ 
            width: 12, 
            alwaysShowResizeHandle: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
            resizable: { 
                handles: 'e, se, s, sw, w' 
            } 
        }); 
    }); 
    </script> 
    
    <?php 
        include './db/ServerFunctions.php'; 
        session_start(); 
        $_SESSION['nivelReporte'] = 1; 
        $_SESSION['tipoDato'] = 1; 
        $_SESSION['path'] = $_SERVER['PHP_SELF']; 
        
        $y = date("Y"); 
        $mes = date("M"); 
        $fecha = $mes.", ".$y; 
        
    ?>    
</head> 
<script> 
    $(document).ready(function() { 
        //MANDAMOS LA LISTA DE LINEAS EN EL COMBO DE AJAX 
        $.ajax({ 
            success: function(respuesta) { 
                $('#cmbLinea').load('db/lineas.php'); 
            }, 
            error: function() { 
                console.log("No se ha podido obtener la información"); 
            } 
        }); 
        
        $.ajax({ 
            url: 'db/sesionReportes.php', 
            success: function(respuesta) { 
                $('#pnl1').load('./contenedores/index/cValor/1_Seguridad.php'); //SEGURIDAD               
                $('#pnl2').load('./contenedores/index/cValor/2_QQSheet.php'); // QQ-Sheet
                $('#pnl3').load('./contenedores/index/cValor/3_FInternas.php'); // INTERNAL FAILURES
                $('#pnlX').load('./contenedores/index/cValor/4_LJuntas.php'); // ASSITANCE
                $('#pnl4').load('./contenedores/index/cValor/5_Entregas.php'); // DELIVERY
                $('#pnl5').load('./contenedores/index/cValor/6_OEE.php'); // OEE
                $('#pnl6').load('./contenedores/index/cValor/7_Productividad.php'); //PRODUCTIVITY
                $('#pnl7').load('./contenedores/index/cValor/8_Scrap.php'); // SCRAP     
                $('#pnl8').load('./contenedores/index/cValor/9_SubEnsamble.php'); // SUB-ASSEMBLY
                $('#pnl9').load('./contenedores/index/cValor/10_NivelacionBTS.php'); // BTS
                $('#pnl10').load('./contenedores/index/cValor/11_Inventario.php'); //INVENTORI
                $('#pnl11').load('./contenedores/index/cValor/14_Pareto.php'); //FOCUS
                $('#pnl12').load('./contenedores/index/13_OPL.php'); // OPL
            },
            error: function() { 
                console.log("No se ha podido obtener la información"); 
            } 
        }); 
    }); 
    
    $(function () { 
        $('#mPicker').MonthPicker({ 
            ShowIcon: false, 
            SelectedMonth: '<?php echo $fecha; ?>', 
            MonthFormat: 'M, yy' 
        }); 
        
        //FUNCIONES PARA LOS BOTONES DE LA CABECERA
        $('#fCalcular').submit(function( event ) {  
            var selecciona = document.getElementById('cmbLinea').value;
            var mPicker = document.getElementById('mPicker').value;
            
            if (selecciona == 'ST' || selecciona == 'GE'){
               var parametros = $(this).serialize(); 
                $.ajax({ 
                    type: "POST", 
                    url: "./db/sesionReportes.php", 
                    data: parametros, 
                    success: function(datos){ 
                        //LIMPIAMOS EL SELECT 
                        $('#pnl1').load('./contenedores/index/cValor/1_Seguridad.php'); //SEGURIDAD               
                        $('#pnl2').load('./contenedores/index/cValor/2_QQSheet.php'); // QQ-Sheet
                        $('#pnl3').load('./contenedores/index/cValor/3_FInternas.php'); // INTERNAL FAILURES
                        $('#pnlX').load('./contenedores/index/cValor/4_LJuntas.php'); // ASSITANCE
                        $('#pnl4').load('./contenedores/index/cValor/5_Entregas.php'); // DELIVERY
                        $('#pnl5').load('./contenedores/index/cValor/6_OEE.php'); // OEE
                        $('#pnl6').load('./contenedores/index/cValor/7_Productividad.php'); //PRODUCTIVITY
                        $('#pnl7').load('./contenedores/index/cValor/8_Scrap.php'); // SCRAP     
                        $('#pnl8').load('./contenedores/index/cValor/9_SubEnsamble.php'); // SUB-ASSEMBLY
                        $('#pnl9').load('./contenedores/index/cValor/10_NivelacionBTS.php'); // BTS
                        $('#pnl10').load('./contenedores/index/cValor/11_Inventario.php'); //INVENTORI
                        $('#pnl11').load('./contenedores/index/cValor/12_Focus.php'); //FOCUS
                        $('#pnl12').load('./contenedores/index/13_OPL.php'); // OPL
                    } 
                }).fail( function( jqXHR, textStatus, errorThrown ) { 
                    if (jqXHR.status === 0) { 
                        alert('Not connect: Verify Network.'); 
                    } else if (jqXHR.status == 404) { 
                        alert('Requested page not found [404]'); 
                    } else if (jqXHR.status == 500) { 
                        alert('Internal Server Error [500].'); 
                    } else if (textStatus === 'parsererror') { 
                        alert('Requested JSON parse failed.'); 
                    } else if (textStatus === 'timeout') { 
                        alert('Time out error.'); 
                    } else if (textStatus === 'abort') { 
                        alert('Ajax request aborted.'); 
                    } else { 
                        alert('Uncaught Error: ' + jqXHR.responseText); 
                    } 
                }); 
            } else {                 
                 $.ajax({ 
                    type: "POST", 
                    url: "db/sesionReportes.php", 
                    data: {tEntrada: 1, mPicker: mPicker, cmbProd: selecciona },                 
                    success: function(datos){ 
                        window.location = './iCad.php';
                    } 
                }).fail( function( jqXHR, textStatus, errorThrown ) { 
                    if (jqXHR.status === 0) { 
                        alert('Not connect: Verify Network.'); 
                    } else if (jqXHR.status == 404) { 
                        alert('Requested page not found [404]'); 
                    } else if (jqXHR.status == 500) { 
                        alert('Internal Server Error [500].'); 
                    } else if (textStatus === 'parsererror') { 
                        alert('Requested JSON parse failed.'); 
                    } else if (textStatus === 'timeout') { 
                        alert('Time out error.'); 
                    } else if (textStatus === 'abort') { 
                        alert('Ajax request aborted.'); 
                    } else { 
                        alert('Uncaught Error: ' + jqXHR.responseText); 
                    } 
                }); 
            } 
            event.preventDefault(); 
        }); 
    }); 
</script> 

<?php 
     include './master/index/modals/mPJunta.php'; 
?>

<body class="respo" > 
    <a align=center id="headerFixedPrincipal" class="contenedor" > 
        <div class='fila0' > 
        </div> 
        <h5 class="tituloPareto" > 
            <?php echo $_SESSION['nameProductoL']; ?>
        </h5> 
        <div class="fila1" > 
            <div class="pickersPrincipal" > 
                <form  id = "fCalcular" method = "POST" aling = "center" > 
                    <ul class="nav justify-content-center" style="margin-left: -15%;" > 
                        <li class="nav-item" > 
                            <select id="cmbLinea" name="cmbLinea" class="btn btn-secondary btn-sm" style="text-align: left" > 
                            </select> 
                        </li> 
                        <li class="nav-item" > 
                            &nbsp;&nbsp;&nbsp;
                            <label>Mes: </label>
                            <input id="mPicker" name="mPicker" class="btn btn-secondary btn-sm" style="width: 130px;" >
                        </li> 
                        &nbsp;&nbsp;&nbsp;
                        <li class="nav-item" > 
                            <button type="submit" id="calcular" name="calular" class="btn btn-default btn-sm" >Calcular Gr&aacute;ficas</button> 
                        </li> 
                        <br><br> 
                    </ul> 
                </form>                 
                <div class="btn-group btn-sm " style="margin-left: 82%; margin-top: -5.6%;" > 
                    <button type="button" class="btn btn-sm" data-toggle="dropdown" style="background-color: transparent; border: 0;" > 
                        <img src="imagenes/buttons/main.png" >
                    </button> 
                    <ul class="dropdown-menu btn-sm" > 
                        <li> 
                            <form action="" method="POST" > 
                                <button class="dropdown-item" > Over Views </button> 
                            </form> 
                        </li> 
                        <li> 
                            <form action="master/otras/targets.php" method="POST" > 
                                <button class="dropdown-item" > Targets </button>  
                            </form> 
                        </li> 
                    </ul> 
                </div> 
            </div> 
        </div> 
    </a> 
    
    <br><br> 
    <div aling = "center" id="graficasPrincipal" class="container" > 
        <br><br> 
        <div class="fix-header card-no-border fix-sidebar" > 
            <div class="grid-stack" data-gs-width="12" data-gs-animate="yes" > 
                <!-- 1. SEGURIDAD -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="0" data-gs-width="3" data-gs-height="3" onclick="window.location = './master/otras/seguridad.php'; " > 
                    <div class="grid-stack-item-content contenidoCentrado" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/cValor/1.png" style=" width: 50%; height: 15%; margin-left: -1.5%; " > 
                        <div id="pnl1" name="pnl1" > 
                        </div> 
                    </div> 
                </div> 
                <!-- 2. QQ SHEET --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                     data-gs-x="3" data-gs-y="0" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/otras/calidadInterna.php'; "  > 
                    <div class="grid-stack-item-content contenidoCentrado" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" >
                        <img src="imagenes/cValor/2.png" style=" width: 50%; height: 15%; margin-left: -1.5% " > 
                        <div id="pnl2" name="pnl2" > 
                        </div> 
                    </div> 
                </div> 
                <!-- X. LISTA DE ASISTENCIA --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="0" data-gs-width="3" data-gs-height="3" data-toggle="modal" data-target="#mIJunta" > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" > 
                        <img src="imagenes/cValor/x.png" style=" width: 50%; height: 15%; margin-left: -1.5% " > 
                        <div id="pnlX" name="pnlX" style="margin-top: 20px; " >                         
                        </div> 
                    </div> 
                </div> 
                <!-- 4. ENTREGAS -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="3" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/jidokas/producidas.php'; " > 
                    <div class="grid-stack-item-content"  style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;"  >
                        <img src="imagenes/cValor/4.png" style=" width: 50%; height: 15%; margin-left: -1.5% " > 
                        <div id="pnl4" name="pnl4" > 
                        </div> 
                    </div> 
                </div> 
                <!-- 5. OEE --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="6" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/jidokas/oee.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/cValor/5.png" style=" width: 50%; height: 15%; margin-left: -1.5%; " > 
                        <div id="pnl5" name="pnl5" style="overflow-x: hidden; overflow-y: hidden;" ></div> 
                    </div> 
                </div> 
                <!-- 6. PRODUCTIVIDAD --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="12" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/jidokas/productividad.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/cValor/7.png" style=" width: 50%; height: 15%; margin-left: -1.5% " >
                        <div id="pnl6" name="pnl6" ></div> 
                    </div> 
                </div> 
                <!-- 7. SCRAP -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="3" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/analisis/scrapG.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden; "  > 
                        <img src="imagenes/cValor/8.png" style=" width: 50%; height: 15%; margin-left: -1.5% " > 
                        <div id="pnl7" name="pnl7" ></div> 
                    </div> 
                </div> 
                <!-- 8. SUB-ENSAMBLE -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="0" data-gs-y="6" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/otras/inventario.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/cValor/16.png" style=" width: 50%; height: 15%; margin-left: -1.5% " > 
                        <div id="pnl8" name="pnl8" > </div>
                    </div> 
                </div> 
                <!-- 3. FALLAS INTERNAS -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="6" data-gs-y="0" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/otras/fInternas.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/cValor/3.png" style=" width: 50%; height: 15%; margin-left: -1.5% " > 
                        <div id="pnl3" name="pnl3" style="overflow-x: hidden; overflow-y: hidden;" ></div>
                    </div> 
                </div> 
                <!-- 9. BTS -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="12" data-gs-width="3" data-gs-height="3" onclick="window.location = 'master/otras/personal.php'; " > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" > 
                        <img src="imagenes/cValor/18.png" style=" width: 50%; height: 15%; margin-left: -1.5% " > 
                        <div id="pnl9" name="pnl9" style="margin-top: 20px; overflow-x: hidden; overflow-y: hidden;" ></div>
                    </div> 
                </div> 
                <!-- 10. INVENTARIO -->
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="9" data-gs-width="3" data-gs-height="3" > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" style="overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/cValor/9.png" style=" width: 50%; height: 15%; margin-left: -1.5% " > 
                        <div id="pnl10" name="pnl10" style="margin-top: 20px; overflow-x: hidden; overflow-y: hidden;" ></div> 
                    </div> 
                </div>                 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="3" data-gs-y="12" data-gs-width="3" data-gs-height="3" > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" style="overflow-x: hidden; overflow-y: hidden;" > 
                        <div id="pnl10" name="pnl10" style="margin-top: 20px; overflow-x: hidden; overflow-y: hidden;" ></div> 
                        <label >5Ss</label> 
                    </div> 
                </div> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="6" data-gs-y="12" data-gs-width="3" data-gs-height="3" > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" style="overflow-x: hidden; overflow-y: hidden;" > 
                        <div id="pnl10" name="pnl10" style="margin-top: 20px; overflow-x: hidden; overflow-y: hidden;" ></div> 
                        <label >Mejoras</label>
                    </div> 
                </div> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="9" data-gs-y="12" data-gs-width="3" data-gs-height="3" > 
                    <div class="grid-stack-item-content" style="border: 1px solid #000000;" style="overflow-x: hidden; overflow-y: hidden;" > 
                        <div id="pnl10" name="pnl10" style="margin-top: 20px; overflow-x: hidden; overflow-y: hidden;" ></div> 
                        <label >TPM</label>
                    </div> 
                </div> 
                
                <!-- 11. FOCUS  --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="3" data-gs-y="3" data-gs-width="6" data-gs-height="5" target="_blank" onclick="window.location = 'master/analisis/hourly.php'; " >                     
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden;" > 
                        <img src="imagenes/cValor/17.png" style=" width: 30%; height: 10%; margin-left: -1.0% " > 
                        <div id="pnl11" name="pnl11" style="height: 90%; " ></div> 
                    </div> 
                </div> 
                <!-- 12. OPL --> 
                <div class="grid-stack-item" data-gs-min-width="3" data-gs-min-height="3" data-gs-max-width="12" data-gs-max-height="9" 
                    data-gs-x="3" data-gs-y="8" data-gs-width="6" data-gs-height="4" onclick="window.location = 'master/otras/opl.php'; "  >                     
                    <div class="grid-stack-item-content" style="border: 1px solid #000000; overflow-x: hidden; overflow-y: hidden; " > 
                        <img src="imagenes/cValor/13.png" style=" width: 29%; height: 13%; margin-left: -1.0% " > 
                        <div id="pnl12" name="pnl12" style="overflow-x: hidden; overflow-y: hidden;" ></div> 
                    </div> 
                </div> 
            </div> 
        </div> 
        <br> 
    </div> 
</body> 
</html> 
