<!DOCTYPE html>
<html> 
<head> 
    <!-- LIBRERIAS PARA EL LAYOUT --> 
    <LINK REL=StyleSheet HREF="css/indexPrincipal.css" TYPE="text/css" MEDIA=screen > 
    <title>Principal Indicadores</title> 
    <link href="../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" /> 
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" > 
    <meta name="viewport" content="width=device-width, initial-scale=1.0" > 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" > 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous" >
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" > 
    
    <!-- LIBRERIAS PARA EL PICKER --> 
    <link href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" /> 
    <link href="css/MonthPicker.min.css" rel="stylesheet" type="text/css" /> 
    <script src="https://code.jquery.com/jquery-1.12.1.min.js" ></script> 
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js" ></script> 
    <script src="https://cdn.rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js" ></script> 
    <script src="js/MonthPicker.min.js" ></script> 
    
    <!-- LIBRERIAS PARA GRAFICAS --> 
    <script src="//www.amcharts.com/lib/3/amcharts.js" ></script> 
    <script src="//www.amcharts.com/lib/3/serial.js" ></script> 
    
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <link href="css/dropDown.css" rel="stylesheet" type="text/css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script> 
    
    <?php 
        session_start(); 
    ?> 
    
</head>
<script> 
    function redireccion( tipo, valor){  
        $.ajax({ 
            type: "POST", 
            url: "./db/sesionReportes.php", 
            data: {nivlReporte: tipo, valor: valor }, 
            success: function(datos){ 
                window.location = datos; 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
    } 
    
    $(document).ready(function() { 
        $.ajax({ 
            url: './db/sesionReportes.php', 
            error: function( jqXHR, textStatus, errorThrown ) { 
                if (jqXHR.status === 0) { 
                    alert('doc.Not connect: Verify Network.'); 
                } else if (jqXHR.status == 404) { 
                    alert('doc.Requested page not found [404]'); 
                } else if (jqXHR.status == 500) { 
                    alert('doc.Internal Server Error [500].'); 
                } else if (textStatus === 'parsererror') { 
                    alert('doc.Requested JSON parse failed.'); 
                } else if (textStatus === 'timeout') { 
                    alert('doc.Time out error.'); 
                } else if (textStatus === 'abort') { 
                    alert('doc.Ajax request aborted.'); 
                } else { 
                    alert('doc.Uncaught Error: ' + jqXHR.responseText); 
                } 
            } 
        });   
        
        //ENTREGAS DIARIAS
        $.ajax({ 
            url: './db/sesionReportes.php',
            success: function(respuesta) { 
                $('#MX').load('./contenedores/index/principal/0_planta.php'); 
                $('#MOE1').load('./contenedores/index/principal/1_moe1.php'); 
                $('#MOE2').load('./contenedores/index/principal/1_moe2.php'); 
                $('#ELPL').load('./contenedores/index/principal/2_cv_elpl.php'); 
                $('#CV').load('./contenedores/index/principal/2_cv_dt.php'); 
                $('#NBL').load('./contenedores/index/principal/2_cv_nbl.php'); 
                $('#ST').load('./contenedores/index/principal/2_cv_st.php'); 
            }, 
            error: function( jqXHR, textStatus, errorThrown ) { 
                if (jqXHR.status === 0) { 
                    alert('dE.Not connect: Verify Network.'); 
                } else if (jqXHR.status == 404) { 
                    alert('dE.Requested page not found [404]'); 
                } else if (jqXHR.status == 500) { 
                    alert('dE.Internal Server Error [500].'); 
                } else if (textStatus === 'parsererror') { 
                    alert('dE.Requested JSON parse failed.'); 
                } else if (textStatus === 'timeout') { 
                    alert('dE.Time out error.'); 
                } else if (textStatus === 'abort') { 
                    alert('dE.Ajax request aborted.'); 
                } else { 
                    alert('dE.Uncaught Error: ' + jqXHR.responseText); 
                } 
            } 
        }); 
    }); 
</script> 

<body> 
    <a align=center id="headerFixedPrincipal" class="contenedor" > 
        <div class='fila0' > 
            <img src="imagenes/blanca.jpg" > 
        </div> 
        <h5 class="tituloPareto" > 
            SMC Le 
        </h5> 
        <div class="fila1" > 
            <div class="pickersPrincipal" > 
            </div> 
        </div> 
    </a> 
    <br><br> 
    <div aling = "center" id="graficasPrincipal" class="contened" > 
        <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 25px; height: 10%; border: 0.1px solid #888; width: 96%; margin-left: 2%;" onclick="redireccion(0,'SEG-MX'); " > 
            <img src="imagenes/eMenu/1_1.png" style=" width:400px; height: 35px; margin-left: -1.2%;" >  
            <div class="panel-body graficaPanel" id="MX" name="MX" style="margin-top: -20px; height: 10%; " > 
            </div> 
        </div> 
        
        <div class="col-lg-12 col-md-12 col-sm-12" style="margin-top: 7px;" > 
            <!-- MOE 1 --> 
            <div class="col-lg-8 col-md-8 col-sm-8 " > 
                <div class="col-lg-12 col-md-12 col-sm-12 " style="height: 10%; border: 0.1px solid #888;" onclick="redireccion(1,'GE')" > 
                    <img src="imagenes/eMenu/2_1.png" style=" width: 150px; height: 35px; margin-left: -1.8% " >  
                    <div class="panel-body graficaPanel" id="MOE1" name="MOE1" style="height: 8%; margin-top: -20px;" > 
                    </div> 
                </div> 
                
                <!-- CADENA DE VALOR --> 
                <div class="col-lg-4 col-md-4 col-sm-4 " style="margin-top: 7px; border: 0.1px solid #888; " onclick="redireccion(2,'EL/PL')" > 
                    <img src="imagenes/eMenu/4_1.png" style=" width: 150px; height: 35px; margin-left: -5.5% " > 
                    <div class="panel-body graficaPanel" id="ELPL" name="ELPL" style="height: 8%; margin-top: -20px;" > 
                    </div> 
                </div> 
                <div class="col-lg-4 col-md-4 col-sm-4 " style="margin-top: 7px; border: 0.1px solid #888; " onclick="redireccion(2,'CV')" > 
                    <img src="imagenes/eMenu/5_1.png" style=" width: 150px; height: 35px; margin-left: -5.5% " > 
                    <div class="panel-body graficaPanel" id="CV" name="CV" style="height: 8%; margin-top: -20px;" > 
                    </div> 
                </div> 
                <div class="col-lg-4 col-md-4 col-sm-4 " style="margin-top: 7px; border: 0.1px solid #888; " onclick="redireccion(2,'NBL')" > 
                    <img src="imagenes/eMenu/6_1.png" style=" width: 150px; height: 35px; margin-left: -5.5% " > 
                    <div class="panel-body graficaPanel" id="NBL" name="NBL" style="height: 8%; margin-top: -20px;" > 
                    </div> 
                </div>                 
            </div> 
            <!-- MOE 2 --> 
            <div class="col-lg-4 col-md-4 col-sm-4 " >
                <div class="col-lg-12 col-md-12 col-sm-12 " style=" border: 0.1px solid #888;" onclick="redireccion(1,'ST')" > 
                    <img src="imagenes/eMenu/3_1.png" style=" width: 150px;  height: 35px; margin-left: -15px " > 
                    <div class="panel-body graficaPanel" id="MOE2" name="MOE2" style="height: 8%; margin-top: -20px;" > 
                    </div> 
                </div> 
                <div class="col-lg-12 col-md-12 col-sm-12 " style="margin-top: 7px; border: 0.1px solid #888;" onclick="redireccion(2,'ST')" > 
                    <img src="imagenes/eMenu/7_1.png" style=" width: 150px; height: 35px; margin-left: -15px " > 
                    <div class="panel-body graficaPanel" id="ST" name="ST" style="height: 8%; margin-top: -20px;" > 
                    </div> 
                </div> 
            </div> 
        </div>
    </div> 
</body> 
</html> 
