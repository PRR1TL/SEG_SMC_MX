<?php

    require("class.phpmailer.php");
    //include '../db/funciones.php';
    
    $mail = new PHPMailer();

    $mail->isSMTP();
    $mail->SMTPDebug = 2;
    $mail->Debugoutput = 'html';
    //CONFIGURACION DE HOST DE SEG
    $mail->Host = "smtp.sg.lan";
    $mail->Port = 25;
    $mail->SMTPAuth = false;
    $mail->SMTPSecure = false;
    
    $mail->setFrom("no-reply@seg-automotive.com", "Portal SMC");
    $mail->addAddress("areli.perezcalixto@seg-automotive.com", "Recepient Name");
    $mail->isHTML(true);
    
    $mail->Subject = "OPL SMC";
    $mail->Body = "Prueba de smtp ";
    $mail->AltBody = "This is the plain text version of the email content";
    
    if(!$mail->send()) {
        echo "Mailer Error: " . $mail->ErrorInfo;
    } else {
        echo "Message has been sent successfully";
    }
    
?>