<!--
    Created on  : 9/01/2019 14:30:21 PM
    Author      : Areli Pérez Calixto (PRR1TL)
    Description : Archivo maestro para jidokas de paros tecnicos

-->

<HTML> 
    <LINK rel="stylesheet" href="../../css/pnlGeneral.css"/> 
    <title>OEE</title> 
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" /> 
    
    <!--LIBRERIAS PARA DISEÑO-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
    
    <!--LIBRERIA PARA GRAFICAS-->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    
    <!--LIBRERIAS PARA TABLA--> 
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script>  
    <script>        
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 2, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 1, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 
        
        function getFixedColumnsData() {} 
        
    </script> 
    <link rel="stylesheet" href="../../css/demo.css" /> 
    
    <!--LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR-->    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <?php    
        include '../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;
        
    ?>
    
    <head>
        <a align=center id="headerFixed" class="contenedor"> 
            <div class='fila0'> 
            </div> 
            <h5 class="tituloPareto" style="color: #FFFFFF" > 
                <?php echo 'OEE<br>','Linea:&nbsp'.$_SESSION['nameLinea']; ?>
            </h5> 
            <div class="fila1"> 
                <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%"> 
                <form action="../../iLinea.php" method="POST">
                    <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                            onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 40px; height: 40px; margin-left: -5px; " >
                    </button> 
                </form>  
                <form action="../paretos/losses.php" method="POST">
                    <button class="btn btn-success btn-sm btnTop3" onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer'" onmouseout="this.style.background='#008C5B'" >
                        Losses
                    </button>
                </form>  
            </div>                
        </a> 
        
        <script type="text/javascript">
            window.onload = function () {
                var gridViewScroll = new GridViewScroll({
                    elementID : "gvMain",
                    freezeColumn : true,
                    freezeFooter : true,
                    freezeColumnCssClass : "GridViewScrollItemFreeze",
                    freezeFooterCssClass : "GridViewScrollFooterFreeze"
                }); 
                gridViewScroll.enhance(); 
            }; 

            jQuery().ready(            
                function() {
                    getResult();
                    //setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
                }
            );

            function getResult() {
                //CUANDO SE ACTIVA LA HOJA TENDREMOS QUE MANDAR SOLO EL TEMA, PARA PODER HACER LA CONSULTA ADECUADA
                //PARA ELLO SE UTILIZA AJAX                

                $.ajax({ 
                    url: "../../contenedores/jidokas/oee.php", 
                    type: "post", 
                    success: function (result) { 
                        //Actualizamos el apartado de graficas 
                        jQuery("#pnlGraficas").html(result); 
                    } 
                }); 
            } 
            
        </script>    
    </head>
   
    <body> 
        <div id ="pnlHoja" > 
            <br> 
            <div id="pnlGraficas" > 
            
            </div> 
        </div> 
    </body> 