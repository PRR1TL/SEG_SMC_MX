<!--
    Created on  : 9/01/2019 14:30:21 PM
    Author      : Areli Pérez Calixto (PRR1TL)
    Description : Archivo maestro para jidokas de paros tecnicos

-->

<HTML>
    <LINK rel="stylesheet" href="../../css/pnlGeneral.css"/>
    <title>Piezas Producidas</title>
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    
    <!--LIBRERIAS PARA DISEÑO-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"> 
    
    <!--LIBRERIA PARA GRAFICAS-->
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script> 
    <!--LIBRERIAS PARA TABLA--> 
    <script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script>  
   
    <link rel="stylesheet" href="../../css/tblDescriptionScroll.css" />   
   
    <!--LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR-->    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <?php    
        include '../../db/ServerFunctions.php'; 
        session_start(); 
        $date = new DateTime; 
        
    ?>
    
    <head>
        <a align=center id="headerFixed" class="contenedor" >   
            <div class='fila0'>                
            </div>             
            <h5 class="tituloPareto" style="color: #FFFFFF" >
                <?php echo 'Piezas Producidas<br>','Linea:&nbsp'.$_SESSION['nameLinea']; ?>
            </h5>            
            <div class="fila1" >    
                <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
                <form action="../../iLinea.php" method="POST">
                    <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                        onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 40px; height: 40px; margin-left: -5px; margin-top: 0px; " >
                    </button> 
                </form> 
            </div>                
        </a> 
        
        <script type="text/javascript">
            jQuery().ready( 
                function() { 
                    getResult();
                    //setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
                }
            );

            function getResult() {
                $.ajax({
                    url: "../../contenedores/jidokas/prodNP.php",
                    type: "post",
                    success: function (result) {
                        jQuery("#pnlGraficas").html(result);
                    }
                }); 
            }
            
        </script>    
    </head>
   
    <body>
        <div id ="pnlHoja" > 
            <div style="margin-top: 2.9%;" > 
                <ul class="nav nav-tabs" > 
                    <li class="nav-item" > 
                        <a class="nav-link" data-toggle="tab" href="producidas.php" >Entregas</a> 
                    </li> 
                    <li class="nav-item" > 
                        <a class="nav-link active" data-toggle="tab" href="prodNoParte.php" >No. Parte</a>
                    </li> 
                </ul> 
            </div> 
            
            <div id="pnlGraficas" > 
            </div> 
            <br><br>
        </div> 
    </body>