<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    date_default_timezone_set("America/Mexico_City");    
    $_SESSION['path'] = $_SERVER['PHP_SELF']; 
   
    if (isset($_SESSION['usuario']) && isset($_SESSION['privilegio'])){ 
        $producto = $_SESSION['productoU']; 
        $usuario = $_SESSION['usuario']; 
        $nombre = $_SESSION['nameUsuario']; 
        $nickName = $_SESSION['nickName'];     
        $privilegio = $_SESSION['privilegio']; 
        $correo = $_SESSION['correo']; 
    } else {
        $userName = ''; 
        $nombre = 'INICIAR SESION'; 
        $privilegio = 0; 
        $typeEv = 0; 
    }
        
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" >  
        
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <link href="../../css/dropDown.css" rel="stylesheet" type="text/css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script> 
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    
    <!-- ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS --> 
    <link rel="stylesheet" href="../../css/opl.css"> 
    <script src="../../js/opl.js"> </script> 
    
    <?php      
        //FUNCIONES   
        include '../../db/ServerFunctions.php';
     
        //LLAMAMOS EL ARCHIVO QUE HACE REFERENCIA A LOS MODAL INFORMATIVOS
        //MODAL DE OPL
        include '../admin/modals/mIOplP.php';
        include '../admin/modals/mUOplP.php';
        include '../admin/modals/mDOplP.php';
        
        //MODALS PARA INICIO DE SESION          
        include '../admin/modals/mLogin.php';
        include '../admin/modals/mLogOut.php'; 
        
    ?>    
    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align=center id="headerFixed" class="contenedor"> 
        <div class='fila0' > 
        </div>             
        <h5 class="tituloPortal" style="color: #FFFFFF" >
            OPL: Proyectos
        </h5>            
        <div class="fila1">    
            <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
            <form action="../../iLinea.php" method="POST">
                <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                        onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 40px; height: 40px; margin-left: -5px; " >
                </button> 
            </form> 
            <?php if ($nombre != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 

    <script>
        jQuery().ready( 
            function() {
                getResult();
                //setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
            }
        );

        function getResult() { 
            $.ajax({ 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    $('#tblProyecto').load('../../contenedores/otros/tblProyectos.php'); 
                } 
            }); 
        }        
        
        function newData() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var filtro = document.getElementById("cmbFiltro").value; 
            var linea = document.getElementById("linea").value; 
            var ini = document.getElementById("fIniOpl").value; 
            var fin = document.getElementById("fFinOpl").value; 
            
            $.ajax({ 
                type: "POST", 
                url: "../../db/admin/sesionAdmin.php", 
                data: { tipo: 1, linea: linea, fIni: ini, fFin: fin, filtro: filtro }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    $('#tblProyecto').load('../../contenedores/otros/tblProyectos.php'); 
                } 
            }); 
        } 
    </script>
</head>

<body> 
    <br>
    <div id="menuPrincipal" style="margin-top: 2.5%"> 
        <div class="btn-group btn-sm " style="margin-top: 0.7vh; margin-left: 90%" >
            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" style="background-color: transparent; border: 0; margin-top: -10px" >
                <img src="../../imagenes/m.png">
            </button>
            <ul class="dropdown-menu btn-sm" > 
                <li>
                    <form action="calidadInterna.php" >
                        <button class="dropdown-item" > Calidad </button> 
                    </form>   
                </li>
                <li>
                    <form target="_blank" action="../analisis/hourly.php" > 
                        <button class="dropdown-item" > Hourly </button> 
                    </form> 
                </li>
                <li>
                    <form action="inventario.php" >
                        <button class="dropdown-item" > Inventario </button> 
                    </form>   
                </li>
                <li>
                    <form action="LAsistencia.php" >
                        <button class="dropdown-item"> Lista de Asistencia </button>  
                    </form>   
                </li>
                <li> 
                    <form action="targets.php" > 
                        <button class="dropdown-item"> Targets </button>  
                    </form> 
                </li>
            </ul>
        </div> 
        
        <!-- Nav tabs --> 
        <ul class="nav nav-tabs" style="margin-top: -2.5%" >         
            <li class="nav-item"> 
                <a href="OPL.php">Mis Opl</a> 
            </li> 
            <li class="nav-item"> 
                <a class="nav-link active" data-toggle="tab" href="Proyectos.php">Proyectos</a> 
            </li> 
            <li class="nav-item"> 
                <a href="EstadisticasOpl.php">Estadisticas</a> 
            </li> 
        </ul> 
        <!-- Tab panes -->            
        <div class="tab-content" style="width: 98%; margin-left: 1%" > 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" style="margin-top: 1%" > 
                <div class="col-md-1" > 
                    <select class="form-control" id="linea" name="linea" style="height: 33px" > 
                        <?php for ($i = 0; $i < count($cLineas); $i++) { ?> 
                        <option value="<?php echo $cLineas[$i][0]; ?>" <?php if($cLineas[$i][0] == $_SESSION["linea"]){ ?> selected <?php } ?>  > <?php echo $cLineas[$i][0]; ?>  </option> 
                        <?php } ?> 
                    </select> 
                </div> 
                <div class="col-md-2 input-group" > 
                    <input type="text" class="form-control" id="fIniOpl" name="fIniOpl" value="<?php echo date('m/d/Y', strtotime($_SESSION['FIni'])); ?>" placeholder="F.Compromiso" >
                    <span class="input-group-addon"> a </span> 
                    <input type="text" class="form-control" id="fFinOpl" name="fFinOpl" value="<?php echo date('m/d/Y', strtotime($_SESSION['FFin'])); ?>" placeholder="F.Compromiso" >
                </div> 
                <div class="col-md-2" style="margin-left: 25%; margin-top: -4.0vh" > 
                    <select class="form-control" id="cmbFiltro" name="cmbFiltro" style="height: 33px" > 
                        <option value="0" > Todo </option> 
                        <option value="1" > Abiertos / Espera </option> 
                        <option value="2" > Cerrados en tiempo </option> 
                        <option value="3" > Cerrados fuera de tiempo </option> 
                        <option value="4" > Sin cerrar </option> 
                    </select> 
                </div> 
                <div class="col-md-3" > 
                    <button type="button" class="btn btn-success btn-sm" style="margin-left: 0.2%; margin-top: -9%" onclick="newData()" > 
                        <span class="glyphicon glyphicon-ok" ></span> 
                    </button> 
                </div> 
                <?php if ($privilegio >= 2 ){ ?> 
                    <button type="button" class="btn btn-warning " data-toggle="modal" data-target="#mIOPLP" style="margin-left: 90%; margin-top: -2% " >
                        Agregar 
                    </button> 
                <?php } ?>
            </div> 
            <br><br> 
            <div id="tblProyecto" name="tblProyecto" >
                
            </div> 
            <br><br>
        </div>
    </div>    
</body>
