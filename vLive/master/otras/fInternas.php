<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    session_start();
    date_default_timezone_set("America/Mexico_City"); 
    $_SESSION['path'] = $_SERVER['PHP_SELF']; 
    
    if (isset($_SESSION['usuario']) && isset($_SESSION['privilegio'])){ 
        $producto = $_SESSION['productoU']; 
        $usuario = $_SESSION['usuario']; 
        $nombre = $_SESSION['nameUsuario']; 
        $nickName = $_SESSION['nickName']; 
        $privilegio = $_SESSION['privilegio']; 
        $correo = $_SESSION['correo']; 
    } else { 
        $userName = ''; 
        $nombre = 'INICIAR SESION'; 
        $privilegio = 0; 
        $typeEv = 0;  
    } 
    
    
?>

<head>    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" >  
    
    <!-- ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS --> 
    <link rel="stylesheet" href="../../css/opl.css"> 
    <script src="../../js/opl.js"> </script> 
    
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <link href="../../css/dropDown.css" rel="stylesheet" type="text/css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script> 
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>     
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>  
    
    <link href="../../css/MonthPicker.min.css" rel="stylesheet" type="text/css" />
    <script src="../../js/MonthPicker.min.js"></script>  
    
    
    <?php      
        //FUNCIONES   
        include '../../db/ServerFunctions.php'; 
        
        //MODALS PARA INICIO DE SESION          
        include '../admin/modals/mLogin.php';
        include '../admin/modals/mLogOut.php';         
        include '../admin/modals/mIQI.php'; 
        
        $fechaP = date('M', strtotime($_SESSION['fIni'])).', '.$_SESSION['anio']; 
    ?> 
    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align=center id="headerFixed" class="contenedor"> 
        <div class='fila0' > 
        </div>             
        <h5 class="tituloPortal" style="color: #FFFFFF" >
            FALLAS INTERNAS
        </h5>            
        <div class="fila1">    
            <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
            <form action="../../iLinea.php" method="POST">
                <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                        onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 40px; height: 40px; margin-left: -5px; " >
                </button> 
            </form> 
            <?php if ($nombre != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 

    <script type="text/javascript">  
        jQuery().ready( 
            function() { 
                getResult(); 
//                setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN 
            } 
        ); 

        function getResult() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            $.ajax({                 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    $('#grafI').load('../../contenedores/otros/grafInternas.php'); 
                } 
            }); 
        }  
        
        function newData() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla 
            var linea = document.getElementById("cmbLinea").value; 
            var ini = document.getElementById("mPicker").value; 
            
            $.ajax({ 
                type: "POST", 
                url: "../../db/sesionReportes.php", 
                data: { tipoDato: 1, cmbLinea: linea, fecha: ini },
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    $('#grafI').load('../../contenedores/otros/grafInternas.php'); 
                } 
            }); 
        }  
        
        $(document).ready(function() {             
            // LISTADO DE LINEAS 
            $.ajax({ 
                success: function(respuesta) { 
                    $('#cmbLinea').load('../../db/lineasProducto.php'); 
                }, 
                error: function() { 
                    console.log("No se ha podido obtener la información"); 
                } 
            }); 
            
            //CONFIGURACION DE PICKER
            $('#mPicker').MonthPicker({ 
                ShowIcon: false, 
                MonthFormat: 'M, yy' 
            });
        });
        
    </script> 

    <style>
        #chartdiv { 
            width: 70%; 
            height: 75%; 
            font-size: 11px; 
        } 
    </style> 
</head> 

<body>
    <br> 
    <div id="menuPrincipal" style="margin-top: 2.5%" > 
        <div class="tab-content" style="width: 98%; margin-left: 1%"> 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" style="margin-top: 1%" >              
                <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2"> 
                    <select class="form-control" id="cmbLinea" name="cmbLinea" style="height: 33px; " > 
                    </select>                     
                </div> 
                <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2"> 
                    <input id="mPicker" name="mPicker" class="btn btn-secondary btn-sm" value="<?php echo $fechaP ?>" style="border-color: #888;" /> 
                </div> 
                <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3"> 
                    <button type="button" class="btn btn-success btn-sm" style="margin-left: -20%" onclick="newData()"> 
                        <span class="glyphicon glyphicon-ok"></span> 
                    </button> 
                </div> 
                
                <div class="btn-group btn-sm " style="margin-top: -3.9vh; margin-left: 90%" >
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" style="background-color: transparent; border: 0; margin-top: -10px" >
                        <img src="../../imagenes/m.png">
                    </button>
                    <ul class="dropdown-menu btn-sm" >                         
                        <li>
                            <form target="_blank" action="../analisis/hourly.php" > 
                                <button class="dropdown-item" > Hourly </button> 
                            </form> 
                        </li>
                        <li>
                            <form action="inventario.php" >
                                <button class="dropdown-item" > Inventario </button> 
                            </form>   
                        </li>
                        <li>
                            <form action="LAsistencia.php" >
                                <button class="dropdown-item"> Lista de Asistencia </button>  
                            </form>   
                        </li>
                        <li>
                            <form action="OPL.php" >
                                <button class="dropdown-item" > OPL </button> 
                            </form>   
                        </li>
                        <li> 
                            <form action="targets.php" > 
                                <button class="dropdown-item"> Targets </button>  
                            </form> 
                        </li> 
                    </ul> 
                </div> 
                
                <?php if ($privilegio >= 2 ){ ?> 
                    <button type="button" class="btn btn-warning " data-toggle="modal" data-target="#mIQI" style="margin-left: 80%; margin-top: -2.7% " >
                        Agregar 
                    </button> 
                <?php } ?> 
            </div> 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" id="grafI" >                
            </div> 
            <br> 
        </div> 
    </div> 
</body> 
