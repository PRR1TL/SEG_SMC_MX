<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    include '../admin/modals/mUTLinea.php';
    date_default_timezone_set("America/Mexico_City"); 
    $_SESSION['path'] = $_SERVER['PHP_SELF']; 
    
    if (isset($_SESSION['usuario']) && isset($_SESSION['privilegio'])){ 
        $producto = $_SESSION['productoU']; 
        $linea = $_SESSION['linea']; 
        $usuario = $_SESSION['usuario']; 
        $nombre = $_SESSION['nameUsuario']; 
        $nickName = $_SESSION['nickName']; 
        $privilegio = $_SESSION['privilegio']; 
        $correo = $_SESSION['correo'];        
    } else {         
        $userName = ''; 
        $nombre = 'INICIAR SESION'; 
        $privilegio = 0; 
        $typeEv = 0; 
    } 
        
?>

<head>    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" >  
    
    <!-- ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS --> 
    <link rel="stylesheet" href="../../css/opl.css"> 
    <script src="../../js/opl.js"> </script> 
    
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <link href="../../css/dropDown.css" rel="stylesheet" type="text/css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script> 
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    
    <!-- LIBRERIAS MONTH-PICKER -->
    <script src="https://www.amcharts.com/lib/4/core.js"></script>
    <script src="https://www.amcharts.com/lib/4/charts.js"></script>
    <script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>    
    
    <link href="../../css/MonthPicker.min.css" rel="stylesheet" type="text/css" />
    <script src="../../js/MonthPicker.min.js"></script>  
    
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script> 
    <script src="../../js/table-scroll.min.js"></script> 
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({ 
                fixedColumnsLeft: 1, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 0, //CABECERA FIJAS 
                columnsInScrollableArea: 20, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 

        function getFixedColumnsData() {} 
    </script> 
    
    <?php      
        //FUNCIONES   
        include '../../db/ServerFunctions.php'; 
        
        //MODALS PARA INICIO DE SESION 
        include '../admin/modals/mLogin.php'; 
        include '../admin/modals/mLogOut.php'; 
        
        include '../admin/modals/mITarget.php'; 
        include '../admin/modals/mITProduccion.php'; 
        
        //CONSULTA DE LINEAS         
            $fechaP = date("M, Y"); 
        
    ?> 
    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align = center id="headerFixed" class="contenedor"> 
        <div class='fila0'> 
        </div> 
        <h5 class="tituloPortal" style="color: #FFFFFF" >
            SMC: TARGETS
        </h5> 
        <div class="fila1">    
            <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
            <form action="../../iLinea.php" method="POST">
                <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                        onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 40px; height: 40px; margin-left: -5px; " >
                </button> 
            </form> 
            <?php if ($nombre != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 

    <script type="text/javascript">  
        jQuery().ready( 
            function() { 
                getResult(); 
//                setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN 
            } 
        ); 

        function getResult() {             
            $.ajax({                 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    $('#grafOpl').load('../../contenedores/otros/tblTargetsL.php'); 
                } 
            }); 
        }  
        
        function newData() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla 
            var linea = document.getElementById("cmbLinea").value; 
            var ini = document.getElementById("mPicker").value; 
            
            $.ajax({ 
                type: "POST", 
                url: "../../db/sesionReportes.php", 
                data: { tipo: 3, cmbLinea: linea, fecha: ini }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas 
                    $('#grafOpl').load('../../contenedores/otros/tblTargetsL.php'); 
                } 
            }); 
        } 
        
        $(document).ready(function() { 
            //CONFIGURACION DE PICKER 
            $('#mPicker').MonthPicker({ 
                ShowIcon: false, 
                MonthFormat: 'M, yy' 
            }); 
            
            $('#mPickerM').MonthPicker({ 
                ShowIcon: false, 
                MonthFormat: 'M, yy' 
            });
            
            $('#mPickerMP').MonthPicker({ 
                ShowIcon: false, 
                MonthFormat: 'M, yy',
                OnAfterChooseMonth: function() { 
                    var dato = $(this).val(); 
                    $.ajax({ 
                        type: "POST", 
                        url: "../../db/admin/sesionTargets.php", 
                        data: { dato: dato }, 
                        success: function (result) { 
                            //Actualizamos el apartado de graficas 
                            $('#panelBody').load('../../contenedores/modals/mITProduccion.php'); 
                        } 
                    }); 
                } 
            }); 
            
            $.ajax({ 
                //url: 'db/lineas.php', 
                success: function(respuesta) { 
                    $('#cmbLinea').load('../../db/lineasProducto.php'); 
                }, 
                error: function() { 
                    console.log("No se ha podido obtener la información"); 
                } 
            }); 
            
            
        }); 
        
    </script> 
</head> 

<body>
    <br> 
    <div id="menuPrincipal" style="margin-top: 2.5%" > 
        <div class="tab-content" style="width: 98%; margin-left: 1%" > 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" style="margin-top: 1%" > 
                <ul class="nav nav-tabs" style="margin-top: -0.7%" > 
                    <li class="nav-item"> 
                        <a href="targets.php" >Vista General</a> 
                    </li> 
                    <li class="nav-item">  
                        <a class="nav-link active" data-toggle="tab"  href="targetsL.php" >Losses</a> 
                    </li> 
                    <li class="nav-item"> 
                        <a href="targetsP.php" >Producción</a> 
                    </li> 
                    
                    <div class="btn-group btn-sm " style="margin-top: -3.9vh; margin-left: 90%" >
                        <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" style="background-color: transparent; border: 0; margin-top: -10px" >
                            <img src="../../imagenes/m.png" >
                        </button>
                        <ul class="dropdown-menu btn-sm" > 
                            <li>
                                <form action="calidadInterna.php" >
                                    <button class="dropdown-item" > Calidad </button> 
                                </form> 
                            </li> 
                            <li>
                                <form target="_blank" action="../analisis/hourly.php" > 
                                    <button class="dropdown-item" > Hourly </button> 
                                </form> 
                            </li>
                            <li>
                                <form action="inventario.php" >
                                    <button class="dropdown-item" > Inventario </button> 
                                </form>   
                            </li>
                            <li>
                                <form action="LAsistencia.php" >
                                    <button class="dropdown-item" > Lista de Asistencia </button>  
                                </form>   
                            </li>
                            <li>
                                <form action="OPL.php" > 
                                    <button class="dropdown-item" > OPL </button> 
                                </form> 
                            </li>  
                        </ul>
                    </div> 
                    
                </ul> 
                
                <div style="margin-top: 9px;" > 
                    <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2" > 
                        <select class="form-control" id="cmbLinea" name="cmbLinea" style="height: 33px;" >                             
                        </select> 
                    </div> 
                    <div class="col-xs-2 col-sh-2 col-md-2 col-lg-2" > 
                        <input id="mPicker" name="mPicker" class="btn btn-secondary btn-sm" value="<?php echo $fechaP ?>" style="border-color: #888;" > 
                    </div> 
                    <div class="col-xs-3 col-sh-3 col-md-3 col-lg-3" > 
                        <button type="button" class="btn btn-success btn-sm" style="margin-left: -20%" onclick="newData()" > 
                            <span class="glyphicon glyphicon-ok" ></span> 
                        </button> 
                    </div> 
                </div> 
                
                <?php if ($privilegio >= 2 ){ ?> 
                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#mITarget" style="margin-left: 90%; margin-top: -2.7% " >
                        Agregar 
                    </button> 
                <?php } ?> 
            </div> 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" id="grafOpl" > 
                
            </div>
            <br>
        </div>
    </div>    
</body>




