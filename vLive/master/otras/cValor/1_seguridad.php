<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

    session_start();
    date_default_timezone_set("America/Mexico_City");    
    $_SESSION['path'] = $_SERVER['PHP_SELF']; 
    
    $linea = $_SESSION['linea']; 
    
    if (isset($_SESSION['usuario']) && isset($_SESSION['privilegio'])){ 
        $producto = $_SESSION['producto']; 
        $usuario = $_SESSION['usuario']; 
        $nombre = $_SESSION['nombre']; 
        $nickName = $_SESSION['nickName']; 
        $puesto = $_SESSION['puesto']; 
        $privilegio = $_SESSION['privilegio']; 
        $correo = $_SESSION['correo'];        
    } else { 
        $userName = ''; 
        $nombre = 'INICIAR SESION'; 
        $privilegio = 0; 
        $typeEv = 0;  
    } 
    
    $dia = date("Y-m-d"); 
    
?>

<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous" >
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" ></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" > 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" >  
    
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <link href="../../../css/dropDown.css" rel="stylesheet" type="text/css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script> 
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" > 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script> 
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js" ></script>
    <script src="https://www.amcharts.com/lib/3/serial.js" ></script>

    <!--ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS-->
    <link rel="stylesheet" href="../../../css/opl.css" >     
    <script src="../../../js/opl.js" > </script> 
    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align = center id="headerFixed" class="contenedor" > 
        <div class='fila0 col-lg-6'>
            <img src="../../../imagenes/blanca.jpg" style="margin-left: -2.5%; height: 88%; margin-top: 10.8%;" > 
            <form action="../../../iLinea.php" method="POST">
                <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                    onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 100%; height: 100%; margin-left: -5px; " >
                </button> 
            </form> 
        </div>  
        <div class="fila1 col-lg-6" >
            <h3 class="tituloPortal" >
                SMC: LISTA DE ASISTENCIA
            </h3 > 
           <?php if ($nombre != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 

    <script type="text/javascript" > 
        jQuery().ready( 
            function() { 
                getResult(); 
                //setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
            } 
        ); 

        function getResult() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla 
            
            $.ajax({ 
                success: function (result) { 
                    $('#pnlGraficas').load('../../../contenedores/otros/cValor/1_gSeguridad.php'); 
                } 
            }); 
        } 
        
        function newData() { 
            //Obtenemos la informacion de los pickers para hacer el recalculo de la tabla
//            var linea = document.getElementById("cmbLinea").value; 
//            var ini = document.getElementById("fStandard").value; 
//            var fin = document.getElementById("fFin").value; 
//            var turno = document.getElementById("turnoLA").value; 
//            
//            $.ajax({ 
//                type: "POST",
//                url: "../../db/admin/sesionAdmin.php", 
//                data: { tipo:1, linea: linea, fIni: ini, fFin: fin, turno: turno },
//                success: function (result) { 
//                    //Actualizamos el apartado de graficas
//                    $('#grafInventario').load('../../contenedores/otros/tblAsistencia.php'); 
//                } 
//            }); 
        } 
    </script> 

    <style>
        #chartdiv {
            width: 70%;
            height: 75%;
            font-size: 11px;
        }	
    </style> 
</head>

<body>
    <br>
    <div id="menuPrincipal" style="margin-top: 2.5%" > 
        <div class="btn-group btn-sm " style="margin-top: -4.3vh; margin-left: 91%" > 
            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" style="background-color: transparent; border: 0; margin-top: 30px; " > 
                <img src="../../../imagenes/m.png"> 
            </button>
            <ul class="dropdown-menu btn-sm" > 
                <li>
                    <form action="calidadInterna.php" >
                        <button class="dropdown-item" > Calidad </button> 
                    </form> 
                </li> 
                <li>
                    <form target="_blank" action="../analisis/hourly.php" > 
                        <button class="dropdown-item" > Hourly </button> 
                    </form> 
                </li> 
                <li> 
                    <form action="inventario.php" > 
                        <button class="dropdown-item" > Inventario </button> 
                    </form> 
                </li> 
                <li> 
                    <form action="OPL.php" > 
                        <button class="dropdown-item"> OPL </button> 
                    </form> 
                </li> 
                <li> 
                    <form action="targets.php" > 
                        <button class="dropdown-item" > Targets </button> 
                    </form> 
                </li> 
            </ul> 
        </div> 
        
        <!-- Tab panes --> 
        <div style="width: 98%; margin-left: 1%" > 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado container container-fluid" style="margin-top: -2%; " >                 
                <div class="col-md-1 mb-1" >  
                    <select class="form-control" id="cmbLinea" name="cmbLinea" style="height: 33px" > 
                        <?php for ($i = 0; $i < count($cLineas); $i++) { ?> 
                        <option <?php if ($cLineas[$i][0] == $linea ){ ?> selected <?php } ?> value="<?php echo $cLineas[$i][0]; ?>" > <?php echo $cLineas[$i][0]; ?> </option> 
                        <?php } ?> 
                    </select> 
                </div> 
                <div class="col-md-2 mb-1 input-group" style="float: left;" > 
                    <input type="text" class="form-control" id="fStandard" name="fStandard" value="<?php echo $fIni; ?>" placeholder="F.Compromiso" >
                </div> 
                
                <div class="col-md-3 mb-1" style="float: left;" > 
                    <button type="submit" class="btn btn-success btn-sm" onclick="newData()" > 
                        <span class="glyphicon glyphicon-ok" ></span> 
                    </button> 
                </div> 
                <?php if ($privilegio >= 2 ){ ?> 
                <button type="button" class="btn btn-warning " data-toggle="modal" data-target="#mILAsistencia" style="margin-left: 80%; margin-top: -2.7% " >
                    Agregar 
                </button> 
                <?php } ?> 
            </div> 
            <br><br> 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" id="pnlGraficas" > 
            </div> 
        </div> 
    </div> 
</body> 
