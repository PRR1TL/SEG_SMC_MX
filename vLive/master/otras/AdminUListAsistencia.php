<!--
    Created on  : 9/01/2019 14:30:21 PM
    Author      : Areli Pérez Calixto (PRR1TL)
    Description : Archivo maestro para jidokas de paros tecnicos
-->

<HTML> 
    <LINK rel="stylesheet" href="../../css/pnlInterno.css"/> 
    <title>USUARIOS LISTA DE ASISTENCIA</title> 
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" /> 
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous" > 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous" > 
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" ></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" > 
    <script src="../../js/opl.js" > </script> 
    
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" > 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script> 

    <?php    
        include '../../db/ServerFunctions.php'; 
        session_start(); 
        include '../admin/modals/mIUCapacitacion.php'; 
        $date = new DateTime; 

        if (isset($_SESSION['linea'])) { 
            $line = $_SESSION['linea']; 
        } else { 
            $line = 'L001'; 
        } 
        
        $cLineas = listarLineas();   
        
    ?>
    
    <head>
        <a align=center id="headerFixed" class="contenedor"> 
            <div class='fila0' > 
                <img src="../../imagenes/blanca.jpg" class="imagen-SEG" >                 
            </div>             
            <h3 class="tituloPareto" > USUARIOS DE LISTA DE ASISTENCIA </h3> 
            <div class="fila1" > 
            </div> 
        </a> 
        
        <button class="btn-Home" onclick="javascript:location.href='../../iLinea.php'" style="background-color: transparent; border: 0;" > 
            <img src="../../imagenes/buttons/icon.png" class="imagen-Home" >
        </button> 
    
        <script type="text/javascript">
            jQuery().ready( 
                function() { 
                    getResult();
                    //setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN 
                } 
            ); 

            function getResult() { 
                $.ajax({ 
                    //url: "../../db/sesionReportes.php", 
                    type: "post", 
                    success: function (result) { 
                        //Actualizamos el apartado de graficas 
                        $('#pnlGraficas').load('../../contenedores/otros/tblAdminULAsistencia.php'); 
                    } 
                }); 
            } 
        </script>    
    </head>
   
    <body>
        <div id ="pnlHoja" > 
            <ul class="nav nav-tabs" style="margin-top: 1.8%; font-size: 16px; " > 
                <li class="nav-item" > 
                    <a class="nav-link active" data-toggle="tab" href="OPL.php" >Usuarios</a> 
                </li> 
                <li class="nav-item" > 
                    <a href="Proyectos.php" >Matriz</a> 
                </li> 
                <li class="nav-item" > 
                    <a href="EstadisticasOpl.php" >Historico</a> 
                </li> 
            </ul> 
            
            <div class="col-lg-12" style="margin-top: 10px;" > 
                <div class="col-lg-8" > 
                    <div class="col-lg-2 col-md-5" > 
                        <select class="form-control" id="cmbLinea" name="cmbLinea" style="height: 33px" > 
                            <?php for ($i = 0; $i < count($cLineas); $i++) { ?> 
                            <option value="<?php echo $cLineas[$i][0]; ?>" <?php if($cLineas[$i][0] == $_SESSION["linea"]){ ?> selected <?php } ?>  > <?php echo $cLineas[$i][0]; ?> </option> 
                            <?php } ?> 
                        </select> 
                    </div> 
                </div> 
                <div class="col-lg-3" > 
                    <button type="button" class="btn btn-warning " data-toggle="modal" data-target="#mINewPersonal" style="margin-left: 90%; margin-top: 0% " >
                        Agregar 
                    </button> 
                </div> 
            </div> 
            
            <div id="pnlGraficas" > 
                
            </div> 
            <br> 
        </div> 
    </body> 
    