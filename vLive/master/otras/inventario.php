<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    session_start();
    date_default_timezone_set("America/Mexico_City");    
    $_SESSION['path'] = $_SERVER['PHP_SELF'];
    
    if (isset($_SESSION['usuario']) && isset($_SESSION['privilegio'])){ 
        $linea = $_SESSION['linea']; 
        $usuario = $_SESSION['usuario']; 
        $nombre = $_SESSION['nameUsuario']; 
        $nickName = $_SESSION['nickName']; 
        $privilegio = $_SESSION['privilegio']; 
        $correo = $_SESSION['correo']; 
    } else { 
        $userName = ''; 
        $nombre = 'INICIAR SESION'; 
        $privilegio = 0; 
        $typeEv = 0; 
    }
    
    $linea = $_SESSION['linea']; 
    
    $dia = date("Y-m-d");
        
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" >  
    
    <!-- ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS --> 
    <link rel="stylesheet" href="../../css/opl.css"> 
    <script src="../../js/opl.js"> </script> 
    
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <link href="../../css/dropDown.css" rel="stylesheet" type="text/css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script> 
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
    <!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>--> 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>

    <!-- LIBRERIAS Y CONFIGURACION PARA TABLA -->
    <!--<script src="http://code.jquery.com/jquery-1.12.2.min.js"></script>-->
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js"></script>
    <script src="../../js/table-scroll.min.js"></script> 
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 1, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 0, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 

        function getFixedColumnsData() {} 
    </script>

    <?php      
        //FUNCIONES   
        include '../../db/ServerFunctions.php';     
        
        //MODALS PARA INICIO DE SESION          
        include '../admin/modals/mLogin.php';
        include '../admin/modals/mLogOut.php';
        
        include '../admin/modals/mIInventory.php';
        
        //FECHA         
        $month = $_SESSION['mes']; 
        $year = $_SESSION['anio'];         
        
        if (isset($_SESSION['mes'])){ 
            $fechaP = date('M, Y', strtotime($_SESSION['anio'].'-'.$_SESSION['mes'])); 
        } else {
            $fechaP = date('M, Y'); 
        } 
        
    ?>    
    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align=center id="headerFixed" class="contenedor"> 
        <div class='fila0' > 
        </div>             
        <h5 class="tituloPortal" style="color: #FFFFFF" >
            SEGURIDAD Y MEDIO AMBIENTE
        </h5>            
        <div class="fila1">    
            <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
            <form action="../../iLinea.php" method="POST">
                <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                        onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 40px; height: 40px; margin-left: -5px; " >
                </button> 
            </form> 
            <?php if ($nombre != 'INICIAR SESION') { ?>
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } else { ?>
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 

    <script type="text/javascript">
        jQuery().ready(             
            function() {
                getResult();
                //setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
            }
        );

        function getResult() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            $.ajax({                
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    $('#grafInventario').load('../../contenedores/otros/grafInventario.php'); 
                } 
            }); 
        } 
        
        function newData() { 
            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
            var linea = document.getElementById("cmbLinea").value; 
            var fecha = document.getElementById("fIni").value; 
            $.ajax({ 
                type: "POST",
                url: "../../db/sesionReportes.php", 
                data: { tipoDato: 1, cmbLinea: linea, fechaP: fecha },
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    $('#grafInventario').load('../../contenedores/otros/grafInventario.php'); 
                } 
            }); 
        } 
        
        $(document).ready(function() {             
            // LISTADO DE LINEAS 
            $.ajax({ 
                success: function(respuesta) { 
                    $('#cmbLinea').load('../../db/lineasProducto.php'); 
                }, 
                error: function() { 
                    console.log("No se ha podido obtener la información"); 
                } 
            }); 
        }); 
        
    </script> 

    <style>
        #chartdiv {
            width: 70%;
            height: 75%;
            font-size: 11px;
        }	
    </style>

</head>

<body>
    <br>
    <div id="menuPrincipal" style="margin-top: 2.5%" > 
        <!-- Nav tabs --> 
        
        <!-- Tab panes -->            
        <div class="tab-content" style="width: 98%; margin-left: 1%"> 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" style="margin-top: 1%" >                 
                <div class="col-md-2" >                     
                    <select class="form-control" id="cmbLinea" name="cmbLinea" style="height: 33px" > 
                    </select> 
                </div> 
                <div class="col-md-3 input-group"> 
                    <input type="text" class="form-control" id="fIni" name="fIni" value="<?php echo $fechaP; ?>" placeholder="F.Compromiso" >
                </div>
                <div class="col-md-3"> 
                    <button type="submit" class="btn btn-success btn-sm" style="margin-left: 185%; margin-top: -9%" onclick="newData()"> 
                        <span class="glyphicon glyphicon-ok"></span> 
                    </button> 
                </div> 
                <div class="btn-group btn-sm " style="margin-top: -3.9vh; margin-left: 90%" >
                    <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" style="background-color: transparent; border: 0; margin-top: -10px" >
                        <img src="../../imagenes/m.png">
                    </button>
                    <ul class="dropdown-menu btn-sm" > 
                        <li>
                            <form action="calidadInterna.php" >
                                <button class="dropdown-item" > Calidad </button> 
                            </form>   
                        </li>
                        <li>
                            <form target="_blank" action="../analisis/hourly.php" > 
                                <button class="dropdown-item" > Hourly </button> 
                            </form> 
                        </li>                        
                        <li>
                            <form action="LAsistencia.php" >
                                <button class="dropdown-item"> Lista de Asistencia </button>  
                            </form>   
                        </li>
                        <li>
                            <form action="OPL.php" >
                                <button class="dropdown-item" > OPL </button> 
                            </form>   
                        </li>
                        <li> 
                            <form action="targets.php" > 
                                <button class="dropdown-item"> Targets </button>  
                            </form> 
                        </li> 
                    </ul> 
                </div> 
                
                <?php if ($privilegio >= 2 ){ ?> 
                <button type="button" class="btn btn-warning " data-toggle="modal" data-target="#mIInventory" style="margin-left: 80%; margin-top: -2.7% " >
                    Agregar 
                </button> 
                <?php } ?>

            </div> 
            <br><br>
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" id="grafInventario" > 
               
            </div>
            <br>
        </div>
    </div>    
</body>
