<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    session_start();
    date_default_timezone_set("America/Mexico_City");    
    $_SESSION['path'] = $_SERVER['PHP_SELF']; 
    
    if (isset($_SESSION['usuario']) && isset($_SESSION['privilegio'])){ 
        $producto = $_SESSION['productoL']; 
        $usuario = $_SESSION['usuario']; 
        $nombre = $_SESSION['nameUsuario']; 
        $nickName = $_SESSION['nickName']; 
        $puesto = $_SESSION['puestoU']; 
        $privilegio = $_SESSION['privilegio']; 
        $correo = $_SESSION['correo']; 
    } else { 
        $userName = ''; 
        $nombre = 'INICIAR SESION'; 
        $privilegio = 0; 
        $typeEv = 0; 
    } 
    
?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous" >
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous" >
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js" ></script> 
    
    <!-- LINK PARA ICONOS --> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" > 
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" >  
    
    <!-- LIBRERIAS PARA EL MENU DESPEGABLE --> 
    <link href="../../css/dropDown.css" rel="stylesheet" type="text/css" /> 
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js" ></script> 
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" > 
    <!--<script src="https://code.jquery.com/jquery-1.12.4.js" ></script>--> 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script> 
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js" ></script>
    <script src="https://www.amcharts.com/lib/3/serial.js" ></script>

    <!-- LIBRERIAS Y CONFIGURACION PARA TABLA -->
    <!--<script src="http://code.jquery.com/jquery-1.12.2.min.js" ></script>-->
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" ></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/mustache.js/0.8.1/mustache.min.js" ></script>
    <script src="../../js/table-scroll.min.js" ></script> 
    <script> 
        //CONFIGURACION DE LA TABLA 
        $(function () { 
            var semple1Html = Mustache.to_html($('#tamplate-semple-1').html(), getFixedColumnsData()); 
            $(semple1Html).appendTo($('#holder-semple-1')).table_scroll({
                fixedColumnsLeft: 1, //CONTADOR Y FILA FIJOS 
                fixedColumnsRight: 0, //CABECERA FIJAS 
                columnsInScrollableArea: 12, //CANTIDAD DE DIAS A VER 
                scrollX: 0, //INICIO DEL SCROLL INFERIOR _ 
                scrollY: 0 //INICIO DE SCROLL LATERAL | 
            }); 
        }); 

        function getFixedColumnsData() {} 
    </script>

    <!--ESTILO PARA LA PAGINA YA EDITANDO NUESTRO CSS-->
    <link rel="stylesheet" href="../../css/opl.css" >     
    <script src="../../js/opl.js" > </script> 
    
    <?php      
        //FUNCIONES   
        include '../../db/ServerFunctions.php';     
        
        //MODALS PARA INICIO DE SESION 
        include '../admin/modals/mLogin.php'; 
        include '../admin/modals/mLogOut.php'; 
        //include '../admin/modals/mIJunta.php'; 
        
        //FECHA         
        $pNotClose = 1; 
        $pExtraTime = 3; 
        $pAtTime = 5; 
        $pWait = 0; 
        
        //CONSULTA DE LINEAS
        $cLineas = listarLineas(); 
        
    ?>    
    
    <!--ELEMENTOS PARA LA CABECERA-->
    <a align=center id="headerFixed" class="contenedor" > 
        <div class='fila0' > 
        </div> 
        <h5 class="tituloPortal" style="color: #FFFFFF" > 
            LISTA DE JUNTAS
        </h5> 
        <div class="fila1" > 
            <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%; height: 100%; " > 
            <form action="../../iLinea.php" method="POST" > 
                <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start; "
                        onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 40px; height: 40px; margin-left: -5px; " >
                </button> 
            </form> 
            <?php if ($nombre != 'INICIAR SESION') { ?> 
                <button type="button" id="btn-logOut" class="btn glyphicon glyphicon-off btn-logOut" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } else { ?> 
                <button type="button" id="login" class="btn glyphicon glyphicon-user btn-login" style="color: #ffffff;" ><?php echo " $nombre" ; ?></button>
            <?php } ?> 
        </div> 
    </a> 

    <script type="text/javascript" > 
        jQuery().ready( 
            function() { 
                getResult(); 
            } 
        ); 
        
        function getResult() { 
            $.ajax({ 
                success: function (result) { 
                    //Actualizamos el apartado de graficas 
                    $('#grafInventario').load('../../contenedores/otros/tblJunta.php'); 
                } 
            }); 
        } 
        
        function newData() { 
            //Obtenemos la informacion de los pickers para hacer el recalculo de la tabla 
            var linea = document.getElementById("cmbLinea").value; 
            var ini = document.getElementById("fStandard").value; 
            var fin = document.getElementById("fFin").value; 
            
            $.ajax({ 
                type: "POST", 
                url: "../../db/sesionReportes.php", 
                data: { tipoVista:1, cmbLinea: linea, fIni: ini, fFin: fin }, 
                success: function (result) { 
                    //Actualizamos el apartado de graficas
                    $('#grafInventario').load('../../contenedores/otros/tblJunta.php'); 
                } 
            }); 
        } 
        
        $(document).ready(function() {  
            // LISTADO DE LINEAS 
            $.ajax({ 
                //url: 'db/lineas.php', 
                success: function(respuesta) { 
                    $('#cmbLinea').load('../../db/lineasProducto.php'); 
                }, 
                error: function() { 
                    console.log("No se ha podido obtener la información"); 
                } 
            }); 
        }); 
        
    </script> 
</head>

<body> 
    <br> 
    <div id="menuPrincipal" style="margin-top: 35px;" > 
        <!-- Nav tabs --> 
        <ul class="nav nav-tabs" > 
            <li class="nav-item" > 
                <a class="nav-link active" data-toggle="tab" >Visualizar</a> 
            </li> 
            <li class="nav-item" > 
                <a href="MLJunta.php" > Modificar </a> 
            </li> 
        </ul> 
        
        <div class="btn-group btn-sm " style="margin-top: -4.3vh; margin-left: 91%" >
            <button type="button" class="btn btn-sm dropdown-toggle" data-toggle="dropdown" style="background-color: transparent; border: 0; margin-top: -10px" >
                <img src="../../imagenes/m.png">
            </button>
            <ul class="dropdown-menu btn-sm" > 
                <li>
                    <form action="calidadInterna.php" >
                        <button class="dropdown-item" > Calidad </button> 
                    </form> 
                </li> 
                <li>
                    <form target="_blank" action="../analisis/hourly.php" > 
                        <button class="dropdown-item" > Hourly </button> 
                    </form> 
                </li> 
                <li> 
                    <form action="inventario.php" > 
                        <button class="dropdown-item" > Inventario </button> 
                    </form> 
                </li> 
                <li> 
                    <form action="OPL.php" > 
                        <button class="dropdown-item"> OPL </button> 
                    </form> 
                </li> 
                <li> 
                    <form action="targets.php" > 
                        <button class="dropdown-item" > Targets </button> 
                    </form> 
                </li> 
            </ul> 
        </div> 
        
        <!-- Tab panes --> 
        <div style="width: 98%; margin-left: 1%" > 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado container container-fluid" style="margin-top: 9px; " > 
                <div class="col-md-2 mb-1" > 
                    <select class="form-control" id="cmbLinea" name="cmbLinea" style="height: 33px;" > 
                    </select> 
                </div> 
                <div class="col-md-2 mb-1 input-group" style="float: left;" > 
                    <input type="text" class="form-control" id="fStandard" name="fStandard" value="<?php echo date('m/d/Y', strtotime($_SESSION['FIni'])); ?>" placeholder="F.Compromiso" >
                    <span class="input-group-addon" > a </span> 
                    <input type="text" class="form-control" id="fFin" name="fFin" value="<?php echo date('m/d/Y', strtotime($_SESSION['FFin'])); ?>" placeholder="F.Compromiso" >
                </div> 
                <div class="col-md-3 mb-1" style="float: left;" > 
                    <button type="submit" class="btn btn-success btn-sm" onclick="newData()" > 
                        <span class="glyphicon glyphicon-ok" ></span> 
                    </button> 
                </div> 
                <?php if ($privilegio >= 2 ){ ?> 
                <button type="button" class="btn btn-warning " data-toggle="modal" data-target="#mIJunta" style="margin-left: 80%; margin-top: -2.7%;" > 
                    Agregar 
                </button> 
                <?php } ?> 
            </div> 
            <br><br><br> 
            <div class="col-xs-12 col-sh-12 col-md-12 col-lg-12 contenidoCentrado" id="grafInventario" > 
            </div> 
            <br> 
        </div> 
    </div> 
</body> 
