<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
        <meta name="viewport" content="width=device-width, initial-scale=1" > 
        <meta http-equiv=”Content-Type” content=”text/html; charset=ISO-8859-1″ /> 
        <!--LIBRERIAS PARA DISEÑO--> 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" > 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
        
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
        <script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script> 
        
        <link rel="stylesheet" href="../../css/losses.css"/> 
        
        <!--LIBRERIAS PARA PICKERS, NO MOVER NI ELIMINAR--> 
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
        
        <style> 
            #gTec {
                width : 98%; 
                height : 300px; 
            } 
            
            #gOrg { 
                width : 98%; 
                height : 300px; 
            } 
            
            #gCali { 
                width : 98%; 
                height : 300px; 
            } 

            #chartdiv {
                width : 98%;
                height : 300px;
            } 
        </style> 
    <?php 
        include '../../db/ServerFunctions.php';
        session_start();

        $anio = $_SESSION['anio']; 
        $mes = $_SESSION['mes']; 
                
        $fIni = date("Y-m-d", strtotime($_SESSION["FIni"])); 
        $fFin = date("Y-m-d", strtotime($_SESSION["FFin"]));         
        
        //TIPO DE CALCULO QUE SE VA REALIZAR DE ACUERDO A LA OPCION DEL COMBO 
        $tipo = $_SESSION['tipoDato'];  
    
        for ($i = 0; $i < 12; $i++) { 
            $opTec[$i] = 0;
            for ($j = 0; $j < 15; $j++ ) { 
                $subDurTec[$i][$j] = 0;
                $subDurOrg[$i][$j] = 0;
                $subProbTec[$i][$j] = "-"; 
                $subPercentTec[$i][$j] = 0; 
                $subProbOrg[$i][$j] = ""; 
                $subPercentOrg[$j][$i] = 0;
                $subProbCali[$i][$j] = "-";
                $subProbCali[$i][$j] = "";
                $subPzasCali[$i][$j] = 0;
                $subPercentCali[$i][$j] = 0;
            } 
        } 
        
        //CONTADORES
        $acumPercentTec = 0;
        $acumPercentOrg = 0;
        
        //POCENTAJES ACUMULADOS DE OEE DE ACUERDO A LINEA Y FECHA  
        $percentPerTopic = cPercentsTopicPeriodLine($_SESSION['linea'], $fIni, $fFin);
        for ($i = 0; $i < count($percentPerTopic); $i++){
            $percentGeneralTech = $percentPerTopic[$i][0];
            $percentGeneralOrg = $percentPerTopic[$i][1];
            $percentGeneralCali = $percentPerTopic[$i][2];
        }
        
        //TIEMPOS TOTALES REGISTRADOS  
        $datMinTec = lossesTimeTechnicals($_SESSION['linea'], $fIni, $fFin); 
        $datMinOrg = lossesTimeOrganizational($_SESSION['linea'], $fIni, $fFin); 
        $datMinCali = lossesTimeQuality($_SESSION['linea'], $fIni, $fFin); 
        
        //OBTENCION DE TOPS POR TEMA
        switch ($tipo) { 
            case 1:
            case 3:                 
                //TECNICAS
                $acumPercentTec = 0;
                $acumTimeTec = 0;
                $datTop3DiaTecOp = t3TecDiaOpLosses($_SESSION['linea'], $fIni, $fFin);
                $contTec = count($datTop3DiaTecOp); 
                for ($i = 0; $i < count($datTop3DiaTecOp); $i++ ){ 
                    $opTec[$i] = $datTop3DiaTecOp[$i][0]; 
                    if (!empty($datTop3DiaTecOp[$i][1]) && !empty($datTop3DiaTecOp[$i][2])){
                        $descTec[$i] = strtoupper($opTec[$i].' ('.$datTop3DiaTecOp[$i][1].')/'.$datTop3DiaTecOp[$i][2]) ; 
                    } else { 
                        if (!empty($datTop3DiaTecOp[$i][1]) && empty($datTop3DiaTecOp[$i][2])){
                            $descTec[$i] = strtoupper($opTec[$i].' ('.$datTop3DiaTecOp[$i][1].')') ; 
                        } else if (empty($datTop3DiaTecOp[$i][1]) && !empty($datTop3DiaTecOp[$i][2])){
                            $descTec[$i] = strtoupper($opTec[$i].'/'.$datTop3DiaTecOp[$i][2]);  
                        } else {
                            $descTec[$i] = strtoupper($opTec[$i]);
                        }
                    } 
                    
                    $durTec[$i] = $datTop3DiaTecOp[$i][3];
                    $frecTec[$i] = $datTop3DiaTecOp[$i][4]; 
                    $percentTec[$i] = @round(($durTec[$i]*$percentGeneralTech)/$datMinTec[0][0],2); 
                    $acumTimeTec += $durTec[$i]; 
                    $acumPercentTec += $percentTec[$i]; 
                    $sCTec[$i] = 0; 
                    $acumSubPercentTec[$i] = 0; 
                    
                    $datTop3DiaTecProblemaOp = t3ProblemTecDiaOpLosses($_SESSION['linea'], $fIni, $fFin, $opTec[$i]); 
                    $sCTec[$i] = count($datTop3DiaTecProblemaOp); 
                    for ($j = 0; $j < count($datTop3DiaTecProblemaOp); $j++) { 
                        $subProbTec[$i][$j] = strtoupper($datTop3DiaTecProblemaOp[$j][0]); 
                        $subDurTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][1]; 
                        $subFrecTec[$i][$j] = $datTop3DiaTecProblemaOp[$j][2]; 
                        $subPercentTec[$i][$j] = @round(($subDurTec[$i][$j]*$percentTec[$i])/$durTec[$i],2); 
                        $acumSubPercentTec[$i] += $subPercentTec[$i][$j]; 
                        $sNPTec[$i][$j] = 0; 
                        
                        //TOP DE NUMERO DE PARTE
                        $datTopNoParteTec = t3NoParteTecDiaOpLosses($_SESSION['linea'], $fIni, $fFin, $opTec[$i], $subProbTec[$i][$j]);
                        $sNPTec[$i][$j] = count($datTopNoParteTec);
                        for ($k = 0; $k < count($datTopNoParteTec); $k++){
                            $noPartTec[$i][$j][$k] = $datTopNoParteTec[$k][0];
                            $durNoPartTec[$i][$j][$k] = $datTopNoParteTec[$k][1];
                            $frecNoPartTec[$i][$j][$k] = $datTopNoParteTec[$k][2];                            
                            $percentNoPartTec[$i][$j][$k] = @round(($durNoPartTec[$i][$j][$k]*$subPercentTec[$i][$j])/$subDurTec[$i][$j],2); 
                        } 
                    }
                    
                    //COMPARACION PARA SACAR CANTIDADES DE OTROS
                    if ($acumSubPercentTec[$i] < $percentTec[$i] && $acumSubPercentTec[$i] > 0 ) {                         
                        $contPOTec = $sCTec[$i]; 
                        $subProbTec[$i][$contPOTec] = "OTROS"; 
                        $subPercentTec[$i][$contPOTec] = @round($percentTec[$i] - $acumSubPercentTec[$i],2); 
                        $sNPTec[$i][$contPOTec] = 0; 
                        $sCTec[$i] = $contPOTec+1; 
                    }                     
                } 
                
                if ($acumPercentTec < @round($percentGeneralTech,2) && $percentGeneralTech > 0 ) { 
                    $descTec[$contTec] = "OTROS";
                    $durTec[$contTec] = (int)$datMinTec[0][0] - (int)$acumTimeTec;
                    $percentTec[$contTec] = @round($percentGeneralTech - $acumPercentTec,2); 
                    $sCTec[$contTec] = 0; 
                    $acumPercentSubOtrosTec = 0; 
                    
                    //HACEMOS EL TOP DE LOS 10 PROBLEMAS DISTINTOS DE LAS OPERACIONES QUE YA TENEMOS
                    //OBENEMOS EL TOTAL DE PROBLEMAS RESTANTES
                    $datMinOtrosTec = tiempoTecOtrosLosses($_SESSION['linea'], $fIni, $fFin, $opTec[0], $opTec[1], $opTec[2], $opTec[3], $opTec[4]); 
                    $minOtrosTec = $datMinOtrosTec[0][0]; 
                    
                    $datTopOtrosTec = topProblemOtrosTecDiaOpLosses($_SESSION['linea'], $fIni, $fFin, $opTec[0], $opTec[1], $opTec[2], $opTec[3], $opTec[4]); 
                    for ($i = 0; $i < count($datTopOtrosTec); $i++) { 
                        $subProbTec[$contTec][$i] = $datTopOtrosTec[$i][1]; 
                        $subDurTec[$contTec][$i] = $datTopOtrosTec[$i][2]; 
                        $subFrecTec[$contTec][$i] = $datTopOtrosTec[$i][3]; 
                        $subPercentTec[$contTec][$i] = @round(($subDurTec[$contTec][$i]*$percentTec[$contTec])/$minOtrosTec,2); 
                        $acumPercentSubOtrosTec += $subPercentTec[$contTec][$i]; 
                        $sCTec[$contTec] = $i+1; 
                        $sNPTec[$contTec][$i] = 0; 
                    }
                    
                    if ($acumPercentSubOtrosTec < $percentTec[$contTec] && $acumPercentSubOtrosTec > 0 ) {                         
                        $contPOTec = count($datTopOtrosTec);    
                        $subProbTec[$contTec][$contPOTec] = "OTROS"; 
                        $subPercentTec[$contTec][$contPOTec] = @round($percentTec[$contTec] - $acumPercentSubOtrosTec,2);
                        $sNPTec[$contTec][$contPOTec] = 0;
                        $sCTec[$contTec] = $contPOTec+1; 
                    }                    
                    $contTec++; 
                } 
                
                # ---------------- ORGANIZACIONALES
                $datTop3DiaOrgArea = t3OrgDiaAreaLosses($_SESSION['linea'], $fIni, $fFin);
                for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) { 
                    $areaOrg[$i] = strtoupper($datTop3DiaOrgArea[$i][0]); 
                    $durOrg[$i] = $datTop3DiaOrgArea[$i][1]; 
                    $frecOrg[$i] = $datTop3DiaOrgArea[$i][2]; 
                    $percentOrg[$i] = @round(($durOrg[$i]*$percentGeneralOrg)/$datMinOrg[0][0],2); 
                    $sCOrg[$i] = 0; 
                    $acumSubPercentOrg[$i] = 0; 
                    $acumPercentOrg += $percentOrg[$i];  
                    $datTop3DiaOrgProblemaOp = t3ProblemOrgDiaAreaLosses($_SESSION['linea'], $fIni, $fFin, $areaOrg[$i]); 
                    $sCOrg[$i] = count($datTop3DiaOrgProblemaOp); 
                    for ($j = 0; $j < count($datTop3DiaOrgProblemaOp); $j++) {                        
                        $sProbOrg[$i][$j] = strtoupper($datTop3DiaOrgProblemaOp[$j][0]); 
                        $sMatOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][1]; 
                        if ($sMatOrg[$i][$j] != "") { 
                            $subProbOrg[$i][$j] = $sProbOrg[$i][$j].' ['.$sMatOrg[$i][$j].']'; 
                        } else { 
                            $subProbOrg[$i][$j] = $sProbOrg[$i][$j]; 
                        }                         
                        $subDurOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][2]; 
                        $subFrecOrg[$i][$j] = $datTop3DiaOrgProblemaOp[$j][3]; 
                        $subPercentOrg[$i][$j] = @round(($subDurOrg[$i][$j]*$percentOrg[$i])/$durOrg[$i],2); 
                        $acumSubPercentOrg[$i] += $subPercentOrg[$i][$j]; 
                        $sNPOrg[$i][$j] = 0; 
                        //TOP DE NUMERO DE PARTE
                        $datTopNoParteOrg = t3NoParteOrgDiaAreaLosses($_SESSION['linea'], $fIni, $fFin, $areaOrg[$i], $subProbOrg[$i][$j]); 
                        $sNPOrg[$i][$j] = count($datTopNoParteOrg);
                        for ($k = 0; $k < count($datTopNoParteOrg); $k++ ) { 
                            $noPartOrg[$i][$j][$k] = $datTopNoParteOrg[$k][0]; 
                            $durNoPartOrg[$i][$j][$k] = $datTopNoParteOrg[$k][1]; 
                            $frecNoPartOrg[$i][$j][$k] = $datTopNoParteOrg[$k][2]; 
                            $percentNoPartOrg[$i][$j][$k] = @round(($durNoPartOrg[$i][$j][$k]*$subPercentOrg[$i][$j])/$subDurOrg[$i][$j],2); 
                        }                        
                    } 
                    if ($acumSubPercentOrg[$i] != $percentOrg[$i] && $acumSubPercentOrg[$i] > 0 ) {
                        $contOrg = count($datTop3DiaOrgProblemaOp);
                        $subProbOrg[$i][$contOrg] = "OTROS";
                        $subPercentOrg[$i][$contOrg] = @round($percentOrg[$i]-$acumSubPercentOrg[$i],2); 
                        $sNPOrg[$i][$contOrg] = 0; 
                        $sCOrg[$i] = count($datTop3DiaOrgProblemaOp)+1;                        
                    }                    
                }
                
                //CALIDAD
                $datTop3DiaCaliArea = t3CaliDiaOpLosses($_SESSION['linea'], $fIni, $fFin);
                for ($i = 0; $i < count($datTop3DiaCaliArea); $i++ ) { 
                    $opC[$i] = $datTop3DiaCaliArea[$i][0]; 
                    $opCali[$i] = $datTop3DiaCaliArea[$i][0]; 
                    $maCali[$i] = $datTop3DiaCaliArea[$i][1]; 
                    $descCali[$i] = $datTop3DiaCaliArea[$i][2]; 
                    if (!empty($maCali[$i]) && !empty($descCali[$i])){ 
                        $opCali[$i] = strtoupper($opCali[$i].' ('.$maCali[$i].')/'.$descCali[$i]); 
                    } else {
                        if (!empty($maCali[$i]) && empty($descCali[$i])){ 
                            $opCali[$i] = strtoupper($opCali[$i].' ('.$maCali[$i].')'); 
                        } 
                    } 
                    
                    $pzasCali[$i] = @round(($datTop3DiaCaliArea[$i][3]* $percentGeneralCali)/$datMinCali[0][0],2);  
                    //$datTop3DiaCaliArea[$i][3]; 
                    $frecCali[$i] = $datTop3DiaCaliArea[$i][4]; 
                    $sCCali[$i] = 0; 
                    $datTop3DiaCaliProblemaOp = t3ProblemCaliDiaOpLosses($_SESSION['linea'], $fIni, $fFin, $opC[$i]); 
                    for ($j = 0; $j < count($datTop3DiaCaliProblemaOp); $j++) { 
                        $subProbCali[$i][$j] = strtoupper($datTop3DiaCaliProblemaOp[$j][0]); 
                        $subPzasCali[$i][$j] = $datTop3DiaCaliProblemaOp[$j][1]; 
                        $subFrecCali[$i][$j] = $datTop3DiaCaliProblemaOp[$j][2]; 
                        $subPercentCali[$i][$j] = @round(($subPzasCali[$i][$j] * $percentGeneralCali)/$datMinCali[0][0],3); 
                        $sCCali[$i] = $j+1; 
                        $sNPCali[$i][$j] = 0;
                        //OBTENCION DE NUMERO DE PARTE
                        $datTopNoParteCali = t3NoParteCaliDiaOpLosses($_SESSION['linea'], $fIni, $fFin, $opC[$i], $subProbCali[$i][$j]);
                        for ($k = 0; $k < count($datTopNoParteCali); $k++) {
                            $noPartCali[$i][$j][$k] = $datTopNoParteCali[$k][0]; 
                            $pzasNoPartCali[$i][$j][$k] = $datTopNoParteCali[$k][1]; 
                            $frecNoPartCali[$i][$j][$k] = $datTopNoParteCali[$k][2]; 
                            $sNPCali[$i][$j] = $k+1; 
                        } 
                    } 
                } 
                
//                if ($acumPercentCali < @round($percentGeneralCali,2) && $percentGeneralCali > 0 ) { 
//                    $contCali = count($datTop3DiaCaliArea)+1;
//                    $descCali[$contCali] = "OTROS";
//                    $durCali[$contCali] = (int)$datMinTec[0][0] - (int)$acumTimeTec;
//                    $percentTec[$contCali] = @round($percentGeneralTech - $acumPercentTec,2); 
//                    $sCCali[$contCali] = 0; 
//                    $acumPercentSubOtrosTec = 0; 
//                    
//                    //HACEMOS EL TOP DE LOS 10 PROBLEMAS DISTINTOS DE LAS OPERACIONES QUE YA TENEMOS
//                    //OBENEMOS EL TOTAL DE PROBLEMAS RESTANTES
//                    $datMinOtrosTec = tiempoTecOtrosLosses($_SESSION['linea'], $fIni, $fFin, $opTec[0], $opTec[1], $opTec[2], $opTec[3], $opTec[4]); 
//                    $minOtrosTec = $datMinOtrosTec[0][0]; 
//                    
//                    $datTopOtrosTec = topProblemOtrosTecDiaOpLosses($_SESSION['linea'], $fIni, $fFin, $opTec[0], $opTec[1], $opTec[2], $opTec[3], $opTec[4]); 
//                    for ($i = 0; $i < count($datTopOtrosTec); $i++) { 
//                        $subProbTec[$contTec][$i] = $datTopOtrosTec[$i][1]; 
//                        $subDurTec[$contTec][$i] = $datTopOtrosTec[$i][2]; 
//                        $subFrecTec[$contTec][$i] = $datTopOtrosTec[$i][3]; 
//                        $subPercentTec[$contTec][$i] = @round(($subDurTec[$contTec][$i]*$percentTec[$contTec])/$minOtrosTec,2); 
//                        $acumPercentSubOtrosTec += $subPercentTec[$contTec][$i]; 
//                        $sCTec[$contTec] = $i+1; 
//                        $sNPTec[$contTec][$i] = 0; 
//                    }
//                    
//                    if ($acumPercentSubOtrosTec < $percentTec[$contTec] && $acumPercentSubOtrosTec > 0 ) {                         
//                        $contPOTec = count($datTopOtrosTec);    
//                        $subProbTec[$contTec][$contPOTec] = "OTROS"; 
//                        $subPercentTec[$contTec][$contPOTec] = @round($percentTec[$contTec] - $acumPercentSubOtrosTec,2);
//                        $sNPTec[$contTec][$contPOTec] = 0;
//                        $sCTec[$contTec] = $contPOTec+1; 
//                    }                    
//                    $contTec++; 
//                } 
                
                
                break;
            case 2:  
                $datTop3DiaTecOp = t3TecDiaOpLossesFrec($_SESSION['linea'], $pDiaI, $pDiaF);
                $datTop3DiaOrgArea = t3OrgDiaAreaLossesFrec($_SESSION['linea'], $pDiaI, $pDiaF); 
                $datTop3DiaCaliOp = t3CaliDiaOpLossesFrec($_SESSION['linea'], $pDiaI, $pDiaF); 

                //HASTA AQUI MODIFIQUE
                $datTop3MesTecOp = t3TecMesopLossesFrec($_SESSION['linea'], $year, $month); 
                $datTop3MesOrgArea = t3OrgMesAreaLossesFrec($_SESSION['linea'], $year, $month); 
                $datTop3MesCaliOp = t3CaliMesOpLossesFrec($_SESSION['linea'], $year, $month); 
                
                break;
        }
    ?>
    
        <a align=center id="headerFixed" class="contenedor"> 
            <div class='fila0'> 
            </div> 
            <h5 class="tituloPareto" style="color: #FFFFFF" > 
                <?php echo 'LOSSES<br>'.$_SESSION['nameLinea']; ?>
                <div class="col-lg-12 col-sm-12 col-md-12" style="margin-top: 6px; " > 
                    <div class="col-lg-4 col-sm-4 col-md-4" >
                    </div>
                    <div class="col-lg-4 col-sm-4 col-md-4 contenidoCentrado" style="z-index: 1001; " > 
                        <input class="col-md-5 btn-sm btn-secondary contenidoCentrado" style="border-radius: 5px; border: 1px solid #474545; " type="text" name="dateIni" id="dateIni" value="<?php echo date("m/d/Y", strtotime($fIni)); ?>" > 

                        <input class="col-md-5 btn-sm btn-secondary contenidoCentrado" style="margin-left: 3.5%; border-radius: 5px; border: 1px solid #474545;" type="text" name="dateEnd" id="dateEnd" placeholder="Dia Final" value="<?php echo date("m/d/Y", strtotime($fFin)); ?>">
                    </div> 
                    <div class="col-lg-6 col-sm-6 col-md-3">
                    </div>
                </div> 
            </h5> 
            <div class="fila1"> 
                <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%"> 
                <form action="../../iLinea.php" method="POST">
                    <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                            onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 40px; height: 40px; margin-left: -5px; margin-top: -5px " >
                    </button> 
                </form>  
                <form action="../jidokas/oee.php" >
                    <button class="btn btn-success btn-sm btnTop3" onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer'" onmouseout="this.style.background='#008C5B'" >
                        Regresar
                    </button>
                </form>                 
            </div>             
        </a> 
        
        <!--FUNCIONALIDAD DE LOS DATEPICKET-->
        <script>
            jQuery().ready( 
                function() { 
                    getResult();
                    //setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
                } 
            );

            function getResult() { 
                //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                var ini = document.getElementById("dateIni").value;
                var fin = document.getElementById("dateEnd").value;
                $.ajax({
                    url: "../../db/sesionReportes.php",
                    type: "post",
                    data: { tipoVista: 1, fIni: ini, fFin: fin },
                    success: function (result) {
                    }
                }); 
            }

            function setTipoDatos() {
                //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                var ini = document.getElementById("dateIni").value;
                var fin = document.getElementById("dateEnd").value;
                $.ajax({
                    url: "../../db/sesionReportes.php",
                    type: "post",
                    data: { tipoVista: 1, fIni: ini, fFin: fin },
                    success: function (result) { 
                        //location.reload(); 
                    }
                }); 
            }

            $(function() {
                $("#dateIni").datepicker({
                    //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                    maxDate: new Date(), 
                    //OYENTES DE LOS PICKERS 
                    onSelect: function(date) {
                        //Cuando se seleccione una fecha se cierra el panel
                        $("#ui-datepicker-div").hide();                                

                        //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                        var ini = document.getElementById("dateIni").value;
                        var fin = document.getElementById("dateEnd").value; 

                        //console.log("tipo: "+tipo+" op: "+op+" ini: "+ini+" fin: "+fin);
                        if (new Date(fin) < new Date(date) ){
                            alert("OPS! REVISAR RANGO DE FECHA ");
                        } else {
                            //Mandamos el ajax para la actualizacion de la tabla                                     
                            $.ajax({
                                url: "../../db/sesionReportes.php",
                                type: "post",
                                data: { tipoVista: 1, fIni: ini, fFin: fin },
                                success: function (result) { 
                                    location.reload(); 
                                    //jQuery("#pnlGraficas").html(result);
                                }
                            }); 
                        }
                    }
                });

                $("#dateEnd").datepicker({
                    //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                    //OYENTES DE LOS PICKERS 
                    maxDate: new Date(),
                    onSelect: function ( date ) { 
                        //cuando se seleccione una fecha se cierra el panel
                        $("#ui-datepicker-div").hide(); 
                        //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                        var ini = document.getElementById("dateIni").value;
                        var fin = document.getElementById("dateEnd").value; 
                        if (new Date(ini) > new Date(date) ) {
                            alert("OPS! REVISAR RANGO DE FECHA ");
                        } else {                                
                            //mandamos el ajax para la actuilizacion de la tabla 
                            $.ajax({
                                url: "../../db/sesionReportes.php",
                                type: "post",
                                data: { tipoVista: 1, fIni: ini, fFin: fin },
                                success: function (result) { 
                                    location.reload(); 
                                    //jQuery("#pnlGraficas").html(result); 
                                }
                            });
                        }
                    } 
                }); 
            }); 
        </script>     
    </head>
   
    <body >
        <div> 
            <div id ="pnlHoja" style="overflow-y: auto;" >                 
                <div class="panel-group col-lg-12" id="accordion" style="margin-top:50px; ">
                    <div class="panel panel-default"> 
                        <div class="panel-heading"> 
                            <h4 class="panel-title"> 
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Technical Losses</a> 
                            </h4> 
                        </div> 
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <div class="demo-containerTec" style="width: 100%">                                     
                                    <div id="gTec" >
                                        <script>
                                            var dataTec = [
                                            <?php for ($i = 0; $i < $contTec; $i++ ) { ?>  
                                            {
                                                "category": "<?php echo $descTec[$i] ?>", 
                                                "income": <?php echo $percentTec[$i]; ?>, 
                                                "url": "#", 
                                                "description": "click to drill-down", 
                                                "months": [ 
                                                <?php for ($j = 0; $j < $sCTec[$i]; $j++){ ?> 
                                                    { "category": "<?php echo utf8_decode($subProbTec[$i][$j]) ?>", "income": <?php echo $subPercentTec[$i][$j] ?> },
                                                <?php } ?>  
                                                ] 
                                            }, 
                                            <?php } ?> 
                                            ];

                                            var chartTec = AmCharts.makeChart("gTec", {
                                                "type": "serial",
                                                "theme": "none",
                                                "pathToImages": "/lib/3/images/",
                                                "autoMargins": false,
                                                "marginLeft": 33,
                                                "marginRight": 8,
                                                "marginTop": 10,
                                                "marginBottom": 26,
                                                "titles": [{
                                                    "text": "LOSSES: Técnicas"
                                                }],
                                                "dataProvider": dataTec,
                                                "startDuration": 1,
                                                "graphs": [{
                                                    "alphaField": "alpha",
                                                    "balloonText": "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b> [[additional]]</span>",
                                                    "dashLengthField": "dashLengthColumn",
                                                    "fillAlphas": 1,
                                                    "title": "Income",
                                                    "type": "column",
                                                    "lineColor": "#02538b",
                                                    "valueField": "income","urlField":"url"
                                                }],
                                                    "categoryField": "category",
                                                    "categoryAxis": {
                                                        "gridPosition": "rigth",
                                                        "gridAlpha": 0,
                                                        "labelFunction": function(valueText, serialDataItem, categoryAxis) {
                                                          if (valueText.length > 15)
                                                            return valueText.substring(0, 15) + '...';
                                                          else
                                                            return valueText;
                                                        }
                                                    }
                                            });

                                            chartTec.addListener("clickGraphItem", function (event) { 
                                                if ( 'object' === typeof event.item.dataContext.months ) { 
                                                    // set the monthly data for the clicked month
                                                    event.chart.dataProvider = event.item.dataContext.months;
                                                    // update the chart title
                                                    event.chart.titles[0].text = event.item.dataContext.category + ' ';
                                                    // let's add a label to go back to yearly data
                                                    event.chart.addLabel(
                                                        "34", !40, 
                                                        "< Go back",
                                                        "left", 
                                                        17, 
                                                        undefined, 
                                                        undefined, 
                                                        undefined, 
                                                        true, 
                                                        'javascript:resetGTec();');

                                                    // validate the new data and make the chart animate again
                                                    event.chart.validateData();
                                                    event.chart.animateAgain();
                                                }
                                            });

                                            // function which resets the chart back to yearly data
                                            function resetGTec() {
                                                chartTec.dataProvider = dataTec;
                                                chartTec.titles[0].text = 'LOSSES: TECNICAS';

                                                // remove the "Go back" label
                                                chartTec.allLabels = [];

                                                chartTec.validateData();
                                                chartTec.animateAgain();
                                            }
                                        </script>
                                    </div>
                                    
                                    <!-- TABLA DE DESCRIPTIVA -->
                                    <table style="border: 1px solid #BABABA; width: 100%; font-size: 11px; margin-top: 15px" >
                                        <tr >
                                            <?php for ($i = 1; $i <= $contTec; $i++ ) { 
                                                if (strlen($descTec[$i-1]) > 1){ ?>
                                                    <th style="<?PHP if ($i%2 == 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center;" ><?php echo $descTec[$i-1].'<br>'.$durTec[$i-1].'min - '. $percentTec[$i-1].'%' ?></th>
                                            <?php } } ?>                                        
                                        </tr>
                                        <?php for ($i = 1; $i <= 5; $i++ ) { ?> 
                                        <tr> 
                                            <?php for ($j = 0; $j < $contTec; $j++) { ?> 
                                                <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA;">
                                                    <?php echo $subProbTec[$j][$i-1].'<br><b>'.$subDurTec[$j][$i-1].' min - '.$subPercentTec[$j][$i-1].'%</b' ?></td> 
                                            <?php } ?> 
                                        </tr> 
                                        <?php } ?>
                                    </table> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Organizational Losses</a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body" >
                                <!-- ORGANIZACIONALES -->
                                <div class="demo-containerOrg" style="width: 98%;" >                                    
                                    <div id="gOrg" style="margin-left: 2%" >
                                        <script>
                                            var dataOrg = [
                                            <?php for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) {  ?> 
                                            {
                                                "category": "<?php echo $areaOrg[$i]; ?>",
                                                "income": <?php echo $percentOrg[$i]; ?>, 
                                                "url": "#", 
                                                "description": "click to drill-down",
                                                "months": [ 
                                                <?php for ($j = 0; $j < $sCOrg[$i]; $j++){ ?> 
                                                    { "category": "<?php echo utf8_decode($subProbOrg[$i][$j]) ?>", "income": <?php echo $subPercentOrg[$i][$j] ?> },
                                                <?php } ?>  
                                                ] 
                                            }, 
                                            <?php } ?> 
                                            ];

                                            var chartOrg = AmCharts.makeChart("gOrg", {
                                                "type": "serial", 
                                                "theme": "none", 
                                                "pathToImages": "/lib/3/images/", 
                                                "autoMargins": false, 
                                                "marginLeft": 33, 
                                                "marginRight": 8, 
                                                "marginTop": 10, 
                                                "marginBottom": 26, 
                                                "titles": [{ 
                                                    "text": "LOSSES: ORGANIZACIONALES" 
                                                }], 
                                                "dataProvider": dataOrg, 
                                                "startDuration": 1, 
                                                "graphs": [{
                                                    "alphaField": "alpha",
                                                    "balloonText": "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b> [[additional]]</span>",
                                                    "dashLengthField": "dashLengthColumn",
                                                    "fillAlphas": 1,
                                                    "title": "Income",
                                                    "type": "column",
                                                    "lineColor": "#FFB5A6",
                                                    "valueField": "income","urlField":"url"
                                                }],
                                                    "categoryField": "category",
                                                    "categoryAxis": {
                                                        "gridPosition": "rigth",
                                                        "gridAlpha": 0,
                                                        "labelFunction": function(valueText, serialDataItem, categoryAxis) {
                                                          if (valueText.length > 15)
                                                            return valueText.substring(0, 15) + '...';
                                                          else
                                                            return valueText;
                                                        }
                                                    }
                                            });

                                            chartOrg.addListener("clickGraphItem", function (event) { 
                                                if ( 'object' === typeof event.item.dataContext.months ) { 
                                                    // set the monthly data for the clicked month
                                                    event.chart.dataProvider = event.item.dataContext.months;
                                                    // update the chart title
                                                    event.chart.titles[0].text = event.item.dataContext.category + ' ';
                                                    // let's add a label to go back to yearly data
                                                    event.chart.addLabel(
                                                        "34", !40, 
                                                        "< Go back",
                                                        "left", 
                                                        17,  
                                                        undefined, 
                                                        undefined, 
                                                        undefined, 
                                                        true, 
                                                        'javascript:resetGOrg();');

                                                    // validate the new data and make the chart animate again
                                                    event.chart.validateData();
                                                    event.chart.animateAgain();
                                                }
                                            });

                                            // function which resets the chart back to yearly data
                                            function resetGOrg() {
                                                chartOrg.dataProvider = dataOrg;
                                                chartOrg.titles[0].text = 'LOSSES: ORGANIZACIONALES';

                                                // remove the "Go back" label
                                                chartOrg.allLabels = [];

                                                chartOrg.validateData();
                                                chartOrg.animateAgain();
                                            }
                                        </script>
                                    </div>
                                    
                                    <!-- TABLA DE DESCRIPTIVA -->
                                    <table style="border: 1px solid #BABABA; width: 100%; font-size: 11px; margin-top: 15px" >
                                        <tr >
                                            <?php for ($i = 0; $i < count($datTop3DiaOrgArea); $i++ ) { ?>
                                                <th style="<?PHP if ($i%2 != 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center;" ><?php echo $areaOrg[$i]; ?></th>
                                                <th style="<?PHP if ($i%2 != 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center; width: 2%; "> <?php echo $durOrg[$i].'min - '.$percentOrg[$i].'%' ?> </th>                                         
                                            <?php } ?>

                                        </tr>
                                        <?php for ($i = 0; $i < 10;  $i++ ) { ?>
                                        <tr> 
                                            <?php for ($j = 0; $j < count($datTop3DiaOrgArea); $j++) { ?> 
                                                <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA;"><?php echo $subProbOrg[$j][$i] ?></td> 
                                                <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA; width: 2%; "><?php echo $subDurOrg[$j][$i].'min - '.$subPercentOrg[$j][$i].'%' ?></td> 
                                            <?php } ?> 
                                        </tr> 
                                        <?php } ?> 
                                    </table> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Quality Losses</a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div class="demo-containerCali">
                                    
                                    <div id="gCali" >
                                        <script>
                                            var dataCali = [
                                            <?php for ($i = 0; $i < count($datTop3DiaCaliArea); $i++ ) {  ?> 
                                            {
                                                "category": "<?php echo $opCali[$i]; ?>",
                                                "income": <?php echo $pzasCali[$i]; ?>, 
                                                "url": "#", 
                                                "description": "click to drill-down",
                                                "months": [ 
                                                <?php for ($j = 0; $j < $sCCali[$i]; $j++){ ?> 
                                                    { "category": "<?php echo utf8_decode($subProbCali[$i][$j]) ?>", 
                                                        "income": <?php echo $subPercentCali[$i][$j] ?> },
                                                <?php } ?>  
                                                ] 
                                            }, 
                                            <?php } ?> 
                                            ];

                                            var chartCali = AmCharts.makeChart("gCali", {
                                                "type": "serial",
                                                "theme": "none",
                                                "pathToImages": "/lib/3/images/",
                                                "autoMargins": false,
                                                "marginLeft": 33,
                                                "marginRight": 8,
                                                "marginTop": 10,
                                                "marginBottom": 26,
                                                "titles": [{
                                                    "text": "LOSSES: CALIDAD"
                                                }],
                                                "dataProvider": dataCali, 
                                                "startDuration": 1, 
                                                "graphs": [{ 
                                                    "alphaField": "alpha", 
                                                    "balloonText": "<span style='font-size:13px;'>[[category]]:<b>[[value]]</b> [[additional]]</span>", 
                                                    "dashLengthField": "dashLengthColumn", 
                                                    "fillAlphas": 1, 
                                                    "title": "Income", 
                                                    "type": "column", 
                                                    "lineColor": "#A61212", 
                                                    "valueField": "income","urlField":"url" 
                                                }],
                                                    "categoryField": "category",
                                                    "categoryAxis": {
                                                        "gridPosition": "rigth",
                                                        "gridAlpha": 0,
                                                        "labelFunction": function(valueText, serialDataItem, categoryAxis) {
                                                          if (valueText.length > 35)
                                                            return valueText.substring(0, 30) + '...';
                                                          else
                                                            return valueText;
                                                        }
                                                    }
                                            });

                                            chartCali.addListener("clickGraphItem", function (event) { 
                                                if ( 'object' === typeof event.item.dataContext.months ) { 
                                                    // set the monthly data for the clicked month
                                                    event.chart.dataProvider = event.item.dataContext.months;
                                                    // update the chart title
                                                    event.chart.titles[0].text = event.item.dataContext.category + ' ';
                                                    // let's add a label to go back to yearly data
                                                    event.chart.addLabel(
                                                        "34", !40, 
                                                        "< Go back",
                                                        "left", 
                                                        17, 
                                                        undefined, 
                                                        undefined, 
                                                        undefined, 
                                                        true, 
                                                        'javascript:resetGCali();');

                                                    // validate the new data and make the chart animate again
                                                    event.chart.validateData();
                                                    event.chart.animateAgain();
                                                }
                                            });

                                            // function which resets the chart back to yearly data
                                            function resetGCali() {
                                                chartCali.dataProvider = dataCali;
                                                chartCali.titles[0].text = 'LOSSES: CALIDAD';

                                                // remove the "Go back" label
                                                chartCali.allLabels = [];

                                                chartCali.validateData();
                                                chartCali.animateAgain();
                                            }
                                        </script>
                                    </div>
                                    
                                    <div class="button-containerCali"> 
                                        <div id="backButtonCali"></div> 
                                    </div> 
                                    <!-- TABLA DE DESCRIPTIVA -->
                                    <table style="border: 1px solid #BABABA; width: 100%; font-size: 11px; margin-top: 15px" >
                                        <tr >
                                            <?php for ($i = 0; $i < count($datTop3DiaCaliArea); $i++ ) { ?>
                                                <th style="<?PHP if ($i%2 != 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center;" ><?php echo $opCali[$i]; ?></th>
                                                <th style="<?PHP if ($i%2 != 1){ ?> background: #f4f9ff; <?php } ?> border: 1px solid #BABABA; text-align: center; width: 2%; "> <?php echo $pzasCali[$i].'%' ?> </th>                                         
                                            <?php } ?>

                                        </tr>
                                        <?php for ($i = 0; $i < 5;  $i++ ) { ?>
                                        <tr> 
                                            <?php for ($j = 0; $j < count($datTop3DiaCaliArea); $j++) { ?> 
                                            <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA;"><?php echo $subProbCali[$j][$i] ?></td> 
                                                <td align="center" style="<?PHP if ($j%2 != 1){ ?> background: #f4f9ff ; <?php } ?> width: 7.5%; border: 1px solid #BABABA; width: 2%; "><?php echo $subPercentCali[$j][$i].'%' ?></td> 
                                            <?php } ?> 
                                        </tr> 
                                        <?php } ?>
                                    </table> 
                                </div> 
                            </div> 
                        </div> 
                    </div>
                </div> 
            </div>
        </div>    
    </body>
</html>
