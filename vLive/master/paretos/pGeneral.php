<!--
    Created on  : 28/03/2019 15:00:01 PM
    Author      : Areli Pérez Calixto (PRR1TL)
    Description : Archivo maestro para TOP de tecnicas, calidad, y cambios de modelo
-->

<HTML>
    <LINK rel="stylesheet" href="../../css/tops.css"/> 
    <title>Pareto general</title> 
    <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" /> 
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <?php    
        include '../../db/ServerFunctions.php'; 
        session_start();
        $date = new DateTime;

        if (isset($_SESSION['linea'])) {
            $line = $_SESSION['linea'];
        } else {
            $line = 'L001';
        }
        
        $listOperations = operationsLine($line);
    ?>
    
    <head>
        <a align=center id="headerFixed" class="contenedor">   
            <div class='fila0'>                
            </div> 
            <h5 class="tituloPareto">
                <?php echo 'TOP 5: Organizacionales <br>','Linea:&nbsp'.$line.' - '.$_SESSION['nameLinea']; ?>
            </h5> 
            <div class="fila1"> 
                <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
                <form action="../../iLinea.php" method="POST">
                    <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                        onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 100%; height: 100%; margin-left: -5px; " >
                    </button> 
                </form> 
                <form action="../paretos/pT5.php" method="POST">
                    <button  class="btn btn-success btn-sm btnTop3" onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer'" onmouseout="this.style.background='#008C5B'" >
                        Regresar
                    </button> 
                </form> 
                <div class="pickers" >
                    <ul class="nav justify-content-center">
                        <li class="nav-item"> 
                            <input class="btn btn-secondary dropdown-toggle datesPickets " type="text" name="dateIni" id="dateIni" value="<?php echo "03/01/2019";?>">
                        </li> 
                        &nbsp;
                        <li class="nav-item"> 
                            <input class="btn btn-secondary dropdown-toggle datesPickets" type="text" name="dateEnd" id="dateEnd" placeholder="Dia Final" value="<?php echo "03/28/2019";?>">
                        </li> 
                        &nbsp;
                        <li class="nav-item"> 
                            <select class=" btn btn-secondary btn-sm datesPickets" name="tipo" id="tipo" onchange="setTipoDatos()" >
                                <option value="0" >Seleccione</option> 
                                <option value="1" href="#">Duración</option> 
                                <option value="2" href="#">Frecuencia</option> 
                                <option value="3" href="#">Piezas</option> 
                            </select> 
                        </li> 
                        &nbsp;
                    </ul>
                </div>
                   
                <!--FUNCIONALIDAD DE LOS DATEPICKET-->
                <script>
                    jQuery().ready(            
                        function() {
                            getResult();
                            //setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
                        }
                    );

                    function getResult() {
                        //CUANDO SE ACTIVA LA HOJA TENDREMOS QUE MANDAR SOLO EL TEMA, PARA PODER HACER LA CONSULTA ADECUADA
                        //PARA ELLO SE UTILIZA AJAX   
                        var tipo = document.getElementById("tipo").value;

                        //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                        var ini = document.getElementById("dateIni").value;
                        var fin = document.getElementById("dateEnd").value;
                        $.ajax({
                            url: "../../contenedores/paretos/pGeneral.php",
                            type: "post",
                            data: { tipoDato: tipo, fIni: ini, fFin: fin },
                            success: function (result) {
                                jQuery("#pnlGraficas").html(result);
                            } 
                        }); 
                    }

                    function setTipoDatos() { 
                        //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON
                        var tipo = document.getElementById("tipo").value;

                        //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                        var ini = document.getElementById("dateIni").value;
                        var fin = document.getElementById("dateEnd").value;
                        $.ajax({
                            url: "../../contenedores/paretos/pGeneral.php",
                            type: "post",
                            data: { tipoDato: tipo, fIni: ini, fFin: fin },
                            success: function (result) { 
                                jQuery("#pnlGraficas").html(result);
                            }
                        }); 
                    }

                    $(function() {
                        $("#dateIni").datepicker({
                            //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                            maxDate: new Date(),
                            //OYENTES DE LOS PICKERS 
                            onSelect: function(date) {
                                //Cuando se seleccione una fecha se cierra el panel
                                $("#ui-datepicker-div").hide();                                
                                //Pasamos los valores para que se hagan los calculos nuevamente
                                var tipo = document.getElementById("tipo").value;

                                //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                                var ini = document.getElementById("dateIni").value;
                                var fin = document.getElementById("dateEnd").value; 

                                console.log("tipo: "+tipo+" ini: "+ini+" fin: "+fin);
                                if (new Date(fin) < new Date(date) ){
                                    alert("OPS! REVISAR RANGO DE FECHA ");
                                } else {
                                    //Mandamos el ajax para la actualizacion de la tabla                                     
                                    $.ajax({
                                        url: "../../contenedores/paretos/pGeneral.php",
                                        type: "post",
                                        data: { tipoDato: tipo, fIni: ini, fFin: fin },
                                        success: function (result) {
                                            jQuery("#pnlGraficas").html(result);
                                        }
                                    }); 
                                } 
                            }
                        }); 

                        $("#dateEnd").datepicker({ 
                            //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                            maxDate: new Date(),
                            //OYENTES DE LOS PICKERS 
                            onSelect: function ( date ) {
                                //cuando se seleccione una fecha se cierra el panel
                                $("#ui-datepicker-div").hide();                                
                                //pasamos los valores para que se hagan los calculos nuevamente
                                var tipo = document.getElementById("tipo").value;

                                //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                                var ini = document.getElementById("dateIni").value;
                                var fin = document.getElementById("dateEnd").value;                
                                console.log("tipo: "+tipo+" ini: "+ini+" fin: "+fin);
                                if (new Date(ini) > new Date(date) ) {
                                    alert("OPS! REVISAR RANGO DE FECHA ");
                                } else {                                
                                    //mandamos el ajax para la actuilizacion de la tabla 
                                    $.ajax({ 
                                        url: "../../contenedores/paretos/pGeneral.php", 
                                        type: "post", 
                                        data: { tipoDato: tipo, fIni: ini, fFin: fin }, 
                                        success: function (result) { 
                                            jQuery("#pnlGraficas").html(result);
                                        }
                                    });
                                }
                            }
                        });
                    });                        
                </script> 
            </div> 
        </a> 
    </head>
   
    <body>
        <div id ="pnlHoja" > 
            <br><br>
            <div id="pnlGraficas" > 
            </div>                     
            <br><br><br><br>
        </div> 
    </body>