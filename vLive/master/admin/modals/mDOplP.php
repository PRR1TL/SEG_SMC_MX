<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

$cLineas = listarLineas();

?>
<form id="fDOplP" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mDOplP">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > ELIMINAR OPL </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertOplPD"></div> 
                    
                    <input id="idOplPD" name="idOplPD" class="no" >
                    <input id="idOplPrPD" name="idOplPrPD" class="no" >
                    <div> 
                        <div class="form-row"> 
                            <input id="tipOplLD" name="tipOplLD" value="1" class="no" > 
                            <div class="col-md-6 mb-1" >
                                <label >Proyecto</label> 
                                <select class="form-control" id="proyectoPD" name="proyectoPD" style="height: 33px; " > 
                                    <option value="1">L. Proyectos</option>
                                </select> 
                            </div> 
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" >Detectado por:</label> 
                                <input class="form-control"  id="detectaPD" name="detectaPD" maxlength="15" style="width: 80%" type="text" readonly >
                            </div>                             
                            
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" >Fecha Inicio: </label>
                                <input class="form-control" id="fRegistroPD" name="fRegistroPD" style="width: 55%" type="text" readonly >
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%" >
                    <br>
                    <div class="panel " id="pnlOplProjectPD" name="pnlOplProjectPD" style="margin-top: -3vh" >                         
                        <!--DATOS DE LA DESVIACION--> 
                        <div class="panel panel-info" style="width: 100%;" >
                            <div class="panel-heading" ><strong> Acci&oacute;n <strong></div>
                            <div class="panel-body" >
                                <div class="form-row">                                    
                                    <div class="col-md-5 mb-1">
                                        <label >Acci&oacute;n requerida</label> 
                                        <input type="text" class="form-control" id="accionPD" name="accionPD" placeholder="Accion requerida" readonly >                                      
                                    </div> 
                                    
                                    <div class="col-md-2 mb-2"> 
                                        <label > &Aacute;rea </label> 
                                        <input type="text" class="form-control" id="areaPD" name="areaPD" placeholder="&Aacute;rea" readonly > 
                                    </div> 
                                    <div class="col-md-2 mb-2" > 
                                        <label >Responsable</label> 
                                        <input type="text" class="form-control" id="responsablePD" name ="responsableP" placeholder="Responsable" readonly >
                                    </div> 
                                    <div class="col-md-2 mb-2" >  
                                        <label >F. Compromiso</label> 
                                        <input type="text" class="form-control" id="fCompromisoPD" name="fCompromisoPD" placeholder="F. COmpromiso" readonly >
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="form-row">                             
                        <div class="col-md-6"> 
                            <label class="my-1 mr-2" >Estado</label>
                            <select class="form-control" id="estadoPD" name="estadoPD" style="width: 90%; height: 33px;" > 
                                <option value = "1"> Accion definida</option>
                                <option value = "2"> Accion en proceso</option>
                                <option value = "3"> Accion ejecutada </option>
                                <option value = "4"> Accion cerrada </option>  
                            </select> 
                        </div> 
                        <div class="col-md-6" > 
                            <label class="my-1 mr-2" >Ecaluaci&oacute;n </label>
                            <select class="form-control" id="evaluacionPD" name="evaluacionPD" style="width: 90%; height: 33px;" >
                                <option value = "0" > Acorde a plan </option> 
                                <option value = "1" > ¡ATENCION! Mejoras requeridas </option> 
                                <option value = "2" > ¡PROBLEMA! Contramedidas requeridas </option> 
                            </select> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer" > 
                    <button type="button" class="btn btn-primary" data-dismiss="modal" > CANCELAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw" > </i> </button> 
                    <button type="submit" class="btn btn-danger" > ELIMINAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw" > </i> </button>
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


