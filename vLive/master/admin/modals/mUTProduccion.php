<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));



if (isset($_SESSION["aMes"])){ 
    $fechaP = date("M, Y", strtotime($_SESSION["aMes"])); 
} else { 
    $fechaP = date("M, Y"); 
} 


?>
<form id="fUTProd" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mUTProd" > 
        <div class="modal-dialog modal-lg" style="width: 30vw" > 
            <div class="modal-content" > 
                <div class="modal-header" style="background: #02538B;" > 
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > ACTUALIZACION TARGETS DE PRODUCCION</h4> 
                </div> 
                <div class="modal-body" style=" max-height: calc(100vh - 180px); overflow-y: auto;" > 
                    <div id="alertUTProduccion" ></div> 
                    <div> 
                        <div class="form-row" > 
                            <div align="center" class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" >REGISTRA </label> 
                                <input class="form-control" id="detecta" name="detecta" maxlength="15" style="width: 80%" value="<?php echo $_SESSION["nickName"] ?>" type="text" readonly  />
                            </div> 
                            <div align="center" class="col-md-3" > 
                                <label class="my-1 mr-2" >LINEA </label> 
                                <input class="form-control" id="lineaTU" name="lineaTU" style="width: 90%; height: 33px;" readonly >                                    
                            </div> 
                            <div align="center" class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" >FECHA </label>
                                <input id="fProdU" name="fProdU" class="btn btn-secondary btn-sm" style="border-color: #888; width: 80%; " readonly />
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%; margin-top: 8px" > 
                    <div class="panel" id="pnlOplLinea" name="pnlOplLinea" style="margin-top: -1.5vh" > 
                        <div class="panel panel-info" style="width: 100%;" > 
                            <div class="panel-heading" ><strong> Targets <strong></div>
                            <div name="panelBody" class="panel-body" >
                                <div class="form-row" > 
                                    <div align="center" class="col-md-3" > 
                                        <label class="my-1 mr-2" >META</label> 
                                        <input id="mtaPzasU0" name="mtaPzasU0" class="no" > 
                                        <input id="mtaPzasU" name="mtaPzasU" class="form-control" maxlength="4" > 
                                    </div> 
                                    <div align="center" class="col-md-9" > 
                                        <label class="my-1 mr-2" >COMENTARIO</label> 
                                        <input id="cCambioU" name="cCambioU" class="form-control" required > 
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > ACTUALIZAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


