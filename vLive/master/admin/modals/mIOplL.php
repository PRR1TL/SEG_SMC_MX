<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

if (isset($_SESSION['usuario'])){ 
    $listaLinea = listar_Lineas_Producto($_SESSION['productoU']); 
} else {
    $listaLinea = listar_Lineas_Producto($_SESSION['productoL']);
} 

for ($i = 0; $i < count($listaLinea); $i++) { 
    $lblLinea[$i] = $listaLinea[$i][0]; 
    if (isset($listaLinea[$i][1])){ 
        $lblLinea[$i] = $listaLinea[$i][0].' - '.$listaLinea[$i][1]; 
    } 
} 
?>
<form id="fIOPL" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mIOPL">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > NUEVA OPL </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="datos_ajaxSEL"></div> 
                    <div> 
                        <div class="form-row"> 
                            <div class="col-md-3 no"> 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Tipo</label>
                                <select class="form-control" id="tipOpl" name="tipOpl" style="width: 90%; height: 33px; " onchange="cambioOpcionesIEvento()" >
                                    <option value="3" selected >Linea</option> 
                                </select> 
                            </div>                             
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Detectado por:</label> 
                                <input class="form-control"  id="detecta" name="detecta" maxlength="15" style="width: 80%" value="<?php echo $_SESSION["nickName"] ?>" type="text"  />
                            </div> 
                            <div class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Linea</label>
                                <!--<input class="form-control" id="linea" name="linea" maxlength="15" style="width: 80%" value="<?php echo $_SESSION["linea"] ?>" type="text"  />-->
                                <select class="form-control" id="linea" name="linea" style="height: 33px; width: 80%; " > 
                                    <?php for ($i = 0; $i < count($listaLinea); $i++) { ?> 
                                        <option value="<?php echo $listaLinea[$i][0]; ?>" <?php if ($_SESSION['linea'] == $listaLinea[$i][0] ){ ?> selected <?php } ?>  > <?php echo $lblLinea[$i]; ?> </option> 
                                    <?php } ?> 
                                </select> 
                            </div>                             
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Fecha Actual: </label>
                                <input class="form-control" id="fRegistro" name="fRegistro" style="width: 55%" value="<?php echo $fInicio ?>" type="text" readonly />
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%">
                    <br>
                    <div class="panel" id="pnlOplLinea" name="pnlOplLinea"  style="margin-top: -3vh"> 
                        <!--DATOS DE LA DESVIACION--> 
                        <div id="alertOpl" name="alertOpl"></div>
                        <div class="panel panel-info" style="width: 100%;">
                            <div class="panel-heading" ><strong> Datos desviacion<strong></div>
                            <div class="panel-body" >
                                <div class="form-row">
                                    <div class="col-md-2 mb-1">
                                        <label for="validationCustom01">Estacion</label> 
                                        <input type="text" class="form-control" id="operacion" name="operacion" placeholder="1210" value="" >                                      
                                    </div> 
                                    <div class="col-md-6 mb-3"> 
                                        <label for="validationCustom02"> Descripci&oacute;n </label> 
                                        <input type="text" class="form-control" id="hallazgoL" name="hallazgoL" placeholder="Descripci&oacute;n" > 
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <label for="validationCustomUsername">Causa</label> 
                                        <input type="text" class="form-control" id="causaL" name="causaL" placeholder="Causa" aria-describedby="inputGroupPrepend" >
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                        <!--DATOS DE LA ACCION--> 
                        <div class="panel panel-info" style="width: 100%;"> 
                            <div class="panel-heading" >Datos acción</div> 
                            <div class="panel-body" > 
                                <div class="form-row"> 
                                    <div class="col-md-6 mb-3"> 
                                        <label for="validationCustom01">Descripci&oacute;n</label> 
                                        <input type="text" class="form-control" id="accionL" name="accionL" placeholder="Descripción" value="" >                                      
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="validationCustom02">Responsable</label>
                                        <input type="text" class="form-control" id="responsableL" name="responsableL" placeholder="Responsable" >                                      
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="validationCustomUsername">F.Compromiso</label>
                                        <input type="text" class="form-control" id="fCompromisoL" name="fCompromisoL" value="<?php echo $fProp; ?>" placeholder="F.Compromiso" >
                                    </div>
                                </div>             
                            </div>
                        </div> 
                    </div>                     
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


