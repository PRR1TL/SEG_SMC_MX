<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

$cLineas = listarLineas();

?>

<script type="text/javascript" > 
    var a = 0; 
    var cST = 0; 
    
    function keyShipTo(e,id){ 
        (e.keyCode) ? ev = e.keyCode: ev = e.which; 
        //console.log('K: '+ev+', id: '+id);         
        var elemento = id; 
        // Si la tecla pulsada es enter (codigo ascii 13) 
        if( ev == 13 || ev == 9 ){ 
            // Si la variable id contiene "submit" enviamos el formulario 
            if(id=="submit"){ 
                document.forms[0].submit(); 
            } else { 
                // nos posicionamos en el siguiente input 
                document.getElementById(id).focus(); 
            } 
        } else { 
            var valE = document.getElementById(elemento).value; 
            var datA = []; 
            if (valE.length > 3){ 
                $.ajax({ 
                    url: '../../db/admin/cUsuarioType.php',
                    type: "POST",
                    data: { nombre: valE },
                    cache: false,
                    success: function(datos){
                        $("#c"+elemento).empty(); 
                        var respuesta = datos.trim();
                        var a1 = respuesta.split(",");
                        for (var i in a1) {
                            datA[i] = a1[i];
                            $("#c"+elemento).append("<option value='"+datA[i]+"' >"+datA[i]+"</option>");
                        }
                    } 
                }).fail( function( jqXHR, textStatus, errorThrown ) {
                    if (jqXHR.status === 0) {
                        alert('Not connect: Verify Network.');
                    } else if (jqXHR.status == 404) {
                        alert('Requested page not found [404]');
                    } else if (jqXHR.status == 500) {
                        alert('Internal Server Error [500].');
                    } else if (textStatus === 'parsererror') {
                        alert('Requested JSON parse failed.');
                    } else if (textStatus === 'timeout') {
                        alert('Time out error.');
                    } else if (textStatus === 'abort') {
                        alert('Ajax request aborted.');
                    } else {
                        alert('Uncaught Error: ' + jqXHR.responseText);
                    }
                }); 
            } else {
                $("#c"+elemento).append("<option value='-' > - </option>");
            }
        }
    } 

    function changeDatos() { 
        var linea = document.getElementById('cmbLineaLA').value; 
        var turno = document.getElementById('turnoC').value; 
        var fecha = document.getElementById('fRegistroC').value; 
        var datA = [];
        var c = 0;
        
        //CONSULTA DE TURNO 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/cListaAsistencia.php", 
            data: {linea: linea, turno: turno, fecha: fecha }, 
            success: function(datos) { 
                //LIMPIEZA DE COMPONENTES
                for (x = 1; x < 40; x++ ){ 
                    $('#iNombre'+x).val("");
                    $('#iATipo'+x).val(0);
                    $('#iUTipo'+x).val(1);
                } 
                
                //LLENA COMPONENTES DE LA BASE DE DATOS
                if (datos != 0){ 
                    var respuesta = datos.trim(); 
                    var a1 = respuesta.split(","); 
                    
                    //ASIGNACION DE VALORES A EL ARRAY
                    for (var i in a1) {                         
                        datA[i] = a1[i]; 
                        c++; 
                    } 
                    
                    var c1 = 0; 
                    var c2 = 0; 
                    var c3 = 1; 
                    
                    //RECORRIDO E IMPRESION DE VALORES EN LOS COMPONENTES
                    for (var j = 0; j < c; j++ ) { 
                        c1 = j+1; 
                        c2 = j+2;                      
                        if (j == 0 ){ 
                            $("#iNombre"+c1).val(datA[j]); 
                            $("#iATipo"+c1).val(datA[c1]); 
                            $('#iUTipo'+c1).val(datA[c2]); 
                        } else { 
                            //NUMEROS IMPARES 
                            if (j % 3 == 0 && j > 1 ) { 
                                c3++;
                                $("#iNombre"+c3).val(datA[j]); 
                                $("#iATipo"+c3).val(datA[c1]); 
                                $('#iUTipo'+c3).val(datA[c2]);  
                            } 
                        } 
                    } 
                } 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        //event.preventDefault(); 
    } 

</script>

<form id="fILAsistencia" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mILAsistencia" >
        <div class="modal-dialog modal-lg" style="width: 120vh" >
            <div class="modal-content" style="width: 110%" >
                <div class="modal-header" style="background: #02538B;" >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > Lista de asistencia </h4>
                </div>
                <div class="modal-body" style="max-height: calc(100vh - 190px); overflow-y: auto;" > 
                    <div id="alertILAsistencia"> </div> 
                    <div > 
                        <div class="form-row"> 
                            <div id="#alertILAsistencia"></div>                            
                            <div class="col-md-2" >  
                                <div class="col-md-10 contenidoCentrado" style="margin-top: 7%; " > 
                                    <label class="my-1 mr-2" >Linea: </label> 
                                    <select class="form-control" id="cmbLineaLA" name="cmbLineaLA" style="height: 33px" onchange="changeDatos()" > 
                                        <?php for ($i = 0; $i < count($cLineas); $i++) { ?> 
                                        <option <?php if ($cLineas[$i][0] == $linea ){ ?> selected <?php } ?> value="<?php echo $cLineas[$i][0]; ?>" > <?php echo $cLineas[$i][0]; ?> </option> 
                                        <?php } ?> 
                                    </select> 
                                </div> 
                            </div>  
                            <div class="col-md-4 " > 
                                <label class="my-1 mr-2" style="margin-left: 33%" >CONSULTA</label> 
                                <hr style="height: 1px; background-color: #B4B4B4; margin-top: -2%;" >
                                <div style="margin-top: -5%;" >
                                    <div class="col-md-5 contenidoCentrado" > 
                                        <label class="my-1 mr-2" >Fecha: </label>
                                        <input class="form-control" id="fRegistroC" name="fRegistroC" value="<?php echo $fInicio ?>" type="text" />
                                    </div> 
                                    <div class="col-md-6 contenidoCentrado" > 
                                        <label class="my-1 mr-2" > Turno: </label> 
                                        <select class="form-control" id="turnoC" name="turnoC" style="height: 33px;" onchange="changeDatos()" > 
                                            <option value="0" > --- </option> 
                                            <option value="1" > Primer Turno </option> 
                                            <option value="2" > Segundo Turno </option> 
                                            <option value="3" > Tercer Turno </option> 
                                        </select> 
                                    </div>  
                                </div> 
                            </div>  
                            <div class="col-md-1" > </div> 
                            <div class="col-md-5"  >
                                <label class="my-6 mr-6" style="margin-left: 35%" >REGISTRO</label>
                                <hr style="height: 1px; background-color: #B4B4B4; margin-top: -2%;" >
                                <div style="margin-top: -5%;" >
                                    <div class="col-md-4 contenidoCentrado" >                                     
                                        <label class="my-1 mr-2" >Fecha: </label>
                                        <input class="form-control" id="fRegistro" name="fRegistro" value="<?php echo $fInicio ?>" type="text" />
                                    </div>  
                                    <div class="col-md-5 contenidoCentrado" > 
                                        <label class="my-1 mr-2" > Turno: </label> 
                                        <select class="form-control" id="turno" name="turno" style="height: 33px;" onchange="changeTurno(event,this.value)" > 
                                            <option value="1" > Primer Turno </option> 
                                            <option value="2" > Segundo Turno </option> 
                                            <option value="3" > Tercer Turno </option> 
                                        </select> 
                                    </div> 
                                    <div class="col-md-3" > 
                                        <label class="my-1 mr-2" >Horas: </label> 
                                        <input class="form-control" id="horas" name="horas" type="text" /> 
                                    </div> 
                                </div>                              
                            </div>                              
                        </div>
                        <hr width="100%" > 
                        <div class="form-row" id="datos1" >                              
                            <div class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" >Nombre: </label>
                                <input class="form-control" id="iNombre1" name="iNombre1" type="text" onkeyup="keyShipTo(event,this.name)" >
                            </div> 
                            <div class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" >Consulta: </label>
                                <select class="form-control" id="ciNombre1" name="ciNombre1" style="height: 33px" > 
                                </select >                                
                            </div> 
                            <div class="col-md-2 contenidoCentrado" > 
                                <label class="my-1 mr-2" > Asistencia: </label> 
                                <select class="form-control" id="iATipo1" name="iATipo1" style="height: 33px" >
                                    <option value="0" > ---- </option> 
                                    <option value="1" > Asistencia </option> 
                                    <option value="2" > Baja </option> 
                                    <option value="3" > Capacitacion </option> 
                                    <option value="4" > Falta </option> 
                                    <option value="5" > Incapacidad </option> 
                                    <option value="6" > Permiso </option> 
                                    <option value="7" > Suspencion </option> 
                                    <option value="8" > Vacaciones </option> 
                                    <option value="9" > Retardo </option> 
                                </select >  
                            </div>
                            <div class="col-md-2 contenidoCentrado" > 
                                <label class="my-1 mr-2" > Puesto: </label> 
                                <select class="form-control" id="iUTipo1" name="iUTipo1" style="width: 70%; height: 33px" >
                                    <option value="1" > Operador </option> 
                                    <option value="2" > Ajustador </option> 
                                    <option value="3" > Team Leader </option> 
                                    <option value="4" > M. Materiales </option> 
                                    <option value="5" > POUP </option> 
                                    <option value="6" > Auditor Calidad </option> 
                                    <option value="7" > Auditor Proceso </option> 
                                </select >  
                            </div>
                        </div> 
                        <div id ="tblDatosLinea" > 
                            
                        </div>
                    </div>   
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


