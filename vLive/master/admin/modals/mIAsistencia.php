<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL

$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));
$linea = $_SESSION["linea"]; 

if (isset($_SESSION['usuario'])){ 
    $listaLinea = listar_Lineas_Producto($_SESSION['productoU']); 
} else {
    $listaLinea = listar_Lineas_Producto($_SESSION['productoL']);
} 

for ($i = 0; $i < count($listaLinea); $i++) { 
    $lblLinea[$i] = $listaLinea[$i][0]; 
    if (isset($listaLinea[$i][1])){ 
        $lblLinea[$i] = $listaLinea[$i][0].' - '.$listaLinea[$i][1]; 
    } 
} 

?>

<script type="text/javascript" > 
    jQuery().ready( 
        function() { 
            changeDatos();
        } 
    ); 

    function operacion(id){ 
        var selectedOption = $('#'+id).find('option:selected');
        var selectedLabel = selectedOption.text(); 
        var pnl = "pnl"+id; 
        if (selectedLabel.trim() == 'OTRA'){ 
            document.getElementById(pnl).className = "si"; 
        } else {
            document.getElementById(pnl).className = "no"; 
        }
    } 
    
    function changeDatos() { 
        var linea = document.getElementById('cmbLineaLA').value;
        var turno = document.getElementById('turno').value; 
        var fechaR = document.getElementById('fRegistroC').value; 
        
        //CONSULTA DE TURNO 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/cListaAsistencia.php", 
            data: {linea: linea, turno: turno, fechaR:fechaR }, 
            success: function(datos) { 
                $('#tblDatosLinea').html(datos); 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
    } 

</script>

<form id="fILAsistencia" method="post" >
    <div class="modal" tabindex="-1" role="dialog" id="mILAsistencia" >
        <div class="modal-dialog modal-lg" style="width: 120vh" >
            <div class="modal-content" style="width: 100%" >
                <div class="modal-header" style="background: #02538B;" >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > Lista de asistencia </h4>
                </div>
                <div class="modal-body" style="max-height: calc(100vh - 190px); overflow-y: auto;" >                     
                    <div > 
                        <div class="form-row">                           
                            <div class="col-md-3" >                                  
                                <div class="col-md-10 contenidoCentrado" style="margin-top: 7%; " > 
                                    <label class="my-2 mr-2" >Linea: </label> 
                                    <select class="form-control" id="cmbLineaLA" name="cmbLineaLA" style="height: 33px; width: 125%; margin-left: -5%;" onchange="changeDatos()" > 
                                        <?php for ($i = 0; $i < count($listaLinea); $i++) { ?> 
                                            <option value="<?php echo $listaLinea[$i][0]; ?>" <?php if ($_SESSION['linea'] == $listaLinea[$i][0] ){ ?> selected <?php } ?>  > <?php echo $lblLinea[$i]; ?> </option> 
                                        <?php } ?> 
                                    </select> 
                                </div> 
                            </div>                              
                            <div class="col-md-6 " style="padding-left: 5%; " >
                                <label class="my-6 mr-6" style="margin-left: 33%; " >REGISTRO</label>
                                <hr style="height: 1px; background-color: #B4B4B4; margin-top: -1.4%;" >
                                <div style="margin-top: -3.5%;" > 
                                    <div class="col-md-4 contenidoCentrado" > 
                                        <label class="my-1 mr-2" >Fecha: </label> 
                                        <input class="form-control" id="fRegistroC" name="fRegistroC" value="<?php echo $fInicio; ?>" type="text" />
                                    </div> 
                                    <div class="col-md-5 contenidoCentrado" > 
                                        <label class="my-1 mr-2" > Turno: </label> 
                                        <select class="form-control" id="turno" name="turno" style="height: 33px;" > 
                                            <option value="1" > Primer Turno </option> 
                                            <option value="2" > Segundo Turno </option> 
                                            <option value="3" > Tercer Turno </option> 
                                        </select> 
                                    </div> 
                                    <div class="col-md-3" > 
                                        <label class="my-1 mr-2" >Horas: </label> 
                                        <input class="form-control" id="horas" name="horas" type="text" required /> 
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                        <div id="alertILAsistencia"> </div> 
                        <hr width="100%" > 
                        <div id ="tblDatosLinea" > 
                        </div> 
                        <br> 
                    </div>  
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


