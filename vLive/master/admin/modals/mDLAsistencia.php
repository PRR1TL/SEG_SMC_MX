<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<form id="fDLAsistencia" method="post" > 
    <div class="modal" tabindex="-1" role="dialog" id="mDLAsistencia" > 
        <div class="modal-dialog modal-md" >
            <div class="modal-content" > 
                <div class="modal-header" style="background: #02538B;" > 
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > ELIMINAR ASISTENCIA </h4> 
                </div> 
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;" > 
                    <div id="alertLAD" ></div> 
                    <div class="contenidoCentrado" > 
                        <div class="form-row contenidoCentrado" > 
                            <div class="col-md-12 mb-1 contenidoCentrado" style="font-size: 23px;" > 
                                Deseas eliminar el registro de <strong> 
                                <input class="contenidoCentrado" id="tAsisLAD" name="tAsisLAD" style="width: 25%; border:none; color: #ac2925;" ></strong> de 
                                <strong> <input class="contenidoCentrado" id="nameLAD" name="nameLAD" style="border:none; color: #ac2925; width: 100%; " > </strong> 
                            </div> 
                            <div class="no" > 
                                <input id="idLAD" name="idLAD" >  
                                <input id="lineaLAD" name="lineaLAD" > 
                                <input id="fechaLAD" name="fechaLAD" > 
                                <input id="puestoLAD" name="puestoLAD" > 
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer" > 
                    <button type="button" class="btn btn-primary" data-dismiss="modal" > CANCELAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw" > </i> </button> 
                    <button type="submit" class="btn btn-danger" > CONFIRMAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


