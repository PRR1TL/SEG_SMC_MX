<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<form id="fLogOut" method="post">
    <div class="modal" tabindex="-1" role="dialog" id="mLogOut">
        <div class="modal-dialog modal-sm ">
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B;" >
                    <h4 class="modal-title text-center all-tittles col-lg-12" style="color: #ffffff" >CERRAR SESION</h4>
                    <button type="button" data-dismiss="modal" style="background-color: transparent; border: 0; margin-top: -3.9vh;" > <img src="./../../imagenes/cl.png" > </button> 
                </div>
                <div class="modal-body">
                    <div id="datos_ajax"></div> 
                    <div class="row">
                        <div class="col-sm-12" align="center">
                            <H2>¿Estás seguro?</H2>    
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Quieres salir del sistema y cerrar la sesión actual</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" >Aceptar</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal" >Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</form>







