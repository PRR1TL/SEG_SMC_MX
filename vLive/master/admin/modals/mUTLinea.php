<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<form id="fUTLinea" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mUTLinea" >
        <div class="modal-dialog modal-lg" style="width: 30vw" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B;" >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > REGISTRAR TARGETS </h4>
                </div> 
                <div class="modal-body" style=" max-height: calc(100vh - 180px); overflow-y: auto;" > 
                    <div id="alertITargetU" ></div> 
                    <div> 
                        <div class="form-row" > 
                            <div align="center" class="col-md-4" > 
                                <label class="my-1 mr-2" >LINEA </label> 
                                <input class="form-control" id="lineaU" name="lineaU" style="width: 90%; height: 33px;" > 
                            </div>              
                            <div align="center" class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" >FECHA </label>
                                <input id="fStandard" name="fStandard" class="btn btn-secondary btn-sm" value="<?php echo $fechaP ?>" style="border-color: #888; width: 90%; " />
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%; margin-top: 12px; " > 
                    <div class="panel" id="pnlOplLineaU" name="pnlOplLineaU" style="margin-top: -10px" > 
                        <div class="panel panel-info" style="width: 100%;" > 
                            <div class="panel-heading" ><strong> ACTUALIZACION DE TARGETS <strong></div> 
                            <div class="panel-body" > 
                                <div class="form-row" > 
                                    <div class="col-md-5" > 
                                        <label class="my-1 mr-2" >&nbsp;</label> 
                                        </div> 
                                    <div class="col-md-2 " > 
                                        <label class="my-1 mr-2" >META</label> 
                                    </div> 
                                    <div class="col-md-4 mb-3" > 
                                        <label >DESCRIPCIÓN</label> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" > 
                                        <label class="my-1 mr-2" >OEE</label> 
                                    </div> 
                                    <div class="col-md-2 " > 
                                        <input class="form-control" id="tOEEU" name="tOEEU" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3" > 
                                        <p>%</p> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" > 
                                        <label class="my-1 mr-2" >Tecnicas</label> 
                                    </div> 
                                    <div class="col-md-2 " > 
                                        <input class="form-control" id="tTecU" name="tTecU" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3" > 
                                        <p>min</p> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" > 
                                        <label class="my-1 mr-2" >Organizacionales</label> 
                                    </div> 
                                    <div class="col-md-2 " id="lineaU" > 
                                        <input class="form-control" id="tOrgU" name="tOrgU" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3" > 
                                        <p>min</p> 
                                    </div> 
                                </div>
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" > 
                                        <label class="my-1 mr-2" >Cambio de modelo</label> 
                                    </div> 
                                    <div class="col-md-2 " > 
                                        <input class="form-control" id="tCamU" name="tCamU" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <p>min</p> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" > 
                                        <label class="my-1 mr-2" >NO Calidad</label> 
                                    </div> 
                                    <div class="col-md-2 " id="lineaU" > 
                                        <input class="form-control" id="tCaliU" name="tCaliU" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <p>pzas</p> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" > 
                                        <label class="my-1 mr-2" >Scrap</label> 
                                    </div> 
                                    <div class="col-md-2 " id="lineaU" > 
                                        <input class="form-control" id="tScrapU" name="tScrapU" minlength="1" maxlength="4" style="width: 100%" placeholder="8.5" type="text" /> 
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <p>$</p> 
                                    </div> 
                                </div>
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" > 
                                        <label class="my-1 mr-2" >Productividad</label> 
                                    </div> 
                                    <div class="col-md-2 " id="lineaU" > 
                                        <input class="form-control" id="tProdU" name="tProdU" minlength="1" maxlength="4" style="width: 100%" placeholder="8.5" type="text" /> 
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <p></p> 
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > ACTUALIZAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


