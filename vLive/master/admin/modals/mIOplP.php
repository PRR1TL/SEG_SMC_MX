<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

$cLineas = listarLineas();

?>
<form id="fIOPLP" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mIOPLP">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > NUEVA OPL </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertOplP"></div> 
                    <div> 
                        <div class="form-row"> 
                            <input id="tipOpl" name="tipOpl" value="1" class="no" >
                            <div class="col-md-6 mb-1">
                                <label for="validationCustom01">Proyecto</label> 
                                <select class="form-control" id="proyecto" name="proyecto" style="height: 33px" > 
                                    <option value="1">L. Proyectos</option>
                                </select> 
                            </div> 
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Detectado por:</label> 
                                <input class="form-control"  id="detecta" name="detecta" maxlength="15" style="width: 80%"  value="<?php echo $_SESSION["nickName"] ?>" type="text"  />
                            </div>                             
                            
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Fecha Actual: </label>
                                <input class="form-control" id="fRegistro" name="fRegistro" style="width: 55%" value="<?php echo $fInicio ?>" type="text" readonly="" />
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%">
                    <br>
                    <div class="panel " id="pnlOplProject" name="pnlOplProject" style="margin-top: -3vh">                         
                        <!--DATOS DE LA DESVIACION--> 
                        <div class="panel panel-info" style="width: 100%;">
                            <div class="panel-heading" ><strong> Acci&oacute;n <strong></div>
                            <div class="panel-body" >
                                <div class="form-row">                                    
                                    <div class="col-md-5 mb-1">
                                        <label for="validationCustom01">Acci&oacute;n requerida</label> 
                                        <input type="text" class="form-control" id="accionP" name="accionP" placeholder="Accion requerida" value="" >                                      
                                    </div> 
                                    <div class="col-md-2 mb-2"> 
                                        <label for="validationCustom02"> &Aacute;rea </label> 
                                        <input type="text" class="form-control" id="areaP" name="areaP" placeholder="&Aacute;rea" > 
                                    </div> 
                                    <div class="col-md-2 mb-2"> 
                                        <label for="validationCustomUsername">Responsable</label> 
                                        <input type="text" class="form-control" id="responsableP" name ="responsableP" placeholder="Responsable" aria-describedby="inputGroupPrepend" >
                                    </div> 
                                    <div class="col-md-2 mb-2">  
                                        <label for="validationCustomUsername">F. Compromiso</label> 
                                        <input type="text" class="form-control" id="fCompromisoP" name="fCompromisoP" placeholder="F. COmpromiso" value="<?php echo $fProp; ?>" aria-describedby="inputGroupPrepend" >
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


