<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<form id="fUOplL" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mUOplL">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > MODIFICAR OPL </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertUOplLU"></div> 
                    <div> 
                        <input id="idOplLU" name="idOplLU" class="no" >
                        <div class="form-row"> 
                            <div class="col-md-3"> 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Tipo</label>
                                <select class="form-control" id="tipOplLU" name="tipOplLU" style="width: 90%; height: 33px;" >
                                    <option value="3" selected >Linea</option> 
                                </select> 
                            </div>                             
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Detectado por:</label> 
                                <input class="form-control"  id="detectaLU" name="detectaLU" maxlength="15" style="width: 80%" type="text" readonly />
                            </div>                             
                            <div class="col-md-3 " id="lineaU" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Linea</label>
                                <input class="form-control" id="lineaLU" name="lineaLU" maxlength="15" style="width: 80%" type="text" readonly />
                            </div>                             
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Fecha Actual: </label>
                                <input class="form-control" id="fRegistroLU" name="fRegistroLU" style="width: 55%" type="text" readonly />
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%">
                    <br>
                    <div class="panel" id="pnlOplLineaU" name="pnlOplLineaU"  style="margin-top: -3vh"> 
                        <!--DATOS DE LA DESVIACION--> 
                        <div id="alertOplU" name="alertOplU"></div>
                        <div class="panel panel-info" style="width: 100%;">
                            <div class="panel-heading" ><strong> Datos desviacion<strong></div>
                            <div class="panel-body" >
                                <div class="form-row">
                                    <div class="col-md-2 mb-1">
                                        <label for="validationCustom01">Estacion</label> 
                                        <input type="text" class="form-control" id="operacionLU" name="operacionLU" placeholder="1210" readonly >                                      
                                    </div> 
                                    <div class="col-md-6 mb-3"> 
                                        <label for="validationCustom02"> Descripci&oacute;n </label> 
                                        <input type="text" class="form-control" id="hallazgoLU" name="hallazgoLU" placeholder="Descripci&oacute;n" readonly > 
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <label for="validationCustomUsername">Causa</label> 
                                        <input type="text" class="form-control" id="causaLU" name="causaLU" placeholder="Causa" readonly >
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                        <!--DATOS DE LA ACCION--> 
                        <div class="panel panel-info" style="width: 100%;"> 
                            <div class="panel-heading" >Datos acción</div> 
                            <div class="panel-body" > 
                                <div class="form-row"> 
                                    <div class="col-md-6 mb-3"> 
                                        <label for="validationCustom01">Descripci&oacute;n</label> 
                                        <input type="text" class="form-control" id="accionLU" name="accionLU" placeholder="Descripción" readonly >                                      
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="validationCustom02">Responsable</label>
                                        <input type="text" class="form-control" id="responsableLU" name="responsableLU" placeholder="Responsable" readonly >                                      
                                    </div>
                                    <div class="col-md-3 mb-3">
                                        <label for="validationCustomUsername">F.Compromiso</label>
                                        <input type="text" class="form-control" id="fCompromisoLU" name="fCompromisoLU" placeholder="F.Compromiso" readonly >
                                    </div>
                                </div>             
                            </div>
                        </div> 
                        
                        <div class="form-row">                             
                            <div class="col-md-6"> 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Estado</label>
                                <select class="form-control" id="estadoLU" name="estadoLU" style="width: 90%; height: 33px;" >
                                    <option value = "1"> Accion definida</option>
                                    <option value = "2"> Accion en proceso</option>
                                    <option value = "3"> Accion ejecutada </option>
                                    <option value = "4"> Accion cerrada </option>  
                                </select> 
                            </div> 
                        </div> 
                        
                    </div>                     
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > ACTUALIZAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


