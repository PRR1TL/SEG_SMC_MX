<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<form id="fULAsistencia" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mULAsistencia">
        <div class="modal-dialog " style="width: 170vh" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > MODIFICAR </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;" >
                    <div id="alertULAsistencia" ></div> 
                    <div> 
                        <input id="uLAId" name="uLAId" class="form-control no" > 
                        <div class="form-row" > 
                            <div class="col-md-2" > 
                                <label class="my-1 mr-2" >Linea</label> 
                                <input class="form-control" id="uLALinea" name="uLALinea" readonly > 
                            </div> 
                            <div class="col-md-7 contenidoCentrado" > 
                                <label class="my-1 mr-2" >Nombre</label> 
                                <input class="form-control" id="uLANombre" name="uLANombre" type="text" readonly > 
                            </div> 
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" >Fecha</label> 
                                <input class="form-control" id="uLAFecha" name="uLAFecha" type="text" readonly > 
                            </div> 
                        </div> 
                    </div> 
                    <div class="form-row" style="margin-top: 4.5%" > 
                        <div class="col-md-5 mb-1"> 
                            <label >Operacion</label> 
                            <input class="form-control" id="uLOperacion" name="uLOperacion" readonly >                                 
                        </div> 
                        <div class="col-md-5 mb-3"> 
                            <label > Asistencia </label> 
                            <select class="form-control" id="tAsistencia" name="tAsistencia" style="height: 33px; " > 
                                <option value="A" > Asistencia </option> 
                                <option value="B" > Baja </option> 
                                <option value="C" > Capacitacion </option> 
                                <option value="F" > Falta </option> 
                                <option value="I" > Incapacidad </option> 
                                <option value="P" > Permiso </option> 
                                <option value="S" > Suspencion </option> 
                                <option value="V" > Vacaciones </option> 
                                <option value="R" > Retardo </option> 
                            </select> 
                        </div> 
                        <div class="col-md-2 mb-3"> 
                            <label >Horas</label> 
                            <input type="text" class="form-control" id="uLAHoras" name="uLAHoras" >
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > ACTUALIZAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 