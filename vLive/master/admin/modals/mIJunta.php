<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

$cLineas = listarLineas();

?>

<script type="text/javascript">
    $(document).ready(function() { 
       cLista(); 
    });

    function cLista(){ 
        //CACHA EL VALOR DEL SELECT 
        var parametros = $(this).serialize(); 
        
        $.ajax({ 
            url: '../../db/admin/cListaJunta.php',
            type: "POST",
            data: { linea: linea }, 
            success: function(datos){ 
                $('#pnlJunta').html(datos); 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
    } 
</script>

<form id="fIJunta" method="post" > 
    <div class="modal" tabindex="-1" role="dialog" id="mIJunta" > 
        <div class="modal-dialog modal-lg" style="width: 80vh" > 
            <div class="modal-content" style="width: 110%" > 
                <div class="modal-header" style="background: #02538B;" > 
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > Lista de asistencia </h4>
                </div> 
                <div class="modal-body" style="max-height: calc(100vh - 190px); overflow-y: auto;" > 
                    <div id="alertIJunta" > </div> 
                    <div > 
                        <div class="form-row" > 
                            <div class="col-md-4" > 
                                <div class="col-md-12 contenidoCentrado" > 
                                    <label class="my-1 mr-2" >Linea: </label> 
                                    <select class="form-control" id="cmbLineaJ" name="cmbLineaJ" style="height: 33px" onchange="cLista()" > 
                                        <?php for ($i = 0; $i < count($cLineas); $i++) { ?> 
                                        <option <?php if ($cLineas[$i][0] == $linea ){ ?> selected <?php } ?> value="<?php echo $cLineas[$i][0]; ?>" > <?php echo $cLineas[$i][0]; ?> </option> 
                                        <?php } ?> 
                                    </select> 
                                </div> 
                            </div>  
                            <div class="col-md-2" > 
                            </div>    
                            <div class="col-md-6" > 
                                <div class="col-md-6 contenidoCentrado" >                                  
                                    <label class="my-1 mr-2" >Fecha: </label>
                                    <input class="form-control" id="fRegistro" name="fRegistro" value="<?php echo $fInicio ?>" type="text" />
                                </div>  
                            </div> 
                        </div> 
                        <hr width="100%" > 
                        <div id="pnlJunta" name="pnlJunta"  >
                        </div>  
                    </div> 
                </div> 
                <div class="modal-footer" > 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


