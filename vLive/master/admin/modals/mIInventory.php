<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

if (isset($_SESSION['usuario'])){ 
    $listaLinea = listar_Lineas_Producto($_SESSION['productoU']); 
} else {
    $listaLinea = listar_Lineas_Producto($_SESSION['productoL']);
} 

for ($i = 0; $i < count($listaLinea); $i++) { 
    $lblLinea[$i] = $listaLinea[$i][0]; 
    if (isset($listaLinea[$i][1])){ 
        $lblLinea[$i] = $listaLinea[$i][0].' - '.$listaLinea[$i][1]; 
    } 
} 


?>
<form id="fIInventory" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mIInventory">
        <div class="modal-dialog modal-lg"  >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > Inventario </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="#alertIInventory"></div> 
                    <div> 
                        <div class="form-row">          
                            <div class="col-md-3 " id="linea" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Linea</label> 
                                <select class="form-control" id="linea" name="linea" style="height: 33px" > 
                                    <?php for ($i = 0; $i < count($listaLinea); $i++) { ?> 
                                    <option value="<?php echo $listaLinea[$i][0]; ?>" <?php if ($_SESSION['linea'] == $listaLinea[$i][0] ){ ?> selected <?php } ?>  > <?php echo $lblLinea[$i]; ?> </option> 
                                    <?php } ?> 
                                </select> 
                            </div>                             
                            <div class="col-md-2 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Fecha Actual: </label>
                                <input class="form-control" id="fRegistro" name="fRegistro" value="<?php echo $fInicio ?>" type="text" readonly />
                            </div> 
                            <div class="col-md-2 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref"> Maximo: </label> 
                                <input class="form-control"  id="vmax" name="vmax" maxlength="15" value="50" type="text"  />
                            </div>
                            <div class="col-md-2 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref"> Minimo: </label> 
                                <input class="form-control"  id="vmin" name="vmin" maxlength="15" value="0" type="text"  />
                            </div>
                            <div class="col-md-2 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref"> Real: </label> 
                                <input class="form-control"  id="vreal" name="vreal" maxlength="15" value="0" type="text"  />
                            </div>
                        </div> 
                    </div>   
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


