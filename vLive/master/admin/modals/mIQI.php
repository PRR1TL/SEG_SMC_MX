<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

if (isset($_SESSION['usuario'])){ 
    $listaLinea = listar_Lineas_Producto($_SESSION['productoU']); 
} else {
    $listaLinea = listar_Lineas_Producto($_SESSION['productoL']);
} 

for ($i = 0; $i < count($listaLinea); $i++) { 
    $lblLinea[$i] = $listaLinea[$i][0]; 
    if (isset($listaLinea[$i][1])){ 
        $lblLinea[$i] = $listaLinea[$i][0].' - '.$listaLinea[$i][1]; 
    } 
} 

?>
<form id="fIQI" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mIQI">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > NUEVA OPL </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertIQI"></div> 
                    <div> 
                        <div class="form-row"> 
                            <div class="col-md-3"> 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Tipo</label>
                                <select class="form-control" id="tipo" name="tipo" style="width: 90%; height: 33px;" >                                    
                                    <option value="1" > Fallas internas </option> 
                                    <option value="2" > 0-km </option>                                     
                                </select> 
                            </div>    
                            <div class="col-md-3"> 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Linea</label>
                                <select class="form-control" id="linea" name="linea" style="width: 90%; height: 33px;" >
                                    <?php for ($i = 0; $i < count($listaLinea); $i++) { ?> 
                                        <option value="<?php echo $listaLinea[$i][0]; ?>" <?php if ($_SESSION['linea'] == $listaLinea[$i][0] ){ ?> selected <?php } ?>  > <?php echo $lblLinea[$i]; ?> </option> 
                                    <?php } ?> 
                                </select> 
                            </div>                             
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Detectado por:</label> 
                                <input class="form-control"  id="detecta" name="detecta" maxlength="15" style="width: 80%" value="<?php echo $_SESSION["nickName"] ?>" type="text" onlyread  />
                            </div>                       
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Fecha Actual: </label>
                                <input class="form-control" id="fRegistro" name="fRegistro" style="width: 55%" value="<?php echo $fInicio ?>" type="text" />
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%">
                    <br>
                    <div class="panel" id="pnlOplLinea" name="pnlOplLinea"  style="margin-top: -3vh">                         
                        <div class="panel panel-info" style="width: 100%;">
                            <div class="panel-heading" ><strong> Datos desviacion<strong></div>
                            <div class="panel-body" >
                                <div class="form-row">
                                    <div class="col-md-2 " id="linea" > 
                                        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Cliente</label>
                                        <input class="form-control" id="cliente" name="cliente" maxlength="15" style="width: 100%" placeholder="Cliente" type="text"  />
                                    </div> 
                                    <div class="col-md-2 " id="linea" > 
                                        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">No. Parte</label>
                                        <input class="form-control" id="noParte" name="noParte" minlength="10" maxlength="10" style="width: 100%" placeholder="No. Parte" type="text"  />
                                    </div>
                                    <div class="col-md-4 mb-3"> 
                                        <label for="validationCustomUsername">Descripcion</label> 
                                        <input type="text" class="form-control" id="descripcion" name="descripcion" placeholder="Descripcion" aria-describedby="inputGroupPrepend" >
                                    </div> 
                                    <div class="col-md-4 " id="linea" > 
                                        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Nota</label>
                                        <input class="form-control" id="nota" name="nota" maxlength="15" style="width: 100%" placeholder="Nota" type="text"  />
                                    </div>
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


