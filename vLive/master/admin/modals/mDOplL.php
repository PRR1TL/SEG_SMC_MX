<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<form id="fDOplL" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mDOplL" >
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > ELIMINAR OPL </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;" >
                    <div id="alerDtOplL"></div> 
                    <div> 
                        <input id="idOplLD" name="idOplLD" class="no" > 
                        <div class="form-row" > 
                            <div class="col-md-3" > 
                                <label class="my-1 mr-2" >Tipo</label> 
                                <select class="form-control" id="tipOplLD" name="tipOplLD" style="width: 90%; height: 33px;" > 
                                    <option value="3" selected >Linea</option> 
                                </select> 
                            </div> 
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" >Detectado por:</label> 
                                <input class="form-control"  id="detectaLD" name="detectaLD" maxlength="15" style="width: 80%" type="text" readonly />
                            </div> 
                            <div class="col-md-3 " id="lineaU" > 
                                <label class="my-1 mr-2" >Linea</label> 
                                <input class="form-control" id="lineaLD" name="lineaLD" maxlength="15" style="width: 80%" type="text" readonly /> 
                            </div> 
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" >Fecha Actual: </label> 
                                <input class="form-control" id="fRegistroLD" name="fRegistroLD" style="width: 55%" type="text" readonly /> 
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%"> 
                    <br> 
                    <div class="panel" id="pnlOplLineaU" name="pnlOplLineaU" style="margin-top: -3vh" > 
                        <!--DATOS DE LA DESVIACION--> 
                        <div id="alertOplU" name="alertOplU" ></div>
                        <div class="panel panel-info" style="width: 100%;" >
                            <div class="panel-heading" ><strong> Datos desviacion<strong></div>
                            <div class="panel-body" >
                                <div class="form-row" >
                                    <div class="col-md-2 mb-1" >
                                        <label >Estacion</label> 
                                        <input type="text" class="form-control" id="operacionLD" name="operacionLD" placeholder="1210" readonly >                                      
                                    </div> 
                                    <div class="col-md-6 mb-3" > 
                                        <label > Descripci&oacute;n </label> 
                                        <input type="text" class="form-control" id="hallazgoLD" name="hallazgoLD" placeholder="Descripci&oacute;n" readonly > 
                                    </div> 
                                    <div class="col-md-4 mb-3" > 
                                        <label >Causa</label> 
                                        <input type="text" class="form-control" id="causaLD" name="causaLD" placeholder="Causa" readonly >
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                        <!--DATOS DE LA ACCION--> 
                        <div class="panel panel-info" style="width: 100%;" > 
                            <div class="panel-heading" >Datos acción</div> 
                            <div class="panel-body" > 
                                <div class="form-row" > 
                                    <div class="col-md-6 mb-3" > 
                                        <label >Descripci&oacute;n</label> 
                                        <input type="text" class="form-control" id="accionLD" name="accionLD" placeholder="Descripción" readonly >                                      
                                    </div> 
                                    <div class="col-md-3 mb-3" > 
                                        <label >Responsable</label>
                                        <input type="text" class="form-control" id="responsableLD" name="responsableLD" placeholder="Responsable" readonly >                                      
                                    </div> 
                                    <div class="col-md-3 mb-3" > 
                                        <label >F.Compromiso</label> 
                                        <input type="text" class="form-control" id="fCompromisoLD" name="fCompromisoLD" placeholder="F.Compromiso" readonly >
                                    </div>
                                </div> 
                            </div> 
                        </div> 
                        
                        <div class="form-row" > 
                            <div class="col-md-6" > 
                                <label class="my-1 mr-2" >Estado</label> 
                                <select class="form-control" id="estadoLD" name="estadoLD" style="width: 90%; height: 33px;" > 
                                    <option value = "1" > Accion definida</option> 
                                    <option value = "2" > Accion en proceso</option> 
                                    <option value = "3" > Accion ejecutada </option> 
                                    <option value = "4" > Accion cerrada </option> 
                                </select> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer" > 
                    <button type="button" class="btn btn-primary" data-dismiss="modal" > CANCELAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw" > </i> </button> 
                    <button type="submit" class="btn btn-danger" > ELIMINAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw" > </i> </button>
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


