<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL

$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

if (isset($_SESSION['usuario'])){ 
    $listaLinea = listar_Lineas_Producto($_SESSION['productoU']); 
} else {
    $listaLinea = listar_Lineas_Producto($_SESSION['productoL']);
} 

for ($i = 0; $i < count($listaLinea); $i++) { 
    $lblLinea[$i] = $listaLinea[$i][0]; 
    if (isset($listaLinea[$i][1])){ 
        $lblLinea[$i] = $listaLinea[$i][0].' - '.$listaLinea[$i][1]; 
    } 
} 

$fechaP = date("M, Y"); 

?>
<form id="fITarget" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mITarget" >
        <div class="modal-dialog modal-lg" style="width: 30vw" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B;" >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > REGISTRAR TARGETS </h4>
                </div> 
                <div class="modal-body" style=" max-height: calc(100vh - 180px); overflow-y: auto;" > 
                    <div id="alertITarget" ></div> 
                    <div> 
                        <div class="form-row" > 
                            <div align="center" class="col-md-4" > 
                                <label class="my-1 mr-2" >LINEA </label> 
                                <select class="form-control" id="linea" name="linea" style="width: 90%; height: 33px;" > 
                                    <?php for ($i = 0; $i < count($listaLinea); $i++) { ?> 
                                    <option value="<?php echo $listaLinea[$i][0]; ?>" <?php if ($_SESSION['linea'] == $listaLinea[$i][0] ){ ?> selected <?php } ?>  > <?php echo $lblLinea[$i]; ?> </option> 
                                    <?php } ?> 
                                </select> 
                            </div>              
                            <div align="center" class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" >MES </label>
                                <input id="mPickerM" name="mPickerM" class="btn btn-secondary btn-sm" value="<?php echo $fechaP ?>" style="border-color: #888; width: 90%; " />
                            </div>  
                            <div align="left" class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" > &nbsp; </label> <br> 
                                <input type="checkbox" id="dH" name="dH" > 
                                <label for="dH" >Días habiles</label> 
                            </div>  
                        </div> 
                    </div> 
                    <hr style="width: 100%; margin-top: 12px; " > 
                    <div class="panel" id="pnlOplLinea" name="pnlOplLinea" style="margin-top: -10px" > 
                        <div class="panel panel-info" style="width: 100%;" > 
                            <div class="panel-heading" ><strong> Targets <strong></div> 
                            <div class="panel-body" > 
                                <div class="form-row" > 
                                    <div class="col-md-5" id="linea" > 
                                        <label class="my-1 mr-2" >&nbsp;</label> 
                                        </div> 
                                    <div class="col-md-2 " id="linea" > 
                                        <label class="my-1 mr-2" >META</label> 
                                    </div> 
                                    <div class="col-md-4 mb-3" > 
                                        <label >DESCRIPCIÓN</label> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" id="linea" > 
                                        <label class="my-1 mr-2" >OEE</label> 
                                    </div> 
                                    <div class="col-md-2 " id="linea" > 
                                        <input class="form-control" id="tOEE" name="tOEE" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3" > 
                                        <p>%</p> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" id="linea" > 
                                        <label class="my-1 mr-2" >Tecnicas</label> 
                                    </div> 
                                    <div class="col-md-2 " id="linea" > 
                                        <input class="form-control" id="tTec" name="tTec" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3" > 
                                        <p>min</p> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" id="linea" > 
                                        <label class="my-1 mr-2" >Organizacionales</label> 
                                    </div> 
                                    <div class="col-md-2 " id="linea" > 
                                        <input class="form-control" id="tOrg" name="tOrg" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3" > 
                                        <p>min</p> 
                                    </div> 
                                </div>
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" id="linea" > 
                                        <label class="my-1 mr-2" >Cambio de modelo</label> 
                                    </div> 
                                    <div class="col-md-2 " id="linea" > 
                                        <input class="form-control" id="tCam" name="tCam" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <p>min</p> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" id="linea" > 
                                        <label class="my-1 mr-2" >NO Calidad</label> 
                                    </div> 
                                    <div class="col-md-2 " id="linea" > 
                                        <input class="form-control" id="tCali" name="tCali" minlength="1" maxlength="3" style="width: 100%" placeholder="80" type="text" />
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <p>pzas</p> 
                                    </div> 
                                </div> 
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" id="linea" > 
                                        <label class="my-1 mr-2" >Scrap</label> 
                                    </div> 
                                    <div class="col-md-2 " id="linea" > 
                                        <input class="form-control" id="tScrap" name="tScrap" minlength="1" maxlength="4" style="width: 100%" placeholder="8.5" type="text" /> 
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <p>$</p> 
                                    </div> 
                                </div>
                                <div class="form-row" > 
                                    <div align="right" class="col-md-5" id="linea" > 
                                        <label class="my-1 mr-2" >Productividad</label> 
                                    </div> 
                                    <div class="col-md-2 " id="linea" > 
                                        <input class="form-control" id="tProd" name="tProd" minlength="1" maxlength="4" style="width: 100%" placeholder="8.5" type="text" /> 
                                    </div> 
                                    <div class="col-md-4 mb-3"> 
                                        <p></p> 
                                    </div> 
                                </div>
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


