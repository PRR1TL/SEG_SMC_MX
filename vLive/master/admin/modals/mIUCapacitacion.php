<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

$cLineas = listarLineas();

?>

<script type="text/javascript" > 
    var a = 0; 
    var cST = 0; 
    
    function keyShipTo(e,id){ 
        (e.keyCode) ? ev = e.keyCode: ev = e.which; 
        //console.log('K: '+ev+', id: '+id);         
        var elemento = id; 
        // Si la tecla pulsada es enter (codigo ascii 13) 
        if( ev == 13 || ev == 9 ){ 
            // Si la variable id contiene "submit" enviamos el formulario 
            if(id=="submit"){ 
                document.forms[0].submit(); 
            } else { 
                // nos posicionamos en el siguiente input 
                document.getElementById(id).focus(); 
            } 
        } else { 
            var valE = document.getElementById(elemento).value; 
            var datA = []; 
            if (valE.length > 3){ 
                $.ajax({ 
                    url: '../../db/admin/cUsuarioType.php',
                    type: "POST",
                    data: { nombre: valE },
                    cache: false,
                    success: function(datos){
                        $("#c"+elemento).empty(); 
                        var respuesta = datos.trim();
                        var a1 = respuesta.split(",");
                        for (var i in a1) {
                            datA[i] = a1[i];
                            $("#c"+elemento).append("<option value='"+datA[i]+"' >"+datA[i]+"</option>");
                        }
                    } 
                }).fail( function( jqXHR, textStatus, errorThrown ) {
                    if (jqXHR.status === 0) {
                        alert('Not connect: Verify Network.');
                    } else if (jqXHR.status == 404) {
                        alert('Requested page not found [404]');
                    } else if (jqXHR.status == 500) {
                        alert('Internal Server Error [500].');
                    } else if (textStatus === 'parsererror') {
                        alert('Requested JSON parse failed.');
                    } else if (textStatus === 'timeout') {
                        alert('Time out error.');
                    } else if (textStatus === 'abort') {
                        alert('Ajax request aborted.');
                    } else {
                        alert('Uncaught Error: ' + jqXHR.responseText);
                    }
                }); 
            } else {
                $("#c"+elemento).append("<option value='-' > - </option>");
            }
        }
    } 

    function changeDatos() { 
        var linea = document.getElementById('cmbLineaLA').value; 
        var turno = document.getElementById('turnoC').value; 
        var fecha = document.getElementById('fRegistroC').value; 
        var datA = [];
        var c = 0;
        
        //CONSULTA DE TURNO 
        $.ajax({ 
            type: "POST", 
            url: "../../db/admin/cListaAsistencia.php", 
            data: {linea: linea, turno: turno, fecha: fecha }, 
            success: function(datos) { 
                //LIMPIEZA DE COMPONENTES
                for (x = 1; x < 40; x++ ){ 
                    $('#iNombre'+x).val("");
                    $('#iATipo'+x).val(0);
                    $('#iUTipo'+x).val(1);
                } 
                
                //LLENA COMPONENTES DE LA BASE DE DATOS
                if (datos != 0){ 
                    var respuesta = datos.trim(); 
                    var a1 = respuesta.split(","); 
                    
                    //ASIGNACION DE VALORES A EL ARRAY
                    for (var i in a1) {                         
                        datA[i] = a1[i]; 
                        c++; 
                    } 
                    
                    var c1 = 0; 
                    var c2 = 0; 
                    var c3 = 1; 
                    
                    //RECORRIDO E IMPRESION DE VALORES EN LOS COMPONENTES
                    for (var j = 0; j < c; j++ ) { 
                        c1 = j+1; 
                        c2 = j+2;                      
                        if (j == 0 ){ 
                            $("#iNombre"+c1).val(datA[j]); 
                            $("#iATipo"+c1).val(datA[c1]); 
                            $('#iUTipo'+c1).val(datA[c2]); 
                        } else { 
                            //NUMEROS IMPARES 
                            if (j % 3 == 0 && j > 1 ) { 
                                c3++;
                                $("#iNombre"+c3).val(datA[j]); 
                                $("#iATipo"+c3).val(datA[c1]); 
                                $('#iUTipo'+c3).val(datA[c2]);  
                            } 
                        } 
                    } 
                } 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
        //event.preventDefault(); 
    } 

</script>

<form id="fILAsistencia" method="post" > 
    <div class="modal fade" tabindex="-1" role="dialog" id="mINewPersonal" > 
        <div class="modal-dialog modal-lg" style="width: 180vh" > 
            <div class="modal-content" style="width: 110%" > 
                <div class="modal-header" style="background: #02538B;" > 
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > Nuevo Personal </h4> 
                </div> 
                <div class="modal-body" style="max-height: calc(100vh - 190px); overflow-y: auto;" > 
                    <div id="alertINewPersonal"> </div> 
                    <div > 
                        <div class="form-row" > 
                            <div class="col-md-9" > 
                                <div class="col-md-3 contenidoCentrado" > 
                                    <label class="my-1 mr-2" >Linea: </label> 
                                    <select class="form-control" id="lineaPeronal" name="cmbLineaLA" style="height: 33px" onchange="changeDatos()" > 
                                        <?php for ($i = 0; $i < count($cLineas); $i++) { ?> 
                                        <option <?php if ($cLineas[$i][0] == $linea ){ ?> selected <?php } ?> value="<?php echo $cLineas[$i][0]; ?>" > <?php echo $cLineas[$i][0]; ?> </option> 
                                        <?php } ?> 
                                    </select> 
                                </div> 
                                <div class="col-md-9 contenidoCentrado" > 
                                    <label class="my-1 mr-2" > &nbsp; </label> 
                                    <input class="form-control" id="nombreLinea" class="nombreLinea" > 
                                </div> 
                            </div>                             
                            <div class="col-md-2" > 
                                <label class="my-1 mr-2" > Fecha </label> 
                                <input type="text" class="form-control" id="fIniOpl" name="fIniOpl" value="<?php echo date("d/m/Y"); ?>" placeholder="F.Compromiso" >
                            </div> 
                        </div>
                        <div class="form-row" > 
                            <div class="col-md-9" > 
                                <div class="col-md-3 " > 
                                    <label class="my-1 mr-2" > Puesto: </label> 
                                    <select class="form-control" id="lineaPeronal" name="cmbLineaLA" style="height: 33px" onchange="changeDatos()" > 
                                        <option value="0" >Ajustador</option>
                                        <option value="1" >Operador</option>
                                        <option value="2" >Team Leader</option>
                                        <option value="3" >POUP</option> 
                                        <option value="4" >Auditor Calidad</option> 
                                        <option value="5" >Auditor Proceso</option> 
                                    </select> 
                                </div> 
                                <div class="col-md-4 " > 
                                    <label class="my-1 mr-2" > Nombre </label> 
                                    <input class="form-control" id="nombreLinea" class="nombreLinea" > 
                                </div> 
                                <div class="col-md-5 " > 
                                    <label class="my-1 mr-2" > Apellido  </label> 
                                    <input class="form-control" id="nombreLinea" class="nombreLinea" > 
                                </div> 
                            </div> 
                        </div>
                        <div class="form-row" > 
                            <div class="col-md-10" >
                                <label class="my-1 mr-2" > Evaluacion </label>                                 
                                <table style="width: 90%; text-align: center; " > 
                                    <thead style="text-align: center;" > 
                                        <tr style="border-bottom: black 1px solid; text-align: center;" > 
                                            <th style="border-right: 1px solid #ccc; text-align: center; " > </th> 
                                            <th style="border-right: 1px solid #ccc; text-align: center; " > Satisfactorio </th> 
                                            <th style="border-right: 1px solid #ccc; text-align: center; " > NO satisfactorio </th> 
                                            <th style="border-right: 1px solid #ccc; text-align: center; " > Satisfactorio con lentes </th> 
                                        </tr>
                                    </thead> 
                                    <tbody > 
                                        <tr style="border-bottom: 1px solid #ccc;" > 
                                            <th style="border-right: 1px solid #ccc; " > Auditiva </th> 
                                            <td style="border-right: 1px solid #ccc; " > <input type="radio" name="auditiva" value="0" checked > </td> 
                                            <td style="border-right: 1px solid #ccc; " > <input type="radio" name="auditiva" value="1"> </td> 
                                            <td style="border-right: 1px solid #ccc; " >  </td> 
                                        </tr> 
                                        <tr style="border-bottom: 1px solid #ccc;" > 
                                            <th style="border-right: 1px solid #ccc; " > Visual </th> 
                                            <td style="border-right: 1px solid #ccc; " > <input type="radio" name="visual" value="0" checked > </td> 
                                            <td style="border-right: 1px solid #ccc; " > <input type="radio" name="visual" value="1"> </td> 
                                            <td style="border-right: 1px solid #ccc; " > <input type="radio" name="visual" value="2"> </td> 
                                        </tr>
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                        <hr width="100%" > 
                        <div id ="tblDatosLinea" > 
                            
                        </div>
                    </div>   
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


