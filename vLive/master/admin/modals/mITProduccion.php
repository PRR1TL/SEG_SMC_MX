<?php

/* 
 * To change this license header, choose License Headers in Project Properties. 
 * To change this template file, choose Tools | Templates 
 * and open the template in the editor. 
 */ 

    //PARA FECHAS EN TIEMPO REAL 
    $fInicio = date('m/d/Y'); 
    $fNow = date('Y-m-d'); 
    $fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow))); 
    
    if (isset($_SESSION['usuario'])){ 
        $listaLinea = listar_Lineas_Producto($_SESSION['productoU']); 
    } else { 
        $listaLinea = listar_Lineas_Producto($_SESSION['productoL']); 
    } 
    
    for ($i = 0; $i < count($listaLinea); $i++) { 
        $lblLinea[$i] = $listaLinea[$i][0]; 
        if (isset($listaLinea[$i][1])){ 
            $lblLinea[$i] = $listaLinea[$i][0].' - '.$listaLinea[$i][1]; 
        } 
    } 
    
    $fechaP = date("M, Y"); 
    
?> 

<form id="fITProduccion" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mITProduccion" > 
        <div class="modal-dialog modal-lg" style="width: 30vw" > 
            <div class="modal-content" > 
                <div class="modal-header" style="background: #02538B;" > 
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > REGISTRAR TARGETS DE PRODUCCION</h4> 
                </div> 
                <div class="modal-body" style=" max-height: calc(100vh - 180px); overflow-y: auto;" > 
                    <div id="alertITProduccion" ></div> 
                    <div> 
                        <div class="form-row" > 
                            <div align="center" class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref" >REGISTRA </label> 
                                <input class="form-control" id="detecta" name="detecta" maxlength="15" style="width: 80%" value="<?php echo $_SESSION["nickName"] ?>" type="text" onlyread  />
                            </div> 
                            <div align="center" class="col-md-3" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref" >LINEA </label> 
                                <select class="form-control" id="lineaTP" name="lineaTP" style="width: 99%; height: 33px;" > 
                                    <?php for ($i = 0; $i < count($listaLinea); $i++) { ?> 
                                    <option value="<?php echo $listaLinea[$i][0]; ?>" <?php if ($_SESSION['linea'] == $listaLinea[$i][0] ){ ?> selected <?php } ?>  > <?php echo $lblLinea[$i]; ?> </option> 
                                    <?php } ?> 
                                </select> 
                            </div> 
                            <div align="center" class="col-md-4 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref" >MES </label>
                                <input id="mPickerMP" name="mPickerMP" class="btn btn-secondary btn-sm" value="<?php echo $fechaP ?>" style="border-color: #888; width: 80%; " />
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%; margin-top: 8px" > 
                    <div class="panel" id="pnlOplLinea" name="pnlOplLinea" style="margin-top: -1.5vh" > 
                        <div class="panel panel-info" style="width: 100%;" > 
                            <div class="panel-heading" ><strong> Targets <strong></div>
                            <div name="panelBody" class="panel-body" > 
                                <div class="form-row" > 
                                    <div align="left" class="col-md-2" > 
                                        <label class="my-1 mr-2" >C.WEEK</label> 
                                    </div> 
                                    <div align="center" class="col-md-3" > 
                                        <label class="my-1 mr-2" >RANGO</label> 
                                    </div> 
                                    <div align="center" class="col-md-3" > 
                                        <label class="my-1 mr-2" >META</label> 
                                    </div> 
                                    <div  class="col-md-2 mb-4" > 
                                        <label style="margin-left: 23px;" >L-V </label> 
                                    </div> 
                                    <div align="center" class="col-md-2 mb-4" > 
                                        <label style="margin-left: -5px;" >ALL </label> 
                                    </div> 
                                </div> 
                                <div id="panelBody" > 
                                    
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer" > 
                    <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw" > </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 
