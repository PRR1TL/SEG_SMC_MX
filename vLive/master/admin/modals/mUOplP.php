<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

$cLineas = listarLineas();

?>
<form id="fUOplP" method="post" >
    <div class="modal " tabindex="-1" role="dialog" id="mUOplP">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content">
                <div class="modal-header" style="background: #02538B; " >
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > MODIFICAR OPL </h4>
                </div>
                <div class="modal-body" style=" max-height: calc(100vh - 190px); overflow-y: auto;">
                    <div id="alertOplPU"></div> 
                    
                    <input id="idOplPU" name="idOplPU" class="no" />
                    <input id="idOplPrPU" name="idOplPrPU" class="no" />
                    <div> 
                        <div class="form-row"> 
                            <input id="tipOplLU" name="tipOplLU" value="1" class="no" >
                            <div class="col-md-6 mb-1">
                                <label for="validationCustom01">Proyecto</label> 
                                <select class="form-control" id="proyectoPU" name="proyectoPU" style="height: 33px; " > 
                                    <option value="1">L. Proyectos</option>
                                </select> 
                            </div> 
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Detectado por:</label> 
                                <input class="form-control"  id="detectaPU" name="detectaPU" maxlength="15" style="width: 80%" type="text" readonly />
                            </div>                             
                            
                            <div class="col-md-3 contenidoCentrado" > 
                                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Fecha Actual: </label>
                                <input class="form-control" id="fRegistroPU" name="fRegistroPU" style="width: 55%" type="text" readonly />
                            </div> 
                        </div> 
                    </div> 
                    <hr style="width: 100%">
                    <br>
                    <div class="panel " id="pnlOplProjectPU" name="pnlOplProjectPU" style="margin-top: -3vh">                         
                        <!--DATOS DE LA DESVIACION--> 
                        <div class="panel panel-info" style="width: 100%;">
                            <div class="panel-heading" ><strong> Acci&oacute;n <strong></div>
                            <div class="panel-body" >
                                <div class="form-row">                                    
                                    <div class="col-md-5 mb-1">
                                        <label for="validationCustom01">Acci&oacute;n requerida</label> 
                                        <input type="text" class="form-control" id="accionPU" name="accionPU" placeholder="Accion requerida" readonly >                                      
                                    </div> 
                                    
                                    <div class="col-md-2 mb-2"> 
                                        <label for="validationCustom02"> &Aacute;rea </label> 
                                        <input type="text" class="form-control" id="areaPU" name="areaPU" placeholder="&Aacute;rea" readonly > 
                                    </div> 
                                    <div class="col-md-2 mb-2"> 
                                        <label for="validationCustomUsername">Responsable</label> 
                                        <input type="text" class="form-control" id="responsablePU" name ="responsableP" placeholder="Responsable" readonly >
                                    </div> 
                                    <div class="col-md-2 mb-2">  
                                        <label for="validationCustomUsername">F. Compromiso</label> 
                                        <input type="text" class="form-control" id="fCompromisoPU" name="fCompromisoPU" placeholder="F. COmpromiso" readonly >
                                    </div> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="form-row">                             
                        <div class="col-md-6"> 
                            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Estado</label>
                            <select class="form-control" id="estadoPU" name="estadoPU" style="width: 90%; height: 33px; " >
                                <option value = "1"> Accion definida</option>
                                <option value = "2"> Accion en proceso</option>
                                <option value = "3"> Accion ejecutada </option>
                                <option value = "4"> Accion cerrada </option>  
                            </select> 
                        </div> 
                        <div class="col-md-6"> 
                            <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Ecaluaci&oacute;n </label>
                            <select class="form-control" id="evaluacionPU" name="evaluacionPU" style="width: 90%; height: 33px; " >
                                <option value = "0"> Acorde a plan </option>
                                <option value = "1"> ¡ATENCION! Mejoras requeridas </option>
                                <option value = "2"> ¡PROBLEMA! Contramedidas requeridas </option> 
                            </select> 
                        </div> 
                    </div> 
                </div> 
                <div class="modal-footer"> 
                    <button type="submit" class="btn btn-primary" > ACTUALIZAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


