<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL

$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));
$linea = $_SESSION["linea"]; 

if (isset($_SESSION["producto"])){
    $cLineas = listarLineas_Producto($_SESSION["producto"]); 
} else { 
    $cLineas = listarLineas(); 
}

?>

<script type="text/javascript" > 
    jQuery().ready( 
        function() { 
            changeDatos();
        } 
    ); 

    function changeTIncidencia (tipo){
        if (tipo == 'S'){
            document.getElementById('pnlSeguridad').className = "col-lg-6 col-md-6 si"; 
            document.getElementById('pnlAmbiente').className = "col-lg-6 col-md-6 no"; 
        } else {
            document.getElementById('pnlAmbiente').className = "col-lg-6 col-md-6 si"; 
            document.getElementById('pnlSeguridad').className = "col-lg-6 col-md-6 no"; 
        }
    }

    function operacion(id){ 
        var selectedOption = $('#'+id).find('option:selected');
        var selectedLabel = selectedOption.text(); 
        var pnl = "pnl"+id; 
        if (selectedLabel.trim() == 'OTRA'){ 
            document.getElementById(pnl).className = "si"; 
        } else {
            document.getElementById(pnl).className = "no"; 
        }
    } 
    
    function changeDatos() { 
        var linea = document.getElementById('cmbLineaLA').value;
        var turno = document.getElementById('turno').value; 
        var fechaR = document.getElementById('fecha').value; 
        
        //CONSULTA DE TURNO 
        $.ajax({ 
            type: "POST", 
            url: "./db/admin/cListaAsistencia.php", 
            data: {linea: linea, turno: turno, fechaR:fechaR }, 
            success: function(datos) { 
                $('#tblDatosLinea').html(datos); 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
    } 

</script>

<form id="fISeguridad" method="post" > 
    <div class="modal" tabindex="-1" role="dialog" id="mISeguridad" > 
        <div class="modal-dialog modal-lg" style="width: 120vh; " > 
            <div class="modal-content" style="width: 100%; margin-top: 175px;" > 
                <div class="modal-header" style="background: #02538B;" > 
                    <h4 class="modal-title text-center all-tittles" style="color: #ffffff" > Seguridad y medio ambiente </h4> 
                </div> 
                <div class="modal-body" style="max-height: calc(100vh - 190px); overflow-y: auto;" >  
                    <div class="form-row col-lg-12 col-md-12" > 
                        <div class="col-md-3"  > 
                            <label class="my-1 mr-2" >Linea: </label> 
                            <select class="form-control" id="cmbLineaLA" name="cmbLineaLA" style="height: 38px; margin-left: -5%;" onchange="changeDatos()" > 
                                <?php for ($i = 0; $i < count($cLineas); $i++) { ?> 
                                <option <?php if ($cLineas[$i][0] == $linea ){ ?> selected <?php } ?> value="<?php echo $cLineas[$i][0]; ?>" > <?php echo $cLineas[$i][0]; ?> </option> 
                                <?php } ?> 
                            </select> 
                        </div> 
                        <div class="col-md-3" > 
                            <label class="my-1 mr-2" > Incidencia: </label> 
                            <select class="form-control" id="rIncidencia" name="rIncidencia" style="height: 38px;" onchange="changeTIncidencia(this.value)" > 
                                <option value="S" > Seguridad </option> 
                                <option value="A" > Ambiente </option> 
                            </select> 
                        </div> 
                        <div class="col-md-3" > 
                            <label class="my-1 mr-2" >Fecha: </label> 
                            <input class="form-control" id="fecha" name="fecha" value="<?php echo $fInicio; ?>" type="text" readonly />
                        </div> 
                        <div class="col-md-3" > 
                            <input class="form-control no" id="rNivel" name="rNivel" type="text" /> 
                        </div> 
                    </div> 
                </div> 
                <div id="alertISeguridad"> </div> 
                <hr width="100%" > 
                <div class="col-lg-12 col-md-12" >                     
                    <div class="col-lg-6 col-md-6" id="pnlSeguridad" name="pnlSeguridad" > 
                        <label class="my-1 mr-2" >Tipo de incidencia: </label> 
                        <select class="form-control" id="iSeguridad" name="iSeguridad" style="height: 38px; width: 100%;" > 
                            <option value="Fatal" > Fatal </option> 
                            <option value="Incapacitante" > Incapacitante </option> 
                            <option value="Primer Auxilio" > Primer Auxilio </option> 
                            <option value="Incidente" > Incidente </option> 
                            <option value="Condicion Insegura" > Condicion Insegura </option> 
                            <option value="Acto Inseguro" > Acto Inseguro </option> 
                        </select > 
                    </div> 
                    <div class="col-lg-6 col-md-6 no" id="pnlAmbiente" name="pnlAmbiente" > 
                        <label class="my-1 mr-2" >Tipo de incidencia: </label> 
                        <select class="form-control" id="iAmbiente" name="iAmbiente" style="height: 38px; width: 100%; " > 
                            <option value="Contingencia" > Contingencia </option> 
                            <option value="Emergencia" > Emergencia </option> 
                            <option value="No Conformidad" > No Conformidad </option> 
                        </select> 
                    </div> 
                    <div class="col-lg-6 col-md-6 " > 
                        <label class="my-1 mr-2" >Descripcion: </label> 
                        <input class="form-control " id="iDescripcion" name="iDescripcion" > 
                    </div> 
                    <div class="modal-footer" style="top: 50px;"> 
                        <button type="button" class="btn btn-danger btn-sm" onclick="window.location = './master/otras/LAsistencia.php'" > VER MAS &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                        <button type="submit" class="btn btn-primary" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 
