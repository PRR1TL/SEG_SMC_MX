<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL 
$fInicio = date('m/d/Y'); 

$cLineas = listarLineas(); 

?>

<script type="text/javascript" > 
    $(document).ready(function() { 
        cLista(); 
    }); 
    
    function cLista(){ 
        //CACHA EL VALOR DEL SELECT 
        var linea = document.getElementById('cmbLineaJ').value ;
        
        $.ajax({ 
            url: './db/admin/cListaJunta.php',
            type: "POST",
            data: {linea: linea}, 
            success: function(datos){ 
                $('#pnlJunta').html(datos); 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
    } 
</script>

<form id="fIJunta" method="post" > 
    <div class="modal fade" tabindex="-1" role="dialog" id="mIJunta" > 
        <div class="modal-dialog modal-lg" style="width: 80vh; margin-top: 200px;" > 
            <div class="modal-content" style="width: 110%; " > 
                <div class="modal-header" style="background: #02538B; " > 
                    <h5 class="modal-title text-center all-tittles" style="color: #ffffff" > Lista de asistencia junta </h5>
                </div> 
                <div class="modal-body" style="max-height: calc(100vh - 190px); overflow-y: auto;" > 
                    <div id="alertIJunta" > </div> 
                    <div class="form-row col-lg-12 col-md-12 " > 
                        <div class="col-md-3" > 
                            <div class="col-md-12 contenidoCentrado" > 
                                <label class="my-1 mr-2" >&Aacute;rea: </label> 
                                <input class="form-control" id="cmbLineaJ" name="cmbLineaJ" style="height: 34px;" value="<?php echo $_SESSION['productoL'] ?>" >
                            </div> 
                        </div>   
                        <div class="col-md-4" > 
                            <div class="col-md-12 contenidoCentrado" >                                  
                                <label class="my-1 mr-2" >Fecha: </label>
                                <input class="form-control" id="fecha" name="fecha" value="<?php echo $fInicio ?>" type="text" readonly />
                            </div>  
                        </div> 
                        <div class="col-md-2" style="align-content: center; align-items: center; " > 
                        </div>
                    </div> 
                    <hr width="100%" > 
                    <div id="pnlJunta" name="pnlJunta"  >
                    </div> 
                </div> 
                <div class="modal-footer" > 
                    <button type="button" class="btn btn-danger btn-sm" onclick="window.location = './master/otras/LJunta.php'" > VER MAS &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                    <button type="submit" class="btn btn-primary btn-sm" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


