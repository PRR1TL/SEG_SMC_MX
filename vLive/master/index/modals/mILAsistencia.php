<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//PARA FECHAS EN TIEMPO REAL 
$fInicio = date('m/d/Y');
$fNow = date('Y-m-d');
$fProp = date('m/d/Y' ,strtotime('+1 day', strtotime($fNow)));

if (isset($_SESSION['productoL'])){
    $cLineas = listarLineas_Producto($_SESSION['productoL']); 
} else { 
    $cLineas = listarLineas(); 
}

for ($i = 0; $i < count($cLineas); $i++) { 
    $lblLinea[$i] = $cLineas[$i][0]; 
    if (isset($cLineas[$i][1])){ 
        $lblLinea[$i] = $cLineas[$i][0].' - '.$cLineas[$i][1]; 
    } 
} 

?>

<script type="text/javascript" > 
    jQuery().ready( 
        function() { 
            changeDatos();
        } 
    ); 

    function operacion(id){ 
        var selectedOption = $('#'+id).find('option:selected');
        var selectedLabel = selectedOption.text(); 
        var pnl = "pnl"+id; 
        if (selectedLabel.trim() == 'OTRA'){ 
            document.getElementById(pnl).className = "si"; 
        } else {
            document.getElementById(pnl).className = "no"; 
        }
    } 
    
    function changeDatos() { 
        var linea = document.getElementById('cmbLineaLA').value;
        var turno = document.getElementById('turno').value; 
        var fechaR = document.getElementById('fecha').value; 
        
        //CONSULTA DE TURNO 
        $.ajax({ 
            type: "POST", 
            url: "./db/admin/cListaAsistencia.php", 
            data: {linea: linea, turno: turno, fechaR:fechaR }, 
            success: function(datos) { 
                $('#tblDatosLinea').html(datos); 
            } 
        }).fail( function( jqXHR, textStatus, errorThrown ) { 
            if (jqXHR.status === 0) { 
                alert('Not connect: Verify Network.'); 
            } else if (jqXHR.status == 404) { 
                alert('Requested page not found [404]'); 
            } else if (jqXHR.status == 500) { 
                alert('Internal Server Error [500].'); 
            } else if (textStatus === 'parsererror') { 
                alert('Requested JSON parse failed.'); 
            } else if (textStatus === 'timeout') { 
                alert('Time out error.'); 
            } else if (textStatus === 'abort') { 
                alert('Ajax request aborted.'); 
            } else { 
                alert('Uncaught Error: ' + jqXHR.responseText); 
            } 
        }); 
    } 

</script>

<form id="fILAsistencia" method="post" >
    <div class="modal fade" tabindex="-1" role="dialog" id="mILAsistencia" >
        <div class="modal-dialog modal-lg" style="width: 120vh; margin-top: 225px;" >
            <div class="modal-content" style="width: 100%; margin-top: 175px;" >
                <div class="modal-header" style="background: #02538B;" >
                    <h5 class="modal-title text-center all-tittles" style="color: #ffffff" > Lista de asistencia </h5>
                </div>
                <div class="modal-body" style="max-height: calc(100vh - 220px); overflow-y: auto;" >  
                    <div class="form-row col-lg-12 col-md-12" > 
                        <div class="col-md-3" >                                  
                            <div class="col-md-10 " style="margin-top: 7%; " > 
                                <label class="my-1 mr-2" >Linea: </label> 
                                <select class="form-control" id="cmbLineaLA" name="cmbLineaLA" style="height: 38px; width: 125%; margin-left: -5%;" onchange="changeDatos()" > 
                                    <?php for ($i = 0; $i < count($cLineas); $i++) { ?> 
                                    <option <?php if ($cLineas[$i][0] == $_SESSION['linea'] ){ ?> selected <?php } ?> value="<?php echo $cLineas[$i][0]; ?>" > <?php echo $lblLinea[$i]; ?> </option> 
                                    <?php } ?> 
                                </select> 
                            </div> 
                        </div>                              
                        <div class="col-md-9" style="padding-left: 5%; " >
                            <label style="margin-left: 33%; " >REGISTRO</label>
                            <hr style="height: 1px; background-color: #B4B4B4; margin-top: -1.4%;" >
                            <div class="form-row col-lg-12 col-md-12" style="margin-top: -3.5%;" > 
                                <div class="col-md-4" > 
                                    <label class="my-1 mr-2" >Fecha: </label> 
                                    <input class="form-control" id="fecha" name="fecha" value="<?php echo $fInicio; ?>" type="text" readonly />
                                </div> 
                                <div class="col-md-5" > 
                                    <label class="my-1 mr-2" > Turno: </label> 
                                    <select class="form-control" id="turno" name="turno" style="height: 38px;" > 
                                        <option value="1" > Primer Turno </option> 
                                        <option value="2" > Segundo Turno </option> 
                                        <option value="3" > Tercer Turno </option> 
                                    </select> 
                                </div> 
                                <div class="col-md-3" > 
                                    <label class="my-1 mr-2" >Horas: </label> 
                                    <input class="form-control" id="horas" name="horas" type="text" required /> 
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div id="alertILAsistencia"> </div> 
                    <hr width="100%" > 
                    <div id ="tblDatosLinea" > </div> 
                    <br>                      
                </div> 
                <div class="modal-footer"> 
                    <button type="button" class="btn btn-danger btn-sm" onclick="window.location = './master/otras/LAsistencia.php'" > VER MAS &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button>
                    <button type="submit" class="btn btn-primary btn-sm" > GUARDAR &nbsp;<i class="zmdi zmdi-skip-next zmdi-hc-fw"> </i> </button> 
                </div> 
            </div> 
        </div> 
    </div> 
</form> 


