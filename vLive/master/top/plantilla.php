<HTML>
    <LINK REL=StyleSheet HREF="../../css/estilo.css" TYPE="text/css" MEDIA=screen>
    <title>Top 5: Técnicas</title>
    <link href="../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                
        <?php
            include '../../db/ServerFunctions.php';
            //$pLine = $_REQUEST['pLine']; 
            $pLine = 'L003';            
            //$pMonth = $_REQUEST['pMonth'];
            $pMonth = date("m");
            //$pYear = $_REQUEST['pYear'];
            $pYear = date("Y");
            $varMesStr = listarMeses();
            
            $ultimoDiaMes=date("t",mktime(0,0,0,$pMonth,1,$pYear));
            $diaIni = "01/01/".$pYear;
            $diaFn = $pMonth.'/'.$ultimoDiaMes.'/'.$pYear;          
            
            $dI = $pYear."-01-01";
            $dF = $pYear."-".$pMonth."-".$ultimoDiaMes;
            
            $pDiaI = 1;
            $pDiaF = $ultimoDiaMes;
            $band = 0;
            
            $xI = $diaIni;
            $xF = $diaFn;
            
            $vImp;
            $pzas;
            $min;
            $seg;
            $problemaTec;
            $operacionTec;
            $opTec;
            $durTec;
            $tcTec;             
            $pzasT;
            
            $dattop3 = t3Tecnicas($pLine,$dI,$dF);
            //$diasArrObj = listarDiasMes($pLine,$pYear,$pMonth);
            
            $x = 1;
                     
            $titulo[0] = "Top 5: Técnicas (Duración)";
            
            if (isset($_REQUEST["btnCalcular"])) {                
                $xI = isset($_POST['dateI']) ? $_POST['dateI'] : '';
                $xF = isset($_POST['dateE']) ? $_POST['dateE'] : '';                 
                
                if($pDiaI > $pDiaF){
                    echo '<script language="javascript">alert("El rango de Dias no es correcto \n\n Dia Inicio: '.$xI.' no puede ser mayor a Día Final:'.$xF.'");</script>';  
                    $pDiaI = 01;
                    $pDiaF = $ultimoDiaMes;

                    $xI = $diaIni;
                    $xF = $diaFn;
                } else {
                    $pDiaI = date("Y-m-d", strtotime($xI));
                    $pDiaF = date("Y-m-d", strtotime($xF));
                }
                
                $x = 0;
                $opcion = $_REQUEST["cmbOpcion"];
                if ($opcion == "1" ) { 
                    $dattop3 = t3Tecnicas($pLine,$pDiaI,$pDiaF);

                    for($i = 0; $i < count($dattop3); $i++){
                        $pzasT[$i] = 0;
                        $operacionTec[$i] = $dattop3[$i][0];
                        $problemaTec [$i] = $dattop3[$i][1];
                        $opTec[$i] = (string) $operacionTec[$i]; //cambio de valor para imprimir operacionTecOrg
                        $durTec[$i] = $dattop3[$i][2];      

                        $vImp[$i] = $durTec[$i];
//                        $datNoParte = t3TecnicasNoParte($pLine, $pDiaI, $pDiaF, $operacionTec[$i], $problemaTec[$i]);
//
//                        for($j = 0;  $j < count($datNoParte); $j++){
//                            $noParte[$x] = (string) $datNoParte[$j][0];                    
//                            $durNoParte[$x] = $datNoParte[$j][1];
//                            $seg[$x] = $durNoParte[$x] * 60;
//
////                            $datTcNoParte = t3Tc($pLine, $noParte[$x]);
////                            for($k = 0; $k < count($datTcNoParte); $k++){
////                                $tcTec[$k] = (int) floor($datTcNoParte[$k][0]);
////                                 // TRANSFORMACION DE TIEMPO A PZAS
////                                $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
////                                $pzasT[$i] = $pzasT[$i] + $pzasNP[$k];
////                            }  
////                            $x++;
//                        }
                    }
                    $band = 1;
                    $titulo [0] = "Top 5: Técnicas (Duración)";
                } else if ($opcion == "2"){ //frecunecia
                    $dattop3 = t3TecnicasFrec($pLine,$pDiaI,$pDiaF);

                    for($i = 0; $i < count($dattop3); $i++){
                        $pzasT[$i] = 0;
                        $operacionTec[$i] = $dattop3[$i][0];
                        $problemaTec [$i] = $dattop3[$i][1];
                        $opTec[$i] = (string) $operacionTec[$i]; //cambio de valor para imprimir operacionTecOrg
                        $frec[$i] = $dattop3[$i][2]; 
                        $durTec[$i] = $dattop3[$i][3]; 

                        $vImp[$i] = $frec[$i];

//                        $datNoParte = t3TecnicasNoParte($pLine, $pYear, $pMonth, $pDiaI, $pDiaF, $operacionTec[$i], $problemaTec[$i]);
//
//                        for($j = 0;  $j < count($datNoParte); $j++){
//                            $noParte[$x] = (string) $datNoParte[$j][0];                    
//                            $durNoParte[$x] = $datNoParte[$j][1];
//                            $seg[$x] = $durNoParte[$x] * 60;
//
////                            $datTcNoParte = t3Tc($pLine, $noParte[$x]);
////                            for($k = 0; $k < count($datTcNoParte); $k++){
////                                $tcTec[$k] = (int) floor($datTcNoParte[$k][0]);
////                                 // TRANSFORMACION DE TIEMPO A PZAS
////                                $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
////                                $pzasT[$i] = $pzasT[$i] + $pzasNP[$k];
////                            }  
//                            $x++;
//                        }
                    }
                    $band = 2;
                    $titulo [0] = "Top 5: Técnicas (Frecuencia)";
                } else if($opcion == "3"){
                    $dattop3 = t3Tecnicas($pLine,$pYear,$pMonth,$pDiaI,$pDiaF);

                    for($i = 0; $i < count($dattop3); $i++){
                    $pzasT[$i] = 0;
                    $operacionTec[$i] = $dattop3[$i][0];
                    $problemaTec [$i] = $dattop3[$i][1];
                    $opTec[$i] = (string) $operacionTec[$i]; //cambio de valor para imprimir operacionTecOrg
                    $durTec[$i] = $dattop3[$i][2];      
                    $frec[$i] = $dattop3[$i][3]; 

                        $datNoParte = t3TecnicasNoParte($pLine, $pDiaI, $pDiaF, $operacionTec[$i], $problemaTec[$i]);

                        for($j = 0;  $j < count($datNoParte); $j++){
                            $noParte[$x] = (string) $datNoParte[$j][0];                    
                            $durNoParte[$x] = $datNoParte[$j][1];
                            $seg[$x] = $durNoParte[$x] * 60;

                            $datTcNoParte = t3Tc($pLine, $noParte[$x]);
                            for($k = 0; $k < count($datTcNoParte); $k++){
                                $tcTec[$k] = (int) floor($datTcNoParte[$k][0]);
                                 // TRANSFORMACION DE TIEMPO A PZAS
                                $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
                                $pzasT[$i] = $pzasT[$i] + $pzasNP[$k];
                                //echo 'cNP:',count($datNoParte),' , ', count($datTcNoParte),'<br>';
                                //echo 'np ',$noParte[$x],': ',$durNoParte[$x],' , ',$seg[$x],' , ',$tcTec[$k],' , ',$pzasNP[$k],'<br>'; 
                                //$pzasT[$i] += $pzasNP;
                            }  
                            $x++;
                        }                            
                        $vImp[$i] = $pzasT[$i];
                    }
                    $band = 3;
                    $titulo [0] = "Top 5: Técnicas (Piezas)";
                } else {
                   echo '<script language="javascript">alert("Opcion incorrecta");</script>'; 
                }                                  
            } else{  
                //echo "entra else";
                
                for($i = 0; $i < count($dattop3); $i++){
                    $pzasT[$i] = 0;
                    $operacionTec[$i] = $dattop3[$i][0];
                    $problemaTec [$i] = $dattop3[$i][1];
                    $opTec[$i] = (string) $operacionTec[$i]; //cambio de valor para imprimir operacionTecOrg
                    $durTec[$i] = $dattop3[$i][2]; 
                    $vImp[$i] = $dattop3[$i][2];

//                    $datNoParte = t3TecnicasNoParte($pLine, $pDiaI, $pDiaF, $operacionTec[$i], $problemaTec[$i]);
//                    for($j = 0;  $j < count($datNoParte); $j++){
//                        $noParte[$x] = (string) $datNoParte[$j][0];                    
//                        $durNoParte[$x] = $datNoParte[$j][1];
//                        $seg[$x] = $durNoParte[$x] * 60;
//
////                        $datTcNoParte = t3Tc($pLine, $noParte[$x]);
////                        for($k = 0; $k < count($datTcNoParte); $k++){
////                            $tcTec[$k] = (int) floor($datTcNoParte[$k][0]);
////                             // TRANSFORMACION DE TIEMPO A PZAS
////                            $pzasNP[$k] =  round(($seg[$x] * 1)/$tcTec[$k], 0, PHP_ROUND_HALF_DOWN);
////                            $pzasT[$i] = $pzasT[$i] + $pzasNP[$k];
////                        }  
//                        $x++;
//                    } 
                }     
            }
        ?>
        
    <body>
        <head>
            <a align=center id="headerTop3" class="contenedor">               
                <div class='fila0'>                
                </div>             
                <h5 class="tituloPareto">
                    <?php echo 'Top 5: Técnicos <br>','Linea:&nbsp'.$pLine.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp Mes: '. $varMesStr[$pMonth - 1] ?>
                </h5>            
                <div class="fila1">       
                    <img src="../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
                    <form action="../paretos/pTecnicas2.php" method="POST">
                        <?php
                            echo "<input type="."\"hidden\" name="."\"line\""."value=".$pLine.">";
                            echo "<input type="."\"hidden\" name="."\"month\""."value=".$pMonth.">";
                            echo "<input type="."\"hidden\" name="."\"year\""."value=".$pYear.">";
                        ?>                
                        <button class="btn btn-success btn-sm btnRegresarTop" 
                            onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer'" onmouseout="this.style.background='#008C5B'">
                            Regresar
                        </button> 
                    </form>                    
                        <form action="../principal.php" method="POST">
                        <?php
                            echo "<input type="."\"hidden\" name="."\"contEntrada\""."value=".'1'.">";
                            echo "<input type="."\"hidden\" name="."\"line\""."value=".$pLine.">";
                            echo "<input type="."\"hidden\" name="."\"month\""."value=".$pMonth.">";
                            echo "<input type="."\"hidden\" name="."\"year\""."value=".$pYear.">";
                        ?>
                        <button class="btn btn-primary active btn-sm btnHomeTop" 
                                onmouseover="this.style.background='#0B86D4', this.style.cursor='pointer'" onmouseout="this.style.background='#02538B'">
                            Home
                        </button> 
                    </form>  
                    
                    <div class="pickers">
                        <form aling = "center" action="top3Tecnicas.php" method="POST" >  
                            <ul class="nav justify-content-center">
                                <li class="nav-item">
                                    <input class="btn btn-secondary dropdown-toggle btn-sm datesPickets" type="text" name="dateI" id="dateIni" placeholder="Dia Inicial" value="<?php echo $xI;?>">
                                </li>
                                
                                <li class="nav-item">                        
                                    <input class="btn btn-secondary dropdown-toggle btn-sm datesPickets" type="text" name="dateE" id="dateEnd" placeholder="Dia Final" value="<?php echo $xF;?>">
                                </li>
                                
                                <li class="nav-item">  
                                    <select class=" btn btn-secondary btn-sm datesPickets" name="cmbOpcion" id="Opciones">
                                        <option value="0" >Seleccione</option>
                                        <option value="1" href="#">Duración</option>
                                        <option value="2" href="#">Frecuencia</option>
<!--                                        <option value="3" href="#">Piezas</option>-->
                                    </select>                            
                                </li>
                                
                                <li>
                                    <button class="btn btn-default btn-sm" name="btnCalcular">Calcular</button>
                                </li>                                
                            </ul>
                            <?php
                                echo "<input type="."\"hidden\" name="."\"pLine\""."value=".$pLine.">";
                                echo "<input type="."\"hidden\" name="."\"pMonth\""."value=".$pMonth.">";
                                echo "<input type="."\"hidden\" name="."\"pYear\""."value=".$pYear.">";
                            ?>
                        </form>
                    </div>
                   
                    <!--FUNCIONALIDAD DE LOS DATEPICKET-->
                    <script>
                        $( function() {
                            $( "#dateIni").datepicker({ });
                            $( "#dateEnd").datepicker({ });
                        } ); 

                        $(function() {
                            $("#ini").datepicker({ 
                                onSelect: function(date) { 
                                  $fInicio = date;
                                  alert('Inicio: '+$fInicio);
                                } 
                            });

                            $("#fn").datepicker({ 
                                onSelect: function(date) { 
                                  $fFn = date;
                                  alert('fin: '+ $fFn);
                                } 
                            });
                        });
                    </script>                    
                </div>                
            </a> 
        </head>
        
        <div  id="graficasTop3"> 
            <div aling = "center" id="ptc" class="grafTop3"> 
                <script>
                    chartCPU = new  Highcharts.chart('ptc', {
                    chart: {
                        type: 'bar'
                    },
                    title: {                    
                        text: (function() {
                                    var data = [];
                                    <?php
                                        for($i = 0 ;$i<1;$i++){
                                    ?>
                                    data.push([<?php echo "'$titulo[$i]'";?>]);
                                    <?php } ?>
                                    return data;
                                })()
                    },
                    xAxis: {
                        gridLineWidth: 1,
                        gridLineColor:'#000000',                    
                        categories: (function() {
                                    var data = [];
                                    <?php
                                        for($i = 0 ;$i<count($dattop3);$i++){
                                    ?>
                                    data.push([<?php echo "'$opTec[$i], $problemaTec[$i]'";?>]);
                                    <?php } ?>
                                    return data;
                                })()
                    },
                    yAxis: {
                        min: 0,
                        gridLineWidth: 1,
                        gridLineColor:'#000000',
                        title: {
                            text: <?php if ($band < 2 ){ echo "' Duracion (min)'"; } else if($band == 2) { echo "' Frecuencia'"; } else if($band == 3) { echo "' Piezas'";} ?>
                        }
                    },
                    legend: {
                        reversed: true
                    },
                    tooltip: {
                        valueSuffix: <?php if ($band < 2 ){ echo "' min'"; } else if($band == 2) { echo "' registros'"; } else if($band == 3) { echo "' pzas'";} ?>
                    },
                    plotOptions: {
                        series: {
                            stacking: 'normal'
                        }
                    },
                    series: [{
                        name: 'Incidencia',
                        color: '#08088A',
                        data: (function() {
                                var data = [];
                                <?php
                                    for($i = 0 ;$i < count($dattop3);$i++){
                                ?>
                                data.push([<?php echo $vImp[$i];?>]);
                                <?php } ?>
                                return data;
                            })()
                        }],
                        credits: {
                            enabled: false
                        },
                        responsive: {
                            rules: [{
                                condition: {
                                    maxWidth: 500
                                },
                                chartOptions: {
                                    legend: {
                                        layout: 'horizontal',
                                        align: 'center',
                                        verticalAlign: 'bottom'
                                    }
                                }
                            }]
                        }
                    });
                </script>
            </div>

            <div aling = "center">
                <table  id ="tabla"  class = "tableDescriptionTop3" >
                    <thead>     
                        <tr style="background: #F2F2F2">
                            <th width='100'><span class="textP">Operaci&oacute;n</span></th>
                            <th ><span class="textP">Problema</span></th>
                            <?php
                                if($band == 3 ){
//                                    echo "<th width='100'>";
//                                        echo 'Piezas';
//                                    echo "</th>";                                    
                                    echo "<th width='100'>";
                                        echo 'Duración';
                                    echo "</th>";
                                    echo "<th width='100'>";
                                        echo 'Frecuencia';
                                    echo "</th>";
                                }else if($band == 2 ){
                                    echo "<th width='100'>";
                                        echo 'Frecuencia';
                                    echo "</th>";
                                    echo "<th width='100'>";
                                        echo 'Duración';
                                    echo "</th>";
//                                    echo "<th width='100'>";
//                                        echo 'Piezas';
//                                    echo "</th>";
                                } else {
                                    echo "<th width='100'>";
                                        echo 'Duración';
                                    echo "</th>";
                                    echo "<th width='100'>";
                                        echo 'Frecuencia';
                                    echo "</th>";
//                                    echo "<th width='100'>";
//                                        echo 'Piezas';
//                                    echo "</th>";
                                }
                            ?>
                        </tr>
                    </thead>
                    <tbody>        
                        <?php                        
                            $descripcion;       
                            //CARGA DATOS
                            for($i = 0; $i<count($dattop3);$i++){
                                $vImp[$i] = $dattop3[$i][2];
                                for ($j = 0; $j < 4; $j++){
                                    $descripcion[$i][$j] = $dattop3[$i][$j];
                                }
                                $descripcion[$i][4] = $pzasT[$i];
                            }
                            
                            if($band != 3){
                                //IMPRIME VALORES EN TABLA
                                for($i = 0; $i < count($dattop3); $i++){
                                    echo "<tr>";
                                    for ($j = 0; $j < 4; $j++){
                                        if($j != 1){
                                            echo "<td width='100'>";
                                        } else {
                                            echo "<td>";
                                        }
                                            echo utf8_encode($descripcion[$i][$j]);
                                        echo "</td>";
                                    }
                                    echo "</tr>";
                                }
                            } else {
                                for ($i = 0; $i < count($dattop3); $i++)   {
                                    echo "<tr>";
                                        for($j = 0; $j < 4; $j++){
                                            if( $j == 0){
                                                echo "<td width='100'>";
                                                    echo $operacionTec[$i];
                                            }else if( $j == 1){
                                                echo "<td>";
                                                    echo $problemaTec[$i];
                                            }else if( $j == 2){
                                                echo "<td width='100'>";
                                                    echo $pzasT[$i];
                                            }else if( $j == 3){
                                                echo "<td width='100'>";
                                                    echo $durTec[$i];
                                            }else if( $j == 4){
                                                echo "<td width='100'>";
                                                    echo $frec[$i];
                                            }
                                            echo "</td>";
                                        }
                                    echo "</tr>";
                                }
                            }                            
                        ?>        
                    </tbody> 
                </table>
            </div>            
        </div>
    </body>
</html>
