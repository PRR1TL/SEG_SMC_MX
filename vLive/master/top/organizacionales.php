<!--
    Created on  : 18/01/2019 14:30:21 PM
    Author      : Areli Pérez Calixto (PRR1TL)
    Description : Archivo maestro para TOP de paros tecnicos
-->

<HTML>
    <LINK rel="stylesheet" href="../../css/tops.css"/>
    <title>Top Organizacionales</title>
       <link href="../../imagenes/circulo.png" rel="shortcut icon" type="image/x-icon" />
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    
    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script> 
    <script src="https://www.amcharts.com/lib/3/serial.js"></script> 
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    
    <?php    
        include '../../db/ServerFunctions.php';
        session_start();
        $date = new DateTime;

        $line = $_SESSION['linea'];
        $anio = $_SESSION['anio'];
        $mes = $_SESSION['mes']; 
        $fIni = date("m/d/Y", strtotime($_SESSION["FIni"]));
        $fFin = date("m/d/Y", strtotime($_SESSION["FFin"]));        
        
    ?>
    
    <head>
        <a align=center id="headerFixed" class="contenedor">   
            <div class='fila0'>                
            </div> 
            <h5 class="tituloPareto">
                <?php echo 'TOP 5: Organizacionales <br>'.$_SESSION['nameLinea']; ?>
            </h5 > 
            <div class="fila1"> 
                <img src="../../imagenes/blanca.jpg" style="margin-top: 0px; margin-left: -100%">
                    <form action="../../iLinea.php" method="POST">
                        <button class="btn btn-primary btn-sm btnRegresar" style="background-color: transparent; border: 0; align-content: flex-start; align-items: flex-start  "
                            onmouseover="this.style.cursor='pointer'" > <img src="../../imagenes/buttons/icon.png" style="width: 40px; height: 40px; margin-left: -5px; margin-top: -10px;" >
                        </button> 
                    </form>
                    <form action="../jidokas/organizacional.php" method="POST">
                        <button  class="btn btn-success btn-sm btnTop3" onmouseover="this.style.background='#2ECC71', this.style.cursor='pointer'" onmouseout="this.style.background='#008C5B'" >
                            Regresar
                        </button> 
                    </form> 
                    
                    <div class="pickers" >                         
                        <ul class="nav justify-content-center" >
                            <li class="nav-item" > 
                                <input class="btn btn-sm datesPickets" type="text" name="dateIni" id="dateIni" value="<?php echo $fIni; ?>" style="border-radius: 5px; border: 1px solid #99919C;" >
                            </li> 
                            &nbsp;
                            <li class="nav-item" > 
                                <input class="btn btn-sm datesPickets" type="text" name="dateEnd" id="dateEnd" placeholder="Dia Final" value="<?php echo $fFin; ?>" style="border-radius: 5px; border: 1px solid #99919C;" >
                            </li> 
                            &nbsp;
                            <li class="nav-item" > 
                                <select class="btn btn-sm datesPickets" name="tipo" id="tipo" onchange="setTipoDatos()" style="border-radius: 5px; border: 1px solid #99919C;" >
                                    <option value="0" >Seleccione</option> 
                                    <option value="1" href="#" >Duración</option> 
                                    <option value="2" href="#" >Frecuencia</option> 
                                    <option value="3" href="#" >Piezas</option> 
                                </select> 
                            </li> 
                            &nbsp;
                        </ul>
                    </div>
                   
                    <!--FUNCIONALIDAD DE LOS DATEPICKET-->
                    <script>
                        jQuery().ready(            
                            function() {
                                getResult();
                                //setInterval("getResult()",300000); //ACTUALIZA CADA 5MIN
                            }
                        );

                        function getResult() {
                            //CUANDO SE ACTIVA LA HOJA TENDREMOS QUE MANDAR SOLO EL TEMA, PARA PODER HACER LA CONSULTA ADECUADA
                            //PARA ELLO SE UTILIZA AJAX   
                            var tipo = document.getElementById("tipo").value;

                            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                            var ini = document.getElementById("dateIni").value; 
                            var fin = document.getElementById("dateEnd").value; 
                            $.ajax({ 
                                url: "../../contenedores/top/tOrg.php", 
                                type: "post", 
                                data: { tipoDato: tipo, fIni: ini, fFin: fin }, 
                                success: function (result) { 
                                    jQuery("#pnlGraficas").html(result); 
                                } 
                            }); 
                        }
                        
                        function setTipoDatos() { 
                            //MANDAMOS EL TIPO DE DATO PARA PODER HACER EL CALCULO DE LO QUE SELECCIONARON 
                            var tipo = document.getElementById("tipo").value; 

                            //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla 
                            var ini = document.getElementById("dateIni").value; 
                            var fin = document.getElementById("dateEnd").value; 
                            $.ajax({
                                url: "../../contenedores/top/tOrg.php",
                                type: "post",
                                data: { tipoDato: tipo, fIni: ini, fFin: fin },
                                success: function (result) {
                                    //Actualizamos el apartado de graficas
                                    //jQuery.post("../contenedores/tecGraf.php",function( data ) {
                                    jQuery("#pnlGraficas").html(result);
                                }
                            }); 
                        }
            
                        $(function() {
                            $("#dateIni").datepicker({
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                maxDate: new Date(),
                                //OYENTES DE LOS PICKERS 
                                onSelect: function(date) {
                                    //Cuando se seleccione una fecha se cierra el panel
                                    $("#ui-datepicker-div").hide();                                
                                    //Pasamos los valores para que se hagan los calculos nuevamente
                                    var tipo = document.getElementById("tipo").value;

                                    //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                                    var ini = document.getElementById("dateIni").value;
                                    var fin = document.getElementById("dateEnd").value; 

                                    //console.log("tipo: "+tipo+" op: "+op+" ini: "+ini+" fin: "+fin);
                                    if (new Date(fin) < new Date(date) ){
                                        alert("OPS! REVISAR RANGO DE FECHA ");
                                    } else {
                                        //Mandamos el ajax para la actualizacion de la tabla                                     
                                        $.ajax({
                                            url: "../../contenedores/top/tOrg.php",
                                            type: "post",
                                            data: { tipoDato: tipo, fIni: ini, fFin: fin },
                                            success: function (result) {
                                                //Actualizamos el apartado de graficas
                                                //jQuery.post("../contenedores/tecGraf.php",function( data ) {
                                                jQuery("#pnlGraficas").html(result);
                                            }
                                        }); 
                                    }
                                }
                            });

                            $("#dateEnd").datepicker({
                                //CONFIGURACION DE FECHA MAXIMA EN EL PICKER 
                                maxDate: new Date(), 
                                //OYENTES DE LOS PICKERS 
                                onSelect: function ( date ) {
                                    //cuando se seleccione una fecha se cierra el panel
                                    $("#ui-datepicker-div").hide();                                
                                    //pasamos los valores para que se hagan los calculos nuevamente
                                    var tipo = document.getElementById("tipo").value;

                                    //Obtenemos la infomracion de los pickers para hacer el recalculo de la tabla
                                    var ini = document.getElementById("dateIni").value;
                                    var fin = document.getElementById("dateEnd").value;                
                                    //console.log("tipo: "+tipo+" op: "+op+" ini: "+ini+" fin: "+fin);
                                    if (new Date(ini) > new Date(date) ) {
                                        alert("OPS! REVISAR RANGO DE FECHA ");
                                    } else {                                
                                        //mandamos el ajax para la actuilizacion de la tabla 
                                        $.ajax({
                                            url: "../../contenedores/top/tOrg.php",
                                            type: "post",
                                            data: { tipoDato: tipo, fIni: ini, fFin: fin },
                                            success: function (result) {
                                                //Actualizamos el apartado de graficas
                                                //jQuery.post("../contenedores/tecGraf.php",function( data ) {
                                                jQuery("#pnlGraficas").html(result);
                                            }
                                        });
                                    }
                                } 
                            }); 
                        }); 
                    </script>   
                    <!--</form>--> 
            </div> 
        </a>   
    </head>
   
    <body>
        
        <div id ="pnlHoja" > 
            <button class="btn btn-sm btn-warning" style="margin-left: 94%; margin-top: 4.5%; " >An&aacute;lisis</button>
            <br><br><br>
            <div id="pnlGraficas" >                
            </div>                     
            <br><br><br><br><br><br><br><br>
        </div> 
    </body>